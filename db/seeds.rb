# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Create superadmin
admin = User.where(email: 'admin@reti.vn').first
unless admin.present?
  admin = User.create(email: 'admin@reti.vn', password: 'vsbgxpFmZzZNlYKX', password_confirmation: 'vsbgxpFmZzZNlYKX', full_name: 'Super Admin', account_type: 0)
end

if admin&.is_superadmin == false
  admin.update_attributes is_superadmin: true
end
class AddAndRemoveColumnsToPermissions < ActiveRecord::Migration[5.2]
  def up
    remove_column :permissions, :can_edit_other
    remove_column :permissions, :can_view_other
    remove_column :permissions, :can_delete_other
    # remove_column :permissions, :can_import

    add_column :permissions, :can_assign, :boolean, default: false
    add_column :permissions, :can_update_price, :boolean, default: false
    add_column :permissions, :data_area, :integer, default: 0
    add_column :permissions, :state_area, :string, array: true, default: []
  end

  def down
    add_column :permissions, :can_edit_other, :boolean
    add_column :permissions, :can_view_other, :boolean
    add_column :permissions, :can_delete_other, :boolean
    # add_column :permissions, :can_import, :boolean

    remove_column :permissions, :can_assign
    remove_column :permissions, :can_update_price
    remove_column :permissions, :data_area
    remove_column :permissions, :state_area
  end
end

class CreateReasonLocks < ActiveRecord::Migration[5.2]
  def change
    create_table :reason_locks do |t|
      t.references :account, foreign_key: {on_delete: :cascade}
      t.string :content

      t.timestamps
    end
  end
end

class CreateKanbanTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :kanban_tickets do |t|
      t.string :name
      t.integer :customer_id
      t.integer :product_id
      t.string :state
      t.text :demand
      t.integer :assignee
      t.bigint :commission
      t.bigint :total_price
      t.integer :source
      t.text :tags, array: true, default: []

      t.timestamps
    end
  end
end

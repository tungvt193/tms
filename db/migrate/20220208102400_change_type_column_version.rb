class ChangeTypeColumnVersion < ActiveRecord::Migration[5.2]
  def change
    change_column :versions, :version, :string
  end
end

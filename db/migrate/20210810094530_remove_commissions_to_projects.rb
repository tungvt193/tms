class RemoveCommissionsToProjects < ActiveRecord::Migration[5.2]
  def up
    remove_column :projects, :commission
    remove_column :projects, :commission_type
    remove_column :projects, :bonus
  end

  def down
    add_column :projects, :commission, :bigint
    add_column :projects, :commission_type, :integer
    add_column :projects, :bonus, :bigint
  end
end

class CreateHistoryInteractions < ActiveRecord::Migration[5.2]
  def change
    create_table :history_interactions do |t|
      t.integer :deal_id
      t.integer :created_by_id
      t.integer :interaction_type
      t.date :interaction_date
      t.text :interaction_content
      t.timestamps
    end
  end
end

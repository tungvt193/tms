class RenameDetailToDescription < ActiveRecord::Migration[5.2]
  def change
    rename_column :investors, :detail, :description
    rename_column :constructors, :detail, :description
    rename_column :developments, :detail, :description
    rename_column :operators, :detail, :description
  end
end

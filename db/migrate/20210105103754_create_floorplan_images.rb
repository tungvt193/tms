class CreateFloorplanImages < ActiveRecord::Migration[5.2]
  def change
    create_table :floorplan_images do |t|
      t.integer :real_estate_type
      t.json :images
      t.references :project, foreign_key: true

      t.timestamps
    end
  end
end

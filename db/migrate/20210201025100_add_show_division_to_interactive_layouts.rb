class AddShowDivisionToInteractiveLayouts < ActiveRecord::Migration[5.2]
  def change
    add_column :interactive_layouts, :show_division, :boolean
  end
end

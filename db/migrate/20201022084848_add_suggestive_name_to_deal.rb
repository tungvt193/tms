class AddSuggestiveNameToDeal < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :suggestive_name, :string
  end
end

class AddCountryCodeToCustomers < ActiveRecord::Migration[5.2]
  def change
    add_column :customers, :country_code, :string
  end
end

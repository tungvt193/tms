class AddFromAppToDeals < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :from_app, :boolean, default: false
  end
end

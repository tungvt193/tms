class AddTicketTypeToCustomerTicket < ActiveRecord::Migration[5.2]
  def change
    add_column :customer_tickets, :ticket_type, :integer, default: 0
    add_column :customer_tickets, :state, :string
    add_column :customer_tickets, :source, :integer
    add_column :customer_tickets, :message, :string
    remove_column :customer_tickets, :note, :string
  end
end

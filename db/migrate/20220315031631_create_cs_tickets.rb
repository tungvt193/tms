class CreateCsTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :cs_tickets do |t|
      t.integer :deal_id
      t.integer :created_by_id
      t.string :deal_state
      t.text :support_level
      t.text :processing_speed
      t.integer :sale_rating
      t.text :sale_consulting_quality
      t.integer :deal_rating
      t.integer :voucher_id
      t.integer :cancel_reason
      t.integer :demand_with_deal
      t.text :extra_support_for_customer
      t.timestamps
    end

    add_column :deals, :cs_state, :integer, default: 1
  end
end

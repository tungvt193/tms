class AddPositionToSaleDocuments < ActiveRecord::Migration[5.2]
  def change
    add_column :sale_documents, :position, :integer
  end
end

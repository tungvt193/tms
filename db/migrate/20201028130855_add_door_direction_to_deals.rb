class AddDoorDirectionToDeals < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :door_direction, :text, array: true, default: []
    change_column :deals, :balcony_direction, :text, array: true, using: 'ARRAY[balcony_direction]::INTEGER[]'
  end
end

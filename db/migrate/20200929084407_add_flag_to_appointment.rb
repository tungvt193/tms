class AddFlagToAppointment < ActiveRecord::Migration[5.2]
  def change
    add_column :appointments, :flag, :boolean, default: false
    remove_column :appointments, :state, :integer
    add_column :appointments, :state, :integer
  end
end

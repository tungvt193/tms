class AddSameDealsToDeal < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :same_deals, :integer, array: true, default: []
    add_index :deals, :same_deals, using: 'gin'
  end
end

class CreateCustomerTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :customer_tickets do |t|
      t.integer :project_id
      t.string :name
      t.integer :gender
      t.string :phone
      t.string :email
      t.integer :city_id
      t.string :address
      t.text :note

      t.timestamps
    end
  end
end

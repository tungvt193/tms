class AddManagerDuplicateCommToRegulations < ActiveRecord::Migration[5.2]
  def change
    add_column :regulations, :manager_duplicate_comm, :boolean, default: false
  end
end

class CreatePopupSteps < ActiveRecord::Migration[5.2]
  def change
    create_table :popup_steps do |t|
      t.integer :popup_id
      t.integer :name
      t.text :content

      t.timestamps
    end
  end
end

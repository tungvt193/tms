class ChangeColumnTypeFromProductsAndProjects < ActiveRecord::Migration[5.2]
  def change
    change_table :products do |t|
      t.change :deposit, :bigint
      t.change :price, :bigint
      t.change :sum_price, :bigint
    end

    change_table :projects do |t|
      t.change :commission, :bigint
      t.change :bonus, :bigint
    end
  end
end

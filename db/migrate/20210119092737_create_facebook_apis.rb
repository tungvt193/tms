class CreateFacebookApis < ActiveRecord::Migration[5.2]
  def change
    create_table :facebook_apis do |t|
      t.integer :user_id
      t.string :uid
      t.string :user_access_token
      t.string :long_lived_user_access_token
      t.string :long_lived_access_token
      t.json :pages

      t.timestamps
    end
  end
end

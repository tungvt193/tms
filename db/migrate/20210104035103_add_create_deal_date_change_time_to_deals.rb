class AddCreateDealDateChangeTimeToDeals < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :create_deal_date, :datetime
  end
end

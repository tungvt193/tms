class RenameNoteColumnInTask < ActiveRecord::Migration[5.2]
  def change
    rename_column :tasks, :note, :result
  end
end

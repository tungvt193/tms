class AddPhoneNumberToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :phone_number, :string, null: true
    add_column :users, :city_id, :integer, null: true
    add_column :users, :main_job, :integer, null: true
  end
end

class AddColumnToDeals < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :contact_status, :integer
    add_column :deals, :interest_level, :integer
    add_column :deals, :interaction_status, :integer
    add_column :deals, :financial_capability, :text
    add_column :deals, :demand_for_advances, :text
  end
end

class AddColumnLogos < ActiveRecord::Migration[5.2]
  def change
    add_column :investors, :logo, :json
    add_column :constructors, :logo, :json
    add_column :developments, :logo, :json
    add_column :operators, :logo, :json
  end
end

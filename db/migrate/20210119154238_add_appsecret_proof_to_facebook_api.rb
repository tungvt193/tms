class AddAppsecretProofToFacebookApi < ActiveRecord::Migration[5.2]
  def change
    add_column :facebook_apis, :appsecret_proof, :string
  end
end

class CreateMasterFormJourneys < ActiveRecord::Migration[5.2]
  def change
    create_table :master_form_journeys do |t|
      t.integer :master_customer_id
      t.string :recent_conversion
      t.string :last_name
      t.string :first_name
      t.string :phone_number
      t.string :email
      t.datetime :recent_conversion_date

      t.timestamps
    end
    add_index :master_form_journeys, :master_customer_id
  end
end

class AddColumnsToDealDetail < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :purchase_purpose, :integer
    add_column :deals, :product_type, :integer
    add_column :deals, :balcony_direction, :integer
    add_column :deals, :interested_product, :string
  end
end

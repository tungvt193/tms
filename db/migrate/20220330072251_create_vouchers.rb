class CreateVouchers < ActiveRecord::Migration[5.2]
  def change
    create_table :vouchers do |t|
      t.text :content
      t.integer :applicable_condition
      t.integer :project_id, array: true
      t.integer :voucher_type

      t.timestamps
    end
  end
end

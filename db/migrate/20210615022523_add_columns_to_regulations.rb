class AddColumnsToRegulations < ActiveRecord::Migration[5.2]
  def change
    add_column :regulations, :free_leader_comm, :float
    add_column :regulations, :free_leader_duplicate_comm, :boolean, default: false
  end
end

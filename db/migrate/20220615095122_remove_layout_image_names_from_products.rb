class RemoveLayoutImageNamesFromProducts < ActiveRecord::Migration[5.2]
  def change
    remove_column :products, :layout_image_names
  end
end

class AddLayoutImageNamesToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :layout_image_names, :text, array: true, default: []
  end
end

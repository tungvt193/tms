class AddLabelsToProject < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :labels, :integer, array: true, default: []
    add_index :projects, :labels, using: 'gin'
  end
end

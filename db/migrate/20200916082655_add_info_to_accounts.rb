class AddInfoToAccounts < ActiveRecord::Migration[5.2]
  def change
    add_column :accounts, :full_name, :string
    add_column :accounts, :phone, :string
    add_column :accounts, :phone_verify, :boolean, default: false
    add_column :accounts, :identity_card, :string
    add_column :accounts, :active, :boolean, default: true
  end
end

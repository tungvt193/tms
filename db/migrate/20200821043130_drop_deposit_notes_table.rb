class DropDepositNotesTable < ActiveRecord::Migration[5.2]
  def up
    drop_table :deposit_notes
  end
  def down
    create_table :deposit_notes do |t|
      t.integer :deposit_id
      t.boolean :is_third_holding
      t.integer :created_by_id
      t.text :content

      t.timestamps
    end
  end
end

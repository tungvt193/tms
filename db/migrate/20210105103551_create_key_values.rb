class CreateKeyValues < ActiveRecord::Migration[5.2]
  def change
    create_table :key_values do |t|
      t.string :icon
      t.string :title
      t.string :description
      t.references :project, foreign_key: true

      t.timestamps
    end
  end
end

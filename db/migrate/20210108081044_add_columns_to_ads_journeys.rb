class AddColumnsToAdsJourneys < ActiveRecord::Migration[5.2]
  def change
    add_column :ads_journeys, :image_link, :string
    add_column :ads_journeys, :content_link, :string
  end
end

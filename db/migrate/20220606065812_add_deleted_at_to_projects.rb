class AddDeletedAtToProjects < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :deleted_at, :datetime
    add_index :projects, :deleted_at
    add_column :products, :deleted_at, :datetime
    add_index :products, :deleted_at
  end
end

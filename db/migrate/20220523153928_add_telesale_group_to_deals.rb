class AddTelesaleGroupToDeals < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :telesale_group, :string, null: true
  end
end

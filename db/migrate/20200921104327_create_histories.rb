class CreateHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :histories do |t|
      t.integer :deal_id
      t.text :content

      t.timestamps
    end
  end
end

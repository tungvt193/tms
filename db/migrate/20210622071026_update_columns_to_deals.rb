class UpdateColumnsToDeals < ActiveRecord::Migration[5.2]
  def up
    add_column :deals, :uninterested_reason, :integer
    add_column :deals, :price_range, :integer
    add_column :deals, :acreage_range, :integer
    add_column :deals, :product_selection_criteria, :integer, array: true, default: []
    remove_column :deals, :demand
    remove_column :deals, :commission
    remove_column :deals, :interest_level
    remove_column :deals, :interaction_status
    remove_column :deals, :canceled_note
    remove_column :deals, :reason_not_recall
    rename_column :deals, :reason_to_choose, :product_type_detail
  end

  def down
    remove_column :deals, :uninterested_reason
    remove_column :deals, :price_range
    remove_column :deals, :acreage_range
    remove_column :deals, :product_selection_criteria
    add_column :deals, :demand, :text
    add_column :deals, :commission, :bigint
    add_column :deals, :interest_level, :integer
    add_column :deals, :interaction_status, :integer
    add_column :deals, :canceled_note, :text
    add_column :deals, :reason_not_recall, :string
    rename_column :deals, :product_type_detail, :reason_to_choose
  end
end

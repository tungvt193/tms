class CreateGalleries < ActiveRecord::Migration[5.2]
  def change
    create_table :galleries do |t|
      t.string :name
      t.string :ancestry
      t.string :image
      t.string :file_type
      t.string :file_size
      t.string :dimension
      t.integer :uploaded_by_id

      t.timestamps
    end
    add_index :galleries, :ancestry
  end
end

class UpdateColumnCustomerPersonas < ActiveRecord::Migration[5.2]
  def change
    add_column :customer_personas, :dob, :date
    CustomerPersona.all.each do |cp|
      next if cp.yob == nil
      cp.update_column(:dob, "1/1/#{cp.yob}".to_date)
    end
    remove_column :customer_personas, :yob
  end
end
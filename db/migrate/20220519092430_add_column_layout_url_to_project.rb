class AddColumnLayoutUrlToProject < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :layout_url, :text, null: true
  end
end

class CreateTransferHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :transfer_histories do |t|
      t.references :deal, foreign_key: true
      t.bigint :transferred_id, null: false

      t.timestamps
    end

    add_index :transfer_histories, :transferred_id
  end
end

class DropTicketNotesTable < ActiveRecord::Migration[5.2]
  def up
    drop_table :ticket_notes
  end
  def down
    create_table :ticket_notes do |t|
      t.integer :customer_ticket_id
      t.integer :created_by_id
      t.text :content

      t.timestamps
    end
  end
end

class AddInteractionDetailToDeal < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :interaction_detail, :integer, array: true
    add_column :deals, :trouble_problem_list, :integer, array: true, default: []
  end
end

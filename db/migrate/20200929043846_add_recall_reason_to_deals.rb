class AddRecallReasonToDeals < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :reason_not_recall, :string
  end
end

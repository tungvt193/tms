class AddUninterestedToDealLogs < ActiveRecord::Migration[5.2]
  def change
    add_column :deal_logs, :uninterested_entered, :datetime
    add_column :deal_logs, :uninterested_reentered, :datetime
    add_column :deal_logs, :uninterested_exited, :datetime
    add_column :deal_logs, :uninterested_time_in, :datetime
  end
end

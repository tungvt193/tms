class AddSomeFieldsToProjects < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :link_video, :text
    add_column :projects, :position_description, :text
    add_column :projects, :position_link_embed, :text
    remove_column :projects, :external_utilities, :text
    add_column :projects, :external_utilities, :integer , array: true, default: []
    add_column :projects, :internal_utility_images, :json
    add_column :projects, :external_utility_images, :json
    add_column :projects, :design_style_images, :json
    add_column :projects, :discount_support, :decimal, precision: 4, scale: 1
    remove_column :projects, :floorplan_images, :json
  end
end

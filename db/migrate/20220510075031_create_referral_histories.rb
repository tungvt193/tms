class CreateReferralHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :referral_histories do |t|
      t.references :user, foreign_key: true
      t.bigint :referred_id
      t.string :referred_code
      t.datetime :referred_date

      t.timestamps
    end
  end
end

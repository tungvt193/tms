class ChangeColumnBankProjects < ActiveRecord::Migration[5.2]
  def change
    rename_column :projects, :bank, :banks
    change_column :projects, :banks, :integer, array: true, using: 'ARRAY[banks]::INTEGER[]'
    add_index :projects, :banks, using: 'gin'
  end
  # IMPORTANT: Never rollback this migration!!!
end

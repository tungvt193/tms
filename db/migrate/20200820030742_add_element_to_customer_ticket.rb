class AddElementToCustomerTicket < ActiveRecord::Migration[5.2]
  def change
    add_column :customer_tickets, :objectable_type, :string
    add_column :customer_tickets, :objectable_id, :integer
    add_index :customer_tickets, :objectable_id
    add_index :customer_tickets, :objectable_type
  end
end

class AddColumnsToDeal < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :created_by_id, :integer
    add_column :deals, :updated_by_id, :integer
    add_column :deals, :project_id, :integer
    add_column :deals, :trouble_problem, :text
    add_column :deals, :assigned_at, :datetime
    rename_column :deals, :tags, :labels
  end
end

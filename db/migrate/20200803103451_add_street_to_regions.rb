class AddStreetToRegions < ActiveRecord::Migration[5.2]
  def change
    add_column :regions, :street, :text
  end
end

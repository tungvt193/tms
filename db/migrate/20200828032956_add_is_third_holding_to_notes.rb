class AddIsThirdHoldingToNotes < ActiveRecord::Migration[5.2]
  def change
    add_column :notes, :is_third_holding, :boolean
  end
end

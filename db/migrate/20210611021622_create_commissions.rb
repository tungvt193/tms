class CreateCommissions < ActiveRecord::Migration[5.2]
  def change
    create_table :commissions do |t|
      t.integer :deal_id
      t.integer :assignee_id
      t.integer :leader_id
      t.integer :manager_id
      t.integer :sale_admin_id
      t.bigint :assignee_comm
      t.bigint :leader_comm
      t.bigint :manager_comm
      t.bigint :admin_comm

      t.timestamps
    end
  end
end

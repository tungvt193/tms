class AddPriceRangeAndAcreageRangeToProjects < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :acreage_range, :text
    add_column :projects, :price_range, :text
  end
end

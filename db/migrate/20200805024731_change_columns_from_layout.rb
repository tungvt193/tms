class ChangeColumnsFromLayout < ActiveRecord::Migration[5.2]
  def change
    remove_column :divisions, :division_id, :integer
    add_column :divisions, :label, :integer
    add_column :divisions, :level, :integer
    add_column :layouts, :level, :integer
    add_column :layouts, :number_of_product, :integer
  end
end

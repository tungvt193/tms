class AddDescriptionForSellerToProjects < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :description_for_seller, :text
  end
end

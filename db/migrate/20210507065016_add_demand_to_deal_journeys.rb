class AddDemandToDealJourneys < ActiveRecord::Migration[5.2]
  def change
    add_column :deal_journeys, :demand, :text
  end
end

class AddCodeToDeals < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :code, :string
  end
end

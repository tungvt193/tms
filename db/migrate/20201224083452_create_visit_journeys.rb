class CreateVisitJourneys < ActiveRecord::Migration[5.2]
  def change
    create_table :visit_journeys do |t|
      t.string :title
      t.string :visited_link
      t.datetime :visited_date
      t.integer :customer_id
      t.integer :visit_id

      t.timestamps
    end
  end
end

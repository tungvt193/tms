class AddCanceledReasonToDeals < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :canceled_reason, :integer
    add_column :deals, :canceled_note, :text
  end
end

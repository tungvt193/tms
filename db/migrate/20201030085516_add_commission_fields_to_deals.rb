class AddCommissionFieldsToDeals < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :maintenance_fee, :bigint
    add_column :deals, :discount, :bigint
    add_column :deals, :product_price, :bigint
    add_column :deals, :contract_signed_at, :datetime
    add_column :deals, :manager_comm, :bigint
    add_column :deals, :leader_comm, :bigint
    add_column :deals, :admin_comm, :bigint
    add_column :deals, :member_comm, :bigint
  end
end

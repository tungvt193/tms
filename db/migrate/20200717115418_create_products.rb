class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :code #Mã sản phẩm
      t.string :name #Căn/Lô or Căn/Sàn
      t.integer :state_id #Trạng thái
      t.integer :project_id #Dự án
      t.integer :subdivision_id #Phân khu
      t.integer :block_id #Tòa or Đường/Dãy
      t.integer :floor_id #Tầng
      t.integer :project_layout_id #Mặt bằng

      t.integer :level #high or low
      t.integer :real_estate_type #Loại hình
      t.string :product_type #Loại sản phẩm
      t.integer :certificate #Hình thức sở hữu
      t.integer :use_term #Thời hạn sở hữu
      t.integer :furniture #Nội thất
      t.integer :furniture_quality #Chất lương nội thất
      t.string :statics #Kết cấu
      t.float :density #Mật độ xây dựng
      t.float :deposit #Số tiền cọc

      t.integer :amount_of_floors #Số tầng
      t.integer :direction #Hướng ban công or Hướng
      t.float :carpet_area #Diện tích thông thủy (m2)
      t.float :built_up_area #Diện tích tim tường (m2)
      t.float :plot_area #Diện tích lô đất
      t.float :floor_area #Diện tích mặt sàn
      t.float :setback_front #Khoảng lùi trước
      t.float :setback_behind #Khoảng lùi sau
      t.text :handover_standards #Tiêu chuẩn bàn giao
      t.text :detail #Chi tiết

      t.integer :living_room
      t.string :bedroom
      t.integer :bath_room
      t.integer :dining_room
      t.integer :multipurpose_room
      t.integer :mini_bar
      t.integer :drying_yard
      t.integer :kitchen
      t.integer :balcony
      t.integer :business_space

      t.integer :currency #Đơn vị tiền tệ
      t.float :price #Đơn giá (Đã bao gồm VAT, KTBT)
      t.float :sum_price #Tổng GT (Đã bao gồm VAT, KPBT)

      t.integer :created_by_id
      t.integer :updated_by_id

      t.integer :lock_version
      t.timestamps
    end

    add_index :products, :name
    add_index :products, :code
    add_index :products, :project_id
    add_index :products, :state_id
    add_index :products, :level
    add_index :products, :subdivision_id
    add_index :products, :block_id
    add_index :products, :floor_id
  end
end

class ChangeColumnRealEstateTypeToInvestDeals < ActiveRecord::Migration[5.2]
  def up
    change_column :deals, :real_estate_type_to_invest, :integer, array: true, using: 'ARRAY[real_estate_type_to_invest]::INTEGER[]'
  end
  def down
    remove_column :deals, :real_estate_type_to_invest, :integer, array: true, using: 'ARRAY[real_estate_type_to_invest]::INTEGER[]'
    add_column :deals, :real_estate_type_to_invest, :integer
  end
end

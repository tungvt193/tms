class AddSourceDetailToDeals < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :source_detail, :string
  end
end

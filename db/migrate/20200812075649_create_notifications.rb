class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications do |t|
      t.integer :user_id
      t.integer :tag
      t.text :content
      t.datetime :read_at

      t.timestamps
    end
  end
end

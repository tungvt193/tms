class AddRefundableDepositTimestampToDealLogs < ActiveRecord::Migration[5.2]
  def change
    add_column :deal_logs, :refundable_deposit_entered, :datetime
    add_column :deal_logs, :refundable_deposit_exited, :datetime
    add_column :deal_logs, :refundable_deposit_reentered, :datetime
    add_column :deal_logs, :refundable_deposit_time_in, :integer
  end
end

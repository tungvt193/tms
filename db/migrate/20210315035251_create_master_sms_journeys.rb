class CreateMasterSmsJourneys < ActiveRecord::Migration[5.2]
  def change
    create_table :master_sms_journeys do |t|
      t.integer :master_customer_id
      t.string :sms_subject
      t.string :phone_received
      t.text :sms_detail
      t.datetime :sms_created_date

      t.timestamps
    end
    add_index :master_sms_journeys, :master_customer_id
  end
end

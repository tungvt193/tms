class AddDealSearchToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :deal_search, :json
  end
end

class AddFieldsToGroups < ActiveRecord::Migration[5.2]
  def up
    remove_column :users, :group_id
    add_column :groups, :city_ids, :integer, array: true, default: []
    add_column :groups, :director_id, :integer
    add_column :groups, :user_ids, :integer, array: true, default: []
  end

  def down
    remove_column :groups, :city_ids
    remove_column :groups, :director_id
    remove_column :groups, :user_ids
    add_column :users, :group_id, :integer
  end
end

class AddUserIdToCustomers < ActiveRecord::Migration[5.2]
  def change
    add_column :customers, :created_by_id, :integer
    add_index :customers, :created_by_id
  end
end

class AddFieldsToProjects < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :queue_lock, :integer, default: 5
    add_column :projects, :unc_confirmation_period_for_sale, :integer, default: 30
    add_column :projects, :unc_confirmation_period_for_sale_admin, :integer, default: 10
    add_column :projects, :confirm_deposit_by_sale_admin, :integer
  end
end

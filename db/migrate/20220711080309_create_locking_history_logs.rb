class CreateLockingHistoryLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :locking_history_logs do |t|
      t.integer :project_id
      t.integer :product_id
      t.integer :user_id

      t.timestamps
    end
  end
end

class AddIsImportToJourneys < ActiveRecord::Migration[5.2]
  def change
    add_column :sms_journeys, :is_import, :boolean, default: false
    add_column :email_journeys, :is_import, :boolean, default: false
    add_column :form_journeys, :is_import, :boolean, default: false
    add_column :visit_journeys, :is_import, :boolean, default: false
    add_column :ads_journeys, :is_import, :boolean, default: false
  end
end

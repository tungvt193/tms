class AddColumnsToDeals < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :second_phone_number, :string
    add_column :deals, :gender, :integer
    add_column :deals, :email, :string
    add_column :deals, :identity_card, :string
    add_column :deals, :city_id, :integer
    add_column :deals, :district_id, :integer
    add_column :deals, :ward_id, :integer
    add_column :deals, :street, :string
    add_column :deals, :yob, :integer
    add_column :deals, :property_ownership, :text
    add_column :deals, :nationality, :string
    add_column :deals, :financial_capability_number, :float
    add_column :deals, :customer_position, :string
    add_column :deals, :nation, :string
    add_column :deals, :domicile, :string
    add_column :deals, :workplace, :string
    add_column :deals, :settlements, :string
    add_column :deals, :marital_status, :integer
    add_column :deals, :people_in_family, :string
    add_column :deals, :physical_appearance, :text
    add_column :deals, :personality, :text
    add_column :deals, :hobby, :text
    add_column :deals, :positions_to_invest, :text
    add_column :deals, :real_estate_type_to_invest, :integer
  end
end

class CreateCompanyRevenues < ActiveRecord::Migration[5.2]
  def change
    create_table :company_revenues do |t|
      t.integer :objectable_id
      t.string :objectable_type
      t.float :percentage
      t.date :start_date
      t.date :end_date
      t.integer :real_estate_type

      t.timestamps
    end
  end
end

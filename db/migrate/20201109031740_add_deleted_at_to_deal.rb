class AddDeletedAtToDeal < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :deleted_at, :datetime
    add_index :deals, :deleted_at

    add_column :notes, :deleted_at, :datetime
    add_index :notes, :deleted_at

    add_column :tasks, :deleted_at, :datetime
    add_index :tasks, :deleted_at

    add_column :appointments, :deleted_at, :datetime
    add_index :appointments, :deleted_at

    add_column :calls, :deleted_at, :datetime
    add_index :calls, :deleted_at

    add_column :feedbacks, :deleted_at, :datetime
    add_index :feedbacks, :deleted_at

    add_column :online_feedbacks, :deleted_at, :datetime
    add_index :online_feedbacks, :deleted_at

    add_column :histories, :deleted_at, :datetime
    add_index :histories, :deleted_at

    add_column :deal_sorts, :deleted_at, :datetime
    add_index :deal_sorts, :deleted_at

    add_column :reminders, :deleted_at, :datetime
    add_index :reminders, :deleted_at

    add_column :notifications, :deleted_at, :datetime
    add_index :notifications, :deleted_at
  end
end

class CreateOtps < ActiveRecord::Migration[5.2]
  def change
    create_table :otps do |t|
      t.string :phone_number
      t.string :otp
      t.datetime :expiry
      t.integer :status, default: 0

      t.timestamps
    end
  end
end

class AddColumnsToPermissions < ActiveRecord::Migration[5.2]
  def up
    add_column :permissions, :can_update_commission, :boolean, default: false
    add_column :permissions, :commission_fields, :string, array: true, default: []
  end

  def down
    remove_column :permissions, :can_update_commission
    remove_column :permissions, :commission_fields
  end
end

class ChangeResultToBeTextInCall < ActiveRecord::Migration[5.2]
  def change
    remove_column :calls, :result, :integer
    add_column :calls, :result, :text
  end
end

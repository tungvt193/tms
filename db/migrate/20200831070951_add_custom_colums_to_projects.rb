class AddCustomColumsToProjects < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :custom_description, :text
    add_column :projects, :custom_sale_policy, :text
    add_column :projects, :custom_utilities, :text
  end
end

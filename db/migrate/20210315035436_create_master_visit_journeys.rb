class CreateMasterVisitJourneys < ActiveRecord::Migration[5.2]
  def change
    create_table :master_visit_journeys do |t|
      t.integer :master_customer_id
      t.string :title
      t.string :visited_link
      t.datetime :visited_date
      t.integer :visit_id

      t.timestamps
    end
    add_index :master_visit_journeys, :master_customer_id
  end
end

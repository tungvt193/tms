class AddDealIdToAppointmentCall < ActiveRecord::Migration[5.2]
  def change
    add_column :appointments, :deal_id, :integer
    add_column :calls, :deal_id, :integer
  end
end

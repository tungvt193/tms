class CreateSmsJourneys < ActiveRecord::Migration[5.2]
  def change
    create_table :sms_journeys do |t|
      t.string :sms_subject
      t.string :phone_received
      t.text :sms_detail
      t.datetime :sms_created_date
      t.integer :customer_id

      t.timestamps
    end
  end
end

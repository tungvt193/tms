class CreatePopups < ActiveRecord::Migration[5.2]
  def change
    create_table :popups do |t|
      t.boolean :active
      t.integer :group
      t.integer :position

      t.timestamps
    end
  end
end

class AddPreStateToDeals < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :pre_state, :string
  end
end

class AddStateChangedAtToDeal < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :state_changed_at, :datetime
  end
end

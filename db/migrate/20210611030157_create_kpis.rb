class CreateKpis < ActiveRecord::Migration[5.2]
  def change
    create_table :kpis do |t|
      t.integer :team_id
      t.string :year
      t.float :jan
      t.float :feb
      t.float :mar
      t.float :apr
      t.float :may
      t.float :jun
      t.float :jul
      t.float :aug
      t.float :sep
      t.float :oct
      t.float :nov
      t.float :dec

      t.timestamps
    end
  end
end

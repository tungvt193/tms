class CreateEmailJourneys < ActiveRecord::Migration[5.2]
  def change
    create_table :email_journeys do |t|
      t.text :subject
      t.string :email
      t.string :preview
      t.integer :email_type
      t.datetime :created_date
      t.integer :customer_id
      t.string :campaign_id

      t.timestamps
    end
  end
end

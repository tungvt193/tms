class RemoveCommissionsToDeals < ActiveRecord::Migration[5.2]
  def up
    remove_column :deals, :maintenance_fee
    remove_column :deals, :discount
    remove_column :deals, :product_price
  end

  def down
    add_column :deals, :maintenance_fee, :bigint
    add_column :deals, :discount, :bigint
    add_column :deals, :product_price, :bigint
  end
end

class CreateRegulations < ActiveRecord::Migration[5.2]
  def change
    create_table :regulations do |t|
      t.string :name
      t.string :state
      t.integer :project_id
      t.integer :real_estate_type, array: true
      t.datetime :start_date
      t.datetime :end_date
      t.float :manager_comm
      t.float :admin_comm
      t.float :leader_comm
      t.boolean :leader_duplicate_comm
      t.float :member_comm_sale
      t.float :member_comm_company

      t.timestamps
    end
  end
end

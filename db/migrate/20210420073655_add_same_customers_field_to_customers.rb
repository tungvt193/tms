class AddSameCustomersFieldToCustomers < ActiveRecord::Migration[5.2]
  def change
    add_column :customers, :same_customers, :integer, default: [], array: true
  end
end

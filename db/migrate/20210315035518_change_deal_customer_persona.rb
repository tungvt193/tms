class ChangeDealCustomerPersona < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :reason_to_choose, :text
    remove_column :deals, :settlements, :text
    add_column :deals, :settlements, :text
    remove_column :deals, :hobby, :text
    add_column :deals, :hobby, :string, array: true, default: []
  end
end

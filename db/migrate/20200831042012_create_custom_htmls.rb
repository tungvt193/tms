class CreateCustomHtmls < ActiveRecord::Migration[5.2]
  def change
    create_table :custom_htmls do |t|
      t.text :content
      t.integer :position

      t.timestamps
    end
  end
end

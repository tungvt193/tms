class AddLeadScoringToDeals < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :lead_scoring, :integer
  end
end

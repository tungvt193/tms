class AddSeoFieldsToProjects < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :meta_title, :text, null: true
    add_column :projects, :meta_description, :text, null: true
  end
end

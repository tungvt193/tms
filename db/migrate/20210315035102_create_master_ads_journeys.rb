class CreateMasterAdsJourneys < ActiveRecord::Migration[5.2]
  def change
    create_table :master_ads_journeys do |t|
      t.integer :master_customer_id
      t.string :fb_ad_name
      t.string :fb_campaign_name
      t.string :fb_ad_group_name
      t.datetime :create_date

      t.timestamps
    end
    add_index :master_ads_journeys, :master_customer_id
  end
end

class AddExternalProjectToDeals < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :is_external_project, :boolean
    add_column :deals, :external_project, :string
  end
end

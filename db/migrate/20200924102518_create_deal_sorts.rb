class CreateDealSorts < ActiveRecord::Migration[5.2]
  def change
    create_table :deal_sorts do |t|
      t.integer :deal_id
      t.integer :user_id
      t.integer :position

      t.timestamps
    end
    add_index :deal_sorts, :deal_id
    add_index :deal_sorts, :user_id
  end
end

class AddDealIdToRemindersAndNotifications < ActiveRecord::Migration[5.2]
  def change
    add_column :reminders, :deal_id, :integer
    add_column :notifications, :deal_id, :integer
  end
end

class AddWebPushToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :endpoint, :string
    add_column :users, :p256dh_key, :string
    add_column :users, :auth_key, :string
    add_column :users, :subscribe, :boolean, default: false
  end
end

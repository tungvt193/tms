class AddUniqueIndexToProduct < ActiveRecord::Migration[5.2]
  def change
    add_index :products, [:code, :project_id], unique: true
  end
end

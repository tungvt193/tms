class AddMostPopularToProjects < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :most_popular, :boolean
  end
end

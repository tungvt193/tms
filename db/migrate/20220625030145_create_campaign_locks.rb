class CreateCampaignLocks < ActiveRecord::Migration[5.2]
  def change
    create_table :campaign_locks do |t|
      t.string :state
      t.datetime :session_opening_time
      t.integer :project_id
      t.integer :enable_locked_for_hot_product, default: 1
      t.string :enable_locked_for_inventory, default: 'Không giới hạn'
      t.integer :unc_confirmation_period_for_sale, default: 30
      t.integer :unc_confirmation_period_for_sale_admin, default: 10
      t.integer :confirm_deposit_by_sale_admin
      t.integer :products, array: true

      t.timestamps
    end
  end
end

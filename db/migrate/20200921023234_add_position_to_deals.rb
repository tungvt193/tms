class AddPositionToDeals < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :position, :integer
  end
end

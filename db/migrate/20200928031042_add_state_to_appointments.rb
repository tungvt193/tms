class AddStateToAppointments < ActiveRecord::Migration[5.2]
  def change
    add_column :appointments, :state, :integer, default: 0
  end
end

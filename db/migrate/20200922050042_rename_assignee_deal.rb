class RenameAssigneeDeal < ActiveRecord::Migration[5.2]
  def change
    rename_column :deals, :assignee, :assignee_id
  end
end

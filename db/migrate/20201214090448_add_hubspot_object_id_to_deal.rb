class AddHubspotObjectIdToDeal < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :hs_oid, :string
  end
end

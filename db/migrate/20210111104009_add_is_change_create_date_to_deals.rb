class AddIsChangeCreateDateToDeals < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :is_change_create_date, :boolean
  end
end

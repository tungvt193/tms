class CreateCustomerPersonas < ActiveRecord::Migration[5.2]
  def change
    create_table :customer_personas do |t|
      t.integer :customer_id
      t.integer :deal_id
      t.string :second_phone_number
      t.string :email
      t.integer :gender
      t.string :identity_card
      t.string :domicile
      t.integer :yob
      t.string :nationality
      t.string :nation
      t.string :workplace
      t.string :financial_capability
      t.string :customer_position
      t.text :property_ownership
      t.string :settlements
      t.integer :city_id
      t.integer :district_id
      t.integer :ward_id
      t.string :street
      t.integer :marital_status
      t.string :people_in_family
      t.string :hobbies, array: true, default: []
      t.text :positions_to_invest
      t.integer :real_estate_type_to_invest, array: true, default: []
      t.datetime :created_at, null: false
      t.datetime :updated_at, null: false
      t.datetime :deleted_at

      t.timestamps
    end
  end
end

class CreateProjectNewUpdates < ActiveRecord::Migration[5.2]
  def change
    create_table :project_new_updates do |t|
      t.integer :project_id
      t.string :title
      t.text :content
      t.timestamps
    end
  end
end

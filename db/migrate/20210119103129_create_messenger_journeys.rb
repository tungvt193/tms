class CreateMessengerJourneys < ActiveRecord::Migration[5.2]
  def change
    create_table :messenger_journeys do |t|
      t.string :conversation_id
      t.text :message
      t.string :from_id
      t.string :from_name
      t.string :to_id
      t.string :to_name
      t.datetime :created_time
      t.integer :customer_id

      t.timestamps
    end
  end
end

class AddColumnToProject < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :price_from, :decimal, precision: 10, scale: 2
    add_column :projects, :price_to, :decimal, precision: 10, scale: 2
    add_column :projects, :area_from, :decimal, precision: 10, scale: 2
    add_column :projects, :area_to, :decimal, precision: 10, scale: 2
    remove_column :projects, :acreage_range, :text
    remove_column :projects, :price_range, :text
  end
end

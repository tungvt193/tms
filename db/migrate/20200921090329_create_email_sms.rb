class CreateEmailSms < ActiveRecord::Migration[5.2]
  def change
    create_table :email_sms do |t|
      t.integer :action_type
      t.text :content
      t.datetime :action_date
      t.integer :deal_id

      t.timestamps
    end
  end
end

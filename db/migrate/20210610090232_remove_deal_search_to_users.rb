class RemoveDealSearchToUsers < ActiveRecord::Migration[5.2]
  def up
    remove_column :users, :deal_search
  end

  def down
    add_column :users, :deal_search, :json
  end
end

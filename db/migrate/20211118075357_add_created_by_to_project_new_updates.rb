class AddCreatedByToProjectNewUpdates < ActiveRecord::Migration[5.2]
  def change
    add_column :project_new_updates, :created_by_id, :integer
  end
end

class AddSourceToProduct < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :source, :integer, default: 0
  end
end

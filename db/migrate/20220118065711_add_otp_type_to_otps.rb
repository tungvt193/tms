class AddOtpTypeToOtps < ActiveRecord::Migration[5.2]
  def change
    add_column :otps, :otp_type, :integer
  end
end

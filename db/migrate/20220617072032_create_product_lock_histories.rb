class CreateProductLockHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :product_lock_histories do |t|
      t.integer :user_id
      t.integer :project_id
      t.integer :product_id
      t.string :state
      t.datetime :deadline_for_sale
      t.datetime :deadline_for_sale_admin
      t.text :unc_image
      t.text :failure_reason

      t.timestamps
    end
  end
end

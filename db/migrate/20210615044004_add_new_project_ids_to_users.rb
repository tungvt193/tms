class AddNewProjectIdsToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :project_selected_ids, :integer, array: true, default: []
  end
end

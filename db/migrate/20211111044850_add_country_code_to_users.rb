class AddCountryCodeToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :country_code, :string, null: true
    add_column :users, :phone_verify, :boolean, default: false
  end
end

class ChangeAasmStateColumns < ActiveRecord::Migration[5.2]
  def change
    remove_column :products, :state_id, :integer
    add_column :products, :state, :string
    add_column :deposits, :state, :string
  end
end

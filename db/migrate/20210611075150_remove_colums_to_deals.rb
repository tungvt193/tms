class RemoveColumsToDeals < ActiveRecord::Migration[5.2]
  def up
    remove_column :deals, :manager_comm
    remove_column :deals, :leader_comm
    remove_column :deals, :admin_comm
    remove_column :deals, :member_comm
    remove_column :deals, :sub_source
    remove_column :deals, :second_phone_number
    remove_column :deals, :gender
    remove_column :deals, :email
    remove_column :deals, :identity_card
    remove_column :deals, :domicile
    remove_column :deals, :nationality
    remove_column :deals, :nation
    remove_column :deals, :workplace
    remove_column :deals, :customer_position
    remove_column :deals, :property_ownership
    remove_column :deals, :settlements
    remove_column :deals, :city_id
    remove_column :deals, :district_id
    remove_column :deals, :ward_id
    remove_column :deals, :street
    remove_column :deals, :marital_status
    remove_column :deals, :people_in_family
    remove_column :deals, :positions_to_invest
    remove_column :deals, :real_estate_type_to_invest
    remove_column :deals, :financial_capability_number
    remove_column :deals, :physical_appearance
    remove_column :deals, :yob
    remove_column :deals, :personality
    remove_column :deals, :hobby
  end

  def down
    add_column :deals, :manager_comm, :bigint
    add_column :deals, :leader_comm, :bigint
    add_column :deals, :admin_comm, :bigint
    add_column :deals, :member_comm, :bigint
    add_column :deals, :sub_source, :string
    add_column :deals, :second_phone_number, :string
    add_column :deals, :gender, :integer
    add_column :deals, :email, :string
    add_column :deals, :identity_card, :string
    add_column :deals, :domicile, :string
    add_column :deals, :nationality, :string
    add_column :deals, :nation, :string
    add_column :deals, :workplace, :string
    add_column :deals, :customer_position, :string
    add_column :deals, :property_ownership, :text
    add_column :deals, :settlements, :string
    add_column :deals, :city_id, :integer
    add_column :deals, :district_id, :integer
    add_column :deals, :ward_id, :integer
    add_column :deals, :street, :string
    add_column :deals, :marital_status, :integer
    add_column :deals, :people_in_family, :string
    add_column :deals, :positions_to_invest, :text
    add_column :deals, :real_estate_type_to_invest, :integer
    add_column :deals, :financial_capability_number, :float
    add_column :deals, :physical_appearance, :text
    add_column :deals, :yob, :integer
    add_column :deals, :personality, :text
    add_column :deals, :hobby, :text
  end
end


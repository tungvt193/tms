class ChangeTypeColumnInterationDate < ActiveRecord::Migration[5.2]
  def self.up
    change_table :history_interactions do |t|
      t.change :interaction_date, :datetime
    end
  end

  def self.down
    change_table :history_interactions do |t|
      t.change :interaction_date, :date
    end
  end
end

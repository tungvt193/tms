class AddRelatedUrlToReminder < ActiveRecord::Migration[5.2]
  def change
    add_column :reminders, :related_url, :string
  end
end

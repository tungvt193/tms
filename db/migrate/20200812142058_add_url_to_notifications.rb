class AddUrlToNotifications < ActiveRecord::Migration[5.2]
  def change
    add_column :notifications, :related_url, :string
  end
end

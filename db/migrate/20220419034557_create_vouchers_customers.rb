class CreateVouchersCustomers < ActiveRecord::Migration[5.2]
  def change
    create_table :vouchers_customers do |t|
      t.integer :customer_id
      t.integer :voucher_id
      t.integer :deal_id
      t.integer :cs_ticket_id

      t.timestamps
    end
  end
end

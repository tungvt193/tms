class AddHubspotContactIdToDeal < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :hubspot_contact_id, :string
  end
end

class CreateDealJourneys < ActiveRecord::Migration[5.2]
  def change
    create_table :deal_journeys do |t|
      t.integer :deal_id
      t.datetime :create_date
      t.text :content
      t.integer :lead_scoring
      t.string :next_title
      t.datetime :next_deadline

      t.timestamps
    end
  end
end

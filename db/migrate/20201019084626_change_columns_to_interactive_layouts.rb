class ChangeColumnsToInteractiveLayouts < ActiveRecord::Migration[5.2]
  def change
    add_column :interactive_layouts, :project_id, :integer
    add_column :interactive_layouts, :division_id, :integer
    remove_column :interactive_layouts, :objectable_id, :integer
    remove_column :interactive_layouts, :objectable_type, :string
  end
end

class AddIsdisplayToBanner < ActiveRecord::Migration[5.2]
  def change
    add_column :banners, :is_display, :boolean, default: true
  end
end

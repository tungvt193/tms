class CreateCalls < ActiveRecord::Migration[5.2]
  def change
    create_table :calls do |t|
      t.integer :task_id
      t.integer :result
      t.datetime :contact_date

      t.timestamps
    end
  end
end

class AddCreatedByIdToModel < ActiveRecord::Migration[5.2]
  def change
    add_column :calls, :created_by_id, :integer
    add_column :feedbacks, :created_by_id, :integer
    add_column :online_feedbacks, :created_by_id, :integer
  end
end

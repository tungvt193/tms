class AddSlugToSaleDocuments < ActiveRecord::Migration[5.2]
  def change
    add_column :sale_documents, :slug, :string
  end
end

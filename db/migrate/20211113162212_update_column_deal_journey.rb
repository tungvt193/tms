class UpdateColumnDealJourney < ActiveRecord::Migration[5.2]
  def change
    remove_column :deal_journeys, :demand
    remove_column :deal_journeys, :lead_scoring
    remove_column :deal_journeys, :next_title
    remove_column :deal_journeys, :next_deadline
  end
end

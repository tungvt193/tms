class RemoveHsOid < ActiveRecord::Migration[5.2]
  def change
    remove_column :deals, :hs_oid
    remove_column :customers, :hs_oid
  end
end

class RenameEmailSmToOnlineFeedback < ActiveRecord::Migration[5.2]
  def change
    rename_table :email_sms, :online_feedbacks
  end
end

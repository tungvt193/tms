class AddPositionsToInteractiveLayouts < ActiveRecord::Migration[5.2]
  def change
    add_column :interactive_layouts, :positions, :text
  end
end

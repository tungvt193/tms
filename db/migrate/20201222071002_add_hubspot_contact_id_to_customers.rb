class AddHubspotContactIdToCustomers < ActiveRecord::Migration[5.2]
  def change
    add_column :customers, :hubspot_contact_id, :string
  end
end

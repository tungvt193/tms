class AddTelesaleInfoToDeals < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :miss_project, :boolean, default: false
    add_column :deals, :telesale_note, :text
    add_column :deals, :telesale_interest_detail, :text
    add_column :deals, :callio_campaign_id, :string
  end
end

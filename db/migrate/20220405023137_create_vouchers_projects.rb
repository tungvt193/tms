class CreateVouchersProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :vouchers_projects do |t|
      t.integer :project_id
      t.integer :voucher_id

      t.timestamps
    end
  end
end

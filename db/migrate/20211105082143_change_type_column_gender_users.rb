class ChangeTypeColumnGenderUsers < ActiveRecord::Migration[5.2]
  def change
    User.update_all(gender: nil)
    change_column :users, :gender, "integer USING CAST(gender AS integer)"
  end
end

class AddHubspotObjectIdToCustomer < ActiveRecord::Migration[5.2]
  def change
    add_column :customers, :hs_oid, :string
  end
end

class AddSubSourceToDeals < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :sub_source, :string
  end
end

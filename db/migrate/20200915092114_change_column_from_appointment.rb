class ChangeColumnFromAppointment < ActiveRecord::Migration[5.2]
  def change
    rename_column :appointments, :deal_id, :task_id
  end
end

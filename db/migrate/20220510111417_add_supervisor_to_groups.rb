class AddSupervisorToGroups < ActiveRecord::Migration[5.2]
  def change
    add_column :groups, :supervisor_id, :integer
  end
end

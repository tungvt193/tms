class CreateFormJourneys < ActiveRecord::Migration[5.2]
  def change
    create_table :form_journeys do |t|
      t.integer :customer_id
      t.string :recent_conversion
      t.string :last_name
      t.string :first_name
      t.string :phone_number
      t.string :email
      t.datetime :recent_conversion_date

      t.timestamps
    end
  end
end

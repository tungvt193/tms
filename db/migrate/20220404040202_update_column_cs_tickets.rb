class UpdateColumnCsTickets < ActiveRecord::Migration[5.2]
  def change
    change_column :cs_tickets, :processing_speed, "integer USING CAST(processing_speed AS integer)"
    change_column :cs_tickets, :sale_consulting_quality, "integer USING CAST(sale_consulting_quality AS integer)"
  end
end

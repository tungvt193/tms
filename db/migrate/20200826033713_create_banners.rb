class CreateBanners < ActiveRecord::Migration[5.2]
  def change
    create_table :banners do |t|
      t.integer :banner_type, default: 0
      t.string :title
      t.string :sub_title
      t.string :banner_desktop
      t.string :banner_mobile
      t.string :link
      t.integer :position

      t.timestamps
    end
  end
end

class AddProductLockHitoryToDeals < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :product_lock_history_id, :integer, default: nil
  end
end

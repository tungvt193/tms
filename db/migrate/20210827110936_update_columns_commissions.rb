class UpdateColumnsCommissions < ActiveRecord::Migration[5.2]
  def up
    remove_column :commissions, :assignee_id
    remove_column :commissions, :leader_id
    remove_column :commissions, :manager_id
    remove_column :commissions, :sale_admin_id
    remove_column :commissions, :assignee_comm
    remove_column :commissions, :leader_comm
    remove_column :commissions, :manager_comm
    remove_column :commissions, :admin_comm

    add_column :commissions, :calculation_type, :integer
    add_column :commissions, :cross_selling_agent_code, :string
    add_column :commissions, :applicable_policy, :integer
    add_column :commissions, :seller_type, :integer
    add_column :commissions, :reti_commission_rate, :float
    add_column :commissions, :reti_actually_rate, :float
    add_column :commissions, :reti_commission_rate_for_seller, :float
    add_column :commissions, :seller_commission_rate_will_be_received, :float
    add_column :commissions, :seller_commission_rate_from_reti, :float
    add_column :commissions, :price_to_calculate_commission_for_seller, :bigint
    add_column :commissions, :temporary_commission, :bigint
    add_column :commissions, :actually_commission, :bigint
  end

  def down
    add_column :commissions, :assignee_id, :integer
    add_column :commissions, :leader_id, :integer
    add_column :commissions, :manager_id, :integer
    add_column :commissions, :sale_admin_id, :integer
    add_column :commissions, :assignee_comm, :bigint
    add_column :commissions, :leader_comm, :bigint
    add_column :commissions, :manager_comm, :bigint
    add_column :commissions, :admin_comm, :bigint

    remove_column :commissions, :calculation_type
    remove_column :commissions, :cross_selling_agent_code
    remove_column :commissions, :applicable_policy
    remove_column :commissions, :seller_type
    remove_column :commissions, :reti_commission_rate
    remove_column :commissions, :reti_actually_rate
    remove_column :commissions, :reti_commission_rate_for_seller
    remove_column :commissions, :seller_commission_rate_will_be_received
    remove_column :commissions, :seller_commission_rate_from_reti
    remove_column :commissions, :price_to_calculate_commission_for_seller
    remove_column :commissions, :temporary_commission
    remove_column :commissions, :actually_commission
  end
end

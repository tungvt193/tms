class CreateDocuments < ActiveRecord::Migration[5.2]
  def change
    create_table :documents do |t|
      t.string :name
      t.string :file
      t.text :description
      t.string :preview_image
      t.references :project, foreign_key: true

      t.timestamps
    end
  end
end

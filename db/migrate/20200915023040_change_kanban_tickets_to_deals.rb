class ChangeKanbanTicketsToDeals < ActiveRecord::Migration[5.2]
  def change
    rename_table :kanban_tickets, :deals
    rename_column :appointments, :kanban_ticket_id, :deal_id
  end
end

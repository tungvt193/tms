class AddFromWebsiteToDeals < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :from_website, :boolean
  end
end

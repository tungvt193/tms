class RemoveCustomerIdFromNote < ActiveRecord::Migration[5.2]
  def change
    remove_column :notes, :customer_id, :integer
    add_column :notes, :objectable_type, :string
    add_column :notes, :objectable_id, :integer
    add_index :notes, :objectable_id
    add_index :notes, :objectable_type
  end
end

class CreateInteractiveLayouts < ActiveRecord::Migration[5.2]
  def change
    create_table :interactive_layouts do |t|
      t.integer :objectable_id
      t.string :objectable_type
      t.string :image

      t.timestamps
    end
  end
end

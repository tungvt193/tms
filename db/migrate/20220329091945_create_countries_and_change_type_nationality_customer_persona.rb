class CreateCountriesAndChangeTypeNationalityCustomerPersona < ActiveRecord::Migration[5.2]
  def change
    create_table :countries do |t|
      t.text :name
      t.text :code
    end

    change_column :customer_personas, :nationality, :text
  end
end

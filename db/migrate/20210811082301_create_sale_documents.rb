class CreateSaleDocuments < ActiveRecord::Migration[5.2]
  def change
    create_table :sale_documents do |t|
      t.integer :project_id
      t.string :title
      t.string :file
      t.integer :sale_document_type
      t.timestamps
    end

    add_column :projects, :image_folder_url, :text, null: true
    add_column :projects, :video_folder_url, :text, null: true

    create_table :investor_posts do |t|
      t.integer :project_id
      t.string :title
      t.text :post_url
      t.timestamps
    end
  end
end

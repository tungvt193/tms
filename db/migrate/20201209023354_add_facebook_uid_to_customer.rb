class AddFacebookUidToCustomer < ActiveRecord::Migration[5.2]
  def change
    add_column :customers, :facebook_uid, :string
  end
end

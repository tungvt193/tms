class AddCachedAtToDeals < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :cached_at, :datetime
  end
end

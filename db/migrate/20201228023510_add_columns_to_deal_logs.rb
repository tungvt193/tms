class AddColumnsToDealLogs < ActiveRecord::Migration[5.2]
  def change
    add_column :deal_logs, :approaching_entered, :datetime
    add_column :deal_logs, :approaching_reentered, :datetime
    add_column :deal_logs, :approaching_exited, :datetime
    add_column :deal_logs, :approaching_time_in, :integer
  end
end

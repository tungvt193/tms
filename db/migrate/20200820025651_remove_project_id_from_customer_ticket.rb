class RemoveProjectIdFromCustomerTicket < ActiveRecord::Migration[5.2]
  def change
    remove_column :customer_tickets, :project_id, :integer
  end
end

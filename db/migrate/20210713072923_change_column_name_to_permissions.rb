class ChangeColumnNameToPermissions < ActiveRecord::Migration[5.2]
  def change
    rename_column :permissions, :can_edit, :can_update
    rename_column :permissions, :can_view, :can_edit
    rename_column :permissions, :can_delete, :can_destroy
  end
end

class CreateDealLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :deal_logs do |t|
      t.integer :deal_id
      t.datetime :pending_entered
      t.datetime :interest_entered
      t.datetime :meet_entered
      t.datetime :lock_entered
      t.datetime :confirm_entered
      t.datetime :book_entered
      t.datetime :deposit_entered
      t.datetime :contract_signed_entered
      t.datetime :completed_entered
      t.datetime :canceled_entered
      t.datetime :request_recall_entered

      t.datetime :pending_reentered
      t.datetime :interest_reentered
      t.datetime :meet_reentered
      t.datetime :lock_reentered
      t.datetime :confirm_reentered
      t.datetime :book_reentered
      t.datetime :deposit_reentered
      t.datetime :contract_signed_reentered
      t.datetime :completed_reentered
      t.datetime :canceled_reentered
      t.datetime :request_recall_reentered

      t.datetime :pending_exited
      t.datetime :interest_exited
      t.datetime :meet_exited
      t.datetime :lock_exited
      t.datetime :confirm_exited
      t.datetime :book_exited
      t.datetime :deposit_exited
      t.datetime :contract_signed_exited
      t.datetime :completed_exited
      t.datetime :canceled_exited
      t.datetime :request_recall_exited

      t.integer :pending_time_in
      t.integer :interest_time_in
      t.integer :meet_time_in
      t.integer :lock_time_in
      t.integer :confirm_time_in
      t.integer :book_time_in
      t.integer :deposit_time_in
      t.integer :contract_signed_time_in
      t.integer :completed_time_in
      t.integer :canceled_time_in
      t.integer :request_recall_time_in

      t.datetime :close_date
      t.integer :days_to_close

      t.datetime :deleted_at

      t.timestamps
    end
  end
end

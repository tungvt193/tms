class RemoveColumnReferredCode < ActiveRecord::Migration[5.2]
  def change
    remove_column :referral_histories, :referred_code, :string
  end
end

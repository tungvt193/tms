class UpdateColumnsToPermissions < ActiveRecord::Migration[5.2]
  def change
    remove_column :permissions, :can_update_commission
    rename_column :permissions, :commission_fields, :commission_fields_validation
    add_column :permissions, :commission_fields_can_view, :string, array: true, default: []
    add_column :permissions, :commission_fields_can_edit, :string, array: true, default: []
  end

  def down
    add_column :permissions, :can_update_commission, :boolean, default: false
    remove_column :permissions, :commission_fields_can_view
    remove_column :permissions, :commission_fields_can_edit
  end
end

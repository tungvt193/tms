# frozen_string_literal: true

class DeviseCreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      ## Database authenticatable
      t.string :email, default: ""
      t.string :encrypted_password, null: false, default: ""

      t.timestamps null: false
    end
  end
end

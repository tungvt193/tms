class AddIsCustomerNoteToNote < ActiveRecord::Migration[5.2]
  def change
    add_column :notes, :is_customer_note, :boolean
  end
end

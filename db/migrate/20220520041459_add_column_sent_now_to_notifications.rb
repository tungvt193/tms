class AddColumnSentNowToNotifications < ActiveRecord::Migration[5.2]
  def change
    add_column :notifications, :send_now, :boolean, deafult: true
  end
end

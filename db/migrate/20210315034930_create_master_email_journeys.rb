class CreateMasterEmailJourneys < ActiveRecord::Migration[5.2]
  def change
    create_table :master_email_journeys do |t|
      t.integer :master_customer_id
      t.text :subject
      t.string :email
      t.string :preview
      t.integer :email_type
      t.datetime :created_date
      t.string :campaign_id

      t.timestamps
    end
    add_index :master_email_journeys, :master_customer_id
  end
end

class CreateAdsJourneys < ActiveRecord::Migration[5.2]
  def change
    create_table :ads_journeys do |t|
      t.integer :customer_id
      t.string :fb_ad_name
      t.string :fb_campaign_name
      t.string :fb_ad_group_name
      t.datetime :create_date

      t.timestamps
    end
  end
end

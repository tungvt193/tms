class CreateAppointments < ActiveRecord::Migration[5.2]
  def change
    create_table :appointments do |t|
      t.integer :kanban_ticket_id
      t.text :content
      t.datetime :appointment_date
      t.text :result
      t.integer :created_by_id

      t.timestamps
    end
  end
end

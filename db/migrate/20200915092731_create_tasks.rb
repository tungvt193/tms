class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.integer :deal_id
      t.string :title
      t.datetime :deadline
      t.text :note

      t.timestamps
    end
  end
end

class CreateWishLists < ActiveRecord::Migration[5.2]
  def change
    create_table :wish_lists do |t|
      t.boolean :favourite
      t.integer :user_id
      t.belongs_to :objectable, polymorphic: true
      t.timestamps
    end
    add_index :wish_lists, :objectable_id
    add_index :wish_lists, :objectable_type
  end
end

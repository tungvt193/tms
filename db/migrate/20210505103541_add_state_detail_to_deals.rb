class AddStateDetailToDeals < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :state_detail, :string
    add_column :deals, :financial_capability_review, :integer
    add_column :deals, :products_state, :integer
  end
end

class CreateProductLockHistoryLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :product_lock_history_logs do |t|
      t.references :product_lock_history, foreign_key: true
      t.datetime :enqueued_entered
      t.datetime :locking_entered
      t.datetime :deposit_confirmation_entered
      t.datetime :waiting_reti_acceptance_entered
      t.datetime :out_of_time_entered
      t.datetime :success_entered
      t.datetime :pending_entered
      t.datetime :canceled_entered
      t.datetime :failed_signed_entered
      t.datetime :deal_confirmation_entered

      t.datetime :enqueued_exited
      t.datetime :locking_exited
      t.datetime :deposit_confirmation_exited
      t.datetime :waiting_reti_acceptance_exited
      t.datetime :out_of_time_exited
      t.datetime :success_exited
      t.datetime :pending_exited
      t.datetime :canceled_exited
      t.datetime :failed_signed_exited
      t.datetime :deal_confirmation_exited

      t.timestamps
    end
  end
end

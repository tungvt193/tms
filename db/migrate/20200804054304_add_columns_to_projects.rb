class AddColumnsToProjects < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :high_level_number, :integer
    add_column :projects, :low_level_number, :integer
  end
end

class AddImportIdToDeals < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :import_id, :string
  end
end

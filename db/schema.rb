# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_07_11_080309) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string "email", default: ""
    t.string "encrypted_password", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "full_name"
    t.string "phone"
    t.boolean "phone_verify", default: false
    t.string "identity_card"
    t.boolean "active", default: true
  end

  create_table "ads_journeys", force: :cascade do |t|
    t.integer "customer_id"
    t.string "fb_ad_name"
    t.string "fb_campaign_name"
    t.string "fb_ad_group_name"
    t.datetime "create_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_link"
    t.string "content_link"
    t.boolean "is_import", default: false
  end

  create_table "appointments", force: :cascade do |t|
    t.integer "task_id"
    t.text "content"
    t.datetime "appointment_date"
    t.text "result"
    t.integer "created_by_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "deal_id"
    t.boolean "flag", default: false
    t.integer "state"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_appointments_on_deleted_at"
  end

  create_table "banners", force: :cascade do |t|
    t.integer "banner_type", default: 0
    t.string "title"
    t.string "sub_title"
    t.string "banner_desktop"
    t.string "banner_mobile"
    t.string "link"
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_display", default: true
    t.string "banner_app"
  end

  create_table "calls", force: :cascade do |t|
    t.integer "task_id"
    t.datetime "contact_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "deal_id"
    t.integer "created_by_id"
    t.text "result"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_calls_on_deleted_at"
  end

  create_table "campaign_locks", force: :cascade do |t|
    t.string "state"
    t.datetime "session_opening_time"
    t.integer "project_id"
    t.integer "enable_locked_for_hot_product", default: 1
    t.string "enable_locked_for_inventory", default: "Không giới hạn"
    t.integer "unc_confirmation_period_for_sale", default: 30
    t.integer "unc_confirmation_period_for_sale_admin", default: 10
    t.integer "confirm_deposit_by_sale_admin"
    t.integer "products", array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string "data_file_name", null: false
    t.string "data_content_type"
    t.integer "data_file_size"
    t.string "data_fingerprint"
    t.string "type", limit: 30
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["type"], name: "index_ckeditor_assets_on_type"
  end

  create_table "commissions", force: :cascade do |t|
    t.integer "deal_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "calculation_type"
    t.string "cross_selling_agent_code"
    t.integer "applicable_policy"
    t.integer "seller_type"
    t.float "reti_commission_rate"
    t.float "reti_actually_rate"
    t.float "reti_commission_rate_for_seller"
    t.float "seller_commission_rate_will_be_received"
    t.float "seller_commission_rate_from_reti"
    t.bigint "price_to_calculate_commission_for_seller"
    t.bigint "temporary_commission"
    t.bigint "actually_commission"
  end

  create_table "company_revenues", force: :cascade do |t|
    t.integer "objectable_id"
    t.string "objectable_type"
    t.float "percentage"
    t.date "start_date"
    t.date "end_date"
    t.integer "real_estate_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "constructors", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.json "logo"
  end

  create_table "coordinates", force: :cascade do |t|
    t.integer "layout_id"
    t.integer "product_id"
    t.string "product_code"
    t.string "positions"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "countries", force: :cascade do |t|
    t.text "name"
    t.text "code"
  end

  create_table "cs_tickets", force: :cascade do |t|
    t.integer "deal_id"
    t.integer "created_by_id"
    t.string "deal_state"
    t.text "support_level"
    t.integer "processing_speed"
    t.integer "sale_rating"
    t.integer "sale_consulting_quality"
    t.integer "deal_rating"
    t.integer "voucher_id"
    t.integer "cancel_reason"
    t.integer "demand_with_deal"
    t.text "extra_support_for_customer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "note"
    t.integer "customer_opinion"
  end

  create_table "custom_htmls", force: :cascade do |t|
    t.text "content"
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "customer_personas", force: :cascade do |t|
    t.integer "customer_id"
    t.integer "deal_id"
    t.string "second_phone_number"
    t.string "email"
    t.integer "gender"
    t.string "identity_card"
    t.string "domicile"
    t.text "nationality"
    t.string "nation"
    t.string "workplace"
    t.string "financial_capability"
    t.string "customer_position"
    t.text "property_ownership"
    t.string "settlements"
    t.integer "city_id"
    t.integer "district_id"
    t.integer "ward_id"
    t.string "street"
    t.integer "marital_status"
    t.string "people_in_family"
    t.string "hobbies", default: [], array: true
    t.text "positions_to_invest"
    t.integer "real_estate_type_to_invest", default: [], array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.date "dob"
  end

  create_table "customer_tickets", force: :cascade do |t|
    t.string "name"
    t.integer "gender"
    t.string "phone"
    t.string "email"
    t.integer "city_id"
    t.string "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "objectable_type"
    t.integer "objectable_id"
    t.integer "ticket_type", default: 0
    t.string "state"
    t.integer "source"
    t.string "message"
    t.index ["objectable_id"], name: "index_customer_tickets_on_objectable_id"
    t.index ["objectable_type"], name: "index_customer_tickets_on_objectable_type"
  end

  create_table "customers", force: :cascade do |t|
    t.string "name"
    t.string "phone_number"
    t.string "second_phone_number"
    t.integer "gender"
    t.string "email"
    t.string "identity_card"
    t.integer "city_id"
    t.text "address"
    t.date "dob"
    t.integer "data_source"
    t.integer "customer_characteristic", array: true
    t.string "job"
    t.string "property_ownership"
    t.string "nationality"
    t.string "financial_capability"
    t.text "detail"
    t.string "income"
    t.string "position"
    t.string "work_place"
    t.integer "marital_status"
    t.text "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "state"
    t.integer "created_by_id"
    t.string "country_code"
    t.string "facebook_uid"
    t.string "hubspot_contact_id"
    t.datetime "deleted_at"
    t.integer "same_customers", default: [], array: true
    t.index ["created_by_id"], name: "index_customers_on_created_by_id"
    t.index ["deleted_at"], name: "index_customers_on_deleted_at"
  end

  create_table "deal_journeys", force: :cascade do |t|
    t.integer "deal_id"
    t.datetime "create_date"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "deal_logs", force: :cascade do |t|
    t.integer "deal_id"
    t.datetime "pending_entered"
    t.datetime "interest_entered"
    t.datetime "meet_entered"
    t.datetime "lock_entered"
    t.datetime "confirm_entered"
    t.datetime "book_entered"
    t.datetime "deposit_entered"
    t.datetime "contract_signed_entered"
    t.datetime "completed_entered"
    t.datetime "canceled_entered"
    t.datetime "request_recall_entered"
    t.datetime "pending_reentered"
    t.datetime "interest_reentered"
    t.datetime "meet_reentered"
    t.datetime "lock_reentered"
    t.datetime "confirm_reentered"
    t.datetime "book_reentered"
    t.datetime "deposit_reentered"
    t.datetime "contract_signed_reentered"
    t.datetime "completed_reentered"
    t.datetime "canceled_reentered"
    t.datetime "request_recall_reentered"
    t.datetime "pending_exited"
    t.datetime "interest_exited"
    t.datetime "meet_exited"
    t.datetime "lock_exited"
    t.datetime "confirm_exited"
    t.datetime "book_exited"
    t.datetime "deposit_exited"
    t.datetime "contract_signed_exited"
    t.datetime "completed_exited"
    t.datetime "canceled_exited"
    t.datetime "request_recall_exited"
    t.integer "pending_time_in"
    t.integer "interest_time_in"
    t.integer "meet_time_in"
    t.integer "lock_time_in"
    t.integer "confirm_time_in"
    t.integer "book_time_in"
    t.integer "deposit_time_in"
    t.integer "contract_signed_time_in"
    t.integer "completed_time_in"
    t.integer "canceled_time_in"
    t.integer "request_recall_time_in"
    t.datetime "close_date"
    t.integer "days_to_close"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "refundable_deposit_entered"
    t.datetime "refundable_deposit_exited"
    t.datetime "refundable_deposit_reentered"
    t.integer "refundable_deposit_time_in"
    t.datetime "approaching_entered"
    t.datetime "approaching_reentered"
    t.datetime "approaching_exited"
    t.integer "approaching_time_in"
    t.datetime "uninterested_entered"
    t.datetime "uninterested_reentered"
    t.datetime "uninterested_exited"
    t.datetime "uninterested_time_in"
  end

  create_table "deal_sorts", force: :cascade do |t|
    t.integer "deal_id"
    t.integer "user_id"
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deal_id"], name: "index_deal_sorts_on_deal_id"
    t.index ["deleted_at"], name: "index_deal_sorts_on_deleted_at"
    t.index ["user_id"], name: "index_deal_sorts_on_user_id"
  end

  create_table "deals", force: :cascade do |t|
    t.string "name"
    t.integer "customer_id"
    t.integer "product_id"
    t.string "state"
    t.integer "assignee_id"
    t.bigint "total_price"
    t.integer "source"
    t.text "labels", default: [], array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "contact_status"
    t.text "financial_capability"
    t.text "demand_for_advances"
    t.integer "created_by_id"
    t.integer "updated_by_id"
    t.integer "project_id"
    t.text "trouble_problem"
    t.datetime "assigned_at"
    t.integer "position"
    t.integer "interaction_detail", array: true
    t.integer "trouble_problem_list", default: [], array: true
    t.integer "canceled_reason"
    t.datetime "state_changed_at"
    t.integer "purchase_purpose"
    t.integer "product_type"
    t.text "balcony_direction", array: true
    t.string "interested_product"
    t.string "suggestive_name"
    t.text "door_direction", default: [], array: true
    t.datetime "contract_signed_at"
    t.integer "lock_version", default: 0, null: false
    t.datetime "deleted_at"
    t.string "hubspot_contact_id"
    t.string "pre_state"
    t.datetime "cached_at"
    t.boolean "is_external_project"
    t.string "external_project"
    t.string "code"
    t.integer "same_deals", default: [], array: true
    t.boolean "from_website"
    t.datetime "create_deal_date"
    t.boolean "is_change_create_date"
    t.text "product_type_detail"
    t.string "state_detail"
    t.integer "financial_capability_review"
    t.integer "products_state"
    t.integer "lead_scoring"
    t.string "import_id"
    t.integer "uninterested_reason"
    t.integer "price_range"
    t.integer "acreage_range"
    t.integer "product_selection_criteria", default: [], array: true
    t.string "source_detail"
    t.integer "cs_state", default: 1
    t.boolean "miss_project", default: false
    t.text "telesale_note"
    t.text "telesale_interest_detail"
    t.string "callio_campaign_id"
    t.boolean "from_app", default: false
    t.string "telesale_group"
    t.integer "product_lock_history_id"
    t.string "cross_selling_product_code"
    t.index ["deleted_at"], name: "index_deals_on_deleted_at"
    t.index ["same_deals"], name: "index_deals_on_same_deals", using: :gin
  end

  create_table "deposits", force: :cascade do |t|
    t.string "name"
    t.integer "customer_id"
    t.integer "product_id"
    t.integer "user_id"
    t.string "product_code"
    t.float "area"
    t.integer "direction"
    t.bigint "price"
    t.bigint "total_price"
    t.bigint "deposit"
    t.bigint "total_price_after_discount"
    t.bigint "commission"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "state"
  end

  create_table "developments", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.json "logo"
  end

  create_table "device_tokens", force: :cascade do |t|
    t.integer "user_id"
    t.text "device_token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "divisions", force: :cascade do |t|
    t.string "name"
    t.integer "parent_id"
    t.integer "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "label"
    t.integer "level"
  end

  create_table "documents", force: :cascade do |t|
    t.string "name"
    t.string "file"
    t.text "description"
    t.string "preview_image"
    t.bigint "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id"], name: "index_documents_on_project_id"
  end

  create_table "email_journeys", force: :cascade do |t|
    t.text "subject"
    t.string "email"
    t.string "preview"
    t.integer "email_type"
    t.datetime "created_date"
    t.integer "customer_id"
    t.string "campaign_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_import", default: false
  end

  create_table "facebook_apis", force: :cascade do |t|
    t.integer "user_id"
    t.string "uid"
    t.string "user_access_token"
    t.string "long_lived_user_access_token"
    t.string "long_lived_access_token"
    t.json "pages"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "appsecret_proof"
  end

  create_table "feedbacks", force: :cascade do |t|
    t.string "title"
    t.text "content"
    t.integer "deal_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "created_by_id"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_feedbacks_on_deleted_at"
  end

  create_table "floorplan_images", force: :cascade do |t|
    t.integer "real_estate_type"
    t.json "images"
    t.bigint "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id"], name: "index_floorplan_images_on_project_id"
  end

  create_table "form_journeys", force: :cascade do |t|
    t.integer "customer_id"
    t.string "recent_conversion"
    t.string "last_name"
    t.string "first_name"
    t.string "phone_number"
    t.string "email"
    t.datetime "recent_conversion_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_import", default: false
  end

  create_table "groups", force: :cascade do |t|
    t.string "name", null: false
    t.integer "city_ids", default: [], array: true
    t.integer "director_id"
    t.integer "user_ids", default: [], array: true
    t.integer "supervisor_id"
  end

  create_table "histories", force: :cascade do |t|
    t.integer "deal_id"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_histories_on_deleted_at"
  end

  create_table "history_interactions", force: :cascade do |t|
    t.integer "deal_id"
    t.integer "created_by_id"
    t.integer "interaction_type"
    t.datetime "interaction_date"
    t.text "interaction_content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "instruction_documents", force: :cascade do |t|
    t.string "title"
    t.string "file"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "interactive_layouts", force: :cascade do |t|
    t.string "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "project_id"
    t.integer "division_id"
    t.text "positions"
    t.boolean "show_division"
  end

  create_table "investor_posts", force: :cascade do |t|
    t.integer "project_id"
    t.string "title"
    t.text "post_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "investors", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.json "logo"
  end

  create_table "key_values", force: :cascade do |t|
    t.string "icon"
    t.string "title"
    t.string "description"
    t.bigint "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id"], name: "index_key_values_on_project_id"
  end

  create_table "kpis", force: :cascade do |t|
    t.integer "team_id"
    t.string "year"
    t.float "jan"
    t.float "feb"
    t.float "mar"
    t.float "apr"
    t.float "may"
    t.float "jun"
    t.float "jul"
    t.float "aug"
    t.float "sep"
    t.float "oct"
    t.float "nov"
    t.float "dec"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "layouts", force: :cascade do |t|
    t.string "name"
    t.string "image"
    t.integer "project_id"
    t.integer "subdivision_id"
    t.integer "block_id"
    t.integer "floor_ids", array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "level"
    t.integer "number_of_product"
  end

  create_table "legal_documents", force: :cascade do |t|
    t.integer "project_id"
    t.integer "doc_type"
    t.string "doc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "locking_history_logs", force: :cascade do |t|
    t.integer "project_id"
    t.integer "product_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "master_ads_journeys", force: :cascade do |t|
    t.integer "master_customer_id"
    t.string "fb_ad_name"
    t.string "fb_campaign_name"
    t.string "fb_ad_group_name"
    t.datetime "create_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["master_customer_id"], name: "index_master_ads_journeys_on_master_customer_id"
  end

  create_table "master_customer_personas", force: :cascade do |t|
    t.integer "master_customer_id"
    t.integer "gender"
    t.string "identity_card"
    t.string "domicile"
    t.integer "yob"
    t.string "nationality"
    t.string "nation"
    t.string "workplace"
    t.string "financial_capability"
    t.string "customer_position"
    t.text "property_ownership"
    t.string "settlements"
    t.integer "city_id"
    t.integer "district_id"
    t.integer "ward_id"
    t.string "street"
    t.integer "marital_status"
    t.string "people_in_family"
    t.string "hobbies", default: [], array: true
    t.text "positions_to_invest"
    t.integer "real_estate_type_to_invest", default: [], array: true
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["master_customer_id"], name: "index_master_customer_personas_on_master_customer_id"
  end

  create_table "master_customers", force: :cascade do |t|
    t.integer "customer_ids", array: true
    t.string "name"
    t.string "phone_number"
    t.string "second_phone_number"
    t.integer "gender"
    t.string "email"
    t.string "identity_card"
    t.integer "city_id"
    t.text "address"
    t.date "dob"
    t.integer "data_source"
    t.integer "customer_characteristic"
    t.string "job"
    t.string "property_ownership"
    t.string "nationality"
    t.string "financial_capability"
    t.text "detail"
    t.string "income"
    t.string "position"
    t.string "work_place"
    t.integer "marital_status"
    t.text "note"
    t.string "state"
    t.integer "created_by_id"
    t.string "country_code"
    t.string "facebook_uid"
    t.string "hubspot_contact_id"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_ids"], name: "index_master_customers_on_customer_ids", using: :gin
    t.index ["deleted_at"], name: "index_master_customers_on_deleted_at"
  end

  create_table "master_email_journeys", force: :cascade do |t|
    t.integer "master_customer_id"
    t.text "subject"
    t.string "email"
    t.string "preview"
    t.integer "email_type"
    t.datetime "created_date"
    t.string "campaign_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["master_customer_id"], name: "index_master_email_journeys_on_master_customer_id"
  end

  create_table "master_form_journeys", force: :cascade do |t|
    t.integer "master_customer_id"
    t.string "recent_conversion"
    t.string "last_name"
    t.string "first_name"
    t.string "phone_number"
    t.string "email"
    t.datetime "recent_conversion_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["master_customer_id"], name: "index_master_form_journeys_on_master_customer_id"
  end

  create_table "master_sms_journeys", force: :cascade do |t|
    t.integer "master_customer_id"
    t.string "sms_subject"
    t.string "phone_received"
    t.text "sms_detail"
    t.datetime "sms_created_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["master_customer_id"], name: "index_master_sms_journeys_on_master_customer_id"
  end

  create_table "master_visit_journeys", force: :cascade do |t|
    t.integer "master_customer_id"
    t.string "title"
    t.string "visited_link"
    t.datetime "visited_date"
    t.integer "visit_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["master_customer_id"], name: "index_master_visit_journeys_on_master_customer_id"
  end

  create_table "messenger_journeys", force: :cascade do |t|
    t.string "conversation_id"
    t.text "message"
    t.string "from_id"
    t.string "from_name"
    t.string "to_id"
    t.string "to_name"
    t.datetime "created_time"
    t.integer "customer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "milestones", force: :cascade do |t|
    t.integer "project_id"
    t.string "event"
    t.date "event_time"
    t.boolean "is_default", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notes", force: :cascade do |t|
    t.integer "created_by_id"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "objectable_type"
    t.integer "objectable_id"
    t.boolean "is_third_holding"
    t.datetime "deleted_at"
    t.boolean "is_customer_note"
    t.index ["deleted_at"], name: "index_notes_on_deleted_at"
    t.index ["objectable_id"], name: "index_notes_on_objectable_id"
    t.index ["objectable_type"], name: "index_notes_on_objectable_type"
  end

  create_table "notifications", force: :cascade do |t|
    t.integer "user_id"
    t.integer "tag"
    t.text "content"
    t.datetime "read_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "related_url"
    t.datetime "deleted_at"
    t.integer "deal_id"
    t.string "title"
    t.integer "notification_type"
    t.boolean "send_now"
    t.index ["deleted_at"], name: "index_notifications_on_deleted_at"
  end

  create_table "online_feedbacks", force: :cascade do |t|
    t.integer "action_type"
    t.text "content"
    t.datetime "action_date"
    t.integer "deal_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "created_by_id"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_online_feedbacks_on_deleted_at"
  end

  create_table "operators", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.json "logo"
  end

  create_table "otps", force: :cascade do |t|
    t.string "phone_number"
    t.string "otp"
    t.datetime "expiry"
    t.integer "status", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "otp_type"
  end

  create_table "payment_schedules", force: :cascade do |t|
    t.integer "project_id"
    t.integer "label_schedule"
    t.string "name"
    t.decimal "pay", precision: 5, scale: 2
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "permissions", force: :cascade do |t|
    t.integer "role_id"
    t.boolean "can_update", default: false
    t.boolean "can_create", default: false
    t.boolean "can_edit", default: false
    t.boolean "can_destroy", default: false
    t.boolean "can_import", default: false
    t.boolean "change_state", default: false
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "can_assign", default: false
    t.boolean "can_update_price", default: false
    t.integer "data_area", default: 0
    t.string "state_area", default: [], array: true
    t.string "commission_fields_validation", default: [], array: true
    t.string "commission_fields_can_view", default: [], array: true
    t.string "commission_fields_can_edit", default: [], array: true
  end

  create_table "popup_steps", force: :cascade do |t|
    t.integer "popup_id"
    t.integer "name"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "popups", force: :cascade do |t|
    t.boolean "active"
    t.integer "group"
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "product_lock_histories", force: :cascade do |t|
    t.integer "user_id"
    t.integer "project_id"
    t.integer "product_id"
    t.string "state"
    t.datetime "deadline_for_sale"
    t.datetime "deadline_for_sale_admin"
    t.text "unc_image"
    t.text "failure_reason"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "product_lock_history_logs", force: :cascade do |t|
    t.bigint "product_lock_history_id"
    t.datetime "enqueued_entered"
    t.datetime "locking_entered"
    t.datetime "deposit_confirmation_entered"
    t.datetime "waiting_reti_acceptance_entered"
    t.datetime "out_of_time_entered"
    t.datetime "success_entered"
    t.datetime "pending_entered"
    t.datetime "canceled_entered"
    t.datetime "failed_signed_entered"
    t.datetime "deal_confirmation_entered"
    t.datetime "enqueued_exited"
    t.datetime "locking_exited"
    t.datetime "deposit_confirmation_exited"
    t.datetime "waiting_reti_acceptance_exited"
    t.datetime "out_of_time_exited"
    t.datetime "success_exited"
    t.datetime "pending_exited"
    t.datetime "canceled_exited"
    t.datetime "failed_signed_exited"
    t.datetime "deal_confirmation_exited"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_lock_history_id"], name: "index_product_lock_history_logs_on_product_lock_history_id"
  end

  create_table "products", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.integer "project_id"
    t.integer "subdivision_id"
    t.integer "block_id"
    t.integer "floor_id"
    t.integer "project_layout_id"
    t.integer "level"
    t.integer "real_estate_type"
    t.string "product_type"
    t.integer "certificate"
    t.integer "use_term"
    t.integer "furniture"
    t.integer "furniture_quality"
    t.string "statics"
    t.float "density"
    t.bigint "deposit"
    t.integer "amount_of_floors"
    t.integer "direction"
    t.float "carpet_area"
    t.float "built_up_area"
    t.float "plot_area"
    t.float "floor_area"
    t.float "setback_front"
    t.float "setback_behind"
    t.text "handover_standards"
    t.text "detail"
    t.integer "living_room"
    t.string "bedroom"
    t.integer "bath_room"
    t.integer "dining_room"
    t.integer "multipurpose_room"
    t.integer "mini_bar"
    t.integer "drying_yard"
    t.integer "kitchen"
    t.integer "balcony"
    t.integer "business_space"
    t.integer "currency"
    t.bigint "price"
    t.bigint "sum_price"
    t.integer "created_by_id"
    t.integer "updated_by_id"
    t.integer "lock_version"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.string "state"
    t.json "images"
    t.integer "source", default: 0
    t.integer "label"
    t.datetime "deleted_at"
    t.string "tag"
    t.index ["block_id"], name: "index_products_on_block_id"
    t.index ["code", "project_id"], name: "index_products_on_code_and_project_id", unique: true
    t.index ["code"], name: "index_products_on_code"
    t.index ["deleted_at"], name: "index_products_on_deleted_at"
    t.index ["floor_id"], name: "index_products_on_floor_id"
    t.index ["level"], name: "index_products_on_level"
    t.index ["name"], name: "index_products_on_name"
    t.index ["project_id"], name: "index_products_on_project_id"
    t.index ["subdivision_id"], name: "index_products_on_subdivision_id"
  end

  create_table "project_new_updates", force: :cascade do |t|
    t.integer "project_id"
    t.string "title"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "created_by_id"
  end

  create_table "projects", force: :cascade do |t|
    t.string "name"
    t.integer "locking_time"
    t.decimal "construction_density", precision: 4, scale: 1
    t.decimal "total_area", precision: 10, scale: 2
    t.integer "real_estate_type", array: true
    t.integer "investors", array: true
    t.integer "constructors", array: true
    t.integer "developments", array: true
    t.integer "operators", array: true
    t.integer "features", array: true
    t.text "description"
    t.json "images"
    t.integer "internal_utilities", array: true
    t.integer "ownership_period"
    t.boolean "foreigner"
    t.string "sale_policy"
    t.integer "banks", array: true
    t.decimal "loan_percentage_support", precision: 4, scale: 1
    t.integer "loan_support_period"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "high_level_number"
    t.integer "low_level_number"
    t.string "slug"
    t.decimal "price_from", precision: 10, scale: 2
    t.decimal "price_to", precision: 10, scale: 2
    t.decimal "area_from", precision: 10, scale: 2
    t.decimal "area_to", precision: 10, scale: 2
    t.text "custom_description"
    t.text "custom_sale_policy"
    t.text "custom_utilities"
    t.text "link_video"
    t.text "position_description"
    t.text "position_link_embed"
    t.integer "external_utilities", default: [], array: true
    t.json "internal_utility_images"
    t.json "external_utility_images"
    t.json "design_style_images"
    t.decimal "discount_support", precision: 4, scale: 1
    t.text "meta_title"
    t.text "meta_description"
    t.boolean "most_popular"
    t.text "image_folder_url"
    t.text "video_folder_url"
    t.integer "labels", default: [], array: true
    t.text "description_for_seller"
    t.text "layout_url"
    t.datetime "deleted_at"
    t.integer "queue_lock", default: 5
    t.integer "unc_confirmation_period_for_sale", default: 30
    t.integer "unc_confirmation_period_for_sale_admin", default: 10
    t.integer "confirm_deposit_by_sale_admin"
    t.index ["banks"], name: "index_projects_on_banks", using: :gin
    t.index ["constructors"], name: "index_projects_on_constructors", using: :gin
    t.index ["deleted_at"], name: "index_projects_on_deleted_at"
    t.index ["developments"], name: "index_projects_on_developments", using: :gin
    t.index ["features"], name: "index_projects_on_features", using: :gin
    t.index ["internal_utilities"], name: "index_projects_on_internal_utilities", using: :gin
    t.index ["investors"], name: "index_projects_on_investors", using: :gin
    t.index ["labels"], name: "index_projects_on_labels", using: :gin
    t.index ["operators"], name: "index_projects_on_operators", using: :gin
    t.index ["real_estate_type"], name: "index_projects_on_real_estate_type", using: :gin
  end

  create_table "reason_locks", force: :cascade do |t|
    t.bigint "account_id"
    t.string "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_reason_locks_on_account_id"
  end

  create_table "referral_histories", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "referred_id"
    t.datetime "referred_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_referral_histories_on_user_id"
  end

  create_table "region_datas", force: :cascade do |t|
    t.string "region_type"
    t.string "slug"
    t.integer "parent_id"
    t.string "name"
    t.string "name_with_type"
    t.string "path"
    t.string "path_with_type"
    t.string "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "regions", force: :cascade do |t|
    t.integer "city_id"
    t.integer "district_id"
    t.integer "ward_id"
    t.string "objectable_type"
    t.bigint "objectable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "street"
    t.index ["objectable_id"], name: "index_regions_on_objectable_id"
    t.index ["objectable_type", "objectable_id"], name: "index_regions_on_objectable_type_and_objectable_id"
    t.index ["objectable_type"], name: "index_regions_on_objectable_type"
  end

  create_table "regulations", force: :cascade do |t|
    t.string "name"
    t.string "state"
    t.integer "project_id"
    t.integer "real_estate_type", array: true
    t.datetime "start_date"
    t.datetime "end_date"
    t.float "manager_comm"
    t.float "admin_comm"
    t.float "leader_comm"
    t.boolean "leader_duplicate_comm"
    t.float "member_comm_sale"
    t.float "member_comm_company"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "manager_duplicate_comm", default: false
    t.float "free_leader_comm"
    t.boolean "free_leader_duplicate_comm", default: false
  end

  create_table "reminders", force: :cascade do |t|
    t.integer "user_id"
    t.integer "tag"
    t.text "content"
    t.datetime "read_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "related_url"
    t.datetime "deleted_at"
    t.integer "deal_id"
    t.index ["deleted_at"], name: "index_reminders_on_deleted_at"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sale_documents", force: :cascade do |t|
    t.integer "project_id"
    t.string "title"
    t.string "file"
    t.integer "sale_document_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.integer "position"
  end

  create_table "sms_journeys", force: :cascade do |t|
    t.string "sms_subject"
    t.string "phone_received"
    t.text "sms_detail"
    t.datetime "sms_created_date"
    t.integer "customer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_import", default: false
  end

  create_table "tasks", force: :cascade do |t|
    t.integer "deal_id"
    t.string "title"
    t.datetime "deadline"
    t.text "result"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "code"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_tasks_on_deleted_at"
  end

  create_table "teams", force: :cascade do |t|
    t.integer "user_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "transfer_histories", force: :cascade do |t|
    t.bigint "deal_id"
    t.bigint "transferred_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["deal_id"], name: "index_transfer_histories_on_deal_id"
    t.index ["transferred_id"], name: "index_transfer_histories_on_transferred_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: ""
    t.string "encrypted_password", default: "", null: false
    t.string "full_name"
    t.integer "account_type"
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "role_id"
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.text "tokens"
    t.boolean "allow_password_change", default: false
    t.boolean "deactivated", default: false
    t.boolean "is_superadmin", default: false
    t.string "ancestry"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "access_token"
    t.string "refresh_token"
    t.integer "expires_at"
    t.string "avatar"
    t.string "endpoint"
    t.string "p256dh_key"
    t.string "auth_key"
    t.boolean "subscribe", default: false
    t.string "code"
    t.integer "project_selected_ids", default: [], array: true
    t.integer "gender"
    t.string "phone_number"
    t.integer "city_id"
    t.integer "main_job"
    t.string "country_code"
    t.boolean "phone_verify", default: false
    t.string "referral_code"
    t.integer "created_by_id"
    t.index ["ancestry"], name: "index_users_on_ancestry"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email"
    t.index ["referral_code"], name: "index_users_on_referral_code", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
  end

  create_table "versions", force: :cascade do |t|
    t.string "version"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "visit_journeys", force: :cascade do |t|
    t.string "title"
    t.string "visited_link"
    t.datetime "visited_date"
    t.integer "customer_id"
    t.integer "visit_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_import", default: false
  end

  create_table "vouchers", force: :cascade do |t|
    t.text "content"
    t.integer "applicable_condition"
    t.integer "project_id", array: true
    t.integer "voucher_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "vouchers_customers", force: :cascade do |t|
    t.integer "customer_id"
    t.integer "voucher_id"
    t.integer "deal_id"
    t.integer "cs_ticket_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "vouchers_projects", force: :cascade do |t|
    t.integer "project_id"
    t.integer "voucher_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "wish_lists", force: :cascade do |t|
    t.boolean "favourite"
    t.integer "user_id"
    t.string "objectable_type"
    t.bigint "objectable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["objectable_id"], name: "index_wish_lists_on_objectable_id"
    t.index ["objectable_type", "objectable_id"], name: "index_wish_lists_on_objectable_type_and_objectable_id"
    t.index ["objectable_type"], name: "index_wish_lists_on_objectable_type"
  end

  add_foreign_key "documents", "projects"
  add_foreign_key "floorplan_images", "projects"
  add_foreign_key "key_values", "projects"
  add_foreign_key "product_lock_history_logs", "product_lock_histories"
  add_foreign_key "reason_locks", "accounts", on_delete: :cascade
  add_foreign_key "referral_histories", "users"
  add_foreign_key "transfer_histories", "deals"
end

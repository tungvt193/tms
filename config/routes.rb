Rails.application.routes.draw do

  # GraphQL API
  unless Rails.env.production?
    mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "/api/v1/graphql"
  end
  post "/api/v1/graphql", to: "api/v1/graphql#execute"

  namespace :api do
    namespace :v1 do
      mount_devise_token_auth_for 'User', at: 'auth', controllers: {
        sessions: 'api/v1/sessions',
        registrations: 'api/v1/registrations',
        passwords: 'api/v1/passwords'
      }, skip: [:omniauth_callbacks]
      put 'users/update_avatar' => 'users#update_avatar'
      post 'users/register_phone_number' => 'users#register_phone_number'
      post 'users/verify_phone_number' => 'users#verify_phone_number'
      post 'versions/check_version' => 'versions#check_version'
      post 'auth/refresh_token' => "users#refresh_token"
      post 'users/find_referrer' => 'users#find_referrer'
      patch 'product_lock_histories/:id/upload_unc' => 'product_lock_histories#upload_unc'
      post 'sync/users' => 'sync#users'
      post 'sync/projects' => 'sync#projects'
      post 'sync/products' => 'sync#products'
      post 'sync/deals' => 'sync#deals'
      patch 'users/change_password' => 'users#change_password'
    end
  end

  # CKEditor
  mount Ckeditor::Engine => '/ckeditor'

  # Sidekiq Web
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

  # ActionCable
  mount ActionCable.server => '/cable'

  # Authenticate user
  devise_for :users, path: 'user',
             controllers: {
               sessions: 'users/sessions',
               passwords: 'users/passwords',
               omniauth_callbacks: "users/omniauth_callbacks"
             }

  # authenticated :user do
  #   root to: 'admin/dashboard#index'
  # end
  unauthenticated :user do
    devise_scope :user do
      get '/', to: 'users/sessions#new'
    end
  end

  post '/sync-lead-callio', to: 'sync_lead_callio#sync'

  # Admin Namespace
  namespace :admin, path: '' do
    root to: 'dashboard#index'
    get '/get_access_token', to: 'dashboard#get_access_token'
    resources :dashboard do
      collection do
        post :update_project_selected_ids
        get :get_projects
        get :get_api_dashboard_assignee
        get :get_api_dashboard_deals
        get :get_api_dashboard_commission
        get :unc_product_lock_history
      end
    end
    get '/deals/filter/:type', to: 'deals#filter'
    resources :deals do
      resources :appointments
      resources :calls
      resources :feedbacks
      resources :notes
      resources :online_feedbacks
      resources :history_interactions
      collection do
        get :autocomplete
        get :product_search
        get :product_type_search
        get :project_search
        get :customer_search
        get :assignee_search
        post :reorder
        post :update_commission
        get :get_comm_info
        get :get_all_deal_by_query
        post :update_source_detail
        get :complete_deals
        post :export_deals
      end
      member do
        get :kanban_item
        patch :update_state
        patch :update_assignee
        patch :update_source
        patch :update_create_date
        patch :update_lead_scoring
        post :check_deal_contract_signed_product
        get :new_transfer
        post :create_transfer
      end
      resources :histories, only: :index
    end
    resources :cs_tickets do
      collection do
        get :autocomplete
        get :download_csv
      end
    end
    resources :investors do
      member do
        post :destroy_attachment
      end
      collection do
        get :autocomplete
        get :search
      end
    end
    resources :constructors do
      member do
        post :destroy_attachment
      end
      collection do
        get :autocomplete
        get :search
      end
    end
    resources :operators do
      member do
        post :destroy_attachment
      end
      collection do
        get :autocomplete
        get :search
      end
    end
    resources :developments do
      member do
        post :destroy_attachment
      end
      collection do
        get :autocomplete
        get :search
      end
    end
    resources :projects do
      resources :products do
        collection do
          get :autocomplete
          get :export_template
          post :import
          get :search
          get :state_filter
          get :subdivision_id_filter
          get :block_id_filter
          get :floor_id_filter
          post :get_divisions
        end
        member do
          post :destroy_attachment
          post :update_wish_list
        end
      end
      resources :layouts do
        collection do
          post :get_divisions
        end
      end
      member do
        post :destroy_attachment
        get :show_products
        get :information_for_sellers
        get :lock_rules
        patch :update_lock_rules
        patch :update_sale_policy
        get :product_autocomplete
      end
      collection do
        get :autocomplete
        get :search
        post :import
      end
      resources :custom_details, only: [:index, :create]
      resources :interactive_layouts do
        collection do
          post :get_divisions
          post :find_divisions
        end
      end
      resources :company_revenues, only: [:index, :create, :update, :destroy] do
        collection do
          get :new_exclusive_revenue
          get :new_exception_revenue
        end
      end
      resources :sale_documents, only: [:index, :destroy, :show, :create] do
        member do
          get :download
        end
        collection do
          post :update_media_url
          get :new_investor_post
          post :create_investor_post
          patch '/update_investor_post/:id', to: 'sale_documents#update_investor_post'
          delete '/destroy_investor_post/:id', to: 'sale_documents#destroy_investor_post'
          post :update_sale_kit
          patch '/update_sale_kit', to: 'sale_documents#update_sale_kit'
          post :update_sale_policy
          patch '/update_sale_policy', to: 'sale_documents#update_sale_policy'
          post :create_sale_kit
          post :create_sale_policy
          post :reorder_position
        end
      end
    end
    resources :customers do
      collection do
        get :autocomplete
        post :import
        get :check_same_phone
      end
      member do
        get :vouchers
      end
      resources :customer_journeys do
        collection do
          patch '/', to: 'customer_journeys#update'
          get '/', to: 'customer_journeys#edit'
        end
      end
    end
    resources :regions do
      collection do
        post :get_regions, to: 'regions#get_regions'
      end
    end
    resources :users do
      collection do
        get :export_users
        get :autocomplete
        get :account_autocomplete
        get :search
        get :profile
        patch :update_profile
        post :subscribe
        post :destroy_attachment
      end
    end
    resources :roles do
      member do
        get :new_clone
      end
    end
    resources :deposits do
      collection do
        get :stream_product
        post :finish_countdown
      end
    end
    resources :customer_tickets do
      collection do
        get :autocomplete
      end
    end
    resources :banners do
      member do
        post :destroy_attachment
      end
      collection do
        post :sort
      end
    end
    resources :popups, param: :group
    resources :app_news, only: [:index] do
      collection do
        post :import
      end
    end
    resources :app_banners do
      member do
        post :destroy_attachment
      end
      collection do
        patch '/', to: 'app_banners#update'
      end
    end
    resources :instruction_documents, only: [:index, :create, :destroy]
    resources :custom_htmls, param: :position
    resources :notifications do
      collection do
        get :load_more
        post :read
        get :read_all
      end
    end
    resources :reminders do
      collection do
        get :load_more
        post :read
        get :read_all
      end
    end
    resources :accounts do
      collection do
        get :autocomplete
        get :search
      end
    end
    resources :groups do
      member do
        get :members, to: 'members#index'
        post :add_root_ancestry, to: 'members#add_root_ancestry'
        post :add_child_ancestry, to: 'members#add_child_ancestry'
        post :add_sibling_ancestry, to: 'members#add_sibling_ancestry'
        post :remove_ancestry, to: 'members#remove_ancestry'
      end
      collection do
        get :autocomplete
      end
    end
    # resources :regulations do
    #   collection do
    #     get :autocomplete
    #     post :get_types
    #   end
    # end
    resources :same_deals do
      collection do
        get :autocomplete
      end
    end
    resources :same_customers do
      collection do
        get :autocomplete
      end
    end
    get "/facebook/callback", to: "facebooks#callback"
    resource :facebook
    resources :custom_keywords do
      collection do
        get :projects
      end
    end
    resources :project_new_updates
    resources :kpis do
      collection do
        post :get_teams
      end
    end
    resources :deal_imports
    resources :calendar do
      collection do
        get :get_events
      end
    end
    resources :vouchers do
      collection do
        post :send_voucher
      end
    end
    resources :versions, only: [:index, :update]
    resources :product_lock_histories, only: [:index, :edit, :update]
    resources :campaign_locks do
      collection do
        get :export_template
        put :preview_products
      end
    end
  end
end

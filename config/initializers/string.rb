class String
  def titleize_name
    separator = ' '
    self.strip.split(separator).map do |part|
      part.first.capitalize + part[1..part.size]
    end.join(separator)
  end
end

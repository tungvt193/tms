require_relative 'boot'

require 'rails/all'
require 'fog/aws'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)
Dotenv::Railtie.load

module Tms
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2
    config.i18n.default_locale = :vi
    config.active_job.queue_adapter = :sidekiq
    config.eager_load_paths += %W(#{config.root}/lib)
    config.autoload_paths += %w(#{config.root}/app/models/ckeditor)
    config.time_zone = 'Hanoi'
    config.active_record.default_timezone = :local
    config.active_job.queue_name_prefix = "tms_#{Rails.env}"
    config.action_controller.perform_caching = true
    config.cache_store = :file_store, "#{Rails.root}/tmp/cache/"

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
  end
end

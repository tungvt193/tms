require File.expand_path("./environment", __dir__)

set :repo_url, 'git@bitbucket.org:reti-vn/tms.git'
set :application, 'reti-tms'
if fetch(:stage) == :production
  set :user, 'ver2-tms-production'
  set :branch, :master
elsif fetch(:stage) == :staging
  set :user, 'ver2-tms-staging'
  set :branch, :staging
end
if fetch(:stage) == :production
  set :puma_threads, [16, 64]
  set :puma_workers, 4
else
  set :puma_threads, [4, 16]
  set :puma_workers, 0
end
# Don't change these unless you know what you're doing
set :pty, false
set :use_sudo, false
set :deploy_via, :remote_cache
set :deploy_to, "/home/#{fetch(:user)}/#{fetch(:application)}"
set :puma_bind, "unix://#{shared_path}/tmp/sockets/#{fetch(:application)}-puma.sock"
set :puma_state, "#{shared_path}/tmp/pids/puma.state"
set :puma_pid, "#{shared_path}/tmp/pids/puma.pid"
set :puma_error_log, "#{release_path}/log/puma.error.log"
set :puma_access_log, "#{release_path}/log/puma.access.log"
set :ssh_options, {forward_agent: true, user: fetch(:user), keys: %w(~/.ssh/id_rsa.pub)}
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :puma_init_active_record, true # Change to false when not using ActiveRecord
set :sidekiq_role, :app
set :sidekiq_concurrency, 25
set :sidekiq_pid, "#{shared_path}/tmp/pids/sidekiq.pid"
set :sidekiq_log, "#{release_path}/log/sidekiq.log"
if fetch(:stage) == :production
  set :sidekiq_env, 'production'
  set :sidekiq_queue, %w(tms_production_default tms_production_deal_sorting tms_production_deal_synchronization tms_production_customer_birthday tms_production_lock_product tms_production_appointment tms_production_notification tms_production_campaign_lock)
else
  set :sidekiq_env, 'staging'
  set :sidekiq_queue, %w(tms_staging_default tms_staging_deal_sorting tms_staging_deal_synchronization tms_staging_customer_birthday tms_staging_lock_product tms_staging_appointment tms_staging_notification tms_staging_campaign_lock)
end
set :rvm_ruby_version, '2.6.8'
# set :whenever_command, 'source ~/.rvm/environments/ruby-2.2.4 && cd #{release_path} ; RAILS_ENV=production bundle exec whenever'
set :whenever_roles, -> {[:web, :app]}
set :whenever_identifier, -> {"#{fetch(:application)}_#{fetch(:stage)}"}

# set :linked_files, fetch(:linked_files, []).push('config/master.key')
set :linked_files, fetch(:linked_files, []).push('config/database.yml', '.env')

## Defaults:
# set :scm,           :git
# set :branch,        :master
# set :format,        :pretty
# set :log_level,     :debug
# set :keep_releases, 5

## Linked Files & Directories (Default None):
# set :linked_files, %w{config/database.yml}
# set :linked_dirs,  %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}
set :linked_dirs, %w{tmp/cache public/uploads}

namespace :puma do
  desc 'Create Directories for Puma Pids and Socket'
  task :make_dirs do
    on roles(:app) do
      execute "mkdir #{shared_path}/tmp/sockets -p"
      execute "mkdir #{shared_path}/tmp/pids -p"
    end
  end

  before :start, :make_dirs
end

# set the locations that we will look for changed assets to determine whether to precompile
# set :assets_dependencies, %w(app/assets lib/assets vendor/assets Gemfile.lock config/routes.rb)

# clear the previous precompile task
# Rake::Task['deploy:assets:precompile'].clear_actions
# class PrecompileRequired < StandardError; end

namespace :deploy do

  desc 'Make sure local git is in sync with remote.'
  task :check_revision do
    on roles(:app) do
      unless `git rev-parse HEAD` == `git rev-parse origin/#{fetch(:branch)}`
        puts 'WARNING: HEAD is not the same as origin/#{fetch(:branch)}'
        puts 'Run `git push` to sync changes.'
        exit
      end
    end
  end

  desc 'Initial Deploy'
  task :initial do
    on roles(:app) do
      before 'deploy:restart', 'puma:start'
      invoke 'deploy'
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      Rake::Task['puma:restart'].reenable
      invoke 'puma:restart'
    end
  end

  desc 'reload the database with seed data'
  task :seed do
    puts '\n=== Seeding Database ===\n'
    on primary :db do
      within current_path do
        with rails_env: fetch(:stage) do
          execute :rake, 'db:seed'
        end
      end
    end
  end

  before :starting, :check_revision
  after :finishing, :compile_assets
  after :finishing, :cleanup
  after :finishing, :seed
  after :finishing, :restart
end

# ps aux | grep puma    # Get puma pid
# kill -s SIGUSR2 pid   # Restart puma
# kill -s SIGTERM pid   # Stop puma

set :capenv, ->(env) {
  env.add /^reti_/
  env.add /^reti__/ do |key|
    key.gsub /^reti__/, '' # replace keyname like MYAPP_DATABASE_URL => DATABASE_URL
  end
  env.add 'SECRET_KEY_BASE', Rails.application.credentials.dig(:secret_key_base)
  env.add 'S3_ACCESS_KEY', Rails.application.credentials.dig(fetch(:stage).to_sym, :aws, :s3_access_key)
  env.add 'S3_SECRET_KEY', Rails.application.credentials.dig(fetch(:stage).to_sym, :aws, :s3_secret_key)
  env.add 'S3_REGION', Rails.application.credentials.dig(fetch(:stage).to_sym, :aws, :s3_region)
  env.add 'S3_BUCKET', Rails.application.credentials.dig(fetch(:stage).to_sym, :aws, :s3_bucket)
  env.add 'ELASTICSEARCH_URL', Rails.application.credentials.dig(fetch(:stage).to_sym, :elasticsearch_url)
  env.add 'GOOGLE_CLIENT_ID', Rails.application.credentials.dig(fetch(:stage).to_sym, :google, :calendar_client_id)
  env.add 'GOOGLE_SECRET', Rails.application.credentials.dig(fetch(:stage).to_sym, :google, :calendar_secret)
  env.add 'VAPID_PUBLIC_KEY', Rails.application.credentials.dig(fetch(:stage).to_sym, :vapid, :vapid_public_key)
  env.add 'VAPID_PRIVATE_KEY', Rails.application.credentials.dig(fetch(:stage).to_sym, :vapid, :vapid_private_key)
  env.add 'TMS_URL', Rails.application.credentials.dig(fetch(:stage).to_sym, :tms_url)
  env.add 'GOOGLE_SHEET_FILE_KEY', Rails.application.credentials.dig(fetch(:stage).to_sym, :google, :sheet_file_key)
  env.add 'HUBSPOT_API_KEY', Rails.application.credentials.dig(fetch(:stage).to_sym, :hubspot, :api_key)
  env.add 'HUBSPOT_CLIENT_ID', Rails.application.credentials.dig(fetch(:stage).to_sym, :hubspot, :client_id)
  env.add 'HUBSPOT_CLIENT_SECRET', Rails.application.credentials.dig(fetch(:stage).to_sym, :hubspot, :client_secret)
  env.add 'FACEBOOK_APP_ID', Rails.application.credentials.dig(fetch(:stage).to_sym, :facebook, :app_id)
  env.add 'FACEBOOK_APP_SECRET', Rails.application.credentials.dig(fetch(:stage).to_sym, :facebook, :app_secret)
  env.add 'WP_API_POST', Rails.application.credentials.dig(fetch(:stage).to_sym, :wordpress, :api_post)
  env.add 'WEBSITE_URL', Rails.application.credentials.dig(fetch(:stage).to_sym, :website_url)
  env.add 'SMS_USERNAME', Rails.application.credentials.dig(fetch(:stage).to_sym, :vivas, :sms_username)
  env.add 'SMS_PASSWORD', Rails.application.credentials.dig(fetch(:stage).to_sym, :vivas, :sms_password)
  env.add 'SMS_BRANDNAME', Rails.application.credentials.dig(fetch(:stage).to_sym, :vivas, :sms_brandname)
  env.add 'SMS_API_URL', Rails.application.credentials.dig(fetch(:stage).to_sym, :vivas, :sms_api_url)
  env.add 'FIREBASE_SERVER_KEY', Rails.application.credentials.dig(fetch(:stage).to_sym, :firebase, :server_key)
  env.add 'CALLIO_TOKEN', Rails.application.credentials.dig(fetch(:stage).to_sym, :callio_token)
  env.add 'FIREBASE_DYNAMIC_LINK_URL', Rails.application.credentials.dig(fetch(:stage).to_sym, :firebase, :dynamic_link_url)
  env.add 'FIREBASE_APN', Rails.application.credentials.dig(fetch(:stage).to_sym, :firebase, :apn)
  env.add 'FIREBASE_ISI', Rails.application.credentials.dig(fetch(:stage).to_sym, :firebase, :isi)
  env.add 'FIREBASE_IBI', Rails.application.credentials.dig(fetch(:stage).to_sym, :firebase, :ibi)
  env.add 'ESMS_ZALO_API_KEY', Rails.application.credentials.dig(fetch(:stage).to_sym, :esms, :zalo_api_key)
  env.add 'ESMS_ZALO_SECRET_KEY', Rails.application.credentials.dig(fetch(:stage).to_sym, :esms, :zalo_secret_key)
  env.add 'ESMS_ZALO_OAID', Rails.application.credentials.dig(fetch(:stage).to_sym, :esms, :zalo_oaid)
  env.add 'ESMS_ZALO_API_URL', Rails.application.credentials.dig(fetch(:stage).to_sym, :esms, :zalo_api_url)
}

# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
require_relative "environment"
set :environment, Rails.env
puts "Crontab is run on #{@environment}"

# every 3.minutes do
#   rake "hubspot_tms_deal_synchronization:execute"
# end
#
# every :monday, at: '0:00 am' do
#   rake "hubspot_tms_customer_synchronization:email_execute"
# end
#
# every :day, at: '0:00 am' do
#   rake "hubspot_tms_customer_synchronization:form_execute"
# end
#
# every :day, at: '0:00 am' do
#   rake "hubspot_tms_customer_synchronization:ads_execute"
# end
#
# every :day, at: '0:00 am' do
#   rake "hubspot_tms_customer_synchronization:sms_execute"
# end

every 4.hours do
  rake "notification:send_after_four_hours"
end

every :day, at: '9:00 am' do
  rake "customer:remind_birthday"
end

# TODO: delete exported files in folder cs_ticket(S3)

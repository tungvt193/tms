require 'securerandom'

module ReferralCode
  SYMBOL = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.freeze

  def self.generate
    (0...6).map { random_symbol }.join
  end

  def self.random_symbol
    SYMBOL[rand(SYMBOL.length)]
  end
end

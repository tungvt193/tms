module Reti::Product
  LEVELS = {
    0 => 'Cao tầng',
    1 => 'Thấp tầng'
  }

  DIVISION_TYPES = {
    0 => 'Phân khu',
    1 => 'Toà',
    2 => 'Đường/Dãy',
    3 => 'Tầng'
  }

  REAL_ESTATE_TYPES = {
    0 => {
      0 => 'Apartel',
      1 => 'Căn hộ',
      2 => 'Condotel',
      3 => 'Duplex',
      4 => 'Penthouse',
      5 => 'Sky villa',
      6 => 'Hometel',
      7 => 'Officetel',
      8 => 'Sàn thương mại'
    },
    1 => {
      9 => 'Biệt thự',
      10 => 'Bungalow',
      11 => 'Đất nền',
      12 => 'Liền kề',
      13 => 'Shophouse',
      14 => 'Shoptel',
      15 => 'Đất TMDV',
      16 => 'Mini Hotel'
    }
  }

  TYPES = {
    0 => {
      0 => '1 PN',
      1 => '2 PN',
      2 => '3 PN',
      3 => '4 PN',
      4 => 'Studio',
      5 => 'Không chia phòng'
    },
    1 => {
      6 => 'Đơn lập',
      7 => 'Song lập',
      8 => 'Tứ lập'
    }
  }

  DIRECTIONS = {
    0 => 'Đông',
    1 => 'Tây',
    2 => 'Nam',
    3 => 'Bắc',
    4 => 'Đông Nam',
    5 => 'Tây Nam',
    6 => 'Đông Bắc',
    7 => 'Tây Bắc'
  }

  CERTIFICATES = {
    0 => 'Sổ hồng',
    1 => 'Sổ đỏ'
  }

  UNITS = {
    0 => 'Tỷ',
    1 => 'Triệu',
    2 => 'Nghìn'
  }

  CURRENCIES = {
    0 => 'VNĐ',
    1 => 'USD',
    2 => 'EUR'
  }

  USE_TERMS = {
    0 => '50 năm',
    1 => '70 năm',
    2 => 'Lâu dài'
  }

  FURNITURES = {
    0 => 'Thô',
    1 => 'Đính tường',
    2 => 'Toàn bộ'
  }

  FURN_QUALITIES = {
    0 => 'Cao cấp',
    1 => 'Trung cấp',
    2 => 'Bình dân'
  }

  SOURCE = {
    0 => 'Công ty',
    1 => 'Khác'
  }

  LABEL = {
    0 => 'Độc quyền',
    1 => 'CĐT'
  }

  TAG = {
    0 => 'Hàng HOT',
    1 => 'Hàng tồn'
  }
end

namespace :deal do
  desc "TODO"
  task sort: :environment do
    # Deal seed position
    arr_pos = {}
    Deal.aasm.states.each do |sta|
      arr_pos["#{sta.name.to_s}"] = Deal.where(state: sta.name.to_s).minimum(:position).to_i
    end
    Deal.where(position: nil).order(created_at: :asc).find_each do |deal|
      arr_pos["#{deal.state}"] -= 1
      deal.update_column(:position, arr_pos["#{deal.state}"])
    end
  end

  task update_notifications: :environment do
    Notification.where(deal_id: nil).each do |notification|
      uri = URI::parse(notification.related_url)
      next unless uri.query
      params = CGI::parse(uri.query)
      deal_id = params["force_id"]&.first.to_i
      if deal_id != 0
        notification.update_column(:deal_id, deal_id)
      end
    end

    Reminder.where(deal_id: nil).each do |reminder|
      uri = URI::parse(reminder.related_url)
      next unless uri.query
      params = CGI::parse(uri.query)
      deal_id = params["force_id"]&.first.to_i
      if deal_id != 0
        reminder.update_column(:deal_id, deal_id)
      end
    end
  end

  task update_sort: :environment do
    Deal.all.order(:state_changed_at).each do |deal|
      User.all.each do |user|
        if Pundit.policy(user, deal).show?
          unless deal.get_deal_sort(user)
            position_min = Deal.joins(:deal_sorts).where(state: deal.state, 'deal_sorts.user_id': user.id).minimum('deal_sorts.position').to_i
            deal.deal_sorts.create(user_id: user.id, position: position_min - 1)
          end
        end
      end
    end
  end

  task update_same_deals: :environment do
    Deal.find_each do |deal|
      next if deal.customer&.phone_number.blank?
      same_deals = if deal.project_id
        Deal.where.not(id: deal.id).joins(:customer).where('customers.phone_number': deal&.customer.phone_number).where('deals.project_id': [deal.project_id, nil])
      else
        Deal.where.not(id: deal.id).joins(:customer).where('customers.phone_number': deal&.customer.phone_number)
      end
      deal.update_column(:same_deals, same_deals.pluck('id'))
    end
  end

  task update_create_date: :environment do
    Deal.where(is_change_create_date: nil).each do |deal|
      if deal.create_deal_date.present?
        deal.update_columns(is_change_create_date: true)
      else
        deal.create_deal_date = deal.created_at
        deal.update_columns(create_deal_date: deal.created_at)
      end
    end
  end

  desc "Refactor Deal source"
  task refactor_source: :environment do
    updated_deals = Deal.where.not(source: nil).order(:id)
    DealMailer.notification_fyi("Danh sách nguồn giao dịch (cũ) trên TMS (#{Rails.env})", updated_deals.pluck(:id, :source).to_json).deliver_later(wait: 3.second)
    updated_deals.each do |deal|
      old_source = deal.source
      source = case old_source
      # SOURCE_QCNVKD           = 7
      # SOURCE_KHAC             = 9
      # SOURCE_CHIEN_DICH_KHAC  = 13
      # SOURCE_DIRECT_BOOKING   = 19
      # SOURCE_OTHER_CAMPAIGN   = 21
      when 7, 9, 13, 19, 21 then nil
      # Seeding (4) -> Seeding (32)
      when 4 then 32
      # Đăng tin (5) -> Đăng tin (50)
      when 5 then 50
      # Telesale (6) -> Telesale (35)
      when 6 then 35
      # Người quen của NVKD giới thiệu (10) -> Giới thiệu (37)
      # Khách cũ giới thiệu (12) -> Giới thiệu (37)
      when 10, 12 then 37
      # Facebook Messenger (0) -> Quảng cáo Facebook (38)
      # Facebook Comment (24) -> Quảng cáo Facebook (38)
      # Paid social (14) -> Quảng cáo Facebook (38)
      when 0, 24, 14 then 38
      # Social Media (15) -> Seeding (39)
      when 15 then 39
      # Email Marketing (16) -> Email Marketing (40)
      when 16 then 40
      # Paid Search (17) -> Quảng cáo Google Search (41)
      when 17 then 41
      # Organic search (18) -> Tìm kiếm miễn phí (42)
      when 18 then 42
      # Direct traffic (20) -> Truy cập trực tiếp (43)
      when 20 then 43
      # Display (23) -> GDN (44)
      when 23 then 44
      # Telemarketing (3) -> Telesale (45)
      when 3 then 45
      # SMS marketing (25) -> SMS (46)
      when 25 then 46
      # Hotline (1) -> Hotline (47)
      when 1 then 47
      # Trực dự án (8) -> Trực dự án (48)
      when 8 then 48
      # Người quen (Nguồn Công ty) giới thiệu (11) -> Giới thiệu (49)
      # Referral (22) -> Giới thiệu (49)
      when 11, 22 then 49
      else old_source
      end
      deal.update_columns source: source
      puts "Updated: #{deal.id} (from #{old_source} to #{source})"
    end
    puts "Total: #{updated_deals.count}"
  end
  
  task update_new_state: :environment do
    current_time = DateTime.now
    Deal.where(state: %w(approaching confirm book completed)).each do |deal|
      case deal.state
      when 'approaching'
        deal.update_columns(state: 'pending')
        log = deal.deal_log
        log.pending_entered = log.pending_entered ? log.pending_entered : current_time
        log.pending_reentered = current_time
        log.pending_exited = nil
        log.save
      when 'confirm'
        deal.update_columns(state: 'lock')
        log = deal.deal_log
        log.lock_entered = log.lock_entered ? log.lock_entered : log.confirm_entered
        log.lock_reentered = log.lock_reentered ? log.lock_reentered : log.confirm_reentered
        log.lock_exited = nil
        log.save
      when 'book'
        deal.update_columns(state: 'lock')
        log = deal.deal_log
        log.lock_entered = log.lock_entered ? log.lock_entered : log.book_entered
        log.lock_reentered = log.lock_reentered ? log.lock_reentered : log.book_reentered
        log.lock_exited = nil
        log.save
      when 'completed'
        deal.update_columns(state: 'contract_signed')
        log = deal.deal_log
        log.contract_signed_entered = log.contract_signed_entered ? log.contract_signed_entered : log.completed_entered
        log.contract_signed_reentered = log.contract_signed_reentered ? log.contract_signed_reentered : log.completed_reentered
        log.contract_signed_exited = nil
        log.save
      end
    end
  end

  task update_deal_contact_status: :environment do
    Deal.where(contact_status: [1, 9]).each{ |deal| deal.update_columns(contact_status: nil, uninterested_reason: 1, state: 'uninterested') }
    Deal.where(contact_status: [3, 4]).each{ |deal| deal.update_columns(contact_status: 2) }
    Deal.where(contact_status: [5, 6, 7]).each{ |deal| deal.update_columns(contact_status: 3) }
    Deal.where(contact_status: [8]).each{ |deal| deal.update_columns(contact_status: 1) }
    Deal.where(state: 'canceled', canceled_reason: 7).each{ |deal| deal.update_columns(uninterested_reason: 3, state: 'uninterested') }
    Deal.where(state: 'canceled', canceled_reason: [0, 1, 2, 3, 4, 5, 6]).each{ |deal| deal.update_columns(canceled_reason: 11) }
    Deal.where(purchase_purpose: 4).each{ |deal| deal.update_columns(purchase_purpose: 3) }
  end

  task update_labels: :environment do 
    Deal.all.each do |deal|
      labels = []
      case deal.state
      when 'canceled'
        labels << Constant::CANCELED_REASON[deal.canceled_reason] if deal.canceled_reason.present?
      when 'uninterested'
        labels << Constant::UNINTERESTED_REASON[deal.uninterested_reason] if deal.uninterested_reason.present?
      else
        labels << Constant::CONTACT_STATUSES[deal.contact_status] if deal.contact_status.present?
      end
      labels << 'Chưa có số điện thoại' unless deal.customer&.phone_number.present?
      deal.update_columns(labels: labels, cached_at: DateTime.now)
    end
  end

  desc "Fix Deal Error Name"
  task update_error_names: :environment do 
    Deal.where("name like '%đ%'").each do |deal|
      deal.name = deal.is_external_project ? deal.customer.name.titleize_name : "#{deal&.customer&.name&.titleize_name} - #{deal&.project&.name&.titleize_name}"
      deal.save validate: false
    end
  end

  desc "Update Commission"
  task update_commission: :environment do
    ActiveRecord::Base.transaction do
      TransferHistory.includes(:deal).each do |transfer|
        transfer.deal.commission.update_columns(actually_commission: 0, temporary_commission: 0) if transfer.deal.commission.present?
      end
    end    
  end
end

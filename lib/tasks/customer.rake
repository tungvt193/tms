namespace :customer do
  desc "TODO"
  task format_phone: :environment do
    Customer.all.each do |customer|
      if customer.phone_number.present?
        if customer.phone_number.length == 10
          phone_number = "84#{customer.phone_number[1..9]}"
          customer.update_columns(phone_number: phone_number, country_code: "VN")
        else
          if customer.phone_number.include?("+65")
            phone_number = customer.phone_number.sub("+", "")
            customer.update_columns(phone_number: phone_number, country_code: "SG")
          end
        end
      end
    end
  end

  task update_source: :environment do
    Customer.all.each do |customer|
      deal_first = customer.deals.order(:created_at).first
      customer.update_columns(data_source: deal_first.source) if deal_first&.source
    end
  end

  task remind_birthday: :environment do
    RemindBirthdayJob.perform_later
  end
end

namespace :nationality do
  desc "Import countries from JSON file"
  task update: :environment do  
    CustomerPersona.where.not(nationality: nil).update_all(nationality: Country::VIETNAM_NAME)
  end
end

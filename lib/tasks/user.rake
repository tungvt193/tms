namespace :user do
  desc "update code"

  task update_code: :environment do
    User.all.each do |user|
      user.update_columns(code: user.email.split("@")[0]&.upcase)
    end
  end

  task update_team: :environment do
    users = User.where.not(id: Team.all.pluck(:user_id))
    users.all.each do |user|
      Team.create(user_id: user.id, name: user.full_name)
    end
  end

  task update_referral_code: :environment do
    users = User.where(referral_code: nil)
    users.each do |user|
      code = User.generate_referral_code
      user.update_column(:referral_code, code)
    rescue => e
      puts e
      next
    end
  end
end

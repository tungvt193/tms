namespace :update_customer_hubspot_contact_id do
  desc "Update customer's hubspot_contact_id from deal"

  task execute: :environment do
    Deal.includes(:customer).where.not(hubspot_contact_id: nil).each do |deal|
      next unless customer = deal.customer
      customer.update_column(:hubspot_contact_id, deal.hubspot_contact_id)
    end
  end
end

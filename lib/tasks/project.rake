namespace :project do
  desc "TODO"
  task update_slug: :environment do
    Project.where(slug: [nil, '']).each do |project|
      slug = pre_slug = project.name.mb_chars.normalize(:kd).gsub(/\p{Mn}/, '').downcase.to_s.parameterize
      i = 1
      while Project.exists?(slug: slug)
        slug = "#{pre_slug}-#{i}"
        i += 1
      end
      project.update_attributes(slug: slug)
    end
  end

  task update_labels: :environment do
    Project.all.each do |project|
      if project.products.for_sale.size > 0 && !project.labels.include?(0)
        project.update(labels: project.labels += [0])
      end
    end
  end

  task sync_to_google_sheet: :environment do
    SyncProjectToGoogleSheetJob.perform_later(nil, 'sync_all_projects')
  end
end

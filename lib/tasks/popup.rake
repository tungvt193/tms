namespace :popup do
  desc "Init popup"
  task init: :environment do
    # Subcribes
    Popup.create(group: :subscribe, position: :home, active: false)
    Popup.create(group: :subscribe, position: :project_list, active: false)
    Popup.create(group: :subscribe, position: :project_detail, active: false)
    Popup.create(group: :subscribe, position: :product_detail, active: false)
    # Guide tours
    Popup.create(group: :guide_tour, position: :home, active: false)
    Popup.create(group: :guide_tour, position: :project_detail, active: false)
  end
end

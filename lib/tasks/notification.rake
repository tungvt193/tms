namespace :notification do
  desc "Update old data"
  task update_old_data: :environment do
    ApplicationRecord.transaction do 
      notifications = Notification.where(send_now: nil)
      notifications.each do |notification|
        notification.update_column(:send_now, true)  
      end
    end
  end

  desc "Update notification pending"
  task send_after_four_hours: :environment do 
    puts "Start update"
    ApplicationRecord.transaction do 
      Notification.where(send_now: false).each do |notification|
        notification.update(send_now: true, created_at: Time.now)
      end
    end
    puts "Update Done"
  end
end

namespace :regulation do
  desc "Check expired regulation"

  task check_expired: :environment do
    puts "RUNNING rake regulation:check_expired"
    expired_list = Regulation.expired_list
    expired_list.each do |reg|
      reg.to_expired
      if reg.save
        puts "Regulation check expired done: #{reg.id}"
      else
        puts "Regulation check expired fail: #{reg.id}"
      end
    end
  end
end

namespace :sale_document do
  desc "TODO"

  task update_slug: :environment do
    SaleDocument.where(slug: [nil, '']).each do |sale_document|
      s = sale_document.title
      slug = pre_slug = s.mb_chars.normalize(:kd).gsub(/\p{Mn}/, '').downcase.to_s.parameterize
      i = 1
      while SaleDocument.exists?(slug: slug)
        slug = "#{pre_slug}-#{i}"
        i += 1
      end
      sale_document.update_attributes(slug: slug)
    end
  end

  task update_position: :environment do
    project_ids = SaleDocument.pluck(:project_id).uniq
    project_ids.each do |project_id|
      project = Project.find(project_id)
      sale_kits = project.sale_documents.sale_kit.order(created_at: :desc)
      sk_index = 0
      sale_kits.each do |sale_kit|
        sale_kit.update_column(:position, sk_index)
        sk_index += 1
      end
      sale_policies = project.sale_documents.sale_policy.order(created_at: :desc)
      sp_index = 0
      sale_policies.each do |sale_policy|
        sale_policy.update_column(:position, sp_index)
        sp_index += 1
      end
    end
  end
end

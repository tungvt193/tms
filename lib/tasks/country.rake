namespace :country do
  desc "Import countries from JSON file"
  task import: :environment do
    countries = JSON.parse(File.read("data/countries.json"))
    Country.create(countries)
  end
end

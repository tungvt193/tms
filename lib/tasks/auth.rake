namespace :auth do
  desc "Logout all user"
  task logout: :environment do
    puts "Start logout"
    ApplicationRecord.transaction do 
      User.all.each do |user|
        if user.tokens.present?
          user.tokens = {}
          user.save(validate: false)
        end      
      end
    end
    puts "End logout"
  end
end

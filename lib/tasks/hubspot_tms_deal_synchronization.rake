namespace :hubspot_tms_deal_synchronization do
  desc "Import deal from google sheet"

  task execute: :environment do
    require "google_drive"
    session = GoogleDrive::Session.from_config("client_secret.json")
    wss = session.spreadsheet_by_key(ENV['GOOGLE_SHEET_FILE_KEY']).worksheets
    ws = wss.detect{|w| w.title == "Hubspot Contacts - For import"}
    return unless ws
    ws.rows.drop(1).each.with_index(2) do |row, index|
      assignee_email = row[10]&.strip
      assignee = User.find_by(email: assignee_email)
      project = Project.find_by_id(row[6])
      next if row[0] == "TRUE" || row[1]&.strip.blank? || assignee.blank? || project.blank?
      hubspot_contact_id = row[1]&.strip
      created_at = row[2]
      customer_email = row[3]&.strip
      customer_phone = row[4]&.strip
      customer_name = row[5]&.strip
      project_id = row[6]
      deal_name = row[7]&.strip
      source_name = row[8]&.strip
      # group_sale = row[9]
      assigned_at = row[11]
      # Create customer
      customer = Customer.new(email: customer_email, phone_number: customer_phone, name: customer_name, hubspot_contact_id: hubspot_contact_id, data_source: Constant::DEAL_SOURCES.key(source_name))
      customer.save(validate: false)
      # Create deal
      deal = Deal.new(
        name: deal_name,
        project_id: project_id,
        source: Constant::DEAL_SOURCES.key(source_name),
        assignee_id: assignee&.id,
        customer_id: customer&.id,
        contact_status: 0, # chưa liên hệ
        create_deal_date: created_at,
        hubspot_contact_id: hubspot_contact_id,
        assigned_at: assigned_at
      )
      deal.save(validate: false)
      ws[index, 1] = "TRUE"
    end
    Deal.reindex
    ws.save
  end
end

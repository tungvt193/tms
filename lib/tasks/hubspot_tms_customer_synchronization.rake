namespace :hubspot_tms_customer_synchronization do
  desc "Import customer email journeys from google sheet"

  task email_execute: :environment do
    require "google_drive"
    session = GoogleDrive::Session.from_config("client_secret.json")
    wss = session.spreadsheet_by_key(ENV['GOOGLE_SHEET_FILE_KEY']).worksheets
    ws = wss.detect{|w| w.title.strip == "Email for import"}
    return unless ws
    header = ws.rows.first.map{|c| c&.strip }
    cols = {
      campaign_id: header.find_index('id_chien_dich'),
      email: header.find_index('email_address'),
      subject: header.find_index('email_subject'),
      email_type: header.find_index('email_type'),
      created_date: header.find_index('email_createdate'),
      preview: header.find_index('email_preview'),
      hs_contact_id: header.find_index('id_hubspot_contact')
    }
    types = {
      'SENT': :sent,
      'DELIVERED': :delivered,
      'OPEN': :open,
      'CLICK': :click
    }
    return if cols.values.include?(nil)
    ws.rows.drop(1).each do |row|
      hs_contact_id = row[cols[:hs_contact_id]]&.strip
      customers = Customer.where(hubspot_contact_id: hs_contact_id)
      next unless customers.present?
      email_type = types[row[cols[:email_type]]&.strip&.to_sym]
      next unless [:sent, :delivered, :open, :click].include?(email_type)
      subject = row[cols[:subject]]&.strip
      created_date = row[cols[:created_date]]&.strip&.to_datetime
      preview = row[cols[:preview]]&.strip
      email = row[cols[:email]]&.strip
      campaign_id = row[cols[:campaign_id]]&.strip
      next if [hs_contact_id, email_type, created_date, preview, email, campaign_id].any?(&:blank?)
      customers.each do |customer|
        customer.update_column(:email, email) if customer.email.blank?
        next if customer.email_journeys.find_by(campaign_id: campaign_id, email: email, email_type: email_type, created_date: created_date)
        journey = customer.email_journeys.build(
          campaign_id: campaign_id,
          email: email,
          subject: subject,
          email_type: email_type,
          created_date: created_date,
          preview: preview,
          is_import: true
        )
        journey.save if journey.valid?
      end
    end
  end

  task form_execute: :environment do
    require "google_drive"
    session = GoogleDrive::Session.from_config("client_secret.json")
    wss = session.spreadsheet_by_key(ENV['GOOGLE_SHEET_FILE_KEY']).worksheets
    ws = wss.detect{|w| w.title.strip == "Form for import"}
    return unless ws
    header = ws.rows.first.map{|c| c&.strip }
    cols = {
      id_hubspot_contact: header.find_index('id_hubspot_contact'),
      recent_conversion_date: header.find_index('recent_conversion_date'),
      recent_conversion: header.find_index('recent_conversion'),
      email: header.find_index('email'),
      first_name: header.find_index('first_name'),
      last_name: header.find_index('last_name'),
      phone_number: header.find_index('phone_number')
    }
    return if cols.values.include?(nil)
    ws.rows.drop(1).each do |row|
      hs_contact_id = row[cols[:id_hubspot_contact]]&.strip
      customers = Customer.where(hubspot_contact_id: hs_contact_id)
      next unless customers.present?
      recent_conversion = row[cols[:recent_conversion]]&.strip
      recent_conversion_date = Time.zone.parse(row[cols[:recent_conversion_date]]&.strip)&.to_datetime
      phone_number = row[cols[:phone_number]]&.strip
      email = row[cols[:email]]&.strip
      first_name = row[cols[:first_name]]&.strip
      last_name = row[cols[:last_name]]&.strip
      customers.each do |customer|
        next if recent_conversion_date.blank? || customer.form_journeys.exists?(recent_conversion_date: recent_conversion_date)
        journey = customer.form_journeys.build(
          recent_conversion: recent_conversion,
          email: email,
          phone_number: phone_number,
          first_name: first_name,
          last_name: last_name,
          recent_conversion_date: recent_conversion_date,
          is_import: true
        )
        journey.save if journey.valid?
      end
    end
  end

  task ads_execute: :environment do
    require "google_drive"
    session = GoogleDrive::Session.from_config("client_secret.json")
    wss = session.spreadsheet_by_key(ENV['GOOGLE_SHEET_FILE_KEY']).worksheets
    ws = wss.detect{|w| w.title.strip == "Ads for import"}
    return unless ws
    header = ws.rows.first.map{|c| c&.strip }
    cols = {
      id_hubspot_contact: header.find_index('id_hubspot_contact'),
      create_date: header.find_index('create_date'),
      fb_campaign_name: header.find_index('fb_campaign_name'),
      fb_ad_group_name: header.find_index('fb_ad_group_name'),
      fb_ad_name: header.find_index('fb_ad_name')
    }
    return if cols.values.include?(nil)
    ws.rows.drop(1).each do |row|
      hs_contact_id = row[cols[:id_hubspot_contact]]&.strip
      customers = Customer.where(hubspot_contact_id: hs_contact_id)
      next unless customers.present?
      fb_campaign_name = row[cols[:fb_campaign_name]]&.strip
      create_date = Time.zone.parse(row[cols[:create_date]]&.strip)&.to_datetime
      fb_ad_group_name = row[cols[:fb_ad_group_name]]&.strip
      fb_ad_name = row[cols[:fb_ad_name]]&.strip
      customers.each do |customer|
        next if [fb_campaign_name, fb_ad_group_name, create_date, fb_ad_name].any?(&:blank?) || customer.ads_journeys.exists?(create_date: create_date)
        journey = customer.ads_journeys.build(
          fb_campaign_name: fb_campaign_name,
          fb_ad_group_name: fb_ad_group_name,
          fb_ad_name: fb_ad_name,
          create_date: create_date,
          is_import: true
        )
        journey.save if journey.valid?
      end
    end
  end

  task sms_execute: :environment do
    require "google_drive"
    session = GoogleDrive::Session.from_config("client_secret.json")
    wss = session.spreadsheet_by_key(ENV['GOOGLE_SHEET_FILE_KEY']).worksheets
    ws = wss.detect{|w| w.title.strip == "SMS for import"}
    return unless ws
    header = ws.rows.first.map{|c| c&.strip }
    cols = {
      sms_created_date: header.find_index('sms_created_date'),
      sms_detail: header.find_index('sms_detail'),
      phone_received: header.find_index('phone_received'),
      sms_subject: header.find_index('sms_subject')
    }
    return if cols.values.include?(nil)
    ws.rows.drop(1).each do |row|
      phone_received = row[cols[:phone_received]]&.strip
      customers = Customer.where(phone_number: phone_received).or(Customer.where(second_phone_number: phone_received))
      next unless customers.present?
      sms_subject = row[cols[:sms_subject]]&.strip
      sms_created_date = Time.zone.parse(row[cols[:sms_created_date]]&.strip)&.to_datetime
      sms_detail = row[cols[:sms_detail]]&.strip
      customers.each do |customer|
        next if [sms_subject, sms_created_date, sms_detail].any?(&:blank?) || customer.sms_journeys.exists?(phone_received: phone_received, sms_subject: sms_subject, sms_created_date: sms_created_date)
        journey = customer.sms_journeys.build(
          sms_subject: sms_subject,
          sms_detail: sms_detail,
          phone_received: phone_received,
          sms_created_date: sms_created_date,
          is_import: true
        )
        journey.save if journey.valid?
      end
    end
  end
end

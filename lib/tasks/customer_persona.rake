namespace :customer_persona do
  desc "Customer persona normalization"
  task normalize: :environment do
    User.current = User.first
    Deal.where.not(customer_id: nil).each do |deal|
      next if persona = deal.customer_persona && ENV['force'] != 'true'
      persona ||= deal.build_customer_persona
      customer = deal.customer
      persona.attributes = {
        customer_id: deal.customer_id,
        second_phone_number: deal.second_phone_number,
        email: deal.email,
        gender: deal.gender,
        identity_card: deal.identity_card,
        domicile: deal.domicile,
        dob: deal.dob,
        nationality: deal.nationality,
        nation: deal.nation,
        workplace: deal.workplace,
        financial_capability: deal.financial_capability,
        customer_position: deal.customer_position,
        property_ownership: deal.property_ownership,
        settlements: deal.settlements,
        city_id: deal.city_id,
        district_id: deal.district_id,
        ward_id: deal.ward_id,
        street: deal.street,
        marital_status: deal.marital_status,
        people_in_family: deal.people_in_family,
        hobbies: [],
        positions_to_invest: deal.positions_to_invest,
        real_estate_type_to_invest: deal.real_estate_type_to_invest
      }
      persona.save if persona.valid?
    end
    ENV['force'] = nil
  end

  task clean_deals: :environment do
    Deal.where(id: CustomerPersona.pluck(:deal_id).uniq).update_all(
      second_phone_number: nil,
      email: nil, gender: nil,
      identity_card: nil,
      domicile: nil,
      dob: nil,
      nationality: nil,
      nation: nil,
      workplace: nil,
      financial_capability: nil,
      customer_position: nil,
      property_ownership: nil,
      settlements: nil,
      city_id: nil,
      district_id: nil,
      ward_id: nil,
      street: nil,
      marital_status: nil,
      people_in_family: nil,
      hobby: [],
      positions_to_invest: nil,
      real_estate_type_to_invest: []
    )
  end
end

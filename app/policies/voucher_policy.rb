class VoucherPolicy < ApplicationPolicy
  attr_reader :user, :voucher

  def initialize(user, voucher)
    @user = user
    @voucher = voucher
  end

  def index?
    @user.check_permission_index 'Voucher'
  end

  def update?
    @user.check_permission @voucher.class.to_s, 'can_update'
  end

  def edit?
    @user.check_permission @voucher.class.to_s, 'can_edit' || update?
  end

  def create?
    @user.check_permission @voucher.class.to_s, 'can_create'
  end

  def show?
    @user.check_permission(@voucher.class.to_s, 'can_edit') || create? || update?
  end

  def destroy?
    @user.check_permission @voucher.class.to_s, 'can_destroy'
  end
end
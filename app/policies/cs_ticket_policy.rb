class CsTicketPolicy < ApplicationPolicy
  attr_reader :user, :ticket

  def initialize(user, ticket)
    @user = user
    @ticket = ticket
  end

  def index?
    @user.check_permission_index 'CsTicket'
  end

  def update?
    @user.check_permission @ticket.class.to_s, 'can_update'
  end

  def edit?
    @user.check_permission @ticket.class.to_s, 'can_edit' || update?
  end

  def create?
    @user.check_permission @ticket.class.to_s, 'can_create'
  end

  def show?
    @user.check_permission(@ticket.class.to_s, 'can_edit') || create? || update?
  end

  def destroy?
    @user.check_permission @ticket.class.to_s, 'can_destroy'
  end
end
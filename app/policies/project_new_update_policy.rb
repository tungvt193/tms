class ProjectNewUpdatePolicy < ApplicationPolicy
  attr_reader :user, :news

  def initialize(user, news)
    @user = user
    @news = news
  end

  def index?
    return false unless @user.internal?
    @user.check_permission_index 'ProjectNewUpdate'
  end

  def update?
    return false unless @user.internal?
    @user.check_permission @news.class.to_s, 'can_update'
  end

  def edit?
    return false unless @user.internal?
    @user.check_permission @news.class.to_s, 'can_edit' || update?
  end

  def create?
    return false unless @user.internal?
    @user.check_permission @news.class.to_s, 'can_create'
  end

  def destroy?
    return false unless @user.internal?
    @user.check_permission @news.class.to_s, 'can_destroy'
  end
end

class DealPolicy < ApplicationPolicy
  attr_reader :user, :deal

  def initialize(user, deal)
    @user = user
    @deal = deal
  end

  def index?
    @user.check_permission_index('Deal') && @user.internal?
  end

  def update?
    return false unless Scope.new(@user, Deal.all).resolve.include?(@deal)
    @user.super_admin? || @user.check_permission(@deal.class.to_s, 'can_update')
  end

  def edit?
    return false unless Scope.new(@user, Deal.all).resolve.include?(@deal)
    @user.super_admin? || @user.check_permission(@deal.class.to_s, 'can_edit') || update?
  end

  def create?
    @user.super_admin? || @user.check_permission(@deal.class.to_s, 'can_create')
  end

  # def import?
  #   return false unless (@user.internal? && Scope.new(@user, Deal.all).resolve.include?(@deal))
  #   @user.super_admin? || @user.check_permission @deal.class.to_s, 'can_import'
  # end

  def show?
    return false unless Scope.new(@user, Deal.all).resolve.include?(@deal)
    @user.super_admin? || @user.check_permission(@deal.class.to_s, 'can_edit')
  end

  def destroy?
    return false unless (@user.internal? && Scope.new(@user, Deal.all).resolve.include?(@deal))
    @user.super_admin? || @user.check_permission(@deal.class.to_s, 'can_destroy')
  end

  def change_state?
    return false unless (@user.internal? && Scope.new(@user, Deal.all).resolve.include?(@deal))
    @user.super_admin? || @user.check_permission(@deal.class.to_s, 'change_state')
  end

  def assign?
    return false unless (@user.internal? && Scope.new(@user, Deal.all).resolve.include?(@deal))
    @user.super_admin? || @user.check_permission(@deal.class.to_s, 'can_assign')
  end

  def update_price?
    return false unless (@user.internal? && Scope.new(@user, Deal.all).resolve.include?(@deal))
    @user.super_admin? || @user.check_permission(@deal.class.to_s, 'can_update_price')
  end

  def update_commission?
    return false unless (@user.internal? && Scope.new(@user, Deal.all).resolve.include?(@deal))
    @user.super_admin? || @user.check_permission(@deal.class.to_s, 'can_update_commission')
  end

  def transfer?
    return false unless (@user.internal? && Scope.new(@user, Deal.all).resolve.include?(@deal) && @deal.can_transfer?)

    @user.super_admin? || @user.sale_admin?
  end

  class Scope < Scope
    def resolve
      records = if @user.super_admin?
                  scope
                else
                  if permission = @user.get_permission("Deal")
                    case permission.data_area
                    when 0
                      scope.where(assignee_id: @user.id).where(state: permission.state_area)
                    when 1
                      scope.where(assignee_id: @user.subtree_ids ).where(state: permission.state_area)
                    when 2
                      scope.where(state: permission.state_area)
                    else
                      []
                    end
                  else
                    []
                  end
                end
      return records
    end
  end
end

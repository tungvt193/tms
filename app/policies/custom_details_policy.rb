class CustomDetailsPolicy < ApplicationPolicy
  attr_reader :user, :custom_detail
  def initialize user, custom_detail
    @user = user
    @custom_detail = custom_detail
  end

  def index?
    return false unless @user.internal?
    # @user.check_permission 'CustomDetail', 'can_view'
    @user.check_permission_index 'CustomDetail'
  end

  def create?
    return false unless @user.internal?
    (@user.check_permission 'CustomDetail', 'can_create') ||
    (@user.check_permission 'CustomDetail', 'can_edit') ||
    (@user.check_permission 'CustomDetail', 'can_update')
  end

end

class KpiPolicy < ApplicationPolicy
  attr_reader :user, :kpi
  def initialize user, kpi
    @user = user
    @kpi = kpi
  end

  def index?
    return false unless @user.internal?
    # @user.check_permission 'kpi', 'can_view'
    @user.check_permission_index 'Kpi'
  end

  def update?
    return false unless @user.internal?
    @user.check_permission @kpi.class.to_s, 'can_update'
  end

  def edit?
    return false unless @user.internal?
    @user.check_permission @kpi.class.to_s, 'can_edit' || update?
  end

  def create?
    return false unless @user.internal?
    @user.check_permission @kpi.class.to_s, 'can_create'
  end

  def show?
    return false unless @user.internal?
    @user.check_permission(@kpi.class.to_s, 'can_edit') ||
    create? || update?
  end

  def destroy?
    return false unless @user.internal?
    @user.check_permission @kpi.class.to_s, 'can_destroy'
  end

end

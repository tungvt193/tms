class CustomerTicketPolicy < ApplicationPolicy
  attr_reader :user, :customer_ticket
  def initialize user, customer_ticket
    @user = user
    @customer_ticket = customer_ticket
  end

  def index?
    return false unless @user.internal?
    # @user.check_permission 'CustomerTicket', 'can_view'
    @user.check_permission_index 'CustomerTicket'
  end

  def update?
    return false unless @user.internal?
    @user.check_permission @customer_ticket.class.to_s, 'can_update'
  end

  def edit?
    return false unless @user.internal?
    @user.check_permission @customer_ticket.class.to_s, 'can_edit' || update?
  end

  def create?
    return false unless @user.internal?
    @user.check_permission @customer_ticket.class.to_s, 'can_create'
  end

  def show?
    return false unless @user.internal?
    @user.check_permission(@customer_ticket.class.to_s, 'can_edit') ||
    create? || update?
  end

  def destroy?
    return false unless @user.internal?
    @user.check_permission @customer_ticket.class.to_s, 'can_destroy'
  end

  def change_state?
    return false unless @user.internal?
    @user.check_permission @customer_ticket.class.to_s, 'change_state'
  end

end

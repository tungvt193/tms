class LayoutPolicy < ApplicationPolicy
  attr_reader :user, :layout
  def initialize user, layout
    @user = user
    @layout = layout
  end

  def index?
    return false unless @user.internal?
    # @user.check_permission 'Layout', 'can_view'
    @user.check_permission_index 'Layout'
  end

  def update?
    return false unless @user.internal?
    @user.check_permission @layout.class.to_s, 'can_update'
  end

  def edit?
    return false unless @user.internal?
    @user.check_permission @layout.class.to_s, 'can_edit' || update?
  end

  def create?
    return false unless @user.internal?
    @user.check_permission @layout.class.to_s, 'can_create'
  end

  def show?
    return false unless @user.internal?
    @user.check_permission(@layout.class.to_s, 'can_edit') ||
    create? || update?
  end

  def destroy?
    return false unless @user.internal?
    @user.check_permission @layout.class.to_s, 'can_destroy'
  end

end

class CompanyRevenuePolicy < ApplicationPolicy
  attr_reader :user, :revenue

  def initialize(user, revenue)
    @user = user
    @revenue = revenue
  end

  def index?
    return false unless @user.internal?
    @user.check_permission_index 'CompanyRevenue'
  end

  def update?
    return false unless @user.internal?
    @user.check_permission @revenue.class.to_s, 'can_update'
  end

  def edit?
    return false unless @user.internal?
    @user.check_permission @revenue.class.to_s, 'can_edit' || update?
  end

  def create?
    return false unless @user.internal?
    @user.check_permission @revenue.class.to_s, 'can_create'
  end

  def destroy?
    return false unless @user.internal?
    @user.check_permission @revenue.class.to_s, 'can_destroy'
  end
end

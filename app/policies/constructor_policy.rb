class ConstructorPolicy < ApplicationPolicy
  attr_reader :user, :constructor
  def initialize user, constructor
    @user = user
    @constructor = constructor
  end

  def index?
    return false unless @user.internal?
    @user.check_permission_index 'Constructor'
  end

  def update?
    return false unless @user.internal?
    @user.check_permission @constructor.class.to_s, 'can_update'
  end

  def edit?
    return false unless @user.internal?
    @user.check_permission @constructor.class.to_s, 'can_edit' || update?
  end

  def create?
    return false unless @user.internal?
    @user.check_permission @constructor.class.to_s, 'can_create'
  end

  def show?
    return false unless @user.internal?
    @user.check_permission(@constructor.class.to_s, 'can_edit') ||
    create? || update?
  end

  def destroy?
    return false unless @user.internal?
    @user.check_permission @constructor.class.to_s, 'can_destroy'
  end

end

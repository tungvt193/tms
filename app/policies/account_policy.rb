class AccountPolicy < ApplicationPolicy
  attr_reader :user, :account
  def initialize user, account
    @user = user
    @account = account
  end

  def index?
    return false unless @user.internal?
    @user.check_permission_index 'Account'
  end

  def update?
    return false unless @user.internal?
    @user.check_permission @account.class.to_s, 'can_update'
  end

  def edit?
    return false unless @user.internal?
    @user.check_permission @account.class.to_s, 'can_edit' || update?
  end

  def create?
    return false unless @user.internal?
    @user.check_permission @account.class.to_s, 'can_create'
  end

  def show?
    return false unless @user.internal?
    @user.check_permission(@account.class.to_s, 'can_edit') ||
    create? || update? || change_state?
  end

  def destroy?
    return false unless @user.internal?
    @user.check_permission @account.class.to_s, 'can_destroy'
  end

  def change_state?
    return false unless @user.internal?
    @user.check_permission @account.class.to_s, 'change_state'
  end

end

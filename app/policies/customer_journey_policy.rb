class CustomerJourneyPolicy
  attr_reader :user, :customer_journey
  def initialize user, customer_journey
    @user = user
    @customer_journey = customer_journey
  end

  def index?
    return false unless @user.internal?
    @user.check_permission(@customer_journey.to_s.camelize, 'can_view')
  end

  def update?
    return false unless @user.internal?
    @user.check_permission @customer_journey.to_s.camelize, 'can_update'
  end

  def create?
    return false unless @user.internal?
    @user.check_permission @customer_journey.to_s.camelize, 'can_create'
  end

  def show?
    return false unless @user.internal?
    @user.check_permission(@customer_journey.to_s.camelize, 'can_edit') ||
    create? || update?
  end

  def destroy?
    return false unless @user.internal?
    @user.check_permission @customer_journey.to_s.camelize, 'can_destroy'
  end

end

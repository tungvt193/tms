class OperatorPolicy < ApplicationPolicy
  attr_reader :user, :operator
  def initialize user, operator
    @user = user
    @operator = operator
  end

  def index?
    return false unless @user.internal?
    # @user.check_permission 'Operator', 'can_view'
    @user.check_permission_index 'Operator'
  end

  def update?
    return false unless @user.internal?
    @user.check_permission @operator.class.to_s, 'can_update'
  end

  def edit?
    return false unless @user.internal?
    @user.check_permission @operator.class.to_s, 'can_edit' || update?
  end

  def create?
    return false unless @user.internal?
    @user.check_permission @operator.class.to_s, 'can_create'
  end

  def show?
    return false unless @user.internal?
    @user.check_permission(@operator.class.to_s, 'can_edit') ||
    create? || update?
  end

  def destroy?
    return false unless @user.internal?
    @user.check_permission @operator.class.to_s, 'can_destroy'
  end

end

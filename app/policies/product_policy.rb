class ProductPolicy < ApplicationPolicy
  attr_reader :user, :product
  def initialize user, product
    @user = user
    @product = product
  end

  def index?
    return false unless @user.internal?
    # @user.check_permission 'Product', 'can_view'
    @user.check_permission_index 'Product'
  end

  def update?
    return false unless @user.internal?
    @user.check_permission 'Product', 'can_update'
  end

  def edit?
    return false unless @user.internal?
    @user.check_permission 'Product', 'can_edit' || update?
  end

  def create?
    return false unless @user.internal?
    @user.check_permission 'Product', 'can_create'
  end

  def show?
    return false unless @user.internal?
    @user.check_permission('Product', 'can_edit') ||
    create? || update?
  end

  def destroy?
    return false unless @user.internal?
    @user.check_permission 'Product', 'can_destroy'
  end

  def import?
    return false unless @user.internal?
    @user.check_permission 'Product', 'can_import'
  end

  def change_state?
    return false unless @user.internal?
    @user.check_permission 'Product', 'change_state'
  end

end

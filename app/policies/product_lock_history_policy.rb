class ProductLockHistoryPolicy < ApplicationPolicy
  attr_reader :user, :product_lock_history

  def initialize(user, product_lock_history)
    @user = user
    @product_lock_history = product_lock_history
  end

  def index?
    @user.check_permission_index 'ProductLockHistory'
  end

  def update?
    @user.check_permission @product_lock_history.class.to_s, 'can_update'
  end

  def edit?
    @user.check_permission @product_lock_history.class.to_s, 'can_edit' || update?
  end

  def create?
    @user.check_permission @product_lock_history.class.to_s, 'can_create'
  end

  def show?
    @user.check_permission(@product_lock_history.class.to_s, 'can_edit') || create? || update?
  end

  # def destroy?
  #   @user.check_permission @product_lock_history.class.to_s, 'can_destroy'
  # end
end
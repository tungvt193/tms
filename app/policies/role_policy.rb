class RolePolicy < ApplicationPolicy
  attr_reader :user, :role
  def initialize user, role
    @user = user
    @role = role
  end

  def index?
    return false unless @user.internal?
    # @user.check_permission 'role', 'can_view'
    @user.check_permission_index 'role'
  end

  def new_clone?
    return false unless @user.internal?
    @user.check_permission @role.class.to_s, 'can_create'
  end

  def update?
    return false unless @user.internal?
    @user.check_permission @role.class.to_s, 'can_update'
  end

  def edit?
    return false unless @user.internal?
    @user.check_permission @role.class.to_s, 'can_edit' || update?
  end

  def create?
    return false unless @user.internal?
    @user.check_permission @role.class.to_s, 'can_create'
  end

  def show?
    return false unless @user.internal?
    @user.check_permission(@role.class.to_s, 'can_edit') ||
    create? || update?
  end

  def destroy?
    return false unless @user.internal?
    @user.check_permission @role.class.to_s, 'can_destroy'
  end

end

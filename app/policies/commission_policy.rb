class CommissionPolicy < ApplicationPolicy
  attr_reader :user, :commission
  def initialize(user, commission)
    @user = user
    @commission = commission
  end

  def update?
    return false unless @user.internal?
    @user.check_permission @commission.class.to_s, 'can_update'
  end

  def show?
    return false unless @user.internal?
    @user.check_permission(@commission.class.to_s, 'can_edit') || update?
  end
end

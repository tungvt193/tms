class InvestorPolicy < ApplicationPolicy
  attr_reader :user, :investor
  def initialize user, investor
    @user = user
    @investor = investor
  end

  def index?
    return false unless @user.internal?
    # @user.check_permission 'Investor', 'can_view'
    @user.check_permission_index 'Investor'
  end

  def update?
    return false unless @user.internal?
    @user.check_permission @investor.class.to_s, 'can_update'
  end

  def edit?
    return false unless @user.internal?
    @user.check_permission @investor.class.to_s, 'can_edit' || update?
  end

  def create?
    return false unless @user.internal?
    @user.check_permission @investor.class.to_s, 'can_create'
  end

  def show?
    return false unless @user.internal?
    @user.check_permission(@investor.class.to_s, 'can_edit') ||
    create? || update?
  end

  def destroy?
    return false unless @user.internal?
    @user.check_permission @investor.class.to_s, 'can_destroy'
  end

end

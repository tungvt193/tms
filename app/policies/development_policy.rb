class DevelopmentPolicy < ApplicationPolicy
  attr_reader :user, :development
  def initialize user, development
    @user = user
    @development = development
  end

  def index?
    return false unless @user.internal?
    # @user.check_permission 'Development', 'can_view'
    @user.check_permission_index 'Development'
  end

  def update?
    return false unless @user.internal?
    @user.check_permission @development.class.to_s, 'can_update'
  end

  def edit?
    return false unless @user.internal?
    @user.check_permission @development.class.to_s, 'can_edit' || update?
  end

  def create?
    return false unless @user.internal?
    @user.check_permission @development.class.to_s, 'can_create'
  end

  def show?
    return false unless @user.internal?
    @user.check_permission(@development.class.to_s, 'can_edit') ||
    create? || update?
  end

  def destroy?
    return false unless @user.internal?
    @user.check_permission @development.class.to_s, 'can_destroy'
  end

end

class UserPolicy < ApplicationPolicy
  attr_reader :current_user, :user
  def initialize current_user, user
    @current_user = current_user
    @user = user
  end

  def index?
    return false unless @current_user.internal?
    # @current_user.check_permission 'User', 'can_view'
    @current_user.check_permission_index 'User'
  end

  def update?
    return false unless @current_user.internal?
    @current_user.check_permission @user.class.to_s, 'can_update'
  end

  def edit?
    return false unless @current_user.internal?
    @current_user.check_permission @user.class.to_s, 'can_edit' || update?
  end

  def create?
    return false unless @current_user.internal?
    @current_user.check_permission @user.class.to_s, 'can_create'
  end

  def show?
    return false unless @current_user.internal?
    @current_user.check_permission(@user.class.to_s, 'can_edit') ||
    create? || update?
  end

  def destroy?
    return false unless @current_user.internal?
    @current_user.check_permission @user.class.to_s, 'can_destroy'
  end

end

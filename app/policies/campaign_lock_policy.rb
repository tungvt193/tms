class CampaignLockPolicy < ApplicationPolicy
  attr_reader :user, :campaign_lock

  def initialize(user, campaign_lock)
    @user = user
    @campaign_lock = campaign_lock
  end

  def index?
    @user.check_permission_index 'CampaignLock'
  end

  def update?
    @user.check_permission @campaign_lock.class.to_s, 'can_update'
  end

  def edit?
    @user.check_permission @campaign_lock.class.to_s, 'can_edit' || update?
  end

  def create?
    @user.check_permission @campaign_lock.class.to_s, 'can_create'
  end

  def show?
    @user.check_permission(@campaign_lock.class.to_s, 'can_edit') || create? || update?
  end

  def destroy?
    @user.check_permission @campaign_lock.class.to_s, 'can_destroy'
  end
end
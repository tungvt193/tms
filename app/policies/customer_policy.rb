class CustomerPolicy < ApplicationPolicy
  attr_reader :user, :customer
  def initialize(user, customer)
    @user = user
    @customer = customer
  end

  def index?
    @user.check_permission_index 'Customer'
  end

  def update?
    return false unless (Scope.new(@user, Customer.all).resolve.include?(@customer))
    @user.check_permission @customer.class.to_s, 'can_update'
  end

  def edit?
    return false unless (Scope.new(@user, Customer.all).resolve.include?(@customer))
    @user.check_permission @customer.class.to_s, 'can_edit' || update?
  end

  def create?
    @user.check_permission @customer.class.to_s, 'can_create'
  end

  def show?
    return false unless (Scope.new(@user, Customer.all).resolve.include?(@customer))
    @user.check_permission(@customer.class.to_s, 'can_edit') ||
    create? || update?
  end

  def destroy?
    return false unless (Scope.new(@user, Customer.all).resolve.include?(@customer))
    @user.check_permission @customer.class.to_s, 'can_destroy'
  end

  def import?
    @user.check_permission @customer.class.to_s, 'can_import'
  end

  class Scope < Scope
    def resolve
      if @user.super_admin?
        scope
      else
        if customer_permission = @user.get_permission("Customer")
          case customer_permission.data_area
          when 0
            deals = Deal.where(assignee_id: @user.id)
            return scope.where(id: deals.pluck('customer_id')).or(scope.where(created_by_id: user.id))
          when 1
            user_deals = []
            user_customers = []
            # tìm tất cả các deal của current_user và cấp dưới
            # tìm tất cả khách hàng của cấp dưới tạo và được sử dụng
            @user.subtree.each do |u|
              u_deal_permission = u.get_permission("Deal")
              u_customer_permission = u.get_permission("Customer")
              if u_deal_permission
                # với u là current_user chỉ cần tìm deal của chính ông đó, còn deal cấp dưới sẽ được tìm với từng ông cấp dưới
                # vì là tìm khách hàng nên tìm trên tất cả các deal đc asign không phải xét phạm vi trạng thái
                if @user == u
                  user_deals += Deal.where(assignee_id: u.id).pluck(:customer_id)
                else
                  case u_deal_permission.data_area
                  when 0
                    user_deals += Deal.where(assignee_id: u.id).pluck(:customer_id)
                  when 1
                    user_deals += Deal.where(assignee_id: u.subtree_ids).pluck(:customer_id)
                  when 2
                    user_deals += Deal.all.pluck(:customer_id)
                  end
                end
              end

              if u_customer_permission
                case u_customer_permission.data_area
                when 0
                  user_customers += Customer.where(created_by_id: u.id).pluck(:id)
                when 1
                  user_customers += Customer.where(created_by_id: u.subtree_ids).pluck(:id)
                when 2
                  user_customers += Customer.all.pluck(:id)
                end
              end
            end

            return scope.where(id: user_deals).or(scope.where(id: user_customers))
          when 2
            scope
          else
            []
          end
        end
      end
    end
  end
end

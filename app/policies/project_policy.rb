class ProjectPolicy < ApplicationPolicy
  attr_reader :user, :project
  def initialize user, project
    @user = user
    @project = project
  end

  def index?
    # @user.check_permission 'Project', 'can_view'
    @user.check_permission_index('Project') ||
    @user.check_permission_index('Product') ||
    @user.check_permission_index('Layout') ||
    @user.check_permission_index('CustomDetail')
  end

  def update?
    return false unless @user.internal?
    @user.check_permission 'Project', 'can_update'
  end

  def edit?
    return false unless @user.internal?
    @user.check_permission 'Project', 'can_edit' || update?
  end

  def create?
    return false unless @user.internal?
    @user.check_permission 'Project', 'can_create'
  end

  def show?
    @user.check_permission('Project', 'can_edit') ||
    create? || update? ||
    @user.check_permission_index('Product') ||
    @user.check_permission_index('Layout') ||
    @user.check_permission_index('CustomDetail')
  end

  def destroy?
    return false unless @user.internal?
    @user.check_permission 'Project', 'can_destroy'
  end

  def import?
    return false unless @user.internal?
    @user.check_permission( 'Project', 'can_import')
  end
end

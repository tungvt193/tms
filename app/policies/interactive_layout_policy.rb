class InteractiveLayoutPolicy < ApplicationPolicy
  attr_reader :user, :interactive_layout
  def initialize user, interactive_layout
    @user = user
    @interactive_layout = interactive_layout
  end

  def index?
    return false unless @user.internal?
    # @user.check_permission 'interactive_layout', 'can_view'
    @user.check_permission_index 'InteractiveLayout'
  end

  def update?
    return false unless @user.internal?
    @user.check_permission @interactive_layout.class.to_s, 'can_update'
  end

  def edit?
    return false unless @user.internal?
    @user.check_permission(@interactive_layout.class.to_s, 'can_edit') || update?
  end

  def create?
    return false unless @user.internal?
    @user.check_permission @interactive_layout.class.to_s, 'can_create'
  end

  def show?
    return false unless @user.internal?
    @user.check_permission(@interactive_layout.class.to_s, 'can_edit') ||
    create? || update?
  end

  def destroy?
    return false unless @user.internal?
    @user.check_permission @interactive_layout.class.to_s, 'can_destroy'
  end

end

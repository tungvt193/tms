class RegulationPolicy < ApplicationPolicy
  attr_reader :user, :regulation
  def initialize user, regulation
    @user = user
    @regulation = regulation
  end

  def index?
    return false unless @user.internal?
    # @user.check_permission 'regulation', 'can_view'
    @user.check_permission_index 'Regulation'
  end

  def update?
    return false unless @user.internal?
    @user.check_permission @regulation.class.to_s, 'can_update'
  end

  def edit?
    return false unless @user.internal?
    @user.check_permission @regulation.class.to_s, 'can_edit' || update?
  end

  def create?
    return false unless @user.internal?
    @user.check_permission @regulation.class.to_s, 'can_create'
  end

  def show?
    return false unless @user.internal?
    @user.check_permission(@regulation.class.to_s, 'can_edit') ||
    create? || update?
  end

  def destroy?
    return false unless @user.internal?
    @user.check_permission @regulation.class.to_s, 'can_destroy'
  end

  def change_state?
    return false unless @user.internal?
    @user.check_permission @regulation.class.to_s, 'change_state'
  end

end

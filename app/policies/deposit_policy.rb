class DepositPolicy < ApplicationPolicy
  attr_reader :user, :deposit
  def initialize user, deposit
    @user = user
    @deposit = deposit
  end

  def index?
    return false unless @user.internal?
    # @user.check_permission 'Deposit', 'can_view'
    @user.check_permission_index 'Deposit'
  end

  def update?
    return false unless @user.internal?
    @user.check_permission @deposit.class.to_s, 'can_update'
  end

  def edit?
    return false unless @user.internal?
    @user.check_permission @deposit.class.to_s, 'can_edit' || update?
  end

  def create?
    return false unless @user.internal?
    @user.check_permission @deposit.class.to_s, 'can_create'
  end

  def show?
    return false unless @user.internal?
    @user.check_permission(@deposit.class.to_s, 'can_edit') ||
    create? || update?
  end

  def destroy?
    return false unless @user.internal?
    @user.check_permission @deposit.class.to_s, 'can_destroy'
  end

end

class ValidUrlValidator < ActiveModel::EachValidator
  URL_CONTENT_REGEX = /^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/ix

  def validate_each record, attribute, value
    return if value.blank?
    unless URL_CONTENT_REGEX =~ value
      record.errors.add(attribute, I18n.t("errors.url.attributes.#{attribute}"))
    end
  end
end

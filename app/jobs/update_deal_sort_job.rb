class UpdateDealSortJob < ApplicationJob
  queue_as :deal_sorting

  def perform(deal)
    User.all.each do |user|
      if Pundit.policy(user, deal).show?
        unless deal.get_deal_sort(user)
          position_min = Deal.joins(:deal_sorts).where(state: deal.state, 'deal_sorts.user_id': user.id).minimum('deal_sorts.position').to_i
          deal.deal_sorts.create(user_id: user.id, position: position_min - 1)
        end
      end
    end
  end
end

class AppointmentJob < ApplicationJob
  queue_as :appointment

  def perform(type, appointment, old_value, new_value, deal)
    case type
    when 'create_appointment'
      NotificationService.create_appointment(deal)
    when 'appointment_date'
      NotificationService.remind_appointment_date(appointment, deal)
    else
      NotificationService.update_result_appointment(appointment, deal)
    end
  rescue => e
    Sidekiq.logger.info(e)
  end
end

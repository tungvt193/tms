class CsTicketJob < ApplicationJob
  queue_as :default

  def perform(state, user)
    return unless state.present?
    file_name = if state == 'deposit'
                  "da_dat_coc_#{Date.current.strftime('%d-%m-%Y')}"
                elsif state == 'canceled'
                  "da_huy_#{Date.current.strftime('%d-%m-%Y')}"
                else
                  "ky_hop_dong_#{Date.current.strftime('%d-%m-%Y')}"
                end
    cs_ticket_service = CsTicketService.new(state, file_name)
    cs_ticket_service.export

    cs_ticket_uploader = CsTicketUploader.new
    path = "#{Rails.root}/tmp/#{file_name}.xlsx"
    if File.exist? path
      cs_ticket_uploader.store!(File.open(path))
      path_file = 'https://' + ENV['S3_BUCKET'] + '.s3.' + ENV['S3_REGION'] + ".amazonaws.com/cs_tickets/#{file_name}.xlsx"
      DealMailer.download_cs_tickets(user.email, path_file).deliver
      ::File.delete path
    end
  end
end

class RemindBirthdayJob < ApplicationJob
  queue_as :customer_birthday

  def perform
    next_date = Date.today.next
    customer_personals = CustomerPersona.where('extract(month from "customer_personas"."dob") = ? AND extract(day from "customer_personas"."dob") = ?', next_date.month, next_date.day)
    customers = Customer.where(id: customer_personals.pluck(:customer_id))
    
    customers.each do |customer|
      # Lấy tất cả deals của customer
      # điều kiện deal phải có ngày sinh nhật trong tab chân dung KH
      deals_by_customer = customer.deals.where(id: customer_personals.pluck(:deal_id))

      # Lấy deal gần nhất của KH
      last_deal = deals_by_customer.order(updated_at: :desc).first
      next unless last_deal.present?

      NotificationService.remind_hpbd_customer_for_cskh(customer, last_deal)

      assignee_ids = deals_by_customer.pluck(:assignee_id).uniq
      User.where(id: assignee_ids).each do |assignee|
        # lấy deal có cập nhật gần nhất của KH với sale
        last_deal_with_assignee = deals_by_customer.where(assignee_id: assignee.id).order(updated_at: :desc).first

        NotificationService.remind_hpbd_customer_for_sale(customer, assignee, last_deal_with_assignee)
      end
    rescue => e
      Sidekiq.logger.info(e)
      next
    end
  rescue => e
    Sidekiq.logger.info(e)
  end
end

class DealSortJob < ApplicationJob
  queue_as :deal_sorting

  def perform(state_to, deal_id_current, deal_id_above, current_user)
    deal = Deal.find(deal_id_current) # deal hiện tại
    deal_new = deal.deal_sorts.blank?
    # User new and deal_id_above not position
    create_deal_sort(current_user) if current_user.deal_sorts.blank? || (deal_id_above.present? && !Deal.find_by_id(deal_id_above)&.get_position(current_user))
    # Deal new
    create_deal_sort_new(deal) if deal_new
    deal_sort_current = deal.get_deal_sort(current_user)
    deal_sort_current ||= create_deal_sort_user(deal, current_user) # Deal old and not position
    if deal_id_above.present?
      deal_above = Deal.find(deal_id_above) # deal phía trên
      deal_above_position = deal_above.get_position(current_user)
      deal_sort_current.position = deal_above_position
      # cập nhật tất cả stt phía trên của deal +1
      all_deal_sort_above = current_user.deal_sorts.joins(:deal).where('deals.state': state_to).where('deal_sorts.position <= ?', deal_above_position)
      all_deal_sort_above.find_each do |deal_sort|
        deal_sort.update_columns({ position: deal_sort.position.to_i - 1 })
      end
    else
      deal_above_position = current_user.deal_sorts.joins(:deal).where('deals.state': state_to).minimum('deal_sorts.position').to_i - 1
      deal_sort_current.position = deal_above_position
    end
    deal_sort_current.save
  end

  def create_deal_sort(current_user)
    arr_pos = {}
    Deal.aasm.states.each do |sta|
      arr_pos["#{sta.name.to_s}"] = Deal.joins(:deal_sorts).where(state: sta.name.to_s, 'deal_sorts.user_id': current_user.id).minimum('deal_sorts.position').to_i
    end
    Pundit.policy_scope(current_user, Deal).left_outer_joins(:deal_sorts).where('deal_sorts.id is null or deal_sorts.user_id != ?', current_user.id).find_each do |deal|
      unless deal.get_deal_sort(current_user)
        arr_pos["#{deal.state}"] ||= 0
        arr_pos["#{deal.state}"] -= 1
        deal.deal_sorts.create(user_id: current_user.id, position: arr_pos["#{deal.state}"])
      end
    end
  end

  def create_deal_sort_new(deal)
    User.find_each do |user|
      if Pundit.policy(user, deal).show? && !deal.get_deal_sort(user)
        position_min = Deal.joins(:deal_sorts).where(state: deal.state, 'deal_sorts.user_id': user.id).minimum('deal_sorts.position').to_i
        deal.deal_sorts.create(user_id: user.id, position: position_min - 1)
      end
    end
  end

  def create_deal_sort_user(deal, user)
    position_min = Deal.joins(:deal_sorts).where(state: deal.state, 'deal_sorts.user_id': user.id).minimum('deal_sorts.position').to_i
    deal.deal_sorts.create(user_id: user.id, position: position_min - 1)
  end
end

class ReorderSaleDocumentPositionJob < ApplicationJob
  queue_as :default

  def perform(current_document_id, prev_document_id, project)
    current_doc = SaleDocument.find_by(id: current_document_id)
    doc_type = current_doc.sale_document_type
    if prev_document_id.present?
      prev_doc = SaleDocument.find_by(id: prev_document_id)
      position = prev_doc.position
      project.sale_documents.where(sale_document_type: doc_type).where('position <= ?', position).each do |doc|
        doc.update_column(:position, doc.position - 1)
      end
    else
      position = project.sale_documents.where(sale_document_type: doc_type).minimum('position') - 1
    end
    current_doc.update_column(:position, position)
  end
end

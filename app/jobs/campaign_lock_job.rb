class CampaignLockJob < ApplicationJob
  queue_as :campaign_lock

  def perform(type, campaign_lock)
    case type
    when 'locking'
      campaign_lock.state = 'locking'
      campaign_lock.save
    when 'create_campaign_lock'
      NotificationService.create_campaign_lock(campaign_lock)
    when 'before_campaign_lock_open'
      NotificationService.before_campaign_lock_open(campaign_lock)
    when 'campaign_lock_open'
      NotificationService.campaign_lock_open(campaign_lock)
    when 'check_out_of_stock'
      products = Product.where(id: campaign_lock.products, state: %i(deposited sold))
      campaign_lock.update(state: :out_of_stock) if products.size == campaign_lock.products.size
    end
  rescue => e
    Sidekiq.logger.info(e)
  end
end

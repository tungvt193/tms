class SyncLeadFromCallio < ApplicationJob

  queue_as :deal_synchronization

  def perform(lead)
    callio_campaign_id = lead[:campaign]
    response = RestClient.get("https://clientapi.phonenet.io/call-campaign/#{callio_campaign_id}",
                              headers = { 'token': ENV['CALLIO_TOKEN'] }
    )
    campaign = JSON.parse(response.body)
    lead_from_telesale = campaign['name'].downcase.include?('telesale')
    project_name = campaign['name'].split('-')[1].strip
    if lead_from_telesale
      customer_phone = lead[:phone]
      custom_fields = lead[:customFields]
      telesale_note = custom_fields.detect { |e| e[:key] == 'ghi-chu' }[:val].first
      telesale_interest_detail = custom_fields.detect { |e| e[:key] == 'chi-tiet-dong-y' }[:val].first
      telesale_group = custom_fields.detect { |e| e[:key] == 'cn-nhan-lead' }[:val].first
      customer_name = custom_fields.detect { |e| e[:key] == 'ten-kh-chinh-xac' }[:val].present? ? custom_fields.detect { |e| e[:key] == 'ten-kh-chinh-xac' }[:val].first : lead[:name]
      project = Project.find_by(name: project_name)
      if (telesale_interest_detail == 'Khách có quan tâm dự án khác , sale liên hệ tv thêm' || !project.present?)
        miss_project = true
        project_id = nil
      else
        miss_project = false
        project_id = project.id
      end
      if project.present? && miss_project == false
        group = Group.where('city_ids @> ARRAY[?]::integer[]', [project.region.city_id])
                     .where(name: telesale_group).first
        assignee_id = group.director_id
      end
      ActiveRecord::Base.transaction do
        customer = Customer.create(name: customer_name,
                                   phone_number: customer_phone,
                                   country_code: 'VN',
                                   data_source: 45,
                                   created_by_id: 1)
        deal = Deal.new(customer_id: customer.id,
                        project_id: project_id,
                        callio_campaign_id: callio_campaign_id,
                        telesale_note: telesale_note,
                        telesale_interest_detail: telesale_interest_detail,
                        telesale_group: telesale_group,
                        miss_project: miss_project,
                        source: 45,
                        assignee_id: assignee_id)
        deal.save(validate: false)
        Deal.reindex
        puts 'Yeahooo!'
      rescue
        puts 'Oops! Something went wrong'
      end
    end
  end
end

class NotificationJob < ApplicationJob
  queue_as :notification

  def perform(type, object)
    case type
    when 'create_project'
      NotificationService.create_project(object)
    when 'sale_admin_accept_unc'
      NotificationService.sale_admin_accept_unc(object)
    when 'sale_admin_reject_unc'
      NotificationService.sale_admin_reject_unc(object)
    when 'sale_admin_pending_unc'
      NotificationService.sale_admin_pending_unc(object)
    when 'warning_lock_timeout'
      NotificationService.warning_lock_timeout(object)
    end
  rescue => e
    Sidekiq.logger.info(e)
  end
end

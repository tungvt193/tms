# Sync Data from User, Deal, Product, Project table to GG Sheet
class SyncDataToGoogleSheetJob < ApplicationJob
  queue_as :default

  def perform(klass)
    sheets = { 'User' => 'IN_SALES', 'Project' => 'IN_PROJECTS', 'Deal' => 'IN_TRANS', 'Product' => 'IN_STOCK' }
    session = GoogleDrive::Session.from_config("client_secret.json")
    wss = session.spreadsheet_by_key('1RX_x7sEPm2TLsu546xf7QI9es0l6wcyxwX0_hJbt83E').worksheets
    ws = wss.detect { |w| w.title == sheets[klass] }
    insert_data(ws, klass)
  end

  private

  def insert_data(ws, klass)
    ws.delete_rows(3, ws.rows.size - 2)
    attributes = ws.rows[1]
    case klass
    when 'User'
      data = User.joins(:role).order(id: :asc).pluck(attributes)
    when 'Project'
      data = Project.joins(:region).order(id: :asc).pluck(attributes)
    when 'Product'
      data = Product.joins(:project).order(id: :asc).pluck(attributes)
    when 'Deal'
      data = Deal.joins(:customer, :customer_persona, :project, :product, :assignee).order(id: :asc).pluck(attributes)
    end
    ws.insert_rows(3, data)
    ws.save
  end
end

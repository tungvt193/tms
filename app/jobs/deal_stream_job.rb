class DealStreamJob < ApplicationJob
  queue_as :default
  def perform(deal, user_id)
    ActionCable.server.broadcast "deal_channel", deal: deal, user_id: user_id
  end
end
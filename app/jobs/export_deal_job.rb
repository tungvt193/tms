class ExportDealJob < ApplicationJob
  queue_as :default

  def self.perform(deals, attributes)
    tempfile = File.new("#{Rails.root}/public/Bao_cao_QLGD_#{Date.today.strftime(Settings.date.formats.time_export_xlsx)}.xlsx", "w")
    file_name = "Bao_cao_QLGD_#{Date.today.strftime(Settings.date.formats.time_export_xlsx)}.xlsx"
    DealService.new(deals, attributes).export.serialize(tempfile)
    DealMailer.export_deals(User.current.email, tempfile, file_name).deliver
    ::File.delete(tempfile)
  end
end

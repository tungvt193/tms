class CheckProductLockHistoryJob < ApplicationJob
  queue_as :lock_product

  def perform(lock_history_id, type_check)
    product_lock_history = ProductLockHistory.find(lock_history_id)
    case type_check
    when 'timeout_for_sale'
      if product_lock_history.locking? || product_lock_history.deposit_confirmation?
        product_lock_history.update(state: :out_of_time)
        product_lock_history.product.update_column(:state, :for_sale)
        NotificationService.lock_out_of_time(product_lock_history)
      end
    when 'timeout_for_sale_admin'
      if product_lock_history.waiting_reti_acceptance?
        product_lock_history.update(state: :pending)
        NotificationService.remind_timeout_for_sale_admin(product_lock_history)
      end
    end
  end
end

class SyncProjectToGoogleSheetJob < ApplicationJob
  queue_as :default

  def perform(project_id, event)
    session = GoogleDrive::Session.from_config("client_secret.json")
    wss = session.spreadsheet_by_key(ENV['GOOGLE_SHEET_FILE_KEY']).worksheets
    ws = wss.detect{|w| w.title == "Sync from TMS"}
    raise "Can't get google sheet" unless ws

    case event
    when 'destroy_project'
      ws.rows.drop(1).each_with_index do |row, index|
        next unless row[0] == project_id.to_s

        ws.delete_rows(index + 2, 1)
        ws.save
      end
    when 'update_project'
      ws.rows.drop(1).each_with_index do |row, index|
        next unless row[0] == project_id.to_s

        ws[index + 2, 2] = Project.find(project_id).name
        ws.save
      end
    else
      insert_data(ws)
    end
  rescue => e
    Sidekiq.logger.info(e)
  end

  private

  def insert_data(ws)
    project_ids = ws.rows.drop(1).map { |row| row[0] }
    project_records = Project.where.not(id: project_ids).order(id: :asc).pluck(:id, :name)
    if project_records.present?
      ws.insert_rows(ws.num_rows + 1, project_records)
      ws.save
    end
  end
end

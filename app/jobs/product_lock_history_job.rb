class ProductLockHistoryJob < ApplicationJob

  queue_as :lock_product

  def perform(type, product, lock, current_user)
    if type == 'enqueued'
      ::NotificationService.remind_product_for_sale(product, lock, current_user)
    end
  rescue => e
    Sidekiq.logger.info(e)
  end
end
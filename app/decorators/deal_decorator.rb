module DealDecorator

  def allow_assignee?
    ((User.current.has_children? && User.current.check_permission(self.class.to_s, 'can_assign')) || User.current.super_admin? || User.current.sale_admin? || User.current.director?) && !%w(canceled uninterested).include?(self.state)
  end

  def comm_fields_blank?
    self.total_price.blank? || self.contract_signed_at.blank?
  end

  def states
    Deal.aasm.states.map do |state|
      [I18n.t("deal.states.#{state}"), state]
    end.uniq
  end

  def available_states
    states = []
    all_states = if %w(canceled contract_signed uninterested).include?(self.state)
                   []
                 elsif self.state == 'request_recall'
                   %w(pending)
                 else
                   case true
                   when User.current.sale_member?
                     %w(pending interest meet refundable_deposit lock deposit contract_signed canceled)
                   when (User.current.sale_leader? || User.current.sale_manager? || User.current.director? || User.current.marketing? || User.current.sale_admin? || User.current.customer_service?)
                     %w(pending interest meet refundable_deposit lock deposit contract_signed canceled request_recall)
                   else
                     []
                   end
                 end
    all_states = %w(pending interest meet refundable_deposit lock deposit contract_signed canceled request_recall) if User.current.super_admin?
    all_states.push('uninterested') if self.state == 'pending'
    all_states.map do |next_state|
      states.push([I18n.t("deal.states.#{next_state}"), next_state.to_s])
    end
    states.push([I18n.t("deal.states.#{state}"), state.to_s, 'data-valid': true]) unless states.any? { |e| e.include? state }
    return states.uniq
  end

  def available_tasks
    self.tasks.where(result: nil)
  end

  def showed_task
    return nil unless ((pending? || request_recall? || interest?) && assignee_id && company_source?)
    if pending? || request_recall?
      available_tasks&.last
    elsif interest?
      available_tasks.where(code: %w(appointment update_interest))&.last
    end
  end

  def suggestive_display_name
    self.suggestive_name.present? ? "(#{self.suggestive_name})" : ''
  end

  def project_disabled?
    Deal.find_by(id: self.id)&.project_id.present?
  end

  def customer_disabled?
    Deal.find_by(id: self.id)&.customer_id.present?
  end

  def display_label_class(label)
    case label
    when 'Đã liên hệ', 'Đã hẹn gặp'
      'status-success'
    when 'Thuê bao'
      'status-danger'
    when 'Chưa liên hệ', 'Máy bận/ Không nghe máy'
      'status-warning'
    when 'Chưa có số điện thoại'
      'status-no-phone'
    end
  end

  def days_in_state
    exist_days = 0
    if self.deal_log.present?
      if self.deal_log.send("#{self.state}_reentered").present?
        time_enter = self.deal_log.send("#{self.state}_reentered")
      elsif self.deal_log.send("#{self.state}_entered").present?
        time_enter = self.deal_log.send("#{self.state}_entered")
      else
        time_enter = nil
      end
    end
    if time_enter.present? && !%w(uninterested contract_signed canceled).include?(self.state) && self.valid?
      exist_days = ((Time.now - time_enter) / 1.day).to_i
    end
    exist_days
  end

  def get_label_assignee
    time_exist = ((Time.now - self .created_at)/1.day).to_i
    case
    when time_exist < 7
      'assign-deal-day'
    when time_exist < 30
      'assign-deal-week'
    when time_exist < 365
      'assign-deal-month'
    else
      'assign-deal-year'
    end
  end

  def get_time_exist_assignee
    time_exist = ((Time.now - self .created_at)/1.day).to_i
    case
    when time_exist < 1
      ['assign-deal-label-day', "< 1 ngày"]
    when time_exist < 7
      ['assign-deal-label-day', "#{time_exist} ngày"]
    when time_exist < 30 && time_exist % 7 == 0
      ['assign-deal-label-week', "#{time_exist/7} tuần"]
    when time_exist < 30 && time_exist % 7 != 0
      ['assign-deal-label-week', "#{time_exist} ngày"]
    when time_exist < 365 && time_exist % 30 == 0
      ['assign-deal-label-month', "#{time_exist/30} tháng"]
    when time_exist < 365 && time_exist % 30 != 0
      ['assign-deal-label-month', "#{time_exist} ngày"]
    when time_exist >= 365 && time_exist % 365 == 0
      ['assign-deal-label-year', "#{time_exist/365} năm"]
    else time_exist >= 365&& time_exist % 365 != 0
      ['assign-deal-label-year', "#{time_exist} ngày"]
    end
  end

  def state_disabled?
    !self.source
  end

  def contact_status_disabled?
    !self.pending? && !self.interest?
  end

  def have_an_appointment?
    self.interaction_status == 2
  end

  def external_project_disabled?
    Deal.find_by(id: self.id).is_external_project && !(User.current.super_admin? || User.current.bi_ba?)
  end

  def display_labels
    self.labels || []
  end

  def get_applicable_condition_voucher
    if self.state == 'canceled'
      return nil
    end
    total_price = self.total_price.present? ? self.total_price.to_f / 1000000000 : 0
    applicable_condition_voucher = [7]
    case true
    when total_price == 3
      return applicable_condition_voucher + [0, 1]
    when total_price == 5
      return applicable_condition_voucher + [1, 2]
    when total_price == 7
      return applicable_condition_voucher + [2, 3]
    when total_price == 10
      return applicable_condition_voucher + [3, 4]
    when total_price == 15
      return applicable_condition_voucher + [4, 5]
    when total_price == 20
      return applicable_condition_voucher + [5, 6]
    when (1..3).include?(total_price)
      return applicable_condition_voucher << 0
    when (3..5).include?(total_price)
      return applicable_condition_voucher << 1
    when (5..7).include?(total_price)
      return applicable_condition_voucher << 2
    when (7..10).include?(total_price)
      return applicable_condition_voucher << 3
    when (10..15).include?(total_price)
      return applicable_condition_voucher << 4
    when (15..20).include?(total_price)
      return applicable_condition_voucher << 5
    when total_price >= 20
      return applicable_condition_voucher << 6
    else
      return applicable_condition_voucher
    end
    applicable_condition_voucher
  end

  def is_show_commission
    !User.current.marketing? && %w(contract_signed).include?(self.state)
  end

  def commission_disabled?
    self.state != 'contract_signed' || !User.current.sale_admin?
  end

  def label_required(attr_field)
    case self.state
    when 'pending'
      return 'label-required' if User.current.is_sale? && %w(project_id customer_id).include?(attr_field)
    when 'interest'
      return 'label-required' if User.current.is_sale? && %w(project_id customer_id gender product_type contact_status ).include?(attr_field)
    when 'meet'
      return 'label-required' if User.current.is_sale? && %w(project_id customer_id gender product_type contact_status purchase_purpose).include?(attr_field)
    when 'refundable_deposit'
      return 'label-required' if User.current.is_sale? && %w(project_id customer_id gender product_type contact_status purchase_purpose).include?(attr_field)
      return 'label-required' if User.current.sale_admin? && %w(settlements city_id district_id ward_id identity_card domicile dob nationality).include?(attr_field)
    when 'lock'
      return 'label-required' if User.current.is_sale? && %w(project_id customer_id gender product_type contact_status purchase_purpose product_id).include?(attr_field)
      return 'label-required' if User.current.sale_admin? && %w(settlements city_id district_id ward_id identity_card domicile dob nationality).include?(attr_field)
    when 'deposit'
      return 'label-required' if User.current.is_sale? && %w(project_id customer_id gender product_type contact_status purchase_purpose product_id email).include?(attr_field)
      return 'label-required' if User.current.sale_admin? && %w(settlements city_id district_id ward_id identity_card domicile dob nationality).include?(attr_field)
    when 'contract_signed'
      return 'label-required' if User.current.is_sale? && %w(project_id customer_id gender product_type contact_status purchase_purpose product_id email).include?(attr_field)
      return 'label-required' if User.current.sale_admin? && %w(settlements city_id district_id ward_id identity_card domicile dob nationality total_price contract_signed_at).include?(attr_field)
    end
  end
end

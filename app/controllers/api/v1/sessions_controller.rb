module Api::V1
  class SessionsController < ::DeviseTokenAuth::SessionsController

    def create
      field = (resource_params.keys.map(&:to_sym) & resource_class.authentication_keys).first
      @resource = nil
      if field
        q_value = resource_params[field].downcase.strip
        if q_value =~ Devise.email_regexp
          q = "#{field.to_s} = ? AND provider='email'"
        else
          q_value = Phonelib.parse(q_value, 'VN').full_e164.sub('+', '')
          q = "#{field.to_s} = ? AND provider='phone_number'"
        end

        if ActiveRecord::Base.connection.adapter_name.downcase.starts_with? "mysql"
          q = "BINARY " + q
        end

        # LOG IN BY EMAIL AND phone_number
        if field == :username
          @resource = resource_class.where("email = ? OR phone_number = ?", q_value, q_value).first
        else
          @resource = resource_class.where(q, q_value).first
        end
        # LOG IN BY EMAIL AND phone_number END
      end

      if @resource && valid_params?(field, q_value) && (!@resource.respond_to?(:active_for_authentication?) || @resource.active_for_authentication?)
        valid_password = @resource.valid_password?(resource_params[:password])
        if (@resource.respond_to?(:valid_for_authentication?) && !@resource.valid_for_authentication? { valid_password }) || !valid_password
          render_create_error_bad_credentials
          return
        end
        # create client id
        DeviceToken.where(device_token: request.headers['fcm-token']).destroy_all
        @token = @resource.create_token
        @resource.device_tokens.build(
          device_token: request.headers['fcm-token']
        ) if request.headers['fcm-token']
        @resource.save(validate: false)
        sign_in(:user, @resource, store: false, bypass: false)

        yield @resource if block_given?

        render json: {
          data: {
            "userLogin": {
              "authenticatable": {
                "id": @resource.id,
                "email": @resource.email,
                "fullName": @resource.full_name,
                "gender": @resource.gender,
                "code": @resource.code,
                "phoneNumber": Phonelib.parse(@resource.phone_number, @resource.country_code).national&.delete(' '),
                "accountType": @resource.account_type,
                "role": @resource&.role&.name,
                "avatar": @resource&.avatar&.url,
                "cityId": @resource&.city_id,
                "mainJob": @resource&.main_job,
                "referralCode": @resource&.referral_code
              },
              "credentials": {
                "accessToken": @token.token,
                "client": @token.client,
                "expiry": @token.expiry,
                "tokenType": "Bearer",
                "uid": @resource.uid
              }
            }
          }
        }
      elsif @resource && !(!@resource.respond_to?(:active_for_authentication?) || @resource.active_for_authentication?)
        render_create_error_not_confirmed
      else
        render_create_error_bad_credentials
      end
    end

    def destroy
      # remove auth instance variables so that after_action does not run
      user = remove_instance_variable(:@resource) if @resource
      client = @token.client
      @token.clear!

      if user && client && user.tokens[client]
        user.tokens.delete(client)
        user.save(validate: false)

        if DeviseTokenAuth.cookie_enabled
          # If a cookie is set with a domain specified then it must be deleted with that domain specified
          # See https://api.rubyonrails.org/classes/ActionDispatch/Cookies.html
          cookies.delete(DeviseTokenAuth.cookie_name, domain: DeviseTokenAuth.cookie_attributes[:domain])
        end

        if request.headers['fcm-token']
          DeviceToken.where(device_token: request.headers['fcm-token']).destroy_all
        end

        yield user if block_given?

        render_destroy_success
      else
        render_destroy_error
      end
    end
  end
end

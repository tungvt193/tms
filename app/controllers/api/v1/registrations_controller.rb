module Api::V1
  class RegistrationsController < ::DeviseTokenAuth::RegistrationsController
    skip_before_action :validate_sign_up_params, only: :create
    before_action :set_referrer, only: :create

    def create
      ActiveRecord::Base.transaction do
        phone_number = Phonelib.parse(user_params[:phone_number], 'VN').full_e164.sub('+', '')
        otp = Otp.confirmed.where(phone_number: phone_number).first
        unless otp.present?
          return render json: {
            data: {
              error: ["Số điện thoại chưa được xác thực"],
              user: nil
            }
          }
        end
        user = User.new user_params
        user.phone_number = phone_number
        user.country_code = 'VN'
        user.account_type = :agent
        user.role = Role.find_by_name('Cộng tác viên')
        user.uid = phone_number
        user.provider = 'phone_number'
        user.skip_gender_validation = true
        user.phone_verify = true
        if user.save
          otp.destroy
          @referrer.create_new_referral!(user) if @referrer.present?
          
          render json: {
            data: {
              error: [],
              user: user
            }
          }, status: 200
        else
          render json: {
            data: {
              error: user.errors.full_messages,
              user: nil
            }
          }
        end
      end
    end

    private

    def user_params
      params.require(:user).permit(:phone_number, :password, :password_confirmation, :full_name, :country_code)
    end

    def referral_params
      params.require(:user).permit(:referral_code)
    end

    def set_referrer
      return unless referral_params[:referral_code].present?
      @referrer = User.find_by(referral_code: referral_params[:referral_code])
      unless @referrer
        return render json: {
          data: {
            error: ["Mã giới thiệu không tồn tại"],
            user: nil
          }
        }
      end
    end
  end
end

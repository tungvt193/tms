module Api::V1
  class SyncController < Api::V1::ApiController

    def users
      SyncDataToGoogleSheetJob.perform_later('User')
    end

    def projects
      SyncDataToGoogleSheetJob.perform_later('Project')
    end

    def products
      SyncDataToGoogleSheetJob.perform_later('Product')
    end

    def deals
      SyncDataToGoogleSheetJob.perform_later('Deal')
    end
  end
end
module Api::V1
  class ProductLockHistoriesController < Api::V1::ApiController
    before_action :authenticate_user!, only: [:upload_unc]

    def upload_unc
      lock_history = ProductLockHistory.find(params[:id])
      return render_json('422',['Cập nhật UNC không thành công'], nil, nil) unless lock_history.deposit_confirmation?
      if lock_history_params[:unc_image].present? && lock_history.update(lock_history_params)
        render_json('200', [], lock_history.unc_image.url, lock_history.unc_image_identifier)
        lock_history.update_column(:unc_image, nil)
      else
        render_json('422', lock_history.errors.full_messages, nil, nil)
      end
    end

    private

    def lock_history_params
      params.require(:product_lock_history).permit(:unc_image)
    end

    def render_json(code, error, url, file_name)
      render json: {
        data: {
          code: code,
          error: error,
          unc_image_url: url,
          unc_image_file_name: file_name
        }
      }
    end
  end
end

module Api::V1
  class PasswordsController < ::DeviseTokenAuth::PasswordsController

    def create
      return render_create_error_missing_username unless resource_params[:username].present?
      @redirect_url = "#{ENV['TMS_URL']}/api/v1/auth/sign_in"
      field = (resource_params.keys.map(&:to_sym) & resource_class.authentication_keys).first
      value = resource_params[field].strip
      is_email = value =~ URI::MailTo::EMAIL_REGEXP
      if field
        value.downcase! if is_email.present?
        value = Phonelib.parse(value, 'VN').full_e164.sub('+', '') unless is_email.present?
        @resource = resource_class.where("email = ? OR phone_number = ?", value, value).first
      end

      if @resource
        token = @resource.send_password_reset
        yield @resource if block_given?
        if is_email
          @email = @resource.email
          UserMailer.retizy_reset_password_instructions(@resource, token, @redirect_url, 'default').deliver
          if @resource.errors.empty?
            return render_result(true, [], token)
          else
            return render_result(false, @resource.errors, nil)
          end
        else
          otp = Otp.find_or_initialize_by(phone_number: value, otp_type: 'forgot_password')
          if !otp.new_record? && Time.current >= otp.expiry
            otp.expiry = Time.current + 180.seconds
            otp.otp = otp.generate_otp
          end
          otp.status = :open
          if otp.save
            sms_service = SendSmsService.new(otp)
            sms_status = sms_service.send_sms
            if sms_status == 0
              return render_result(true, [], token)
            else
              return render_result(false, ["Không thể gửi mã xác thực"], nil)
            end
          else
            return render_result(false, otp.errors.full_messages, nil)
          end
        end
      else
        render_not_found_error
      end
    end

    def update
      # make sure user is authorized
      if request.headers['reset-password-token']
        @resource = resource_class.with_reset_password_token(request.headers['reset-password-token'])
        return render_update_error_unauthorized unless @resource
        @token = @resource.create_token
      else
        @resource = set_user_by_token
      end

      return render_update_error_unauthorized unless @resource

      # ensure that password params were sent
      unless password_resource_params[:password] && password_resource_params[:password_confirmation]
        return render_update_error_missing_password
      end

      @resource.skip_gender_validation = true
      if @resource.send(resource_update_method, password_resource_params)
        @resource.allow_password_change = false if recoverable_enabled?
        @resource.save!

        yield @resource if block_given?
        if request.headers['phone-number'].present?
          phone_number = Phonelib.parse(request.headers['phone-number'], 'VN').full_e164.sub('+', '')
          otp = Otp.confirmed.where(phone_number: phone_number, otp_type: 'forgot_password').first
          otp.destroy
        end
        return render_update_success
      else
        return render_update_error
      end
    end

    private

    def resource_params
      params.permit(:username)
    end

    def password_resource_params
      params.permit(:password, :password_confirmation)
    end

    protected

    def validate_redirect_url_param
    end

    def render_create_error_missing_username
      render_error(401, 'Vui lòng nhập email hoặc số điện thoại để khôi phục mật khẩu.')
    end

    def render_not_found_error
      render json: {
        success: false,
        errors: ['Email hoặc số điện thoại không đúng.']
      }
    end

    def render_result(success, error, token)
      render json: {
        data: {
          success: success,
          errors: error,
          token: token
        }
      }
    end
  end
end

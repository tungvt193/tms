module Api::V1
  class VersionsController < Api::V1::ApiController

    def check_version
      if ::Version.first.version.delete!(".").to_i <= params[:version].delete!(".").to_i
        render json: { status: "latest" }
      else
        render json: { status: "outdate" }
      end
    end
  end
end
module Api::V1
  class UsersController < Api::V1::ApiController
    before_action :authenticate_user!, only: [:update_avatar]

    def update_avatar
      current_user.skip_gender_validation = true
      if current_user.update(user_params)
        render json: {
          data: {
            error: [],
            avatar_url: current_user.avatar.url
          }
        }, status: 200
      else
        render json: {
          data: {
            error: current_user.errors.full_messages,
            avatar_url: nil
          }
        }
      end
    end

    def register_phone_number
      phone_number = Phonelib.parse(otp_params[:phone_number], 'VN').full_e164.sub('+', '')
      otp = Otp.find_or_initialize_by(phone_number: phone_number)
      if !otp.new_record? && Time.current >= otp.expiry
        otp.expiry = Time.current + 180.seconds
        otp.otp = otp.generate_otp
      end
      otp.status = :open
      if otp.save
        sms_service = SendSmsService.new(otp)
        sms_status = sms_service.send_sms
        if sms_status == 0
          render_result([], otp)
        else
          render_result(["Không thể gửi mã xác thực"], nil)
        end

      else
        render_result(otp.errors.full_messages, nil)
      end
    end

    def verify_phone_number
      otp = Otp.find_by(otp: otp_params[:otp])
      return render_result(["Mã xác thực không chính xác! Vui lòng nhập lại."], nil) unless otp.present?
      if Time.current > otp.expiry
        render_result(["Mã xác thực đã hết hạn. Vui lòng bấm Gửi lại mã"], otp)
      else
        otp.update(status: :confirmed)
        render_result([], otp)
      end
    end

    def refresh_token
      status = false
      if (uid = request.headers['uid']).present? && (client = request.headers['client']).present?
        if (user = User.find_by(uid: uid)).present?
          if user.tokens[client].present? && Time.at(user.tokens[client]['expiry']).to_datetime < Time.zone.now
            now = Time.zone.now
            token = create_token(
              client: client,
              last_token: tokens.fetch(client, {})['token'],
              updated_at: now
            )
            user.clean_old_tokens
            user.save(validate: false)
            status = true
          end
        end
      end
      return render json: {
        data: {
          success: false,
          errors: "Access Token không hợp lệ",
          credentials: nil
        }
      } unless status

      render json: {
        data: {
          success: true,
          errors: nil,
          credentials: {
            "accessToken": token['access-token'],
            "client": token['client'],
            "expiry": token['expiry'],
            "tokenType": "Bearer",
            "uid": user.uid
          }
        }
      }
    end

    def find_referrer
      user = User.find_by(referral_code: params[:referral_code])
      error = user ? nil : "Không tìm thấy người giới thiệu"
      render json: {
        data: {
          error: error,
          user: user&.full_name
        }
      }
    end

    def change_password
      password_recent = BCrypt::Password.new(current_user.encrypted_password)
      old_password = params[:old_password]
      new_password = user_password_params[:password]
      new_password_confirmation = user_password_params[:password_confirmation]
      if new_password.present? && new_password_confirmation.present? && password_recent == old_password && old_password != new_password &&  new_password == new_password_confirmation && current_user.update(user_password_params)
        return render json: { 
          success: true,
          message: "Thay đổi mật khẩu thành công"
        }  
      else 
        return render_update_error_old_password if password_recent != old_password
        return render_update_error_new_password_confirmation if new_password != new_password_confirmation  
        return render_update_error_new_password if old_password == new_password
        return render_update_error_too_short if current_user.errors.present?
      end
    end

    private

    def render_result(error, otp)
      render json: {
        data: {
          error: error,
          otp: otp
        }
      }
    end

    def otp_params
      params.require(:user).permit(:phone_number, :otp)
    end

    def user_params
      params.require(:user).permit(:avatar)
    end

    def user_password_params
      params.require(:user).permit(:password, :password_confirmation)
    end
    
    def render_update_error_old_password
      render json: { 
        success: false,
        errors: 'Mật khẩu hiện tại chưa đúng'
      }  
    end

    def render_update_error_new_password
      render json: { 
        success: false,
        errors: 'Mật khẩu mới phải khác mật khẩu hiện tại'
      }  
    end

    def render_update_error_new_password_confirmation
      render json: { 
        success: false,
        errors: 'Mật khẩu không trùng nhau'
      }  
    end

    def render_update_error_too_short
      render json: { 
          success: false,
          errors: 'Mật khẩu phải có tối thiểu 6 ký tự'
      }
    end
  end
end

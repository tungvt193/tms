# frozen_string_literal: true

class Users::SessionsController < Devise::SessionsController
  # after_action :sign_in_message, only: :create
  layout 'devise'
  # before_action :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  # def new
  #   super
  # end

  # POST /resource/sign_in
  def create
    field = (resource_params.keys.map(&:to_sym) & resource_class.authentication_keys).first
    q_value = resource_params[field].downcase.strip
    unless q_value =~ Devise.email_regexp
      q_value = Phonelib.parse(q_value, 'VN').full_e164.sub('+', '')
    end
    user = resource_class.where("email = ? OR phone_number = ?", q_value, q_value).first
    url = ''
    url = 'retizy-inhouse' if user.is_sale?
    url = 'referral-user'if user.agent?
    if user.is_sale? || user.agent?
      redirect_to "#{ENV['WEBSITE_URL']}/#{url}"
    else
      super
    end
  end

  # DELETE /resource/sign_out
  def destroy
    current_user.update_columns({auth_key: nil, endpoint: nil, p256dh_key: nil})
    super
  end

  private

  def sign_in_message
    flash.delete(:notice)
  end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
end

class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def google_oauth2
    auth = request.env['omniauth.auth']
    @user = User.from_omniauth(auth)

    @user.access_token = auth.credentials.token
    @user.expires_at = auth.credentials.expires_at
    @user.refresh_token = auth.credentials.refresh_token
    @user.save(validate: false)

    if @user.persisted?
      flash[:notice] = I18n.t 'devise.omniauth_callbacks.success', kind: 'Google'
      # sign_in_and_redirect @user, event: :authentication
      redirect_to profile_admin_users_path(popup: 'google_calendar')
    else
      session['devise.google_data'] = request.env['omniauth.auth'].except('extra') # Removing extra as it can overflow some session stores
      redirect_to new_user_registration_url, alert: @user.errors.full_messages.join("\n")
    end
  end
  
  def failure
    redirect_to admin_root_path(popup: 'google_calendar')
  end
end
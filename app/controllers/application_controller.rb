class ApplicationController < ActionController::Base
  protect_from_forgery prepend: true

  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit :sign_up, keys: [:phone_number, :country_code, :password, :password_confirmation]
    devise_parameter_sanitizer.permit :sign_in, keys: [:username, :password]
  end

  def after_sign_in_path_for(resource)
    session[:refresh_subscribe] = 'true' if resource.subscribe
    if resource.customer_service?
      admin_cs_tickets_path
    else
      admin_root_path
    end
  end
end

class Admin::KpisController < Admin::BaseController
  before_action :load_kpi, only: [:show, :new, :create, :edit, :update, :destroy]

  def index
    authorize :kpi, :index?
    team_ids = Team.where(user_id: current_user.root.descendant_ids).pluck :id
    @kpis = Kpi.where(team_id: team_ids).order(year: :desc, id: :desc)
  end

  def show
    authorize @kpi
  end

  def new
    @kpi = Kpi.new
    team_ids = Kpi.where(year: Time.zone.now.year).pluck :team_id
    @teams = @teams.select {|team| !team_ids.include?(team.second)}

    authorize @kpi
    @is_disabled = !policy(@kpi).create?
  end

  def get_teams
    team_ids = Kpi.where(year: params[:year]).pluck :team_id
    respond_to do |format|
      format.json { render json: Team.for_kpi.select {|team| !team_ids.include?(team.id)}.map { |e| {name: e.name, id: e.id} } }
    end
  end

  def edit
    authorize @kpi
    @is_disabled = !policy(@kpi).update?
    @readonly = true
  end

  def create
    @kpi = Kpi.new kpi_params
    authorize @kpi
    team_ids = Kpi.where(year: @kpi.year).pluck :team_id
    @teams = @teams.select {|team| !team_ids.include?(team.second)}

    if @kpi.valid?
      @kpi.save
      redirect_to edit_admin_kpi_path(@kpi.id), notice: 'KPI đã được tạo thành công.'
    else
      render :new
    end
  end

  def update
    authorize @kpi
    @kpi.assign_attributes kpi_params

    if @kpi.valid?
      @kpi.save
      redirect_to edit_admin_kpi_path(@kpi.id), notice: 'KPI đã được chỉnh sửa thành công.'
    else
      render :edit
    end
  end

  def destroy
    authorize @kpi
    @kpi.destroy
    redirect_to admin_kpis_url, notice: 'Xóa thành công.'
  end

  private
  def load_kpi
    @kpi = Kpi.find_by id: params[:id]
    @teams = Team.for_kpi.map { |e| [e.name, e.id] }
    if params[:id] && @kpi.nil?
      redirect_to admin_kpis_url
    end
  end

  def kpi_params
    params.require(:kpi).permit Kpi::ATTRS
  end
end

class Admin::CustomerTicketsController < Admin::BaseController
  before_action :set_customer_ticket, only: [:edit, :update, :destroy]

  def index
    authorize :customer_ticket, :index?
    query = params[:query].presence || "*"
    @customer_tickets = CustomerTicket.search_result(query).order(created_at: :desc)
    @customer_tickets = @customer_tickets
  end

  def autocomplete
    render json: CustomerTicket.all.autocomplete_result(params[:query])
  end

  def edit
    if policy(@customer_ticket).change_state? || policy(@customer_ticket).update? || policy(@customer_ticket).show?
      @is_disabled = !policy(@customer_ticket).update?
      @state_is_disable = !policy(@customer_ticket).change_state?
    else
      respond_to do |format|
        flash[:error] = 'Bạn không có quyền thực hiện hành động này.'
        format.html { redirect_to admin_customer_tickets_path }
      end
    end
  end

  def update
    respond_to do |format|
      if policy(@customer_ticket).change_state? || policy(@customer_ticket).update?
        if @customer_ticket.update_attributes customer_ticket_params
          format.html { redirect_to edit_admin_customer_ticket_path(@customer_ticket.id), notice: 'Đăng ký tư vấn đã được chỉnh sửa thành công.' }
        else
          format.html { render :edit, error: 'Đã xảy ra lỗi. Vui lòng kiểm tra lại.' }
        end
      else
        flash[:error] = 'Bạn không có quyền thực hiện hành động này.'
        format.html { redirect_to edit_admin_customer_ticket_path(@customer_ticket.id) }
      end
    end
  end

  def destroy
    authorize @customer_ticket
    @customer_ticket.destroy
    respond_to do |format|
      format.html { redirect_to admin_customer_tickets_url, notice: 'Xóa thành công.' }
    end
  end

  private
  def set_customer_ticket
    @customer_ticket = CustomerTicket.find params[:id]
    @cities = RegionData.cities.pluck(:name_with_type, :id)
    @projects = Project.pluck(:name, :id)
    @products = Product.all
  end

  def customer_ticket_params
    if policy(@customer_ticket).change_state?
      params.require(:customer_ticket).permit(:state)
    elsif policy(@customer_ticket).update?
      params.require(:customer_ticket).permit(:name, :phone, :gender, :email, :city_id, :address,
                                            :message, :objectable_id, :objectable_type, :ticket_type, :source,
                                            notes_attributes: [:id, :created_by_id, :content, :_destroy])
    end
  end
end

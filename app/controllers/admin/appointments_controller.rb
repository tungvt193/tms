class Admin::AppointmentsController < Admin::BaseController
  before_action :load_appointment, only: [:update, :create, :destroy, :cancel]

  def create
    respond_to do |format|
      @appointment.save if @appointment.valid?
      format.js { render layout: false }
    end
  end

  def update
    respond_to do |format|
      @appointment.save if !@appointment.canceled && @appointment.valid?
      format.js { render layout: false }
    end
  end

  def destroy
    respond_to do |format|
      @appointment.destroy
      format.js { render layout: false }
    end
  end

  private

  def load_appointment
    @deal = Deal.find_by(id: params[:deal_id])
    @appointment = params[:id].present? ? Appointment.find_by(id: params[:id]) : @deal.appointments.build
    @projects = Project.all.pluck :name, :id
    @customer = Customer.new
    if @deal
      @call = Call.new(deal_id: @deal.id)
      @feedback = Feedback.new(deal_id: @deal.id)
    end
    @appointment.assign_attributes appointment_params
    if params[:cancel] == 'true'
      @appointment.reload
      @appointment.canceled = true
    end
    @appointment.pseudo_id = params[:appointment][:pseudo_id]
  end

  def appointment_params
    params.require(:appointment).permit(:deal_id, :created_by_id, :content, :appointment_date, :result, :state, :flag)
  end
end

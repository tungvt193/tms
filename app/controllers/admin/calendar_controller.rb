class Admin::CalendarController < Admin::BaseController
  def index
    @event_count = current_user.appointments.where('appointment_date BETWEEN ? AND ?', DateTime.now.beginning_of_day, DateTime.now.end_of_day).size
    respond_to do |format|
      if browser.device.mobile?
        @group_events = current_user.appointments.where('appointment_date BETWEEN ? AND ?', DateTime.now.beginning_of_week, DateTime.now.end_of_week).order(:appointment_date).group_by(&:group_by_date)
        format.html { render 'admin/calendar/mobile/index', layout: 'deal_forms' }
      else
        format.html { }
      end
    end
  end

  def get_events
    events =  current_user.appointments.where('appointment_date BETWEEN ? AND ?', DateTime.now.beginning_of_week, DateTime.now.end_of_week).map do |e|
                title = "<div class='custom-event-time'> #{e.appointment_date.strftime("%H:%M")} </div><div class='custom-event-title'> #{e.content} </div><div class='custom-event-content'> #{e.result} </div>"
                event_class = case e.state
                              when nil
                                if DateTime.now < e.appointment_date
                                  'custom-event future-meet'
                                else
                                  'custom-event not-meet'
                                end
                              when 1
                                'custom-event cancel-meet'
                              when 0
                                'custom-event meet'
                              end
                { id: e.id,
                  title: title,
                  start: e.appointment_date.strftime("%Y-%m-%dT%H:%M:%S"),
                  allDay: false,
                  url: edit_admin_deal_path(e.deal),
                  backgroundColor: '#fff',
                  className: event_class
                }
              end
    render json: events
  end
end
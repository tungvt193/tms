class Admin::FeedbacksController < Admin::BaseController
  before_action :load_feedback, only: [:update, :create, :destroy]

  def create
    @feedback.assign_attributes feedback_params
    respond_to do |format|
      if @feedback.valid?
        @feedback.save
        @not_errors = true
        format.js {render layout: false}
      else
        @not_errors = false
        format.js {render layout: false}
      end
    end
  end

  def update
    @feedback.assign_attributes feedback_params
    is_canceled = params[:cancel] == 'true'
    @feedback.reload if is_canceled
    respond_to do |format|
      if @feedback.valid?
        @feedback.save unless is_canceled
        @not_errors = true
        format.js {render layout: false}
      else
        @not_errors = false
        format.js {render layout: false}
      end
    end
  end

  def destroy
    respond_to do |format|
      @feedback.destroy
      format.js {render layout: false}
    end
  end

  private
  def load_feedback
    @deal = Deal.find_by( id: params[:deal_id])
    @feedback = if params[:id]
                    Feedback.find_by(id: params[:id])
                  else
                    @deal.feedbacks.build
                  end
  end

  def feedback_params
    params.require(:feedback).permit(:deal_id, :created_by_id, :title, :content)
  end
end

class Admin::CsTicketsController < Admin::BaseController
  before_action :set_cs_ticket, only: [:update]

  def index
    authorize :cs_ticket, :index?
    params[:state] ||= 'deposit'
    @deals = Deal.includes(:project, :customer).where(state: params[:state]).order("deals.cs_state ASC, deals.updated_at DESC")
    @deals_count = [Deal.deposit.size, Deal.contract_signed.size, Deal.canceled.size]
    case params[:state]
    when 'deposit'
      @deals_count[0] = Deal.search_result(params).deposit.size
    when 'contract_signed'
      @deals_count[1] = Deal.search_result(params).contract_signed.size
    when 'canceled'
      @deals_count[2] = Deal.search_result(params).canceled.size
    end
    @projects = [['Khác', 0]] + @deals.map { |d| d.project }.compact.uniq.sort_by(&:name).pluck(:name, :id)
    @assignee = User.all.sort_by(&:full_name).pluck(:full_name, :id)
    @deals = @deals.search_result(params)
    @check_params_filter = params && (params[:assignee])
  end

  def create
    deal = Deal.find(params[:cs_ticket][:deal_id])
    @cs_ticket = CsTicket.new(cs_ticket_params)
    respond_to do |format|
      if @cs_ticket.save
        if params[:cs_ticket][:voucher_object_id].present?
          VouchersCustomers.find(params[:cs_ticket][:voucher_object_id]).update(cs_ticket_id: @cs_ticket.id)
        end
        if params[:cs_ticket][:customer_opinion] == '1' && deal.cs_tickets.pluck(:customer_opinion).select{ |ce| ce == 2 }.size == 0
          deal.update_column(:cs_state, 2)
        else
          deal.update_column(:cs_state, 3)
        end
        format.html { redirect_to edit_admin_deal_path(params[:cs_ticket][:deal_id], form: 'cs_tickets', return: 'cs_tickets', latest_created: @cs_ticket.created_at), notice: 'Phiếu CSKH đã được tạo thành công.' }
      end
    end
  end

  def update
    if params[:reload]
      @deal = Deal.find(params[:deal_id])
      @vouchers = @deal.project.vouchers.pluck(:content, :id)
      deposit_tickets = @deal.cs_tickets.where(deal_state: 'deposit').order(created_at: :desc)
      contract_signed_tickets = @deal.cs_tickets.where(deal_state: 'contract_signed').order(created_at: :desc)
      canceled_tickets = @deal.cs_tickets.where(deal_state: 'canceled').order(created_at: :desc)
      @cs_tickets_groups = {'deposit' => deposit_tickets, 'contract_signed' => contract_signed_tickets, 'canceled' => canceled_tickets}
    end
    authorize @cs_ticket
    respond_to do |format|
      format.js
      unless params[:reload]
        if @cs_ticket.update(cs_ticket_params)
          if params[:cs_ticket][:voucher_object_id].present?
            VouchersCustomers.find(params[:cs_ticket][:voucher_object_id]).update(cs_ticket_id: @cs_ticket.id)
          end
          @is_edit_cs_ticket = true
          format.html { redirect_to edit_admin_deal_path(params[:cs_ticket][:deal_id], form: 'cs_tickets', return: 'cs_tickets', after_update: true, updated_at: @cs_ticket.updated_at), notice: 'Cập nhật phiếu thành công.'}
        end
      end
    end
  end

  def autocomplete
    render json: policy_scope(Deal).autocomplete_result(params[:query])
  end

  def download_csv
    authorize :cs_ticket, :index?
    CsTicketJob.perform_later params[:state], current_user
    render json: {message: "File Download sẽ được gửi vào email của bạn."}
  end

  private

  def set_cs_ticket
    unless params[:reload]
      params[:id] = params[:cs_ticket][:cs_ticket_id]
    end
    @cs_ticket = CsTicket.find(params[:id])
  end

  def cs_ticket_params
    params[:cs_ticket][:voucher_id] = params[:cs_ticket][:selected_voucher_id]
    if params[:cs_ticket][:voucher_object_id] == ''
      params[:cs_ticket][:voucher_id] = nil
    end
    if params[:cs_ticket][:customer_opinion] == '2'
      params[:cs_ticket].each do |cs_ticket_field|
        params[:cs_ticket][cs_ticket_field.first.to_sym] = nil if %w(support_level processing_speed sale_rating sale_consulting_quality deal_rating voucher_id cancel_reason demand_with_deal extra_support_for_customer note).include?(cs_ticket_field.first)
      end
    end
    params.require(:cs_ticket).permit(:support_level, :processing_speed, :sale_rating, :sale_consulting_quality, :deal_rating, :voucher_id,
                                      :cancel_reason, :demand_with_deal, :extra_support_for_customer, :deal_id, :created_by_id, :deal_state, :note, :customer_opinion)
  end
end
class Admin::OnlineFeedbacksController < Admin::BaseController
  before_action :load_online_feedback, only: [:update, :create, :destroy]

  def create
    @online_feedback.assign_attributes online_feedback_params
    respond_to do |format|
      if @online_feedback.valid?
        @online_feedback.save
        @not_errors = true
        format.js {render layout: false}
      else
        @not_errors = false
        format.js {render layout: false}
      end
    end
  end

  def update
    @online_feedback.assign_attributes online_feedback_params
    is_canceled = params[:cancel] == 'true'
    @online_feedback.reload if is_canceled
    respond_to do |format|
      if @online_feedback.valid?
        @online_feedback.save unless is_canceled
        @not_errors = true
        format.js {render layout: false}
      else
        @not_errors = false
        format.js {render layout: false}
      end
    end
  end

  def destroy
    respond_to do |format|
      @online_feedback.destroy
      format.js {render layout: false}
    end
  end

  private
  def load_online_feedback
    @deal = Deal.find_by( id: params[:deal_id])
    @online_feedback = if params[:id]
                    OnlineFeedback.find_by(id: params[:id])
                  else
                    @deal.online_feedbacks.build
                  end
  end

  def online_feedback_params
    params.require(:online_feedback).permit(:deal_id, :created_by_id, :action_date, :action_type, :content)
  end
end

class Admin::CustomHtmlsController < Admin::BaseController
  before_action :load_custom_html, only: [:edit, :update, :create]

  def edit
    authorize @custom_html
    @is_disabled = !policy(@custom_html).update?
  end

  def create
    authorize @custom_html
    @custom_html.assign_attributes custom_html_params
    respond_to do |format|
      if @custom_html.valid?
        @custom_html.save
        format.html { redirect_to edit_admin_custom_html_path(@custom_html.position), notice: 'Sửa thành công!' }
        format.json { render :edit }
      else
        format.html { render :edit }
        format.json { render json: @custom_html.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    authorize @custom_html
    @custom_html.assign_attributes custom_html_params
    respond_to do |format|
      if @custom_html.valid?
        @custom_html.save
        format.html { redirect_to edit_admin_custom_html_path(@custom_html.position), notice: 'Sửa thành công!' }
        format.json { render :edit }
      else
        format.html { render :edit }
        format.json { render json: @custom_html.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  def load_custom_html
    position = params[:position]&.to_i || 0
    position = 0 unless Constant::CUSTOM_HTML_TYPES[position]
    @custom_html = CustomHtml.find_or_initialize_by position: position
  end

  def custom_html_params
    params.require(:custom_html).permit CustomHtml::ATTRS
  end
end

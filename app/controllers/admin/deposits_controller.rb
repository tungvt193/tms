class Admin::DepositsController < Admin::BaseController
  before_action :set_deposit, only: [:edit, :update, :destroy_attachment, :destroy]

  def index
    authorize :deposit, :index?
    @pagy, @deposits = pagy(Deposit.all.order(created_at: :desc), items: Settings.admin.pagination.deposit.per_page)
  end

  def edit
    authorize @deposit
    @is_disabled = !policy(@deposit).update?
  end

  def update
    authorize @deposit
    respond_to do |format|
      @deposit.execute_state_change deposit_params[:state], is_third_holding: (params.require(:deposit)[:is_third_holding] == 'true')
      @deposit.assign_attributes deposit_params
      add_third_holding_note
      if @deposit.valid? && @deposit.product.valid?
        @deposit.save
        @deposit.product.save
        format.html { redirect_to edit_admin_deposit_path(@deposit.id), notice: 'Phiếu cọc đã được chỉnh sửa thành công.' }
      else
        format.html { render :edit, error: 'Đã xảy ra lỗi. Vui lòng kiểm tra lại.' }
        format.json { render json: @deposit.errors, status: :unprocessable_entity }
      end
    end
  end

  def stream_product
    Notification.push_notify_arr params[:noti_ids]
    # deposit = Deposit.find_by id: params[:id]

    # respond_to do |format|
    #   if deposit&.aasm&.current_state == :request_lock
    #     if deposit&.product&.aasm&.current_state == :for_sale
    #       # push_notify
    #       # Notification.push_notify_arr params[:noti_ids]
    #       format.json { render json: deposit }
    #     else
    #       format.json { render json: deposit, status: :unprocessable_entity }
    #     end
    #   else
    #     format.json { render json: deposit, status: :unprocessable_entity }
    #   end
    # end
  end

  def finish_countdown
    @deposit = Deposit.find_by id: params[:id]
    render json: {status: 'error', message: 'Không tồn tại phiếu cọc'} and return unless @deposit
    if @deposit.aasm.current_state == :canceled || @deposit.aasm.current_state != :request_lock
      render json: {status: 'error', message: "Phiếu cọc #{@deposit.name} đã huỷ hoặc không chờ lock"} and return
    end

    @deposit.execute_state_change :canceled, is_time_out: true
    add_locking_time_note
    if @deposit.valid? && @deposit.product.valid?
      @deposit.save
      @deposit.product.save
      render json: {status: 'success', message: "Huỷ thành công phiếu cọc #{@deposit.name}"}
    else
      render json: {status: 'error', message: 'Huỷ không thành công phiếu cọc'}
    end
  end

  def destroy
    authorize @deposit
    @deposit.destroy
    respond_to do |format|
      format.html { redirect_to admin_deposits_url, notice: 'Xóa thành công.' }
      format.json { head :no_content }
    end
  end

  def destroy_attachment
    delete_attachment(params[:type])
  end

  private

  def set_deposit
    @deposit = Deposit.find params[:id]
    @stored_state = @deposit.state
    @customers = Customer.all.pluck :name, :id
    @users = User.all.pluck :full_name, :id
  end

  def delete_attachment type
    @deposit.send("remove_#{type}!")
    if @deposit.valid?
      @deposit.save
      @deposit.reload
      render json: {}
    else
      render json: { error: @deposit.errors[params[:type].to_sym]&.first }
    end
  end

  def add_third_holding_note
    return unless params.require(:deposit)[:is_third_holding] == 'true'
    @deposit.notes.new content: params.require(:deposit)[:third_holding_reason], is_third_holding: true, created_by_id: current_user.id
  end

  def add_locking_time_note
    @deposit.notes.new content: 'Yêu cầu tạo phiếu bị hủy do vượt quá thời gian xử lý.', created_by_id: current_user.id
  end

  def deposit_params
    params.require(:deposit).permit(Deposit::ATTRS)
  end
end

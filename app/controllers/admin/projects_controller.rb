require 'net/http'
include BlogService

class Admin::ProjectsController < Admin::BaseController
  before_action :set_project, only: [:show, :edit, :update, :destroy_attachment, :destroy, :show_products, :information_for_sellers, :update_sale_policy, :lock_rules, :update_lock_rules]
  before_action :get_icdo, except: [:index, :show, :destroy]
  before_action :set_labels, only: :update_sale_policy

  # GET /projects
  # GET /projects.json
  def index
    authorize :project, :index?
    query = params[:query].presence || "*"
    @projects = Project.includes(:deals).search_result(query)
    if browser.device.mobile?
      render 'admin/projects/mobile/index'
    else
      render 'index'
    end
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
    authorize @project
    @project_investors = @project.investors.map { |i| Investor.find(i).name }.join(', ')
    @sale_kits = @project.sale_documents.sale_kit
    @sale_policies = @project.sale_documents.sale_policy
    news_url = "#{ENV['WP_API_POST']}?_embed&per_page=6&tags_exclude=312&filter[tag]=#{@project.slug}"
    videos_url = "#{ENV['WP_API_POST']}?_embed&per_page=3&tags=312&filter[tag]=#{@project.slug}"
    @news_posts = get_posts_from_blog(news_url, 'news')
    @videos_posts = get_posts_from_blog(videos_url, 'video')
    @news_posts_url = defined?("/blog/tag/#{@project.slug}") ? "#{ENV['WP_API_POST'][0, ENV['WP_API_POST'].index('n') + 1]}/blog/tag/#{@project.slug}" : "#"
    @videos_posts_url = defined?("/blog/tag/#{@project.slug}") ? "#{ENV['WP_API_POST'][0, ENV['WP_API_POST'].index('n') + 1]}/blog/tag/video" : "#"
  end

  def search
    redirect_to admin_projects_path(query: params[:query])
  end

  def autocomplete
    render json: Project.autocomplete_result(params[:query])
  end

  def product_autocomplete
    @project = Project.find(params[:id])
    render json: @project&.products.where('state IN (?)', %w(for_sale locking deposited sold)).autocomplete_result(params[:query], level: params[:level]&.to_i)
  end

  # GET /projects/new
  def new
    @district_children = []
    @ward_children = []
    @project = Project.new
    authorize @project
    @is_disabled = !policy(@project).create?
    @project.build_region
    @project.milestones.build(
      [
        { event_time: '', event: 'Khởi công', is_default: true },
        { event_time: '', event: 'Dự kiến bàn giao', is_default: true }
      ]
    )
    @project.legal_documents.build
    @cities = RegionData.cities.pluck(:name_with_type, :id)
  end

  # GET /projects/1/edit
  def edit
    authorize @project
    @is_disabled = !policy(@project).update?
    @cities = RegionData.cities.pluck(:name_with_type, :id)
    customer_city_id = @project.region.present? && @project.region.city.present? ? @project.region.city.id : ''
    customer_district_id = @project.region.present? && @project.region.district.present? ? @project.region.district.id : ''
    @district_children = RegionData.where(parent_id: customer_city_id).order(:name).pluck(:name_with_type, :id)
    @ward_children = RegionData.where(parent_id: customer_district_id).order(:name).pluck(:name_with_type, :id)
  end

  # POST /projects
  # POST /projects.json
  def create
    return render json: { status: :ok } if params[:type].present?
    @cities = RegionData.cities.pluck(:name_with_type, :id)
    @project = Project.new(project_params)
    authorize @project
    respond_to do |format|
      if @project.save
        format.html { redirect_to edit_admin_project_path(@project.id), notice: 'Dự án đã được tạo thành công.' }
        format.json { render :edit, status: :created, location: @project }
      else
        customer_city_id = @project.region.present? && @project.region.city.present? ? @project.region.city.id : ''
        customer_district_id = @project.region.present? && @project.region.district.present? ? @project.region.district.id : ''
        @district_children = RegionData.where(parent_id: customer_city_id).order(:name).pluck(:name_with_type, :id)
        @ward_children = RegionData.where(parent_id: customer_district_id).order(:name).pluck(:name_with_type, :id)
        format.html { render :new }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /projects/1
  # PATCH/PUT /projects/1.json
  def update
    authorize @project
    @cities = RegionData.cities.pluck(:name_with_type, :id)
    respond_to do |format|
      if @project.update(project_params)
        format.html { redirect_to edit_admin_project_path(@project.id), notice: 'Dự án đã được chỉnh sửa thành công.' }
        format.json { render :edit, status: :ok, location: @project }
      else
        customer_city_id = @project.region.present? && @project.region.city.present? ? @project.region.city.id : ''
        customer_district_id = @project.region.present? && @project.region.district.present? ? @project.region.district.id : ''
        @district_children = RegionData.where(parent_id: customer_city_id).order(:name).pluck(:name_with_type, :id)
        @ward_children = RegionData.where(parent_id: customer_district_id).order(:name).pluck(:name_with_type, :id)
        format.html { render :edit }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    authorize @project
    @project.destroy
    respond_to do |format|
      format.html { redirect_to admin_projects_url, notice: 'Xóa thành công.' }
      format.json { head :no_content }
    end
  end

  def destroy_attachment
    if params[:model_name] == 'projects' || params[:model_name].blank?
      case params[:column_name]
      when 'sale_policy'
        delete_attachment(params[:column_name])
      else
        delete_attachment(params[:column_name], name: params[:filename])
      end
    else
      delete_attachment_nested(params[:model_name], params[:nested_id], params[:column_name], params[:filename])
    end
    if @project.valid?
      @project.save
      @project.reload
      render json: {}
    else
      render json: { error: @project.errors[params[:type].to_sym]&.first }
    end
  end

  def import
    authorize :project, :import?
    ImportProjectJob.perform params[:file]&.path, current_user
  end

  def show_products
    @limit = params[:limit] ||= 20
    if @project.products.present?
      params[:level] ||= @project.products.where(level: 0).present? ? '0' : '1'
    end
    products = @project&.products.where(level: params[:level]).where('state IN (?)', %w(for_sale locking deposited sold))
    @all_states = Product.states.select { |s| products.map { |p| p.state }.uniq.include?(s.second) }.reject { |p| %w(recall third_holding third_sold).include?(p.second) }
    @all_product_names = products.map { |p| p.name }.uniq.reject { |p| p.empty? }.sort
    @all_real_estate_types = products.map { |p| [Reti::Product::REAL_ESTATE_TYPES[params[:level].to_i].values_at(p.real_estate_type).first, p.real_estate_type] }.uniq.sort
    @all_product_types = products.map { |p| p.product_type }.uniq.reject { |p| p.empty? }.sort
    @subdivisions = Division.where(project_id: params[:id], level: params[:level]).pluck(:name, :id).uniq.reject { |p| p.first.empty? }.sort
    @blocks = Division.where(parent_id: @subdivisions.map { |k, v| v }, level: params[:level]).pluck(:name, :id).uniq.sort
    @floors = Division.where(parent_id: @blocks.map { |k, v| v }, level: params[:level]).pluck(:name, :id).uniq.sort
    if browser.device.mobile?
      @pagy, @products = pagy(@project.products.search_result(params, level: params[:level]), items: @limit.to_i)
    else
      @products = @project.products.search_result(params, level: params[:level])
    end
    @products = @products.where('state IN (?)', %w(for_sale locking deposited sold)).sort_by(&:code)
    @check_params_filter = params && (params[:subdivision_id] || params[:block_id] || params[:floor_id] || params[:name] || params[:product_type] || params[:real_estate_type])
  end

  def information_for_sellers
    authorize @project, :edit?
    @labels = Constant::PROJECT_LABELS.without(0) # Do not allow user pick 'Đang mở bán'
  end

  def update_sale_policy
    @project.update_sale_policy!(params, @labels)
    redirect_to information_for_sellers_admin_project_path(@project), notice: 'Cập nhật thông tin cho Sale/CTV thành công.'
  end

  def lock_rules
    authorize @project, :edit?
    @users = User.internal.joins(:role).where(roles: { name: ['Sale Admin', 'Sale Admin Chi Nhánh'] }).pluck(:full_name, :id)
  end

  def update_lock_rules
    if params[:project].present?
      @project.assign_attributes(
        queue_lock: params[:project][:queue_lock],
        unc_confirmation_period_for_sale: params[:project][:unc_confirmation_period_for_sale],
        unc_confirmation_period_for_sale_admin: params[:project][:unc_confirmation_period_for_sale_admin],
        confirm_deposit_by_sale_admin: params[:project][:confirm_deposit_by_sale_admin]
      )
      if @project.save
        redirect_to lock_rules_admin_project_path(@project), notice: 'Thiết lập quy tắc lock căn thành công.'
      else
        @users = User.internal.joins(:role).where(roles: { name: ['Sale Admin', 'Sale Admin Chi Nhánh'] }).pluck(:full_name, :id)
        render 'admin/projects/lock_rules'
      end
    end
  end

  private

  def delete_attachment(type, name = nil)
    @project.not_required_image = true
    if name.present?
      remain_attachments = @project.send(type)
      # if @project.send(type).size == 1
      #   @project.send("remove_#{type}!")
      # else
      index = nil
      remain_attachments.each_with_index { |a, idx| (index = idx) and break if a.identifier == name }
      deleted_image = remain_attachments.delete_at(index) if index
      deleted_image.try(:remove!)
      @project[type.to_sym] = remain_attachments
      # end
    else
      @project.send("remove_#{type}!")
    end
  end

  def delete_attachment_nested(model_name, nested_id, column_name, filename)
    object = @project.send(model_name).find(nested_id)
    if filename.present?
      remain_attachments = object.send(column_name)
      index = nil
      remain_attachments.each_with_index { |a, idx| (index = idx) and break if a.identifier == filename }
      deleted_image = remain_attachments.delete_at(index) if index
      deleted_image.try(:remove!)
      object[column_name.to_sym] = remain_attachments
    else
      object.send("remove_#{column_name}!")
    end
    object.save(validate: false)
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_project
    @project = Project.find(params[:id])
  end

  # Get data Investor, Constructor, Development, Operator
  def get_icdo
    @investors = Investor.pluck(:name, :id)
    @constructors = Constructor.pluck(:name, :id)
    @developments = Development.pluck(:name, :id)
    @operators = Operator.pluck(:name, :id)
  end

  # Only allow a list of trusted parameters through.
  def project_params
    params[:project][:investors] = params[:project][:investors].nil? ? [] : params[:project][:investors].reject { |n| n.blank? }
    params[:project][:real_estate_type] = params[:project][:real_estate_type].nil? ? [] : params[:project][:real_estate_type].reject { |n| n.blank? }
    params[:project][:constructors] = params[:project][:constructors].nil? ? [] : params[:project][:constructors].reject { |n| n.blank? }
    params[:project][:developments] = params[:project][:developments].nil? ? [] : params[:project][:developments].reject { |n| n.blank? }
    params[:project][:operators] = params[:project][:operators].nil? ? [] : params[:project][:operators].reject { |n| n.blank? }
    params[:project][:features] = params[:project][:features].nil? ? [] : params[:project][:features].reject { |n| n.blank? }
    params[:project][:internal_utilities] = params[:project][:internal_utilities].nil? ? [] : params[:project][:internal_utilities].reject { |n| n.blank? }
    params[:project][:external_utilities] = params[:project][:external_utilities].nil? ? [] : params[:project][:external_utilities].reject { |n| n.blank? }
    params[:project][:banks] = params[:project][:banks].nil? ? [] : params[:project][:banks].reject { |n| n.blank? }
    params.require(:project).permit(:name, :locking_time, :construction_density, :total_area, :high_level_number, :low_level_number,
                                    :price_from, :price_to, :area_from, :area_to, :custom_description, :custom_sale_policy, :custom_utilities,
                                    :link_video, :position_description, :position_link_embed, :discount_support, { :labels => [] },
                                    { internal_utility_images: [] }, { external_utility_images: [] }, { design_style_images: [] },
                                    { :real_estate_type => [] }, { :features => [] }, :description, { images: [] },
                                    :images_cache, :internal_utility_images_cache, :external_utility_images_cache, :design_style_images_cache,
                                    { :internal_utilities => [] }, { :external_utilities => [] }, :ownership_period, :foreigner, { banks: [] },
                                    :loan_percentage_support, :loan_support_period, { :investors => [] },
                                    { :constructors => [] }, { :developments => [] }, { :operators => [] },
                                    milestones_attributes: [:id, :event, :event_time, :is_default, :_destroy],
                                    legal_documents_attributes: [:id, :doc_type, :doc, :doc_cache, :_destroy],
                                    payment_schedules_attributes: [:id, :name, :label_schedule, :pay, :_destroy],
                                    region_attributes: [:id, :city_id, :district_id, :ward_id, :street, :objectable_id, :objectable_type],
                                    key_values_attributes: [:id, :icon, :icon_cache, :title, :description, :_destroy],
                                    floorplan_images_attributes: [:id, :real_estate_type, { images: [] }, :images_cache, :_destroy],
                                    documents_attributes: [:id, :name, :file, :file_cache, :description, :preview_image, :preview_image_cache, :_destroy],
                                    sale_documents_attributes: [:id, :project_id, :title, :file, :sale_document_type, :_destroy]
    )
  end

  def export_to_xlsx(file, file_name)
    tempfile = "#{Rails.root}/tmp/tempfile.xlsx"
    file.serialize tempfile
    ::File.open tempfile, "r" do |f|
      send_data f.read, filename: file_name, type: "application/xlsx"
    end
    ::File.delete tempfile
  end

  def set_labels
    @labels = params[:project][:labels].nil? ? [] : params[:project][:labels].reject { |n| n.blank? }
    # 3 is label out of stock
    if @labels.include?("3")
      @labels = [3]
    else
      @labels += [0] if @project.products.for_sale.size > 0
    end
  end
end

class Admin::ProductsController < Admin::BaseController
  before_action :load_product, only: [:new, :edit, :update, :create, :destroy, :destroy_attachment, :update_wish_list]
  before_action :load_project, only: [:state_filter, :subdivision_id_filter, :block_id_filter, :floor_id_filter]
  before_action :new_product, only: [:new]

  def index
    authorize :product, :index?
    @project = Project.find_by(id: params[:project_id])
    params[:level] ||= 0
    products = @project&.products.where(level: params[:level]).where('state IN (?)', %w(for_sale locking deposited sold))
    @all_states = Product.states.select { |s| products.map { |p| p.state }.uniq.include?(s.second) }.reject { |p| %w(recall third_holding third_sold).include?(p.second) }
    @all_product_names = products.map { |p| p.name }.uniq.reject { |p| p.empty? }.sort
    @all_real_estate_types = products.map { |p| [Reti::Product::REAL_ESTATE_TYPES[params[:level].to_i].values_at(p.real_estate_type).first, p.real_estate_type] }.uniq.sort
    @all_product_types = products.map { |p| p.product_type }.uniq.reject { |p| p.empty? }.sort
    @subdivisions = Division.where(project_id: params[:project_id], level: params[:level]).pluck(:name, :id).uniq.reject { |p| p.first.empty? }.sort
    @blocks = Division.where(parent_id: @subdivisions.map { |k, v| v }, level: params[:level]).pluck(:name, :id).uniq.sort
    @floors = Division.where(parent_id: @blocks.map { |k, v| v }, level: params[:level]).pluck(:name, :id).uniq.sort
    @products = @project.products.search_result(params, level: params[:level])
    @products = @products.where('state IN (?)', %w(for_sale locking deposited sold out_of_stock)).sort_by(&:code)
    @check_params_filter = params && (params[:subdivision_id] || params[:block_id] || params[:floor_id] || params[:name] || params[:product_type] || params[:real_estate_type])
  end

  def state_filter
    all_states = Product.aasm.states.map { |state| { id: state.name, text: I18n.t("product.states.#{state}") } }.uniq
    search_states = params[:search].present? ? all_states.reject { |r| !included?(r[:text], params[:search]) } : all_states
    render json: (search_states & all_states)
  end

  def subdivision_id_filter
    all_subdivision_ids = @project.divisions.where(level: params[:level]).map { |division| { id: division.id, text: division.name } }
    search_subdivision_ids = params[:search].present? ? all_subdivision_ids.reject { |r| !r[:text].downcase.include?(params[:search].downcase) } : all_subdivision_ids
    render json: (search_subdivision_ids & all_subdivision_ids)
  end

  def block_id_filter
    all_block_ids = Division.where(level: params[:level], parent_id: params[:parent_id].present? ? params[:parent_id] : @project.divisions.pluck(:id)).map { |division| { id: division.id, text: division.name } }
    search_block_ids = params[:search].present? ? all_block_ids.reject { |r| !r[:text].downcase.include?(params[:search].downcase) } : all_block_ids
    render json: (search_block_ids & all_block_ids)
  end

  def floor_id_filter
    parent_ids = if params[:parent_id].present?
                   params[:parent_id]
                 else
                   []
                 end
    all_floor_ids = Division.where(level: params[:level], parent_id: parent_ids).map { |division| { id: division.id, text: division.name } }
    search_floor_ids = params[:search].present? ? all_floor_ids.reject { |r| !r[:text].downcase.include?(params[:search].downcase) } : all_floor_ids
    render json: (search_floor_ids & all_floor_ids)
  end

  def search
    redirect_to admin_project_products_path(project_id: params[:project_id], level: params[:level]&.to_i,
                                            high_query: params[:high_query], low_query: params[:low_query])
  end

  def autocomplete
    @project = Project.find_by(id: params[:project_id])
    render json: @project.products.autocomplete_result(params[:query], level: params[:level]&.to_i)
  end

  def get_divisions
    @divisions = if params[:is_project].present?
                   Division.where(project_id: params[:parent_id])
                 else
                   Division.where(parent_id: params[:parent_id])
                 end

    respond_to do |format|
      format.json { render json: @divisions }
    end
  end

  def import
    authorize :product, :import?
    ImportProductJob.perform params[:file]&.path, params[:project_id], current_user
  end

  def export_template
    file_name = "Bang_hang_#{Time.now.strftime(Settings.date.formats.time_export_xlsx)}.xlsx"
    export_to_xlsx ProductService.new.export_template, file_name
  end

  def new
    @product = @project.products.build(level: (params[:level] || 0))
    authorize @product
    @is_disabled = !policy(@product).create?
  end

  def edit
    if policy(@product).change_state? || policy(@product).update? || policy(@product).show?
      @is_disabled = !policy(@product).update?
      @state_is_disable = @product.is_state?(:out_of_stock) || !policy(@product).change_state?
    else
      respond_to do |format|
        flash[:error] = 'Bạn không có quyền thực hiện hành động này.'
        format.html { redirect_to admin_project_products_path }
      end
    end
  end

  def create
    return render json: { status: :ok } if params[:type].present?
    @product = @project.products.build(product_params)
    authorize @product

    respond_to do |format|
      if @product.save
        NotificationService.create_product(@project)
        format.html { redirect_to edit_admin_project_product_path(@project, @product), notice: 'Thêm mới sản phẩm thành công!' }
        format.json { render :edit }
      else
        @blocks = Division.where(parent_id:  @subdivisions.map{|x| x.second}).pluck(:name, :id)
        @floors = Division.where(parent_id: @blocks.map{|x| x.second}).pluck(:name, :id)
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity, level: params[:product][:level] }
      end
    end
  end

  def update
    respond_to do |format|
      if policy(@product).change_state?
        @product.execute_state_change product_params[:state]
      end
      if policy(@product).change_state? || policy(@product).update?
        @product.assign_attributes(product_params)
        # logic deal product
        @deals = @product.deals.where.not(state: 'canceled')
        if params[:confirm].blank?
          @show_confirm = true if @product.state_changed? && %w(recall third_holding third_sold).include?(@product.state) && @deals.present?
          @check_state = true if @product.state_changed? && (@product.state_was == 'locking' || @product.state_was == 'sold') && !@deals.contract_signed.present?
          @check_complete_deal = true if @product.state_changed? &&  @product.state_was == 'sold' && @deals.contract_signed.present?
          return render :update
        end
        if @product.valid?
          @product.save
          format.html { redirect_to edit_admin_project_product_path(@project, @product), notice: 'Thay đổi thông tin sản phẩm thành công.' }
          format.json { render :edit, status: :ok, location: @product }
        else
          # format.html { redirect_to edit_admin_project_product_path(@project, @product) }
          format.html { render :edit }
          format.json { render json: @product.errors, status: :unprocessable_entity }
        end
      else
        flash[:error] = 'Bạn không có quyền thực hiện hành động này.'
        format.html { redirect_to edit_admin_project_product_path(@project, @product) }
      end
    end
  end

  def destroy
    authorize @product
    @product.destroy
    respond_to do |format|
      format.html { redirect_to admin_project_products_path(@product.project), notice: 'Xoá thành công.' }
      format.json { head :no_content }
    end
  end

  def destroy_attachment
    case params[:type]
    when 'sale_policy'
      delete_attachment(params[:type])
    else
      delete_attachment(params[:type], name: params[:filename])
    end
    if @product.valid?
      @product.save
      @product.reload
      render json: {}
    else
      render json: { error: @product.errors[params[:type].to_sym]&.first }
    end

  end

  def update_wish_list
    if @product.wish_lists.where(user_id: current_user.id).blank?
      @product.wish_lists.new(favourite: params[:favourite], user_id: current_user.id)
      @product.save
    else
      @product.wish_lists.where(user_id: current_user.id).update(favourite: params[:favourite])
    end
  end

  private

  def delete_attachment(type, name: nil)
    if name.present?
      remain_attachments = @product.send(type)
      # if @product.send(type).size == 1
      #   @product.send("remove_#{type}!")
      # else
      index = nil
      remain_attachments.each_with_index { |a, idx| index = idx and break if a.identifier == name }
      deleted_image = remain_attachments.delete_at(index) if index
      deleted_image.try(:remove!)
      @product[type.to_sym] = remain_attachments
      # end
    else
      @product.send("remove_#{type}!")
    end
  end

  def load_product
    @projects = Project.where(id: params[:project_id]).pluck(:name, :id)
    @project = Project.find_by(id: params[:project_id])
    @product = @project.products.find_by(id: params[:id])
    if @product
      @stored_state = @product.state
    else
      @stored_state = nil
    end
    @subdivisions = Division.where(project_id: params[:project_id]).pluck(:name, :id)
    @blocks = Division.where(parent_id: @product&.subdivision_id).pluck(:name, :id)
    @floors = Division.where(parent_id: @product&.block_id).pluck(:name, :id)
  end

  def product_params
    level = (params.require(:product)[:level] || @product&.level)&.to_i
    if @product.blank? || (policy(@product).update? && policy(@product).change_state?)
      params.require(:product).permit(level == 0 ? Product::HIGH_ATTRS : Product::LOW_ATTRS)
    elsif @product.blank? || !policy(@product).update? && policy(@product).change_state?
      params.require(:product).permit(:state)
    elsif @product.blank? || policy(@product).update? && !policy(@product).change_state?
      params.require(:product).permit(level == 0 ? Product::HIGH_ATTRS_NOT_STATE : Product::LOW_ATTRS_NOT_STATE)
    end
  end

  def new_product
    @projects = Project.where(id: params[:project_id]).pluck(:name, :id)
    @project = Project.find_by(id: params[:project_id])
    @subdivisions = Division.where(project_id: params[:project_id], level: params[:level] ||= 0 ).pluck(:name, :id)
    @blocks = []
    @floors = []
  end

  def export_to_xlsx(file, file_name)
    tempfile = "#{Rails.root}/tmp/tempfile.xlsx"

    file.serialize tempfile
    ::File.open tempfile, "r" do |f|
      send_data f.read, filename: file_name, type: "application/xlsx"
    end
    ::File.delete tempfile
  end

  def load_project
    @project = Project.find_by(id: params[:project_id])
  end
end

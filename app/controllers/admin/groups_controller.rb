class Admin::GroupsController < Admin::BaseController
  before_action :set_group, only: [:show, :edit, :update, :destroy]
  before_action :load_data, only: [:new, :edit]

  # GET /groups
  # GET /groups.json
  def index
    groups = Group.all.order(name: :asc)
    @groups = groups.search_result(params)
    @cities = RegionData.cities.order(name: :asc).pluck(:name, :id)
    @directors = User.directors.pluck(:full_name, :id)
    @check_params_filter = params && params[:directors].present?
  end

  def autocomplete
    render json: Group.autocomplete_result(params[:query])
  end

  # GET /groups/1
  # GET /groups/1.json
  def show
  end

  # GET /groups/new
  def new
    @group = Group.new
    @staffs = User.available_add_to_group.map { |u| ["#{u.full_name} - #{u.email}", u.id] }
  end

  # GET /groups/1/edit
  def edit
    staffs = User.available_add_to_group.map { |u| ["#{u.full_name} - #{u.email}", u.id] }
    selected_staffs = User.where(id: @group.user_ids).map { |u| ["#{u.full_name} - #{u.email}", u.id] }
    @staffs = selected_staffs + staffs
  end

  # POST /groups
  # POST /groups.json
  def create
    @group = Group.new(group_params)

    respond_to do |format|
      if @group.save
        format.html { redirect_to admin_groups_path, notice: 'Sơ đồ tổ chức đã được tạo thành công.' }
        format.json { render :edit, status: :created, location: @group }
      else
        @cities = RegionData.cities.order(name: :asc).pluck(:name, :id)
        @directors = User.directors.pluck(:full_name, :id)
        @staffs = User.available_add_to_group.map { |u| ["#{u.full_name} - #{u.email}", u.id] }
        format.html { render :edit }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /groups/1
  # PATCH/PUT /groups/1.json
  def update
    respond_to do |format|
      if @group.update(group_params)
        format.html { redirect_to edit_admin_group_path(@group.id), notice: 'Sơ đồ tổ chức đã được chỉnh sửa thành công.' }
        format.json { render :edit, status: :ok, location: @group }
      else
        @cities = RegionData.cities.order(name: :asc).pluck(:name, :id)
        @directors = User.directors.pluck(:full_name, :id)
        @supervisors = User.supervisors.pluck(:full_name, :id)
        staffs = User.available_add_to_group.pluck(:full_name, :id)
        selected_staffs = User.where(id: @group.user_ids).pluck(:full_name, :id)
        @staffs = selected_staffs + staffs
        format.html { render :edit }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /groups/1
  # DELETE /groups/1.json
  def destroy
    User.where(id: @group.user_ids << @group.director_id).update(ancestry: nil)
    @group.destroy
    respond_to do |format|
      format.html { redirect_to admin_groups_url, notice: 'Xóa thành công.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_group
    @group = Group.find(params[:id])
  end

  def load_data
    @cities = RegionData.cities.order(name: :asc).pluck(:name, :id)
    @directors = User.directors.pluck(:full_name, :id)
    @supervisors = User.supervisors.pluck(:full_name, :id)
  end

  # Only allow a list of trusted parameters through.
  def group_params
    params[:group][:city_ids] = params[:group][:city_ids].nil? ? [] : params[:group][:city_ids].reject { |n| n.blank? }
    params[:group][:user_ids] = params[:group][:user_ids].nil? ? [] : params[:group][:user_ids].reject { |n| n.blank? }
    params.require(:group).permit(:name, { :city_ids => [] }, :director_id, :supervisor_id, { :user_ids => [] })
  end
end

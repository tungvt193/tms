class Admin::DealsController < Admin::BaseController
  include DateTimeHelper
  include DealHelper
  include CommonHelper

  respond_to :html, :json
  before_action :set_deals, only: [:index, :edit, :update, :reorder, :update_state, :update_commission]
  before_action :set_order, only: [:index, :edit, :reorder, :kanban_item]

  def index
    authorize :deal, :index?
    @deal = Deal.new
    @check_params_filter = check_params_filter_present params
    @current_state = I18n.t("deal.states.#{params[:state]&.first || (current_user.sale_admin? ? 'refundable_deposit' : 'pending')}")
    @clear_filter = !params['clear_filter'].nil?
    states = Deal.aasm.states.map(&:name)
    states.delete(:request_recall) unless current_user.director? || current_user.sale_leader? || current_user.sale_manager? || current_user.sale_admin? || current_user.super_admin?
    states.delete(:uninterested) unless current_user.director? || current_user.super_admin?
    @states = states.map { |state| [I18n.t("deal.states.#{state}"), state] }.uniq
    @assignees = if current_user.super_admin? || User.current.director? || User.current.marketing? || User.current.sale_admin? || User.current.customer_service?
                   User.order(full_name: :asc).to_a.map { |a| [a.full_name, a.id] }.unshift(['(Chưa có người chăm sóc)', 0])
                 else
                   current_user.subtree.to_a.flatten.map { |a| [a.full_name, a.id] }
                 end
    if browser.device.mobile?
      mobile_state = params[:state]
      if params.present?
        params.reject! { |k| k == 'state' }
      end
      params['browser'] = 'mobile'
      @deals = if params['clear_filter'].present? || (!@check_params_filter && params[:query]&.empty? && params[:state]&.reject(&:blank?).blank?)
                 Deal.all
               else
                 Deal.search_result(params)
               end
      @show_states = case true
                     when current_user.super_admin?
                       %w(pending interest meet refundable_deposit lock deposit contract_signed canceled request_recall uninterested)
                     when current_user.sale_admin?
                       %w(refundable_deposit lock deposit contract_signed canceled)
                     when current_user.sale_member?
                       %w(pending interest meet refundable_deposit lock deposit contract_signed canceled)
                     when (current_user.sale_leader? || current_user.sale_manager?)
                       %w(pending interest meet refundable_deposit lock deposit contract_signed canceled request_recall)
                     when current_user.director?
                       %w(pending interest meet refundable_deposit lock deposit contract_signed canceled request_recall uninterested)
                     else
                       %w(pending)
                     end
      @projects = policy_scope(Deal.includes(:project).order(name: :asc)).map(&:project).uniq.compact.map { |project| { id: project.id, text: project.name } } << { id: '0', text: 'Khác' }
    else
      @projects = policy_scope(Deal.includes(:project).order(name: :asc)).map(&:project).uniq.compact.map { |project| [project.name, project.id] }.unshift(['Khác', 0])
      @deals = if params['clear_filter'].present? || (!@check_params_filter && params[:query].blank? && params[:state]&.reject(&:blank?).blank?)
                 Deal.all
               else
                 Deal.search_result(params)
               end
    end
    if params[:view_mode] == 'table'
      @deals = if (params[:order_by].present? && params[:sort].present?) || (params['order_by'].present? && params['sort'].present?)
                 case params[:order_by]
                 when 'created_at', 'updated_at', 'source', 'project_id', 'purchase_purpose', 'product_type'
                   @deals.eager_load([:product, :project, :assignee]).send("order_by_#{params[:order_by]}", params[:sort].to_s)
                 else
                   @deals.eager_load([:product, :project, :assignee]).send("order_by_updated_at", 'DESC')
                 end
               else
                 @deals
               end
    end
    @deals = policy_scope(@deals)
    @deal_groups = if (params[:order_by].present? && params[:sort].present?) || (params['order_by'].present? && params['sort'].present?)
                     case params[:order_by]
                     when 'created_at', 'updated_at', 'source', 'project_id', 'purchase_purpose', 'product_type'
                       @deals.eager_load([:product, :project, :assignee]).send("order_by_#{params[:order_by]}", params[:sort].to_s).group_by(&:state)
                     else
                       @deals.eager_load([:product, :project, :assignee]).send("order_by_created_at", 'DESC').group_by(&:state)
                     end
                   else
                     @deals.eager_load([:product, :project, :assignee]).with_sort(current_user.id).group_by(&:state)
                   end

    @num_item = %w(pending interest meet refundable_deposit lock deposit contract_signed uninterested canceled request_recall).to_h do |state|
      [state, @deal_groups[state]&.size || 0]
    end
    @deal_groups = @deal_groups.transform_values { |v| v[params[:page].to_i * 5, 5] }
    params['state'] = mobile_state if mobile_state.present?

    @label_filters = current_user.is_sale? ? Constant::LABELS_FILTER - Constant::UNINTERESTED_REASON.values : Constant::LABELS_FILTER
    if params[:is_loaded]
      if browser.device.mobile?
        render 'admin/deals/mobile/_kanban', layout: false
      else
        render 'admin/deals/_kanban', layout: false
      end
    else
      if browser.device.mobile?
        render 'admin/deals/mobile/index', layout: 'deal_forms'
      else
        render 'index'
      end
    end
  end

  def show
    @deal = Deal.find_by(id: params[:id])
    respond_modal_with @deal
  end

  def new
    @deal = Deal.new
    authorize @deal
    @projects = Project.all.pluck :name, :id
    respond_to do |format|
      if browser.device.mobile?
        format.html { render 'admin/deals/mobile/new', layout: 'deal_forms' }
      else
        format.html {}
      end
    end
  end

  def create
    @deal = Deal.new(deal_params(:general))
    authorize @deal
    respond_to do |format|
      if @deal.save
        format.html { redirect_to edit_admin_deal_path(@deal), notice: 'Thêm mới giao dịch thành công!' }
      else
        @projects = Project.all.pluck :name, :id
        flash[:error] = 'Thêm mới giao dịch không thành công!'
        if browser.device.mobile?
          format.html { render 'admin/deals/mobile/new', layout: 'deal_forms' }
        else
          format.html { render :new }
        end
      end
    end
  end

  def edit
    @transfer_histories = TransferHistory.by_deal(@deal.id).order(created_at: :desc)
    if policy(@deal).show?
      @is_edit_deal = policy(@deal).update?
      @commission = @deal.commission.present? ? @deal.commission : @deal.build_commission
      @deal_commission_policy_edit = CommissionPolicy.new(current_user, @commission).show?
      @history_interaction_group = @deal.history_interactions.order(interaction_date: :desc).group_by { |history| history&.interaction_date.strftime("%d/%m/%Y") }
      if params[:drag_state_changed].present?
        @deal.state = params[:drag_state_changed]
        @drag_state_changed = params[:drag_state_changed]
      end
      @flash_general, @flash_customer = count_flash_error(@deal)
      if (@deal.invalid? || @deal.customer_persona.invalid?) && @deal.source  
        @is_edit_general = @flash_general > 0 && policy(@deal).update?
        @is_edit_customer = @flash_customer > 0 && policy(@deal).update?
      end
      @deal_update_comm = true if @deal.comm_fields_blank? && @deal.contract_signed? && current_user.sale_admin?
      @deal.build_commission(calculation_type: 0) unless @deal.commission
      respond_to do |format|
        if browser.device.mobile?
          format.html { render 'admin/deals/mobile/edit', layout: 'deal_forms' }
        else
          format.html {}
        end
      end
    else
      msg = I18n.t("alert.no_authorize")
      flash[:error] = msg
      redirect_back(fallback_location: admin_root_path)
    end
    @cs_ticket = CsTicket.new
    if policy(@cs_ticket).show?
      @deal = Deal.find(params[:id])
      @projects = Project.all.pluck(:name, :id)
      applicable_condition_voucher = @deal.get_applicable_condition_voucher
      @vouchers = @deal.project&.vouchers&.where(voucher_type: @deal.state == 'contract_signed' ? 2 : 1, applicable_condition: applicable_condition_voucher)&.order('content ASC')&.pluck(:content, :id)
      @cities = RegionData.cities.pluck(:name, :id)
      @districts = @deal.customer_persona&.city_id ? RegionData.where(parent_id: @deal.customer_persona&.city_id).order(:name).pluck(:name_with_type, :id) : []
      @wards = @deal.customer_persona&.district_id ? RegionData.where(parent_id: @deal.customer_persona&.district_id).order(:name).pluck(:name_with_type, :id) : []
      @source_group = Deal.get_source_group
      @history_interaction_group = @deal.history_interactions.order(interaction_date: :desc).group_by { |history| history&.interaction_date.strftime("%d/%m/%Y") }
      params[:form] = 'general' unless params[:form]
      deposit_tickets = @deal.cs_tickets.where(deal_state: 'deposit').order(created_at: :desc)
      contract_signed_tickets = @deal.cs_tickets.where(deal_state: 'contract_signed').order(created_at: :desc)
      canceled_tickets = @deal.cs_tickets.where(deal_state: 'canceled').order(created_at: :desc)
      @cs_tickets_groups = { 'deposit' => deposit_tickets, 'contract_signed' => contract_signed_tickets, 'canceled' => canceled_tickets }
    end
  end

  def update_name
    @deal.assign_attributes(deal_params)
    respond_to do |format|
      @deal.save(validate: false) if @deal.name_changed?
      format.js { render :edit }
    end
  end

  # Refactor update deal state
  def update_state
    if policy(@deal).change_state?
      if @deal.source.present?
        @deal.assign_attributes(deal_params)
        if @deal.state_changed? && @deal.valid? && @deal.customer_persona.valid?
          if @deal.state == 'request_recall'
            NotificationService.update_state_to_request_recall(@deal, @deal.assignee)
            @deal.assignee_id = nil
          end
          @deal.save
          @is_edit_state = true
          if params[:deal_id_above]
            DealSortJob.perform_later(@deal.state, @deal.id, params[:deal_id_above], current_user)
          end
          @reload_general_form = true
          @reload_customer_form = true
          respond_to do |format|
            format.js { render :edit }
          end
        else
          @drag_state_changed = params[:deal][:state]
          @deal.form = 'general'
          @deal.invalid? ? (@is_edit_general = true) : (@reload_general_form = true)
          @deal.form = 'customer'
          @deal.customer_persona.invalid? ? (@is_edit_customer = true) : (@reload_customer_form = true)
          @deal.form = nil
          # TODO: Highlight required fields
          if @is_edit_general || @is_edit_customer
            msg = "Thay đổi sang trạng thái <b>#{I18n.t("deal.states.#{@deal.state}")}</b> không thành công. Vui lòng kiểm tra lại!"
            if @deal.contact_status != 1 && %w(interest meet refundable_deposit lock deposit contract_signed).include?(@deal.state)
              msg << "<br>Bạn cần liên hệ được với khách hàng để chuyển giao dịch sang trạng thái kế tiếp"
            end
            flash[:error] = msg
          end
          if params[:is_ajax]
            redirect_to edit_admin_deal_path(@deal, drag_state_changed: @deal.state)
          else
            respond_to do |format|
              format.js { render :edit }
            end
          end
        end
      end
    else
      msg = I18n.t("alert.no_authorize")
      flash[:error] = msg
      return render 'error'
    end
  end

  # Refactor updated deal assignee
  def update_assignee
    @deal = Deal.find_by(id: params[:id])
    if policy(@deal).assign?
      respond_to do |format|
        current_assignee = @deal.assignee
        if params[:deal][:assignee_id] != ''
          @deal.assignee_id = params[:deal][:assignee_id].to_i
        else
          @deal.assignee_id = nil
        end
        if @deal.save(validate: false)
          if params[:deal][:assignee_id].present?
            NotificationService.notify_change_assignee(@deal, current_assignee)
            if (Time.now - @deal.created_at) / 1.day.to_i < 1
              @deal.push_notification_appointment
            end
            format.json { render json: { team_lead: @deal.assignee.sale_leader? ? @deal.assignee.full_name : @deal.assignee&.leader&.full_name, status: :ok } }
          else
            format.json { render json: { status: :cancel } }
          end
        else
          format.json { render json: @deal.errors, status: :unprocessable_entity }
        end
      end
    else
      msg = I18n.t("alert.no_authorize")
      flash[:error] = msg
      return render 'error'
    end
  end

  # Refactor update deal source
  def update_source
    @deal = Deal.find_by(id: params[:id])
    respond_to do |format|
      if @deal.update_attribute(:source, params[:deal][:source])
        format.json { render json: { allow_assignee: @deal.allow_assignee?, general_invalid: @deal.invalid? }, status: :ok }
      else
        format.json { render json: @deal.errors, status: :unprocessable_entity }
      end
    end
  end

  # Refactor update create date
  def update_create_date
    @deal = Deal.find_by(id: params[:id])
    respond_to do |format|
      @deal.create_deal_date = params[:deal][:create_deal_date].to_time
      @deal.is_change_create_date = true
      if @deal.save(validate: false)
        heading = "#{Deal.time_history(DateTime.now) } - " +
          "#{Role::ROLE_SHORT[current_user&.role&.name]} <span class='detail-item-heading'>#{current_user&.full_name}</span> đã cập nhật thông tin cho giao dịch:"
        @deal.histories.create(content: @deal.render_content(heading, ["Ngày tạo giao dịch: Đã cập nhật"]))
        format.json { render json: {}, status: :ok }
      else
        format.json { render json: @deal.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_lead_scoring
    @deal = Deal.find_by(id: params[:id])
    @deal.update_column(:lead_scoring, params[:lead_scoring].to_i) if @deal.lead_scoring_changed?
    respond_to do |format|
      format.json { render json: {}, status: :ok }
    end
  end

  def update
    authorize @deal
    # Refactor deal update
    if params[:refresh].present?
      @flash_general, @flash_customer = count_flash_error(@deal)
      if (@deal.invalid? || @deal.customer_persona.invalid?) && @deal.source  
        @flash_general > 0 && policy(@deal).update? ? (@is_edit_general = true) : (@reload_general_form = true)
        @flash_customer > 0 && policy(@deal).update? ? (@is_edit_customer = true) : (@reload_customer_form = true)
      else
        @reload_general_form = true
        @reload_customer_form = true
      end
      @deal_update_comm = true if @deal.comm_fields_blank? && @deal.contract_signed? && current_user.sale_admin?
      @refresh = true
      return
    end
    if params[:form] == 'general'
      update_general
      return
    elsif params[:form] == 'customer'
      update_customer
      return
    elsif params[:form] == 'name'
      update_name
      return
    elsif params[:form] == 'commission'
      update_commission
      return
    end
  rescue ActiveRecord::StaleObjectError
    flash[:error] = 'Thông tin giao dịch đã bị thay đổi'
    @deal.reload
  ensure
    return if performed?
    old_deal = Deal.find_by(id: @deal.id)
    @flash_general, @flash_customer = count_flash_error(@deal)
    respond_to do |format|
      @deal.is_update_comm = false
      @deal_update_comm = true if @deal.comm_fields_blank? && @deal.contract_signed? && @deal.valid?
      format.js { render :edit }
    end
  end

  # Refactor deal general
  def update_general
    @deal.assign_attributes deal_params(:general)
    @deal.form = 'general'
    @is_edit_general = params[:edit].present? || (params[:save].present? && @deal.invalid?) && !@deal.request_recall?
    respond_to do |format|
      if params[:save].present?
        state = @deal.state
        if params[:drag_state_changed].present? || params[:deal][:contact_status_when_update].present?
          @drag_state_changed = params[:drag_state_changed].present? ? params[:drag_state_changed] : params[:deal][:contact_status_when_update]
          @deal.state = @drag_state_changed
          if @deal.valid?
            @deal.form = 'customer'
            if @deal.customer_persona.valid?
              @deal.form = 'general'
              @deal.save
              @is_edit_state = true
              @drag_state_changed = ''
              flash[:notice] = "Thay đổi sang trạng thái <b>#{I18n.t("deal.states.#{@deal.state}")}</b> thành công!"
            else
              if params[:deal][:contact_status_when_update].present?
                @deal.form = 'general'
                @deal.invalid? ? (@is_edit_general = true) : (@reload_general_form = true)
                @deal.form = 'customer'
                @deal.customer_persona.invalid? ? (@is_edit_customer = true) : (@reload_customer_form = true)
                respond_to do |format|
                  format.js { render :edit }
                end
              end
              @deal.form = 'general'
              @deal.state = state
              @deal.save
              flash[:notice] = 'Chỉnh sửa thông tin chung thành công!'
            end
          else
            if params[:deal][:contact_status_when_update].present?
              @deal.form = 'general'
              @deal.invalid? ? (@is_edit_general = true) : (@reload_general_form = true)
              @deal.form = 'customer'
              @deal.customer_persona.invalid? ? (@is_edit_customer = true) : (@reload_customer_form = true)
              @deal.form = nil
              if @is_edit_general || @is_edit_customer
                flash[:error] = "Thay đổi sang trạng thái <b>#{I18n.t("deal.states.#{@deal.state}")}</b> không thành công. Vui lòng kiểm tra lại!"
              end
              respond_to do |format|
                format.js { render :edit }
              end
            end
            @is_edit_general = true
            msg = "Thay đổi sang trạng thái <b>#{I18n.t("deal.states.#{@deal.state}")}</b> không thành công. Vui lòng kiểm tra lại!"
            if @deal.contact_status != 1 && %w(interest meet refundable_deposit lock deposit contract_signed).include?(@deal.state)
              msg << "<br>Bạn cần liên hệ được với khách hàng để chuyển giao dịch sang trạng thái kế tiếp"
            end
            flash[:error] = msg
          end
        else
          if @deal.save
            flash[:notice] = 'Chỉnh sửa thông tin chung thành công!'
          else
            flash[:error] = "Chỉnh sửa thông tin không thành công. Vui lòng kiểm tra lại!"
          end
        end
      else
        @deal.reload
        @deal.form = 'general'
        if params[:drag_state_changed].present?
          @stop_change_state = true
          @drag_state_changed = ''
          @is_edit_state = true
        end
      end
      # Trigger valid? to highlight required fields
      @deal.valid?
      format.js { render :edit }
    end
  end

  # Refactor deal customer
  def update_customer
    @deal.assign_attributes deal_params(:customer)
    @deal.form = 'customer'
    @is_edit_customer = params[:edit].present? || (params[:save].present? && (@deal.invalid? || @deal.customer_persona.invalid?)) && !@deal.request_recall?  
    respond_to do |format|
      if params[:save].present?
        state = @deal.state
        if params[:drag_state_changed].present? || params[:deal][:contact_status_when_update].present?
          @drag_state_changed = params[:drag_state_changed].present? ? params[:drag_state_changed] : params[:deal][:contact_status_when_update]
          @deal.state = @drag_state_changed
          if @deal.save
            @is_edit_state = true
            @drag_state_changed = ''
            flash[:notice] = "Thay đổi sang trạng thái <b>#{I18n.t("deal.states.#{@deal.state}")}</b> thành công!"
          else
            @deal.state = state
            if @deal.save
              flash[:notice] = 'Chỉnh sửa thông tin thành công!'
            else
              flash[:error] = "Chỉnh sửa thông tin không thành công. Vui lòng kiểm tra lại!"
            end
          end
        else
          if @deal.valid? && @deal.customer_persona.valid?
            @deal.save
            flash[:notice] = 'Chỉnh sửa thông tin thành công!'
          elsif current_user.customer_service?
            @deal.save(validate: false)
            flash[:notice] = 'Chỉnh sửa thông tin thành công!'
          else
            flash[:error] = "Chỉnh sửa thông tin không thành công. Vui lòng kiểm tra lại!"
          end
        end
      else
        @deal.reload
        @deal.form = 'customer'
        if params[:drag_state_changed].present?
          @stop_change_state = true
          @drag_state_changed = ''
          @is_edit_state = true
        end
      end
      # Trigger valid? to highlight required fields
      @cities = RegionData.cities.pluck(:name, :id)
      @districts = @deal.customer_persona&.city_id ? RegionData.where(parent_id: @deal.customer_persona&.city_id).order(:name).pluck(:name_with_type, :id) : []
      @wards = @deal.customer_persona&.district_id ? RegionData.where(parent_id: @deal.customer_persona&.district_id).order(:name).pluck(:name_with_type, :id) : []
      @deal.valid?
      format.js { render :edit }
    end
  end

  def update_commission
    if params[:deal].present? && params[:deal][:commission_attributes].present?
      @deal.assign_attributes deal_params(:commission)
    end
    @is_edit_commission = params[:edit].present? || (params[:save].present? && (@deal.invalid? || @deal.commission&.invalid?)) && !@deal.request_recall?
    respond_to do |format|
      if params[:form] === 'commission'
        @deal.form = 'commission'
        if params[:save].present?
          @deal.assign_attributes deal_params(:commission)
          if @deal.save && @deal.commission.valid?
            flash[:notice] = 'Chỉnh sửa thông tin thành công!'
          else
            flash[:error] = "Chỉnh sửa thông tin không thành công. Vui lòng kiểm tra lại!"
          end
        else
          @deal.reload
        end
      else
        @deal.total_price = (params[:value][:total_price].delete '.').to_i if params[:value][:total_price].present?
        @deal.contract_signed_at = string_to_datetime(params[:value][:signed_at]) if params[:value][:signed_at].present?
        @deal.save(validate: false)
      end
      @is_update_comm = true
      format.js { render :edit }
    end
  end

  def update_source_detail
    @deal = Deal.find_by(id: params[:id])
    @is_edit_state = true
    source_detail = params[:value].present? ? params[:value] : nil
    @deal.source_detail = source_detail
    @deal.save validate: false
    respond_to do |format|
      format.json { render json: { source_detail: source_detail }, status: :ok }
    end
  end

  def reorder
    DealSortJob.perform_later(params[:state_to], params[:deal_id_current], params[:deal_id_above], current_user)
    if params[:deal_id_above].blank? && !policy(@deal).change_state?
      respond_to do |format|
        msg = I18n.t("alert.no_authorize")
        flash[:error] = msg
        format.html {redirect_to admin_deals_url}
      end
    end
  end

  def kanban_item
    deals = Deal.where(id: params[:id])
    deals = policy_scope(deals)
    @deal = deals.first
    @user_update = User.find_by(id: params[:user_id])
  end

  def destroy
    @deal = Deal.find(params[:id])
    authorize(@deal) unless params[:is_duplicated]
    @deal.destroy
    respond_to do |format|
      format.html { redirect_to (admin_deals_url || request.referrer), notice: "Xóa thành công#{params[:is_duplicated] ? ' giao dịch trùng' : ''}." }
    end
  end

  def get_comm_info
    deal = Deal.find_by(id: params[:id])
    render json: {
      status: 'success',
      can_update_comm: current_user.sale_admin?,
      is_external_project: deal.is_external_project,
      current_state: deal.state,
      comm_fields: {
        total_price: deal&.total_price,
        contract_signed_at: format_regulation(deal&.contract_signed_at)
      }
    }
  end

  def get_all_deal_by_query
    @deals = Deal.all
    @deals = policy_scope(@deals)
    respond_to do |format|
      format.json { render json: @deals }
    end
  end

  def product_search
    @project = Project.find_by(id: params[:project_id])
    deal = Deal.find_by(id: params[:deal_id])
    @products = @project.products.without_state(:out_of_stock)
    if current_user.sale_admin? && deal&.state == 'contract_signed'
      @products = @products.select2_search_result(params[:search], true)
    else
      @products = @products.where('products.state': 'for_sale').select2_search_result(params[:search], false)
    end
    render json: @project.present? ? @products : []
  end

  def product_type_search
    project = Project.find_by(id: params[:project_id])
    if project.present?
      real_estate_type = project.real_estate_type
    else
      real_estate_type = Constant::PROJECT_REAL_ESTATE_TYPE.keys
    end
    project_real_estate_type = Constant::PROJECT_REAL_ESTATE_TYPE.values_at(*real_estate_type)
    render json: project_real_estate_type.each_with_index.map { |x, index| { text: x, id: real_estate_type[index], criteria: Constant::PRODUCT_SELECTION_CRITERIA[real_estate_type[index]] } }.select { |pt| convert_string(pt[:text].to_s).include? convert_string(params[:search].to_s) }
  end

  def project_search
    render json: Project.select2_search_result(params[:search])
  end

  def customer_search
    @customers = policy_scope(Customer)
    render json: @customers.select2_search_result(params[:search])
  end

  def assignee_search
    render json: User.assignee_search_result(params[:query])
  end

  def autocomplete
    if request.referer.include?('complete_deals')
      render json: policy_scope(Deal.contract_signed).autocomplete_result(params[:query], params[:type], params[:state])
    else
      render json: policy_scope(Deal).autocomplete_result(params[:query], params[:type], params[:state]).uniq { |e| e[:value] }
    end
  end

  def filter
    search_items = all_items = case params[:type]
                               when 'project'
                                 if request.referer.include?('complete_deals')
                                   complete_deals = policy_scope(Deal.contract_signed).map(&:project).uniq.compact.map { |project| { id: project.id, text: project.name } }
                                   if policy_scope(Deal.contract_signed).select { |d| d.is_external_project == true }.present?
                                     complete_deals << { id: '0', text: 'Khác' }
                                   end
                                   complete_deals
                                 else
                                   policy_scope(Deal.all).map(&:project).uniq.compact.map { |project| { id: project.id, text: project.name } } << { id: '0', text: 'Khác' }
                                 end
                               when 'assignee'
                                 if current_user.super_admin? || User.current.director? || User.current.marketing? || User.current.sale_admin? || User.current.customer_service?
                                   User.all.to_a.map { |a| { id: a.id, text: a.full_name } }.unshift({ id: '0', text: '(Chưa có người chăm sóc)' })
                                 else
                                   current_user.subtree.to_a.flatten.map { |a| { id: a.id, text: a.full_name } }
                                 end
                               when 'state'
                                 states = Deal.aasm.states.map(&:name)
                                 states.delete(:request_recall) unless current_user.director? || current_user.sale_leader? || current_user.sale_manager? || current_user.sale_admin?
                                 states.delete(:uninterested) unless current_user.director?
                                 states.map { |state| { id: state, text: I18n.t("deal.states.#{state}") } }.uniq
                               when 'source'
                                 Constant::DEAL_SOURCES.map { |k, v| { id: k, text: v } }
                               when 'add'
                                 Constant::FILTER_RESULTS.map { |k, v| { id: k, text: v } }
                               when 'label'
                                 Constant::CONTACT_STATUSES.map { |k, v| { id: "#{k}-contact", text: v } }
                               when 'group'
                                 current_user.select2_search_group_result
                               when 'purchase_purpose'
                                 Constant::PURCHASE_PURPOSE.map { |k, v| { id: k, text: v } }
                               when 'product_type'
                                 Constant::PROJECT_REAL_ESTATE_TYPE.map { |k, v| { id: k, text: v } }
                               when 'door_direction', 'balcony_direction'
                                 Reti::Product::DIRECTIONS.map { |k, v| { id: k, text: v } }
                               end

    if params[:search].present?
      search_items = case params[:type]
                     when 'project'
                       Project.select2_search_result(params[:search]) << { id: '0', text: 'Khác' }
                     when 'assignee'
                       User.assignee_search_result(params[:search], avatar: false)
                     when 'state', 'source', 'add', 'label', 'group', 'purchase_purpose', 'product_type', 'door_direction', 'balcony_direction'
                       all_items.reject { |r| !included?(r[:text], params[:search]) }
                     end
    end
    res = params[:type] == 'stored' ? {} : (search_items & all_items)
    render json: res
  end

  def check_deal_contract_signed_product
    deal = Deal.find(params[:deal_id])
    if deal.product.present? && deal.state == 'contract_signed' && deal.product_id != params[:product_id] && params[:product_id].present?
      @product_for_contract_signed = Product.find(params[:product_id]).deals.reject { |d| d == deal }.select { |d| d.state == 'contract_signed' }.present?
    end
    render json: { value: @product_for_contract_signed }
  end

  def complete_deals
    @complete_deals = policy_scope(Deal.contract_signed)
    @projects = @complete_deals.map { |d| d.project }.compact.uniq.sort_by(&:name).pluck(:name, :id)
    @assignee = @complete_deals.map { |d| d.assignee }.compact.uniq.sort_by(&:full_name).pluck(:full_name, :id)
    @complete_deals = @complete_deals.search_result(params)
    @check_params_filter = params && (params[:calculation_type] || params[:assignee] || (params[:contract_signed_at].present? && params[:contract_signed_at] != ''))
    @sum_total_price = @complete_deals.without_transfer.pluck(:total_price).reject { |total_price| total_price == nil }.reduce { |sum, total_price| sum + total_price }
    @total_commission_temporary = @complete_deals.includes(:commission).pluck(:temporary_commission).reject { |temporary_commission| temporary_commission == nil }.reduce { |sum, temporary_commission| sum + temporary_commission }
    @total_commission_actually = @complete_deals.includes(:commission).pluck(:actually_commission).reject { |actually_commission| actually_commission == nil }.reduce { |sum, actually_commission| sum + actually_commission }
  end

  def export_deals
    params[:list_fields] = params[:list_fields].reject { |f| f == 'check_all' }
    if params[:export_complete_deals] && params[:export_complete_deals].to_sym == :true
      deals = policy_scope(Deal.contract_signed)
      deals = if params[:filter].blank?
                deals.order(updated_at: :desc)
              else
                deals.search_result(params[:filter]).order(updated_at: :desc)
              end
    else
      deals = if params[:filter].blank?
                Deal.order(updated_at: :desc)
              else
                Deal.search_result(params[:filter]).order(updated_at: :desc)
              end
      deals = policy_scope(deals)
    end

    ExportDealJob.perform(deals, params[:list_fields])
  end

  def new_transfer
    @deal = Deal.find_by(id: params[:id])
    respond_to { |format| format.html { redirect_to edit_admin_deal_path(@deal), notice: "Bạn không có quyền chuyển nhượng giao dịch!" } } unless policy(@deal)

    @states = @deal.states.map { |state| [state[0], state.last.name] unless %i(canceled request_recall uninterested).include?(state.last.name) }
  end

  def create_transfer
    @deal = Deal.find_by(id: params[:id])
    if policy(@deal).transfer?
      @new_deal = Deal.new(transfer_params.merge(is_deal_transfer: true))
      if @new_deal.valid?
        @new_deal.save
        @deal.transfer_histories.create(transferred_id: @new_deal.id)
        @deal.commission.update_column(:actually_commission, 0) if @deal.commission&.actually_commission.present?
        @deal.commission.update_column(:temporary_commission, 0) if @deal.commission&.temporary_commission.present?
        redirect_to edit_admin_deal_path(@new_deal), notice: 'Tạo giao dịch chuyển nhượng thành công'
      else
        @states = @deal.states.map { |state| [state[0], state.last.name] unless %i(canceled request_recall uninterested).include?(state.last.name) }
        flash[:error] = "Chuyển nhượng không thành công. Vui lòng kiểm tra lại!"
        render :new_transfer
      end
    else
      redirect_to edit_admin_deal_path(@deal), notice: "Bạn không có quyền chuyển nhượng giao dịch!"
    end
  end

  private

  def set_deals
    params[:id] ||= params[:deal_id_current]
    @projects = Project.all.pluck(:name, :id)
    @deal = Deal.find_by(id: params[:id]) if params[:id].present?
    @customer = Customer.new
    if @deal
      @call = Call.new(deal_id: @deal.id)
      @appointment = Appointment.new(deal_id: @deal.id)
      @feedback = Feedback.new(deal_id: @deal.id)
      @deal.build_customer_persona(customer_id: @deal.customer_id) unless @deal.customer_persona
      @deal.customer_persona.customer_id = @deal.customer_id
      @cities = RegionData.cities.pluck(:name, :id)
      @districts = @deal.customer_persona&.city_id ? RegionData.where(parent_id: @deal.customer_persona&.city_id).order(:name).pluck(:name_with_type, :id) : []
      @wards = @deal.customer_persona&.district_id ? RegionData.where(parent_id: @deal.customer_persona&.district_id).order(:name).pluck(:name_with_type, :id) : []
      @source_group = Deal.get_source_group
      @current_hobbies = @deal.customer_persona.hobbies.reject { |h| h.empty? }.map(&:to_i).to_json if @deal && @deal.customer_persona.present?
    end
    @hobbies = Constant::HOBBIES_SELECT2_DATA.to_json
    # Refactor
    params[:form] = 'general' unless params[:form]
  end

  def set_order
    @disable_drag_states = %w[pending]
  end

  def deal_params(form = nil)
    if params[:deal]
      params[:deal][:trouble_problem_list] = params[:deal][:trouble_problem_list].reject { |n| n.blank? } if params[:deal][:trouble_problem_list].present?
      params[:deal][:balcony_direction] = params[:deal][:balcony_direction].reject { |n| n.blank? } if params[:deal][:balcony_direction].present?
      params[:deal][:door_direction] = params[:deal][:door_direction].reject { |n| n.blank? } if params[:deal][:door_direction].present?
      params[:deal][:interaction_detail] = params[:deal][:interaction_detail].reject { |n| n.blank? } if params[:deal][:interaction_detail].present?
      params[:deal][:product_selection_criteria] = params[:deal][:product_selection_criteria].reject { |n| n.blank? } if params[:deal][:product_selection_criteria].present?
      if params[:deal][:customer_persona_attributes]
        params[:deal][:customer_persona_attributes][:hobbies] = params[:deal][:customer_persona_attributes][:hobbies].reject { |n| n.blank? } if params[:deal][:customer_persona_attributes][:hobbies].present?
      end
    end
    case form
    when :general
      params.fetch(:deal, {}).permit(Deal::GENERAL_ATTRS)
    when :customer
      params.fetch(:deal, {}).permit(Deal::CUSTOMER_ATTRS)
    when :commission
      if params[:deal][:commission_attributes][:calculation_type] == '1'
        params.fetch(:deal, {}).permit(Deal::COMMISSION_FIXED_ATTRS)
      else
        params.fetch(:deal, {}).permit(Deal::COMMISSION_PERCENTAGE_ATTRS)
      end
    else
      params.fetch(:deal, {}).permit(Deal::ATTRS)
    end
  end

  def count_flash_error(deal)
    flash_general_arr = []
    deal.build_customer_persona(customer_id: deal.customer_id) unless deal.customer_persona
    deal.valid?
    deal.customer_persona.valid?
    deal.errors.each do |error|
      case error
      when :project_id, :product_id, :customer_id, :contact_status, :product_type, :purchase_purpose
        flash_general_arr.push(error)
      end
    end
    [flash_general_arr.uniq.count, deal.customer_persona.errors.count]
  end

  def string_to_datetime(string, format = "%d/%m/%Y")
    return nil if string.blank?
    DateTime.strptime(string, format).to_time
  end

  def convert_string(str)
    str.mb_chars.normalize(:kd).gsub(/\p{Mn}/, '').downcase.to_s.parameterize
  end

  def check_params_filter_present params
    params[:source]&.reject(&:blank?).present? || params[:project]&.reject(&:blank?).present? ||
      params[:assignee]&.reject(&:blank?).present? || params[:label]&.reject(&:blank?).present? || params[:purchase_purpose]&.reject(&:blank?).present? ||
      params[:product_type]&.reject(&:blank?).present? || params[:created_at].present? || params[:updated_at].present? ||
      params[:balcony_direction]&.reject(&:blank?).present? || params[:door_direction]&.reject(&:blank?).present?
  end

  def transfer_params
    params.require(:deal).permit(:customer_id, :product_id, :assignee_id, :state, :project_id)
  end
end

class Admin::VouchersController < Admin::BaseController

  before_action :set_voucher, only: [:update, :destroy]

  def index
    authorize :voucher, :index?
    @vouchers = Voucher.all.order(created_at: :desc)
    @applicable_condition = Constant::DEAL_PRICE_RANGE.merge({ 7 => 'Không giới hạn' })
    @projects = [['Tất cả dự án', '0']] + Project.all.order('labels @> ARRAY[[0]]::integer[] DESC NULLS LAST, projects.updated_at DESC').pluck(:name, :id)
    @voucher = Voucher.new
  end

  def create
    @voucher = Voucher.new(voucher_params)
    authorize @voucher
    respond_to do |format|
      if @voucher.save
        if params[:voucher][:project_id].present?
          params[:voucher][:project_id].each do |project_id|
            VouchersProjects.create(project_id: project_id, voucher_id: @voucher.id)
          end
        end
        format.html { redirect_to admin_vouchers_path, notice: 'Voucher đã được tạo thành công.' }
      end
    end
  end

  def update
    authorize @voucher
    respond_to do |format|
      if @voucher.update(voucher_params)
        @voucher.projects.pluck(:id).each do |project_id|
          VouchersProjects.find_by(project_id: project_id, voucher_id: @voucher.id).destroy
        end
        params[:voucher][:project_id].map { |x| x.to_i }.reject { |p| p == 0 }.each do |project_id|
          VouchersProjects.create(project_id: project_id, voucher_id: @voucher.id)
        end
        format.html { redirect_to admin_vouchers_path, notice: 'Cập nhật voucher thành công.' }
      end
    end
  end

  def destroy
    authorize @voucher
    @voucher.projects.pluck(:id).each do |project_id|
      VouchersProjects.find_by(project_id: project_id).destroy
    end
    @voucher.destroy
    respond_to do |format|
      format.html { redirect_to admin_vouchers_path, notice: 'Xóa thành công.' }
      format.json { head :no_content }
    end
  end

  def send_voucher
    if params[:cs_ticket_id]
      cs_ticket = CsTicket.find(params[:cs_ticket_id]).update_attribute('voucher_id', params[:voucher_id])
    end
    if params[:customer_id] && params[:voucher_id] && params[:deal_id]
      voucher_customer = VouchersCustomers.new(customer_id: params[:customer_id], voucher_id: params[:voucher_id], deal_id: params[:deal_id])
      voucher_customer.save
      voucher = Voucher.find(params[:voucher_id])
      if params[:value_option_send_voucher] == '1'
        DealMailer.notification_send_voucher(params[:email], voucher.content).deliver
        deal = Deal.find(params[:deal_id])
        deal.customer_persona.update_attribute('email', params[:email])
      end
      render json: { status: 'success', voucher_id: voucher_customer.id}
    else
      render json: { status: 'error' }
    end
  end

  private

  def set_voucher
    @voucher = Voucher.find(params[:id])
  end

  def voucher_params
    if params[:voucher][:project_id].present?
      if params[:voucher][:project_id].reject { |p| p.empty? }.first == '0'
        params[:voucher][:project_id] = Project.all.order('labels @> ARRAY[[0]]::integer[] DESC NULLS LAST, projects.updated_at DESC').pluck(:id)
      else
        params[:voucher][:project_id] = params[:voucher][:project_id].uniq.reject { |p| p.empty? }
      end
    end
    params.require(:voucher).permit(:voucher_type, { :project_id => [] }, :content, :applicable_condition)
  end

end
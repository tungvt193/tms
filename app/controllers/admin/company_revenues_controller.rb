class Admin::CompanyRevenuesController < Admin::BaseController

  before_action :get_common_data

  def index
    authorize :company_revenue, :index?
    @revenue = CompanyRevenue.new
    @exclusive_revenues = CompanyRevenue.where(objectable_type: 'Project', objectable_id: @project.id)
    @exception_revenues = CompanyRevenue.where(objectable_type: 'Product', objectable_id: @project.product_ids)
  end

  def new_exclusive_revenue
    @company_revenue = CompanyRevenue.new
    render partial: 'admin/company_revenues/new_exclusive_revenue', locals: { real_estate_types: @real_estate_types, company_revenue: @company_revenue }
  end

  def new_exception_revenue
    @company_revenue = CompanyRevenue.new
    @exception_products = @project.products.where(source: 1)
    render partial: 'admin/company_revenues/new_exception_revenue', locals: { exception_products: @exception_products, company_revenue: @company_revenue }
  end

  def create
    @company_revenue = CompanyRevenue.new(company_revenue_params)
    respond_to do |format|
      @company_revenue.save
      format.js
    end
  end

  def update
    @company_revenue = CompanyRevenue.find_by(id: params[:id])
    respond_to do |format|
      @company_revenue.update(company_revenue_params)
      format.js
    end
  end

  def destroy
    @company_revenue = CompanyRevenue.find_by(id: params[:id])
    @company_revenue.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private

  def get_common_data
    @project = Project.find_by(id: params[:project_id])
    @real_estate_types = Constant::PROJECT_REAL_ESTATE_TYPE.invert.select { |k, v| @project.real_estate_type.include?(v) }
    @exception_products = @project.products.where(source: 1)
  end

  def company_revenue_params
    if params[:company_revenue][:apply_period].present?
      params[:company_revenue][:start_date], params[:company_revenue][:end_date] = params[:company_revenue][:apply_period].split(' - ')
    end
    params.require(:company_revenue).permit(:objectable_type, :objectable_id, :percentage, :start_date, :end_date, :real_estate_type, :pseudo_id)
  end
end

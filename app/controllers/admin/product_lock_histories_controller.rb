class Admin::ProductLockHistoriesController < Admin::BaseController
  def index
    authorize :product_lock_history, :index?
    data = []
    product_lock_histories = ProductLockHistory.includes(:project, :product, :user)
    data << product_lock_histories.with_state(:pending).order(updated_at: :asc)
    data << product_lock_histories.with_state(:waiting_reti_acceptance).order(deadline_for_sale_admin: :asc)
    data << product_lock_histories.with_state(:deposit_confirmation).order(deadline_for_sale: :asc)
    data << product_lock_histories.with_state(:locking).order(updated_at: :desc)
    data << product_lock_histories.with_state(:success).order(updated_at: :desc)
    data << product_lock_histories.with_state(:failed).order(updated_at: :desc)
    data << product_lock_histories.with_state(:out_of_time).order(updated_at: :desc)
    data << product_lock_histories.with_state(:canceled).order(updated_at: :desc)
    @pagy, @product_lock_histories = pagy_array(data.flatten, items: Settings.admin.pagination.role.per_page)
  end
  
  def edit

  end

  def update
    product_lock_history = ProductLockHistory.find(params[:id])
    authorize product_lock_history
    respond_to do |format|
      if product_lock_history.update(product_lock_history_params)
        if product_lock_history.state == 'deal_confirmation'
          NotificationService.product_sold(product_lock_history)
          product_lock_history.product.update(state: 'sold')
        elsif product_lock_history.state == 'failed'
          product_lock_history.product.update(state: 'for_sale')
        end
        format.html { redirect_to admin_dashboard_index_path, notice: product_lock_history.message_update_state }
      end
    end
  end

  private

  def product_lock_history_params
    params.require(:product_lock_history).permit(:state, :failure_reason)
  end
end

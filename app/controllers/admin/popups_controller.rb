class Admin::PopupsController < Admin::BaseController
  before_action :load_popup_groups, only: :index
  before_action :load_popups, only: [:edit, :update]

  def index
    authorize :banner, :index?
  end

  def edit
    authorize :banner, :index?
  end

  def update
    authorize :banner, :index?
    respond_to do |format|
      @popup = case params[:popup].try(:[], :id)
      when @guide_tour_home.id.to_s
        @guide_tour_home
      when @guide_tour_project_detail.id.to_s
        @guide_tour_project_detail
      end
      if @popup.present?
        @current_tab = @popup.position
        if @popup.update(popup_params)
          flash[:notice] = 'Popup đã được cập nhật thành công.'
          format.html { redirect_to edit_admin_popup_path(params[:group], tab: @current_tab) }
        else
          flash[:error] = 'Cập nhật không thành công. Vui lòng kiểm tra lại.'
          format.html { render :edit }
        end
      else
        popups_params = params.to_unsafe_h.select {|key, value| Popup.pluck(:id).map(&:to_s).include?(key) }
        if Popup.update(popups_params.keys, popups_params.values)
          flash[:notice] = 'Popup đã được cập nhật thành công.'
          format.html { redirect_to edit_admin_popup_path(params[:group]) }
        else
          flash[:error] = 'Cập nhật không thành công. Vui lòng kiểm tra lại.'
          format.html { render :edit }
        end
      end
    end
  end

  private

  def load_popup_groups
    @popup_groups = Popup.pluck(:group).uniq
  end

  def load_popups
    @popups = Popup.where(group: params[:group]).order(position: :asc)
    @guide_tour_home = @popups.find_by(position: :home)
    @guide_tour_project_detail = @popups.find_by(position: :project_detail)
    @current_tab = params[:tab]
  end

  def popup_params
    params.require(:popup).permit(Popup::ATTRS)
  end
end

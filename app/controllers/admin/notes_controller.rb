class Admin::NotesController < Admin::BaseController
  before_action :load_note, only: [:update, :create, :destroy]

  def create
    @note.assign_attributes note_params
    respond_to do |format|
      if @note.valid?
        @note.save
        @not_errors = true
        format.js {render layout: false}
      else
        @not_errors = false
        format.js {render layout: false}
      end
    end
  end

  def update
    @note.assign_attributes note_params
    is_canceled = params[:cancel] == 'true'
    @note.reload if is_canceled
    if params[:deal_id].present?
      @deal = @note.objectable
    end
    respond_to do |format|
      if @note.valid?
        @note.save unless is_canceled
        @not_errors = true
        format.js {render layout: false}
      else
        @not_errors = false
        format.js {render layout: false}
      end
    end
  end

  def destroy
    respond_to do |format|
      @note.destroy
      format.js {render layout: false}
    end
  end

  private
  def load_note
    @note = if params[:id]
                    Note.find_by(id: params[:id])
                  elsif params[:deal_id].present?
                    @deal = Deal.find_by( id: params[:deal_id])
                    @deal.notes.build
                  end
  end

  def note_params
    params.require(:note).permit(:created_by_id, :content)
  end
end

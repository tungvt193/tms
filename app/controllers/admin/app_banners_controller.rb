class Admin::AppBannersController < Admin::BaseController
  before_action :set_banner, only: [:index, :update, :destroy_attachment, :destroy]

  def index
    authorize :banner, :index?
    @banners = Banner.banner_for_app.order(position: :asc)
    @pagy, @banners = pagy_array(@banners, items: Settings.admin.pagination.banner.per_page)
  end

  def create
    @banner = Banner.new(banner_params)
    authorize @banner

    respond_to do |format|
      if @banner.save
        format.html { redirect_to admin_app_banners_path, notice: 'App banner đã được tạo thành công.' }
        format.json { render :index, status: :ok, location: @banner }
      else
        format.html { render :index }
        format.json { render json: @banner.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    authorize @banner
    respond_to do |format|
      if @banner.update(banner_params)
        format.html { redirect_to admin_app_banners_path, notice: 'App banner đã được cập nhật thành công.' }
        format.json { render :index, status: :ok, location: @banner }
      else
        format.html { render :index }
        format.json { render json: @banner.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    authorize @banner
    @banner.destroy
    respond_to do |format|
      format.html { redirect_to admin_app_banners_url, notice: 'Xóa thành công.' }
      format.json { head :no_content }
    end
  end

  def destroy_attachment
    delete_attachment(params[:type])
  end

  private
    def set_banner
      @banner = Banner.find_by(banner_type: 2, id: params[:id])
    end

    def banner_params
      params.require(:banner).permit(:banner_type, :banner_app)
    end

    def delete_attachment type
      @banner.send("remove_#{type}!")
      if @banner.valid?
        @banner.save
        @banner.reload
        render json: {}
      else
        render json: { error: @banner.errors[params[:type].to_sym]&.first }
      end
    end
end

class Admin::CustomDetailsController < Admin::BaseController
  before_action :set_project, only: [:index, :create]

  def index
    authorize :custom_details, :index?
    @is_disabled = !policy(:custom_details).create?
  end

  def create
    authorize :custom_details, :create?
    respond_to do |format|
      if @project.update(project_params)
        format.html { redirect_to admin_project_custom_details_path(@project.id), notice: "Tuỳ chỉnh chi tiết của dự án \"#{@project.name}\" đã được cập nhật thành công." }
        format.json { render :index, status: :created, location: @project }
      else
        format.html { render :index }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_project
    @project = Project.find(params[:project_id])
  end

  def project_params
    params.require(:project).permit(:custom_description, :custom_sale_policy, :custom_utilities, :meta_title, :meta_description)
  end
end

class Admin::FacebooksController < Admin::BaseController
  require 'openssl'

  def callback
    render json: {} and return unless current_user.bi_ba?
    facebook_api = FacebookApi.find_or_initialize_by user_id: current_user.id
    # Get user access token - expired 1 day
    response_body = get_api_request "oauth/access_token?
    client_id=#{ENV['FACEBOOK_APP_ID']}&client_secret=#{ENV['FACEBOOK_APP_SECRET']}&code=#{params[:code]}&redirect_uri=#{admin_facebook_callback_url}"
    user_access_token = response_body["access_token"]
    facebook_api.user_access_token = user_access_token

    # Get long-lived user access token - expired 60 days
    response_body = get_api_request "oauth/access_token?
    grant_type=fb_exchange_token&client_id=#{ENV['FACEBOOK_APP_ID']}&client_secret=#{ENV['FACEBOOK_APP_SECRET']}&fb_exchange_token=#{user_access_token}"
    long_lived_user_access_token = response_body["access_token"]
    facebook_api.long_lived_user_access_token = long_lived_user_access_token

    # get long-lived access token - expired unlimited
    response_body = get_api_request "oauth/client_code?
    client_id=#{ENV['FACEBOOK_APP_ID']}&client_secret=#{ENV['FACEBOOK_APP_SECRET']}&redirect_uri=#{admin_facebook_callback_url}&access_token=#{long_lived_user_access_token}"
    code = response_body["code"]

    response_body = get_api_request "oauth/access_token?
    code=#{code}&client_id=#{ENV['FACEBOOK_APP_ID']}&redirect_uri=#{admin_facebook_callback_url}"
    long_lived_access_token = response_body["access_token"]
    facebook_api.long_lived_access_token = long_lived_access_token

    response_body = get_api_request "oauth/access_token?
    client_id=#{ENV['FACEBOOK_APP_ID']}&client_secret=#{ENV['FACEBOOK_APP_SECRET']}&grant_type=client_credentials"
    long_lived_access_token = response_body["access_token"]
    facebook_api.long_lived_access_token = long_lived_access_token

    # Get current user facebook UID
    response_body = get_api_request "me?access_token=#{user_access_token}"
    user_uid = response_body["id"]
    facebook_api.uid = user_uid

    # Get long-lived page access token - expired unlimited
    response_body = get_api_request "#{user_uid}/accounts?access_token=#{long_lived_user_access_token}"
    facebook_api.pages = response_body

    facebook_api.appsecret_proof = OpenSSL::HMAC.hexdigest('sha256', long_lived_access_token, ENV['FACEBOOK_APP_SECRET'])
    facebook_api.save
    render json: {}
  end

  def new

  end

  private
  def get_api_request endpoint
    uri = URI "https://graph.facebook.com/v9.0/#{endpoint}"
    response_body = JSON.parse Net::HTTP.get_response(uri).body
  end
end

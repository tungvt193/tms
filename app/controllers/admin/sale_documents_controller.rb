class Admin::SaleDocumentsController < Admin::BaseController
  protect_from_forgery with: :null_session
  
  before_action :set_project
  before_action :set_sale_document

  def index
    authorize :project, :update?
    @sale_kits = @project.sale_documents.sale_kit.order(position: :asc)
    @sale_policies = @project.sale_documents.sale_policy.order(position: :asc)
    @investor_posts = @project.investor_posts
    @sale_kit = SaleDocument.new
    @sale_policy = SaleDocument.new
  end

  def create
    position = @project.sale_documents.where(sale_document_type: params[:project][:sale_document_type]).pluck(:position).min || 0
    file_params = []
    if params[:project][:files].present?
      params[:project][:files].each_with_index do |file, index|
        params[:project][:sale_documents_attributes].values[index][:file] = file
      end
      params[:project][:sale_documents_attributes].each do |sale_document|
        position = position - 1
        file_params << {project_id: @project.id, title: sale_document.second[:title], sale_document_type: sale_document.second[:sale_document_type].to_i, file: sale_document.second[:file], position: position}
      end
      SaleDocument.create(file_params)
      NotificationService.update_sale_document(@project)
      redirect_to admin_project_sale_documents_path, notice: 'Thêm mới tài liệu thành công'
    end
  end

  def update_sale_kit
    @sale_kit = SaleDocument.find(params[:sale_document][:id])
    @sale_kit.assign_attributes(sale_document_params)
    respond_to do |format|
      if @sale_kit.save
        format.html { redirect_to admin_project_sale_documents_path, notice: 'Chỉnh sửa tài liệu thành công.' }
      else
        format.js
      end
    end
  end

  def update_sale_policy
    @sale_policy = SaleDocument.find(params[:sale_document][:id])
    @sale_policy.assign_attributes(sale_document_params)
    respond_to do |format|
      if @sale_policy.save
        format.html { redirect_to admin_project_sale_documents_path, notice: 'Chỉnh sửa tài liệu thành công.' }
      else
        format.js
      end
    end
  end


  def destroy
    @sale_document.destroy
    respond_to do |format|
      format.html { redirect_to admin_project_sale_documents_url, notice: "Xóa #{@sale_document.sale_document_type === 'sale_kit' ? 'tài liệu' : 'chính sách bán hàng'} thành công." }
      format.json { head :no_content }
    end
  end

  def update_media_url
    @project.image_folder_url = params[:project][:image_folder_url]
    @project.video_folder_url = params[:project][:video_folder_url]
    @project.layout_url = params[:project][:layout_url]

    respond_to do |format|
      if @project.save
        format.js
      else
        format.js
      end
    end
  end

  def new_investor_post
    @investor_post = InvestorPost.new
    render partial: 'admin/sale_documents/new_investor_post', locals: { post: @investor_post }
  end

  def create_investor_post
    @investor_post = @project.investor_posts.new(investor_post_params)
    respond_to do |format|
      @investor_post.save
      format.js
    end
  end

  def update_investor_post
    @investor_post = InvestorPost.find_by(id: params[:id])
    respond_to do |format|
      @investor_post.update(investor_post_params)
      format.js
    end
  end

  def destroy_investor_post
    @investor_post = InvestorPost.find_by(id: params[:id])
    @investor_post.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  def download
    file = open(@sale_document.file.url)
    send_data(file.read, type: file.content_type, x_sendfile: true, filename: "#{@sale_document.title}.#{file.content_type.split('/').last}")
  end

  def reorder_position
    ReorderSaleDocumentPositionJob.perform_later(params[:current_document_id], params[:prev_document_id], @project)
  end

  private

  def set_project
    @project = Project.find_by(id: params[:project_id])
  end

  def set_sale_document
    @sale_document = SaleDocument.find_by(id: params[:id])
  end

  def sale_document_params
    params.require(:sale_document).permit(:project_id, :title, :file, :sale_document_type)
  end

  def investor_post_params
    params.require(:investor_post).permit(:title, :post_url, :pseudo_id)
  end
end

class Admin::SameDealsController < Admin::BaseController
  include DateTimeHelper

  def index
    @deals = Deal.where.not(same_deals: []).search_same_deals(params).order(created_at: :desc)
    @pagy, @deals = pagy(@deals, items: Settings.admin.pagination.same_deal.per_page)
  end

  def show
    @deal = Deal.find(params[:id])
    @deals = Deal.where(id: @deal.same_deals).order(created_at: :desc)
    @deals = [@deal] + @deals

    @data = {
      'Sản Phẩm' => nil,
      'Dự án quan tâm' => [],
      'Mã sản phẩm' => [],
      'Khách Hàng' => nil,
      'Khách hàng' => [],
      'Số điện thoại' => [],
      'Trạng thái liên hệ' => [],
      'Nội dung tương tác' => [],
      'Giao Dịch' => nil,
      'Trạng thái giao dịch' => [],
      'Nguồn' => [],
      'Người chăm sóc' => [],
      'Ngày tạo' => [],
      'Ngày cập nhật' => [],
    }

    @deals.each do |deal|
      @data['Dự án quan tâm'].push(deal.project&.name)
      @data['Mã sản phẩm'].push(deal.product&.code)
      @data['Khách hàng'].push(deal.customer&.name)
      @data['Số điện thoại'].push(deal.customer&.phone_number)
      @data['Trạng thái liên hệ'].push(Constant::CONTACT_STATUSES[deal.contact_status])
      @data['Nội dung tương tác'].push(Constant::INTERACTION_CHECKLIST[deal.interaction_detail])
      @data['Trạng thái giao dịch'].push(I18n.t("deal.states.#{deal.state}"))
      @data['Nguồn'].push(Constant::DEAL_SOURCES[deal.source])
      @data['Người chăm sóc'].push(deal.assignee&.full_name)
      @data['Ngày tạo'].push( format_regulation(deal.created_at) )
      @data['Ngày cập nhật'].push( format_regulation(deal.updated_at) )
    end
  end

  def autocomplete
    @deals = Deal.where.not(same_deals: [])
    render json: @deals.autocomplete_same_deals(params[:query])
  end
end

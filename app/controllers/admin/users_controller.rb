class Admin::UsersController < Admin::BaseController
  before_action :find_user, except: [:index, :create, :new, :search, :autocomplete, :account_autocomplete, :profile, :update_profile, :subscribe, :export_users]
  before_action :get_city, only: [:show, :edit, :new, :update, :create, :destroy, :profile, :update_profile]

  def index
    authorize :user, :index?
    params[:account_type] ||= 'internal'
    @users = User.includes(:role).where(account_type: params[:account_type]).order(id: :asc)
    query = params[:query].presence || "*"
    @users = @users.search_result(query)
  end

  def show
    authorize @user
  end

  def edit
    authorize @user
  end

  def new
    @user = User.new(account_type: params[:account_type])
    @user.role_id = get_role(@user)
    authorize @user
  end

  def update
    authorize @user
    @user.skip_gender_validation = true if @user.account_type == 'agent'
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to edit_admin_user_path(@user.id), notice: 'Tài khoản đã được chỉnh sửa thành công.' }
        format.json { render :edit, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def create
    @user = User.new(user_params)
    @user.role_id = get_role(@user) unless @user.account_type == 'internal'
    @user.created_by_id = current_user.id 
    @user.skip_gender_validation = true if @user.account_type == 'agent'
    authorize @user
    respond_to do |format|
      if @user.save
        format.html { redirect_to edit_admin_user_path(@user.id), notice: 'Tài khoản đã được tạo thành công.' }
        format.json { render :edit, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    authorize @user
    respond_to do |format|
      begin
        @user.destroy
        format.html { redirect_to admin_users_url(account_type: params[:account_type]), notice: 'Xóa thành công.' }
        format.json { head :no_content }
      rescue Exception => e
        if e.class.to_s == "Ancestry::AncestryException"
          flash[:error] = "Không thể xóa bản ghi này do có cấp dưới."
        else
          flash[:error] = e.message
        end
        format.html { redirect_to admin_users_url(account_type: params[:account_type]) }
      end
    end
  end

  def search
    redirect_to admin_users_path(query: params[:query])
  end

  def autocomplete
    render json: User.autocomplete_result(params[:query], params[:ancestry])
  end

  def account_autocomplete
    render json: User.where(account_type: request.referrer&.include?('agent') ? 'agent' : 'internal').account_autocomplete_result(params[:query])
  end

  def profile
    @user = current_user
  end

  def update_profile
    respond_to do |format|
      current_user.assign_attributes(profile_params)
      unless params['sync-gg-calendar'].present?
        current_user.assign_attributes({ 'access_token': nil, 'refresh_token': nil, 'expires_at': nil })
      end
      if current_user.save
        bypass_sign_in current_user
        format.html { redirect_to profile_admin_users_path, notice: 'Tài khoản đã được chỉnh sửa thành công.' }
        format.json { render :profile, status: :ok, location: current_user }
      else
        @user = current_user
        format.html { render :profile }
        format.json { render json: current_user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy_attachment
    current_user.remove_avatar!
    if current_user.valid?
      current_user.save
      current_user.reload
      render json: {}
    else
      render json: { error: current_user.errors[params[:type].to_sym]&.first }
    end
  end

  def subscribe
    if params['subscribe'].to_s == 'false'
      current_user.unsubscribe
      return render body: nil
    end
    current_user.update(
      auth_key: params[:subscription][:keys][:auth],
      endpoint: params[:subscription][:endpoint],
      p256dh_key: params[:subscription][:keys][:p256dh],
      subscribe: true
    )
    session.delete(:refresh_subscribe)
    render body: nil
  end

  def export_users
    account_type = params[:account_type]
    file_name = "Tai_khoan_#{I18n.transliterate(Constant::ACCOUNT_TYPE.invert.key(account_type).downcase)}_#{Time.now.strftime('%d_%m_%Y')}.xlsx"
    users = User.includes(:role).where(account_type: params[:account_type]).order(full_name: :asc)
    export_to_xlsx UserService.new(nil, users).export, file_name
  end

  private

  def get_city
    @cities = RegionData.cities.pluck(:name, :id)
  end

  def find_user
    @user = User.includes(:role).find_by_id(params[:id])
    redirect_to admin_users_path unless @user.present?
  end

  def user_params
    if params[:user][:phone_number] == ""
      params[:user] = params[:user].reject { |n| %w(phone_number country_code).include?(n) }
    end
    if params[:user][:password].present? || params[:user][:password_confirmation].present?
      params.require(:user).permit(:email, :password, :password_confirmation, :full_name, :account_type, :role_id, :deactivated, :is_superadmin, :gender, :phone_number, :city_id, :main_job, :country_code)
    else
      params.require(:user).permit(:email, :full_name, :account_type, :role_id, :deactivated, :is_superadmin, :gender, :phone_number, :city_id, :main_job, :country_code)
    end
  end

  def get_role (user)
    if @user.account_type == 'agent'
      @role_id = Role.find_by(name: 'Cộng tác viên').id
      @user.role_id = @role_id
    elsif @user.account_type == 'f2'
      @role_id = Role.find_by(name: 'F2').id
      @user.role_id = @role_id
    end
  end

  def profile_params
    if params[:user][:phone_number] == ""
      params[:user] = params[:user].reject { |n| %w(phone_number country_code).include?(n) }
    end
    if params[:user][:password_confirmation].present? || params[:user][:password].present?
      params.fetch(:user, {}).permit(:avatar, :password, :password_confirmation, :gender, :phone_number, :city_id, :main_job, :country_code)
    else
      params.fetch(:user, {}).permit(:avatar, :gender, :phone_number, :city_id, :main_job, :country_code)
    end
  end

  def export_to_xlsx(file, file_name)
    tempfile = "#{Rails.root}/tmp/tempfile.xlsx"

    file.serialize tempfile
    ::File.open tempfile, "r" do |f|
      send_data f.read, filename: file_name, type: "application/xlsx"
    end
    ::File.delete tempfile
  end
end

class Admin::ProjectNewUpdatesController < Admin::BaseController

  before_action :set_project_new_update, only: [:edit, :update, :destroy]
  before_action :get_list_projects, only: [:new, :create, :edit, :update]

  def index
    authorize :project_new_update, :index?
    @project_new_updates = ProjectNewUpdate.all.order(created_at: :desc)
  end

  def new
    @project_new_update = ProjectNewUpdate.new
    authorize @project_new_update
  end

  def create
    @project_new_update = ProjectNewUpdate.new(project_new_update_param)
    authorize @project_new_update
    respond_to do |format|
      if @project_new_update.save
        format.html { redirect_to admin_project_new_updates_path, notice: 'Tin nhắn đã được tạo thành công.' }
        format.json { render :edit, status: :created, location: @project_new_update }
      else
        format.html { render :new }
        format.json { render json: @project_new_update.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    authorize @project_new_update
    @projects = Project.all.pluck(:name, :id)
  end

  def update
    authorize @project_new_update
    respond_to do |format|
      if @project_new_update.update(project_new_update_param)
        format.html { redirect_to admin_project_new_updates_path, notice: 'Tin nhắn đã được chỉnh sửa thành công.' }
        format.json { render :edit, status: :ok, location: @project_new_update }
      else
        format.html { render :edit }
        format.json { render json: @project_new_update.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    authorize @project_new_update
    @project_new_update.destroy
    respond_to do |format|
      format.html { redirect_to admin_project_new_updates_path, notice: 'Xóa thành công.' }
      format.json { head :no_content }
    end
  end

  private

  def set_project_new_update
    @project_new_update = ProjectNewUpdate.find(params[:id])
  end

  def project_new_update_param
    params[:project_new_update][:created_by_id] = current_user.id if current_user.present?
    params.require(:project_new_update).permit(:project_id, :title, :content, :created_by_id)
  end

  def get_list_projects
    @projects = Project.all.pluck(:name, :id)
  end
end

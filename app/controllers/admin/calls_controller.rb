class Admin::CallsController < Admin::BaseController
  before_action :load_call, only: [:update, :create, :destroy]

  def create
    @call.assign_attributes call_params
    @call.quick_create = params[:form_id] == 'quick_create_call'
    respond_to do |format|
      if @call.valid?
        @call.save
        @not_errors = true
        format.js { render layout: false }
      else
        @not_errors = false
        format.js { render layout: false }
      end
    end
  end

  def update
    @call.assign_attributes call_params
    is_canceled = params[:cancel] == 'true'
    @call.reload if is_canceled
    respond_to do |format|
      if @call.valid?
        @call.save unless is_canceled
        @not_errors = true
        format.js { render layout: false }
      else
        @not_errors = false
        format.js { render layout: false }
      end
    end
  end

  def destroy
    respond_to do |format|
      @call.destroy
      format.js { render layout: false }
    end
  end

  private

  def load_call
    @deal = Deal.find_by(id: params[:deal_id])
    @call = if params[:id]
              Call.find_by(id: params[:id])
            else
              @deal.calls.build
            end
  end

  def call_params
    params.require(:call).permit(:deal_id, :created_by_id, :contact_date, :result)
  end
end

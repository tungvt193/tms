class Admin::LayoutsController < Admin::BaseController
  before_action :load_layout, only: [:new, :edit, :update, :create, :destroy]

  def index
    authorize :layout, :index?
    @project = Project.find_by(id: params[:project_id])
    @pagy, @layouts = pagy(@project.layouts, items: Settings.admin.pagination.layout.per_page)
  end

  def show
    @project = Project.find_by(id: params[:project_id])
    @layout = @project.layouts.find_by(id: params[:id])
    authorize @layout
  end

  def new
    authorize @layout
  end

  def edit
    authorize @layout
  end

  def create
    if params[:layout].try(:[], :image)
      @uploaded = params[:layout][:image]
      params[:layout].delete(:image)
    end

    session[:layout_params].deep_merge!(layout_params) if params[:layout]
    if session[:layout_step] == '2'
      session[:layout_params]['subdivision_id'] = layout_params[:subdivision_id]
      session[:layout_params]['block_id'] = layout_params[:block_id]
      session[:layout_params]['floor_ids'] = layout_params[:floor_ids]
    end

    @layout = @project.layouts.build session[:layout_params]
    authorize @layout
    load_divisions

    if @uploaded
      @layout.image = @uploaded
    end

    @layout.current_step = session[:layout_step]

    if !@layout.image.present? && session[:image].present?
      @layout.image = URI.parse(session[:image].try(:[], 'url')).open
      # @layout.image = URI.parse("#{root_url.delete_suffix('/')}#{session[:image].try(:[], 'url')}" ).open
    end

    params[:back_button] ? @layout.back : @layout.next

    if @layout.valid?
      if params[:back_button]
        @layout.previous_step
      elsif @layout.last_step?
        @layout.save if @layout.all_valid?
      else
        @layout.next_step
      end

      session[:layout_step] = @layout.current_step
    end
    session[:image] = @layout.image if @layout.image.present?

    if @layout.new_record?
      render :new
    else
      session[:layout_step] = session[:layout_params] = session[:image] = nil
      redirect_to admin_project_layouts_path(@project), notice: 'Thêm mới mặt bằng thành công!'
    end
  end

  def update
    authorize @layout
    if params[:layout].try(:[], :image)
      @uploaded = params[:layout][:image]
      params[:layout].delete(:image)
    end

    session[:layout_params].deep_merge!(layout_params) if params[:layout]
    if session[:layout_step] == '2'
      session[:layout_params]['subdivision_id'] = layout_params[:subdivision_id]
      session[:layout_params]['block_id'] = layout_params[:block_id]
      session[:layout_params]['floor_ids'] = layout_params[:floor_ids]
    end

    @layout.assign_attributes session[:layout_params]
    load_divisions

    if @uploaded
      @layout.image = @uploaded
    end

    @layout.current_step = session[:layout_step]

    if session[:image].present?
      @layout.image = URI.parse(session[:image].try(:[], 'url')).open
      # @layout.image = URI.parse("#{root_url.delete_suffix('/')}#{session[:image].try(:[], 'url')}" ).open
    end

    params[:back_button] ? @layout.back : @layout.next

    if @layout.valid?
      if params[:back_button]
        @layout.previous_step
      elsif @layout.last_step?
        if @layout.all_valid?
          @layout.save
          @is_redirect = true
        end
      else
        @layout.next_step
      end

      session[:layout_step] = @layout.current_step
    end
    session[:image] = @layout.image if @layout.image.present?

    unless @is_redirect
      render :edit
    else
      session[:layout_step] = session[:layout_params] = session[:image] = nil
      redirect_to admin_project_layouts_path(@project), notice: 'Sửa mặt bằng thành công!'
    end
  end

  def destroy
    authorize @layout
    @layout.destroy
    respond_to do |format|
      format.html { redirect_to admin_project_layouts_path(@project), notice: 'Mặt bằng đã được xoá thành công.' }
      format.json { head :no_content }
    end
  end

  def get_divisions
    divisions = if params[:parent] == 'level' && params[:child] == 'layout_subdivision_id'
                  session[:layout_params]['subdivision_id'] = session[:layout_params]['block_id'] = session[:layout_params]['floor_ids'] = nil
                  Division.where(project_id: params[:project_id], label: 0, level: params[:parent_id])
                elsif params[:parent] == 'block' && params[:child] == 'layout_floor_ids'
                  existed_floor_ids = Layout.where(block_id: params[:parent_id]).pluck :floor_ids
                  Division.where(parent_id: params[:parent_id]).where.not(id: existed_floor_ids.flatten)
                else
                  existed_block_ids = Layout.where(subdivision_id: params[:parent_id], level: 1).pluck :block_id
                  Division.where(parent_id: params[:parent_id]).where.not(id: existed_block_ids)
                end

    respond_to do |format|
      format.json { render json: divisions }
    end
  end

  private

  def load_layout
    session[:layout_step] = session[:layout_params] = session[:image] = nil if ['edit', 'new'].include? params[:action]
    session[:layout_params] ||= {}
    @project = Project.find_by(id: params[:project_id])
    @layout = @project.layouts.find_by(id: params[:id]) || @project.layouts.build(session[:layout_params])
    @layout.current_step = session[:layout_step]
    @subdivisions = Division.where(project_id: params[:project_id]).pluck(:name, :id)
    @blocks = Division.where(parent_id: @layout&.subdivision_id).pluck(:name, :id)
    @floors = Division.where(parent_id: @layout&.block_id).pluck(:name, :id)
  end

  def load_divisions
    @blocks = Division.where(parent_id: @layout&.subdivision_id).pluck(:name, :id)
    @floors = Division.where(parent_id: @layout&.block_id).pluck(:name, :id)
  end

  def load_layouts
    @project = Project.find_by(id: params[:project_id])
  end

  def layout_params
    params.require(:layout).permit(Layout::LAYOUT_ATTRS)
  end
end

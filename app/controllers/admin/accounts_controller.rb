class Admin::AccountsController < Admin::BaseController
  before_action :set_account, only: [:show, :edit, :update, :destroy]

  def index
    authorize :account, :index?
    @accounts = Account.all
  end

  def show
    authorize @account
  end

  def search
    redirect_to admin_accounts_path(query: params[:query])
  end

  def autocomplete
    render json: Account.autocomplete_result(params[:query])
  end

  def new
    @account = Account.new
    authorize @account
    @is_disabled = !policy(@account).create?
  end

  def edit
    authorize @account
    @is_disabled = !policy(@account).update?
  end

  def create
    @account = Account.new(account_params)
    authorize @account
    respond_to do |format|
      if @account.save
        format.html { redirect_to edit_admin_account_path(@account.id), notice: 'Tài khoản đã được tạo thành công.' }
        format.json { render :edit, status: :created, location: @account }
      else
        format.html { render new_admin_account_path }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    authorize @account
    respond_to do |format|
      if @account.update(account_params)
        format.html { redirect_to edit_admin_account_path(@account.id), notice: 'Tài khoản đã được chỉnh sửa thành công.' }
        format.json { render :edit, status: :ok, location: @account }
      else
        format.html { render :edit }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    authorize @account
    @account.destroy
    respond_to do |format|
      format.html { redirect_to admin_accounts_url, notice: 'Xóa thành công.' }
      format.json { head :no_content }
    end
  end

  private

  def set_account
    @account = Account.find(params[:id])
  end

  def account_params
    params[:account].delete('active') if params[:account] && !policy(@account || Account.new).change_state?
    if params[:account][:password].present?
      params.require(:account).permit(:email, :password, :password_confirmation, :full_name, :phone, :phone_verify, :identity_card, :active, 
                                      reason_locks_attributes: [:id, :content, :_destroy])
    else
      params.require(:account).permit(:email, :full_name, :phone, :phone_verify, :identity_card, :active, 
                                      reason_locks_attributes: [:id, :content, :_destroy])
    end
  end
end

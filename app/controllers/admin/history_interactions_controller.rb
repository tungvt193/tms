class Admin::HistoryInteractionsController < Admin::BaseController

  before_action :load_history_interaction, only: [:update, :create, :cancel]

  def create
    respond_to do |format|
      @history_interaction.save if @history_interaction.valid?
      @history_interaction_group = @deal.history_interactions.order(interaction_date: :desc, created_at: :desc).group_by{|history| history&.interaction_date.strftime("%d/%m/%Y")} 
      format.js
    end
  end

  def update
    respond_to do |format|
      @history_interaction.save if !@history_interaction.canceled && @history_interaction.valid?
      @history_interaction_group = @deal.history_interactions.order(interaction_date: :desc, created_at: :desc).group_by{|history| history&.interaction_date.strftime("%d/%m/%Y")} 
      format.js
    end
  end

  private

  def load_history_interaction
    @deal = Deal.find_by(id: params[:deal_id])
    @history_interaction = params[:id].present? ? HistoryInteraction.find_by(id: params[:id]) : @deal.history_interactions.build
    @history_interaction.assign_attributes history_interaction_params
    if params[:cancel] == 'true'
      @history_interaction.reload
      @history_interaction.canceled = true
    end
    @history_interaction.pseudo_id = params[:history_interaction][:pseudo_id]
  end

  def history_interaction_params
    params.require(:history_interaction).permit(:deal_id, :created_by_id, :interaction_type, :interaction_date, :interaction_content)
  end
end

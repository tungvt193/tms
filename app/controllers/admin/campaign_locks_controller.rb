class Admin::CampaignLocksController < Admin::BaseController
  before_action :set_campaign_lock, only: [:edit, :update, :destroy]
  before_action :get_data

  def index
    authorize :campaign_lock, :index?
    campaign_locks = []
    campaign_locks << CampaignLock.where(state: 'upcoming').order(session_opening_time: :asc)
    campaign_locks << CampaignLock.where(state: 'locking').order(session_opening_time: :desc)
    campaign_locks << CampaignLock.where(state: 'out_of_stock').order(session_opening_time: :desc)
    @campaign_locks = campaign_locks.flatten
    @pagy, @campaign_locks = pagy_array(@campaign_locks, items: Settings.admin.pagination.role.per_page)
  end

  def new
    @campaign_lock = CampaignLock.new
    @projects = Project.all.pluck(:name, :id)
    @users = User.internal.joins(:role).where(roles: { name: ['Sale Admin', 'Sale Admin Chi Nhánh'] }).pluck(:full_name, :id)
    authorize @campaign_lock
    @is_disabled = !policy(@campaign_lock).create?
  end

  def edit
    @projects = Project.all.pluck(:name, :id)
    @users = User.internal.joins(:role).where(roles: { name: ['Sale Admin', 'Sale Admin Chi Nhánh'] }).pluck(:full_name, :id)
    authorize @campaign_lock
    @high_products = Product.where(id: @campaign_lock.products, level: 0)
    @low_products = Product.where(id: @campaign_lock.products, level: 1)
    @is_disabled = true
  end

  def create
    @campaign_lock = CampaignLock.new(campaign_lock_params)
    authorize @campaign_lock
    respond_to do |format|
      if import_product_params[:products_hot].present? || import_product_params[:products_inventory].present?
        @campaign_lock.products = get_products
        if @campaign_lock.save
          Product.where(id: import_product_params[:products_hot]).update_all(tag: Reti::Product::TAG[0])
          Product.where(id: import_product_params[:products_inventory]).update_all(tag: Reti::Product::TAG[1])

          format.html { redirect_to edit_admin_campaign_lock_path(@campaign_lock.id), notice: 'Lịch lock căn đã được tạo thành công.' }
          format.json { render :edit, status: :created, location: @campaign_lock }
        else
          format.html { render :new }
          format.json { render json: @campaign_lock.errors, status: :unprocessable_entity }
        end
      else
        format.html { render :new }
      end
    end
  end

  def update
    authorize @campaign_lock
    respond_to do |format|
      if @campaign_lock.update(campaign_lock_params)
        format.html { redirect_to edit_admin_campaign_lock_path(@campaign_lock.id), notice: 'Lịch lock căn đã được chỉnh sửa thành công.' }
        format.json { render :edit, status: :ok, location: @campaign_lock }
      else
        format.html { render :edit }
        format.json { render json: @campaign_lock.errors }
      end
    end
  end

  def destroy
    authorize @campaign_lock
    @campaign_lock.destroy
    respond_to do |format|
      format.html { redirect_to admin_campaign_locks_url, notice: 'Xóa thành công.' }
      format.json { head :no_content }
    end
  end

  def export_template
    file_name = "Bang_hang_#{Time.now.strftime(Settings.date.formats.time_export_xlsx)}.xlsx"
    export_to_xlsx CampaignLockService.new.export_template, file_name
  end

  def preview_products
    project = Project.find_by(id: params[:project_id])
    respond_to do |format|
      if project.present? && params[:file].present?
        valid, error, high_products, low_products = CampaignLockService.new(params[:file].path, params[:project_id]).import_products
        if valid
          format.json { render json: { high_products: high_products, low_products: low_products}, status: :ok }
        else
          format.json { render json: { error: error }, status: :unprocessable_entity }
        end
      else
        format.json { render json: { error: "Thông tin tải lên chưa hợp lệ. Bạn vui lòng kiểm tra lại!" }, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_campaign_lock
    @campaign_lock = CampaignLock.find(params[:id])
  end

  def get_data
    @projects = Project.all.pluck(:name, :id)
    @users = User.internal.joins(:role).where(roles: { name: ['Sale Admin', 'Sale Admin Chi Nhánh'] }).pluck(:full_name, :id)
  end

  def campaign_lock_params
    params.require(:campaign_lock).permit(:session_opening_time, :project_id, :enable_locked_for_hot_product, :enable_locked_for_inventory, :unc_confirmation_period_for_sale,
                                          :unc_confirmation_period_for_sale_admin, :confirm_deposit_by_sale_admin)
  end

  def import_product_params
    params.require(:campaign_lock).permit({ products_hot: [], products_inventory: []})
  end

  def export_to_xlsx(file, file_name)
    tempfile = "#{Rails.root}/tmp/tempfile.xlsx"

    file.serialize tempfile
    ::File.open tempfile, "r" do |f|
      send_data f.read, filename: file_name, type: "application/xlsx"
    end
    ::File.delete tempfile
  end

  def get_products
    (import_product_params[:products_hot].to_a + import_product_params[:products_inventory].to_a).map(&:to_i).uniq
  end
end

class Admin::VersionsController < Admin::BaseController

  def index
    @version = Version.first
  end

  def update
    @version = Version.first.present? ? Version.first : Version.new
    @version.update(version_params)
    redirect_to admin_versions_path, notice: 'Cập nhật thông tin version thành công.'
  end

  private

  def version_params
    params.require(:version).permit(:version, :description)
  end
end

class Admin::DealImportsController < Admin::BaseController
  def new
    @deal_import = DealImport.new
  end

  def create
    @deal_import = DealImport.new deal_import_params
    if @deal_import.valid?
      @deal_import.save
      msg = "#{@deal_import.imported_items.count} deals imported."
      redirect_to new_admin_deal_import_path, notice: msg
    else
      render :new
    end
  end

  private
  def deal_import_params
    params.fetch(:deal_import, {}).permit(:file)
  end
end

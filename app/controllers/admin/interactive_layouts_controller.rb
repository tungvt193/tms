class Admin::InteractiveLayoutsController < Admin::BaseController
  respond_to :html, :json
  before_action :get_project

  def index
    @general_layout = @project.interactive_layouts.where(division_id: nil).first
  end

  def new

  end

  def create
    respond_to do |format|
      if @project.update(project_interactive_layout_params)
        format.html { redirect_to admin_project_interactive_layouts_path(@project), notice: 'Đã thêm mới mặt bằng thành công.' }
      else
        format.html { render :index }
      end
    end
  end

  def edit
    @interactive_layout = InteractiveLayout.find_by(id: params[:id])
    authorize @interactive_layout
    respond_modal_with @interactive_layout
  end

  def update
    @interactive_layout = InteractiveLayout.find_by(id: params[:id])
    authorize @interactive_layout
    respond_to do |format|
      if @interactive_layout.update(interactive_layout_params)
        format.html { redirect_to admin_project_interactive_layouts_path(@project), notice: 'Mặt bằng đã được chỉnh sửa thành công.' }
        format.json { render :edit, status: :ok, location: @interactive_layout }
      else
        format.html { render :edit }
        format.json { render json: @interactive_layout.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @interactive_layout = InteractiveLayout.find_by(id: params[:id])
    authorize @interactive_layout
    @interactive_layout.destroy
    respond_to do |format|
      format.html { redirect_to admin_project_interactive_layouts_path(@project), notice: 'Xóa thành công.' }
      format.json { head :no_content }
    end
  end

  def find_divisions
    layout_division_ids = InteractiveLayout.all.pluck :division_id
    @division = Division.find_by(id: params[:division_id])
    data = []
    if @division
      if @division_parent = @division.parent
        if @division_parent_parent = @division_parent.parent
          data.push({select: 'select2-subdivision-layout', value: @division_parent_parent.id, items: @project.divisions.where(label: 0)})
          existed_block_ids = []
          @division_parent_parent.childrens.each do |block|
            if block.childrens.pluck(:id) - layout_division_ids == [] && layout_division_ids.include?(block.id)
              existed_block_ids.push(block.id)
            end
          end
          data.push({select: 'select2-block-layout', value: @division_parent.id, items: @division_parent_parent.childrens.where.not(id: existed_block_ids)})
          data.push({select: 'select2-floor-layout', value: @division.id, items: @division_parent.childrens.where.not(id: existed_floor_ids)})
        else
          data.push({select: 'select2-subdivision-layout', value: @division_parent.id, items: @project.divisions.where(label: 0)})
          existed_block_ids = []
          @division_parent.childrens.each do |block|
            if block.childrens.pluck(:id) - layout_division_ids == [] && layout_division_ids.include?(block.id)
              existed_block_ids.push(block.id)
            end
          end
          data.push({select: 'select2-block-layout', value: @division.id, items: @division_parent.childrens.where.not(id: existed_block_ids)})
        end
      else
        data.push({select: 'select2-subdivision-layout', value: @division.id, items: @project.divisions.where(label: 0)})
      end
    end
    respond_to do |format|
      format.json { render json: data }
    end
  end

  def get_divisions
    layout_division_ids = InteractiveLayout.all.pluck :division_id
    divisions = if params[:parent] == 'block' && params[:child] == 'select2-floor-layout'
                  existed_floor_ids = layout_division_ids
                  Division.where(parent_id: params[:parent_id]).where.not(id: existed_floor_ids)
                else
                  existed_block_ids = []
                  Division.where(parent_id: params[:parent_id]).each do |block|
                    if block.childrens.pluck(:id) - layout_division_ids == [] && layout_division_ids.include?(block.id)
                      existed_block_ids.push(block.id)
                    end
                  end
                  Division.where(parent_id: params[:parent_id]).where.not(id: existed_block_ids)
                end

    respond_to do |format|
      format.json { render json: divisions }
    end
  end

  private
  def project_interactive_layout_params
    params.require(:project).permit( interactive_layouts_attributes: [:project_id, :division_id, :image, :image_cache, :_destroy] )
  end

  def interactive_layout_params
    params.require(:interactive_layout).permit( :show_division, :image, :image_cache )
  end

  def get_project
    @project = Project.find_by(id: params[:project_id])
    @subdivisions = @project.divisions.where(label: 0).pluck(:name, :id)
    @layout_division_ids = InteractiveLayout.all.pluck :division_id
    @layout_subdivisions = @project.divisions.where(label: 0, id: @layout_division_ids)
    # @layout_blocks = @layout_subdivisions.childrens
  end
end

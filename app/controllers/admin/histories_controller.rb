class Admin::HistoriesController < Admin::BaseController
  layout false
  def index
    @deal = Deal.find(params[:deal_id])
    @histories = @deal.histories
    @pagy, @histories = pagy(@histories, items: Settings.admin.pagination.history.per_page)
  end

end

class Admin::NotificationsController < Admin::BaseController
  def index
    @notifications = current_user.notifications
    @notifications = @notifications.order(created_at: :desc)
    @pagy, @notifications = pagy(@notifications, items: Settings.admin.pagination.notification.per_page)
  end

  def load_more
    @notification_load_more = current_user.notifications.where(read_at: nil)
    @notification_load_more = @notification_load_more.order(created_at: :desc).limit(Settings.admin.pagination.notification.limit).offset(params[:offset])
    render layout: false
  end

  def read
    notification = current_user.notifications.where(read_at: nil).find(params[:id])
    notification.update(read_at: Time.current)
    render json: notification, status: :ok
  end

  def read_all
    notification = current_user.notifications.where(read_at: nil)
    notification.update_all(read_at: Time.current)
    render json: {}, status: :ok
  end
end

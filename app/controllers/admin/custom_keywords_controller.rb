class Admin::CustomKeywordsController < Admin::BaseController

  def index
    authorize :banner, :index?
    @current_keywords = Project.where(most_popular: true).map { |project| { id: project.id, text: project.name } }.to_json
  end

  def projects
    all_projects = Project.all.map { |project| { id: project.id, text: project.name } }
    search_projects = params[:search].present? ? Project.select2_search_result(params[:search]) : all_projects
    render json: (search_projects & all_projects)
  end

  def create
    Project.where(most_popular: true).update_all(most_popular: false)
    Project.where(id: params[:keywords]).update_all(most_popular: true)
    flash[:notice] = I18n.t("notice.keywords")
    redirect_to admin_custom_keywords_path
  end
end

class Admin::BannersController < Admin::BaseController
  before_action :set_banner, only: [:show, :edit, :update, :destroy, :destroy_attachment]

  def index
    authorize :banner, :index?
    banner_type = params[:banner_type] || 0
    @banners = Banner.where(banner_type: banner_type).order(position: :asc)
  end

  def show
    authorize @banner
  end

  def new
    @banner = Banner.new
    authorize @banner
    @is_disabled = !policy(@banner).create?
    @banner.banner_type = params[:banner_type] if params[:banner_type].present?
  end

  def edit
    authorize @banner
    @is_disabled = !policy(@banner).update?
  end

  def create
    @banner = Banner.new(banner_params)
    authorize @banner

    respond_to do |format|
      if @banner.save
        format.html { redirect_to edit_admin_banner_path(@banner.id), notice: 'Banner đã được tạo thành công.' }
        format.json { render :show, status: :created, location: @banner }
      else
        format.html { render :new }
        format.json { render json: @banner.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    authorize @banner
    respond_to do |format|
      if @banner.update(banner_params)
        format.html { redirect_to edit_admin_banner_path(@banner.id), notice: 'Banner đã được cập nhật thành công.' }
        format.json { render :show, status: :ok, location: @banner }
      else
        format.html { render :edit }
        format.json { render json: @banner.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    authorize @banner
    banner_type = @banner.banner_type
    @banner.destroy
    respond_to do |format|
      format.html { redirect_to admin_banners_url(banner_type: banner_type), notice: 'Xóa thành công.' }
      format.json { head :no_content }
    end
  end

  def destroy_attachment
    delete_attachment(params[:type])
  end

  def sort
    Banner.update(params[:sort].keys, params[:sort].values)
    render json: { notice: 'Cập nhật vị trí banner thành công.' }
  end

  private
    def set_banner
      @banner = Banner.find(params[:id])
      @banner.banner_type = params[:banner_type] if params[:banner_type].present?
    end

    def banner_params
      params.require(:banner).permit(:banner_type, :title, :sub_title, :banner_desktop, :banner_mobile, :link, :is_display)
    end

    def delete_attachment type
      @banner.send("remove_#{type}!")
      if @banner.valid?
        @banner.save
        @banner.reload
        render json: {}
      else
        render json: { error: @banner.errors[params[:type].to_sym]&.first }
      end
    end
end

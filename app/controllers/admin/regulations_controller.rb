class Admin::RegulationsController < Admin::BaseController
  include DateTimeHelper
  before_action :load_regulation, only: [:show, :new, :create, :edit, :update, :destroy]

  def index
    authorize :regulation, :index?
    query = params[:query].presence || "*"
    @pagy, @regulations = pagy Regulation.search_result(query, params[:reg_type]), items: Settings.admin.pagination.regulation.per_page
  end

  def autocomplete
    if params[:type] == 'real_estate'
      all_types = Constant::PROJECT_REAL_ESTATE_TYPE.map { |k, v| { value: v, type: 'real_estate' } }
      search_types = params[:query].present? ? all_types.reject { |r| !included?(r[:value], params[:query]) } : all_types
      render json: (search_types & all_types)
    else
      render json: Regulation.autocomplete_result(params[:query], params[:type])
    end
  end

  def show
    authorize @regulation
  end

  def new
    @regulation = Regulation.new
    authorize @regulation
    @is_disabled = !policy(@regulation).create?
  end

  def edit
    authorize @regulation
    check_duplicate_regulation
    @is_disabled = !policy(@regulation).update?
  end

  def create
    @regulation = Regulation.new regulation_params
    set_date_range
    @regulation.real_estate_type.compact!
    authorize @regulation

    if check_duplicate_regulation && @regulation.valid?
      @regulation.save
      redirect_to edit_admin_regulation_path(@regulation.id), notice: 'Cơ chế hoa hồng đã được tạo thành công.'
    else
      render :new
    end
  end

  def update
    authorize @regulation
    @regulation.assign_attributes regulation_params
    set_date_range
    @regulation.real_estate_type.compact!

    if check_duplicate_regulation && @regulation.valid?
      @regulation.save
      redirect_to edit_admin_regulation_path(@regulation.id), notice: 'Cơ chế hoa hồng đã được chỉnh sửa thành công.'
    else
      render :edit
    end
  end

  def destroy
    authorize @regulation
    @regulation.destroy
    redirect_to admin_regulations_url, notice: 'Xóa thành công.'
  end

  def get_types
    types = Project.find_by(id: params[:project_id])&.real_estate_type || []
    all_types = types.map { |x| { id: x, text: Constant::PROJECT_REAL_ESTATE_TYPE[x] } }
    search_types = params[:search].present? ? all_types.reject { |r| !included?(r[:text], params[:search]) } : all_types
    render json: (search_types & all_types)
  end

  private

  def load_regulation
    @regulation = Regulation.find_by id: params[:id]
    if params[:id] && @regulation.nil?
      redirect_to admin_regulations_url
    end
    set_date_range
    @projects = Project.all.pluck :name, :id
    @state_is_disable = true
    @state_is_disable = (!policy(@regulation).change_state? || @regulation.expired?) if @regulation
  end

  def regulation_params
    params[:regulation][:start_date], params[:regulation][:end_date] = params[:regulation][:date_range].split(' - ') if params[:regulation][:date_range].present?
    params.require(:regulation).permit Regulation::ATTRS
  end

  def included? str, search_str
    I18n.transliterate(str).downcase.include? I18n.transliterate(search_str).downcase
  end

  def set_date_range
    return unless @regulation
    if @regulation&.start_date && @regulation&.end_date
      @regulation.date_range = "#{format_regulation @regulation.start_date} - #{format_regulation @regulation.end_date}"
    else
      @regulation.date_range = ''
    end
  end

  def check_duplicate_regulation
    return true unless (dup_reg = Regulation.by_current(@regulation)&.first)
    overlap_time = merge_ranges @regulation.start_date..@regulation.end_date, dup_reg.start_date..dup_reg.end_date
    @dup_reg_info = {
      project_name: dup_reg.project.name,
      type: (dup_reg.real_estate_type & @regulation.real_estate_type).map { |t| Constant::PROJECT_REAL_ESTATE_TYPE[t] }.join(', '),
      time: "#{format_regulation overlap_time.begin} - #{format_regulation overlap_time.end}",
      url: edit_admin_regulation_path(dup_reg)
    }
    return false
  end

  def merge_ranges a, b
    [a.begin, b.begin].max..[a.end, b.end].min
  end
end

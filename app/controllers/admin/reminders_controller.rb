class Admin::RemindersController < Admin::BaseController
  def index
    @reminders = current_user.reminders
    @reminders = @reminders.order(created_at: :desc)
    @pagy, @reminders = pagy(@reminders, items: Settings.admin.pagination.notification.per_page)
  end

  def load_more
    @reminder_load_more = current_user.reminders.where(read_at: nil)
    @reminder_load_more = @reminder_load_more.order(created_at: :desc).limit(Settings.admin.pagination.notification.limit).offset(params[:offset])
    render layout: false
  end

  def read
    reminder = current_user.reminders.where(read_at: nil).find(params[:id])
    reminder.update(read_at: Time.current)
    render json: reminder, status: :ok
  end

  def read_all
    reminders = current_user.reminders.where(read_at: nil)
    reminders.update_all(read_at: Time.current)
    Reminder.read_all_reminder
    render json: {}, status: :ok
  end
end

class Admin::SameCustomersController < Admin::BaseController
  include DateTimeHelper

  def index
    @customers = policy_scope(Customer).where.not(same_customers: []).where.not(phone_number: nil).search_same_customers(params).order(created_at: :desc)
    customer_ids = @customers.reject{|c| policy_scope(Customer.where(id: c.same_customers)).count == 0}.pluck(:id)
    @customers =Customer.where(id: customer_ids)
    @pagy, @customers = pagy(@customers, items: Settings.admin.pagination.same_deal.per_page)
  end

  def show
    @customer = Customer.find(params[:id])
    @customers = policy_scope(Customer.where(id: @customer.same_customers).order(created_at: :desc))
    @customers = [@customer] + @customers

    @data = {
      'Số điện thoại' => @customers.map { |c| c.phone_number },
      'Giới tính' => @customers.map { |c| c.gender ? Constant::CUSTOMER_GENDER.values[c.gender] : nil },
      'Email' => @customers.map { |c| c.email }
    }
  end

  def autocomplete
    @customers = policy_scope(Customer).where.not(same_customers: [])
    customer_ids = @customers.reject{|c| policy_scope(Customer.where(id: c.same_customers)).count == 0}.pluck(:id)
    @customers = Customer.where(id: customer_ids)
    render json: @customers.autocomplete_same_customers(params[:query])
  end
end

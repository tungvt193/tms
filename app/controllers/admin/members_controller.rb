class Admin::MembersController < Admin::BaseController

  before_action :find_group
  before_action :check_exist, only: [:add_root_ancestry, :add_child_ancestry, :add_sibling_ancestry]

  def index
    @supervisor_group = @group.supervisor
    @director_group = @group.director
    @children = @group.staffs.where(ancestry: nil).map { |u| ["#{u.full_name} - #{u.email}", u.id] }
    if @director_group.present?
      if @supervisor_group
        @data_source = {
          'name': @supervisor_group.full_name,
          'email': @supervisor_group.email,
          'children': [
            'name': @director_group.full_name,
            'email': @director_group.email,
            'user_id': @director_group.id,
            'children': @director_group.prepare_data_org_chart(@group)
          ]
        }
      else
        @data_source = {
          'name': @director_group.full_name,
          'email': @director_group.email,
          'children': @director_group.prepare_data_org_chart(@group)
        }
      end
    end
  end

  def add_root_ancestry
    if params[:parent].present?
      node = User.find_by(email: params[:parent])
      if node.update_column(:group, @group)
        render json: { status: 'success' }
      end
    end
  end

  def add_child_ancestry
    if params[:parent].present? && params[:child].present?
      parent = User.find_by(email: params[:parent])
      ancestry = parent.root? ? "#{parent.id}" : "#{parent.ancestry}/#{parent.id}"
      User.where(email: params[:child]).each do |child|
        child.update_column(:ancestry, ancestry)
      end
      render json: { status: 'success' }
    end
  end

  def add_sibling_ancestry
    if params[:parent].present? && params[:child].present?
      parent = User.find_by(email: params[:parent])
      User.where(email: params[:child]).each do |child|
        child.update_column(:ancestry, parent.ancestry)
      end
      render json: { status: 'success' }
    end
  end

  def remove_ancestry
    if params[:node].present?
      node = User.find_by(email: params[:node])
      descendant_ids = node.descendants.by_ids(@group.members).map(&:id)
      ids = descendant_ids.push(node.id)
      User.where(id: ids).each do |u|
        u.update_column(:ancestry, nil)
      end
    end
    render json: { user: node }
  end

  private

  def find_group
    @group = Group.find_by(id: params[:id])
  end

  def check_exist
    if params[:child].present?
      node = User.find_by(email: params[:child])
      if @group.director.descendant_ids.include?(node.id)
        render json: { status: 'error', message: 'Tài khoản này đã tồn tại trong sơ đồ. Vui lòng chọn tài khoản khác.' }
        return
      end
    end
  end
end

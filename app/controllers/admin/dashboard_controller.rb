class Admin::DashboardController < Admin::BaseController
  include NumberFormatHelper
  before_action :set_target_block, only: :index

  def index
    # Block quản lý lượt lock
    @current_product_lock_histories = []
    @current_product_lock_histories << ProductLockHistory.with_state(:waiting_reti_acceptance).order(deadline_for_sale_admin: :asc)
    @current_product_lock_histories << ProductLockHistory.with_state(:deposit_confirmation).order(deadline_for_sale: :asc)
    @current_product_lock_histories = @current_product_lock_histories.flatten
    @pending_product_lock_histories = ProductLockHistory.with_state(:pending).order(updated_at: :asc)

    deals = current_user.get_deals
    # Block 1
    if current_user.internal?
      @first_counter = deals.where('state IN (?)', %w(interest meet refundable_deposit lock deposit))
      @second_counter = current_user.appointments.where(state: nil).where('appointment_date >= ?', DateTime.now).size
      @third_counter = deals.contract_signed
      @fourth_counter = if current_user.super_admin? || current_user.director?
                          policy_scope(Deal.contract_signed).sum(:total_price)
                        else
                          @third_counter.sum(:total_price)
                        end
      start_month = Time.now.beginning_of_month
      end_month = Time.now.end_of_month
      if !current_user.super_admin? && !current_user.sale_admin?
        @contract_signed_deals = @third_counter.where.not(total_price: nil).where('contract_signed_at BETWEEN ? AND ?', start_month, end_month)
        @sum_price = (@contract_signed_deals.sum(:total_price).to_f / 1000000000).round(2)
        deals_users = Deal.contract_signed.where(assignee_id: current_user.root.subtree_ids).where.not(total_price: nil).where('contract_signed_at BETWEEN ? AND ?', start_month, end_month)

        @sum_price_users = deals_users.group_by(&:assignee_id).map do |assignee_id, records|
          sum_price_users = (records.map(&:total_price).sum.to_f / 1000000000).round(2)
          [assignee_id, '', sum_price_users, records.size]
        end.sort_by(&:third).reverse

        @sum_price_rank = @sum_price_users.present? ? @sum_price_users.map(&:first).index(current_user.id) : '0'
        @sum_price_users = @sum_price_users.first(5)

        if @sum_price_users.select { |u| u.first == current_user.id }.blank? && @contract_signed_deals.present?
          if @sum_price_users.size > 4
            @sum_price_users.shift(4)
          end
          @sum_price_users << [current_user.id, "", @sum_price, @contract_signed_deals.size]
        end
        @sum_price_users = @sum_price_users.map do |user|
          user[1] = "#{user.first == current_user.id ? '*' : ''}#{User.find(user.first).full_name}"
          user
        end
        @sum_price_max = @sum_price_users.present? ? @sum_price_users.first.third : '0'
      end
    end
    # Block 2
    @pending_deals = deals.pending
    @today_pending_deals = @pending_deals.where('assigned_at BETWEEN ? AND ?', DateTime.now.beginning_of_day, DateTime.now.end_of_day)
    # Block 3
    @list_project_news = if current_user.project_selected_ids.empty?
                           ProjectNewUpdate.all.order("created_at DESC")
                         else
                           ProjectNewUpdate.where(project_id: current_user.project_selected_ids).order("created_at DESC")
                         end
    get_api_dashboard_assignee
    get_api_dashboard_deals
    get_api_dashboard_commission
  end

  def update_project_selected_ids
    params[:project_selected_ids] = params[:project_selected_ids].reject { |n| n.blank? } if params[:project_selected_ids].present?
    current_user.project_selected_ids = params[:project_selected_ids] || []
    current_user.save(validate: false)
    @list_project_news = if current_user.project_selected_ids.empty?
                           ProjectNewUpdate.all.order("created_at DESC")
                         else
                           ProjectNewUpdate.where(project_id: current_user.project_selected_ids).order("created_at DESC")
                         end
    respond_to do |format|
      format.js { render layout: false }
    end
  end

  def get_projects
    project_selected_ids = current_user.project_selected_ids
    projects = Project.all.map { |project| { id: project.id, text: project.name, check: project_selected_ids.include?(project.id) ? 1 : 0 } }
    render json: projects
  end

  def get_access_token
    token = current_user.create_token
    current_user.save
    sign_in(:user, current_user, store: false, bypass: false)
    render json: {
      "credentials": {
        "accessToken": token.token,
        "client": token.client,
        "expiry": token.expiry,
        "tokenType": "Bearer",
        "uid": current_user.uid
      }
    }
  end

  def get_api_dashboard_assignee
    @deals_assignee = Deal.left_outer_joins(:author).where('assignee_id IS NULL AND (callio_campaign_id IS NOT NULL OR source = ? OR account_type = ?)', 43, 1).order(created_at: :asc).page(params[:page]).per(20)
    @count_deals_assignee = @deals_assignee.size
  end

  def get_api_dashboard_deals
    deals_sort = []
    deals = []
    Deal.contract_signed.each do |deal|
      if deal.comm_fields_blank? || deal.commission == nil || deal.commission.invalid?
        time_exist = ((deal.deal_log&.send("#{deal.state}_reentered").present? ? deal.deal_log&.send("#{deal.state}_reentered") : deal.deal_log&.send("#{deal.state}_entered")))
        deals_sort << [deal, time_exist.present? ? (Time.now - time_exist) : 0]
      end
    end
    deals_sort.sort_by { |d| d.second }.reverse.each do |deal_sort|
      deals << deal_sort.first
    end
    @deals = Kaminari.paginate_array(deals).page(params[:page]).per(20)
    @count_deals = @deals.size
  end

  def get_api_dashboard_commission
    deals_sort = []
    deals = []
    Deal.where(state: %w(deposit lock contract_signed)).each do |deal|
      if deal.invalid? || deal.customer_persona&.invalid?
        time_exist = ((deal.deal_log&.send("#{deal.state}_reentered").present? ? deal.deal_log&.send("#{deal.state}_reentered") : deal.deal_log&.send("#{deal.state}_entered")))
        deals_sort << [deal, time_exist.present? ? (Time.now - time_exist) : 0]
      end
    end
    deals_sort.sort_by { |d| d.second }.reverse.each do |deal_sort|
      deals << deal_sort.first
    end
    @deals_commission = Kaminari.paginate_array(deals).page(params[:page]).per(20)
    @count_deals_commission = @deals_commission.size
  end

  def unc_product_lock_history
    @product_lock_history = ProductLockHistory.find(params[:id])
  end

  private

  def set_target_block
    if (current_user.sale_member? || current_user.sale_leader?)
      @leader = current_user.leader || current_user
      @kpi = Kpi.current_kpi(@leader.team.id)
      @kpi = @kpi * 1000000000 if @kpi
      @revenue = Deal.get_team_revenue(@leader.descendant_ids << @leader.id)
      @most_revenue = User.select { |u| u.sale_member? || u.sale_leader? }
                          .map { |u| u.leader || u }.uniq.compact
                          .map { |u| Deal.get_team_revenue(u.descendant_ids << u.id) }.compact.max
      @kpi_progress = @kpi ? (@revenue * 100 / @kpi).round : 0
      @arrow_progress = [@kpi_progress, 100].min
      @arrow_pos = {
        x: 17 + 15.6 * Math.sin(2 * Math::PI * @arrow_progress / 100),
        y: 17 - 15.6 * Math.cos(2 * Math::PI * @arrow_progress / 100),
        rotate: (@arrow_progress - 50) * 360 / 100
      }
    end
  end

  def get_api_dashboard(type)
    uri = URI.parse("#{ENV['TMS_URL']}/api/v1/graphql")
    request_header = { 'access-token': params[:access_token], 'client': params[:client], 'uid': params[:uid], 'Content-Type': 'application/json' }
    page = params[:page].present? ? params[:page].to_i : 1
    request_params = {
      "query": "query dashboardSaleAdmin#{type}($page: Int, $perPage: Int) {
         dashboardSaleAdmin#{type} (
             page: $page,
             perPage: $perPage
            ){
            collection {
              id
              name
            }
            metadata {
                totalCount
            }
          }
        }",
      "variables": {
        "page": page,
        "per_page": 20
      }
    }
    Net::HTTP.start(uri.host, uri.port, use_ssl: uri.scheme == 'https', read_timeout: 10, open_timeout: 10) do |http|
      request = Net::HTTP::Post.new(uri, request_header)
      request.body = request_params.to_json
      response = JSON.parse(http.request(request).body)
      [response['data']["dashboardSaleAdmin#{type}"]['collection'].map { |d| d['id'] }, response['data']["dashboardSaleAdmin#{type}"]['metadata']['totalCount']]
    end
  end
end


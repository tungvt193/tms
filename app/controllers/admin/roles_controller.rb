class Admin::RolesController < Admin::BaseController
  before_action :find_role, only: [:edit, :update, :show, :destroy]

  def index
    @pagy, @roles = pagy(Role.all, items: Settings.admin.pagination.role.per_page)
  end

  def show
  end

  def new
    @role = Role.new
    authorize @role
    @permissions = []
    Permission::LIST_MODEL_NAME.keys.each do |model_name|
      @permissions.push(@role.permissions.new(name: model_name))
    end
    @permissions = @permissions.sort_by { |hsh| hsh[:name] }
  end

  def new_clone
    role = Role.find params[:id]
    @role = role.deep_clone include: :permissions, except: :name
    authorize @role
    render 'new'
  end

  def edit
    authorize @role
    @permissions = @role.permissions.order(:name)
  end

  def update
    authorize @role
    respond_to do |format|
      if @role.update(role_params)
        format.html { redirect_to edit_admin_role_path(@role.id), notice: 'Cập nhật phân quyền thành công.' }
        format.json { render :show, status: :ok, location: @role }
      else
        format.html { render :edit }
        format.json { render json: @role.errors, status: :unprocessable_entity }
      end
    end
  end

  def create
    @role = Role.new(role_params)
    authorize @role
    respond_to do |format|
      if @role.save
        format.html { redirect_to edit_admin_role_path(@role.id), notice: 'Tạo mới phân quyền thành công.' }
        format.json { render :show, status: :created, location: @role }
      else
        format.html { render :new }
        format.json { render json: @role.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    if @role.users.blank?
      authorize @role
      @role.destroy
      respond_to do |format|
        format.html { redirect_to admin_roles_url, notice: 'Xoá phân quyền thành công.' }
        format.json { head :no_content }
      end
    else
      redirect_to admin_roles_path, alert: 'Quyền đã được gán cho tài khoản. Vui lòng gỡ khỏi tài khoản để xóa.'
    end
  end

  private

  def find_role
    @role = Role.find_by_id params[:id]
    redirect_to admin_roles_path unless @role.present?
  end

  def role_params
    if params[:role][:permissions_attributes]['8'][:commission_fields_validation].present?
      params[:role][:permissions_attributes]['8'][:commission_fields_validation] = params[:role][:permissions_attributes]['8'][:commission_fields_validation].reject { |n| n.blank? }.reject { |n| n == 'check_all' }
    end
    if params[:role][:permissions_attributes]['8'][:commission_fields_can_view].present?
      params[:role][:permissions_attributes]['8'][:commission_fields_can_view] = params[:role][:permissions_attributes]['8'][:commission_fields_can_view].reject { |n| n.blank? }.reject { |n| n == 'check_all' }
    end
    if params[:role][:permissions_attributes]['8'][:commission_fields_can_edit].present?
      params[:role][:permissions_attributes]['8'][:commission_fields_can_edit] = params[:role][:permissions_attributes]['8'][:commission_fields_can_edit].reject { |n| n.blank? }.reject { |n| n == 'check_all' }
    end
    params.require(:role).permit(:name,
                                 permissions_attributes: [
                                   :id, :name, :can_edit, :can_create, :change_state, { state_area: [] }, :can_import,
                                   :can_update, :can_destroy, :can_assign, :can_update_price, { commission_fields_validation: [] }, { commission_fields_can_view: [] }, { commission_fields_can_edit: [] }, :data_area, :role_id
                                 ])
  end
end

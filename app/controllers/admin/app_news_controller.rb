class Admin::AppNewsController < Admin::BaseController
  def index
  end

  def import
    unless params[:file].present?
      flash[:alert] = 'Định dạng file không hợp lệ. Hãy chọn file có định dạng CSV.'
      return redirect_to admin_app_news_index_path
    end
    uploader = NewsUploader.new
    uploader.store!(params[:file])
    flash[:notice] = 'Upload file thành công!'
    redirect_to admin_app_news_index_path
  end
end

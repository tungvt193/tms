class Admin::InstructionDocumentsController < Admin::BaseController

  before_action :set_instruction_document

  def index
    @instruction_documents = InstructionDocument.all
    @instruction_document = InstructionDocument.new
  end

  def create
    @instruction_document = InstructionDocument.new(instruction_document_params)
    respond_to do |format|
      if @instruction_document.save
        format.html { redirect_to admin_instruction_documents_path, notice: 'Thêm mới tài liệu HDSD thành công.' }
      else
        format.js
      end
    end
  end

  def destroy
    @instruction_document.destroy
    respond_to do |format|
      format.html { redirect_to admin_instruction_documents_url, notice: "Xóa tài liệu HDSD thành công." }
      format.json { head :no_content }
    end
  end

  private

  def set_instruction_document
    @instruction_document = InstructionDocument.find_by(id: params[:id])
  end

  def instruction_document_params
    params.require(:instruction_document).permit(:title, :file)
  end
end

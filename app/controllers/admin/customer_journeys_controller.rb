class Admin::CustomerJourneysController < Admin::BaseController
  before_action :set_customer, only: [:edit, :update, :destroy]

  def edit
    authorize :customer_journey, :show?
    fb_api = FacebookApi.last
    if fb_api.present?
      graph = Koala::Facebook::API.new(fb_api.pages['data'][0]['access_token'])
      @conversations = graph.get_connections('me', 'conversations', {fields: 'messages{message,from,to,created_time}'}).first(2)
      @ads = graph.get_connections('me', 'ads_posts', {fields: 'from,picture,message,created_time'})
    end
  end

  def update
    authorize :customer_journey, :update?

    if params[:cancel] || params[:save_visit_journey]|| params[:save_ads_journey]
      if params[:cancel]
        @customer.reload
      elsif params[:save_visit_journey] || params[:save_ads_journey]
        @customer.assign_attributes(customer_params)
        @customer.save if @customer.valid?
      end
      respond_to do |format|
        format.js { render :edit }
      end
    else
      respond_to do |format|
        if @customer.update(customer_params)
          format.html { redirect_to admin_customer_customer_journeys_path(@customer), notice: 'Hành trình tương tác đã được chỉnh sửa thành công.' }
          format.json { render :edit, status: :ok, location: @customer }
        else
          format.js { render :edit }
          format.json { render json: @customer.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  def destroy
    authorize :customer_journey, :destroy?

    journeys = if params[:email_journey_ids]
      @customer.email_journeys.where(id: params[:email_journey_ids])
    elsif params[:visit_journey_ids]
      @customer.visit_journeys.where(id: params[:visit_journey_ids])
    elsif params[:form_journey_ids]
      @customer.form_journeys.where(id: params[:form_journey_ids])
    elsif params[:ads_journey_ids]
      @customer.ads_journeys.where(id: params[:ads_journey_ids])
    elsif params[:sms_journey_ids]
      @customer.sms_journeys.where(id: params[:sms_journey_ids])
    end
    respond_to do |format|
      if journeys.destroy_all
        email_size = @customer.email_journeys.count if params[:email_journey_ids]
        format.json { render json: {status: :ok, email_size: email_size} }
      else
        format.json { render json: {status: :error} }
      end
    end
  end

  private
  def set_customer
    @customer = Customer.find(params[:customer_id])
    @email_journey_groups = @customer.email_journey_groups
    authorize @customer, :show?
  end

  def customer_params
    params.require(:customer).permit(visit_journeys_attributes: [:id, :title, :visited_link, :visited_date, :visit_id, :_destroy],
      form_journeys_attributes: [:id, :recent_conversion, :first_name, :last_name, :phone_number, :email, :recent_conversion_date, :_destroy],
      ads_journeys_attributes: [:id, :fb_ad_name, :fb_campaign_name, :fb_ad_group_name, :create_date, :content_link, :image_link, :_destroy])
  end
end

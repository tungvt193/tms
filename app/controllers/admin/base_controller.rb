class Admin::BaseController < TmsController

  include Pagy::Backend
  include Pundit

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  before_action :authenticate_user!
  before_action :set_current_user
  before_action :get_notifications

  def set_current_user
    User.current = current_user
  end

  def get_notifications
    @notifications = []
    @count = 0
    @reminders = []
    @count_reminder = 0
    return unless current_user
    all_notifications = current_user.notifications.where(read_at: nil)
    @count = all_notifications.count
    @notification_load_more = all_notifications.order(created_at: :desc).limit(Settings.admin.pagination.notification.limit)
    all_reminders = current_user.reminders.where(read_at: nil)
    @reminders = all_reminders.order(created_at: :desc).limit(Settings.admin.pagination.notification.limit)
    @count_reminder = all_reminders.count
    @reminder_load_more = all_reminders.order(created_at: :desc).limit(Settings.admin.pagination.notification.limit)
  end

  def respond_modal_with(*args, &blk)
    options = args.extract_options!
    options[:responder] = ModalResponder
    respond_with *args, options, &blk
  end

  private

  def user_not_authorized
    flash[:alert] = I18n.t("alert.no_authorize")
    redirect_to(request.referrer || admin_root_path)
  end
end

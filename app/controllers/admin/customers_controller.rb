class Admin::CustomersController < Admin::BaseController
  before_action :set_customer, only: [:show, :edit, :update, :destroy]
  before_action :set_countries
  before_action :get_source_group, only: [:new, :edit, :create, :update]

  # GET /customers
  # GET /customers.json
  def index
    authorize :customer, :index?
    @customers = policy_scope(Customer)
    query = params[:query].presence || "*"
    @pagy, @customers = pagy_countless(@customers.search_result(query), items: Settings.admin.pagination.customer.per_page)
  end

  def autocomplete
    @customers = policy_scope(Customer)
    render json: @customers.autocomplete_result(params[:query])
  end

  # GET /customers/1
  # GET /customers/1.json
  def show
    authorize @customer
    @customer_characteristic = Constant::CUSTOMER_CUSTOMER_CHARACTERISTIC.values_at(*@customer.customer_characteristic)
    @customer_notes = @customer.notes
  end

  # GET /customers/new
  def new
    @cities = RegionData.cities.pluck(:name_with_type, :id)
    @customer = Customer.new
    authorize @customer
    @is_disabled = !policy(@customer).create?
  end

  # GET /customers/1/edit
  def edit
    authorize @customer
    @is_disabled = !policy(@customer).update?
    @cities = RegionData.cities.pluck(:name_with_type, :id)
    @customer_notes = @customer.notes
  end

  # POST /customers
  # POST /customers.json
  def create
    @customer = Customer.new(customer_params)
    @customer.created_by_id = current_user.id
    if params[:form_id] == 'quick_create'
      @customer.is_mobile = params[:is_mobile] == 'true'
      @customer.data_source = Deal.find_by(id: params[:deal_id])&.source
      authorize @customer
      respond_to do |format|
        @customer_valid = @customer.valid? || (@customer.errors.count == 1 && @customer.errors.include?(:data_source))
        @customer.save(validate: false) if @customer_valid
        format.js { render :new }
      end
      return
    end
    @cities = RegionData.cities.pluck(:name_with_type, :id)
    authorize @customer
    respond_to do |format|
      if @customer.save
        format.html { redirect_to edit_admin_customer_path(@customer.id), notice: 'Khách hàng đã được tạo thành công.' }
        format.json { render :edit, status: :created, location: @customer }
      else
        count_errors
        format.html { render :new }
        format.json { render json: @customer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /customers/1
  # PATCH/PUT /customers/1.json
  def update
    authorize @customer
    @cities = RegionData.cities.pluck(:name_with_type, :id)
    respond_to do |format|
      if params[:customer][:deal_id].present?
        @deal = Deal.find(params[:customer][:deal_id])
        if @customer.update(customer_params)
          format.html { redirect_to edit_admin_deal_path(params[:customer][:deal_id]), notice: 'Khách hàng đã được chỉnh sửa thành công.' }
          format.json { render :edit, status: :ok }
        else
          format.js { render :update }
        end
      else
        if @customer.update(customer_params)
          format.html { redirect_to edit_admin_customer_path(@customer.id), notice: 'Khách hàng đã được chỉnh sửa thành công.' }
          format.json { render :edit, status: :ok, location: @customer }
        else
          count_errors
          format.html { render :edit }
          format.json { render json: @customer.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /customers/1
  # DELETE /customers/1.json
  def destroy
    authorize @customer
    @customer.destroy
    respond_to do |format|
      format.html { redirect_to admin_customers_url, notice: 'Xóa thành công.' }
      format.json { head :no_content }
    end
  end

  def import
    authorize :customer, :import?
    ImportCustomerJob.perform params[:file]&.path, current_user
  end

  def check_same_phone
    phone_number = Phonelib.parse(params[:phone_number], params[:country_code]).full_e164.sub('+', '')
    customer_same = policy_scope(Customer).where.not(id: params[:customer_id].to_i).where(phone_number: phone_number)
    if phone_number.present? && customer_same.present?
      arr = []
      customer_same.each do |cus|
        phone = Phonelib.parse(cus.phone_number, cus.country_code).national&.delete(' ') if cus.phone_number.present? && cus.country_code.present?
        arr.push("<span class='same-customer' data-customer-id=#{cus.id} data-customer-name='#{cus.name}' data-customer-phone='#{phone}'>#{cus.name}</a>")
      end
      msg = "Số điện thoại này đang bị trùng với khách hàng: #{arr.join(', ')}"
      return render json: { status: false, msg: msg }
    end
    return render json: { status: true }
  end

  def vouchers
    @customer = Customer.find(params[:id])
    @vouchers_customers = @customer.vouchers_customers.order(created_at: :desc)
    @pagy, @vouchers_customers = pagy(@vouchers_customers, items: 10)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_customer
    @customer = Customer.find(params[:id])
  end

  def set_countries
    @countries = Phonelib.phone_data.map { |k, v| ["#{ISO3166::Country.new(k).translation('en')} (+#{ISO3166::Country.new(k)&.country_code})", k, { 'data-code': "+#{ISO3166::Country.new(k)&.country_code}" }] if ISO3166::Country.new(k)&.local_name }.compact
  end

  # Only allow a list of trusted parameters through.
  def customer_params
    params.require(:customer).permit(:name, :phone_number, :second_phone_number, :gender, :email, :identity_card, :city_id, :address, :dob,
                                     :data_source, :country_code, { :customer_characteristic => [] }, :facebook_uid, :job, :property_ownership, :nationality, :financial_capability,
                                     :detail, :income, :position, :work_place, :marital_status, notes_attributes: [:id, :created_by_id, :content, :_destroy])
  end

  def get_source_group
    @source_group = Deal.get_source_group
  end

  def count_errors
    @error_count = @customer.errors.messages.except(:"notes.content").count + @customer.notes.map(&:errors).map(&:messages).reject(&:blank?).count
  end
end

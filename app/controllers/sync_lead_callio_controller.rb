class SyncLeadCallioController < ApplicationController

  def sync
    lead = request.params[:sync_lead_callio][:callcampaigncontact]
    SyncLeadFromCallio.perform_later(lead)
  end
end

module CommonHelper
  def included? str, search_str
    I18n.transliterate(str).downcase.include? I18n.transliterate(search_str).downcase
  end

  def product_cell_value(column, product)
    cell = product[column[:field]]

    case column[:type].to_s
    when 'text'
      return Division.find(cell).name if %i(subdivision_id block_id floor_id).include?(column[:field])
      
      cell
    when 'dropdown'
      return cell if %i(tag source label).include?(column[:field])

      cell = column[:value][cell]
    when 'numeric'
      return cell.to_f if cell.is_a?(Float)
      return cell.to_i > 0 ? cell.to_i : nil
    end
  end

  def display_active_class(object)
    return 'active' if object.present?
  end
end

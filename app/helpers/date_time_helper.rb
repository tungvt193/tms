module DateTimeHelper
  def format_date(date)
    date.localtime.strftime(Settings.date.formats.strftime_date_time_vi)
  end

  def format_task_date(date)
    date.localtime.strftime(Settings.date.formats.strftime_short)
  end

  def format_regulation(date)
    return '' unless date
    date.localtime.strftime(Settings.date.formats.strftime_regulation)
  end

  def format_ymdhm(date)
    return '' unless date
    date.localtime.strftime(Settings.date.formats.strftime_ymdhm)
  end

  def format_ymd(date)
    return '' unless date
    date.localtime.strftime(Settings.date.formats.strftime_ymd)
  end

  def format_customer_journey(date)
    return '' unless date
    date.localtime.strftime(Settings.date.formats.strftime_time_history)
  end

  def format_datetime_with_second(date)
    return '' unless date
    date.localtime.strftime(Settings.date.formats.strftime_time_second)
  end
end

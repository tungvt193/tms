module NumberFormatHelper
  def currency(number)
    number_to_currency(number, precision: 0, unit: '', delimiter: '.', separator: ',')
  end

  def acreage(number)
    number_to_currency(number, precision: 1, unit: '', delimiter: '.', separator: ',')
  end

  def kpi_currency(number)
    return '-' unless number
    number = number / 1000000000.to_f
    number_to_currency(number, precision: 3, unit: '', delimiter: '.', separator: ',', strip_insignificant_zeros: true)
  end
end

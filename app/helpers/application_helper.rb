module ApplicationHelper
  include Pagy::Frontend

  def constant_options constant, sublayer = nil
    (sublayer.present? ? constant[sublayer]&.invert : constant&.invert) || {}
  end

  def countdown_timer deposit
    deposit.created_at.localtime + deposit.locking_time.to_i.minutes
  end

  def current_path *paths
    paths.map { |path| current_page? path }.include?(true) ? 'menu-open' : ''
  end

  def link_to_deal_add_fields(name, deal, association, append, mobile: false, class_name: '')
    new_object = deal.send(association).klass.new
    if association == :history_interactions
      fields = render("admin/history_interactions/" + association.to_s.singularize + "_fields", o: new_object, deal: deal)
    else
      fields = render("admin/deals/" + "#{mobile ? 'mobile/' : ''}" + association.to_s.singularize + "_fields", o: new_object, deal: deal)
    end
    link_to('javascript:0', class: "deal_add_fields", data: { fields: fields.gsub("\n", ""), append: append, mobile: mobile }) do
      ('<button class="btn btn-kanban-danger ' + class_name + '" type="button">
            <i class="tms tms-plus-retangle"></i>
            ' + name + '</button>').html_safe
    end
  end
end

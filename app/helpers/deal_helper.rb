module DealHelper
  def more_commission data
    comm_info = ""
    comm_info += "- Trưởng phòng KD: #{currency data[:manager_comm]} vnđ</br>" if currency data[:manager_comm].present?
    comm_info += "- Trưởng nhóm KD: #{currency data[:leader_comm]} vnđ</br>" if currency data[:leader_comm].present?
    comm_info += "- Sale admin:  #{currency data[:admin_comm]} vnđ</br>" if currency data[:admin_comm].present?
    comm_info.html_safe
  end

  def text_auto_link(content)
    auto_link(simple_format(content), html: { target: '_blank' })
  end
end

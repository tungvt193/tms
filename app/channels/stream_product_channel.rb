class StreamProductChannel < ApplicationCable::Channel
  def subscribed
    stream_from "stream_product_channel"
  end

  def unsubscribed
  end
end

class ProductChannel < ApplicationCable::Channel
  def subscribed
    stream_from "product_channel_#{params[:product_id]}"
  end

  def unsubscribed
  end
end

class DealChannel < ApplicationCable::Channel
  def subscribed
    stream_from "deal_channel"
  end

  def unsubscribed
  end
end

module Types
  class InstructionDocumentType < Types::BaseObject
    field :id, ID, null: false
    field :title, String, null: false
    field :file, String, null: false

    def file
      object.file.url
    end
  end
end

module Types
  class DivisionType < Types::BaseObject
    field :id, ID, null: false
    field :name, String, null: false
    field :level, Int, null: true
    field :parent_id, Int, null: true
    field :products_count, Int, null: true
    field :children, [Types::DivisionType], null: true

    def children
      Division.where(parent_id: object.id, level: object.level)
    end

    def products_count
      return 0 unless object.parent_id

      object.block_products.size
    end
  end
end

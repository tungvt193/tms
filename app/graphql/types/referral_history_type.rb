module Types
  class ReferralHistoryType < Types::BaseObject
    field :id, ID, null: false
    field :full_name, String, null: true
    field :phone_number, String, null: true
    field :referred_date, String, null: false

    def phone_number
      Phonelib.parse(object.referred&.phone_number, object.referred&.country_code).national&.delete(' ')
    end

    def full_name
      object.referred&.full_name
    end

    def referred_date
      object.referred_date.strftime('%d/%m/%Y')
    end
  end
end

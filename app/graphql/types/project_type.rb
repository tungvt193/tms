module Types
  class ProjectType < Types::BaseObject

    include BlogService
    include ActionView::Helpers::SanitizeHelper

    field :id, ID, null: false
    field :name, String, null: true
    field :real_estate_type, [String], null: true
    field :investors, [String], null: true
    field :features, [String], null: true
    field :description, String, null: true
    field :images, [String], null: true
    field :sale_policy, String, null: true
    field :slug, String, null: true
    field :price_from, Float, null: true
    field :price_to, Float, null: true
    field :area_from, Float, null: true
    field :area_to, Float, null: true
    field :labels, [String], null: true
    field :available_products, Integer, null: true
    field :address, String, null: true
    field :sale_documents, [Types::SaleDocumentType], null: true
    field :investor_posts, [Types::InvestorPostType], null: true
    field :image_folder_url,  String, null: true
    field :video_folder_url, String, null: true
    field :layout_url, String, null: true
    field :news_posts, [String], null: true
    field :videos_posts, [String], null: true
    field :subdivisions, [Types::DivisionType], null: true
    field :project_lockable, Boolean, null: true

    def real_estate_type
      Constant::PROJECT_REAL_ESTATE_TYPE.map { |k, v| v if object.real_estate_type.include?(k) }.compact
    end

    def investors
      Investor.where(id: object.investors).pluck(:name)
    end

    def features
      Constant::PROJECT_FEATURES.map { |k, v| v if object.features.include?(k) }.compact
    end

    def images
      object.images.first(5).map { |img| img.url }
    end

    def labels
      object.labels.map { |x| Constant::PROJECT_LABELS[x] }
    end

    def available_products
      object.products.for_sale.size
    end

    def address
      get_regions = []
      region = object.region
      if region.present?
        if region.ward
          get_regions << region.ward.name_with_type
        end
        if region.district
          get_regions << region.district.name
        end
        if region.city
          get_regions << region.city.name
        end
      end
      get_regions.join(', ')
    end

    def news_posts
      news_url = "#{ENV['WP_API_POST']}?_embed&per_page=6&tags_exclude=312&filter[tag]=#{object.slug}"
      get_posts_from_blog(news_url, 'news')
    end

    def videos_posts
      videos_url = "#{ENV['WP_API_POST']}?_embed&per_page=3&tags=312&filter[tag]=#{object.slug}"
      get_posts_from_blog(videos_url, 'video')
    end

    def description
      object.description_for_seller
    end

    def sale_documents
      object.sale_documents.order(position: :asc)
    end

    def subdivisions
      Division.where(project_id: object.id, level: 0).uniq
    end

    def project_lockable
      campaign_locks = object&.campaign_locks&.where(state: %w(upcoming locking))
      products = Product.where(id: campaign_locks.pluck(:products).flatten)
      if (campaign_locks.present? && products.select{ |p| !%w(deposited sold).include?(p.state) }.blank?) || campaign_locks.blank?
        false
      else
        true
      end
    end
  end
end

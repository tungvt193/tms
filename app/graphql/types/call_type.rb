module Types
  class CallType < Types::BaseObject
    field :id, ID, null: false
    field :contact_date, String, null: false
    field :result, String, null: true

    def contact_date
      object.contact_date.strftime('%d/%m/%Y - %H:%M')
    end
  end
end

module Types
  class DealType < Types::BaseObject
    field :id, ID, null: false
    field :name, String, null: false
    field :state, String, null: false
    field :lead_scoring, Int, null: true
    field :source, Int, null: true
    field :source_detail, String, null: true
    field :labels, [String], null: true
    field :suggestive_name, String, null: true
    field :project, Types::ProjectType, null: true
    field :product, Types::ProductType, null: true
    field :customer, Types::CustomerType, null: true
    field :contact_status, String, null: true
    field :interaction_detail, [Int], null: true
    field :product_type, Int, null: true
    field :product_type_detail, String, null: true
    field :price_range, Int, null: true
    field :acreage_range, Int, null: true
    field :interested_product, String, null: true
    field :purchase_purpose, Int, null: true
    field :product_selection_criteria, [Int], null: true
    field :door_direction, [Int], null: true
    field :balcony_direction, [Int], null: true
    field :trouble_problem_list, [Int], null: true
    field :trouble_problem, String, null: true
    field :demand_for_advances, String, null: true
    field :customer_persona, Types::CustomerPersonaType, null: true
    field :temporary_commission, Int, null: true
    field :actually_commission, Int, null: true
    field :create_deal_date, String, null: true
    field :canceled_reason, String, null: true
    field :uninterested_reason, String, null: true
    field :appointments, [Types::AppointmentType], null: true
    field :history_interactions, [Types::HistoryInteractionType], null: true
    field :assignee, Types::UserType, null: true
    field :deal_journeys, [Types::DealJourneyType], null: true
    field :telesale_note, String, null: true
    field :telesale_interest_detail, String, null: true
    field :calls, [Types::CallType], null: true
    field :is_transfer, Boolean, null: true
    field :total_price, GraphQL::Types::BigInt, null: true
    field :external_project, String, null: true
    field :cross_selling_product_code, String, null: true
    field :product_lock_history, Types::ProductLockHistoryType, null: true

    def canceled_reason
      Constant::CANCELED_REASON[object.canceled_reason]
    end

    def uninterested_reason
      Constant::UNINTERESTED_REASON[object.uninterested_reason]
    end

    def temporary_commission
      object.transfer_histories.present? ? 0 : object.commission&.temporary_commission
    end

    def actually_commission
      object.transfer_histories.present? ? 0 : object.commission&.actually_commission
    end

    def create_deal_date
      object.create_deal_date.strftime('%d/%m/%Y - %H:%M')
    end

    def appointments
      object.appointments.order(appointment_date: :desc, created_at: :desc)
    end

    def history_interactions
      object.history_interactions.order(interaction_date: :desc, created_at: :desc)
    end

    def deal_journeys
      object.deal_journeys.order("deal_journeys.create_date DESC")
    end
    
    def is_transfer
      object.transfer_histories.present?
    end
  end
end

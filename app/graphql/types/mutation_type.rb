module Types
  class MutationType < Types::BaseObject
    # All endpoints to handle POST/PUT/PATCH/DELETE requests from client

    field :create_user, mutation: Mutations::CreateUser
    field :create_customer, mutation: Mutations::CreateCustomer
    field :create_deal,  mutation: Mutations::CreateDeal
    field :share_customer,  mutation: Mutations::ShareCustomer
    field :update_profile,  mutation: Mutations::UpdateProfile
    field :create_appointment,  mutation: Mutations::CreateAppointment
    field :create_history_interaction,  mutation: Mutations::CreateHistoryInteraction
    field :update_appointment,  mutation: Mutations::UpdateAppointment
    field :update_history_interaction,  mutation: Mutations::UpdateHistoryInteraction
    field :update_deal,  mutation: Mutations::UpdateDeal
    field :read_all_notification, mutation: Mutations::ReadAllNotification
    field :read_a_notification, mutation: Mutations::ReadANotification
    field :remove_appointment, mutation: Mutations::RemoveAppointment
    field :remove_history_interaction, mutation: Mutations::RemoveHistoryInteraction
    field :create_history_call, mutation: Mutations::CreateHistoryCall
    field :update_call, mutation: Mutations::UpdateCall
    field :create_product_lock_history, mutation: Mutations::CreateProductLockHistory
    field :create_product_lock_history_enqueued, mutation: Mutations::CreateProductLockHistoryEnqueued
    field :update_product_lock_history, mutation: Mutations::UpdateProductLockHistory
    field :update_deal_product_locking, mutation: Mutations::UpdateDealProductLocking
  end
end

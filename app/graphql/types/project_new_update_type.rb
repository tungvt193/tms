module Types
  class ProjectNewUpdateType < Types::BaseObject
    field :id, ID, null: false
    field :project_id, Int, null: false
    field :project_thumb, String, null: false
    field :title, String, null: false
    field :author, UserType, null: true
    field :content, String, null: false
    field :created_at, String, null: false

    def project_thumb
      object.project.images.first.url
    end

    def created_at
      object.created_at.to_i
    end

    def title
      object.project.name
    end
  end
end

module Types
  class RevenueRankingType < Types::BaseObject
    field :full_name, String, null: false
    field :total_revenue, Int, null: true
    field :current_user, Boolean, null: false
  end
end

module Types
  class CustomerType < Types::BaseObject
    field :id, ID, null: false
    field :name, String, null: false
    field :phone_number, String, null: true
    field :country_code, String, null: true
    field :data_source, Int, null: true
    field :email, String, null: true
    field :gender, Int, null: true
    field :identity_card, String, null: true
    field :address, String, null: true
    field :second_phone_number, String, null: true
    field :state, String, null: true
    field :note, String, null: true
    field :work_place, String, null: true
    field :position, String, null: true
    field :income, String, null: true
    field :detail, String, null: true
    field :financial_capability, String, null: true
    field :nationality, String, null: true
    field :property_ownership, String, null: true
    field :job, String, null: true
    field :marital_status, Int, null: true
    field :customer_characteristic, [Int], null: true

    def phone_number
      Phonelib.parse(object.phone_number, object.country_code).national&.delete(' ')
    end

    def second_phone_number
      Phonelib.parse(object.second_phone_number, object.country_code).national&.delete(' ')
    end
  end
end

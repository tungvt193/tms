module Types
  class CompleteDealsRankingType < Types::BaseObject
    field :full_name, String, null: false
    field :total_complete_deals, Int, null: true
    field :current_user, Boolean, null: false
  end
end

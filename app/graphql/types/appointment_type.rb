module Types
  class AppointmentType < Types::BaseObject
    field :id, ID, null: false
    field :content, String, null: false
    field :appointment_date, String, null: false
    field :created_by_id, Int, null: false
    field :state, String, null: true
    field :result, String, null: true

    def appointment_date
      object.appointment_date.strftime('%d/%m/%Y - %H:%M')
    end

    def state
      Constant::APPOINTMENT_STATE[object.state]
    end
  end
end

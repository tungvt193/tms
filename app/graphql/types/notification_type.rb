module Types
  class NotificationType < Types::BaseObject
    field :id, ID, null: false
    field :content, String, null: true
    field :object_id, ID, null: true
    field :notification_type, String, null: true
    field :created_at, String, null: false
    field :is_read, Boolean, null: true
    field :clickable, Boolean, null: true
    field :related_url, String, null: true

    def content
      ActionView::Base.full_sanitizer.sanitize(object.content)
    end

    def object_id
      object.get_object_id
    end

    def is_read
      object.read_at.present? ? true : false
    end

    def clickable
      object.related_url.present? ? true : false
    end
  end
end

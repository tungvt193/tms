module Types
  class InvestorPostType < Types::BaseObject
    field :id, ID, null: false
    field :project_id, Int, null: false
    field :title, String, null: false
    field :post_url, String, null: false
  end
end

module Types
  class QueryType < Types::BaseObject
    # Add `node(id: ID!) and `nodes(ids: [ID!]!)`
    include GraphQL::Types::Relay::HasNodeField
    include GraphQL::Types::Relay::HasNodesField

    # All endpoints to handle GET requests from client

    field :user, resolver: Resolvers::User
    field :projects, resolver: Resolvers::Projects
    field :project, resolver: Resolvers::Project
    field :project_suggestion, resolver: Resolvers::ProjectSuggestion
    field :list_city_for_filter_project, resolver: Resolvers::ListCityForFilterProject
    field :notifications, resolver: Resolvers::Notifications
    field :notification, resolver: Resolvers::Notification
    field :unread_notifications, Int, null: true
    field :news, resolver: Resolvers::News
    field :banners, resolver: Resolvers::Banners
    field :deals, resolver: Resolvers::Deals
    field :deal, resolver: Resolvers::Deal
    field :deal_suggestion, resolver: Resolvers::DealSuggestion
    field :list_project_for_filter_deal, resolver: Resolvers::ListProjectForFilterDeal
    field :project_popular_search, resolver: Resolvers::ProjectPopularSearch
    field :customers, resolver: Resolvers::Customers
    field :low_products, resolver: Resolvers::LowProducts
    field :low_product, resolver: Resolvers::LowProduct
    field :high_products, resolver: Resolvers::HighProducts
    field :high_product, resolver: Resolvers::HighProduct
    field :subdivisions, resolver: Resolvers::Subdivisions
    field :real_estate_types, resolver: Resolvers::RealEstateTypes
    field :instruction_documents, resolver: Resolvers::InstructionDocuments
    field :project_new_updates, resolver: Resolvers::ProjectNewUpdates
    field :assignee, resolver: Resolvers::Assignee
    field :list_child_region_by_parent, resolver: Resolvers::ListChildRegionByParent
    field :list_available_product, resolver: Resolvers::ListAvailableProduct
    field :shared_customers, resolver: Resolvers::SharedCustomers
    field :report, resolver: Resolvers::Report
    field :dashboard_sale_admin_assignee, resolver: Resolvers::DashboardSaleAdminAssignee
    field :dashboard_sale_admin_deals, resolver: Resolvers::DashboardSaleAdminDeals
    field :dashboard_sale_admin_commission, resolver: Resolvers::DashboardSaleAdminCommission
    field :referral_histories, resolver: Resolvers::ReferralHistories
    field :referral_count, Int, null: true
    field :product_suggestion, resolver: Resolvers::ProductSuggestion
    field :product_lock_histories, resolver: Resolvers::ProductLockHistories
    field :check_project_lockable, resolver: Resolvers::CheckProjectLockable
    field :product_lock_history, resolver: Resolvers::ProductLockHistory
    field :products_for_lock, resolver: Resolvers::ProductsForLock

    def unread_notifications
      context[:current_resource].notifications.where(read_at: nil).size
    end

    def referral_count
      context[:current_resource].referral_histories.size
    end
  end
end

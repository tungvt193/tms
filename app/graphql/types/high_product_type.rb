module Types
  class HighProductType < Types::BaseObject
    field :id, ID, null: false
    field :code, String, null: false
    field :project_id, Int, null: false
    field :state, String, null: false
    field :name, String, null: false
    field :carpet_area, Float, null: false
    field :direction, Int, null: false
    field :sum_price, GraphQL::Types::BigInt, null: false
    field :subdivision, DivisionType, null: false
    field :block, DivisionType, null: false
    field :floor, DivisionType, null: true
    field :label, String, null: true
    field :built_up_area, Float, null: false
    field :product_type, String, null: false
    field :level, Int, null: false
    field :real_estate_type, Int, null: false
    field :images, [String], null: true
  end
end

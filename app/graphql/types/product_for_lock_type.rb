module Types
  class ProductForLockType < Types::BaseObject
    field :id, ID, null: false
    field :code, String, null: false
    field :sum_price, Float, null: false
    field :label, String, null: true
    field :label_type, String, null: false
    field :time_remaining, Int, null: true
    field :maximum_lock, Boolean, null: false

    def label
      campaign_lock = ::CampaignLock.where("products && ?", "{#{object.id}}").first
      if object.state == 'for_sale' && campaign_lock.present? && campaign_lock.session_opening_time < Time.zone.now
        'Được lock luôn'
      elsif object.product_lock_histories.where(state: %w(locking deposit_confirmation)).present?
        'Đang được lock'
      elsif object.product_lock_histories.where(state: 'waiting_reti_acceptance').present?
        'Đang xác nhận cọc'
      elsif object.deals.where(state: 'deposit').present? && object.product_lock_histories.where(state: 'deal_confirmation').present?
        'Đã đặt cọc'
      elsif object.product_lock_histories.where(state: 'pending').present?
        'Tạm treo'
      else
        'Chưa ra hàng'
      end
    end

    def label_type
      campaign_lock = ::CampaignLock.where("products && ?", "{#{object.id}}").first
      if object.state == 'for_sale' && campaign_lock.present? && campaign_lock.session_opening_time < Time.zone.now
        'enable_lock'
      elsif object.product_lock_histories.where(state: %w(locking deposit_confirmation)).present?
        'locked'
      elsif object.product_lock_histories.where(state: 'waiting_reti_acceptance').present?
        'deposit_confirming'
      elsif object.deals.where(state: 'deposit').present? && object.product_lock_histories.where(state: 'deal_confirmation').present?
        'deposited'
      elsif object.product_lock_histories.where(state: 'pending').present?
        'pending'
      else
        'not_release'
      end
    end

    def time_remaining
      product_locking = object.product_lock_histories.where(state: %w(locking deposit_confirmation waiting_reti_acceptance))
      return nil unless product_locking.present?
      if (product_locking.first.state == 'waiting_reti_acceptance') && (product_locking.first.deadline_for_sale_admin > Time.zone.now)
        (product_locking.first.deadline_for_sale_admin - Time.zone.now).round()
      elsif product_locking.first.deadline_for_sale > Time.zone.now
        (product_locking.first.deadline_for_sale - Time.zone.now).round()
      end
    end

    def maximum_lock
      current_user = context[:current_resource]
      campaign_lock = ::CampaignLock.where("products && ?", "{#{object.id}}").first
      products_lock = ::ProductLockHistory.where(user_id: current_user.id, state: 'locking', project_id: object.project.id).select { |plh| plh.product.tag == 'Hàng HOT' }
      if campaign_lock&.enable_locked_for_hot_product == products_lock.size
        true
      else
        false
      end
    end
  end
end

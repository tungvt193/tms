module Types
  class DealJourneyType < Types::BaseObject
    field :id, ID, null: false
    field :content, String, null: false
    field :create_date, String, null: false

    def create_date
      object.create_date.strftime('%d/%m/%Y - %H:%M')
    end
  end
end

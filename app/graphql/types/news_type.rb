module Types
  class NewsType < Types::BaseObject
    field :message, String, null: false
  end
end

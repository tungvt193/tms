module Types
  class HistoryInteractionType < Types::BaseObject
    field :id, ID, null: false
    field :interaction_type, String, null: false
    field :interaction_content, String, null: false
    field :interaction_date, String, null: false
    field :created_by_id, Int, null: false

    def interaction_date
      object.interaction_date.strftime('%d/%m/%Y - %H:%M')
    end

    def interaction_type
      object.get_interaction_type
    end
  end
end

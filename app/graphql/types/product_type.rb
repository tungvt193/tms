module Types
  class ProductType < Types::BaseObject
    field :id, ID, null: false
    field :code, String, null: false
    field :real_estate_type, Int, null: false
  end
end

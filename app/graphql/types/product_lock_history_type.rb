module Types
  class ProductLockHistoryType < Types::BaseObject
    field :id, ID, null: false
    field :state, String, null: false
    field :time_remaining, Int, null: true
    field :user, Types::UserType, null: true
    field :project, Types::ProjectType, null: true
    field :product, Types::ProductType, null: true
    field :unc_image, String, null: true
    field :failure_reason, String, null: true
    field :locking_session, Types::ProductLockHistoryType, null: true
    field :sale_admin, Types::UserType, null: true

    def time_remaining
      if %i(locking deposit_confirmation).include?(object.state.to_sym) && object.deadline_for_sale.present? && object.deadline_for_sale > Time.zone.now
        (object.deadline_for_sale - Time.zone.now).round()
      elsif object.state == 'waiting_reti_acceptance' && object.deadline_for_sale_admin.present? && object.deadline_for_sale_admin > Time.zone.now
        (object.deadline_for_sale_admin - Time.zone.now).round()
      else
        nil
      end
    end

    def locking_session
      ::ProductLockHistory.locking_process.find_by(product_id: object&.product_id)
    end

    def unc_image
      object.unc_image&.url
    end

    def sale_admin
      campaign_lock = ::CampaignLock.where("products && ?", "{#{object.product_id}}").first
      User.find_by(id: campaign_lock.confirm_deposit_by_sale_admin)
    end
  end
end

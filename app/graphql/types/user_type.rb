module Types
  class UserType < Types::BaseObject
    field :id, ID, null: true
    field :email, String, null: true
    field :full_name, String, null: false
    field :role, String, null: true
    field :avatar, String, null: true
    field :code, String, null: true
    field :account_type, Integer, null: false
    field :phone_number, String, null: true
    field :actually_commission, String, null: true
    field :temporary_commission, String, null: true
    field :gender, String, null: true
    field :city_id, Int, null: true
    field :main_job, Int, null: true
    field :referral_code, String, null: false

    def role
      object.role.name
    end

    def account_type
      ::User.account_types[object.account_type]
    end

    def phone_number
      Phonelib.parse(object.phone_number, object.country_code).national&.delete(' ')
    end

    # TODO: format commission when return to client
    def actually_commission
      current_user = context[:current_resource]
      commission = ::Deal.joins(:commission).contract_signed
                         .where(assignee_id: current_user.id, contract_signed_at: DateTime.now.beginning_of_month..DateTime.now.end_of_month)
                         .sum('commissions.actually_commission')
      commission > 0 ? commission : 'Chưa cập nhật'
    end

    # TODO: format commission when return to client
    def temporary_commission
      current_user = context[:current_resource]
      commission = ::Deal.joins(:commission).contract_signed
                         .where(assignee_id: current_user.id, contract_signed_at: DateTime.now.beginning_of_month..DateTime.now.end_of_month)
                         .sum('commissions.temporary_commission')
      commission > 0 ? commission : 'Chưa cập nhật'
    end
  end
end

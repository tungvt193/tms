module Types
  class BannerType < Types::BaseObject
    field :banner_app_url, String, null: true
    field :title, String, null: true
    field :link, String, null: true

    def banner_app_url
      object.banner_mobile&.url
    end
  end
end

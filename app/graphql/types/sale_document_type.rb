module Types
  class SaleDocumentType < Types::BaseObject
    field :id, ID, null: false
    field :project_id, Int, null: false
    field :title, String, null: false
    field :file, String, null: true
    field :slug, String, null: true
    field :sale_document_type, String, null: true
    field :document_url, String, null: true
    field :created_at, String, null: false
    field :updated_at, String, null: false

    def file
      object.file.url
    end

    def document_url
      "#{ENV['WEBSITE_URL']}/tai-lieu/#{object.slug}"
    end

    def created_at
      object.created_at.strftime('%d/%m/%Y')
    end

    def updated_at
      object.updated_at.strftime('%d/%m/%Y')
    end
  end
end

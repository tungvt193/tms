module Types
  class ReportType < Types::BaseObject
    field :total_deals, Int, null: true
    field :complete_deals, Int, null: true
    field :total_value, Float, null: true
    field :temporary_commission, Float, null: true
    field :actually_commission, Float, null: true
    field :revenue_ranking, [Types::RevenueRankingType], null: true
    field :complete_deals_ranking, [Types::CompleteDealsRankingType], null: true

    def total_deals
      if object.super_admin? || object.director?
        Deal.all.size
      else
        object.get_deals.where('state IN (?)', %w(interest meet refundable_deposit lock deposit)).size
      end
    end

    def complete_deals
      if object.super_admin? || object.director?
        Deal.contract_signed
      else
        object.get_deals.contract_signed.size
      end
    end

    def total_value
      if object.super_admin? || object.director?
        Deal.contract_signed.sum(:total_price)
      else
        object.get_deals.contract_signed.sum(:total_price)
      end
    end

    def temporary_commission
      ::Commission.where(deal_id: object.get_deals.contract_signed.pluck(:id)).sum(:temporary_commission)
    end

    def actually_commission
      ::Commission.where(deal_id: object.get_deals.contract_signed.pluck(:id)).sum(:actually_commission)
    end

    def revenue_ranking
      deals_users = Deal.contract_signed.where(assignee_id: object.root.subtree_ids).where.not(total_price: nil)
      total_revenue = deals_users.group_by(&:assignee_id).map do |records|
        sum_revenue_users = (records.second.map(&:total_price).sum.to_f / 1000000000).round(0)
        { full_name: User.find(records.first)&.full_name, total_revenue: sum_revenue_users, current_user: false }
      end.sort_by { |x| x[:total_price] }.reverse.first(4)
      total_revenue << { full_name: object&.full_name, total_revenue: (object.get_deals.contract_signed.sum(:total_price).to_f / 1000000000).round(0), current_user: true }
      total_revenue.sort_by { |x| x[:total_price] }.reverse
    end

    def complete_deals_ranking
      deals_users = Deal.contract_signed.where(assignee_id: object.root.subtree_ids).where.not(total_price: nil)
      total_complete_deals = deals_users.group_by(&:assignee_id).map do |records|
        { full_name: User.find(records.first)&.full_name, total_complete_deals: records.size, current_user: false }
      end.sort_by { |x| x[:total_complete_deals] }.reverse.first(4)
      total_complete_deals << { full_name: object&.full_name, total_complete_deals: object.get_deals.contract_signed.size, current_user: true }
      total_complete_deals.sort_by { |x| x[:total_complete_deals] }.reverse
    end
  end
end

module Types
  class LowProductType < Types::BaseObject
    field :id, ID, null: false
    field :code, String, null: false
    field :project_id, Int, null: false
    field :subdivision, Types::DivisionType, null: false
    field :real_estate_type, Int, null: true
    field :state, String, null: false
    field :plot_area, Float, null: false
    field :density, Float, null: false
    field :floor_area, Float, null: false
    field :direction, Int, null: false
    field :sum_price, GraphQL::Types::BigInt, null: false
    field :label, String, null: true
    field :block, DivisionType, null: false
    field :product_type, String, null: false
    field :statics, String, null: false
    field :images, [String], null: true
  end
end

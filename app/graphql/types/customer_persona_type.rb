module Types
  class CustomerPersonaType < Types::BaseObject
    field :id, ID, null: false
    field :customer_id, Int, null: true
    field :deal_id, Int, null: false
    field :second_phone_number, String, null: true
    field :email, String, null: true
    field :city, Types::RegionType, null: true
    field :district, Types::RegionType, null: true
    field :ward, Types::RegionType, null: true
    field :settlements, String, null: true
    field :gender, Int, null: true
    field :identity_card, String, null: true
    field :domicile, String, null: true
    field :dob, String, null: true
    field :nationality, String, null: true
    field :workplace, String, null: true
    field :customer_position, String, null: true
    field :financial_capability, String, null: true
    field :property_ownership, String, null: true
    field :yob, String, null: true

    def city
      ::RegionData.find_by(id: object.city_id)
    end

    def district
      ::RegionData.find_by(id: object.district_id)
    end

    def ward
      ::RegionData.find_by(id: object.ward_id)
    end

    def dob
      object.dob.present? ? object.dob.strftime('%d/%m/%Y') : nil
    end

    def yob
      object.dob.present? ? object.dob.strftime('%d/%m/%Y') : nil
    end
  end
end

module Resolvers
  class LowProducts < Resolvers::BaseResolver
    description 'Get all low products'

    type Types::LowProductType.collection_type, null: false

    argument :project_id, Int, required: true
    argument :query, String, required: false
    argument :state, [String], required: false
    argument :real_estate_type, [Int], required: false
    argument :subdivision_id, [Int], required: false

    argument :per_page, Integer, required: false, default_value: 20
    argument :page, Integer, required: false, default_value: 1

    def resolve(project_id:, query:, state:, real_estate_type:, subdivision_id:, page:, per_page:)
      project = ::Project.find_by_id(project_id)
      return [] unless  project.present?
      params = {
        'query': query,
        'state': state,
        'subdivision_id': subdivision_id,
        'real_estate_type': real_estate_type
      }
      project.products.search_result(params, level: 1).where('state IN (?)', %w(for_sale locking deposited sold)).page(page).per(per_page).order("products.code ASC")
    end
  end
end

module Resolvers
  class Notification < Resolvers::BaseResolver
    description 'Find a notification by ID'

    type Types::NotificationType, null: false

    argument :id, ID, required: true

    def resolve(id:)
      ::Notification.find(id)
    end
  end
end

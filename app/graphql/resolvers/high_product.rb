module Resolvers
  class HighProduct < Resolvers::BaseResolver
    description 'Find a high product by ID'

    argument :id, ID, required: true
    
    type Types::HighProductType, null: false

    def resolve(id:)
      ::Product.where(level: 0, id: id).first
    end
  end
end

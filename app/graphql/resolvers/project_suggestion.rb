module Resolvers
  class ProjectSuggestion < Resolvers::BaseResolver
    description 'Suggest project name when user typing'

    type [Types::ProjectType], null: false

    argument :keyword, String, required: true

    def resolve(keyword:)
      ::Project.agent_app_autocomplete_result(keyword)
    end
  end
end

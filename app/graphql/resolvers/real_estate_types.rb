module Resolvers
  class RealEstateTypes < Resolvers::BaseResolver
    description 'Get list real estate types for low products'

    type [Types::RealEstateType], null: false

    argument :project_id, Int, required: true
    argument :level, Int, required: true

    def resolve(project_id:, level:)
      project = ::Project.find_by_id(project_id)
      return [] unless  project.present?
      products = project&.products.where(level: level).where('state IN (?)', %w(for_sale locking deposited sold))
      products.map { |p| {name: Reti::Product::REAL_ESTATE_TYPES[level].values_at(p.real_estate_type).first, id: p.real_estate_type} }.uniq.sort
    end
  end
end

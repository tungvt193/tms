module Resolvers
  class DealSuggestion < Resolvers::BaseResolver
    description 'Suggest deal name when user typing'

    type [Types::DealType], null: false

    argument :keyword, String, required: true

    def resolve(keyword:)
      current_user = context[:current_resource]
      ::Deal.agent_app_autocomplete_result(keyword, current_user.id)
    end
  end
end


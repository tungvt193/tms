module Resolvers
  class InstructionDocuments < Resolvers::BaseResolver
    description 'Get list subdivision for low products'

    type [Types::InstructionDocumentType], null: false

    def resolve
      ::InstructionDocument.all.order(id: :asc)
    end
  end
end
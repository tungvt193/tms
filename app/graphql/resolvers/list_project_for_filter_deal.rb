module Resolvers
  class ListProjectForFilterDeal < Resolvers::BaseResolver
    description 'List project for filter deal'

    type [Types::ProjectType], null: false

    def resolve
      current_user = context[:current_resource]
      project_ids = current_user.deals.pluck(:project_id).uniq
      ::Project.by_ids(project_ids).order(name: :asc) + [id: 0, name: 'Khác']
    end
  end
end

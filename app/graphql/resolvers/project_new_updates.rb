module Resolvers
  class ProjectNewUpdates < Resolvers::BaseResolver
    description 'Get all project new updates'

    type [Types::ProjectNewUpdateType], null: false

    def resolve
      ::ProjectNewUpdate.all.order(created_at: :desc)
    end
  end
end

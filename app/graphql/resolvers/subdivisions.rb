module Resolvers
  class Subdivisions < Resolvers::BaseResolver
    description 'Get list subdivision for low products'

    type [Types::DivisionType], null: false

    argument :project_id, Int, required: true
    argument :level, Int, required: true

    def resolve(project_id:, level:)
      project = ::Project.find_by_id(project_id)
      return [] unless  project.present?
      ::Division.where(project_id: project.id, level: level).order(name: :asc)
    end
  end
end

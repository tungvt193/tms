module Resolvers
  class Deal < Resolvers::BaseResolver
    description 'Find a deal by ID'

    type Types::DealType, null: false

    argument :id, ID, required: true

    def resolve(id:)
      ::Deal.find(id)
    end
  end
end

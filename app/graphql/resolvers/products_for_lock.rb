module Resolvers
  class ProductsForLock < Resolvers::BaseResolver
    description 'Products for lock'

    type Types::ProductForLockType.collection_type, null: false

    argument :project_id, Int, required: true
    argument :query, String, required: false
    argument :per_page, Integer, required: false, default_value: 20
    argument :page, Integer, required: false, default_value: 1

    def resolve(project_id:, query:, per_page:, page:)
      project = ::Project.find_by_id(project_id)
      product_list = []
      campaign_locks = project.campaign_locks.where('session_opening_time < ?', DateTime.now)
      campaign_locks.each do |campaign_lock|
        product_list << ::Product.where(id: campaign_lock.products).order(code: :asc)
      end
      if query.present?
        product_list = product_list.flatten.select{ |p| p.code.to_s.include?(query)}
      else
        product_list = product_list.flatten
      end
      products = []
      products << product_list.select { |p| p.state == 'for_sale' && ::CampaignLock.where("products && ?", "{#{p.id}}").present? }
      products << product_list.select { |p| p.product_lock_histories.where(state: 'locking').present? }
      products << product_list.select { |p| p.product_lock_histories.where(state: 'deposit_confirmation').present? }
      products << product_list.select { |p| p.product_lock_histories.where(state: 'waiting_reti_acceptance').present? }
      products << product_list.select { |p| p.deals.where(state: 'deposit').present? && p.product_lock_histories.where(state: 'deal_confirmation').present? }
      products << product_list.select { |p| p.product_lock_histories.where(state: 'pending').present? }
      products << product_list.select { |p| p.state == 'for_sale' && ::CampaignLock.where("products && ?", "{#{p.id}}").blank? }
      products_not_in_campaign_lock = (query.present? ? project.products.where(state: %w(for_sale locking deposited)).select{ |p| p.code.to_s.include?(query)} : project.products.where(state: %w(for_sale locking deposited))) - product_list
      products << products_not_in_campaign_lock.select { |p| p.state == 'for_sale' }
      products << products_not_in_campaign_lock.select { |p| p.state == 'locking' }
      products << products_not_in_campaign_lock.select { |p| p.state == 'deposited' }
      Kaminari.paginate_array(products.flatten.uniq).page(page).per(per_page)
    end
  end
end

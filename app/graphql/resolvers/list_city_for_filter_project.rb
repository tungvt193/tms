module Resolvers
  class ListCityForFilterProject < Resolvers::BaseResolver
    description 'List city for filter project'

    type [Types::RegionType], null: false

    def resolve
      city_ids = ::Region.where(objectable_type: 'Project').pluck(:city_id).uniq
      RegionData.where(id: city_ids)
    end
  end
end

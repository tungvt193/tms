module Resolvers
  class LowProduct < Resolvers::BaseResolver
    description 'Find a product by ID'

    argument :id, ID, required: true
    
    type Types::LowProductType, null: false

    def resolve(id:)
      ::Product.where(level: 1, id: id).first
    end
  end
end

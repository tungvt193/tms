module Resolvers
  class DealJourneys < Resolvers::BaseResolver
    description 'Get list deal journey by deal_id'

    type [Types::DealJourneyType], null: false

    argument :deal_id, Int, required: true

    def resolve(deal_id:)
      deal = ::Deal.find_by_id deal_id
      return [] unless deal.present?

      deal.deal_journeys
    end
  end
end

module Resolvers
  class Banners < Resolvers::BaseResolver
    type [Types::BannerType], null: false

    def resolve
      ::Banner.banner_for_app.order(position: :asc)
    end
  end
end

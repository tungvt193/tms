module Resolvers
  class Deals < Resolvers::BaseResolver
    description 'Get all deals'

    type Types::DealType.collection_type, null: false

    argument :query, String, required: false
    argument :state, [String], required: false
    argument :source, [Int], required: false
    argument :product_type, [Int], required: false
    argument :project, [Int], required: false
    argument :per_page, Integer, required: false, default_value: 20
    argument :page, Integer, required: false, default_value: 1

    def resolve(query:, state:, source:, product_type:, project:, page:, per_page:)
      params = {
        'query': query,
        'state': state,
        'source': source,
        'product_type': product_type,
        'project': project
      }
      deals = ::Deal.search_result(params).order("deals.created_at DESC")
      current_user = context[:current_resource]
      permission = current_user.get_permission("Deal")
      unless permission
        []
      else
        case permission.data_area
        when 0
          deals.where(assignee_id: current_user.id).where(state: permission.state_area).page(page).per(per_page)
        when 1
          deals.where(assignee_id: current_user.subtree_ids).where(state: permission.state_area).page(page).per(per_page)
        when 2
          deals.where(state: permission.state_area).page(page).per(per_page)
        else
          []
        end
      end
    end
  end
end

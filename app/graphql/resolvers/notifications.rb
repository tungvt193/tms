module Resolvers
  class Notifications < Resolvers::BaseResolver
    description 'Get all notification'

    type Types::NotificationType.collection_type, null: false

    argument :unread, Boolean, required: false, default_value: false
    argument :per_page, Integer, required: false, default_value: 20
    argument :page, Integer, required: false, default_value: 1

    def resolve(unread:, page:, per_page:)
      user = context[:current_resource]
      if unread
        user.notifications.where(read_at: nil).order(created_at: :desc).page(page).per(per_page)
      else
        user.notifications.order(created_at: :desc).page(page).per(per_page)
      end
    end
  end
end

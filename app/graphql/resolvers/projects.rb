module Resolvers
  class Projects < Resolvers::BaseResolver
    description 'Get all projects'

    type Types::ProjectType.collection_type, null: false

    argument :query, String, required: false
    argument :real_estate_type, [Int], required: false
    argument :features, [Int], required: false
    argument :city, [Int], required: false
    argument :per_page, Integer, required: false, default_value: 20
    argument :page, Integer, required: false, default_value: 1

    def resolve(query:, real_estate_type:, features:, city:, per_page:, page:)
      params = {
        'query': query,
        'real_estate_type': real_estate_type,
        'features': features,
        'city': city
      }
      ::Project.agent_app_search_result(params).page(page).per(per_page)
    end
  end
end

module Resolvers
  class User < Resolvers::BaseResolver
    description 'Get current user profile'
    
    type Types::UserType, null: false
    
    def resolve
      context[:current_resource]
    end
  end
end

module Resolvers
  class Users < Resolvers::BaseResolver
    description 'Get all users'
    
    type [Types::UserType], null: false
    
    def resolve
      ::User.all.order(id: :asc)
    end
  end
end

module Resolvers
  class ListChildRegionByParent < Resolvers::BaseResolver
    description 'Get list child region by parent_id'

    type [Types::RegionType], null: true

    argument :parent_id, Int, required: true

    def resolve(parent_id:)
      ::RegionData.where(parent_id: parent_id).order(name: :asc)
    end
  end
end

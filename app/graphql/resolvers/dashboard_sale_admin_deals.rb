module Resolvers
  class DashboardSaleAdminDeals < Resolvers::BaseResolver
    description 'Get all deals incomplete (deposit lock contract_signed)'

    type Types::DealType.collection_type, null: false

    argument :per_page, Integer, required: false, default_value: 20
    argument :page, Integer, required: false, default_value: 1


    def resolve(page:, per_page:)
      deals_sort = []
      deals = []
      ::Deal.where(state: %w(deposit lock contract_signed)).each do |deal|
        if deal.invalid? || deal.customer_persona&.invalid?
          time_exist = ((deal.deal_log&.send("#{deal.state}_reentered").present? ? deal.deal_log&.send("#{deal.state}_reentered") : deal.deal_log&.send("#{deal.state}_entered")))
          deals_sort << [deal, time_exist.present? ? (Time.now - time_exist) : 0]
        end
      end
      deals_sort.sort_by { |d| d.second }.reverse.each do |deal_sort|
        deals << deal_sort.first
      end
      Kaminari.paginate_array(deals).page(page).per(per_page)
    end
  end
end

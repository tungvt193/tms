module Resolvers
  class ProductLockHistories < Resolvers::BaseResolver
    description 'Get all product lock histories'

    type Types::ProductLockHistoryType.collection_type, null: false

    argument :product_lock_type, String, required: false, default_value: 'locking'
    argument :per_page, Integer, required: false, default_value: 20
    argument :page, Integer, required: false, default_value: 1

    def resolve(product_lock_type:, page:, per_page:)
      current_user = context[:current_resource]
      product_lock_histories = []
      if product_lock_type == 'locking'
        product_lock_histories << ::ProductLockHistory.get_product_lock_histories('locking', current_user.id)
        product_lock_histories << ::ProductLockHistory.get_product_lock_histories('deposit_confirmation', current_user.id)
        product_lock_histories << ::ProductLockHistory.get_product_lock_histories('waiting_reti_acceptance', current_user.id)
        product_lock_histories << ::ProductLockHistory.get_product_lock_histories('deal_confirmation', current_user.id)
        product_lock_histories << ::ProductLockHistory.get_product_lock_histories('enqueued', current_user.id)
        product_lock_histories << ::ProductLockHistory.get_product_lock_histories('pending', current_user.id)
      else
        product_lock_histories << ::ProductLockHistory.get_product_lock_histories('success', current_user.id)
        product_lock_histories << ::ProductLockHistory.get_product_lock_histories('failed', current_user.id)
        product_lock_histories << ::ProductLockHistory.get_product_lock_histories('out_of_time', current_user.id)
        product_lock_histories << ::ProductLockHistory.get_product_lock_histories('canceled', current_user.id)
      end
      Kaminari.paginate_array(product_lock_histories.flatten).page(page).per(per_page)
    end
  end
end

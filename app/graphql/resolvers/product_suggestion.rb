module Resolvers
  class ProductSuggestion < Resolvers::BaseResolver
    description 'Product Suggestion'

    type Types::HighProductType.collection_type, null: false

    argument :project_id, Int, required: true
    argument :query, String, required: true
    argument :level, Int, required: false, default_value: 0

    argument :per_page, Integer, required: false, default_value: 20
    argument :page, Integer, required: false, default_value: 1

    def resolve(project_id:, query:, level:, per_page:, page:)
      project = ::Project.find_by_id(project_id)
      return [] unless  project.present?

      project.products.autocomplete_result_graphql(query, level.to_i).page(page).per(per_page).order("products.code ASC")
    end
  end
end

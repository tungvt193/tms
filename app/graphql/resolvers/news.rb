module Resolvers
  class News < Resolvers::BaseResolver
    require 'open-uri'
    description 'Get all messages'

    type [Types::NewsType], null: false

    def resolve
      data = read_data_from_csv
      result = []
      users = ::User.includes(:role).internal
      data.split("\n").each do |v|
        users.each do |user|
          next unless user.is_sale?
          name = user.full_name.split(' ')
          name[name.length - 1] = "*****"
          name = name.join(' ')
          result << { message: "#{name} #{v.gsub('"', '').gsub(v[0], v[0].downcase)}" }
        end
      end
      result
    rescue
      []
    end

    def read_data_from_csv
      path_file = 'https://' + ENV['S3_BUCKET'] + '.s3.' + ENV['S3_REGION'] + '.amazonaws.com/app/news/news.csv'
      open(path_file, "r:UTF-8") {|f| f.read }
    end
  end
end

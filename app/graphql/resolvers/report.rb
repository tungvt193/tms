module Resolvers
  class Report < Resolvers::BaseResolver
    type Types::ReportType, null: false

    def resolve
      context[:current_resource]
    end
  end
end

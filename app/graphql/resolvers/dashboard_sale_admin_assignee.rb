module Resolvers
  class DashboardSaleAdminAssignee < Resolvers::BaseResolver
    description 'Get all deals assignee nil from callio and app'

    type Types::DealType.collection_type, null: false

    argument :per_page, Integer, required: false, default_value: 20
    argument :page, Integer, required: false, default_value: 1

    def resolve(page:, per_page:)
      ::Deal.left_outer_joins(:author).where('assignee_id IS NULL AND (callio_campaign_id IS NOT NULL OR source = ? OR account_type = ?)', 43, 1).order(created_at: :asc).page(page).per(per_page)
    end
  end
end

module Resolvers
  class CheckProjectLockable < Resolvers::BaseResolver
    description 'Check project lockable'

    argument :id, ID, required: true
    type Types::ProjectType, null: false

    def resolve(id:)
      ::Project.find(id)
    end
  end
end

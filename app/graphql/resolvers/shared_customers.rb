module Resolvers
  class SharedCustomers < Resolvers::BaseResolver
    description 'Get all shared customers'

    type Types::DealType.collection_type, null: false

    argument :query, String, required: false
    argument :per_page, Integer, required: false, default_value: 20
    argument :page, Integer, required: false, default_value: 1

    def resolve(query:, page:, per_page:)
      params = {
        'query': query
      }
      deals = ::Deal.search_result(params).where.not(state: 'request_recall').order("deals.created_at DESC")
      current_user = context[:current_resource]
      permission = current_user.get_permission("Deal")
      unless permission
        []
      else
        deals.where(created_by_id: current_user.id).where.not(assignee_id: current_user.id).or(deals.where(created_by_id: current_user.id).where(assignee_id: nil)).page(page).per(per_page)
      end
    end
  end
end

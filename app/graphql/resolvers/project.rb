module Resolvers
  class Project < Resolvers::BaseResolver
    description 'Find a project by ID'

    type Types::ProjectType, null: false

    argument :id, ID, required: true

    def resolve(id:)
      ::Project.find(id)
    end
  end
end

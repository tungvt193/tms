module Resolvers
  class HighProducts < Resolvers::BaseResolver
    description 'Get all high products'

    type Types::HighProductType.collection_type, null: false

    argument :project_id, Int, required: true
    argument :query, String, required: false, default_value: nil
    argument :subdivision_id, String, required: false, default_value: nil
    argument :block_id, String, required: false, default_value: nil

    argument :per_page, Integer, required: false, default_value: 20
    argument :page, Integer, required: false, default_value: 1

    def resolve(project_id:, query:, subdivision_id:, block_id:, page:, per_page:)
      project = ::Project.find_by_id(project_id)
      return [] unless  project.present?

      if query == nil && subdivision_id == nil && block_id == nil
        project.products.where(level: 0).where('state IN (?)', %w(for_sale locking deposited sold)).page(page).per(per_page).order("products.code ASC")
      else
        params = {
          'query': query,
          'subdivision_id': subdivision_id,
          'block_id': block_id
        }
        project.products.search_result(params, level: 0).where('state IN (?)', %w(for_sale locking deposited sold)).page(page).per(per_page).order("products.code ASC")
      end
    end
  end
end

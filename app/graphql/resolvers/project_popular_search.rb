module Resolvers
  class ProjectPopularSearch < Resolvers::BaseResolver
    description 'List project popular search'

    type [Types::ProjectType], null: false

    def resolve
      ::Project.where(most_popular: true).order(name: :asc)
    end
  end
end

module Resolvers
  class Assignee < Resolvers::BaseResolver
    description 'Get assignee by phone_number'

    type Types::UserType, null: true

    argument :phone_number, String, required: true

    def resolve(phone_number:)
      phone_number = Phonelib.parse(phone_number, 'VN').full_e164.sub('+', '')
      ::User.internal.find_by_phone_number(phone_number)
    end
  end
end

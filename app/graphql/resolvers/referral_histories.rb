module Resolvers
  class ReferralHistories < Resolvers::BaseResolver
    description 'Get all referral histories'

    type Types::ReferralHistoryType.collection_type, null: false

    argument :per_page, Integer, required: false, default_value: 20
    argument :page, Integer, required: false, default_value: 1

    def resolve(page:, per_page:)
      current_user = context[:current_resource]
      current_user.referral_histories.order("referral_histories.created_at DESC").page(page).per(per_page)
    end
  end
end

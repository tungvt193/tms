module Resolvers
  class ProductLockHistory < Resolvers::BaseResolver
    description 'Find a product lock history by ID'

    type Types::ProductLockHistoryType, null: false

    argument :id, ID, required: true

    def resolve(id:)
      ::ProductLockHistory.find(id)
    end
  end
end

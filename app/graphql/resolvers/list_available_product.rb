module Resolvers
  class ListAvailableProduct < Resolvers::BaseResolver
    description 'Get all available products of project'

    type [Types::ProductType], null: false

    argument :project_id, Int, required: true

    def resolve(project_id:)
      project = ::Project.find_by_id(project_id)
      return [] unless  project.present?
      project.products.for_sale.order("products.code ASC")
    end
  end
end

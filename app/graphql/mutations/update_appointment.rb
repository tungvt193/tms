module Mutations
  class UpdateAppointment < Mutations::BaseMutation
    argument :id, ID, required: true
    argument :content, String, required: true
    argument :appointment_date, String, required: true
    argument :result, String, required: false

    field :appointment, Types::AppointmentType, null: true
    field :errors, [String], null: false

    def resolve(id:, content:, appointment_date:, result:)
      params = {
        content: content,
        appointment_date: appointment_date,
        result: result
      }
      appointment = ::Appointment.find(id)
      appointment.assign_attributes(params)
      if appointment.save
        {
          appointment: appointment,
          errors: []
        }
      else
        {
          appointment: nil,
          errors: appointment.errors.full_messages
        }
      end
    end
  end
end

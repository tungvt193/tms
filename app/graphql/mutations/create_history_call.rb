module Mutations
  class CreateHistoryCall < Mutations::BaseMutation
    argument :deal_id, Integer, required: true

    field :code, String, null: false
    field :errors, [String], null: true

    def resolve(deal_id:)
      current_user = context[:current_resource]
      call = ::Call.new(
        deal_id: deal_id,
        created_by_id: current_user.id,
        contact_date: ::Time.zone.now
      )
      call.save ? { code: 'success', errors: nil } : { code: 'error', errors: call.errors.full_messages }
    end
  end
end

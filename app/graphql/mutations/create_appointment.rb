module Mutations
  class CreateAppointment < Mutations::BaseMutation
    argument :deal_id, Int, required: true
    argument :content, String, required: true
    argument :appointment_date, String, required: true

    field :appointment, Types::AppointmentType, null: true
    field :errors, [String], null: false

    def resolve(deal_id:, content: ,appointment_date:)
      current_user = context[:current_resource]
      appointment = ::Appointment.new(
        deal_id: deal_id,
        content: content,
        appointment_date: appointment_date,
        created_by_id: current_user.id
      )
      if appointment.save
        {
          appointment: appointment,
          errors: []
        }
      else
        {
          appointment: nil,
          errors: appointment.errors.full_messages
        }
      end
    end
  end
end

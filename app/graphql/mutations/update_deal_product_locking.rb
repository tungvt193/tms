module Mutations
  class UpdateDealProductLocking < Mutations::BaseMutation
    attr_reader :params
    argument :id, ID, required: true
    argument :attribute, Types::AttributeType, required: true

    field :deal, Types::DealType, null: true
    field :errors, GraphQL::Types::JSON, null: true

    def resolve(args)
      @params = ::ActionController::Parameters.new(args)

      deal = ::Deal.find_by(id: args[:id])

      unless deal.present?
        return {
          deal: nil,
          errors: ["Giao dịch không tồn tại."]
        }
      end

      ApplicationRecord.transaction do
        deal.assign_attributes(deal_params)
        if deal.product_lock_history.present?
          deal.product_id = deal.product_lock_history.product_id
          deal.state = 'deposit'
        end
        if deal.save(validate: false )
          deal.product_lock_history.update(state: 'success')
        end
      end

      return {
        deal: deal,
        errors: nil
      }
    end

    private

    def deal_params
      params.require(:attribute).permit(
        ::Deal::ATTRS
      )
    end
  end
end

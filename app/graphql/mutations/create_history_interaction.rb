module Mutations
  class CreateHistoryInteraction < Mutations::BaseMutation
    argument :deal_id, Int, required: true
    argument :interaction_type, Int, required: true
    argument :interaction_content, String, required: true
    argument :interaction_date, String, required: true

    field :history_interaction, Types::HistoryInteractionType, null: true
    field :errors, [String], null: false

    def resolve(deal_id:, interaction_type:, interaction_content: ,interaction_date:)
      current_user = context[:current_resource]
      history_interaction = ::HistoryInteraction.new(
        deal_id: deal_id,
        interaction_type: interaction_type,
        interaction_content: interaction_content,
        interaction_date: interaction_date,
        created_by_id: current_user.id
      )
      if history_interaction.save
        {
          history_interaction: history_interaction,
          errors: []
        }
      else
        {
          history_interaction: nil,
          errors: history_interaction.errors.full_messages
        }
      end
    end
  end
end

module Mutations
  class UpdateDeal < Mutations::BaseMutation
    attr_reader :params
    argument :id, ID, required: true
    argument :attribute, Types::AttributeType, required: true

    field :deal, Types::DealType, null: true
    field :errors, GraphQL::Types::JSON, null: true

    def resolve(args)
      @params = ::ActionController::Parameters.new(args)

      deal = ::Deal.find_by(id: args[:id])

      unless deal.present?
        return {
          deal: nil,
          errors: ["Giao dịch không tồn tại."]
        }
      end

      unless deal_params.present?
        return {
          deal: deal,
          errors: nil
        }
      end

      ApplicationRecord.transaction do
        deal.assign_attributes(deal_params)
        deal_valid = deal.valid?
        customer_persona_valid = deal.customer_persona.valid?
        if deal_valid && customer_persona_valid
          deal.save
          {
            deal: deal,
            errors: nil
          }
        else
          {
            deal: deal,
            errors: {
              deal_attributes: deal.errors.messages,
              customer_persona_attributes: deal.customer_persona.errors.messages
            }
          }
        end
      end
    end

    private

    def deal_params
      params.require(:attribute).permit(
        ::Deal::ATTRS
      )
    end
  end
end

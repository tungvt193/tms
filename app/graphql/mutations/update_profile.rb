module Mutations
  class UpdateProfile < Mutations::BaseMutation
    argument :email, String, required: false
    argument :full_name, String, required: false
    argument :phone_number, String, required: false
    argument :gender, String, required: false
    argument :city_id, Integer, required: false
    argument :main_job, Integer, required: false

    field :user, Types::UserType, null: true
    field :errors, [String], null: false

    def resolve(full_name:, phone_number:, gender:, city_id:, main_job:, email:)
      phone_number = Phonelib.parse(phone_number, 'VN').full_e164.sub('+', '') if phone_number.present?
      params = {
        full_name: full_name,
        phone_number: phone_number,
        gender: gender,
        city_id: city_id,
        main_job: main_job,
        email: email,
        is_me_page: true
      }

      params.except!(:email) if context[:current_resource].internal?
      params.except!(:phone_number) if context[:current_resource].agent?

      if context[:current_resource].update(params)
        {
          user: context[:current_resource],
          errors: []
        }
      else
        {
          user: nil,
          errors: context[:current_resource].errors.messages.values.flatten.map{ |m| m == 'không phải là email' ? 'Email không hợp lệ' : m }
        }
      end
    end
  end
end

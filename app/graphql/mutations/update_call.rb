module Mutations
  class UpdateCall < Mutations::BaseMutation
    argument :id, ID, required: true
    argument :result, String, required: true

    field :code, String, null: false
    field :errors, [String], null: true

    def resolve(id:, result:)
      params = {
        result: result
      }
      call = ::Call.find(id)
      call.assign_attributes(params)
      if call.save
        {
          code: 'success',
          errors: []
        }
      else
        {
          code: 'error',
          errors: call.errors.full_messages
        }
      end
    end
  end
end

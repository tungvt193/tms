module Mutations
  class CreateProductLockHistoryEnqueued < Mutations::BaseMutation
    argument :product_id, Int, required: true

    field :product_lock_history, Types::ProductLockHistoryType, null: true
    field :code, String, null: false
    field :errors, [String], null: false

    def resolve(product_id:)
      current_user = context[:current_resource]
      unless Pundit.policy(current_user, ProductLockHistory.new).create?
        return {
          success: false,
          code: '403',
          errors: ['Bạn không có quyền thực hiện hành động này.']
        }
      end
      product = ::Product.find(product_id)
      lock_history_locking = product.product_lock_histories.detect{ |p| %w(locking deposit_confirmation waiting_reti_acceptance pending).include?(p.state) }
      lock_history = ::ProductLockHistory.find_or_initialize_by(
        state: 'enqueued',
        project_id: product.project_id,
        product_id: product.id,
        user_id: current_user.id
      )

      if product.state == 'locking' && lock_history_locking.present?
        lock_history.save
        ::ProductLockHistoryJob.set(wait_until: (lock_history_locking.deadline_for_sale - 5.minutes)).perform_later('enqueued', product, lock_history, current_user)
        {
          product_lock_history: lock_history,
          code: '200',
          errors: []
        }
      elsif product.state == 'for_sale'
        lock_history.save
        {
          code: '200',
          product_lock_history: lock_history,
          errors: []
        }
      else
        {
          code: '422',
          errors: ['Đã có lỗi xảy ra. Vui lòng thử lại!']
        }
      end
    end
  end
end

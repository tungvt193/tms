module Mutations
  class CreateDeal < Mutations::BaseMutation
    argument :project_id, Integer, required: true
    argument :customer_id, Integer, required: true
    argument :source, Integer, required: true
    argument :suggestive_name, String, required: false, default_value: nil
    argument :product_lock_history_id, Integer, required: false, default_value: nil
    argument :gender, Int, required: false, default_value: nil
    argument :email, String, required: false, default_value: nil
    argument :purchase_purpose, Int, required: false, default_value: nil
    argument :product_type, Int, required: false, default_value: nil

    field :deal, Types::DealType, null: true
    field :errors, [String], null: false

    def resolve(project_id:, customer_id:, source:, suggestive_name:, product_lock_history_id:, gender:, email:, purchase_purpose:, product_type:)
      current_user = context[:current_resource]
      customer = ::Customer.find(customer_id)
      check_phone_number = customer && customer.phone_number.blank?
      deal = ::Deal.new(
        project_id: project_id,
        customer_id: customer_id,
        assignee_id: current_user.id,
        created_by_id: current_user.id,
        source: source,
        suggestive_name: suggestive_name,
        labels: check_phone_number == true ? ["Chưa có số điện thoại"] : [],
        assigned_at: ::Time.now,
        state_changed_at: ::Time.now,
        cached_at: ::Time.now,
        create_deal_date: ::Time.now,
        from_app: true,
        product_lock_history_id: product_lock_history_id,
        purchase_purpose: purchase_purpose,
        product_type: product_type
      )
      if gender.present? && email.present?
        deal.build_customer_persona(gender: gender, email: email)
        deal.state = 'deposit'
        deal.source = ::Constant::SOURCES[:company][:offline][:external_agent]
      end
      if deal.save(validate: false)
        deal.product_lock_history.update(state: 'success') if deal.product_lock_history.present? && product_lock_history_id.present?
        {
          deal: deal,
          errors: []
        }
      else
        {
          deal: nil,
          errors: deal.errors.full_messages
        }
      end
    end
  end
end

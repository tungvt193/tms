module Mutations
  class UpdateProductLockHistory < Mutations::BaseMutation
    argument :lock_history_id, Int, required: true
    argument :state, String, required: true
    argument :unc_image, String, required: false, default_value: nil

    field :product_lock_history, Types::ProductLockHistoryType, null: true
    field :code, String, null: false
    field :errors, [String], null: false

    def resolve(lock_history_id:, state:, unc_image:)
      current_user = context[:current_resource]
      lock_history = ::ProductLockHistory.find_by!(id: lock_history_id, user_id: current_user.id)
      unless Pundit.policy(current_user, lock_history).update?
        return {
          success: false,
          code: '403',
          errors: ['Bạn không có quyền thực hiện hành động này.']
        }
      end

      if lock_history.update_state!(state, unc_image)
        {
          product_lock_history: lock_history,
          code: '200',
          errors: []
        }
      else
        {
          product_lock_history: lock_history,
          code: '422',
          errors: ['Cập nhật không thành công']
        }
      end
    end
  end
end

module Mutations
  class UpdateHistoryInteraction < Mutations::BaseMutation
    argument :id, ID, required: true
    argument :interaction_type, Int, required: true
    argument :interaction_content, String, required: true
    argument :interaction_date, String, required: true

    field :history_interaction, Types::HistoryInteractionType, null: true
    field :errors, [String], null: false

    def resolve(id:, interaction_type:, interaction_content: ,interaction_date:)
      params = {
        interaction_type: interaction_type,
        interaction_content: interaction_content,
        interaction_date: interaction_date
      }
      history_interaction = ::HistoryInteraction.find(id)
      history_interaction.assign_attributes(params)
      if history_interaction.save
        {
          history_interaction: history_interaction,
          errors: []
        }
      else
        {
          history_interaction: nil,
          errors: history_interaction.errors.full_messages
        }
      end
    end
  end
end

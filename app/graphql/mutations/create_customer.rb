module Mutations
  class CreateCustomer < Mutations::BaseMutation
    argument :name, String, required: true
    argument :phone_number, String, required: true

    field :customer, Types::CustomerType, null: true
    field :errors, [String], null: false
    field :same_customers, [Types::CustomerType], null: true

    def resolve(name:, phone_number:)
      phone_number = Phonelib.parse(phone_number, 'VN').full_e164.sub('+', '')
      customer = ::Customer.new(
        name: name,
        phone_number: phone_number,
        country_code: 'VN',
        quick_create_from_app: true,
        created_by_id: context[:current_resource].id
      )
      customer_same = CustomerPolicy::Scope.new(customer.user, Customer).resolve.where.not(id: customer.id).where(phone_number: customer.phone_number)
      if customer_same.present?
        return {
          customer: nil,
          errors: ['Số điện thoại này đang bị trùng'],
          same_customers: customer_same
        }
      end
      if customer.save
        {
          customer: customer,
          errors: [],
          same_customers: []
        }
      else
        {
          customer: nil,
          errors: customer.errors.full_messages,
          same_customers: []
        }
      end
    end
  end
end

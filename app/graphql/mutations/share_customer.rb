module Mutations
  class ShareCustomer < Mutations::BaseMutation
    argument :project_id, Integer, required: true
    argument :customer_name, String, required: true
    argument :customer_phone, String, required: true
    argument :assignee_id, Integer, required: false

    field :deal, Types::DealType, null: true
    field :errors, [String], null: false

    def resolve(project_id:, customer_name:, customer_phone:, assignee_id:)
      current_user = context[:current_resource]
      ActiveRecord::Base.transaction do
        phone_number = Phonelib.parse(customer_phone, 'VN').full_e164.sub('+', '')
        customer = ::Customer.create(
          name: customer_name,
          phone_number: phone_number,
          country_code: 'VN',
          created_by_id: current_user.id,
          data_source: ::Constant::SOURCES[:company][:offline][:external_agent]
        )

        deal = customer.deals.build(
          project_id: project_id,
          customer_id: customer.id,
          assignee_id: assignee_id,
          created_by_id: current_user.id,
          assigned_at: assignee_id.present? ? ::Time.now : nil,
          state_changed_at: ::Time.now,
          cached_at: ::Time.now,
          create_deal_date: ::Time.now,
          from_app: true,
          source: ::Constant::SOURCES[:company][:offline][:external_agent]
        )

        deal.deal_journeys.build(
          create_date: DateTime.now,
          content: "Tạo thành công giao dịch chia sẻ"
        )

        if customer.save
          if deal.assignee_id.present?
            ::NotificationService.deal_from_share_customer(deal, deal.author)
          else
            ::NotificationService.assign_deal_from_share_customer(deal, deal.author)
          end

          {
            deal: deal,
            errors: []
          }
        else
          {
            deal: nil,
            errors: [deal.errors.full_messages]
          }
        end
      rescue
        {
          deal: nil,
          errors: ["Không thể tạo"]
        }
      end
    end
  end
end

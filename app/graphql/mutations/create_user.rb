module Mutations
  class CreateUser < Mutations::BaseMutation
    argument :email, String, required: true
    argument :password, String, required: true
    argument :password_confirmation, String, required: true
    argument :full_name, String, required: true
    argument :account_type, Integer, required: true
    argument :role_id, Integer, required: true

    field :user, Types::UserType, null: true
    field :errors, [String], null: false

    def resolve(email:, password:, password_confirmation:, full_name:, account_type:, role_id:)
      user = ::User.new(
        email: email,
        password: password,
        password_confirmation: password_confirmation,
        full_name: full_name,
        account_type: account_type,
        role_id: role_id
      )
      if user.save
        {
          user: user,
          errors: []
        }
      else
        {
          user: nil,
          errors: user.errors.full_messages
        }
      end
    end
  end
end

module Mutations
  class ReadAllNotification< Mutations::BaseMutation
    field :message, String, null: false
    def resolve
      ActiveRecord::Base.transaction do
        current_user = context[:current_resource]
        ::Notification.where(user_id: current_user).update_all(read_at: ::Time.now)
        {
          message: "Cập nhật thông báo thành công"
        }
      rescue
        {
          message: "Không thể cập nhật thông báo"
        }
      end
    end
  end
end

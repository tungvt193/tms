module Mutations
  class RemoveHistoryInteraction < Mutations::BaseMutation
    argument :id, ID, required: true
    argument :deal_id, Int, required: true

    field :message, String, null: false
    field :code, String, null: false

    def resolve(id:, deal_id:)
      current_user = context[:current_resource]
      history_interaction = ::HistoryInteraction.where(id: id, created_by_id: current_user.id, deal_id: deal_id)
      if history_interaction&.first&.destroy
        {
          code: 'success',
          message: 'Xoá lịch sử tương tác thành công.'
        }
      else
        {
          code: 'error',
          message: 'Xoá lịch sử tương tác không thành công.'
        }
      end
    end
  end
end

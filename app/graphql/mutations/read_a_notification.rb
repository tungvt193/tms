module Mutations
  class ReadANotification< Mutations::BaseMutation

    argument :id, ID, required: true

    field :message, String, null: false

    def resolve(id:)
      ActiveRecord::Base.transaction do
        notification = ::Notification.find_by(id: id)
        notification.update_attributes(read_at: ::Time.now)
        {
          message: "Cập nhật thông báo thành công"
        }
      rescue
        {
          message: "Không thể cập nhật thông báo"
        }
      end
    end
  end
end

module Mutations
  class CreateProductLockHistory < Mutations::BaseMutation
    argument :product_id, Int, required: true

    field :product_lock_history, Types::ProductLockHistoryType, null: true
    field :errors, [String], null: false
    field :code, String, null: false
    field :success, Boolean, null: false

    def resolve(product_id:)
      current_user = context[:current_resource]
      unless Pundit.policy(current_user, ProductLockHistory.new).create?
        return {
          success: false,
          code: '403',
          errors: ['Bạn không có quyền thực hiện hành động này.']
        }
      end
      product = ::Product.find(product_id)
      project = ::Project.find(product.project_id)
      campaign_lock = project.campaign_locks.find_by("products && ?", "{#{product.id}}")
      products_lock = ::ProductLockHistory.where(user_id: current_user.id, state: 'locking', project_id: project.id).select{ |plh| plh.product.tag == 'Hàng HOT' && campaign_lock.products.include?(plh.product.id) }
      lock_history = ::ProductLockHistory.find_or_initialize_by(
        project_id: product.project_id,
        product_id: product.id,
        user_id: current_user.id,
        state: :enqueued
      )
      lock_history.assign_attributes(
        deadline_for_sale: Time.zone.now.advance(minutes: campaign_lock.unc_confirmation_period_for_sale),
        state: :locking
      )

      if product.tag == 'Hàng HOT' && campaign_lock.enable_locked_for_hot_product == products_lock.size
        return {
          success: false,
          code: '422',
          errors: ['Bạn đã lock quá lượt lock quy định cho dự án']
        }
      end

      if product.can_lock? && lock_history.save
        {
          product_lock_history: lock_history,
          success: true,
          code: '200',
          errors: []
        }
      else
        lock_history_enqueued = ::ProductLockHistory.find_by(product_id: product.id, user_id: current_user.id, state: :enqueued)
        ::LockingHistoryLog.create(project_id: project.id,product_id: product.id, user_id: current_user.id)
        {
          product_lock_history: lock_history_enqueued.present? ? lock_history_enqueued : ::ProductLockHistory.locking_process.find_by(product_id: product.id),
          success: false,
          code: '422',
          errors: ['Lock căn không thành công']
        }
      end
    end
  end
end

module ProjectSearch
  extend ActiveSupport::Concern

  REFERENCE_TABLES = [:region]
  SEARCH_FIELDS = [:name, :full_address, :investor, :city, :district, :ward, :street]
  FILTER_FIELDS = [:real_estate_type, :city, :features, :labels]

  included do
    after_commit :reindex_product
    searchkick word_middle: SEARCH_FIELDS,
               suggest: SEARCH_FIELDS,
               filterable: FILTER_FIELDS,
               searchable: SEARCH_FIELDS
    scope :search_import, -> { includes(REFERENCE_TABLES) }

    def self.search_result(str_query)
      search_results = search str_query,
                              fields: [:name],
                              match: :word_middle,
                              misspellings: { below: 0 }

      by_ids(search_results.pluck(:id)).order('labels @> ARRAY[[0]]::integer[] DESC NULLS LAST, projects.updated_at DESC')
    end

    def self.autocomplete_result(str_query)
      search(str_query,
             fields: [:name],
             match: :word_middle,
             misspellings: { below: 0 },
             suggest: true).map { |p| { id: p.id, value: p.autocomplete_value } }
    end

    def self.select2_search_result(str_query)
      search(str_query,
             fields: [:name, :full_address],
             match: :word_middle,
             misspellings: { below: 0 }).map { |p| { id: p.id, text: p.name } }
    end

    def self.agent_app_search_result(params)
      str_query = params[:query].presence || "*"
      condition = {}
      condition.merge!({ real_estate_type: params[:real_estate_type] }) if params[:real_estate_type].present?
      condition.merge!({ labels: params[:features] }) if params[:features].present?
      condition.merge!({ city: params[:city] }) if params[:city].present?
      search_results = search str_query,
                              fields: [:name],
                              where: condition,
                              match: :word_middle,
                              misspellings: { below: 0 }
      by_ids(search_results.pluck(:id)).order('labels @> ARRAY[[0]]::integer[] DESC NULLS LAST, projects.updated_at DESC')
    end

    def self.agent_app_autocomplete_result(str_query)
      search(str_query,
             fields: [:name],
             match: :word_middle,
             misspellings: { below: 0 })
    end

    def autocomplete_value
      name
    end

    def should_index?
      true
    end

    def reindex_product
      reindex
    end
  end

  def search_data
    {
      name: name,
      full_address: full_address,
      real_estate_type: real_estate_type,
      min_price: min_price_filter,
      max_price: max_price_filter,
      min_acreage: min_acreage_filter,
      max_acreage: max_acreage_filter,
      furnitures: furnitures,
      features: features,
      online_transaction: online_transaction,
      investors: investors,
      investor: Investor.where(id: investors).pluck(:name).join(', '),
      city: region&.city_id,
      district: region&.district_id,
      ward: region&.ward_id,
      street: region&.street,
      labels: labels
    }
  end
end

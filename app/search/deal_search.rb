module DealSearch
  extend ActiveSupport::Concern

  REFERENCE_TABLES = []
  SEARCH_FIELDS = [:name, :customer_name, :project_name, :customer_phone, :state, :source, :project, :assignee, :label, :leader_id, :created_at, :updated_at, :purchase_purpose, :product_type, :door_direction, :balcony_direction, :name_phone_project, :phone_with_zero, :code]
  FILTER_FIELDS = [:name, :customer_name, :project_name, :customer_phone, :state, :source, :project, :assignee, :label, :leader_id, :created_at, :updated_at, :purchase_purpose, :product_type, :door_direction, :balcony_direction, :name_phone_project, :phone_with_zero, :code]

  included do
    after_commit :reindex_deal
    searchkick word: SEARCH_FIELDS,
               suggest: SEARCH_FIELDS,
               filterable: FILTER_FIELDS,
               searchable: SEARCH_FIELDS
    scope :search_import, -> { includes(REFERENCE_TABLES) }

    def self.search_result(params)
      if params[:query]
        params[:query] = params[:query].strip
      end
      fields = case params[:search_type]
               when 'name'
                 [:name]
               when 'customer_name'
                 [:customer_name]
               when 'customer_phone'
                 [:customer_phone, :phone_with_zero]
               when 'code'
                 [:code]
               when 'project_name'
                 [:project_name]
               when 'api_search'
                 [:name, :customer_phone, :phone_with_zero]
               else
                 if params['browser'].present? && params['browser'] == 'mobile'
                   [:name]
                 else
                   [:name, :customer_name, :customer_phone, :phone_with_zero, :code, :project_name]
                 end
               end
      condition = {}
      condition.merge!({ state: params[:state] }) if params[:state].present?
      condition.merge!({ source: params[:source] }) if params[:source].present?
      condition.merge!({ project: params[:project] }) if params[:project].present?
      condition.merge!({ assignee: params[:assignee].map { |u| u unless u.to_i == 0 } }) if params[:assignee].present?
      condition.merge!({ purchase_purpose: params[:purchase_purpose] }) if params[:purchase_purpose].present?
      condition.merge!({ product_type: params[:product_type] }) if params[:product_type].present?
      condition.merge!({ door_direction: params[:door_direction] }) if params[:door_direction].present?
      condition.merge!({ balcony_direction: params[:balcony_direction] }) if params[:balcony_direction].present?
      condition.merge!({ calculation_type: params[:calculation_type] }) if params[:calculation_type].present?

      if params[:created_at].present?
        sdate, edate = params[:created_at].split(' - ').map { |d| Deal.convert_format d }
        condition.merge!({ created_at: { gte: sdate, lte: edate } })
      end
      if params[:updated_at].present?
        sdate, edate = params[:updated_at].split(' - ').map { |d| Deal.convert_format d }
        condition.merge!({ updated_at: { gte: sdate, lte: edate } })
      end
      if params[:contract_signed_at].present?
        sdate, edate = params[:contract_signed_at].split(' - ').map { |d| Deal.convert_format d }
        condition.merge!({ contract_signed_at: { gte: sdate, lte: edate } })
      end
      if params[:label].present?
        condition.merge!({ label: params[:label] })
      end
      condition.merge!({ leader_id: params[:group] }) if params[:group].present?
      str_query = params[:query].present? ? params[:query] : '*'

      search_results = search str_query,
                              fields: fields,
                              where: condition,
                              match: :word,
                              misspellings: { below: 0 }

      if params[:project].present? && params[:project].include?('0')
        external_projects = where(is_external_project: true)
        return by_ids(search_results.pluck(:id) + external_projects.pluck(:id))
      end
      return by_ids(search_results.pluck(:id))
    end

    def self.autocomplete_result(str_query, type, state)
      condition = {}
      condition.merge!({ state: state }) if state != 'undefined'
      fields = case type
               when 'name'
                 [:name]
               when 'customer_name'
                 [:customer_name]
               when 'customer_phone'
                 [:customer_phone, :phone_with_zero]
               when 'code'
                 [:code]
               when 'project_name'
                 [:project_name]
               end
      search(str_query,
             fields: fields,
             where: condition,
             match: :word,
             misspellings: { below: 0 },
             suggest: true).map { |p| { value: p.autocomplete_value(type), type: type, path: "/deals/#{p.id}/edit" } }.uniq
    end

    def self.deal_form_autocomplete_result(str_query)
      fields = [:customer_phone, :project_name, :customer_name, :phone_with_zero]
      search(str_query,
             fields: fields,
             match: :word,
             misspellings: { below: 0 },
             suggest: true).map { |p| { value: p.deal_form_autocomplete_value, path: "/deal_forms/#{p.id}/edit" } }
    end

    def self.agent_app_autocomplete_result(str_query, assignee_id)
      fields = [:customer_name, :customer_phone, :phone_with_zero]
      search(str_query,
             fields: fields,
             where: { assignee: assignee_id },
             match: :word,
             misspellings: { below: 0 })
    end

    def autocomplete_value(type)
      case type
      when 'name'
        name
      when 'customer_name'
        customer&.name
      when 'customer_phone'
        Phonelib.parse(customer&.phone_number).national(false) if customer&.phone_number.present?
      when 'code'
        product&.code
      when 'project_name'
        project&.name
      end
    end

    def deal_form_autocomplete_value
      "#{customer&.name} #{customer&.phone_number.present? ? ('- ' + customer&.phone_number) : ''} #{project&.name.present? ? ('- ' + project&.name) : ''}"
    end

    def should_index?
      true
    end

    def reindex_deal
      self.reindex
    end

    def self.convert_format(string, from_format = "%d/%m/%Y", to_format = "%Y-%m-%d")
      return '' if string.blank?
      DateTime.strptime(string, from_format).to_time.strftime to_format
    end

    def self.search_same_deals(params)
      fields = [:name, :project_name, :customer_phone, :phone_with_zero, :name_phone_project]
      str_query = params[:query].present? ? params[:query] : '*'
      search_results = search str_query,
                              fields: fields,
                              match: :word,
                              misspellings: { below: 0 }
      return by_ids(search_results.pluck(:id))
    end

    def self.autocomplete_same_deals(str_query)
      fields = [:name, :project_name, :customer_phone, :phone_with_zero, :name_phone_project]
      search(str_query,
             fields: fields,
             match: :word,
             misspellings: { below: 0 },
             suggest: true).map { |p| { value: p.autocomplete_value_same_deals } }
    end

    def autocomplete_value_same_deals
      "#{name} #{customer&.phone_number.present? ? ('- ' + customer&.phone_number) : ''} #{project.present? ? ('- ' + project&.name) : ''}"
    end

    def phone_with_zero
      Phonelib.parse(customer&.phone_number).national(false) if customer&.phone_number.present?
    end
  end

  def search_data
    {
      name: name,
      customer_name: customer&.name,
      customer_phone: customer&.phone_number,
      state: state,
      source: source,
      project: project_id,
      project_name: project&.name,
      assignee: assignee_id,
      label: labels,
      leader_id: assignee&.leader&.id,
      created_at: create_deal_date&.to_date,
      updated_at: updated_at.to_date,
      purchase_purpose: purchase_purpose,
      product_type: product_type,
      door_direction: door_direction&.compact || [],
      balcony_direction: balcony_direction&.compact || [],
      name_phone_project: autocomplete_value_same_deals,
      phone_with_zero: phone_with_zero,
      calculation_type: commission&.calculation_type,
      contract_signed_at: contract_signed_at&.to_date,
      code: product&.code
    }
  end
end

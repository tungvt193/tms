module RegulationSearch
  extend ActiveSupport::Concern

  REFERENCE_TABLES = []
  SEARCH_FIELDS = [:name, :state, :real_estate_type, :project, :start_date, :end_date]

  included do
    after_commit :reindex_regulation
    searchkick word_middle: SEARCH_FIELDS, suggest: SEARCH_FIELDS
    scope :search_import, -> { includes(REFERENCE_TABLES) }

    def self.search_result str_query, type
      fields = case type
      when 'name'
        [:name]
      when 'project'
        [:project]
      when 'state'
        [:state]
      when 'real_estate'
        [:real_estate_type]
      else
        SEARCH_FIELDS
      end

      search_results = search str_query,
        fields: fields,
        match: :word_middle,
        misspellings: {below: 0}

      by_ids(search_results.pluck(:id)).order(name: :asc)
    end

    def self.autocomplete_result str_query, type
      fields = case type
      when 'name'
        [:name]
      when 'project'
        [:project]
      when 'state'
        [:state]
      end
      search(str_query,
        fields: fields,
        match: :word_middle,
        misspellings: {below: 0},
        suggest: true).map{|p| {value: p.autocomplete_value(type), type: type}}.uniq
    end

    def autocomplete_value type
      case type
      when 'name'
        name
      when 'project'
        project&.name
      when 'state'
        I18n.t("regulation.states.#{state}")
      end
    end

    def reindex_regulation
      reindex
    end

    def self.str_included? str, search_str
      I18n.transliterate(str).downcase.include? I18n.transliterate(search_str).downcase
    end
  end

  def search_data
    {
      name: name,
      project: project&.name,
      real_estate_type: real_estate_type.map { |e| Constant::PROJECT_REAL_ESTATE_TYPE[e] }.join(', '),
      start_date: start_date.localtime.strftime(Settings.date.formats.strftime_regulation),
      end_date: end_date.localtime.strftime(Settings.date.formats.strftime_regulation),
      state: I18n.t("regulation.states.#{state}")
    }
  end
end

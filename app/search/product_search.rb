module ProductSearch
  extend ActiveSupport::Concern

  REFERENCE_TABLES = []
  SEARCH_FIELDS = [:code]

  included do
    after_commit :reindex_product
    searchkick word_middle: SEARCH_FIELDS, suggest: SEARCH_FIELDS
    scope :search_import, -> { includes(REFERENCE_TABLES) }

    def self.search_result(params, level: 0)
      condition = { level: level }
      condition.merge!({ subdivision_id: params[:subdivision_id] }) if params[:subdivision_id].present?
      condition.merge!({ block_id: params[:block_id] }) if params[:block_id].present?
      condition.merge!({ floor_id: params[:floor_id] }) if params[:floor_id].present?
      condition.merge!({ name: params[:name] }) if params[:name].present?
      condition.merge!({ product_type: params[:product_type] }) if params[:product_type].present?
      condition.merge!({ real_estate_type: params[:real_estate_type] }) if params[:real_estate_type].present?
      condition.merge!({ state: params[:state] }) if params[:state].present?
      search_results = search params[:query].present? ? params[:query] : '*',
                              fields: SEARCH_FIELDS,
                              match: :word_middle,
                              misspellings: { below: 0 },
                              where: condition

      by_ids(search_results.pluck(:id)).order(code: :asc)
    end

    def self.autocomplete_result(str_query, level: 0)
      search(str_query,
             fields: SEARCH_FIELDS,
             match: :word_middle,
             where: { level: level },
             misspellings: { below: 0 },
             suggest: true).map { |p| { value: p.autocomplete_value } }
    end

    def self.autocomplete_result_graphql(str_query, level)
      autocomplete_result = search(str_query,
                                  fields: SEARCH_FIELDS,
                                  match: :word_middle,
                                  where: { level: level },
                                  misspellings: { below: 0 },
                                  suggest: true)

      by_ids(autocomplete_result.pluck(:id))
    end

    def self.select2_search_result(str_query, is_sale_admin)
      if is_sale_admin == true
        search(str_query,
               fields: SEARCH_FIELDS,
               match: :word_middle,
               misspellings: { below: 0 }).map { |p| { id: p.id, text: p.code } }
      else
        search(str_query,
               fields: SEARCH_FIELDS,
               match: :word_middle,
               where: {
                 state: 'for_sale'
               },
               misspellings: { below: 0 }).map { |p| { id: p.id, text: p.code } }
      end
    end

    def autocomplete_value
      code
    end

    def reindex_product
      reindex
    end
  end

  def search_data
    {
      code: code,
      level: level,
      name: name,
      state: state,
      product_type: product_type,
      real_estate_type: real_estate_type,
      subdivision_id: subdivision_id,
      block_id: block_id,
      floor_id: floor_id
    }
  end
end

module CustomerTicketSearch
  extend ActiveSupport::Concern

  REFERENCE_TABLES = []
  SEARCH_FIELDS = [:name, :phone, :p_name, :name_phone_p_name]

  included do
    scope :by_ids, -> (ids){ where id: ids }
    after_commit :reindex_model
    searchkick word_middle: SEARCH_FIELDS, suggest: SEARCH_FIELDS
    scope :search_import, -> { includes(REFERENCE_TABLES) }

    def self.search_result str_query
      search_results = search str_query,
        fields: SEARCH_FIELDS,
        match: :word_middle,
        misspellings: {below: 0}
      by_ids(search_results.pluck(:id))
    end

    def self.autocomplete_result str_query
      search(str_query,
        fields: SEARCH_FIELDS,
        match: :word_middle,
        misspellings: {below: 0},
        suggest: true).map{|p| {value: p.autocomplete_value}}
    end

    def autocomplete_value
      "#{name} - #{phone} #{get_objectable_name.present? ? ('- ' + get_objectable_name.to_s) : ''}"
    end

    def should_index?
      true
    end

    def reindex_model
      reindex
    end
  end

  def search_data
    {
      name: name,
      phone: phone,
      p_name: get_objectable_name,
      name_phone_p_name: autocomplete_value,
    }
  end
end

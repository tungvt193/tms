module UserSearch
  extend ActiveSupport::Concern
  REFERENCE_TABLES = []
  SEARCH_FIELDS = [:full_name, :email, :phone_number, :phone_with_zero]

  included do
    after_commit :reindex_user
    searchkick word_middle: SEARCH_FIELDS,
               suggest: SEARCH_FIELDS,
               searchable: SEARCH_FIELDS
    scope :search_import, -> { includes(REFERENCE_TABLES) }

    def self.search_result(str_query)
      str_query = str_query.split(' - ')
      search_name = search str_query[0],
                           fields: [:full_name, :email, :phone_number, :phone_with_zero],
                           match: :word,
                           misspellings: { below: 0 }

      search_email = search str_query[1],
                            fields: [:full_name, :email, :phone_number, :phone_with_zero],
                            match: :word,
                            misspellings: { below: 0 }

      search_phone_number = search str_query[2],
                                   fields: [:full_name, :email, :phone_number, :phone_with_zero],
                                   match: :word,
                                   misspellings: { below: 0 }
      if search_phone_number.present?
        records = by_ids((search_name.pluck(:id) & search_email.pluck(:id) & search_phone_number.pluck(:id)).uniq)
      elsif search_phone_number.blank? && search_email.present?
        records = by_ids((search_name.pluck(:id) & search_email.pluck(:id)).uniq)
      else
        records = by_ids((search_name.pluck(:id)).uniq)
      end
      records.order(email: :asc)
    end

    def self.autocomplete_result(str_query, ancestry = false)
      condition_clause = ancestry ? { group_id: nil } : {}
      search(str_query,
             fields: [:email],
             where: condition_clause,
             match: :word_middle,
             misspellings: { below: 0 },
             suggest: true).map { |p| { value: p.autocomplete_value } }
    end

    # Search for Account
    def self.account_autocomplete_result(str_query)
      str_query = str_query.split(' - ')
      search_name = search(str_query[0],
                           fields: [:full_name, :email, :phone_number, :phone_with_zero],
                           match: :word_middle,
                           misspellings: { below: 0 },
                           suggest: true).map { |p| { value: p.autocomplete_value } }

      search_email = search(str_query[1],
                            fields: [:full_name, :email, :phone_number, :phone_with_zero],
                            match: :word_middle,
                            misspellings: { below: 0 },
                            suggest: true).map { |p| { value: p.autocomplete_value } }

      search_phone_number = search str_query[2],
                                   fields: [:full_name, :email, :phone_number, :phone_with_zero],
                                   match: :word_middle,
                                   misspellings: { below: 0 }
      (search_name + search_email + search_phone_number).uniq
    end

    def self.assignee_search_result(str_query, avatar: true)
      str_query = '*' if str_query.blank?
      user_ids = if User.current.super_admin? || User.current.director? || User.current.marketing? || User.current.sale_admin? || User.current.customer_service?
                   User.all.pluck(:id)
                 else
                   User.current.subtree_ids
                 end
      result = search(str_query,
                      fields: [:full_name, :email],
                      where: {
                        id: user_ids,
                      },
                      match: :word_middle,
                      misspellings: { below: 0 },
                      order: { full_name: :asc })
      if avatar
        result.map { |p| { id: p.id, text: p.full_name, avatar: p.get_avatar, email: p.email, deactivated: p.deactivated } }.reject { |user| user[:deactivated] }.unshift({:id => nil, :text => "Chưa phân công", avatar: ActionController::Base.helpers.image_path("user_default.png"), :email => "", deactivated: false})
      else
        result.map { |p| { id: p.id, text: p.full_name } }.reject { |user| user[:deactivated] }.unshift({:id => nil, :text => "Chưa phân công"})
      end
    end

    def autocomplete_value
      value = ""
      value += full_name if full_name.present?
      value += " - #{email}" if email.present?
      value += " - #{phone_with_zero}" if phone_with_zero.present?
      value
    end

    def should_index?
      true
    end

    def reindex_user
      reindex
    end

    def phone_with_zero
      Phonelib.parse(self.phone_number).national.gsub(/\s+/, "") if self.phone_number.present?
    end
  end

  def search_data
    {
      full_name: full_name,
      email: email,
      account_type: account_type,
      is_superadmin: is_superadmin,
      phone_number: phone_number,
      phone_with_zero: phone_with_zero
    }
  end
end

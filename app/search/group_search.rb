module GroupSearch
  extend ActiveSupport::Concern

  REFERENCE_TABLES = []
  SEARCH_FIELDS = [:name, :member_name]
  FILTER_FIELDS = [:city_ids, :director_id, :supervisor_id]

  included do
    after_commit :reindex_group
    searchkick text_middle: SEARCH_FIELDS,
               suggest: SEARCH_FIELDS,
               filterable: FILTER_FIELDS,
               searchable: SEARCH_FIELDS
    scope :search_import, -> { includes(REFERENCE_TABLES) }

    def reindex_group
      self.reindex
    end

    def self.search_result(params)
      str_query = params[:query].present? ? params[:query].strip : '*'
      fields = [:name, :member_name]
      condition = {}
      condition.merge!({ city_ids: params[:cities] }) if params[:cities].present?
      condition.merge!({ director_id: params[:directors] }) if params[:directors].present?
      search_results = search str_query,
                              fields: fields,
                              where: condition,
                              match: :text_middle,
                              misspellings: { below: 0 }
      return by_ids(search_results.pluck(:id))
    end

    def self.autocomplete_result(str_query)
      fields = [:name, :member_name]
      search(str_query,
             fields: fields,
             match: :text_middle,
             misspellings: { below: 0 },
             suggest: true).map{|p| {value: p.name}}
    end

    def member_name
      member_ids = user_ids.concat([director_id, supervisor_id])
      User.where(id: member_ids).pluck(:full_name)
    end
  end

  def search_data
    {
      name: name,
      member_name: member_name,
      city_ids: city_ids,
      director_id: director_id,
      supervisor_id: supervisor_id
    }
  end
end

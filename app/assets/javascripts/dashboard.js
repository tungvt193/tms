$(document).ready(function () {
  $('.list-project-news').css('max-height', $('.kpi-block').height() - 46 + 'px');
})

$(document).on('click', '.dashboard-sale-admin-item', function () {
  window.open($(this).find('.d-none').attr('href'));
})

$(document).on('turbolinks:load', function () {
  $('[data-countdown]').each(function () {
    var $this = $(this),
      finalDate = $(this).data('countdown'),
      currentText = $this.text(),
      itemId = $(this).data('id'),
      lockItem = $this.parents(`#${itemId}`)

    $this.countdown(finalDate, function (event) {
      $this.html(currentText + " " + event.strftime('%M:%S') + "s");
    }).on('finish.countdown', function (event) {
      if (lockItem && lockItem.attr('class') === 'dashboard-sale-admin-lock-item') {
        $this.countdown('stop')
        lockItem.empty()

        var countLockItems = $('[data-countdown]').length
        $('.count-locks').text('Lượt lock hiện tại ' + `(${countLockItems})`)
        if (countLockItems === 0) {
          var $currentLock = $('.dashboard-sale-admin-current-lock')
          $currentLock.addClass('empty')
          $currentLock.text('Chưa có lượt lock nào!')
        }
      } else if (lockItem && lockItem.attr('class') === 'row-lock-item') {
        $this.countdown('stop')
        $this.text('')

        var state = $this.data('state')
        var $labelItem = lockItem.find('.label-campaign-lock')
        if (state === 'deposit_confirmation') {
          $labelItem.removeClass('label-product-lock-history-deposit-confirmation').addClass('label-product-lock-history-out-of-time')
          $labelItem.text('Hết hạn')
        } else if (state === 'waiting_reti_acceptance') {
          $labelItem.removeClass('label-product-lock-history-waiting-reti-acceptance').addClass('label-product-lock-history-pending')
          $labelItem.text('Xác nhận lại')
        }
      }
    });
  });

  if (getDevice() == 'mobile') {
    var height = 48;
  } else {
    var height = 42;
  }
  $('.project-news .content').each(function () {
    if ($(this).height() > height) {
      $(this).css({'height': height + 'px', 'overflow': 'hidden'});
    } else {
      $(this).css({'height': 'auto', 'overflow': 'auto'});
      $(this).closest('.project-news').find('.view-more').addClass('d-none');
    }
  });

  if ($('.dashboard-search').length) {
    dropdown = $('.dashboard-search .dropdown');
    let search = $(dropdown).find('.search');
    let items = $(dropdown).find('.container');
    let dropdown_toggle = dropdown.find('.dropdown-toggle');
    let dropdown_items = dropdown.find('.menuItems');
    let hidden = 0;
    search.on('input', function () {
      items.each(function () {
        if (parameterize($(this).text().trim().toLowerCase()).includes(parameterize(search.val().trim().toLowerCase()))) {
          $(this).show();
        } else {
          $(this).hide();
          hidden++;
        }
      })
    });
    dropdown_items.on('click', '.dropdown-item', function (e) {
      if ($(this).is(":checked")) {
        $(this).closest('.container').attr('data-sort', 0);
      } else {
        $(this).closest('.container').attr('data-sort', 1);
      }
      dropdown_items.find('.container').sort(function (a, b) {
        return +a.dataset.sort - +b.dataset.sort;
      })
      .appendTo(dropdown_items);
      let text = '';
      $('.dashboard-search .container').each(function () {
        if ($(this).attr('data-sort') == '0') {
          text += ($(this).text().trim().replace(/\s\s+/g, ' ').replace(/(\r\n|\n|\r)/gm, "") + ', ');
        }
      })
      text = text.slice(0, -2);
      if (text == '') {
        dropdown_toggle.text('Vui lòng chọn');
      } else {
        dropdown_toggle.text(text);
      }
      dropdown_toggle.append("<i class='fa fa-caret-down'>")
      $('.submit-project-ids').click();
      dropdown_toggle.dropdown('toggle');
    })
    $('.dropdown').on('show.bs.dropdown', function () {
      search.val('');
      dropdown_items.find('.container').show();
    })
  }

  $('.project-news .view-more').on('click', function () {
    $(this).closest('.project-news').find('.content').css({'height': 'auto', 'overflow': 'auto'});
    $(this).addClass('d-none');
    $(this).closest('.project-news').find('.view-less').removeClass('d-none');
  });

  $('.project-news .view-less').on('click', function () {
    $(this).closest('.project-news').find('.content').css({'height': height + 'px', 'overflow': 'hidden'});
    $(this).addClass('d-none');
    $(this).closest('.project-news').find('.view-more').removeClass('d-none');
  });

  if ($('#myChart').length) {
    var sum_price_max = parseInt($('#sum_price_max').text()) + 5;
    var ctx = document.getElementById('myChart').getContext("2d");
    var color = [];
    $('div[name="username"]').each(function () {
      if ($(this).text().includes('*')) {
        color.push('#56C5D0')
      } else {
        color.push('#595959')
      }
    })
    var myChart = new Chart(ctx, {
      showTooltips: false,
      type: 'bar',
      data: {
        labels: [$('#username_0').text(), $('#username_1').text(), $('#username_2').text(), $('#username_3').text(), $('#username_4').text()],
        datasets: [{
          label: 'Tổng GTGD (Tỷ VNĐ)',
          data: [parseFloat($('#sum_price_0').text()), parseFloat($('#sum_price_1').text()), parseFloat($('#sum_price_2').text()), parseFloat($('#sum_price_3').text()), parseFloat($('#sum_price_4').text())],
          backgroundColor: "#56C5D0",
          barPercentage: 1,
          categoryPercentage: 0.5,
          yAxisID: 'revenue'
        },
          {
            label: 'Căn đã chốt (Căn)',
            data: [parseFloat($('#deals_0').text()), parseFloat($('#deals_1').text()), parseFloat($('#deals_2').text()), parseFloat($('#deals_3').text()), parseFloat($('#deals_4').text())],
            backgroundColor: "#F15B40",
            barPercentage: 1,
            categoryPercentage: 0.5,
            yAxisID: 'lock',
          }
        ]
      },
      fontColor: '#FFFFFF',
      responsive: true,
      maintainAspectRatio: false,
      options: {
        animation: {
          duration: 0,
          onComplete: function () {
            var chartInstance = this;
            var ctx = chartInstance.ctx;
            ctx.font = "12px 'Helvetica Neue', Helvetica, Arial, sans-serif";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            this.data.datasets.forEach(function (dataset, i) {
              var meta = chartInstance.getDatasetMeta(i);
              if (i == 0) ctx.fillStyle = "#56C5D0";
              if (i == 1) ctx.fillStyle = "#595959";
              meta.data.forEach(function (bar, index) {
                var data = dataset.data[index];
                if (!isNaN(data)) ctx.fillText(data, bar.x, bar.y - 5);
              });
            });
          }
        },
        scales: {
          x: {
            grid: {drawBorder: true, display: false, drawTicks: true, drawOnChartArea: true},
            ticks: {
              color: color
            },
          },
          'revenue':
            {
              position: 'left',
              max: Math.ceil(sum_price_max / 10) * 10,
              min: 0,
              grid: {drawBorder: true, display: true, drawTicks: true},
              beginAtZero: true,
              ticks: {
                color: '#8C8C8C',
                font: {size: 14},
                stepSize: 10,
                maxTicksLimit: Math.ceil(sum_price_max / 10) + 1
              }
            },
          'lock':
            {
              position: 'right',
              max: Math.ceil(sum_price_max / 10) * 10,
              min: 0,
              grid: {drawBorder: true, display: true, drawTicks: true},
              beginAtZero: true,
              ticks: {
                color: '#8C8C8C',
                font: {size: 14},
                stepSize: 10,
                maxTicksLimit: Math.ceil(sum_price_max / 10) + 1
              }
            },
        },
        plugins: {
          legend: {
            labels: {font: {size: 16}, padding: 8, boxWidth: 16, boxHeight: 16},
            title: {color: '#595959'},
            position: 'bottom',
          },
        }
      }
    });
  }

  $(document).on('click', '.button-collapse-expand', function () {
    let text = $(this).find('span').first().text();
    $(this).addClass('d-none');
    $(this).siblings('.button-collapse-expand').removeClass('d-none');
    if (text === 'Rút gọn') {
      $(this).closest('.dashboard-sale-admin-block').find('.dashboard-sale-admin-body').children().css('max-height', '204px');
    } else {
      $(this).closest('.dashboard-sale-admin-block').find('.dashboard-sale-admin-body').children().css('max-height', '519px');
    }
  })
})

$(document).on('change', "input[type=radio][name='product_lock_history[state]']", function () {
  let modal = $(this).closest('.modal');
  $(this).find('.closeable').val('false');
  modal.find('.btn-save-modal').attr('disabled', false).removeClass('btn-tms-unable-submit').addClass('btn-reti-primary');
  if ($(this).val() === 'failed') {
    $(this).closest('.state-container').find('.reason_refuse').removeClass('d-none');
  } else {
    $(this).closest('.state-container').find('.reason_refuse').addClass('d-none');
  }
})

$(document).on('click', '.cancel-product-lock-history-modal', function (e) {
  if ($(this).closest('.modal.show').find('.closeable').val() === 'true') {
    $(this).closest('.modal.show').modal('hide');
  } else {
    e.preventDefault();
    swalBootstrapUpdateCsTicket.fire({
      title: '<div style="line-height: 24px; font-size: 16px">Lưu ý</div>',
      html: '<div>Bạn chắc chắn muốn thoát chứ?</div>',
      showCloseButton: true,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText: 'Thoát, không lưu',
      cancelButtonText: 'Ở lại',
      onOpen: function () {
        $('.swal2-modal').css({'width': 'auto', 'padding': '0px 0px 16px 0px'});
        $('.swal2-header').css('height', '56px');
        $('.swal2-title').css({'top': '16px', 'font-weight': '500'});
      }
    }).then((result) => {
      if (result.isConfirmed) {
        $('.modal.show').modal('toggle');
        $('.modal-backdrop').remove();
      }
    })
  }
})

$(document).on('show.bs.modal', '.modal-product-lock-history', function () {
  $(this).find('.closeable').val('true');
  $(this).find('.btn-save-modal').attr('disabled', true).addClass('btn-tms-unable-submit').removeClass('btn-reti-primary');
  $(this).find('.reason_refuse').addClass('d-none');
  $(this).find("input[type=radio][name='product_lock_history[state]']").each(function () {
    $(this).prop('checked', false);
  })
})

$(document).on('click', '.btn-copy-unc', function (e) {
  e.preventDefault();
  var copyText = $(this).siblings('.url-unc-image').text();
  var textarea = document.createElement("textarea");
  textarea.textContent = copyText;
  textarea.style.position = "fixed";
  document.body.appendChild(textarea);
  textarea.select();
  textarea.focus();
  document.execCommand("copy");
  document.body.removeChild(textarea);
  toastr.success('Sao chép hình ảnh thành công');
})

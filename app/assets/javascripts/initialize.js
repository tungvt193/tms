$.widget.bridge('uibutton', $.ui.button);

Turbolinks.clearCache();

function readURL(input) {
  if (input.files && input.files[0]) {
    let reader = new FileReader();
    reader.onload = function (e) {
      $('.image-preview').attr('src', e.target.result);
    };
    reader.readAsDataURL(input.files[0]);
  }
}

$('.logo').change(function () {
  readURL(this);
});

$(document).on("turbolinks:before-cache", function () {
  // $('select.select2').select2('destroy');
  $('select').each(function () {
    if ($(this).hasClass("select2-hidden-accessible")) $(this).select2('destroy');
  });
});

$(document).on('turbolinks:load', function () {
  $('.sale-rating-select2').select2({
    theme: 'bootstrap',
    language: 'vi',
    placeholder: 'Chọn 1 hoặc 2 sao'
  }).on('select2:select', function () {
    checkValInput($(this).closest('.modal.show'));
  })

  $('.voucher-select2').select2({
    theme: 'bootstrap',
    language: 'vi',
    placeholder: 'Chọn voucher'
  });
  $('.tms-select2').select2({
    theme: 'bootstrap',
    language: 'vi',
    placeholder: 'Vui lòng chọn'
  });

  if ($(".tms-icheck").length > 0) {
    $(".tms-icheck").each(function () {
      var $el = $(this);
      var skin = ($el.attr('data-skin') !== undefined) ? "_" + $el.attr('data-skin') : "",
        color = ($el.attr('data-color') !== undefined) ? "-" + $el.attr('data-color') : "";
      var opt = {
        checkboxClass: 'icheckbox' + skin + color,
        radioClass: 'iradio' + skin + color,
      };
      $el.iCheck(opt);
    });
  }

  $('.datepicker').datepicker({
    dateFormat: 'dd/mm/yy',
    showOtherMonths: true,
    firstDay: 1,
    monthNames: ['Tháng Một', 'Tháng Hai', 'Tháng Ba', 'Tháng Tư', 'Tháng Năm', 'Tháng Sáu', 'Tháng Bảy', 'Tháng Tám', 'Tháng Chín', 'Tháng Mười', 'Tháng Mười Một', 'Tháng Mười Hai'],
    monthNamesShort: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
    dayNames: ['Chủ Nhật', 'Thứ Hai', 'Thứ Ba', 'Thứ Tư', 'Thứ Năm', 'Thứ Sáu', 'Thứ Bảy'],
    dayNamesShort: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
    dayNamesMin: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7']
  });

  bsCustomFileInput.init();

  if ($('.field_with_errors').length > 0) {
    let errors = $('.field_with_errors');
    let errors_input = errors.find('.form-control')[0];
    if (errors.find('input[type=text]')[0] === undefined && errors.find('input[type=number]')[0] === undefined && errors.find('.tms-select2')[0] === undefined) {
      errors.find('.btn-file').focus();
    } else {
      let length = errors_input.value.length;
      errors_input.focus();
      if (length > 0) {
        errors_input.setSelectionRange(length, length);
      }
    }
  }

  $(window).scroll(function () {
    if ($(this).scrollTop() > 200) {
      $('.sticky-scroll').addClass('sticky-scroll-option');
      $('.sticky-scroll-option').css('top', 'calc(100vh - ' + ($('.sticky-scroll-option').height() - 16) + 'px)')
      $('.window-content').css('padding-bottom', $('.sticky-scroll-option').height() + 'px')
    } else {
      $('.sticky-scroll-option').css('top', 'unset')
      $('.sticky-scroll').removeClass('sticky-scroll-option');
      $('.window-content').css('padding-bottom', '0px')
    }
  })

  $('textarea.h-38').each(function () {
    this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
  }).on('input', function () {
    this.style.height = '38px';
    this.style.height = (this.scrollHeight) + 'px';
  });
});

$(document).on('cocoon:after-insert', '#visit_journeys, .visit-journey-hidden', function (e, insertedItem) {
  insertedItem.find('.customer_daterangepicker').daterangepicker({
    singleDatePicker: true,
    drops: 'up',
    timePicker: true,
    timePicker24Hour: true,
    autoUpdateInput: false,
    minYear: 2019,
    maxYear: 2119,
    locale: {
      format: 'HH:mm - DD/MM/YYYY'
    }
  }).on('apply.daterangepicker', function (ev, picker) {
    $(this).val(picker.startDate.format('HH:mm - DD/MM/YYYY'));
  });

  if ($(this).hasClass('visit-journey-hidden')) {
    insertedItem.find('.edit-visit-id').attr('value', $(this).data('id'));
  } else {
    $('#visit_journeys').find('.edit-visit-buttons').show();
  }
});

$(document).on('cocoon:after-insert', '#ads_journeys', function (e, insertedItem) {
  insertedItem.find('.customer_daterangepicker').daterangepicker({
    singleDatePicker: true,
    drops: 'up',
    timePicker: true,
    timePicker24Hour: true,
    autoUpdateInput: false,
    minYear: 2019,
    maxYear: 2119,
    locale: {
      format: 'HH:mm - DD/MM/YYYY'
    }
  }).on('apply.daterangepicker', function (ev, picker) {
    $(this).val(picker.startDate.format('HH:mm - DD/MM/YYYY'));
  });
  $(".ads-journey-control:first").removeClass('d-none');
});

$(document).on('cocoon:after-insert', '#steps-home, #steps-project_detail', function (e, insertedItem) {
  // insertedItem.find('.step').val($(this).find('.nested-fields:visible').length);
});

$(document).on('cocoon:after-remove', '#steps-home, #steps-project_detail', function (e, removedItem) {
  // var nestedFields = $(this).find('.nested-fields:visible');
  // if (nestedFields.length > 0) {
  //   _.each( nestedFields, function( field, i ){
  //     $(field).find('.step').val(i + 1);
  //   });
  // }
});

$(document).on('cocoon:before-insert', '#visit_journeys, .visit-journey-hidden, #ads_journeys, .ads_journeys', function (e, insertItem) {
  var insertVisit = $(e.target).hasClass('visit-links');
  var insertAds = $(e.target).hasClass('ads-links');

  var allowVisit = $('.nested-fields.visit-nested-fields:visible').length > 0
  var allowAds = $('.nested-fields.ads-nested-fields:visible').length > 0

  if ((allowVisit && insertAds) || (allowAds && insertVisit)) {
    e.preventDefault();
    swalWithBootstrapButtons.fire({
      title: 'Bạn chưa lưu thay đổi',
      html: "Các thay đổi của bạn chưa được lưu lại. Vui lòng <strong>Lưu lại</strong> hoặc <strong>Huỷ</strong> các thay đổi hiện tại!",
      showCloseButton: true,
      confirmButtonText: 'Đóng',
      reverseButtons: true,
      allowOutsideClick: false
    })
  }
});

$(document).on('cocoon:after-remove', '#ads_journeys, .ads_journeys', function (e, removedItem) {
  if ($(this).find('.nested-fields.new-journey').length == 0) {
    $(this).find('.new-ads-journey-control').addClass('d-none');
  }
});

$(document).on('cocoon:after-remove', '#visit_journeys, .visit-journey-hidden', function (e, removedItem) {
  if ($(this).find('.nested-fields').length == 0) {
    $(this).find('.edit-visit-buttons').hide();
  }
});

$(document).on('cocoon:after-insert', '#appointments, #calls, #online-feedbacks', function (e, insertedItem) {
  insertedItem.find('.datetimepicker').datetimepicker({
    format: 'd/m/Y H:i',
    lang: 'vi'
  })

  if ($(".tms-icheck").length > 0) {
    $(".tms-icheck").each(function () {
      var $el = $(this);
      var skin = ($el.attr('data-skin') !== undefined) ? "_" + $el.attr('data-skin') : "",
        color = ($el.attr('data-color') !== undefined) ? "-" + $el.attr('data-color') : "";
      var opt = {
        checkboxClass: 'icheckbox' + skin + color,
        radioClass: 'iradio' + skin + color,
      };
      $el.iCheck(opt);
    });
  }
});

$('#legal-documents').on('cocoon:after-insert', function (e, insertedItem) {
  insertedItem.find('.tms-select2').select2({
    theme: 'bootstrap',
    language: 'vi',
    placeholder: 'Vui lòng chọn'
  });
  bsCustomFileInput.init();
});

$('#milestones').on('cocoon:after-insert', function (e, insertedItem) {
  insertedItem.find('.datepicker').datepicker({
    dateFormat: 'dd/mm/yy',
    showOtherMonths: true,
    firstDay: 1,
    monthNames: ['Tháng Một', 'Tháng Hai', 'Tháng Ba', 'Tháng Tư', 'Tháng Năm', 'Tháng Sáu', 'Tháng Bảy', 'Tháng Tám', 'Tháng Chín', 'Tháng Mười', 'Tháng Mười Một', 'Tháng Mười Hai'],
    monthNamesShort: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
    dayNames: ['Chủ Nhật', 'Thứ Hai', 'Thứ Ba', 'Thứ Tư', 'Thứ Năm', 'Thứ Sáu', 'Thứ Bảy'],
    dayNamesShort: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
    dayNamesMin: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7']
  })
});

$('body').on('change', '.select2-region', function () {
  let parent = $(this);
  let children = parent.data("select-child-target");
  children.forEach(function (child) {
    let $child = $("#" + child);
    let $default_child_value = $("#" + child + "_default_value");
    let parent_selected_value = parent.children('option:selected').val();
    if (parent_selected_value !== null && parent_selected_value !== '') {
      $.ajax({
        method: 'POST',
        url: '/regions/get_regions',
        contentType: 'application/json',
        data: JSON.stringify({
          parent_id: parent_selected_value
        }),
        headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
        success: function (response) {
          if ($child.data('select2')) {
            $child.empty();
          }
          response.forEach(function (item) {
            let option = `<option value="${item.id}">${item.name_with_type}</option>`;
            $child.append(option);
          });
          if ($default_child_value !== undefined) {
            $child.val($default_child_value.val());
            $default_child_value.remove();
          }
        }
      })
    }
  })
});

// Trim whitespace
$('form').submit(function () {
  $(this).find('input:text').each(function () {
    $(this).val($.trim($(this).val()));
  });
  $('textarea').each(function () {
    $(this).val($.trim($(this).val()));
  });
});

// tms-deal-form
var sheet = document.createElement('style'),
  $rangeInput = $('.range input'),
  prefs = ['webkit-slider-runnable-track', 'moz-range-track', 'ms-track'];

document.body.appendChild(sheet);

var getTrackStyle = function (el) {
  var curVal = el.value,
    val = (curVal - 1) * 11.1111111,
    style = '';

  // Set active label
  $('.range-labels li').removeClass('active selected');

  var curLabel = $('.range-labels').find('li:nth-child(' + curVal + ')');

  curLabel.addClass('active selected');
  curLabel.prevAll().addClass('selected');

  // Change background gradient
  for (var i = 0; i < prefs.length; i++) {
    style += '.range {background: linear-gradient(to right, #56C5D0 0%, #56C5D0 ' + val + '%, #fff ' + val + '%, #fff 100%)}';
    style += '.range input::-' + prefs[i] + '{background: linear-gradient(to right, #56C5D0 0%, #56C5D0 ' + val + '%, #E9EAED ' + val + '%, #E9EAED 100%)}';
  }

  return style;
}

$rangeInput.on('input', function () {
  sheet.textContent = getTrackStyle(this);
});

// Change input value on label click
$('.range-labels li').on('click', function () {
  let editable = $(this).closest('#edit-deal-state').find('.source-group').hasClass('editable');
  if ( editable === true || (editable === false && $(this).closest('#edit-deal-state').find('#deal_after_first_click_label').val() === 'false')) {
    var index = $(this).index();
    $rangeInput.val(index + 1).trigger('change');
  }
  $(this).closest('#edit-deal-state').find('#deal_after_first_click_label').val('true');
});

var userAgent = navigator.userAgent.toLowerCase();
var iOS = /(ipad|iphone|ipod)/g.test(userAgent) || (navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1);
var android = /(android)/g.test(userAgent);
var mobile = iOS || android;
var check_divice_ruby = $('body').attr('device');
var check_divice_js = mobile ? 'mobile' : 'pc';
var device;
switch (true) {
  case check_divice_ruby == 'pc' && check_divice_js == 'pc' :
    device = 'pc';
    break;
  case check_divice_ruby == 'mobile' && check_divice_js == 'mobile':
    device = 'mobile';
    break;
  default:
    device = 'tablet';
    break;
}

$(document).ready(function () {
  $('.range-labels li.active').click();
  $('[data-toggle="tooltip"]').tooltip({
    template: '<div class="tooltip tms-tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
  });
  $('.tms-deal-form .btn-tool').click(function () {
    if ($(this).find('.fa-chevron-up').length) {
      $(this).find('.fa-chevron-up').addClass('fa-chevron-down').removeClass('fa-chevron-up');
    } else if ($(this).find('.fa-chevron-down').length) {
      $(this).find('.fa-chevron-down').addClass('fa-chevron-up').removeClass('fa-chevron-down');
    }
  })
});

function parameterize(str) {
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
  str = str.replace(/đ/g, "d");
  str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
  str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
  str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
  str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
  str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
  str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
  str = str.replace(/Đ/g, "D");
  str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // ̀ ́ ̃ ̉ ̣  huyền, sắc, ngã, hỏi, nặng
  str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // ˆ ̆ ̛  Â, Ê, Ă, Ơ, Ư
  str = str.replace(/ + /g, " ");
  str = str.trim();
  str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
  return str;
}

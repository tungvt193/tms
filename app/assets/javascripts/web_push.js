$(document).on('turbolinks:load', function() {
  if ($('#publicKey').length){
    var vapidPublicKey = new Uint8Array(JSON.parse($('#publicKey').val()));
    function checkNotifs(obj){
      if (!("Notification" in window)) {
        console.error("This browser does not support desktop notification");
      }
      else if (Notification.permission === "granted") {
        console.log("Permission to receive notifications has been granted");
        getKeys()
      }
      else if (Notification.permission !== 'denied') {
        Notification.requestPermission(function (permission) {
          if (permission === "granted") {
            console.log("Permission to receive notifications has been granted");
            getKeys();
          }
        });
      }
    }

    function getKeys(){
      if('serviceWorker' in navigator) {
        navigator.serviceWorker.register('/serviceworker.js', {scope: './'})
        .then(function(registration) {
          return registration.pushManager.getSubscription()
          .then(function(subscription) {
            if (subscription) {
              return subscription;
            }
            return registration.pushManager.subscribe({
              userVisibleOnly: true,
              applicationServerKey: vapidPublicKey
            });
          });
        }).then(function(subscription) {
          $.post('/users/subscribe', {
            subscription: subscription.toJSON(),
            message: 'You clicked a button!'
          });
        });
      };
    }

    $.ajaxSetup({
      headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $('#subscribe-message').prop('checked', (typeof Notification !== 'undefined') && Notification.permission === "granted" && $('#subscribe-message').attr('subscribe') == 'true');
    $('#subscribe-message').change(function(){
      if ($('#subscribe-message').is(":checked")){
        getKeys(false);
      } else {
        $.post('/users/subscribe', {
          subscribe: false
        });
      }
    });
    if ($('#publicKey').attr('refresh_subscribe')){
      getKeys(false);
    }
  }
});

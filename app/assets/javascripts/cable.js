// Action Cable provides the framework to deal with WebSockets in Rails.
// You can generate new channels where WebSocket features live using the `rails generate channel` command.
//
//= require action_cable
//= require_self
//= require_tree ./channels
(function () {
  this.App || (this.App = {});

  App.cable = ActionCable.createConsumer();
  App.cors_cable = ActionCable.createConsumer($('#cable_url').val());

}).call(this);

$(document).ready(function () {
  (function () {
    // Notifications
    App.notifications = App.cable.subscriptions.create({
        channel: 'NotificationChannel'
      },
      {
        connected: function () {
        },
        disconnected: function () {
        },
        received: function (data) {
          // append notification
          if (data.notification) {
            let data_id = $(data.notification).attr('data-id');
            let title = ''
            if (data.title.length > 0) {
              title = data.title
            } else {
              title = 'Bạn có thông báo mới'
            }
            if (!$(`.notification-item[data-id="${data_id}"]`).length) {
              $('#notification-list').prepend('' + data.notification);
              let current_counter = $('.notification-counter').text();
              let after_counter = parseInt(current_counter);
              after_counter++;
              $('.notification-counter').text(after_counter);
              $('.notification-counter-bot').text(after_counter);

              // noti bottom right
              $(document).Toasts('create', {
                title: title,
                autohide: true,
                delay: 3000,
                position: 'topRight',
                class: 'bg-info bg-toast-info',
                body: data.popup
              });
            }
          }

          // readed notification
          if (data.readed) {
            let current_counter = $('.notification-counter').text();
            let after_counter = parseInt(current_counter);
            after_counter--;
            $('.notification-counter').text(after_counter);
            $('.notification-counter-bot').text(after_counter);
            $(`.notification-item[data-id="${data.readed}"]`).removeClass('unread');
          }
          // read all
          if (data.read_all) {
            $('.notification-counter').text(0);
            $('.notification-counter-bot').text(0);
            $('.notification-item').removeClass('unread');
          }
          show_count_noti($('.notification-counter'));
        },
      });

    // Reminders
    App.reminders = App.cable.subscriptions.create({
        channel: 'ReminderChannel'
      },
      {
        connected: function () {
        },
        disconnected: function () {
        },
        received: function (data) {
          // append reminder
          if (data.reminder) {
            let data_id = $(data.reminder).attr('data-id');
            if (!$(`.reminder-item[data-id="${data_id}"]`).length) {
              $('#reminder-list').prepend('' + data.reminder);
              let current_counter = $('.reminder-counter').text();
              let after_counter = parseInt(current_counter);
              after_counter++;
              $('.reminder-counter').text(after_counter);
              $('.reminder-counter-bot').text(after_counter);
            }
          }
          // readed reminder
          if (data.readed) {
            let current_counter = $('.reminder-counter').text();
            let after_counter = parseInt(current_counter);
            after_counter--;
            $('.reminder-counter').text(after_counter);
            $('.reminder-counter-bot').text(after_counter);
            $(`.reminder-item[data-id="${data.readed}"]`).removeClass('unread');
          }
          // read all
          if (data.read_all) {
            $('.reminder-counter').text(0);
            $('.reminder-counter-bot').text(0);
            $('.reminder-item').removeClass('unread');
          }
          show_count_noti($('.reminder-counter'));
        },
      });

    // Website create deposit
    App.deposits = App.cors_cable.subscriptions.create({
        channel: 'StreamDepositChannel'
      },
      {
        connected: function () {
        },
        disconnected: function () {
        },
        received: function (data) {
          $.ajax({
            url: '/deposits/stream_product',
            type: 'GET',
            data: data
          })
          .done(function (data) {
          })
          .fail(function (dataFail) {
          });
        },
      }); 

    // Website create deal
    App.deal = App.cors_cable.subscriptions.create({channel: 'DealChannel'}, {
      connected: function () {
      },
      disconnected: function () {
      },
      received: function (data) {
        if ($('body').hasClass('body-kanban')){
          let deal = data.deal;
          let user_id = data.user_id;
          $.ajax({
            url: `/deals/${deal.id}/kanban_item?user_id=${user_id}&query=${$('#deal-search').val()}`,
            success: function(data) {
            }
          });
        }
      },
    });
  }).call(this);
  show_count_noti($('.notification-counter'));
  show_count_noti($('.reminder-counter'));
});

$(document).on('turbolinks:load', function() {
  show_count_noti($('.notification-counter'));
  show_count_noti($('.reminder-counter'));
});
function show_count_noti(that){
  if(that.text() == "0") that.hide();
  else that.show();
}
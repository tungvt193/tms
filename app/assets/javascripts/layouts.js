$(document).on('turbolinks:load', function() {
  var layoutImageConfigs = [];
  var layoutImageUrls = [];
  $('input[name="layout[image]"]').each(function (index, elm) {
    layoutImageConfigs.push({caption: $(elm).val(), width: '120px', url: $(elm).data('delete-url') + '?type=images', filename: $(elm).val()});
    layoutImageUrls.push($(elm).data('url'));
  });

  $("#layout-image-input").fileinput({
      initialPreview: layoutImageUrls,
      initialPreviewAsData: true,
      initialPreviewConfig: layoutImageConfigs,
      overwriteInitial: true,
      allowedFileTypes: ['image'],
      allowedFileExtensions: ['png', 'jpg', 'jpeg'],
      showCancel: false,
      showRemove: false,
      browseLabel: '',
      removeClass: 'btn btn-danger',
      removeLabel: '',
      theme: 'fas',
      maxFileCount: 1,
      deleteUrl: '/',
      fileActionSettings: {
        showZoom: function(config) {
          if (config.type === 'pdf' || config.type === 'image') {
            return true;
          }
          return false;
        }
      }
  }).on('filebeforedelete', function() {
    var aborted = !window.confirm('Are you sure you want to delete this file?');
    return aborted;
  }).on('filedeleted', function() {
    setTimeout(function() {
      window.alert('File deletion was successful! ' + krajeeGetCount('file-5'));
    }, 900);
  });

  var getLayoutDivisionPath = $('.layout-division-static-path').attr('href');

  $('.select2-layout-level, .select2-layout-subdivision, .select2-layout-block').change( function () {
    let parent = $(this);
    let children = parent.data("select-child-target");
    children.forEach(function (child) {
      let $child = $("#" + child);
      let $default_child_value = $("#" + child + "_default_value");
      let parent_selected_value = parent.children('option:selected').val();
      if (parent_selected_value !== null && parent_selected_value !== '') {
        $.ajax({
          method: 'POST',
          url: getLayoutDivisionPath + '/get_divisions',
          contentType: 'application/json',
          headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
          },
          data: JSON.stringify({
            parent_id: parent_selected_value,
            parent: parent.data("id"),
            child: child
          }),
          success: function (response) {
            if ($child.data('select2')) {
              $child.empty();
            }
            response.forEach(function (item) {
              let option = `<option value="${item.id}">${item.name}</option>`;
              $child.append(option);
            });

            if ($default_child_value !== undefined) {
              $child.val($default_child_value.val());
              $default_child_value.remove();
            }
          }
        })
      }
    })
  })

  function checkRoom(){
    let numPro = $('#coordinates').data('number-of-product') || 0;
    if ($('#coordinates .nested-fields:visible').length >= numPro){
      $('.btn-add-room').attr('disabled', '');
      if ($('#coordinates .nested-fields:last .code').val()){
        $('#coordinates .eq-coordinates').removeClass('d-none');
      }
    } else {
      $('.btn-add-room').removeAttr('disabled');
      $('#coordinates .eq-coordinates').addClass('d-none');
    }
  }
  checkRoom();
  $('#coordinates').on('cocoon:after-insert', function(e, insertedItem) {
    checkRoom();
  });
  $('#coordinates').on('cocoon:after-remove', function(e, insertedItem) {
    checkRoom();
  });
  $(':checkbox[readonly]').click(function(){
    return false;
  });

  $('.select2-subdivision-layout, .select2-block-layout').change( function () {
    let parent = $(this);
    let children = parent.data("select-child-target");
    let sub_children = parent.data("select-sub-child-target");

    let $child = parent.closest('.nested-fields').find("." + children + ':first-of-type');
    let $sub_child = parent.closest('.nested-fields').find("." + sub_children + ':first-of-type');
    let parent_selected_value = parent.children('option:selected').val();
    if (parent_selected_value !== null && parent_selected_value !== '') {
      $.ajax({
        method: 'POST',
        url: $('#interactive-layout-info').data('url') + '/get_divisions',
        contentType: 'application/json',
        headers: {
          'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        data: JSON.stringify({
          parent_id: parent_selected_value,
          parent: parent.data("id"),
          child: children
        }),
        success: function (response) {
          if ($child.data('select2')) {
            $child.empty();
            $sub_child.empty();
          }
          $child.append(`<option value=""></option>`);
          response.forEach(function (item) {
            let option = `<option value="${item.id}">${item.name}</option>`;
            $child.append(option);
          });
        }
      })
    }
  })

  $(document).on('show.bs.modal', '#modal-layout', function () {

    $('#interactive-layouts .nested-fields').each(function(){
      let imageUrls = [];
      let imageConfigs = [];
      $(this).find('.interactive-layout-image').each(function (index, elm) {
        imageConfigs.push({caption: $(elm).val(), width: '120px', url: $(elm).data('delete-url') + '?type=logo', filename: $(elm).val()});
        imageUrls.push($(elm).data('url'));
      });

      $(".upload-layout-image").fileinput({
        initialPreview: imageUrls,
        initialPreviewAsData: true,
        initialPreviewConfig: imageConfigs,
        overwriteInitial: false,
        allowedFileExtensions: ['svg'],
        msgInvalidFileExtension: 'File "{name}" không đúng định dạng. Chỉ được upload file "{extensions}"',
        showCancel: false,
        showRemove: false,
        browseLabel: '',
        removeClass: 'btn btn-danger',
        removeLabel: '',
        theme: 'fas',
        maxFileCount: 5,
        deleteUrl: '/',
        ajaxDeleteSettings: {
          headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
          }
        },
        fileActionSettings: {
          showZoom: function(config) {
            if (config.type === 'pdf' || config.type === 'image') {
              return true;
            }
            return false;
          }
        }
      }).on('filebeforedelete', function() {
        var aborted = !window.confirm('Are you sure you want to delete this file?');
        return aborted;
      }).on('filedeleted', function() {
        setTimeout(function() {
          window.alert('File deletion was successful! ' + krajeeGetCount('file-5'));
        }, 900);
      });
    })
    $('.fileinput-remove').addClass('d-none');
  })

  $('#interactive-layouts .nested-fields').each(function(){
    let imageUrls = [];
    let imageConfigs = [];
    let $field = $(this);
    $(this).find('.interactive-layout-image').each(function (index, elm) {
      imageConfigs.push({caption: $(elm).val(), width: '120px', url: $(elm).data('delete-url') + '?type=logo', filename: $(elm).val()});
      imageUrls.push($(elm).data('url'));
    });

    $(".upload-layout-image").fileinput({
      initialPreview: imageUrls,
      initialPreviewAsData: true,
      initialPreviewConfig: imageConfigs,
      overwriteInitial: false,
      allowedFileExtensions: ['svg'],
      msgInvalidFileExtension: 'File "{name}" không đúng định dạng. Chỉ được upload file "{extensions}"',
      showCancel: false,
      showRemove: false,
      browseLabel: '',
      removeClass: 'btn btn-danger',
      removeLabel: '',
      theme: 'fas',
      maxFileCount: 5,
      deleteUrl: '/',
      ajaxDeleteSettings: {
        headers: {
          'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        }
      },
      fileActionSettings: {
        showZoom: function(config) {
          if (config.type === 'pdf' || config.type === 'image') {
            return true;
          }
          return false;
        }
      }
    }).on('filebeforedelete', function() {
      var aborted = !window.confirm('Are you sure you want to delete this file?');
      return aborted;
    }).on('filedeleted', function() {
      setTimeout(function() {
        window.alert('File deletion was successful! ' + krajeeGetCount('file-5'));
      }, 900);
    });

    if ($('.division-id').val()) {
      $.ajax({
        method: 'POST',
        url: $('#interactive-layout-info').data('url') + '/find_divisions',
        contentType: 'application/json',
        headers: {
          'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        data: JSON.stringify({
          division_id: $('.division-id').val(),
        }),
        success: function (response) {
          response.forEach(function (result) {
            let select = $field.find('.' + result.select);

            if (select.data('select2')) {
              select.empty();
            }
            select.append(`<option value=""></option>`);
            result.items.forEach(function (item) {
              let option = `<option value="${item.id}">${item.name}</option>`;
              select.append(option);
            });
            select.val(result.value);
          });
        }
      })
    }
  })

  $('#interactive-layouts').on('cocoon:after-insert', function(e, insertedItem) {
    $('.tms-select2').select2({
      theme: 'bootstrap',
      language: 'vi',
      placeholder: 'Vui lòng chọn'
    });

    let imageUrls = [];
    let imageConfigs = [];
    // $('input[name="investor[logo]"').each(function (index, elm) {
    //   imageConfigs.push({caption: $(elm).val(), width: '120px', url: $(elm).data('delete-url') + '?type=logo', filename: $(elm).val()});
    //   imageUrls.push($(elm).data('url'));
    // });

    $(".upload-layout-image").fileinput({
      initialPreview: imageUrls,
      initialPreviewAsData: true,
      initialPreviewConfig: imageConfigs,
      overwriteInitial: false,
      allowedFileExtensions: ['svg'],
      msgInvalidFileExtension: 'File "{name}" không đúng định dạng. Chỉ được upload file "{extensions}"',
      showCancel: false,
      showRemove: false,
      browseLabel: '',
      removeClass: 'btn btn-danger',
      removeLabel: '',
      theme: 'fas',
      maxFileCount: 5,
      deleteUrl: '/',
      ajaxDeleteSettings: {
        headers: {
          'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        }
      },
      fileActionSettings: {
        showZoom: function(config) {
          if (config.type === 'pdf' || config.type === 'image') {
            return true;
          }
          return false;
        }
      }
    }).on('filebeforedelete', function() {
      var aborted = !window.confirm('Are you sure you want to delete this file?');
      return aborted;
    }).on('filedeleted', function() {
      setTimeout(function() {
        window.alert('File deletion was successful! ' + krajeeGetCount('file-5'));
      }, 900);
    });

    $('.select2-subdivision-layout, .select2-block-layout').change( function () {
      let parent = $(this);
      let children = parent.data("select-child-target");
      let sub_children = parent.data("select-sub-child-target");

      let $child = parent.closest('.nested-fields').find("." + children + ':first-of-type');
      let $sub_child = parent.closest('.nested-fields').find("." + sub_children + ':first-of-type');
      let parent_selected_value = parent.children('option:selected').val();
      if (parent_selected_value !== null && parent_selected_value !== '') {
        $.ajax({
          method: 'POST',
          url: $('#interactive-layout-info').data('url') + '/get_divisions',
          contentType: 'application/json',
          headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
          },
          data: JSON.stringify({
            parent_id: parent_selected_value,
            parent: parent.data("id"),
            child: children
          }),
          success: function (response) {
            if ($child.data('select2')) {
              $child.empty();
              $sub_child.empty();
            }
            $child.append(`<option value=""></option>`);
            response.forEach(function (item) {
              let option = `<option value="${item.id}">${item.name}</option>`;
              $child.append(option);
            });
          }
        })
      }

    })
  });

  $('.add-interactive-layout').submit(function(){
    $(this).find('.nested-fields').each(function(){
      if ($(this).find('.select2-floor-layout').val()){
        $(this).find('.division-id').val($(this).find('.select2-floor-layout').val());
      }else{
        if ($(this).find('.select2-block-layout').val()) {
          $(this).find('.division-id').val($(this).find('.select2-block-layout').val());
        }else{
          if ($(this).find('.select2-subdivision-layout').val()) {
            $(this).find('.division-id').val($(this).find('.select2-subdivision-layout').val());
          }
        }
      }
    })
  })

  $('.btn-show-childrens').click(function(){
    let $this = $(this)
    if(!$(this).hasClass('collapsed')){
      $(this).closest('table').find($(this).data('target')).each(function(){
        $(this).find('.btn-show-childrens').each(function(){
          if(!$(this).hasClass('collapsed')){
            $(this).click();
          }
        })
      })
    }
  })

});

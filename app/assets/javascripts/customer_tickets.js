$(document).on('turbolinks:load', function() {
  var customerTicketSuggestion = new Bloodhound({
    limit: 10,
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: '../customer_tickets/autocomplete?query=%QUERY',
      filter: function(data) {
        return data;
      },
      wildcard: "%QUERY"
    }
  });
  customerTicketSuggestion.initialize();
  var customerTicketTypeahead = $('#customer-ticket-search');
  customerTicketTypeahead.typeahead({
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      limit: 500,
      name: 'value',
      displayKey: 'value',
      source: customerTicketSuggestion.ttAdapter(),
      templates: {
        empty: '<div class="noitems">Không tìm thấy kết quả</div>'
    }
  });
});

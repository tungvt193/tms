App.notify = App.cable.subscriptions.create "ImportNotificationChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    # Called when there's incoming data on the websocket for this channel
    Swal.fire({
      title: data.message,
      html: data.content,
      confirmButtonColor: '#EF5B40',
      confirmButtonText: 'Xác nhận',
      allowOutsideClick: false
    }).then((result) ->
      if result.value
        location.reload(true)
    )

$(document).ready(function () {
  App.notifications = App.cable.subscriptions.create({channel: 'DealChannel'}, {
    connected: function () {
    },
    disconnected: function () {
    },
    received: function (data) {
      if ($('body').hasClass('body-kanban')){
        let deal = data.deal;
        let user_id = data.user_id;
        $.ajax({
          url: `/deals/${deal.id}/kanban_item?user_id=${user_id}&query=${$('#deal-search').val()}`,
          success: function(data) {
          }
        });
      }
    },
  });
});
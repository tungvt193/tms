$(document).on('turbolinks:load', function() {
  $(".permission-state-select2, .permission-commission-fields-select2").select2({
    theme: 'bootstrap checkbox-select2-container',
    placeholder: 'Vui lòng chọn',
    closeOnSelect : false,
  });

  $('.permission-icheck').on('ifChanged', function(event) {
    if (event.target.checked == false) {
      $(this).closest('.permission-group').find('.check-all').iCheck('uncheck');
    } else {
      if ($(this).closest('.permission-group').find('.permission-icheck:checked').length == $(this).closest('.permission-group').find('.permission-icheck').length) {
        $(this).closest('.permission-group').find('.check-all').iCheck('check');
      }
    }
  });

  $('.check-all').on('ifClicked', function(event) {
    if (event.target.checked == false) {
      $(this).closest('.permission-group').find('.permission-icheck').iCheck('check');
    } else {
      $(this).closest('.permission-group').find('.permission-icheck').iCheck('uncheck');
    }
  });

  $('.check-commission-policy').find('.permission-icheck').on('ifChecked', function () {
    if ($(this).parents('.check-commission-policy').children().text().trim() === 'Xem') {
      $("select[name*='commission_fields_can_view']").removeAttr('disabled');
    } else {
      $("select[name*='commission']").removeAttr('disabled');
    }
  }).on('ifUnchecked', function () {
    if ($(this).parents('.check-commission-policy').children().text().trim() === 'Xem') {
      if ($(this).parents('.check-commission-policy').siblings().find("input[name*='can_update']:checked").length === 0) {
        $("select[name*='commission_fields_can_view']").attr('disabled', 'disabled');
      }
    } else if ($(this).parents('.check-commission-policy').siblings().find("input[name*='can_edit']:checked").length > 0) {
      $("select[name*='commission_fields_can_edit']").attr('disabled', 'disabled');
      $("select[name*='commission_fields_validation']").attr('disabled', 'disabled');
    } else {
      $("select[name*='commission']").attr('disabled', 'disabled');
    }
  });

  $('.permission-commission-fields-select2').on("select2:open", function (e) {
    if ($(this).find(":selected").length === 11) {
      $(this).find("option[value='check_all']").prop("selected", "selected");
      $(this).trigger("change");
      $('.select2-selection__rendered').find("li[title='Chọn tất cả']").remove()
    }
  }).on("select2:select", function (e) {
    if (e.params.data.id === 'check_all') {
      $(this).find('option').prop("selected", "selected");
      $(this).trigger("change");
      $('#select2-role_permissions_attributes_10_commission_fields-results').find('li').each(function () {
        $(this).attr('aria-selected', true)
      });
      $(this).parent().find(".select2-container").siblings('select:enabled').select2('close').select2('open');
    } else {
      if ($(this).find(":selected").length === 11) {
        $(this).find("option[value='check_all']").prop("selected", "selected");
        $(this).trigger("change");
        $('.select2-results__option:contains("Chọn tất cả")').attr('aria-selected', 'true');
      }
    }
    $('.checkbox-select2-container').find('li').each(function () {
      if ($(this).attr('title') === 'Chọn tất cả') {
        $(this).remove();
      }
    })
    $(this).parent().find('.select2-search__field').val('');
  }).on("select2:unselect", function (e) {
    if (e.params.data.id === 'check_all') {
      $(this).find('option').prop("selected", "");
      $(this).trigger("change");
      $(this).parent().find(".select2-container").siblings('select:enabled').select2('close').select2('open');
    }
    $('.checkbox-select2-container').find('li').each(function () {
      if ($(this).attr('title') === 'Chọn tất cả') {
        $(this).remove();
      }
    });
    $(".permission-commission-fields-select2 > option[value='check_all']").prop("selected", "");
    $(".permission-commission-fields-select2").trigger("change");
    if ($('.select2-results__option:contains("Chọn tất cả")').attr('aria-selected') === 'true') {
      $('.select2-results__option:contains("Chọn tất cả")').attr('aria-selected', 'false');
    }
  });
})
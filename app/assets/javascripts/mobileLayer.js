!(function($){
  $.extend({
    toast: function(msg, time){
      var $toast = $('<div class="modal-mask"></div><div class="modal m-toast">' + msg + '</div>').appendTo(document.body);
      var w = $(".m-toast").outerWidth();
      $(".m-toast").css({marginLeft:-w/2}).addClass("modal-in");
        setTimeout(function(){
          $.closeModal()
        },time || 2000)
    },
    modal: function(params){
      params = params || {};
      var title = params.title ? '<div class="modal-title">'+params.title+'</div>' : '';
      var textHtml = params.text ? '<div class="modal-text">'+params.text+'</div>' : '';
      var btns = '';
      params.buttons.forEach(function(el){
        btns += '<span class="modal-button">'+el.text+'</span>'
      })
      var tpl = '<div class="modal-overlay modal-overlay-visible"></div>'+
          '<div class="modal m-alert">'+
            '<div class="modal-inner">'+title+textHtml+'</div>'+
            '<div class="modal-buttons ">'+
              btns+
            '</div>'+
          '</div>'
      $("#mobile-modal").append(tpl);
      $.afterOpen(params.afterOpen);
      var h = $(".modal").outerHeight();

      $(".modal").css({marginTop:-h/2+'px'}).addClass("modal-in");

      $(".modal-button").each(function(i, el){
        $(el).click(function(){
          $.closeModal(params.buttons[i].onClick)
        })
      })
    },
    afterOpen: function(fn){
      if(fn && typeof fn == "function"){
        fn();
      }
    },
    closeModal: function(fn){
      $(".modal").addClass("modal-out");
      $(".modal-overlay").removeClass("modal-overlay-visible");

      $(".modal").on("transitionend",function(){
        $(".modal,.modal-overlay,.modal-mask").remove();
        if(fn && typeof fn == "function"){
          fn();
        }
      })
    },
    alert: function(msg, title, callback){
      if(typeof title == "function"){
        callback = arguments[1];
        title = undefined;
      }
      $.modal({
        title: title,
        text: msg,
        buttons: [
          {onClick: callback, text: 'Okey'}
        ]
      })
    },
    confirm: function(msg, title, onOk, onCancle){
      if(typeof title == "function"){
        onCancle = arguments[2] || function(){};
        onOk = arguments[1];
        title = undefined;
      }
      $.modal({
        title: title,
        text: msg,
        buttons: [
          {onClick: onCancle, text: 'Cancel'},
          {onClick: onOk, text: 'Okey'}
        ]
      })
    },
    actions: function(params) {
      params = params || {};
      var actions = params.actions;
      var actionsList = '';
      var title = params.title ? '<div class="actions-title">'+params.title+'</div>' : '';
      sort_actions = actions.reduce(function(rv, x) {
        (rv[x['group']] = rv[x['group']] || []).push(x);
        return rv;
      }, {});
      let search = params.search ? '<div class="p-3" style="background:#fff;"><input type="text" class="actions-search form-control" placeholder="Tìm kiếm"></input></div>' : ''
      $.each( sort_actions, function( key, value ) {
        if (key != 'undefined') {
          actionsList += '<div class="actions-group"><div class="actions-group-title">'+key+'</div>';
          $.each(value, function(index, el) {
            actionsList += '<div class="actions-cell" data-disabled="' + el.disabled + '">'+el.text+'</div>';
          });
          actionsList += '</div>';
        }else{
          $.each(value, function(index, el) {
            actionsList += '<div class="actions-cell" data-disabled="' + el.disabled + '">'+el.text+'</div>';
          });
        }
      });
      var tpl = '<div class="modal-overlay modal-overlay-visible"></div>'+
          '<div class="m-actionsheet">'+title+ search +
            '<div class="actions-menu">'+actionsList+'</div>'+
            '<div class="actions-cancel actions-cell text-center">Huỷ</div>'
          '</div>'
      $("#mobile-modal").append(tpl);
      var h = $(".m-actionsheet").outerHeight();
      $(".m-actionsheet").addClass("active");
      $('body').css('overflow-y','hidden');
      $('.actions-search').keyup(function(){
        let word = $(this).val().toString().toLowerCase();
        $('.actions-cell').each(function(){
          if (!$(this).hasClass('actions-cancel')) {
            if ($.parameterize($(this).text().toString().toLowerCase()).includes($.parameterize(word))) {
              if ($(this).parent().hasClass('actions-group')) {
                $(this).parent().removeClass('d-none');
              }
              $(this).removeClass('d-none');
            }else{
              $(this).addClass('d-none');
              if ($(this).parent().hasClass('actions-group') && $(this).parent().children('.actions-cell').not('.d-none').length == 0) {
                $(this).parent().addClass('d-none');
              }
            }
          }
        })
      })
      $.actionsEvents(params);
    },
    actionsEvents: function(params) {
      // $(".modal-overlay,.actions-cancel").on("click",function(){$.closeActions(params)});

      var actions = params.actions;
      $(".m-actionsheet .actions-cell").each(function(index, el){
        if ($(el).data('disabled') != true) {
          $(el).click(function(){
            $.closeActions(params);
            if(actions[index] && typeof actions[index].onClick == "function"){
              actions[index].onClick()
            }
            $("#mobile-modal .modal-overlay").remove();
          })
        }
      })
    },
    parameterize: function(str){
      str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
      str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
      str = str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
      str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
      str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
      str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
      str = str.replace(/đ/g,"d");
      str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
      str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
      str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
      str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
      str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
      str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
      str = str.replace(/Đ/g, "D");
      str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // ̀ ́ ̃ ̉ ̣  huyền, sắc, ngã, hỏi, nặng
      str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // ˆ ̆ ̛  Â, Ê, Ă, Ơ, Ư
      str = str.replace(/ + /g," ");
      str = str.trim();
      str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g," ");
      return str;
    },
    closeActions: function(opt){
      $(".modal-overlay").removeClass("modal-overlay-visible");
      $(".m-actionsheet").removeClass("active");
      $(".m-actionsheet").on("transitionend",function(){
        if(opt.onClose && typeof opt.onClose == "function"){
          opt.onClose()
        }
        $(this,".modal-overlay").remove();
      });
      $('body').css('overflow-y','auto');
    },
    loading: function(msg,time) {
      var tpl = '<div class="m-loading">'+
              '<div class="modal-mask"></div>'+
              '<div class="toast"><i class="icon"></i><p class="toast-content">'+msg+'</p></div>'+
            '</div>'
      $("#mobile-modal").append(tpl)

      if(typeof(time) == 'number' && time > 0) {
        setTimeout(function(){
          $(".m-loading").remove();
        }, time)
      }
    },
    hideLoading: function() {
      $(".m-loading").remove();
    },
    toptip: function(msg, time, type){
      if(typeof time == 'string'){
        type = arguments[1];
      }
      if(!time && !type){
        type = ''
      }
      if(typeof time != 'number' || time < 0){
        time = undefined
      }
      $('<div class="modal-mask"></div><div class="m-toptip '+type+'">' + msg + '</div>').appendTo(document.body);
      var h = $(".m-toptip").height();
      $(".m-toptip").addClass("active");
      setTimeout(function(){
        $(".m-toptip").removeClass("active");
        $(".m-toptip").on("transitionend",function(){
          $(".m-toptip,.modal-mask").remove();
        })
      },time || 2000)
    }
  });
  //city-picker
  $.fn.cityPicker = function(params){
    var p = new CityPicker(this);
    p.init(params);
    return this;
  }
  function CityPicker (obj) {
    this.el = obj;
    this.wheels = []; //picker 滚动条
    this.pickerSelect = [0,0,0]; //picker选中的值
    this.province = []; //省
    this.cities = []; //市
    this.areas = []; //区
    this.areaFn = [this.setProvince,this.setCity,this.setArea];
    this.areaArray = []; //省市区集合
    this.year = (new Date()).getFullYear();
    this.month = (new Date()).getMonth() + 1;
    this.date = (new Date()).getDate();
  }
  CityPicker.prototype = {
    init:function(params){
      var _this = this;
      this.el.click(function(e){
        if (_this.el.attr('id') == undefined) return false;
        var value = $(params.element).val();
        var tpl = _this.template();
        $("#mobile-modal").append(tpl);
        $(".picker").show();
        var w = $(".picker").width();
        $(".picker").addClass("active");
        _this.confirm(params, value);
        _this.createdPicker(params);
        if(value !== ""){
          _this.setValue(value);
        }
      })
    },
    createWrapper: function(data, type){
      var item = '';
      switch(type) {
        case 'month':
          data.forEach(function(element, index){
            item += '<li class="wheel-item" data-index="'+index+'" data-id="'+(element)+'">Tháng '+(element)+'</li>'
          })
          break;
        default:
          data.forEach(function(element, index){
            item += '<li class="wheel-item" data-index="'+index+'" data-id="'+(element)+'">'+(element)+'</li>'
          })
      }
      return item;
    },
    setProvince: function(p){
      this.province = [];
      for(let i=2020; i<=2050; i++){
        this.province.push(i)
      }
      let pos = p || this.province.indexOf(this.year);
      let wheel = this.createWrapper(this.province, 'year');
      $(".cityPicker .wheel").eq(0).find(".wheel-scroll").html(wheel);
      if(this.wheels.length > 0){
        this.wheels[0].refresh();
        this.wheels[0].wheelTo(pos);
      }
      this.pickerSelect[0] = pos;
      // this.year = this.province[this.province.indexOf(this.year)];
      // var id = this.province[this.province.indexOf(this.year)].value;
      this.setCity(null)
    },
    setCity: function(pid, p, target){
      let _this = this;
      if(!(this instanceof CityPicker)){
        _this = target;
      }
      let pos = p || _this.month - 1;
      _this.cities = []
      for(let i=0; i<12; i++){
        _this.cities.push(i + 1)
      }
      let wheel = _this.createWrapper(_this.cities, 'month');
      $(".cityPicker .wheel").eq(1).find(".wheel-scroll").html(wheel);
      if(_this.wheels.length > 0){
        _this.wheels[1].refresh();
        _this.wheels[1].wheelTo(pos);
      }
      _this.pickerSelect[1] = _this.cities[pos];
      // _this.month = _this.cities[pos];
      let cid = _this.cities[pos];
      _this.setArea(null);
    },
    setArea: function(cid, p, target){
      let _this = this;
      if(!(this instanceof CityPicker)){
        _this = target;
      }
      let len = new Date(_this.year, _this.month, 0).getDate();
      let number;
      if ((_this.date - 1) >= len) {
        number = len -1;
      }else{
        number = _this.date - 1
      }
      let pos = p || number;
      _this.areas = [];

      for(let i=0; i<len; i++){
        _this.areas.push(i+1)
      }
      let wheel = _this.createWrapper(_this.areas, 'date');
      $(".cityPicker .wheel").eq(2).find(".wheel-scroll").html(wheel);

      if(_this.wheels.length > 0){
        _this.wheels[2].refresh();
        _this.wheels[2].wheelTo(pos);
      }
      _this.pickerSelect[2] = _this.areas[pos];
      _this.areaArray = [_this.province,_this.cities,_this.areas]
    },
    setValue: function(val){
      let values = val.split("/");
      let index = [];
      let ids = [];
      let _this = this;
      this.province.forEach(function(el, i, arr){
        if(el == values[2]){
          _this.pickerSelect[0] = arr[i];
          index[0] = i;
          ids.push(el);
        }
      })
      this.setProvince(index[0]);
      this.cities.forEach(function(el, i, arr){
        if(el == values[1]){
          _this.pickerSelect[1] = arr[i];
          index[1] = i;
          ids.push(el);
        }
      })
      this.setCity(ids[0],index[1]);

      this.areas.forEach(function(el, i, arr){
        if(el == values[0]){
          _this.pickerSelect[2] = arr[i];
          index[2] = i;
          ids.push(el);
        }
      })

      this.setArea(ids[1],index[2]);
    },
    getValue: function(params){
      var values = [],ids = [];
      this.pickerSelect.forEach(function(el){
        values.push(el);
        ids.push(el);
      })
      $(params.element).val(('0' + values[2]).slice(-2) + '/' + ('0' + values[1]).slice(-2) + '/' + this.province[values[0]])
      this.el.data("ids",ids)
    },
    template: function(){
      var tpl = '<div class="picker cityPicker">'+
          '<div class="picker-panel">'+
            '<div class="picker-title"><span class="cancel confirm pickerClose">Huỷ</span><h2 class="title mb-0">Chọn ngày</h2><span class="confirm pickerSubmit ">Lưu</span></div>'+
            '<div class="picker-content">'+
              '<div class="mask-top border-1px"></div>'+
              '<div class="wheel-wrapper">'+
                '<div class="wheel"><ul class="wheel-scroll"></ul></div>'+
                '<div class="wheel"><ul class="wheel-scroll"></ul></div>'+
                '<div class="wheel"><ul class="wheel-scroll"></ul></div>'+
              '</div>'+
              '<div class="mask-bottom border-top-1px"></div>'+
            '</div>'+
          '</div>'+
        '</div>'
      return tpl;
    },
    createdPicker: function(params){
      var wrapper = document.getElementsByClassName("wheel");
      var _this = this;
      for(var i=0; i<wrapper.length; i++){
        _this.wheels[i] = new BScroll(wrapper[i],{
          wheel:{selectedIndex: 0},
          probeType: 3
        })
        !(function(i){
          _this.wheels[i].on("scrollEnd",function(pos){
            var index = _this.wheels[i].getSelectedIndex();
            var id = $(_this.wheels[i].items[index]).data("id");
            _this.pickerSelect[i] = _this.areaArray[i][index];
            if (i == 0) {
              _this.year = id;
            }else if(i == 1){
              _this.month = id;
            }else{
              _this.date = id;
            }
            if (i == 0) {
              _this.setProvince()
            }
            if(_this.wheels[i+1]){
              var fn = _this.areaFn[i+1];
              fn(id,null,_this);
            }
            _this.getValue(params);
          })
        })(i)
      }
      this.setProvince()
    },
    confirm: function(params, value){
      $(".picker-title .pickerSubmit").click(function(){
        let date = new Date();
        if (!(value || $(params.element).val()) && value == ''  && $(params.element).val() == '' ) {
          $(params.element).val(('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth()+1)).slice(-2) + '/' + date.getFullYear());
        }
        if(params.onSubmit && typeof params.onSubmit == "function"){
          params.onSubmit();
        }
        $(".picker.cityPicker").removeClass("active");
        setTimeout(function(){
          $(".picker.cityPicker").remove();
        },300)
      })
      $(".picker-title .pickerClose").click(function(){
        $(params.element).val(value);
        if(params.onSubmit && typeof params.onSubmit == "function"){
          params.onClose();
        }
        $(".picker.cityPicker").removeClass("active");
        setTimeout(function(){
          $(".picker.cityPicker").remove();
        },300)
      })
    }
  }
})(jQuery)

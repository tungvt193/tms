$(document).on('turbolinks:load', function() {
  var customerSuggestion = new Bloodhound({
    limit: 10,
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: '../customers/autocomplete?query=%QUERY',
      filter: function(data) {
        return data;
      },
      wildcard: "%QUERY"
    }
  });

  customerSuggestion.initialize();

  var customerTypeahead = $('#customer-search');
  customerTypeahead.typeahead({
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      limit: 500,
      name: 'value',
      displayKey: 'value',
      source: customerSuggestion.ttAdapter(),
      templates: {
        empty: '<div class="noitems">Không tìm thấy kết quả</div>'
    }
  });
  // focus input error first .form-customer
  if ($('.form-customer .field_with_errors input:visible').length){
    $('.form-customer .field_with_errors input:visible:first').focus();
  }
  $(document).on('change', '#customer_country_code', function(){
    $('span.country-code-number').text($( "#customer_country_code option:selected" ).data('code'));
  })

  $(document).on('shown.bs.tab', '#custom-content-above-tab a[data-toggle="pill"]', function (e) {
    var target = $(e.target).attr("href");
    $('input[name="customer[show_history]"').attr('value', (target == '#custom-content-above-profile'));
  });

  $(document).on('click', '.delete-journey', function() {
    var type = $(this).data('type');
    var $this = $(this);
    var title = "Bạn chưa lưu thay đổi";
    var html = "Các thay đổi của bạn chưa được lưu lại. Bạn chắc chắn muốn <strong>Xoá</strong> hoạt động mà không <strong>Lưu lại</strong>?";
    if ($('.nested-fields:visible').length == 0) {
      title = "Bạn đang xoá hoạt động";
      html = "Bạn có chắc chắn muốn <strong>Xoá</strong>?"
    }
    swalWithBootstrapButtons.fire({
      title: title,
      html: html,
      showCloseButton: true,
      showCancelButton: true,
      confirmButtonText: 'Đồng ý',
      cancelButtonText: 'Hủy',
      reverseButtons: true,
      allowOutsideClick: false
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          url: $(this).data('delete-url'),
          type: "DELETE",
          success: function(res) {
            if (res.status == 'ok') {
              $this.closest('.journey-show').siblings( ".journey-hidden" ).remove();
              $this.closest('.journey-show').remove();
              if(res.email_size == 0) $('.email-journey-content').append('<div class="email-journey-empty">Chưa có thông tin</div>');

              swalWithBootstrapButtons.fire({
                title: 'Thành công',
                html: 'Xoá thành công!',
                showConfirmButton: false,
                timer: 1500
              })
            } else {
              swalWithBootstrapButtons.fire({
                title: 'Thất bại',
                html: 'Xoá thất bại. Vui lòng thử lại!',
                showConfirmButton: false,
                timer: 1500
              })
            }
          }
        });
      }
    })
  })

  $(document).on('keyup', '.ads-name', function(){
    $(this).closest('.ads-journey').find('.ads-journey-title').val($(this).val());
  })

  $('.customer_daterangepicker').daterangepicker({
    singleDatePicker: true,
    drops: 'up',
    timePicker: true,
    timePicker24Hour: true,
    autoUpdateInput: false,
    minYear: 2019,
    maxYear: 2119,
    locale: {
      format: 'HH:mm - DD/MM/YYYY'
    }
  }).on('apply.daterangepicker', function(ev, picker) {
    $(this).val(picker.startDate.format('HH:mm - DD/MM/YYYY'));
    $(this).closest('.form-group').find('.custom-validation').hide();
    $(this).closest('.field_with_errors').removeClass('field_with_errors');
  });

  $('.customer_daterangepicker').each(function (elm) {
    if($(this).val()) {
      $(this).data('daterangepicker').setStartDate($(this).val());
    }
  });
});

$(document).on('click', '.edit-journey', function() {
  if ($('.nested-fields:visible').length > 0) {
    swalWithBootstrapButtons.fire({
      title: 'Bạn chưa lưu thay đổi',
      html: "Các thay đổi của bạn chưa được lưu lại. Vui lòng <strong>Lưu lại</strong> hoặc <strong>Huỷ</strong> các thay đổi hiện tại!",
      showCloseButton: true,
      confirmButtonText: 'Đóng',
      reverseButtons: true,
      allowOutsideClick: false
    })
  } else {
    var visitId = $(this).data('id');
    $(this).closest('.journey-show').hide();
    $(this).closest('.journey-show').siblings( ".journey-hidden" ).show();
    $(this).closest('.tms-card').find('.btn-reti-secondary:first').closest('.card-body').remove();
  }
})

$(document).on('cocoon:before-remove', '#ads_journeys, .ads_journeys, #visit_journeys, .visit-journey-hidden', function (e, removedItem) {
  var confirmation = confirm("Bạn có chắc chắn muốn xoá?");
  if(confirmation === false){
    e.preventDefault();
  }
});

$(document).on('turbolinks:load', function() {
  let thirdHoldingMessage = 'Huỷ phiếu do sàn khác giữ căn';

  $(document).on('click', '#deposit-submit', function (e) {
    var storedState = $('#deposit_stored_state').val();
    changingState = $('#edit-deposit .tms-icheck:checked').val();
    if (!(changingState == 'canceled' && storedState != changingState)) {
      $('#deposit_is_third_holding').val(false);
      $('#edit-deposit').submit();
      return;
    }

    var thirdHoldingHtml = '<span class="checkbox">' +
                            '<label for="deposit_third_holding">' +
                              '<input class="check_boxes optional" style="margin-right: 5px; vertical-align: middle;" type="checkbox" value="true" name="deposit_third_holding" id="deposit_third_holding">' +
                              '<span class="standard-form--input-box" style="vertical-align: middle;">' + thirdHoldingMessage + '</span>' +
                            '</label>' +
                          '</span>'
    Swal.fire({
      title: 'Lý do huỷ',
      input: 'textarea',
      inputPlaceholder: 'Nhập lý do huỷ',
      html: thirdHoldingHtml,
      showCancelButton: true,
      confirmButtonText: 'Lưu',
      cancelButtonText: 'Huỷ',
      allowOutsideClick: false,
      preConfirm: (reason) => {
        if (reason == null || reason == '') {
          Swal.showValidationMessage('Chưa nhập lý do huỷ')
        } else {
          $('#deposit_third_holding_reason').val(reason);
          $('#deposit_is_third_holding').val($('#deposit_third_holding').is(':checked'));
        }
      }
    }).then((result) => {
      if (result.value) {
        $('#edit-deposit').submit();
      } else {
        $('#notes .nested-fields').remove();
      }
    })
  })

  $(document).on('change', 'input[name="deposit_third_holding"]', function () {
    if(this.checked) $('.layout-fixed .swal2-textarea').val(thirdHoldingMessage);
  });

  $('[data-countdown]').each(function () {
    var $this = $(this),
      finalDate = $(this).data('countdown'),
      id = $(this).data('id'),
      state = $(this).data('state');

    if (state == 'request_lock') {
      $this.countdown(finalDate, function (event) {
        $this.html(event.strftime('%H:%M:%S'));
      }).on('finish.countdown', function (event) {
        $.ajax({
          method: 'POST',
          url: '/deposits/finish_countdown',
          type: 'JSON',
          beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
          data: {
            id: id
          }
        }).done(function (response) {
          if (response.status == 'success') {
            $this.closest('tr').find("td.state").text('Đã hủy');
            $this.closest('tr').find("td.countdown-timer").text('');
          }
        })
      });
    }
  });
})

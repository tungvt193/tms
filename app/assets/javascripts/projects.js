if ($("#project-images-input").length || $('#product-images-input').length) {
  $('body').attr('data-turbolinks', 'false');
}
let inputFileConfig = {},
  inputFileImageUrls = {},
  inputFileStore = {},
  inputFileImagePreview = {},
  editSaleDocumentConfigs = [],
  editSaleDocumentUrls = [];
$(document).on('turbolinks:load', function () {
  // Project suggestion: suggest project name when typing
  let projectSuggestion = new Bloodhound({
    limit: 10,
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: '../projects/autocomplete?query=%QUERY',
      filter: function (data) {
        return data;
      },
      wildcard: "%QUERY"
    }
  });
  projectSuggestion.initialize();
  let projectTypeahead = $('#project-search');
  projectTypeahead.typeahead({
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      limit: 500,
      name: 'value',
      displayKey: 'value',
      source: projectSuggestion.ttAdapter(),
      templates: {
        empty: '<div class="noitems">Không tìm thấy kết quả</div>'
      }
    }).on('typeahead:select', function ($e, data) {
    if (data !== undefined) $('#project-search').val(data.value);
    window.location.href = '/projects/' + data.id;
  });

  // Upload file(s) with Krajee File Input plugin
  let imageConfigs = [];
  let imageUrls = [];
  let fileStoreImages = [];
  let imagePreviewArr = null;
  $('input[name="project[images][]"]').each(function (index, elm) {
    imageConfigs.push({
      caption: $(elm).val(),
      width: '120px',
      url: $(elm).data('delete-url') + '?model_name=projects&column_name=images&filename=' + $(elm).val()
    });
    // imageUrls.push($(elm).data('url'));
    let src = $(elm).data('url');
    if (src) {
      let filename = src.split('/').pop();
      let element = `${elm.outerHTML}<img src="${src}" class="file-preview-image kv-preview-data" title="${filename}" alt="${filename}">`;
      imageUrls.push(element);
      $(elm).remove();
    }
  });
  $("#project-images-input").fileinput({
    initialPreview: imageUrls,
    // initialPreviewAsData: true,
    initialPreviewConfig: imageConfigs,
    overwriteInitial: false,
    allowedFileTypes: ['image'],
    showCancel: false,
    showRemove: false,
    browseLabel: '',
    removeClass: 'btn btn-danger',
    removeLabel: '',
    theme: 'fas',
    deleteUrl: '/',
    ajaxDeleteSettings: {
      headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
      }
    },
    fileActionSettings: {
      showZoom: function (config) {
        return config.type === 'image';
      }
    }
  }).on('filebeforedelete', function () {
    return !window.confirm('Bạn có chắc muốn xóa ảnh này?');
  }).on('filedeleted', function () {
    setTimeout(function () {
      window.alert('Xóa ảnh thành công! ' + krajeeGetCount('file-5'));
    }, 900);
  }).on('filebatchselected', function (event, files) {
    let removeBtn = `<button type="button" class="kv-file-remove btn btn-sm btn-kv btn-default btn-outline-secondary btn-remove-preview-custom" title="Remove file" data-key="" data-id="project-images-input">
    <i class="fas fa-trash-alt"></i></button>`;
    $('#project-images-input').closest('.tms-field-group-body').find('.kv-preview-thumb:not(.file-preview-initial) .file-footer-buttons').prepend(removeBtn);
    fileStoreImages.push.apply(fileStoreImages, files);
    if (imagePreviewArr) {
      $('#project-images-input').closest('.tms-field-group-body').find('.kv-preview-thumb:not(.file-preview-initial)').first().before(imagePreviewArr);
    }
    imagePreviewArr = $('#project-images-input').closest('.tms-field-group-body').find('.kv-preview-thumb:not(.file-preview-initial)').clone(true);
    // change text file select
    if (imagePreviewArr.length > 1) {
      $('#project-images-input').closest('.tms-field-group-body').find('.file-caption-name').val(`${imagePreviewArr.length} files selected`);
    }
  });
  $('#project-images-input').closest('.tms-field-group-body').find('.file-caption-name').val('');
  $('#interactive-layout-input').fileinput({
    allowedFileTypes: ['pdf'],
    showCancel: false,
    showRemove: false,
    browseLabel: '',
    removeClass: 'btn btn-danger',
    removeLabel: '',
    theme: 'fas',
    maxFileCount: 1,
    deleteUrl: '/'
  })
  $(".project-submit-form").on('submit', function () {
    $('.kv-zoom-cache input').remove();
    $("#high-payment-schedules").find('.label-schedule').val(0);
    $("#low-payment-schedules").find('.label-schedule').val(1);
    if ($('#project-images-input').length > 0) {
      $('#project-images-input')[0].files = new fileListItems(fileStoreImages);
    }
    $('.input-file-container .input-file-init').each(function () {
      let inputId = $(this).attr('id');
      $(this)[0].files = new fileListItems(inputFileStore[inputId]);
    });
    $('.sale-document-input').each(function () {
      $(this)[0].files = new fileListItems(saleDocumentFiles);
    });
  })

  // Import project: upload file with dropzone
  $('#import-project-form #files-field-project').dropzone({
    url: location.pathname + '/import',
    dictDefaultMessage: 'Bạn có thể kéo hoặc thả tài liệu để tải lên',
    addRemoveLinks: true,
    autoProcessQueue: false,
    uploadMultiple: false,
    maxFiles: 1,
    acceptedFiles: ".xls,.xlsx,.csv",
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },
    accept: function (file, done) {
      $('#import-project-form #files-field-project').css({'height': 'auto'});
      done();
    },
    init: function () {
      let productDropzone = this;
      let form = document.getElementById('import-project-form');
      form.addEventListener('submit', function (event) {
        if (productDropzone.getQueuedFiles().length > 0) {
          event.preventDefault();
          event.stopPropagation();
          productDropzone.processQueue();
        }
      });
      productDropzone.on('sending', function (file, xhr, formData) {
        console.log(formData)
      });
      productDropzone.on('success', function () {
        if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
          productDropzone.removeFile(file);
          $('.btn-tool').click();
          $('.flash').html("<div class='tms-flash-notice alert'> <div class='row'> "
            + "<div class='col'style: 'padding-bottom: 8px'><i class='fas fa-check-circle icon-successful'></i> "
            + "<span class='text-successful'>Successfully!</span> <button class='close' 'aria-hidden'= 'true', 'data-dismiss'= 'alert', type= 'button'>"
            + "<i class='fas fa-times'></i></button></div></div><span>Dự án đang được import. Kết quả sẽ được gửi lại sau khi xử lý xong!</span></div>")
        }
      });
    }
  })

  $(document).on('click', '.btn-remove-preview-custom', function () {
    let numElementExist = $(this).closest('.file-preview-thumbnails').find('.kv-preview-thumb.file-preview-initial').length;
    let indexInField = $(this).closest('.kv-preview-thumb').index() - numElementExist;
    $(this).closest('.kv-preview-thumb').remove();
    let inputId = $(this).attr('data-id');
    if (inputId === 'project-images-input') {
      fileStoreImages.splice(indexInField, 1);
      imagePreviewArr = $('#project-images-input').closest('.tms-field-group-body').find('.kv-preview-thumb:not(.file-preview-initial)').clone(true);
      changeTextCap($('#project-images-input'));
    } else if (inputId === 'product-images-input') {
      productFileStore.splice(indexInField, 1);
      productPreviewArr = $('#product-images-input').closest('.tms-field-group-body').find('.kv-preview-thumb:not(.file-preview-initial)').clone(true);
      changeTextCap($('#product-images-input'));
    } else {
      inputFileStore[inputId].splice(indexInField, 1);
      inputFileImagePreview[inputId] = $(`#${inputId}`).closest('.input-file-container').find('.kv-preview-thumb:not(.file-preview-initial)').clone(true);
      changeTextCap($(`#${inputId}`));
    }
  });

  // Remove project image caches
  if ($('[name="project[images_cache]"]')) {
    let productPreviewArrRemove = $('#project-images-input').closest('.tms-field-group-body').find('.kv-file-remove:not(.btn-remove-preview-custom)');
    productPreviewArrRemove.click(function () {
      let fileNameClick = $(this).closest('.kv-preview-thumb').find('.file-footer-caption').attr('title');
      let arrString = $('[name="project[images_cache]"]').val();
      let arr = JSON.parse(arrString);
      arr = arr.filter(e => !e.includes(fileNameClick));
      $('[name="project[images_cache]"]').val(arr);
    });
  }

  // Remove product image cache
  if ($('[name="product[images_cache]"]')) {
    let productPreviewArrRemove = $('#product-images-input').closest('.tms-field-group-body').find('.kv-file-remove:not(.btn-remove-preview-custom)');
    productPreviewArrRemove.click(function () {
      let fileNameClick = $(this).closest('.kv-preview-thumb').find('.file-footer-caption').attr('title');
      let arrString = $('[name="product[images_cache]"]').val();
      let arr = JSON.parse(arrString);
      arr = arr.filter(e => !e.includes(fileNameClick));
      $('[name="product[images_cache]"]').val(arr);
    });
  }

  inputFileConfig = {},
    inputFileImageUrls = {},
    inputFileStore = {},
    inputFileImagePreview = {};

  $('.input-file-container .input-file-init').each(function () {
    let inputId = $(this).attr('id');
    inputFileConfig[inputId] = [];
    inputFileImageUrls[inputId] = [];
    inputFileStore[inputId] = [];
    inputFileImagePreview[inputId] = null;
  });

  $('.input-file-container').each(function (index, elm) {
    let inputId = $(this).find('.input-file-init').attr('id'),
      modelName = $(this).attr('model_name'),
      nestedId = $(this).attr('nested_id'),
      columnName = $(this).attr('column_name');
    $(this).find('.input-file-item').each(function (index, elm) {
      let url = $(elm).data('delete-url') + `?model_name=${modelName}&column_name=${columnName}&filename=` + $(elm).val();
      if (nestedId) {
        url = $(elm).data('delete-url') + `?model_name=${modelName}` + `&column_name=${columnName}` + `&nested_id=${nestedId}&filename=` + $(elm).val();
      }
      inputFileConfig[inputId].push({
        caption: $(elm).val(),
        width: '120px',
        url: url
      });
      let src = $(elm).data('url');
      if (src) {
        let fileName = src.split('/').pop();
        let element = `${elm.outerHTML}<img src="${src}" class="file-preview-image kv-preview-data" title="${fileName}" alt="${fileName}">`;
        inputFileImageUrls[inputId].push(element);
        $(elm).remove();
      }
    });
    initFileInputMultiple(inputId);
  });

  $('.input-file-container').each(function () {
    let inputId = $(this).find('.input-file-init').attr('id');
    // remove image in cache
    if ($(this).find('[name$="_cache"]').length) {
      let previewArrRemove = $(`#${inputId}`).closest('.input-file-container').find('.kv-file-remove:not(.btn-remove-preview-custom)');
      previewArrRemove.click(function () {
        let fileNameClick = $(this).closest('.kv-preview-thumb').find('.file-footer-caption').attr('title');
        let arrString = $(this).find('[name$="_cache"]').val();
        console.log(arrString);
        let arr = JSON.parse(arrString);
        arr = arr.filter(e => !e.includes(fileNameClick));
        $(this).find('[name$="_cache"]').val(arr);
      });
    }
  });

  $('#key_values, #floorplan_images, #documents').on('cocoon:after-insert', function (e, insertedItem) {
    if (insertedItem.find('.input-file-init-single').length) {
      initFileInputSingle(insertedItem.find('.input-file-init-single'), [], []);
      bsCustomFileInput.init();
    }
    if (insertedItem.find('.input-file-init').length) {
      let inputId = insertedItem.find('.input-file-init').attr('id');
      inputFileImageUrls[inputId] = [];
      inputFileConfig[inputId] = [];
      inputFileStore[inputId] = [];
      inputFileImagePreview[inputId] = null;
      initFileInputMultiple(inputId);
    }
    insertedItem.find('.tms-select2').select2({
      theme: 'bootstrap',
      language: 'vi',
      placeholder: 'Vui lòng chọn'
    });
  });

  $('.kv-zoom-cache input').remove();

  $('.input-file-container-single').each(function () {
    let configs = [],
      urls = [],
      modelName = $(this).attr('model_name'),
      nestedId = $(this).attr('nested_id'),
      columnName = $(this).attr('column_name');
    $(this).find('.input-file-item-single').each(function (index, elm) {
      configs.push({
        caption: $(elm).val(),
        width: '120px',
        url: $(elm).data('delete-url') + `?model_name=${modelName}` + `&column_name=${columnName}` + `&nested_id=${nestedId}`,
        filename: $(elm).val()
      });
      urls.push($(elm).data('url'));
    });
    initFileInputSingle($(this).find('.input-file-init-single'), urls, configs);
  });


  // Revenue model of goods
  function initializeDaterangePicker() {
    $(".apply-period").daterangepicker({
      autoUpdateInput: false,
      opens: 'right',
      minYear: 2019,
      maxYear: 2119,
      drops: "auto",
      locale: {
        format: 'DD/MM/YYYY'
      }
    }).on('apply.daterangepicker', function (ev, picker) {
      $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
    });
  }

  initializeDaterangePicker();
  $('#add-exclusive-revenue-btn').click(function () {
    $.get('/projects/' + $(this).data('project-id') + '/company_revenues/new_exclusive_revenue')
    .done(function (response) {
      let pseudoId = (new Date()).getTime();
      let responseHtml = $(response);
      responseHtml.find('input[name="company_revenue[pseudo_id]"]').val(pseudoId);
      $('#exclusive-revenues').append(responseHtml);
      $('.tms-select2').select2({
        theme: 'bootstrap',
        language: 'vi',
        placeholder: 'Vui lòng chọn'
      });
      initializeDaterangePicker();
    });
  });
  $('#add-exception-revenue-btn').click(function () {
    $.get('/projects/' + $(this).data('project-id') + '/company_revenues/new_exception_revenue')
    .done(function (response) {
      let pseudoId = (new Date()).getTime();
      let responseHtml = $(response);
      responseHtml.find('input[name="company_revenue[pseudo_id]"]').val(pseudoId);
      $('#exception-revenues').append(responseHtml);
      $('.tms-select2').select2({
        theme: 'bootstrap',
        language: 'vi',
        placeholder: 'Vui lòng chọn'
      });
    });
  });
  $(document).on('click', '.cancel-create-revenue', function () {
    $(this).closest('form').remove();
  })
  $(document).on('ajax:success', '.delete-revenue', function () {
    $(this).closest('form').remove();
  })
  $(document).on('click', '.edit-revenue', function () {
    $(this).closest('form').find('.form-control').removeAttr('disabled');
    $(this).addClass('d-none');
    $(this).next().addClass('d-none');
    $(this).next().next().removeClass('d-none');
    $(this).next().next().next().removeClass('d-none');
  })
  $(document).on('click', '.cancel-edit-revenue', function () {
    $(this).closest('form').find('.form-control').attr('disabled', true);
    $(this).addClass('d-none');
    $(this).prev().addClass('d-none');
    $(this).prev().prev().removeClass('d-none');
    $(this).prev().prev().prev().removeClass('d-none');
  })
  $(document).on('focus', '.apply-period', function () {
    $(this).closest('.form-group').removeClass('field_with_errors');
    $(this).closest('.form-group').find('.field_with_errors').removeClass('field_with_errors').addClass('field_with_errors_unhighlight');
    $(this).closest('.form-group').find('.custom-validation').hide();
  })

  // Investor Posts
  $('#add-investor-post-btn').click(function () {
    $.get('/projects/' + $(this).data('project-id') + '/sale_documents/new_investor_post')
    .done(function (response) {
      let pseudoId = (new Date()).getTime();
      let responseHtml = $(response);
      responseHtml.find('input[name="investor_post[pseudo_id]"]').val(pseudoId);
      $('#investor-posts').append(responseHtml);
    });
  })
  $(document).on('click', '.cancel-create-investor-post', function () {
    $(this).closest('form').remove();
  })
  $(document).on('ajax:success', '.delete-investor-post', function () {
    $(this).closest('form').remove();
    toastr.success('Xóa bài viết từ chủ đầu tư thành công.');
  })
  $(document).on('click', '.edit-investor-post', function () {
    $(this).closest('form').find('.form-control').removeAttr('disabled');
    $(this).addClass('d-none');
    $(this).next().addClass('d-none');
    $(this).next().next().removeClass('d-none');
    $(this).next().next().next().removeClass('d-none');
  })
  $(document).on('click', '.cancel-edit-investor-post', function () {
    $(this).closest('form').find('.form-control').attr('disabled', true);
    $(this).addClass('d-none');
    $(this).prev().addClass('d-none');
    $(this).prev().prev().removeClass('d-none');
    $(this).prev().prev().prev().removeClass('d-none');
  });

  initFileInputSaleDocument('sale-kit-input');
  initFileInputSaleDocument('sale-policy-input');
  $('#modal-sale-kit, #modal-sale-policy').on('show.bs.modal', function (e) {
    $('#modal-sale-kit, #modal-sale-policy').find('.field_with_errors').removeClass('field_with_errors').removeClass('invalid-feedback');
    $('#modal-sale-kit, #modal-sale-policy').find('.invalid-feedback').remove();
    $('#sale-kit-input, #sale-policy-input').fileinput('clear');
  });

  $(document).on('click', '.btn-new-sale-document', function () {
    $('#sale-kit-input, #sale-policy-input').fileinput('clear');
    $('#sale-kit-input, #sale-policy-input').fileinput('refresh');
    $('#sale-kit-input, #sale-policy-input').closest('.input-file-container').find('.fileinput-remove').addClass('d-none');
    $('.btn-save-sale-document').addClass('d-none');
    $('#sale-kit-input, #sale-policy-input').closest('.input-file-container').find('.file-drop-zone-title').html("Chọn một hoặc nhiều tài<br>liệu tải lên (tối đa 5 file)");
    saleDocumentFiles = [];
    htmlNestedFields = [];
  });

  $(document).on('click', '.btn-edit-sale-document', function (e) {
    let inputId = $(this).siblings('.modal').find('input[name="sale_document[file]"]').eq(1).attr('id');
    let file = $(this).siblings('.modal').find('input[name="sale_document[file]"]').first();
    editSaleDocumentConfigs.push({
      caption: file.val(),
      width: '120px',
      filename: file.val()
    });
    editSaleDocumentUrls.push(file.data('url'));
    initEditFileInputSaleDocument(inputId, editSaleDocumentConfigs, editSaleDocumentUrls);
    editSaleDocumentConfigs = [];
    editSaleDocumentUrls = [];
    $(this).siblings('.modal').find('.file-footer-buttons').addClass('d-none');
    $('.btn-save-sale-document').removeClass('d-none');
  });

  $(document).on('click', '.btn-save-sale-document', function (e) {
    $('.sale-document-input-title').each(function (e) {
      if ($(this).val().trim() === '' && !$(this).parent().hasClass('field_with_errors')) {
        $(this).val($(this).val().trim());
        $(this).parent().addClass('field_with_errors');
        $(this).parent().append('<span class="invalid-feedback custom-validation" style="text-align: start;">Không được để trống trường này!</span>')
      }
    });
    if ($(this).siblings('.input-file-container')[0]) {
      $(this).siblings('.input-file-container').find('.field_with_errors').find('.sale-document-input-title')[0].focus();
    }
    if ($(this).siblings('.form-group')[0]) {
      $(this).siblings('.form-group').find('.form-control')[0].focus();
    }
    if ($(this).siblings('.input-file-container').find('.sale-document-input-title').filter(function (e) {
      return $(this).val().trim() === ''
    }).length > 0) {
      e.preventDefault();
    }
  });

  $(document).on('click', '.btn-remove-sale-document', function () {
    let id = $(this).parents('.nested-fields').find('input[type=hidden]').first().attr('id');
    let fileName = $(this).closest('.nested-fields').find('.col-5').find('input[type=text]').eq(0).val();
    $(this).closest('.input-file-container').find('.file-caption-name').val(`${htmlNestedFields.length - 1} files selected`);
    $(this).parents('.nested-fields').remove();
    htmlNestedFields.forEach(function (value) {
      if (value.includes(id)) {
        htmlNestedFields = htmlNestedFields.filter(item => item !== value)
      }
    });
    saleDocumentFiles.forEach(function (file) {
      if (file.name === fileName) {
        saleDocumentFiles = saleDocumentFiles.filter(item => item.name !== file.name)
      }
    })
    if (htmlNestedFields.length < 1) {
      $('#sale-kit-input, #sale-policy-input').fileinput('clear');
      $('#sale-kit-input, #sale-policy-input').fileinput('refresh');
      $('.title-sale-document').removeClass('d-none');
      $('#sale-kit-input, #sale-policy-input').closest('.input-file-container').find('.fileinput-remove').addClass('d-none')
      $('.btn-save-sale-document').addClass('d-none');
      $('#sale-kit-input, #sale-policy-input').closest('.input-file-container').find('.file-drop-zone-title').html("Chọn một hoặc nhiều tài<br>liệu tải lên (tối đa 5 file)");
    }
  });

  $(document).on('click', '.input-group-btn.input-group-append', function () {
    let inputFile = $(this).closest('.input-file-container');
    if (htmlNestedFields.length > 0) {
      inputFile.find('.file-preview').addClass('d-block');
    }
  });

  $(document).on('keyup', '.sale-document-input-title', function (e) {
    let id = $(this).attr('id');
    let val = $(this).val();
    saleDocumentInputTitleVal.forEach(function (value) {
      if (value.includes(id)) {
        saleDocumentInputTitleVal = saleDocumentInputTitleVal.filter(item => item !== value)
      }
    });
    saleDocumentInputTitleVal.push([id, val]);
  });

  $(document).on('click', '#sale-kit-input, #sale-policy-input', function () {
    $(this).val('');
  })

  $(document).on('keyup', '.input-queue-lock', function () {
    checkValLockRules($(this));
  })

  $(document).on('select2:select', '.input-queue-lock', function () {
    checkValLockRules($(this));
  })

  $(document).on('change', '.input-queue-lock', function () {
    checkValLockRules($(this));
  })

  var defaultDate = $('.campaign-lock-datetimepicker').find('input').val();
  if (defaultDate === '') defaultDate = moment(new Date()).format('DD/MM/YYYY - HH:mm');
  $('.campaign-lock-datetimepicker').not('.disabled').daterangepicker({
    "singleDatePicker": true,
    "minYear": 2022,
    "maxYear": 2119,
    "timePicker": true,
    "timePicker24Hour": true,
    "timePickerIncrement": 1,
    "locale": {
      "format": "DD/MM/YYYY - HH:mm",
      "applyLabel": "Chọn",
      "cancelLabel": "Huỷ",
      "daysOfWeek": ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
      "monthNames": ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
      "firstDay": 1
    },
    "startDate": defaultDate,
    "endDate": defaultDate,
    "opens": "center",
    "drops": "auto"
  }).on('apply.daterangepicker', function (ev, picker) {
    $(this).find('input').val(picker.startDate.format("DD/MM/YYYY - HH:mm")).trigger('input');
    checkValFormControl($(this).closest('form'));
  }).on('showCalendar.daterangepicker', function (ev, picker) {
    $('.hourselect').find('option').each(function () {
      if ($(this).val() < 6 || $(this).val() > 21) {
        $(this).remove();
      }
    })
  });

  // Import sản phẩm trong campaign lock
  $('#import-product #files-field-product').dropzone({
    url: location.pathname + '/import',
    dictDefaultMessage: 'Bạn có thể kéo hoặc thả tài liệu để tải lên',
    addRemoveLinks: true,
    autoProcessQueue: false,
    uploadMultiple: false,
    maxFiles: 1,
    acceptedFiles: ".xls,.xlsx,.csv,.*",
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },
    maxfilesexceeded: function(file) {
      this.removeAllFiles();
      this.addFile(file);
    },
    accept: function (file, done) {
      $('#import-product #files-field-product').css({'height': 'auto'});
      done();
    },
    init: function () {
      let productDropzone = this;
      
      productDropzone.on('addedfile', function (file) {
        $('#import-product-preview').show()
      });

      productDropzone.on("removedfile", function (file) {
        if (productDropzone.getAcceptedFiles().length == 0) {
          $('#import-product-preview').hide()
          resetUploadFileImport()
        };
      });
    }
  });

  $('#btn-import-product-campaign').on('click', function (event) {
    $project_element = $('#campaign_lock_project_id');
    $('.invalid-feedback').remove()
    if ($project_element.val()) {
      $project_element.parent().removeClass('field_with_errors')
      $('#import-product').show()
      $(this).hide()
      $('#export-template').show()
    } else {
      $(this).parent().append('<span class="invalid-feedback custom-validation">Vui lòng chọn dự án trước khi tải file.</span>')
      $project_element.parent().addClass('field_with_errors')
      $project_element.parent().append('<span class="invalid-feedback custom-validation">Không được để trống trường này.</span>')
    }
  })

  $('#import-product-preview').on('click', function (event) {
    resetUploadFileImport()
    var formData = new FormData();
    formData.append('file', $('#import-product #files-field-product')[0].dropzone.getAcceptedFiles()[0]);
    formData.append('project_id', $('#campaign_lock_project_id').val());

    $.ajax({
      method: 'PUT',
      url: '/campaign_locks/preview_products',
      data: formData,
      processData: false,
      contentType: false,
      success: function(response) {
        var high_products_data = response.high_products
        var body_field_high_product = ["tag", "code", "subdivision_id", "block_id", "floor_id", "name", 
          "real_estate_type", "product_type", "direction", "built_up_area", "carpet_area", "currency", 
          "price", "sum_price", "certificate", "use_term", "furniture", "furniture_quality", "deposit", "source", "label"]
        var header_high_product = ["Nhãn sản phẩm", "Mã sản phẩm", "Phân khu", "Tòa", "Tầng", "Mã Căn/Sàn", "Loại hình", "Loại sản phẩm",
          "Hướng ban công", "Diện tích tim tường (m2)", "Diện tích thông thủy (m2)", "Đơn vị tiền tệ", "Đơn giá (Đã bao gồm VAT, KTBT)", 
          "Tổng GT (Đã bao gồm VAT, KPBT)", "Hình thức sở hữu", "Thời hạn sở hữu", "Tiêu chuẩn bàn giao", "Chất lượng nội thất", "Số tiền cọc", "Nguồn sản phẩm", "Giỏ hàng"]
        var header_high_product_html = '<thead><tr>'
          + header_high_product.map( function(e){
              return '<th scope="col">' + e + '</th>'
            }).join('')
          + '</tr></thead>'
        var body_high_product_html = '<tbody>'
          + high_products_data.map(function(prod) {
            var data = body_field_high_product.map(function (field) {
              return '<td>'+ prod[field] +'</td>'
            }).join('')
            return '<tr>' + data + '</tr>'
          }).join('')
          + '</tbody>'

        var table_high_product_html = '<table class="table">'
          + header_high_product_html
          + body_high_product_html
          + '</table>'
          
        $('#high-product').append(table_high_product_html);
        
        
        var low_products_data = response.low_products
        var header_low_product = ["Nhãn sản phẩm", "Mã sản phẩm", "Phân khu", "Đường/Dãy", "Mã Căn/Lô", "Loại hình", "Loại sản phẩm", "Hướng",
          "Diện tích lô đất (m2)", "Mật độ xây dựng (%)", "Diện tích sử dụng (m2)", "Kết cấu", "Đơn vị tiền tệ", "Đơn giá (Giá đất + XD)(Đã bao gồm VAT)",
          "Tổng GT (Giá đất + XD)(Đã bao gồm VAT)", "Hình thức sở hữu", "Thời hạn sở hữu", "Tình trạng bàn giao", "Số tiền cọc", "Nguồn sản phẩm", "Giỏ hàng"]
        var body_field_low_product = ["tag", "code", "subdivision_id", "block_id", "name", "real_estate_type", 
          "product_type", "direction", "plot_area", "density", "floor_area", "statics", "currency", "price", 
          "sum_price", "certificate", "use_term", "handover_standards", "deposit", "source", "label"]
        var header_low_product_html = '<thead><tr>'
          + header_low_product.map(function(e){
              return '<th scope="col">' + e + '</th>'
            }).join('')
          + '</tr></thead>'

        var body_low_product_html = '<tbody>'
          + low_products_data.map(function(prod) {
            var data = body_field_low_product.map(function (field) {
              return '<td>'+ prod[field] +'</td>'
            }).join('')
            return '<tr>' + data + '</tr>'
          }).join('')
          + '</tbody>'
        var table_low_product_html = '<table class="table">'
          + header_low_product_html
          + body_low_product_html
          + '</table>'
        $('#low-product').append(table_low_product_html);
        
        if (high_products_data.length === 0 && low_products_data.length > 0) {
          $('#tab-high-product').removeClass('active');
          $('#high-product').removeClass('active');

          $('#tab-low-product').addClass('active');
          $('#low-product').addClass('active');
        }

        if (high_products_data.length > 0 || low_products_data.length > 0) {
          $('.btn-save-campaign-lock').attr('disabled', false).css('background', '#F15B40');
          $('#campaign-lock #product_hidden_field').html('')
          $('#campaign-lock #product_hidden_field').append(high_products_data.concat(low_products_data).map(function(prod) {
            if (prod['tag'] === 'Hàng tồn') {
              return '<input id="' + prod['id'] + '" value="' + prod['id'] + '" multiple="multiple" type="hidden" name="campaign_lock[products_inventory][]">'
            } else {
              return '<input id="' + prod['id'] + '" value="' + prod['id'] + '" multiple="multiple" type="hidden" name="campaign_lock[products_hot][]">'
            }
          }).join(''))
        }

        // Show previews
        $('#prievew-products').show();
      },
      error: function(response) {
        var result = response.responseJSON
        swalBootstrapErrorCampaignLock.fire({
          title: 'Lỗi',
          html: result.error,
          showConfirmButton: true,
          confirmButtonText: 'Đồng ý',
          onOpen: function () {
            $('.swal2-header').css('background', '#EF5B40');
            $('.swal2-title').css({'font-weight': '500'});
            $('.swal2-content').css('color', '#434657');
            $('.swal2-content span').css({'font-weight': '500', 'color': '#F15B40'});
          }
        })
      }
    });
  })
});

$(document).on('click', ".remove-project", function (e) {
  e.preventDefault();
  let countDeals = parseInt($(this).attr('count_deals'));
  let id = parseInt($(this).attr('id'));
  if (countDeals > 0) {
    swalBootstrapUpdateCsTicket.fire({
      title: `<div style="line-height: 24px; font-size: 16px">Xoá dự án</div>`,
      html: `Dự án này đang liên quan tới ${countDeals} giao dịch. Bạn chắc chắn muốn xoá?`,
      showCloseButton: true,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText: 'Xác nhận xoá',
      cancelButtonText: 'Huỷ',
      onOpen: function () {
        $('.swal2-modal').css('width', 'auto');
        $('.swal2-header').css('height', '56px');
        $('.swal2-title').css({'top': '16px', 'font-weight': '500'});
        $('.swal2-content').css('padding', '16px 44px 0px 45px');
      }
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          method: 'DELETE',
          url: 'projects/'+ id +'',
          type: 'JSON',
          headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
          }
        })
      }
    })
  } else {
    if (confirm('Bạn chắc chắn muốn xóa dự án này?')) {
      $.ajax({
        method: 'DELETE',
        url: 'projects/'+ id +'',
        type: 'JSON',
        headers: {
          'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        }
      })
    }
  }
})

let htmlNestedFields = [];
let saleDocumentFiles = [];
let saleDocumentInputTitleVal = [];

function initFileInputSaleDocument(inputId) {
  var saleDocumentConfigs = [];
  var saleDocumentUrls = [];
  $(`#${inputId}`).fileinput({
    initialPreview: saleDocumentUrls,
    initialPreviewAsData: true,
    initialPreviewConfig: saleDocumentConfigs,
    overwriteInitial: false,
    allowedFileTypes: ['pdf'],
    showCancel: false,
    showRemove: false,
    browseLabel: '',
    removeClass: 'btn btn-danger',
    removeLabel: '',
    theme: 'fas',
    deleteUrl: '/',
    ajaxDeleteSettings: {
      headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
      }
    },
    fileActionSettings: {
      showZoom: function (config) {
        return config.type === 'pdf';
      }
    }
  }).on('fileloaded', function (event, file, previewId, fileId, index, reader) {
    let numberAttr = uid();
    let saleDocumentType;
    if (event.delegateTarget.id.includes('sale-kit')) {
      saleDocumentType = 0
    } else {
      saleDocumentType = 1
    }
    let nestedFields = '<div class="nested-fields">'
      + '<div class="row">'
      + '<input value="' + saleDocumentType + '" type="hidden" name="project[sale_documents_attributes][' + numberAttr + '][sale_document_type]" id="project_sale_documents_attributes_' + numberAttr + '_sale_document_type">'
      + '<div class="col-6">'
      + '<div class="form-group">'
      + '<div class="fields">'
      + '<label class="title-sale-document" for="project_sale_documents_attributes_' + numberAttr + '_title">Tên tài liệu</label>'
      + '<input class="form-control h-48 custom-form-control sale-document-input-title" value="" type="text" placeholder="Vui lòng nhập" name="project[sale_documents_attributes][' + numberAttr + '][title]" id="project_sale_documents_attributes_' + numberAttr + '_title">'
      + '</div>'
      + '</div>'
      + '</div>'
      + '<div class="col-5">'
      + '<div class="form-group" style="margin-top: 32px">'
      + '<div class="input-group border-0 input-sale-document">'
      + '<span class="icon-sale-document">' + '<i class="far fa-folder"></i>' + '</span>'
      + '<input class="form-control h-48 custom-form-control" type="hidden" name="project[sale_documents_attributes][' + numberAttr + '][file]" id="project_sale_documents_attributes_' + numberAttr + '_file">'
      + '<input type="text" value="' + file.name + '" style=" position: absolute; padding-left: 48px" class="form-control custom-form-control h-48 truncate-text" disabled="disabled">'
      + '</div>'
      + '</div>'
      + '</div>'
      + '<div class="col-1">'
      + '<input value="false" type="hidden" name="project[sale_documents_attributes][' + numberAttr + '][_destroy]" id="project_sale_documents_attributes_' + numberAttr + '__destroy">'
      + '<a class="remove_fields existing" href="#"><div class="form-group">' + '<button class="btn btn-outline-danger btn-remove-sale-document" style="margin-top: 36px" type="button">' + '<i class="fas fa-trash"></i>' + '</button>'
      + '</div>'
      + '</a>'
      + '</div>'
      + '</div>'
      + '</div>'
    if (htmlNestedFields.length < 5) {
      htmlNestedFields.push(nestedFields);
      if (event.delegateTarget.id.includes('sale-kit')) {
        $('#modal-sale-kit').find('.title-sale-document').addClass('d-none');
        $('#modal-sale-kit').find('.file-preview').html('').append(htmlNestedFields);
        $('#modal-sale-kit').find('.sale-document-input-title').each(function (index, e) {
          saleDocumentInputTitleVal.forEach(function (value) {
            if (value.includes(e.id)) {
              e.value = value[1];
            }
          });
        });
      } else {
        $('#modal-sale-policy').find('.title-sale-document').addClass('d-none');
        $('#modal-sale-policy').find('.file-preview').html('').append(htmlNestedFields);
        $('#modal-sale-policy').find('.sale-document-input-title').each(function (index, e) {
          saleDocumentInputTitleVal.forEach(function (value) {
            if (value.includes(e.id)) {
              e.value = value[1];
            }
          });
        });
      }
    } else {
      toastr.error('Bạn chỉ được upload tối đa 5 file.');
    }
    $(`#${inputId}`).closest('.input-file-container').find('.file-caption-name').val(`${htmlNestedFields.length} files selected`);
    $('.btn-save-sale-document').removeClass('d-none');
  }).on('filebeforedelete', function () {
    var aborted = !window.confirm('Bạn có chắc muốn xóa file này?');
    return aborted;
  }).on('filedeleted', function () {
    setTimeout(function () {
      window.alert('Xóa file thành công! ' + krajeeGetCount('file-5'));
    }, 900);
  }).on('filebatchselected', function (event, files) {
    saleDocumentFiles.push.apply(saleDocumentFiles, files);
    saleDocumentFiles = saleDocumentFiles.slice(0, 5);
    // change text file select
    if (saleDocumentFiles.length > 1) {
      $(`#${inputId}`).closest('.input-file-container').find('.file-caption-name').val(`${htmlNestedFields.length} files selected`);
    }
  }).on('fileselectnone', function (event) {
    $(this).closest('.input-file-container').find('.file-preview').css('display', 'block');
    if (saleDocumentFiles.length > 1) {
      $(this).closest('.input-file-container').find('.file-caption-icon').css('display', 'inline-block');
      $(this).closest('.input-file-container').find('.kv-fileinput-caption').addClass('icon-visible');
      $(this).closest('.input-file-container').find('.file-caption-name').val(`${$(this).closest('.input-file-container').find('.nested-fields').length} files selected`);
    } else if (saleDocumentFiles.length === 1) {
      $(this).closest('.input-file-container').find('.file-caption-icon').css('display', 'inline-block');
      $(this).closest('.input-file-container').find('.file-caption-name').val(`${saleDocumentFiles[0].name}`).css('padding-left', '15px');
    }
  }).on('fileerror', function (event) {
    $(this).closest('.input-file-container').find('.kv-fileinput-error.file-error-message').text('Định dạng không hợp lệ. Chỉ được upload file "PDF".');
    if ($(this).closest('.input-file-container').find('.nested-fields').length > 0) {
      toastr.error('Bạn chỉ được upload file "PDF".');
    }
  });
}

function initEditFileInputSaleDocument(inputId, editSaleDocumentConfigs, editSaleDocumentUrls) {
  $(`#${inputId}`).fileinput({
    initialPreview: editSaleDocumentUrls,
    initialPreviewAsData: true,
    initialPreviewConfig: editSaleDocumentConfigs,
    overwriteInitial: false,
    allowedFileTypes: ['pdf'],
    showCancel: false,
    showRemove: false,
    browseLabel: '',
    removeClass: 'btn btn-danger',
    removeLabel: '',
    theme: 'fas',
    maxFileCount: 5,
    deleteUrl: '/',
    ajaxDeleteSettings: {
      headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
      }
    },
    fileActionSettings: {
      showZoom: function (config) {
        return config.type === 'pdf';
      }
    }
  }).on('fileloaded', function (event, file, previewId, fileId, index, reader) {
    let filePreview = $(this).closest('.file-input.theme-fas');
    if (filePreview.hasClass('has-error')) {
      filePreview.removeClass('has-error');
      filePreview.find('.kv-fileinput-error.file-error-message').hide().empty();
      filePreview.find('.kv-fileinput-caption.file-caption').removeClass('is-invalid');
      filePreview.find('.file-caption-icon').remove();
    }
    $(this).closest('.modal-body').find('.file-preview-initial').addClass('d-none');
  }).on('filebeforedelete', function () {
    var aborted = !window.confirm('Bạn có chắc muốn xóa file này?');
    return aborted;
  }).on('filedeleted', function () {
    setTimeout(function () {
      window.alert('Xóa file thành công! ' + krajeeGetCount('file-5'));
    }, 900);
  }).on('fileerror', function (event) {
    $(this).closest('.modal-body').find('.kv-fileinput-error.file-error-message').text('Định dạng không hợp lệ. Chỉ được upload file "PDF".');
  })
}

const uid = () => {
  return Math.random().toString().slice(2).substr(0, 10);
};

jQuery.fn.swapWith = function (to) {
  return this.each(function () {
    let copyTo = $(to).clone(true);
    let copyFrom = $(this).clone(true);
    $(to).replaceWith(copyFrom);
    $(this).replaceWith(copyTo);
  });
};
let krajeeGetCount = function (id) {
  let cnt = $('#' + id).fileinput('getFilesCount');
  return cnt === 0 ? 'You have no files remaining.' :
    'You have ' + cnt + ' file' + (cnt > 1 ? 's' : '') + ' remaining.';
};

function fileListItems(files) {
  let b = new ClipboardEvent("").clipboardData || new DataTransfer()
  for (let i = 0, len = files.length; i < len; i++) b.items.add(files[i])
  return b.files
}

function changeTextCap($input) {
  let itemPreview = $input.closest('.tms-field-group-body').find('.kv-preview-thumb:not(.file-preview-initial)');
  let numFile = itemPreview.length;
  if (numFile > 1) {
    $input.closest('.tms-field-group-body').find('.file-caption-name').val(`${itemPreview.length} files selected`);
  } else if (numFile === 1) {
    $input.closest('.tms-field-group-body').find('.file-caption-name').val(itemPreview.find('.file-caption-info').text());
  } else {
    $input.closest('.tms-field-group-body').find('.file-caption-name').val('');
  }
}

function initFileInputSingle(element, imageUrls, imageConfigs) {
  element.fileinput({
    initialPreview: imageUrls,
    initialPreviewAsData: true,
    initialPreviewConfig: imageConfigs,
    overwriteInitial: false,
    allowedFileTypes: ['image'],
    showCancel: false,
    showRemove: false,
    browseLabel: '',
    removeClass: 'btn btn-danger',
    removeLabel: '',
    theme: 'fas',
    maxFileCount: 1,
    deleteUrl: '/',
    showUpload: false,
    ajaxDeleteSettings: {
      headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
      }
    },
    fileActionSettings: {
      showZoom: function (config) {
        return config.type === 'image';
      }
    }
  }).on('filebeforedelete', function () {
    return !window.confirm('Bạn có chắc muốn xóa ảnh này?');
  }).on('filedeleted', function () {
    setTimeout(function () {
      window.alert('Xóa ảnh thành công! ' + krajeeGetCount('file-5'));
    }, 900);
  });
}

function initFileInputMultiple(inputId) {
  $(`#${inputId}`).fileinput({
    initialPreview: inputFileImageUrls[inputId],
    initialPreviewConfig: inputFileConfig[inputId],
    overwriteInitial: false,
    allowedFileTypes: ['image'],
    showCancel: false,
    showRemove: false,
    browseLabel: '',
    removeClass: 'btn btn-danger',
    removeLabel: '',
    theme: 'fas',
    deleteUrl: '/',
    ajaxDeleteSettings: {
      headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
      }
    },
    fileActionSettings: {
      showZoom: function (config) {
        return config.type === 'image';
      }
    }
  }).on('filebeforedelete', function () {
    return !window.confirm('Bạn có chắc muốn xóa ảnh này?');
  }).on('filedeleted', function () {
    setTimeout(function () {
      window.alert('Xóa ảnh thành công! ' + krajeeGetCount('file-5'));
    }, 900);
  }).on('filebatchselected', function (event, files) {
    let removeBtn = `<button type="button" class="kv-file-remove btn btn-sm btn-kv btn-default btn-outline-secondary btn-remove-preview-custom" title="Remove file" data-key="" data-id="${inputId}">
    <i class="fas fa-trash-alt"></i></button>`;
    $(`#${inputId}`).closest('.input-file-container').find('.kv-preview-thumb:not(.file-preview-initial) .file-footer-buttons').prepend(removeBtn);
    inputFileStore[inputId].push.apply(inputFileStore[inputId], files);
    if (inputFileImagePreview[inputId]) {
      $(`#${inputId}`).closest('.input-file-container').find('.kv-preview-thumb:not(.file-preview-initial)').first().before(inputFileImagePreview[inputId]);
    }
    inputFileImagePreview[inputId] = $(`#${inputId}`).closest('.input-file-container').find('.kv-preview-thumb:not(.file-preview-initial)').clone(true);
    // change text file select
    if (inputFileImagePreview[inputId].length > 1) {
      $(`#${inputId}`).closest('.input-file-container').find('.file-caption-name').val(`${inputFileImagePreview[inputId].length} files selected`);
    }
  });
}

function checkValLockRules($this) {
  let parent = $this.closest('.card-body');
  let checkVal = parent.find('.input-queue-lock').filter(function () {return $(this).val().trim().length === 0}).length
  if (checkVal > 0) {
    parent.find('.btn-save-queue-lock').attr('disabled', true).removeClass('btn-reti-primary').addClass('btn-tms-unable-submit');
  } else {
    parent.find('.btn-save-queue-lock').attr('disabled', false).removeClass('btn-tms-unable-submit').addClass('btn-reti-primary');
  }
}

$(document).on('click', '.input-file-container-single .file-drop-zone', function () {
  $(this).closest('.input-file-container-single').find('.input-file-init-single').click();
});

$(document).on('select2:opening', '.floorplan_type', function (e) {
  let arrSelected = [];
  let currentId = $(this).attr('id');
  $(`#floorplan_images select.floorplan_type:not('#${currentId}')`).each(function () {
    arrSelected.push($(this).val());
  });
  $(this).find('option').each(function () {
    $(this).prop('disabled', arrSelected.includes($(this).val()));
  });
});

$(document).on('turbolinks:load', function () {
  selectedOptions = $.map($('#project-labels').find(":selected"), function (a) {
    if (a) return $(a).val();
  });

  // 3 is value of label out of stock
  if (selectedOptions.includes('3')) { // disabled another options if user select out of stock 
    $('#project-labels').find('option:not(:selected)').each(function (_, option) {
      if ($(option).val() !== '3') $(option).attr("disabled", true);
    })
  } else if (selectedOptions.length) { // disabled out of stock option if user select another options
    $('#project-labels').find('option:not(:selected)').each(function (_, option) {
      if ($(option).val() === '3') $(option).attr("disabled", true);
    })
  }
  $(this).find('.text-truncate').each(function () {
    if ($(this).width() < 250) {
      $(this).siblings('.count-value').addClass('d-none');
    }
  })
})

$(document).on('change', '#project-labels', function () {
  selectedOptions = $.map($(this).find(":selected"), function (a) {
    if (a) return $(a).val();
  });

  // 3 is value of label out of stock
  if (selectedOptions.includes('3')) { // disabled another options if user select out of stock 
    $(this).find('option:not(:selected)').each(function (_, option) {
      if ($(option).val() !== '3') $(option).attr("disabled", true);
    })
  } else if (selectedOptions.length) { // disabled out of stock option if user select another options
    $(this).find('option:not(:selected)').each(function (_, option) {
      if ($(option).val() === '3') $(option).attr("disabled", true);
    })
  } else { // enable all if no options selected
    $(this).find('option:not(:selected)').attr("disabled", false)
  }
});

$(document).on('keyup', '.form-control', function () {
  checkValFormControl($(this).closest('form'));
})

$(document).on('select2:select', '.form-control', function () {
  checkValFormControl($(this).closest('form'));
})

$(document).on('select2:unselect', '.form-control', function () {
  checkValFormControl($(this).closest('form'));
})

function checkValFormControl($form) {
  let listField = $form.find('.form-control');
  if (!(listField.filter(function () {return $(this).val().length === 0;}).length === 0)) {
    $form.find('.btn-save-campaign-lock').attr('disabled', true).css('background', '#B5B5B5');
  }
}

function resetUploadFileImport() {
  $('#low-product').html('')
  $('#high-product').html('')
  $('#prievew-products').hide();
  $('.btn-save-campaign-lock').attr('disabled', true).css('background', '#B5B5B5');
}

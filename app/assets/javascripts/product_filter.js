$(document).on('turbolinks:load', function () {
  var products_url = $('#product-info .active.nav-link').attr('href');
  if (products_url && products_url.indexOf("level=0") >= 0) {
    var productSelect2List = {
      'subdivision_id': {
        title: 'Phân Khu',
        url: `${$('#product-info').attr('data-products-url')}/subdivision_id_filter`,
        width: '91px',
        level: 0
      },
      'block_id': {
        title: 'Toà',
        url: `${$('#product-info').attr('data-products-url')}/block_id_filter`,
        width: '70px',
        level: 0
      },
      'floor_id': {
        title: 'Tầng',
        url: `${$('#product-info').attr('data-products-url')}/floor_id_filter`,
        width: '66px',
        level: 0
      },
      'state': {
        title: 'Trạng thái',
        url: `${$('#product-info').attr('data-products-url')}/state_filter`,
        width: '136px',
        level: 0
      }
    };
  } else {
    var productSelect2List = {
      'subdivision_id': {
        title: 'Phân Khu',
        url: `${$('#product-info').attr('data-products-url')}/subdivision_id_filter`,
        width: '91px',
        level: 1
      },
      'block_id': {
        title: 'Đường(Dãy)',
        url: `${$('#product-info').attr('data-products-url')}/block_id_filter`,
        width: '70px',
        level: 1
      },
      'state': {
        title: 'Trạng thái',
        url: `${$('#product-info').attr('data-products-url')}/state_filter`,
        width: '136px',
        level: 1
      }
    };
  }

  var Utils = $.fn.select2.amd.require('select2/utils');
  var Dropdown = $.fn.select2.amd.require('select2/dropdown');
  var DropdownSearch = $.fn.select2.amd.require('select2/dropdown/search');
  var AttachBody = $.fn.select2.amd.require('select2/dropdown/attachBody');
  var dropdownAdapter = Utils.Decorate(Utils.Decorate(Dropdown, DropdownSearch), AttachBody);

  // Set up the Select2 control
  _.forEach(productSelect2List, function (filterValue, filterKey) {
    product_filter_select2(filterValue, filterKey, false, false);
  });

  function product_filter_select2(filterValue, filterKey, parent_ids, ancestor_ids) {
    $(`.select2-filter-${filterKey}`).select2({
      ajax: {
        url: filterValue.url,
        dataType: 'json',
        data: function (params) {
          var query = {
            search: params.term,
            level: productSelect2List[filterKey].level
          }
          if (parent_ids && parent_ids != []) {
            query = $.extend(query, {parent_id: parent_ids});
          }
          if (ancestor_ids && ancestor_ids != []) {
            query = $.extend(query, {ancestor_id: ancestor_ids});
          }

          return query;
        },
        processResults: function (data) {
          data = _.sortBy(data, function (item) {
            return _.includes($(`.select2-filter-${filterKey}`).val(), item.id.toString()) ? 0 : 1;
          });
          var dataResults = [];
          _.forEach(data, function (value) {
            if (_.includes($(`.select2-filter-${filterKey}`).val(), value.id.toString())) {
              value.html = `<input class="icheckbox_square-custom mr-1" type="checkbox" checked="true" id="select2-custom-${filterKey}-${value.id}"><label class="mr-1">${value.text}</label>`;
            } else {
              value.html = `<input class="icheckbox_square-custom mr-1" type="checkbox" id="select2-custom-${filterKey}-${value.id}"><label class="mr-1">${value.text}</label>`;
            }
            value.title = value.text;
            dataResults.push(value);
          });
          return {
            results: dataResults
          };
        },
        cache: true
      },
      theme: 'bootstrap',
      language: 'vi',
      width: 'resolve',
      placeholder: filterValue.title,
      minimumInputLength: 0,
      dropdownAutoWidth: true,
      allowClear: true,
      multiple: true,
      closeOnSelect: false,
      dropdownAdapter: dropdownAdapter,
      escapeMarkup: function (markup) {
        return markup;
      },
      templateResult: function (data) {
        return (data.html == undefined ? data.text : data.html);
      },
      templateSelection: function (data) {
        return ($(`.select2-filter-${filterKey}`).val()[1] == data.id ? '...' : data.text);
      }
    }).on('select2:open', function (e) {
      customProductStyleSelect2();
      if ($(this).data('state') === 'unselected') {
        $(this).removeData('state');
        $(this).select2('close');
      }
    }).on('change.select2', function (e) {
      var searchInline = $('#' + `${filterKey}-search-product`).next().find('.select2-search--inline');
      $(`.select2-filter-${filterKey}`).val()[0] != undefined ? searchInline.hide() : searchInline.show();
      // set width
      var changedWidth = $(`.select2-filter-${filterKey}`).val().length <= 0 ? productSelect2List[filterKey].width : 'auto'
      $(`.select2-filter-${filterKey}`).closest('.form-group').css({width: changedWidth});
      if ($(this).is($('#subdivision_id-search-product'))) {
        $('#block_id-search-product').val(null);
        $('#block_id-search-product').select2('data', null);
        product_filter_select2(productSelect2List['block_id'], 'block_id', $(this).val(), false);
        if ($('#floor_id-search-product').length) {
          let subdivision_filter_val = $('#subdivision_id-search-product').val();
          $('#floor_id-search-product').val(null);
          $('#floor_id-search-product').select2('data', null);
          if (products_url && products_url.indexOf("level=0") >= 0) {
            product_filter_select2(productSelect2List['floor_id'], 'floor_id', $(this).val(), subdivision_filter_val);
          }
        }
      }
      if ($(this).is($('#block_id-search-product'))) {
        let subdivision_filter_val = $('#subdivision_id-search-product').val();
        $('#floor_id-search-product').val(null);
        $('#floor_id-search-product').select2('data', null);
        if (products_url && products_url.indexOf("level=0") >= 0) {
          product_filter_select2(productSelect2List['floor_id'], 'floor_id', $(this).val(), subdivision_filter_val);
        }
      }
      // set width
      _.forEach(productSelect2List, function (filterValue1, filterKey1) {
        let changedWidth = $(`.select2-filter-${filterKey1}`).val().length <= 0 ? productSelect2List[filterKey1].width : 'auto'
        $(`.select2-filter-${filterKey1}`).closest('.form-group').css({width: changedWidth});
      });
      $('.product-search-group .btn-search-product').click();
    }).on("select2:select", function (e) {
      $(`#select2-custom-${filterKey}-${e.params.data.id}`).prop('checked', true);
    }).on("select2:unselect", function (e) {
      $(`#select2-custom-${filterKey}-${e.params.data.id}`).prop('checked', false);
      $(`#select2-custom-${filterKey}-${e.params.data.id}`).parent().removeClass('select2-results__option--highlighted').attr('aria-selected', false);
      $(`.select2-filter-${filterKey} option[value="${e.params.data.id}"]`).remove();
      $(`.select2-filter-${filterKey}`).trigger('change.select2');
    }).on("select2:unselecting", function (e) {
      $(this).data('state', 'unselected');
    });
  }

  var customProductStyleSelect2 = function (isAddFilter) {
    $('.select2-filter--dropdown input.select2-filter__field').prop('placeholder', 'Tìm kiếm');
    if (isAddFilter) {
      $('.select2-container.select2-container--open:not(.select2)').css({
        'margin-top': '25px',
        'margin-left': '5px',
        'border-top': 'solid 2px #3ac5c9'
      });
    } else {
      $('.select2-container.select2-container--open:not(.select2)').css({
        'margin-top': '5px',
        'border-top': 'solid 2px #3ac5c9'
      });
    }
    $('.select2-container.select2-container--open .select2-dropdown').css({'border-radius': '0'});
    $('.select2-filter--dropdown').css({'padding': '0'});
    $('.select2-container:not(.select2) .select2-filter__field').css({
      'border-radius': '0',
      'border': '0',
      'border-bottom': 'solid 1px #3ac5c9',
      'height': '34px',
      'padding-left': '12px'
    });
  }
  let unchecked_states = [], unchecked_sources = [], unchecked_projects = [], unchecked_assignees = [],
    unchecked_labels = [],
    unchecked_purchase_purposes = [], unchecked_product_types = [], unchecked_balcony_directions = [],
    unchecked_door_directions = [], unchecked_subdivision_ids = [], unchecked_block_ids = [], unchecked_floor_ids = [],
    unchecked_names = [], unchecked_real_estate_types = [];
  // Option select for deals
  $('.complete-deals-filter-select2').map(function () {
    switch ($(this).attr('name')) {
      case 'state[]':
        unchecked_states.push($(this).children());
        break;
      case 'source[]':
        unchecked_sources.push($(this).children());
        break;
      case 'project[]':
        unchecked_projects.push($(this).children());
        break;
      case 'assignee[]':
        unchecked_assignees.push($(this).children());
        break;
      case 'label[]':
        unchecked_labels.push($(this).children());
        break;
      case 'purchase_purpose[]':
        unchecked_purchase_purposes.push($(this).children());
        break;
      case 'product_type[]':
        unchecked_product_types.push($(this).children());
        break;
      case 'balcony_direction[]':
        unchecked_balcony_directions.push($(this).children());
        break;
      case 'door_direction[]':
        unchecked_door_directions.push($(this).children());
        break;
    }
  });
  // Option select for products
  $('.product-filter-select2').map(function () {
    switch ($(this).attr('name')) {
      case 'project[]':
        unchecked_projects.push($(this).children());
        break;
      case 'product_type[]':
        unchecked_product_types.push($(this).children());
        break;
      case 'purchase_purpose[]':
        unchecked_purchase_purposes.push($(this).children());
        break;
      case 'subdivision_id[]':
        unchecked_subdivision_ids.push($(this).children());
        break;
      case 'block_id[]':
        unchecked_block_ids.push($(this).children());
        break;
      case 'floor_id[]':
        unchecked_floor_ids.push($(this).children());
        break;
      case 'name[]':
        unchecked_names.push($(this).children());
        break;
      case 'real_estate_type[]':
        unchecked_real_estate_types.push($(this).children());
        break;
      case 'state[]':
        unchecked_states.push($(this).children());
        break;
    }
  });
  $(".product-filter-select2, .complete-deals-filter-select2").select2({
    theme: 'bootstrap checkbox-select2-container',
    placeholder: 'Vui lòng chọn',
    closeOnSelect: false,
  }).on('select2:open', function () {
    if ($(this).siblings().find('.select2-selection__choice').length > 0) {
      let select_option = [];
      let selection_choices = $(this).siblings().find('.select2-selection__choice').map(function () {
        return $(this).attr('title');
      }).get();
      $(this).find('option').each(function (i, e) {
        if (selection_choices.includes($(this).text())) {
          select_option.push(e);
        }
      });
      $(this).find('option').each(function (i, e) {
        if (!selection_choices.includes($(this).text())) {
          select_option.push(e);
        }
      });
      $(this).append(select_option);
    } else {
      switch ($(this).attr('name')) {
        case 'state[]':
          $(this).append(unchecked_states);
          break;
        case 'source[]':
          $(this).append(unchecked_sources);
          break;
        case 'project[]':
          $(this).append(unchecked_projects);
          break;
        case 'assignee[]':
          $(this).append(unchecked_assignees);
          break;
        case 'label[]':
          $(this).append(unchecked_labels);
          break;
        case 'purchase_purpose[]':
          $(this).append(unchecked_purchase_purposes);
          break;
        case 'product_type[]':
          $(this).append(unchecked_product_types);
          break;
        case 'balcony_direction[]':
          $(this).append(unchecked_balcony_directions);
          break;
        case 'door_direction[]':
          $(this).append(unchecked_door_directions);
          break;
        case 'subdivision_id[]':
          $(this).append(unchecked_subdivision_ids);
          break;
        case 'block_id[]':
          $(this).append(unchecked_block_ids);
          break;
        case 'floor_id[]':
          $(this).append(unchecked_floor_ids);
          break;
        case 'name[]':
          $(this).append(unchecked_names);
          break;
        case 'real_estate_type[]':
          $(this).append(unchecked_real_estate_types);
          break;
      }
    }
  }).on('select2:select', function () {
    $(this).parent().find('.select2-search__field').val('');
  })
});

$(document).on('click', '#product-info .page-link', function (e) {
  e.preventDefault();
  let url_string = window.location.origin + $(this).attr('href');
  let url = new URL(url_string);
  let page = url.searchParams.get("page");

  $('#product-info .product-search-group #page').val(page);
  $('.product-search-group .btn-search-product').click();
  $('#product-info .product-search-group #page').val(1);
})

$(document).on('click', '.product-search-group .btn-search-product', function (e) {
  showLoading(true);
})

$(document).on('click', '.product-filter-expand-collapse', function () {
  if ($('.product-filter-expand-collapse').parents('.row').find('.filter-expand').hasClass('d-none')) {
    $('.product-filter-expand-collapse').parents('.col-4').css('margin-top', '16px');
    $('.product-filter-expand-collapse').parents('.row').find('.filter-expand').removeClass('d-none');
    if (!$('.product-filter-expand-collapse').parents('.row').find('.filter-expand').first().find('.unable-remove-d-none').length > 0) {
      $('.div-space').removeClass('d-none');
    }
  } else {
    $('.product-filter-expand-collapse').parents('.col-4').css('margin-top', '0');
    $('.product-filter-expand-collapse').parents('.row').find('.filter-expand, .div-space').addClass('d-none');
    $('.div-space').addClass('d-none');
  }
  if ($('.product-filter-text').text() === 'Mở rộng') {
    $('.product-filter-text').text('Thu gọn');
    $('.product-filter-icon').css('transform', 'rotate(0.5turn)');
  } else {
    $('.product-filter-text').text('Mở rộng');
    $('.product-filter-icon').css('transform', 'rotate(0turn)');
    $(this).parents('.col-4').removeClass('mt-3');
  }
});

$(document).on('click', '.btn-remove-filter', function () {
  $('.product-filter-select2, .complete-deals-filter-select2').val(null).trigger('change');
  $("#complete-deals-daterangepicker").val('');
  $(this).parents('.container-fluid').find('.typeahead').val('');
  $(this).parents('.container-fluid').find('input[type=text]').val('');
});

$(document).on('touchmove', 'body', function (e) {
  if ($(this).find('.product-list-mobile').length > 0) {
    if ($(window).scrollTop() + 300 >= ($(document).height() - $(window).height())) {
      let projectId = $('.product-list-mobile').attr('project_id');
      let level = parseInt($('.product-list-mobile').attr('level'));
      let limit = parseInt($('.product-list-mobile').attr('limit'));
      let subdivisionId = [], blockId = [], floorId = [], state = [], name = [], realEstateType = [], productType = [];
      $('.sidebar-filter-checkbox input:checked').each(function () {
        let value = $(this).val();
        switch ($(this).attr('name')) {
          case 'state[]':
            state.push(value);
            break;
          case 'subdivision_id[]':
            subdivisionId.push(value);
            break;
          case 'block_id[]':
            blockId.push(value);
            break;
          case 'floor_id[]':
            floorId.push(value);
            break;
          case 'name[]':
            name.push(value);
            break;
          case 'real_estate_type[]':
            realEstateType.push(value);
            break;
          case 'product_type[]':
            productType.push(value);
            break;
          default:
            break;
        }
      });
      let data = {
        query: $('#product-search').val(),
        'subdivision_id': subdivisionId,
        'block_id': blockId,
        'floor_id': floorId,
        'name': name,
        'real_estate_type': realEstateType,
        'product_type': productType,
        'state': state,
        limit: limit + 20
      }
      let htmlAppend;
      let products;
      if (level === 0) {
        products = $('#nav-high-products')
      } else {
        products = $('#nav-low-products')
      }
      if (limit <= parseInt(products.find('.product-list-mobile').attr('products'))) {
        showLoading(true);
        $.ajax({
          url: `/projects/${projectId}/show_products?level=${level}`,
          type: 'get',
          data: data,
          headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
          },
          success: function (response) {
            if (level === 0) {
              htmlAppend = $(response).find('#nav-high-products')
            } else {
              htmlAppend = $(response).find('#nav-low-products')
            }
            products.html(htmlAppend);
            showLoading(false);
          },
          timeout: 15000
        });
      }
    }
  }
});
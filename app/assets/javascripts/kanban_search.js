// Define the Select2 list
var select2List = {
  'state': {
    title: 'Trạng thái',
    url: '../deals/filter/state',
    width: '98px'
  },
  'source': {
    title: 'Nguồn',
    url: '../deals/filter/source',
    width: '73px'
  },
  'project': {
    title: 'Dự án',
    url: '../deals/filter/project',
    width: '70px'
  },
  'assignee': {
    title: 'Người chăm sóc',
    url: '../deals/filter/assignee',
    width: '147px'
  },
  'label': {
    title: 'Nhãn',
    url: '../deals/filter/label',
    width: '83px'
  },
  'group': {
    title: 'Nhóm kinh doanh',
    url: '../deals/filter/group',
    width: '173px'
  },
  'purchase-purpose': {
    title: 'Mục đích mua',
    url: '../deals/filter/purchase_purpose',
    width: '159px'
  },
  'product-type': {
    title: 'Loại hình sản phẩm',
    url: '../deals/filter/product_type',
    width: '197px'
  },
  'door-direction': {
    title: 'Hướng cửa',
    url: '../deals/filter/door_direction',
    width: '141px'
  },
  'balcony-direction': {
    title: 'Hướng ban công',
    url: '../deals/filter/balcony_direction',
    width: '180px'
  }
};
var firstLoad = true;
var currentFilter = {};

$(document).on('turbolinks:load', function () {
  firstLoad = true;
  var Utils = $.fn.select2.amd.require('select2/utils');
  var Dropdown = $.fn.select2.amd.require('select2/dropdown');
  var DropdownSearch = $.fn.select2.amd.require('select2/dropdown/search');
  var AttachBody = $.fn.select2.amd.require('select2/dropdown/attachBody');
  var dropdownAdapter = Utils.Decorate(Utils.Decorate(Dropdown, DropdownSearch), AttachBody);

  // Set up the Select2 control
  _.forEach(select2List, function (filterValue, filterKey) {
    $(`.select2-search-${filterKey}`).select2({
      ajax: {
        url: filterValue.url,
        dataType: 'json',
        data: function (params) {
          var query = {
            search: params.term
          }
          return query;
        },
        processResults: function (data) {
          data = _.sortBy(data, function (item) {
            return _.includes($(`.select2-search-${filterKey}`).val(), item.id.toString()) ? 0 : 1;
          });
          var dataResults = [];
          _.forEach(data, function (value) {
            if (_.includes($(`.select2-search-${filterKey}`).data('value-checked'), value.id.toString())) {
              value.html = `<input class="icheckbox_square-custom mr-1" type="checkbox" checked="true" id="select2-custom-${filterKey}-${value.id}"><label class="mr-1">${value.text}</label>`;
            } else {
              value.html = `<input class="icheckbox_square-custom mr-1" type="checkbox" id="select2-custom-${filterKey}-${value.id}"><label class="mr-1">${value.text}</label>`;
            }
            value.title = value.text;
            dataResults.push(value);
          });
          return {
            results: dataResults
          };
        },
        cache: true
      },
      theme: 'bootstrap',
      language: 'vi',
      width: 'resolve',
      placeholder: filterValue.title,
      minimumInputLength: 0,
      dropdownAutoWidth: true,
      allowClear: true,
      multiple: true,
      closeOnSelect: false,
      dropdownAdapter: dropdownAdapter,
      escapeMarkup: function (markup) {
        return markup;
      },
      templateResult: function (data) {
        return (data.html == undefined ? data.text : data.html);
      }
    }).on('select2:open', function (e) {
      customStyleSelect2();
      if ($(this).data('state') === 'unselected') {
        $(this).removeData('state');
        $(this).select2('close');
      }
    }).on('change.select2', function (e) {
      if (!firstLoad) submitFilter();
      var searchInline = $(`#${filterKey}-search-deal`).next().find('.select2-search--inline');
      $(`.select2-search-${filterKey}`).val()[0] != undefined ? searchInline.hide() : searchInline.show();

      var optionalFilters = ['label', 'created-at', 'updated-at', 'purchase-purpose', 'product-type', 'door-direction', 'balcony-direction'];
      if (_.includes(optionalFilters, filterKey)) {
        if ($(`.select2-search-${filterKey}`).val()[0] != undefined) {
          $(`.add-filter-class-${optionalFilters.indexOf(filterKey)} ul.select2-selection__rendered`).addClass('hidden');
        } else {
          $(`.add-filter-class-${optionalFilters.indexOf(filterKey)} ul.select2-selection__rendered`).removeClass('hidden');
        }
      }
      // set width
      var changedWidth = $(`.select2-search-${filterKey}`).val().length <= 0 ? select2List[filterKey].width : 'auto'
      $(`.select2-search-${filterKey}`).closest('.form-group').css({width: changedWidth});
    }).on("select2:select", function (e) {
      $(`#select2-custom-${filterKey}-${e.params.data.id}`).prop('checked', true);
    }).on("select2:unselect", function (e) {
      firstLoad = false;
      $(`#select2-custom-${filterKey}-${e.params.data.id}`).prop('checked', false);
      $(`#select2-custom-${filterKey}-${e.params.data.id}`).parent().removeClass('select2-results__option--highlighted').attr('aria-selected', false);
      $(`.select2-search-${filterKey} option[value="${e.params.data.id}"]`).remove();
      $(`.select2-search-${filterKey}`).trigger('change.select2');
    }).on("select2:clear", function (e) {
      var optionalFilters = ['label', 'created-at', 'updated-at', 'purchase-purpose', 'product-type', 'door-direction', 'balcony-direction'];
      if (_.includes(optionalFilters, filterKey)) {
        $(`.add-filter-class-${optionalFilters.indexOf(filterKey)}`).hide();
        $('.select2-search-filter').val(_.reject($('.select2-search-filter').val(), a => a == optionalFilters.indexOf(filterKey))).trigger('change');
      }
    }).on("select2:unselecting", function (e) {
      $(this).data('state', 'unselected');
      firstLoad = true;
    });
  });

  $('.select2-search-filter').select2({
    ajax: {
      url: '../deals/filter/add',
      dataType: 'json',
      data: function (params) {
        var query = {
          search: params.term
        }
        return query;
      },
      processResults: function (data) {
        data = _.sortBy(data, function (item) {
          return _.includes($('.select2-search-filter').val(), item.id.toString()) ? 0 : 1;
        });
        customerArr = data;
        var dataResults = [];
        _.forEach(data, function (value) {
          if (_.includes($('.select2-search-filter').val(), value.id.toString())) {
            value.html = `<input class="icheckbox_square-custom mr-1" type="checkbox" checked="true" id="select2-custom-filter-${value.id}"><label class="mr-1">${value.text}</label>`;
          } else {
            value.html = `<input class="icheckbox_square-custom mr-1" type="checkbox" id="select2-custom-filter-${value.id}"><label class="mr-1">${value.text}</label>`;
          }
          value.title = value.text;
          dataResults.push(value);
        });
        return {
          results: dataResults
        };
      },
      cache: true
    },
    theme: 'bootstrap',
    language: 'vi',
    placeholder: 'Thêm bộ lọc',
    minimumInputLength: 0,
    dropdownAutoWidth: true,
    allowClear: true,
    multiple: true,
    dropdownAdapter: dropdownAdapter,
    escapeMarkup: function (markup) {
      return markup;
    },
    templateResult: function (data) {
      return (data.html == undefined ? data.text : data.html);
    },
    templateSelection: function (data) {
      return ($('.select2-search-filter').val()[1] == data.id ? '...' : data.text);
    }
  }).on('select2:open', function (e) {
    customStyleSelect2(true);
  }).on('change.select2', function (e) {
    if (!firstLoad) submitFilter();
    var searchInline = $('#filter-search-deal').next().find('.select2-search--inline');
    $('.select2-search-filter').val()[0] != undefined ? searchInline.hide() : searchInline.show();
    $(this).select2('close');
    $('.btn-clear-filter').show();
  }).on("select2:select", function (e) {
    $(`#select2-custom-filter-${e.params.data.id}`).prop('checked', true);
    $(`.add-filter-class-${e.params.data.id}`).show();
  }).on("select2:unselect", function (e) {
    $(`.add-filter-class-${e.params.data.id}`).hide();
    $(`.add-filter-class-${e.params.data.id} select`).val(null).trigger('change');
    $(`#select2-custom-filter-${e.params.data.id}`).prop('checked', false);
    $(`#select2-custom-filter-${e.params.data.id}`).parent().removeClass('select2-results__option--highlighted').attr('aria-selected', false);
    $('.select2-search-filter option[value="' + e.params.data.id + '"]').remove();

    if (e.params.data.id == 3) {
      resetDaterange('created-at');
    } else if (e.params.data.id == 4) {
      resetDaterange('updated-at');
    }
    if (!firstLoad) submitFilter();
  }).on("select2:close", function (e) {
    $('.btn-add-filter').css({'background-color': 'transparent', 'color': '#007bff'});
  });

  if ($('.kanban-search-group').length > 0) {
    // Fetch the preselected item, and add to the control
    $.ajax({
      type: 'GET',
      url: '../deals/filter/stored'
    }).then(function (data) {
      if (data.filter != undefined && data.filter.length > 0) {
        // create the option and append to Select2
        _.forEach(data.filter, function (result) {
          var option = new Option(result.text, result.id, true, true);
          $('.select2-search-filter').append(option).trigger('change');

          // manually trigger the `select2:select` event
          $('.select2-search-filter').trigger({
            type: 'select2:select',
            params: {
              data: result
            }
          });
        })
      }

      _.forEach(select2List, function (filterValue, filterKey) {
        if (data[filterKey] != undefined && data[filterKey].length > 0) {
          // create the option and append to Select2
          _.forEach(data[filterKey], function (result) {
            var option = new Option(result.text, result.id, true, true);
            $(`.select2-search-${filterKey}`).append(option).trigger('change');

            // manually trigger the `select2:select` event
            $(`.select2-search-${filterKey}`).trigger({
              type: 'select2:select',
              params: {
                data: result
              }
            });
          })
        }
      });

      firstLoad = false;

      $('#created-at-search-deal').daterangepicker({
        autoUpdateInput: false,
        minYear: 2019,
        maxYear: 2119,
        locale: {
          format: 'DD/MM/YYYY'
        }
      }).on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        $(this).css({width: '190px'});
        submitFilter();
      });

      $('#updated-at-search-deal').daterangepicker({
        autoUpdateInput: false,
        minYear: 2019,
        maxYear: 2119,
        locale: {
          format: 'DD/MM/YYYY'
        }
      }).on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        $(this).css({width: '190px'});
        submitFilter();
      });

      if (data['created-at'] != undefined && data['created-at'] != null && data['created-at'] != '') {
        $('#created-at-search-deal').val(data['created-at']);
        var createdRange = data['created-at'].split(' - ');
        $('#created-at-search-deal').data('daterangepicker').setStartDate(createdRange[0]);
        $('#created-at-search-deal').data('daterangepicker').setEndDate(createdRange[1]);
        $('#created-at-search-deal').css({width: '190px'});
      }

      if (data['updated-at'] != undefined && data['updated-at'] != null && data['updated-at'] != '') {
        $('#updated-at-search-deal').val(data['updated-at']);
        var updatedRange = data['updated-at'].split(' - ');
        $('#updated-at-search-deal').data('daterangepicker').setStartDate(updatedRange[0]);
        $('#updated-at-search-deal').data('daterangepicker').setEndDate(updatedRange[1]);
        $('#updated-at-search-deal').css({width: '190px'});
      }

      currentFilter = data;
      if (_.isEmpty(currentFilter)) {
        initCurrentFilter();
      } else {
        _.forEach(currentFilter, function (val, k) {
          if (!_.includes(['updated-at', 'created-at'], k)) {
            currentFilter[k] = _.filter(currentFilter[k], a => a.id != 'undefined').map(e => e.id);
          }
        })
      }
    });
  }

  let state = $('#cs-ticket-deals-search').attr('state');

  var dealNameSuggestion = new Bloodhound({
    limit: 10,
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: `../deals/autocomplete?type=name&state=${state}&query=%QUERY`,
      filter: function (data) {
        return data;
      },
      wildcard: "%QUERY"
    }
  });

  var dealCustomerNameSuggestion = new Bloodhound({
    limit: 10,
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: '../deals/autocomplete?type=customer_name&query=%QUERY',
      filter: function (data) {
        return data;
      },
      wildcard: "%QUERY"
    }
  });

  dealCustomerNameSuggestion.initialize();

  var dealCustomerPhoneSuggestion = new Bloodhound({
    limit: 10,
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: `../deals/autocomplete?type=customer_phone&state=${state}&query=%QUERY`,
      filter: function (data) {
        return data;
      },
      wildcard: "%QUERY"
    }
  });

  dealCustomerPhoneSuggestion.initialize();

  var productCodeSuggestion = new Bloodhound({
    limit: 10,
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: `../deals/autocomplete?type=code&state=${state}&query=%QUERY`,
      filter: function (data) {
        return data;
      },
      wildcard: "%QUERY"
    }
  });

  productCodeSuggestion.initialize();

  var dealProjectNameSuggestion = new Bloodhound({
    limit: 10,
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: `../deals/autocomplete?type=project_name&state=${state}&query=%QUERY`,
      filter: function (data) {
        return data;
      },
      wildcard: "%QUERY"
    }
  });

  dealProjectNameSuggestion.initialize();

  var dealTypeahead = $('#deal-search');
  if (isMobile) {
    dealTypeahead.typeahead({
        hint: true,
        highlight: true,
        minLength: 1
      },
      {
        limit: 500,
        name: 'value',
        displayKey: 'value',
        source: dealNameSuggestion.ttAdapter(),
      }).on('typeahead:selected', function ($e, data) {
      if (isMobile) {
        window.location.href = window.location.origin + data.path;
      } else {
        if (data != undefined) $('#search_type').val(data.type);
        submitFilter();
      }
    });
  } else {
    dealTypeahead.typeahead({
        hint: true,
        highlight: true,
        minLength: 1
      },
      {
        limit: 500,
        name: 'value',
        displayKey: 'value',
        source: dealProjectNameSuggestion.ttAdapter(),
        templates: {
          header: '<h5 class="group-name">Tên dự án</h5>'
        }
      },
      {
        limit: 500,
        name: 'value',
        displayKey: 'value',
        source: dealNameSuggestion.ttAdapter(),
        templates: {
          header: '<h5 class="group-name">Tên giao dịch</h5>'
        }
      },
      {
        limit: 500,
        name: 'value',
        displayKey: 'value',
        source: dealCustomerPhoneSuggestion.ttAdapter(),
        templates: {
          header: '<h5 class="group-name">Số điện thoại khách hàng</h5>'
        }
      }).on('typeahead:selected', function ($e, data) {
      if (isMobile) {
        window.location.href = window.location.origin + data.path;
      } else {
        if (data != undefined) $('#search_type').val(data.type);
        submitFilter();
      }
    });
  }

  var completeDealsTypeahead = $('#complete-deals-search');
  completeDealsTypeahead.typeahead({
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      limit: 500,
      name: 'value',
      displayKey: 'value',
      source: productCodeSuggestion.ttAdapter(),
      templates: {
        header: '<h5 class="group-name">Mã SP</h5>'
      }
    },
    {
      limit: 500,
      name: 'value',
      displayKey: 'value',
      source: dealCustomerPhoneSuggestion.ttAdapter(),
      templates: {
        header: '<h5 class="group-name">Số điện thoại khách hàng</h5>'
      }
    })

  var csTickets = $('#cs-ticket-deals-search');
  csTickets.typeahead({
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      limit: 500,
      name: 'value',
      displayKey: 'value',
      source: dealProjectNameSuggestion.ttAdapter(),
      templates: {
        header: '<h5 class="group-name">Tên dự án</h5>'
      }
    },
    {
      limit: 500,
      name: 'value',
      displayKey: 'value',
      source: dealNameSuggestion.ttAdapter(),
      templates: {
        header: '<h5 class="group-name">Tên giao dịch</h5>'
      }
    },
    {
      limit: 500,
      name: 'value',
      displayKey: 'value',
      source: dealCustomerPhoneSuggestion.ttAdapter(),
      templates: {
        header: '<h5 class="group-name">Số điện thoại khách hàng</h5>'
      }
    })
});

$(document).on('keyup', '#deal-search', function (event) {
  if (event.keyCode == 13) {
    $('#deal-search').typeahead('close');
    submitFilter();
  }
})

$(document).on('click', '.btn-add-filter', function () {
  $('#filter-search-deal').select2('open');
  $(this).css({'background-color': '#3ac5c9', 'color': 'white'});
})

$(document).on('change', '#deal-search', function () {
  $('#search_type').val('');
})

$(document).on('click', 'ul.select2-selection__rendered', function (e) {
  if (!$(this).hasClass('hidden') && e.offsetX > $(this).width()) {
    var formGroup = $(this).closest('.form-group');
    var select2Item = formGroup.find('select');
    if (select2Item != undefined) {
      formGroup.hide();
      select2Item.select2('close').trigger('select2:clear');
    }
  }
})

$(document).on('input', '#deal-search', function () {
  if ($(this).val() != '') {
    $('.search-icon').hide();
  } else {
    $('.search-icon').show();
  }
})

$(document).on('click', '.fa-created-at', function () {
  $('.add-filter-class-1').hide();
  resetDaterange('created-at');

  $(`#select2-custom-filter-1`).prop('checked', false);
  $(`.select2-search-filter option[value="1"]`).remove();
  $(`.select2-search-filter`).trigger('change.select2');
})

$(document).on('click', '.fa-updated-at', function () {
  $('.add-filter-class-2').hide();
  resetDaterange('updated-at');

  $(`#select2-custom-filter-2`).prop('checked', false);
  $(`.select2-search-filter option[value="2"]`).remove();
  $(`.select2-search-filter`).trigger('change.select2');
})

$(document).on('click', '.search-button', function () {
  showLoading(true);
})

$(document).on('click', '.btn-clear-filter', function () {
  showLoading(true);
  initCurrentFilter();
})

var customStyleSelect2 = function (isAddFilter) {
  $('.select2-search--dropdown input.select2-search__field').prop('placeholder', 'Tìm kiếm');
  if (isAddFilter) {
    $('.select2-container.select2-container--open:not(.select2)').css({
      'margin-top': '25px',
      'margin-left': '5px',
      'border-top': 'solid 2px #3ac5c9'
    });
  } else {
    $('.select2-container.select2-container--open:not(.select2)').css({
      'margin-top': '5px',
      'border-top': 'solid 2px #3ac5c9'
    });
  }
  $('.select2-container.select2-container--open .select2-dropdown').css({'border-radius': '0'});
  $('.select2-search--dropdown').css({'padding': '0'});
  $('.select2-container:not(.select2) .select2-search__field').css({
    'border-radius': '0',
    'border': '0',
    'border-bottom': 'solid 1px #3ac5c9',
    'height': '34px',
    'padding-left': '12px'
  });
}

var submitFilter = function () {
  var newFilter = {
    'assignee': rejectUndefined('#assignee-search-deal'),
    'balcony-direction': rejectUndefined('#balcony-direction-search-deal'),
    'created-at': $('#created-at-search-deal').val(),
    'door-direction': rejectUndefined('#door-direction-search-deal'),
    'group': rejectUndefined('#group-search-deal'),
    'label': rejectUndefined('#label-search-deal'),
    'product-type': rejectUndefined('#product-type-search-deal'),
    'project': rejectUndefined('#project-search-deal'),
    'purchase-purpose': rejectUndefined('#purchase-purpose-search-deal'),
    'source': rejectUndefined('#source-search-deal'),
    'state': rejectUndefined('#state-search-deal'),
    'updated-at': $('#updated-at-search-deal').val()
  }

  var filterChanged = false;
  _.forEach(newFilter, function (val, k) {
    var diff = false;
    if (_.includes(['updated-at', 'created-at'], k)) {
      diff = currentFilter[k] != val;
    } else {
      diff = _.differenceWith(val, currentFilter[k], _.isEqual).length > 0 || currentFilter[k].length != val.length;
    }
    if (diff) {
      filterChanged = true;
      currentFilter = newFilter;
      return false;
    }
  })

  if (filterChanged) {
    if ($('.kanban-search-group').length > 0 && !isMobile) showLoading(true);
    $('.kanban-search-group .search-button').click();
  }
}

var rejectUndefined = function (elm) {
  return _.filter($(elm).val(), e => e != "undefined");
}

var showLoading = function (isShow) {
  if (isShow) {
    $('div.spanner').addClass('show');
    $('div.overlay').addClass('show');
  } else {
    $('div.spanner').removeClass('show');
    $('div.overlay').removeClass('show');
  }
  setTimeout(function () {
    $('div.spanner').removeClass('show');
    $('div.overlay').removeClass('show');
  }, 30000);
}

var showImporting = function (isShow) {
  if (isShow) {
    $('div.spanner').addClass('show');
    $('div.overlay').addClass('show');
    $('div.spanner p').text('Importing...');
  } else {
    $('div.spanner').removeClass('show');
    $('div.overlay').removeClass('show');
  }
}

var resetDaterange = function (className) {
  $(`#${className}-search-deal`).val('');
  $(`#${className}-search-deal`).data('daterangepicker').setStartDate(moment());
  $(`#${className}-search-deal`).data('daterangepicker').setEndDate(moment());
  var changedWidth = className == 'created-at' ? '107px' : '146px';
  $(`#${className}-search-deal`).css({width: changedWidth});
}

var initCurrentFilter = function () {
  currentFilter = {
    'assignee': [],
    'balcony-direction': [],
    'created-at': "",
    'door-direction': [],
    'group': [],
    'label': [],
    'product-type': [],
    'project': [],
    'purchase-purpose': [],
    'source': [],
    'state': [],
    'updated-at': ""
  }
}

var userAgent = navigator.userAgent.toLowerCase();
var iOS = /(ipad|iphone|ipod)/g.test(userAgent) || (navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1);
var android = /(android)/g.test(userAgent);
var isMobile = iOS || android;
if (!isMobile) {
  $(document).on('change', 'input[type=radio][name=order_by]', function () {
    $(this).closest('form').submit();
  });

  $(document).on('click', '.btn-sort-deal', function () {
    if ($(this).val() == 'ASC') {
      $('#sort').val('DESC');
    } else {
      $('#sort').val('ASC');
    }
    $(this).closest('form').submit();
  });

  $('.complete-deals-daterangepicker').daterangepicker({
    autoUpdateInput: false,
    minYear: 2019,
    maxYear: 2119,
    locale: {
      format: 'DD/MM/YYYY'
    }
  }).on('apply.daterangepicker', function (ev, picker) {
    $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
  });
}


$(document).on('turbolinks:load', function() {
  var dealNameSuggestion = new Bloodhound({
    limit: 10,
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: '../same_deals/autocomplete?query=%QUERY',
      filter: function (data) {
        return data;
      },
      wildcard: "%QUERY"
    }
  });

  dealNameSuggestion.initialize();

  var dealTypeahead = $('#same-deal-search');
  dealTypeahead.typeahead({
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      limit: 500,
      name: 'value',
      displayKey: 'value',
      source: dealNameSuggestion.ttAdapter(),
      templates: {
        empty: '<div class="noitems">Không tìm thấy kết quả</div>'
      }
    });

  // Same customers search
  var customerNameSuggestion = new Bloodhound({
    limit: 10,
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: '../same_customers/autocomplete?query=%QUERY',
      filter: function (data) {
        return data;
      },
      wildcard: "%QUERY"
    }
  });

  customerNameSuggestion.initialize();

  var customerTypeahead = $('#same-customer-search');
  customerTypeahead.typeahead({
    hint: true,
    highlight: true,
    minLength: 1
  },
  {
    limit: 500,
    name: 'value',
    displayKey: 'value',
    source: customerNameSuggestion.ttAdapter(),
    templates: {
      empty: '<div class="noitems">Không tìm thấy kết quả</div>'
    }
  });
});

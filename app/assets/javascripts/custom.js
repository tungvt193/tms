$(document).on('turbolinks:load', function () {
  function custom_search(url_search, input_id) {
    let customerSuggestion = new Bloodhound({
      limit: 10,
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      remote: {
        url: url_search,
        filter: function (data) {
          return data;
        },
        wildcard: "%QUERY"
      }
    });
    customerSuggestion.initialize();

    let customerTypeahead = $(input_id);
    customerTypeahead.typeahead({
        hint: true,
        highlight: true,
        minLength: 1
      },
      {
        limit: 500,
        name: 'value',
        displayKey: 'value',
        source: customerSuggestion.ttAdapter(),
        templates: {
          empty: '<div class="noitems">Không tìm thấy kết quả</div>'
        }
      });
  }

  if ($('.general-search-input').length) {
    let url_autocomplete = $('.general-search-input').attr('url-autocomplete');
    custom_search(`${url_autocomplete}?query=%QUERY`, '.general-search-input');
  }

  //sidebar
  if ($('.navbar-nav .nav-link').click(function () {
    $('.logo-reti').toggle();
    $('.logo-collapse').toggle();
    if ($('.main-sidebar').mouseenter( function () {
      $('.logo-reti').show();
      $('.logo-collapse').hide();
    }).mouseleave( function () {
      $('.logo-reti').toggle();
      $('.logo-collapse').toggle();
    }));
  }));

  if ($('.main-sidebar').mouseenter( function () {
    $('.logo-collapse').hide();
    $('.logo-reti').show();
  }).mouseleave( function () {
    $('.logo-collapse').show();
    $('.logo-reti').hide();
  }));

  $('.logo-collapse').show();
  $('.logo-reti').hide();

  if ($('.main-sidebar').width() == 250) {
    $('.logo-collapse').hide();
    $('.logo-reti').show();
  }
  // formatCurrency
  if ($("input[data-type='currency']").length && $("input[data-type='currency']").val()) {
    $("input[data-type='currency']").each(function () {
      formatCurrency($(this));
    });
  }
  // submit form has currency
  if ($(".form-has-currency").length) {
    $(".form-has-currency").submit(function () {
      // currency
      $(this).find("input[data-type='currency']").each(function () {
        let price = $(this).val();
        if (price) {
          $(this).val(stringToNumber(price));
        }
      });
      // number
      $(this).find("input[data-type='number']").each(function () {
        let price = $(this).val();
        if (price) {
          $(this).val(stringCommaToDot(price));
        }
      });
    });
  }

  if ($('.select-ticket-type')) {
    select_display_and_submit($('.select-ticket-type'));
  }

  $('#user-logout').click(function (e) {
    e.preventDefault();
    swalWithBootstrapButtons.fire({
      title: 'Bạn có chắc chắn muốn đăng xuất?',
      showCancelButton: true,
      confirmButtonText: 'Đăng xuất',
      cancelButtonText: 'Hủy',
      reverseButtons: true,
      allowOutsideClick: false
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          url: $('#user-logout').data('logout'),
          type: "DELETE",
          success: function () {
            window.location.href = $('#user-logout').data('login');
          }
        });
      }
    })
  })
  // Same deal index
  $('.same-deal-detail-row').click(function () {
    window.location.href = $(this).find('.same-deal-detail-link').attr('href');
  })
  // dataTables
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    $.fn.dataTable.tables({visible: true, api: true}).columns.adjust();
  });
  $.fn.DataTable.ext.pager.numbers_length = 5;
  document.addEventListener("turbolinks:before-cache", function () {
    lowProductDatatable.destroy();
    highProductDatatable.destroy();
    viewHighProductDatatable.destroy();
    viewLowProductDatatable.destroy();
    completeDealsDatatable.destroy();
    tmsDatatable.destroy();
    tmsDatatableScroll.destroy();
  });
  // low products
  var lowProductDatatable = $('#low-product-table').DataTable({
    "bInfo": false,
    "lengthChange": false,
    "searching": false,
    "scrollX": true,
    "autoWidth": true,
    responsive: true,
    "fixedHeader": {
      "header": false,
    },
    "columns": [
      {"width": "78px", "targets": 0},
      {"width": "81px", "targets": 1},
      {"width": "105px", "targets": 2},
      {"width": "75px", "targets": 3},
      {"width": "75px", "targets": 4},
      {"width": "75px", "targets": 5},
      {"width": "115px", "targets": 6},
      {"width": "156px", "targets": 7},
      {"width": "173px", "targets": 8},
      {"width": "310px", "targets": 9},
      {"width": "173px", "targets": 10},
      {"width": "150px", "targets": 11},
      {"width": "75px", "targets": 12},
    ],
    "language": {
      "decimal": ",",
      "thousands": ".",
      "emptyTable": "Không có dữ liệu!",
      "search": "Tìm kiếm:",
      "info": "_START_ - _END_ trong _TOTAL_ kết quả",
      "infoEmpty": "0 trên 0 kết quả",
      "infoFiltered": "(được lọc từ _MAX_ kết quả)",
      "lengthMenu": "Hiển thị _MENU_ kết quả",
      "zeroRecords": "Không tìm thấy kết quả!",
      "paginate": {
        "previous": '<i class="fas fa-angle-left"></i>',
        "next": '<i class="fas fa-angle-right"></i>'
      }
    }
  });
  // high products
  var highProductDatatable = $('#high-product-table').DataTable({
    "bInfo": false,
    "lengthChange": false,
    "searching": false,
    "scrollX": true,
    "autoWidth": true,
    responsive: true,
    "fixedHeader": {
      "header": false,
    },
    "columns": [
      {"width": "76px", "targets": 0},
      {"width": "83px", "targets": 1},
      {"width": "107px", "targets": 2},
      {"width": "75px", "targets": 3},
      {"width": "76px", "targets": 4},
      {"width": "35px", "targets": 5},
      {"width": "90px", "targets": 6},
      {"width": "95px", "targets": 7},
      {"width": "75px", "targets": 8},
      {"width": "113px", "targets": 9},
      {"width": "127px", "targets": 10},
      {"width": "188px", "targets": 11},
      {"width": "83px", "targets": 12},
      {"width": "83px", "targets": 13},
      {"width": "83px", "targets": 14},
    ],
    "language": {
      "decimal": ",",
      "thousands": ".",
      "emptyTable": "Không có dữ liệu!",
      "search": "Tìm kiếm:",
      "info": "_START_ - _END_ trong _TOTAL_ kết quả",
      "infoEmpty": "0 trên 0 kết quả",
      "infoFiltered": "(được lọc từ _MAX_ kết quả)",
      "lengthMenu": "Hiển thị _MENU_ kết quả",
      "zeroRecords": "Không tìm thấy kết quả!",
      "paginate": {
        "previous": '<i class="fas fa-angle-left"></i>',
        "next": '<i class="fas fa-angle-right"></i>'
      }
    }
  });
  // view high products
  var viewHighProductDatatable = $('#view-high-product-table').DataTable({
    "bInfo": false,
    "lengthChange": false,
    "searching": false,
    "scrollX": true,
    "autoWidth": true,
    responsive: true,
    "fixedHeader": {
      "header": false,
    },
    "columns": [
      {"width": "76px", "targets": 0},
      {"width": "83px", "targets": 1},
      {"width": "107px", "targets": 2},
      {"width": "76px", "targets": 3},
      {"width": "76px", "targets": 4},
      {"width": "35px", "targets": 5},
      {"width": "45px", "targets": 6},
      {"width": "95px", "targets": 7},
      {"width": "75px", "targets": 8},
      {"width": "113px", "targets": 9},
      {"width": "127px", "targets": 10},
      {"width": "188px", "targets": 11},
      {"width": "252px", "targets": 12}
    ],
    "language": {
      "decimal": ",",
      "thousands": ".",
      "emptyTable": "Không có dữ liệu!",
      "search": "Tìm kiếm:",
      "info": "_START_ - _END_ trong _TOTAL_ kết quả",
      "infoEmpty": "0 trên 0 kết quả",
      "infoFiltered": "(được lọc từ _MAX_ kết quả)",
      "lengthMenu": "Hiển thị _MENU_ kết quả",
      "zeroRecords": "Không tìm thấy kết quả!",
      "paginate": {
        "previous": '<i class="fas fa-angle-left"></i>',
        "next": '<i class="fas fa-angle-right"></i>'
      }
    }
  });
  // view low products
  var viewLowProductDatatable = $('#view-low-product-table').DataTable({
    "bInfo": false,
    "lengthChange": false,
    "searching": false,
    "scrollX": true,
    "autoWidth": true,
    responsive: true,
    "fixedHeader": {
      "header": false,
    },
    "columns": [
      {"width": "78px", "targets": 0},
      {"width": "81px", "targets": 1},
      {"width": "105px", "targets": 2},
      {"width": "75px", "targets": 3},
      {"width": "75px", "targets": 4},
      {"width": "75px", "targets": 5},
      {"width": "115px", "targets": 6},
      {"width": "156px", "targets": 7},
      {"width": "173px", "targets": 8},
      {"width": "310px", "targets": 9},
    ],
    "language": {
      "decimal": ",",
      "thousands": ".",
      "emptyTable": "Không có dữ liệu!",
      "search": "Tìm kiếm:",
      "info": "_START_ - _END_ trong _TOTAL_ kết quả",
      "infoEmpty": "0 trên 0 kết quả",
      "infoFiltered": "(được lọc từ _MAX_ kết quả)",
      "lengthMenu": "Hiển thị _MENU_ kết quả",
      "zeroRecords": "Không tìm thấy kết quả!",
      "paginate": {
        "previous": '<i class="fas fa-angle-left"></i>',
        "next": '<i class="fas fa-angle-right"></i>'
      }
    }
  });

  // complete deals
  var completeDealsDatatable = $('#complete-deals-table').DataTable({
    "bInfo": false,
    "lengthChange": false,
    "searching": false,
    "scrollX": true,
    "autoWidth": true,
    "responsive": true,
    "fixedHeader": {
      "header": false,
    },
    "ordering": true,
    "orderMulti": true,
    "order": [[5, 'desc']],
    "columns": [
      {"width": "20%", "targets": 0},
      {"width": "10%", "targets": 1},
      {"width": "8%", "targets": 2},
      {"width": "6%", "targets": 3},
      {"width": "15%", "targets": 4},
      {"width": "7%", "targets": 5},
      {"width": "9%", "targets": 6},
      {"width": "9%", "targets": 7},
      {"width": "9%", "targets": 8},
      {"width": "9%", "targets": 9},
    ],
    "language": {
      "decimal": ",",
      "thousands": ".",
      "emptyTable": "Không có dữ liệu!",
      "search": "Tìm kiếm:",
      "info": "_START_ - _END_ trong _TOTAL_ kết quả",
      "infoEmpty": "0 trên 0 kết quả",
      "infoFiltered": "(được lọc từ _MAX_ kết quả)",
      "lengthMenu": "Hiển thị _MENU_ kết quả",
      "zeroRecords": "Không tìm thấy kết quả!",
      "paginate": {
        "previous": '<i class="fas fa-angle-left"></i>',
        "next": '<i class="fas fa-angle-right"></i>'
      }
    }
  });

  var tmsDatatable = $('.tms-datatables').DataTable({
    "bInfo": false,
    "lengthChange": false,
    "searching": false,
    "autoWidth": true,
    responsive: true,
    "fixedHeader": {
      "header": false,
    },
    "language": {
      "decimal": ",",
      "thousands": ".",
      "emptyTable": "Không có dữ liệu!",
      "search": "Tìm kiếm:",
      "info": "_START_ - _END_ trong _TOTAL_ kết quả",
      "infoEmpty": "0 trên 0 kết quả",
      "infoFiltered": "(được lọc từ _MAX_ kết quả)",
      "lengthMenu": "Hiển thị _MENU_ kết quả",
      "zeroRecords": "Không tìm thấy kết quả!",
      "paginate": {
        "previous": '<i class="fas fa-angle-left"></i>',
        "next": '<i class="fas fa-angle-right"></i>'
      }
    }
  });

  var tmsDatatableScroll = $('.tms-datatables-scroll').DataTable({
    "bInfo": false,
    "lengthChange": false,
    "searching": false,
    "scrollX": true,
    "autoWidth": true,
    responsive: true,
    "fixedHeader": {
      "header": false,
    },
    "language": {
      "decimal": ",",
      "thousands": ".",
      "emptyTable": "Không có dữ liệu!",
      "search": "Tìm kiếm:",
      "info": "_START_ - _END_ trong _TOTAL_ kết quả",
      "infoEmpty": "0 trên 0 kết quả",
      "infoFiltered": "(được lọc từ _MAX_ kết quả)",
      "lengthMenu": "Hiển thị _MENU_ kết quả",
      "zeroRecords": "Không tìm thấy kết quả!",
      "paginate": {
        "previous": '<i class="fas fa-angle-left"></i>',
        "next": '<i class="fas fa-angle-right"></i>'
      }
    }
  });

  $('.input-decimal').inputmask({
    rightAlign: false,
    removeMaskOnSubmit: true,
  });

  $('.input-price-decimal').inputmask({
    rightAlign: false,
    removeMaskOnSubmit: true,
    groupSeparator: '.',
    radixPoint: '',
    digit: 3,
    placeholder: ""
  });

  if ($('.first-col-stick').length > 0) {
    let first_col_stick_width = $('.first-col-stick').outerWidth();
    if (window.location.href.includes("products?level=1")) {
      first_col_stick_width = $('.first-col-stick').eq(2).outerWidth();
    }
    $('.second-col-stick').css('left', first_col_stick_width + 'px');
    $('.table-hover tbody tr').each(function () {
      $(this).mouseenter(function () {
        $(this).find('.first-col-stick, .second-col-stick, .last-col-stick').css('background-color', '#ededed');
      }).mouseleave(function () {
        $(this).find('.first-col-stick, .second-col-stick, .last-col-stick').css('background-color', 'white');
      })
    })
  }

  $('.table-hover tbody tr td').each(function () {
    if ($(this).find('a.btn-outline-primary').length == 0 && !$('.table-hover').hasClass('complete-deal-table') && !$('.table-hover').hasClass('unable-click') && !$('.table-hover').hasClass('dnd-table')) {
      $(this).click(function () {
        $(this).parent().find('.btn-outline-primary')[0].click();
      })
    }
  })

  $('#import-news-form #files-field-news').dropzone({
    url: location.pathname + '/import',
    dictDefaultMessage: 'Bạn có thể kéo hoặc thả tài liệu để tải lên',
    addRemoveLinks: true,
    autoProcessQueue: false,
    uploadMultiple: false,
    maxFiles: 1,
    acceptedFiles: ".csv",
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },
    accept: function (file, done) {
      $('#import-news-form #files-field-news').css({'height': 'auto'});
      done();
    },
    init: function () {
      let newsDropzone = this;
      let form = document.getElementById('import-news-form');
      form.addEventListener('submit', function (event) {
        if (newsDropzone.getQueuedFiles().length > 0) {
          event.preventDefault();
          event.stopPropagation();
          newsDropzone.processQueue();
        }
      });
      newsDropzone.on('sending', function (file, xhr, formData) {
        console.log(formData)
      });
      newsDropzone.on('success', function () {
        location.reload();
      });
    }
  });

  var instructionDocumentConfigs = [], instructionDocumentUrls = [];
  $('#instruction-document-input').fileinput({
    initialPreview: instructionDocumentUrls,
    initialPreviewAsData: true,
    initialPreviewConfig: instructionDocumentConfigs,
    overwriteInitial: false,
    allowedFileTypes: ['pdf'],
    showCancel: false,
    showRemove: false,
    browseLabel: '',
    removeClass: 'btn btn-danger',
    removeLabel: '',
    theme: 'fas',
    maxFileCount: 5,
    deleteUrl: '/',
    ajaxDeleteSettings: {
      headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
      }
    },
    fileActionSettings: {
      showZoom: function (config) {
        return config.type === 'pdf';
      }
    }
  }).on('fileloaded', function () {
    let filePreview = $(this).closest('.file-input.theme-fas');
    if (filePreview.hasClass('has-error')) {
      filePreview.removeClass('has-error')
      filePreview.find('.kv-fileinput-error.file-error-message').hide().empty();
      filePreview.find('.kv-fileinput-caption.file-caption').removeClass('is-invalid');
      filePreview.find('.file-caption-icon').remove();
    }
  }).on('filebeforedelete', function () {
    var aborted = !window.confirm('Bạn có chắc muốn xóa file này?');
    return aborted;
  }).on('filedeleted', function () {
    setTimeout(function () {
      window.alert('Xóa file thành công! ' + krajeeGetCount('file-5'));
    }, 900);
  });
});


$(document).on('click', '.notification-item', function (e) {
  if ($(this).find('.notification-upload-unc').length > 0) {
    e.preventDefault();
    var productLockHistoryId = $(this).find('.notification-upload-unc').attr('product_lock_history');
    $.ajax({
      method: 'GET',
      url: 'dashboard/unc_product_lock_history',
      type: 'JSON',
      data: {
        id: productLockHistoryId
      },
      headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
      },
      success: function (response) {
        if ($(response).find('#product_lock_history_confirmed').length > 0) {
          var url = window.location.origin + '/product_lock_histories';
          window.open(url, '_blank')
        } else {
          var container = $(response).find('.product-lock-history-container');
          $('body').append(container);
          container.find('.show-product-lock-history').click();
        }
      }
    })
  }
})

$(document).on('click', '#product-submit', function () {
  if ($(this).text() === 'Lưu') {
    $('#edit-product-form').removeAttr('data-remote').removeData('remote').submit();
  }
});

// convert currency
$(document).on('keyup', "input[data-type='currency']", function () {
  formatCurrency($(this));
});

function formatNumber(n) {
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")
}

function formatCurrency(input, blur) {
  let input_val = input.val();
  if (input_val === "") {
    return;
  }
  let original_len = input_val.length;
  let caret_pos = input.prop("selectionStart");
  if (input_val.indexOf(",") >= 0) {
    let decimal_pos = input_val.indexOf(",");
    let left_side = input_val.substring(0, decimal_pos);
    left_side = formatNumber(left_side);
    input_val = left_side;
  } else {
    input_val = formatNumber(input_val);
    input_val = input_val;
  }
  // send updated string to input
  input.val(input_val);
  // put caret back in the right position
  let updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}

function stringToNumber(str) {
  return parseFloat(str.toString().split('.').join("").replace(/,/g, '.'));
}

// number with comma
function formatNumberComma(input) {
  let currentInput = input.val();
  let fixedInput = currentInput.replace(/[^0-9 \,]/g, '');
  input.val(fixedInput);
}

$(document).on('keyup', "input[data-type='number']", function (e) {
  if (e.key != "Enter") {
    formatNumberComma($(this));
  }
});

// number with comma -> dot
function stringCommaToDot(str) {
  return parseFloat(str.toString().replace(/,/g, '.'));
}

// ---- end convert currency ----

function select_display_and_submit(select) {
  select.on('change', function () {
    let block_show = $('.' + $(this).val().toLowerCase() + '-select');
    block_show.siblings().addClass('d-none');
    block_show.parent().removeClass('d-none');
    block_show.removeClass('d-none');
  })

  select.closest('form').on('submit', function () {
    $('.display-block').find('.d-none').remove();
  })
}

// notification
function loadResults() {
  let offset = $('#notification-list .notification-item.unread').length;
  $.ajax({
    url: "/notifications/load_more",
    type: "get",
    data: {
      offset: offset
    },
    beforeSend: function (xhr) {
      $("#notification-list").after($("<li class='loading text-center'>Loading...</li>").fadeIn('slow')).data("loading", true);
    },
    success: function (data) {
      let $results = $("#notification-list");
      $("#notification-list-parent .loading").fadeOut('fast', function () {
        $(this).remove();
      });
      let $data = $(data).find('.notification-item');
      if (!$data.length) {
        $('#notification-list-parent').attr('last-record', true);
      }
      $data.hide();
      $results.append($data);
      $data.fadeIn();
      $results.removeData("loading");
    }
  });
}

$(function () {
  $("#notification-list-parent").scroll(function () {
    var $this = $(this);
    var $results = $("#notification-list");
    var is_last = $('#notification-list-parent').attr('last-record');
    if (!is_last && !$results.data("loading")) {
      if ($this.scrollTop() + $this.height() >= $results.height()) {
        loadResults();
      }
    }
  });
});

// read notification
function clickToNotification(e) {
  let notify_id = e.attr('data-id');
  if (e.hasClass('unread')) {
    $.ajax({
      url: "/notifications/read",
      type: "post",
      data: {
        id: notify_id
      },
      headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
      },
      success: function (data) {
      }
    });
  }
}

$(document).on('click', '.notification-item', function () {
  clickToNotification($(this));
});

$(document).on('mousedown', '.notification-item', function (event) {
  if (event.which === 2) {
    clickToNotification($(this));
  }
})

function read_noti(id) {
  $.ajax({
    url: "/notifications/read",
    type: "post",
    data: {
      id: id
    },
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },
    success: function (data) {
    }
  });
}

$(document).on('click', '.popup-noti a', function () {
  read_noti($(this).closest('.popup-noti').attr('data-id'));
  $(this).closest('.toast').hide();
});

// read reminder
function clickToReminder(e) {
  let reminder_id = e.attr('data-id');
  $.ajax({
    url: "/reminders/read",
    type: "post",
    data: {
      id: reminder_id
    },
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },
    success: function (data) {
    }
  });
}

$(document).on('click', '.reminder-item', function () {
  clickToReminder($(this));
});

$(document).on('mousedown', '.reminder-item', function (event) {
  if (event.which === 2) {
    clickToReminder($(this));
  }
})

$(document).on('click', '.close[data-dismiss="alert"]', function () {
  $('.kanban-ticket.has-flash').removeClass('has-flash');
})
// read all notification
$(document).on('click', '.read_all_notification', function (e) {
  e.stopPropagation();
  url = $(this).data('url');
  $.ajax({
    url: url,
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },
    success: function (data) {
    }
  });
});
// read all reminder
$(document).on('click', '.read_all_reminder', function (e) {
  e.stopPropagation();
  url = $(this).data('url');
  $.ajax({
    url: url,
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },
    success: function (data) {
    }
  });
});
// expand/collapse duplicated deals
$(document).on('click', '.duplicated-expand', function () {
  if ($(this).data('content') == 'collapse') {
    $('.duplicated-hide').show();
    $(this).data('content', 'expand');
    $(this).text('Ẩn bớt');
    $('.duplicated-ellipsis').hide();
  } else if ($(this).data('content') == 'expand') {
    $('.duplicated-hide').hide();
    $(this).data('content', 'collapse');
    $(this).text('Xem thêm');
    $('.duplicated-ellipsis').show();
  }
})

$(document).on('input', '#customer_phone_number', function () {
  let $inputCustomerPhone = $(this);
  let country_code = $('#customer_country_code').val();
  let phone_number = $(this).val();
  let customer_id = $('#customer_id').val();
  if (!country_code || phone_number.length < 9 || phone_number.length > 12 || /^\d+$/.test(phone_number) !== true) {
    $inputCustomerPhone.parent().find('.invalid-feedback').remove();
    $inputCustomerPhone.after(`<span class='invalid-feedback custom-validation'>Số điện thoại không hợp lệ</span>`);
    $inputCustomerPhone.closest('.input-group').addClass('field_with_errors');
    return;
  }
  $('.btn_save_has_phone').removeAttr('disabled');
  $.ajax({
    url: "/customers/check_same_phone",
    type: "get",
    data: {
      customer_id: customer_id,
      country_code: country_code,
      phone_number: phone_number,
      is_mobile: $('#is_mobile').val(),
    },
    success: function (data) {
      if ($('.swal2-container').length > 0) {
        $inputCustomerPhone = $('.swal2-container').find('#customer_phone_number');
      }
      if (data.msg) {
        $inputCustomerPhone.parent().find('.invalid-feedback').remove();
        $inputCustomerPhone.after(`<span class='invalid-feedback custom-validation'>${data.msg} <i class="tms tms-info" data-html="true" data-placement="top" data-toggle="tooltip" title="Click vào Tên khách hàng để kết nối giao dịch với khách hàng này"></i></span>`);
        $('[data-toggle="tooltip"]').tooltip({
          template: '<div class="tooltip tms-tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
        });
        $inputCustomerPhone.parent().addClass('field_with_errors');
        $('.btn_save_has_phone').attr('disabled', 'disabled');
      } else {
        $inputCustomerPhone.parent().removeClass('field_with_errors');
        $inputCustomerPhone.parent().find('.custom-validation').remove();
      }
    }
  });
});

$(document).on('click', '.same-customer', function () {
  $('#deal_customer').append(`<option value="${$(this).data('customer-id')}" data-select2-id="${$(this).data('customer-id')}">${$(this).data('customer-name')}</option>`);
  $('#deal_customer').val(`${$(this).data('customer-id')}`).trigger('change');
  $('#deal_phone_number').val(`${$(this).data('customer-phone')}`);
  $('#deal_customer').closest('.form-group').removeClass('field_with_errors');
  $('#deal_customer').closest('.form-group').find('.field_with_errors').removeClass('field_with_errors');
  $('#deal_customer').closest('.form-group').find('.custom-validation').hide();
  $('.deal-customer-name').val($(this).data('customer-name') != undefined ? $(this).data('customer-name') : '');
  $('.deal-phone-number').val($(this).data('customer-phone') != undefined ? $(this).data('customer-phone') : '');
  if ($('#deal_customer').closest('#general-info').length > 0) {
    $('#general-info-tab').find('span').text($('#general-info').find('.row').not(".input-hidden").find('.field_with_errors:not(:has(input[type="hidden"]))').length / 2);
    if ($('#general-info').find('.row').not(".input-hidden").find('.field_with_errors').length == 0) {
      $('#general-info-tab').find('span').addClass('d-none');
    }
  }
  swalBootstrap.close();
})

$(document).on('click', '#customer-portrait-tab', function () {
  $('.deal-customer-name').val($("#deal_customer option:selected").text() !== undefined ? $("#deal_customer option:selected").text() : '');
  $('.deal-phone-number').val($("#deal_phone_number").val() !== undefined ? $("#deal_phone_number").val() : '');
});

$(document).on('click', '.btn-add-nested-field', function () {
  $('#' + $(this).data('trigger')).click();
})

$(document).on('change', '#kpi_year', function () {
  let parent = $(this);
  let children = parent.data("select-child-target");
  children.forEach(function (child) {
    let $child = $("#" + child);
    let $default_child_value = $("#" + child + "_default_value");
    let parent_selected_value = parent.children('option:selected').val();
    if (parent_selected_value !== null && parent_selected_value !== '') {
      var data = {year: parent_selected_value};
      var successCallback = function (res) {
        if ($child.data('select2')) {
          $child.empty();
        }
        res.forEach(function (item) {
          let option = `<option value="${item.id}">${item.name}</option>`;
          $child.append(option);
        });
        if ($default_child_value !== undefined) {
          $child.val($default_child_value.val());
          $default_child_value.remove();
        }
      }
      $ajax(`/kpis/get_teams`, 'post', data).then(successCallback, null);
    }
  })
})

$(document).on('click', '.btn-more-less', function () {
  var truncate = $(this).closest('.bd-bottom-detail-project').find('.project-description');
  if (truncate.hasClass('truncate-project-description')) {
    truncate.removeClass('truncate-project-description');
    $(this).text('Ẩn bớt thông tin');
  } else {
    truncate.addClass('truncate-project-description');
    $(this).text('Xem thêm');
  }
});

$(document).on('mouseover focus', '.input-price-decimal', function () {
  var id = $(this).attr('id');
  if ($('#edit-deal-commission').find('input[name="save"]').length > 0) {
    if (id.includes('temporary_commission') || id.includes('actually_commission')) {
      $(this).attr('placeholder', 'VD: 100.000.000');
    } else {
      $(this).attr('placeholder', 'VD: 15.000.000.000');
    }
  }
});

$(document).on('click', '.paginate_button.page-item', function () {
  $('.table-hover tbody tr td').each(function () {
    if ($(this).find('a.btn-outline-primary').length == 0 && !$('.table-hover').hasClass('complete-deal-table') && !$('.table-hover').hasClass('unable-click') && !$('.table-hover').hasClass('dnd-table')) {
      $(this).click(function () {
        $(this).parent().find('.btn-outline-primary')[0].click();
      })
    }
  })
})

$(document).on('click', '.btn-referral-code', function (e) {
  e.preventDefault();
  var copyText = $('.referral-code').text();
  var textarea = document.createElement("textarea");
  textarea.textContent = copyText;
  textarea.style.position = "fixed";
  document.body.appendChild(textarea);
  textarea.select();
  document.execCommand("copy");
  document.body.removeChild(textarea);
})

var getDevice = function () {
  if (typeof device !== 'undefined') return device;
  var userAgent = navigator.userAgent.toLowerCase();
  var iOS = /(ipad|iphone|ipod)/g.test(userAgent) || (navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1);
  var android = /(android)/g.test(userAgent);
  var mobile = iOS || android;
  var check_divice_ruby = $('body').attr('device');
  var check_divice_js = mobile ? 'mobile' : 'pc';
  var device;
  switch (true) {
    case check_divice_ruby == 'pc' && check_divice_js == 'pc' :
      device = 'pc';
      break;
    case check_divice_ruby == 'mobile' && check_divice_js == 'mobile':
      device = 'mobile';
      break;
    default:
      device = 'tablet';
      break;
  }
  return device;
}

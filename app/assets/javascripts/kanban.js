$(document).on("turbolinks:before-cache", function () {
  $('body').removeClass('body-kanban');
});

$(document).on('click', '.card.kanban-item', function (e) {
  if (!$(e.target).hasClass('assignee') && !$(e.target).parent().hasClass('assignee') && !$(e.target).is(".btn-show-detail")) {
    if ($(this).find('.detail-modal-btn')) {
      if (device=='mobile') {
        window.location = $(this).find('.detail-modal-btn').attr('href');
      } else {
        window.open($(this).find('.detail-modal-btn').attr('href'), '_blank');
      }
    }
  }
})

// js show and search assignee
$(document).on('click', '#assignee, .assign-btn-close', function () {
  $('#kanban-assign').toggle('show');
  setTimeout(function () {
    $('.input-search-assignee').trigger('focus');
  }, 200);
});

// js close modal edit
const swalWithBootstrapButtons = Swal.mixin({
  customClass: {
    confirmButton: ' btn btn-kanban-danger font-size-16 tms-line-height-22 pl-3 pr-3 mr-2',
    cancelButton: ' btn btn-kanban-border-danger font-size-16 pl-3 pr-3 mr-2'
  },
  buttonsStyling: false
})

const swalBootstrapErrorCampaignLock = Swal.mixin({
  customClass: {
    confirmButton: ' btn btn-kanban-danger font-size-16 tms-line-height-22 pl-3 pr-3 mr-2 font-weight-500',
  },
  buttonsStyling: false
})

const swalBootstrap = Swal.mixin({
  customClass: {
    confirmButton: ' btn btn-kanban-danger font-size-16 tms-line-height-22 pl-3 pr-3 mr-1',
    cancelButton: ' btn btn-kanban-border-danger font-size-16 pl-3 pr-3 mr-2'
  },
  buttonsStyling: false
})

const swalBootstrapTransferHistories = Swal.mixin({
  customClass: {
    confirmButton: ' btn btn-kanban-danger font-size-16 tms-line-height-22 pl-3 pr-3 mr-1',
    cancelButton: ' btn btn-kanban-border-danger font-size-16 pl-3 pr-3 mr-2',
    title: ' transfer-histories-title',
    content: ' transfer-histories-content',
    header: ' transfer-histories-header'
  },
  buttonsStyling: false
})

// js close modal edit
const swalBootstrapCanceled = Swal.mixin({
  customClass: {
    confirmButton: ' btn btn-kanban-danger font-size-16 tms-line-height-22 pl-3 pr-3 mr-2',
    cancelButton: ' btn btn-kanban-border-danger font-size-16 pl-3 pr-3 mr-2',
    container: 'swal-canceled-group'
  },
  buttonsStyling: false
})

const swalBootstrapUninterested = Swal.mixin({
  customClass: {
    confirmButton: ' btn btn-kanban-danger font-size-16 tms-line-height-22 pl-3 pr-3 mr-2',
    cancelButton: ' btn btn-kanban-border-danger font-size-16 pl-3 pr-3 mr-2',
    container: 'swal-uninterested-group'
  },
  buttonsStyling: false
})

const swalBootstrapUpdateProduct = Swal.mixin({
  customClass: {
    confirmButton: ' btn btn-kanban-danger font-size-16 tms-line-height-22 pl-3 pr-3 mr-2',
    cancelButton: ' btn btn-kanban-border-danger font-size-16 pl-3 pr-3 mr-2',
    container: 'swal-update-product-group'
  },
  buttonsStyling: false
})

const swalBootstrapUpdateCommission = Swal.mixin({
  customClass: {
    confirmButton: ' btn btn-kanban-danger font-size-16 tms-line-height-22 pl-3 pr-3 mr-2',
    cancelButton: ' btn btn-kanban-border-danger font-size-16 pl-3 pr-3 mr-2',
    container: 'swal-update-commission-group'
  },
  buttonsStyling: false
})

const swalBootstrapUpdateCsTicket = Swal.mixin({
  customClass: {
    confirmButton: ' btn btn-outline-reti-primary btn-confirm-cs-ticket font-size-16 tms-line-height-22 pl-3 pr-3 mr-2 font-weight-500',
    cancelButton: ' btn btn-reti-primary font-size-16 pl-3 pr-3 mr-2 font-weight-500',
  },
  buttonsStyling: false
})

$(document).on('turbolinks:load', function () {
  if ($('.kanban-ticket').length) {
    $('body').addClass('body-kanban');
  }

  $('#modal-detail').on('hide.bs.modal', function (event) {
    $('.kanban-item').removeClass('active');
    $('.btn-show-detail i').removeClass('fa-chevron-left').addClass('fa-chevron-right');
  })
  $('#modal-assign').on('hide.bs.modal', function (event) {
    $('.kanban-item').removeClass('active');
  })

  // modal assign
  $(document).on('click', '.btn-assign.assignee', function (e) {
    // show modal assgin
    tar_element = $(this).attr('data-target');
    $(`${tar_element}`).modal('show');
    $('.kanban-item').removeClass('active');
    $(this).closest('.kanban-item').addClass('active');

    let kanban_item_offset = $(this).closest('.kanban-item').offset();
    let item_width = $(this).closest('.kanban-item').width();
    let item_height = $(this).closest('.kanban-item').height();
    let modal_width = $('#modal-assign .modal-dialog').width();
    let modal_height = $('#modal-assign .modal-dialog').height();
    modal_height = modal_height > 0 ? modal_height : 320;
    modal_width = 340;

    const left_margin = 32;
    pos_top = 0;
    pos_left = 0;

    checkY = $(window).height() - kanban_item_offset.top - item_height - modal_height;
    if (checkY > 0) {
      pos_top = kanban_item_offset.top + item_height + 4;

    } else {
      pos_top = kanban_item_offset.top + item_height - modal_height;
    }

    checkX = $(window).width() - kanban_item_offset.left - item_width - modal_width;
    if (checkX > 0) {
      pos_left = kanban_item_offset.left + item_width;
    } else {
      pos_left = kanban_item_offset.left - modal_width;
    }

    // lùi bên trái
    if (checkY > 0) {
      if (checkX > 0) {
        pos_left -= left_margin
      } else {
        pos_left += left_margin
      }
    } else {
      if (checkX > 0) {
        pos_left += 4
      } else {
        pos_left -= 4
      }
    }
    $('#modal-assign .modal-dialog').css({'top': `${pos_top}px`, 'left': `${pos_left}px`});


    // doan nay check lai
    btn_click = $(this)
    $('#modal-assign').find('.assign-item').css('display', '');
    $('.input-search-assignee').val('');

    $(document).on('click', '#modal-assign .assign-item', function () {
      let id = $(this).attr('data-value');
      btn_click.closest('form').find('.input-assignee').attr('value', id);
      let content = $(this).find('img').clone();
      // btn_click.empty().append(content);
      $('#modal-assign').modal('toggle');
    })


  });
});

$(document).on('click', '.preview-modal-btn', function () {
  $this = $(this).next().find('.btn-show-detail');
  $this.find('i').addClass('fa-chevron-left').removeClass('fa-chevron-right');

  $('.kanban-item').removeClass('active');
  $this.closest('.kanban-item').addClass('active');
  let kanban_item_offset = $this.closest('.kanban-item').offset();
  let item_width = $this.closest('.kanban-item').width();
  let modal_width = $('#modal-detail .modal-dialog').width();
  let modal_height = $('#modal-detail .modal-dialog').height();
  modal_height = modal_height > 0 ? modal_height : 640;
  // modal_height = 757;

  let maxScrollLeft = $('.kanban-ticket')[0].scrollWidth - $('.kanban-ticket')[0].clientWidth;
  width_include_scroll_x = $(window).width() + maxScrollLeft;
  let checkX = width_include_scroll_x - ($('.kanban-ticket').scrollLeft() + kanban_item_offset.left + item_width + modal_width);

  pos_top = 0;
  pos_left = 0;
  if (checkX > 0) {
    part_modal_hide = $(window).width() - (kanban_item_offset.left + item_width + modal_width) - 20;
    pos_top = kanban_item_offset.top;
    pos_left = kanban_item_offset.left + item_width + 4;
    // scroll center x neu ko du hien thi
    if (part_modal_hide < 0) {
      $('.kanban-ticket').scrollLeft($('.kanban-ticket').scrollLeft() - part_modal_hide);
      pos_left += part_modal_hide;
    }
  } else {
    part_modal_hide = kanban_item_offset.left - modal_width;
    pos_top = kanban_item_offset.top;
    pos_left = kanban_item_offset.left - modal_width - 4;
    // scroll center x neu ko du hien thi
    if (part_modal_hide < 0) {
      $('.kanban-ticket').scrollLeft($('.kanban-ticket').scrollLeft() + part_modal_hide);
      pos_left -= part_modal_hide;
    }
  }
})

$(document).on('hide.bs.modal', '#modal-detail', function (event) {
  $('.kanban-item').removeClass('active');
  $('.btn-show-detail i').removeClass('fa-chevron-left').addClass('fa-chevron-right');
})

$(document).on('show.bs.modal', '#modal-detail', function () {
  let modal_height = $('#modal-detail .modal-dialog').height();
  modal_height = modal_height > 0 ? modal_height : 640;
  $('#modal-detail .modal-dialog').css({'left': `${pos_left}px`});

  scroll_y = $(window).height() - pos_top - modal_height;
  if (scroll_y < 0) {
    $('#modal-detail').animate({scrollTop: pos_top}, 1000);
  }
})

$(document).on('shown.bs.modal', '#modal-detail', function () {
  let modal_height = $('#modal-detail .modal-dialog').height();
  modal_height = modal_height > 0 ? modal_height : 640;
  if ($(window).height() < modal_height) {
    $('#modal-detail .modal-dialog .modal-body').css({'height': `${$(window).height() - 45 - $('#modal-detail .modal-header').height()}px`});
  }
})

$(document).on('click', '.assignee.btn.btn-assign', function (e) {
  // modal assignee outer
  var assigneeSuggestionOuter = new Bloodhound({
    limit: 10,
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: '../deals/assignee_search?query=%QUERY',
      filter: function (data) {
        return data;
      },
      wildcard: "%QUERY"
    }
  });

  assigneeSuggestionOuter.initialize();
  var assigneeTypeaheadOuter = $('#modal-assign .input-search-assignee');
  assigneeTypeaheadOuter.typeahead('destroy');
  assigneeTypeaheadOuter.typeahead({
    hint: true,
    highlight: true,
    minLength: 0
  },
  {
    limit: 500,
    name: 'value',
    displayKey: 'value',
    source: assigneeSuggestionOuter.ttAdapter(),
    showHintOnFocus: true,
    templates: {
      empty: '<div class="noitems">Không tìm thấy kết quả</div>',
      suggestion: function (data) {
        if ((!e.target.parentElement.hasAttribute("data-assignee-id") || data._query.length > 0) && data.id === null) {
          return `<div class="d-none"</div>`;
        }
        return `<div class="row assign-item" data-value="${data.id}">
                <div class="assign-item-avatar mr-2">
                <img alt="User Default Avatar" class="img-circle avatar-assign" src="${data.avatar}">
                </div>
                <div class="assign-item-name">${data.text}</div>
              </div>`
      }
    }
  }).on('typeahead:selected', function (data, selectedItem) {
    let text = selectedItem.text;
    let img_url = selectedItem.avatar;
    let img_unassign = '<img alt="User Default Avatar" class="img-circle w-100 avatar-assign" src="https://reti-tms-staging.s3.ap-southeast-1.amazonaws.com/assets/add-assignee.svg">'
    deal_id = $('.kanban-item.active').attr('data-id');
    $('#modal-assign').modal('hide');
    if (deal_id != undefined) {
      var data = {deal: {assignee_id: selectedItem.id}};
      var successCallback = function (res) {
        if (res.status === 'ok') {
          btn_click.find('img').attr('src', img_url)
          btn_click.attr('data-assignee-id', selectedItem.id)
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: "Bạn đã phân công cho " + text,
            showConfirmButton: false,
            showClass: {
              popup: 'swal-create-customer',
            },
            timer: 2000
          });
        } else if (res.status === 'cancel') {
          btn_click.html(img_unassign)
          btn_click.attr('data-assignee-id', null)
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: "Bạn đã xoá người phân công cho giao dịch",
            showConfirmButton: false,
            showClass: {
              popup: 'swal-create-customer',
            },
            timer: 2000
          });
        }
      }
      var failureCallback = function (res) {
        // Do nothing
      }
      $ajax(`/deals/${deal_id}/update_assignee`, 'patch', data).then(successCallback, failureCallback);
    }
  });
})

$(document).on('shown.bs.modal', '#modal-assign', function () {
  $('.input-search-assignee').trigger('focus');
});

var hoverTimer;
$(document).on('mouseenter', '.btn-show-detail', function () {
  var $this = $(this);
  hoverTimer = setTimeout(function () {
    if (!$('#modal-detail').hasClass('show')) {
      $this.closest('.kanban-item').find('.preview-modal-btn').click();
    }
  }, 100);
});
$(document).on('mouseleave', '.btn-show-detail', function () {
  clearTimeout(hoverTimer);
});

$(document).on('keyup', '.input-edit-title-deal', function () {
  $(this).css('width', (this.value.length * 11) + 'px');
})

// add item new
var allowSubmit = true;
$(document).on('turbolinks:load', function () {
  $(document).on('click', '.btn-add-kanban-item', function () {
    if ($('.kanban-item-new-form').length <= 1) {
      $(this).addClass('kanban-btn-disabled').attr('disabled', '');
      let item_new = $('#kanban-item-new').clone();
      item_new.removeClass('d-none');
      item_new.removeAttr('id');
      $(this).closest('.kanban-col').find('.kanban-body-container').prepend(item_new);
      msg = $('.kanban-item-title-error');
      item_new.after(msg);
      item_new.find('.input-title').focus();
    }
    if (!isMobile) {
      setHeightKanbanColumn();
    }
  });

  $(document).on('click', '.kanban-item-new-cancel', function () {
    $(this).closest('.kanban-col').find('.kanban-btn-disabled').removeAttr('disabled');
    $(this).closest('.kanban-col').find('.kanban-item-title-error').css('display', 'none');
    $(this).closest('.card').remove();
  })

  $(document).on('click', '.kanban-item-new-save', function (e) {
    if (!allowSubmit) {
      e.preventDefault();
      return;
    }
    var dealTitle = $(this).closest('.card').find('.input-title').val().trim();
    if (dealTitle != '') {
      $(this).closest('.card').find('.input-title').val(dealTitle);
      $(this).closest('form').submit();
      allowSubmit = false;
      return true;
    } else {
      // show error
      $(this).closest('.card').addClass('kanban-item-error');
      $(this).closest('.card').next('.kanban-item-title-error').show();
      return false;
    }
  });

  $(document).on('keypress', '.input-title', function (e) {
    if (e.which == 13) {
      $(this).closest('.card').find('.kanban-item-new-save').click();
      return false;
    }
  });

  $(document).on('keydown', '.kanban-clone .input-title', function () {
    $(this).closest('.card').removeClass('kanban-item-error');
    $(this).closest('.card').next('.kanban-item-title-error').hide();
  });
});

function loadmore_deals() {
  let num_page = parseInt($(".init-kanban-deals").attr('num_page'));
  let isloading = $(".init-kanban-deals").attr('isloading');
  let ended = $(".init-kanban-deals").attr('ended');
  $(".init-kanban-deals").attr('isloading', 'true');
  if (isloading || ended) return;
  showLoading(true);
  $.ajax({
    url: `/deals`,
    type: "get",
    data: {
      is_loaded: true,
      page: num_page + 1,
      query: $('[name="query"]').val(),
      'assignee': rejectUndefined('#assignee-search-deal'),
      'balcony_direction': rejectUndefined('#balcony-direction-search-deal'),
      'created_at': $('#created-at-search-deal').val(),
      'door_direction': rejectUndefined('#door-direction-search-deal'),
      'group': rejectUndefined('#group-search-deal'),
      'label': rejectUndefined('#label-search-deal'),
      'product_type': rejectUndefined('#product-type-search-deal'),
      'project': rejectUndefined('#project-search-deal'),
      'purchase_purpose': rejectUndefined('#purchase-purpose-search-deal'),
      'source': rejectUndefined('#source-search-deal'),
      'state': rejectUndefined('#state-search-deal'),
      'updated_at': $('#updated-at-search-deal').val(),
      'order_by': $('input[type=radio][name=order_by]:checked').val(),
      'sort': $('.btn-sort-deal').val()
    },
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },
    success: function (data) {
      let check_end = true;
      $('.kanban-col').each(function () {
        let state = $(this).attr('data-state');
        let html_append = '';
        html_append = $(data).find(`.kanban-col[data-state="${state}"] .kanban-item`);
        $(this).find('.kanban-body-container').append(html_append);
        check_end = check_end ? html_append.length == 0 : check_end;
      });
      if (getDevice() == 'tablet') {
        $('.show-deal-detail').addClass('d-none');
      }
      setHeightKanbanColumn();
      $(".init-kanban-deals").attr({'isloading': '', 'ended': check_end ? 'true' : '', 'num_page': num_page + 1});
      showLoading(false);
    }
  });
}

function load_deal_by_view_mode(page, view_mode = '', deal_completed = false) {
  showLoading(true);
  if (view_mode == 'table' && $('input[type=radio][name=order_by]:checked').val() == 'none') {
    $('input[type=radio][name=order_by][value=updated_at]').attr('checked', true);
    $('#dropdown-sort-deal').text($('input[type=radio][name=order_by]:checked').parent().text());
    $('.btn-sort-deal').removeAttr('disabled');
    $('.btn-sort-deal').val('DESC');
    $('.btn-sort-deal').html('<i class="fas fa-sort-alpha-down-alt"></i>')
  }
  var url;
  if (deal_completed === true) {
    url = `/deals/complete_deals?`;
  }
  else {
    url = `/deals`;
  }
  $.ajax({
    url: url,
    type: "get",
    data: {
      page: page,
      query: $('[name="query"]').val(),
      'assignee': rejectUndefined('#assignee-search-deal'),
      'balcony_direction': rejectUndefined('#balcony-direction-search-deal'),
      'created_at': $('#created-at-search-deal').val(),
      'door_direction': rejectUndefined('#door-direction-search-deal'),
      'group': rejectUndefined('#group-search-deal'),
      'label': rejectUndefined('#label-search-deal'),
      'product_type': rejectUndefined('#product-type-search-deal'),
      'project': rejectUndefined('#project-search-deal'),
      'purchase_purpose': rejectUndefined('#purchase-purpose-search-deal'),
      'source': rejectUndefined('#source-search-deal'),
      'state': rejectUndefined('#state-search-deal'),
      'updated_at': $('#updated-at-search-deal').val(),
      'order_by': $('input[type=radio][name=order_by]:checked').val(),
      'sort': $('.btn-sort-deal').val(),
      'view_mode': view_mode
    },
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },
    success: function (data) {
      $('#deal-container').html($(data).find('#deal-container').children());
      $('.tms-datatables-scroll').DataTable({
        "bInfo": false,
        "lengthChange": false,
        "searching": false,
        "scrollX": true,
        "autoWidth": true,
        responsive: true,
        "fixedHeader": {
          "header": false,
        },
        "language": {
          "decimal": ",",
          "thousands": ".",
          "emptyTable": "Không có dữ liệu!",
          "search": "Tìm kiếm:",
          "info": "_START_ - _END_ trong _TOTAL_ kết quả",
          "infoEmpty": "0 trên 0 kết quả",
          "infoFiltered": "(được lọc từ _MAX_ kết quả)",
          "lengthMenu": "Hiển thị _MENU_ kết quả",
          "zeroRecords": "Không tìm thấy kết quả!",
          "paginate": {
            "previous": '<i class="fas fa-angle-left"></i>',
            "next": '<i class="fas fa-angle-right"></i>'
          }
        }
      });
      if (view_mode != 'table') {
        $(".init-kanban-deals").on('scroll', function () {
          if ($(this).scrollTop() + $(this).innerHeight() + 200 >= $(this)[0].scrollHeight) {
            loadmore_deals();
          }
        })
        setHeightKanbanColumn();
      } else {
        $('.table-hover tbody tr td').each(function () {
          if ($(this).find('a.btn-edit').length == 0) {
            $(this).click(function () {
              if ($(this).parent().find('.btn-edit').length > 0) {
                $(this).parent().find('.btn-edit')[0].click();
              }
            })
          }
        })
      }
      
      showLoading(false);
    }
  });
}

$(document).on('change', '.deal-check-view-mode', function () {
  $(this).closest('form').submit();
});

$(document).on('click', '#list-view .page-item a, .deal-completed-list .page-item a', function(e){
  if ($(this).parent() == $('.page-item.active')) {
    e.preventDefault();
  } else {
    e.preventDefault();
    let url = new URL(window.location.origin + $(this).attr('href'));
    if (window.location.href.includes('complete_deals')) {
      load_deal_by_view_mode(url.searchParams.get('page'), '', true);
    } else {
    load_deal_by_view_mode(parseInt(url.searchParams.get('page')), 'table');
    }
  }
})

$(document).on('turbolinks:load', function () {
  $(".init-kanban-deals").on('scroll', function () {
    if ($(this).scrollTop() + $(this).innerHeight() + 200 >= $(this)[0].scrollHeight) {
      loadmore_deals();
    }
  })

  var text_span = $('.dropdown-toggle').find('span').first();
  if (text_span.text().length <= 16) {
    text_span.removeClass('dropdown-state');
  } else
    text_span.addClass('dropdown-state');
});

$(document).on('turbolinks:load', function() {
  $('#regulation_project_id').select2({
    theme: 'bootstrap',
    language: 'vi',
    placeholder: 'Vui lòng chọn'
  });

  $("#regulation_date_range").daterangepicker({
    autoUpdateInput: false,
    opens: 'right',
    minYear: 2019,
    maxYear: 2119,
    drops: "auto",
    locale: {
      format: 'DD/MM/YYYY'
    },
  }, function(start, end, label) {
    $('#regulation_date_range').closest('.form-group').removeClass('field_with_errors');
    $('#regulation_date_range').closest('.form-group').find('.field_with_errors').removeClass('field_with_errors');
    $('#regulation_date_range').closest('.form-group').find('.custom-validation').hide();
  });

  $('#regulation_date_range').on('apply.daterangepicker', function(ev, picker) {
    $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
  });

  $('#regulation_real_estate_type').select2({
    ajax: {
      method: 'POST',
      url: '/regulations/get_types',
      dataType: 'json',
      data: function (params) {
        var query = {
          search: params.term,
          project_id: $('#regulation_project_id').val()
        }
        return query;
      },
      processResults: function (data) {
        return {
          results: data
        };
      },
      cache: true
    },
    theme: 'bootstrap',
    language: 'vi',
    minimumInputLength: 0,
  });

  var regulationNameSuggestion = new Bloodhound({
    limit: 10,
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: '../regulations/autocomplete?type=name&query=%QUERY',
      filter: function (data) {
        return data;
      },
      wildcard: "%QUERY"
    }
  });

  var regulationProjectSuggestion = new Bloodhound({
    limit: 10,
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: '../regulations/autocomplete?type=project&query=%QUERY',
      filter: function (data) {
        return data;
      },
      wildcard: "%QUERY"
    }
  });

  regulationProjectSuggestion.initialize();

  var regulationTypeSuggestion = new Bloodhound({
    limit: 10,
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: '../regulations/autocomplete?type=real_estate&query=%QUERY',
      filter: function (data) {
        return data;
      },
      wildcard: "%QUERY"
    }
  });

  regulationTypeSuggestion.initialize();

  var regulationStateSuggestion = new Bloodhound({
    limit: 10,
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: '../regulations/autocomplete?type=state&query=%QUERY',
      filter: function (data) {
        return data;
      },
      wildcard: "%QUERY"
    }
  });

  regulationStateSuggestion.initialize();

  var regulationTypeahead = $('.regulation-search-input');
  regulationTypeahead.typeahead({
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      limit: 500,
      name: 'value',
      displayKey: 'value',
      source: regulationNameSuggestion.ttAdapter(),
      templates: {
        header: '<h5 class="group-name">Cơ chế</h5>'
      }
    },
    {
      limit: 500,
      name: 'value',
      displayKey: 'value',
      source: regulationProjectSuggestion.ttAdapter(),
      templates: {
        header: '<h5 class="group-name">Dự án</h5>'
      }
    },
    {
      limit: 500,
      name: 'value',
      displayKey: 'value',
      source: regulationTypeSuggestion.ttAdapter(),
      templates: {
        header: '<h5 class="group-name">Loại hình</h5>'
      }
    },
    {
      limit: 500,
      name: 'value',
      displayKey: 'value',
      source: regulationStateSuggestion.ttAdapter(),
      templates: {
        header: '<h5 class="group-name">Trạng thái</h5>'
      }
    }).on('typeahead:selected', function($e, data){
      if (data != undefined) $('#reg_type').val(data.type);
      $('#regulation-search-btn').click();
    }).on('input', function(e) {
      if ($(this).val() == '') {
        $('#reg_type').val('');
        $('#regulation-search-btn').click();
      }
    });

  $('#regulation_project_id').on('change', function () {
    $('#regulation_real_estate_type option:selected').remove()
  })
});

$(document).on('turbolinks:load', function () {
  if ($('#banner-banner-desktop-input').length){
    var bannerDesktopConfigs = [];
    var bannerDesktopUrls = [];
    $('input[name="banner[banner_desktop]"]').each(function (index, elm) {
      bannerDesktopConfigs.push({caption: $(elm).val(), width: '120px', url: $(elm).data('delete-url') + '?type=banner_desktop', filename: $(elm).val()});
      bannerDesktopUrls.push($(elm).data('url'));
    });
    init_fileinput($('#banner-banner-desktop-input'), bannerDesktopUrls, bannerDesktopConfigs);
  }
  if ($('#banner-banner-mobile-input')){
    var bannerMobileConfigs = [];
    var bannerMobileUrls = [];
    $('input[name="banner[banner_mobile]"]').each(function (index, elm) {
      bannerMobileConfigs.push({caption: $(elm).val(), width: '120px', url: $(elm).data('delete-url') + '?type=banner_mobile', filename: $(elm).val()});
      bannerMobileUrls.push($(elm).data('url'));
    });
    init_fileinput($('#banner-banner-mobile-input'), bannerMobileUrls, bannerMobileConfigs);
  }
  if ($('#banner-banner-app-input').length){
    var bannerAppConfigs = [];
    var bannerAppUrls = [];
    $('input[name="banner[banner_app]"]').each(function (index, elm) {
      bannerAppConfigs.push({caption: $(elm).val(), width: '120px', url: $(elm).data('delete-url') + '?type=banner_app', filename: $(elm).val()});
      bannerAppUrls.push($(elm).data('url'));
    });
    init_fileinput($('#banner-banner-app-input'), bannerAppUrls, bannerAppConfigs);
  }

  var fixHelper = function(e, ui) {
    ui.children().each(function() {
      $(this).width($(this).width());
    });
    return ui;
  };

  $('#sortable').sortable({
    helper: fixHelper,
    start: function(event, ui) {
      var start_pos = ui.item.index();
      ui.item.data('start_pos', start_pos);
    },
    change: function(event, ui) {
      var start_pos = ui.item.data('start_pos');
      var index = ui.placeholder.index();
      if (start_pos < index) {
        $('#sortable tr:nth-child(' + index + ')').addClass('bg-light');
      } else {
        $('#sortable tr:eq(' + (index + 1) + ')').addClass('bg-light');
      }
    },
    update: function(event, ui) {
      $('#sortable tr').removeClass('bg-light');
    }
  });
  // sort stt
  $('#sortable .item').each(function(index){
    $(this).find('.stt').html(index + 1);
  });

  $('#btn-sort-banner').click(function(){
    let sort_params = {}
    $('#sortable .item').each(function(index){
      sort_params[$(this).attr('data-id')] = {'position': index};
      $(this).find('.stt').html(index + 1);
    });
    // sort
    $.ajax({
      method: 'POST',
      url: '/banners/sort',
      type: 'JSON',
      headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
      },
      data: {
        sort: sort_params
      }
    }).done(function (response) {
      toastr.success(response.notice)
    });
  });

});

function init_fileinput(element, imageUrls, imageConfigs){
  element.fileinput({
    initialPreview: imageUrls,
    initialPreviewAsData: true,
    initialPreviewConfig: imageConfigs,
    overwriteInitial: false,
    allowedFileTypes: ['image'],
    showCancel: false,
    showRemove: false,
    browseLabel: '',
    removeClass: 'btn btn-danger',
    removeLabel: '',
    theme: 'fas',
    maxFileCount: 5,
    deleteUrl: '/',
    ajaxDeleteSettings: {
      headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
      }
    },
    fileActionSettings: {
      showZoom: function(config) {
        if (config.type === 'pdf' || config.type === 'image') {
          return true;
        }
        return false;
      }
    }
  }).on('filebeforedelete', function() {
    var aborted = !window.confirm('Are you sure you want to delete this file?');
    return aborted;
  }).on('filedeleted', function() {
    setTimeout(function() {
      window.alert('File deletion was successful! ' + krajeeGetCount('file-5'));
    }, 900);
  });

}

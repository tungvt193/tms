!(function ($) {
  $.extend({
    reorder: function (params) {
      let data = {
        deal_id_current: params.deal_id,
        deal_id_above: params.deal_id_above,
        state_to: params.state,
      }
      $.ajax({
        url: "/deals/reorder",
        type: "post",
        data: data,
        headers: {
          'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function (xhr) {
        },
        success: function (data) {
          // $('#modal-edit [name="cancel"]').click();
        },
        error: function (response) {
          $('.kanban-body-container').sortable('cancel');
        }
      });
    },
    changeState: function (params) {
      let data = {
        deal: {state: params.state},
        deal_id_above: params.deal_id_above,
        is_ajax: true
      }
      $.ajax({
        url: "/deals/" + params.deal_id + "/update_state",
        type: "patch",
        data: data,
        headers: {
          'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function (xhr) {
        },
        success: function (data) {
          if (params.onSucces && typeof params.onSucces == "function") {
            params.onSucces();
          }
        },
        error: function (response) {
          $('.kanban-body-container').sortable('cancel');
        }
      });
    },
    changeStateCanceled: function (params) {
      let data = {
        deal: {state: params.state, canceled_reason: params.canceled_reason},
        deal_id_above: params.deal_id_above,
      }
      $.ajax({
        url: "/deals/" + params.deal_id + "/update_state",
        type: "patch",
        data: data,
        headers: {
          'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function (xhr) {
        },
        success: function (data) {
          if (params.onSucces && typeof params.onSucces == "function") {
            params.onSucces();
          }
        },
        error: function (response) {
          $('.kanban-body-container').sortable('cancel');
        }
      });
    },
    changeStateUninterested: function (params) {
      let data = {
        deal: {state: params.state, uninterested_reason: params.uninterested_reason},
        deal_id_above: params.deal_id_above,
      }
      $.ajax({
        url: "/deals/" + params.deal_id + "/update_state",
        type: "patch",
        data: data,
        headers: {
          'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function (xhr) {
        },
        success: function (data) {
          if (params.onSucces && typeof params.onSucces == "function") {
            params.onSucces();
          }
        },
        error: function (response) {
          $('.kanban-body-container').sortable('cancel');
        }
      });
    },
  })
})(jQuery)

function triggerUpdateFor(selector) {
  var widget = $(selector).sortable("widget");
  if (widget) widget._trigger("update", null, widget._uiHash(widget));
}

function reportAppointment() {
  $('button[value="edit"]').click();
  $(document).off('focusin.modal');
  let html = $('#appointments-flag .nested-fields:first-child').find('form').clone();

  let appointmentNew = '<div class="row content-time new-appoiment">'
    + '<div class="col-12"><hr><h4>Tạo lịch hẹn mới.</h4></div>'
    + '<div class="col-12">'
    + '<label class="detail-item-heading-sub" for="appointment_content">Nội dung</label>'
    + '<input class="form-control new-content" id="" type="text" name="content">'
    + '</div>'
    + '<div class="col-12">'
    + '<label class="detail-item-heading-sub" for="appointment_appointment_date">Ngày hẹn gặp</label>'
    + '<input class="form-control datetimepicker new-date" id="" type="text" name="appointment_date">'
    + '</div>'
    + '</div>'

  html.find('.fields').append(appointmentNew);
  let showCancel = true;
  if ($('.appointment-late').length > 0) {
    showCancel = false;
  } else {
    showCancel = true;
  }
  swalWithBootstrapButtons.fire({
    title: 'Báo cáo kết quả buổi gặp!',
    html: html,
    showConfirmButton: true,
    showCancelButton: showCancel,
    cancelButtonText: 'Huỷ',
    confirmButtonText: 'Đồng ý',
    showCloseButton: showCancel,
    allowOutsideClick: false,
    focusConfirm: true,
    onOpen: function () {
      $('.swal2-content').addClass('text-left');
      $('.swal2-content .group-form').removeClass('d-none');
      $('.swal2-content .group-show,.content-time,.btn-destroy-nested,.btn-save-nested').addClass('d-none');
      $('.swal2-content .btn-save-nested').addClass('swal-save');

      $(".swal2-content .tms-icheck-tab").each(function () {
        var $el = $(this);
        var skin = ($el.attr('data-skin') !== undefined) ? "_" + $el.attr('data-skin') : "",
          color = ($el.attr('data-color') !== undefined) ? "-" + $el.attr('data-color') : "";
        var opt = {
          checkboxClass: 'icheckbox' + skin + color,
          radioClass: 'iradio' + skin + color,
        };
        $el.iCheck(opt);
      });
      $(".swal2-content .tms-icheck-tab").first().iCheck('check');
      $(".swal2-content").find('.datetimepicker').datetimepicker({
        format: 'd/m/Y H:i',
        lang: 'vi'
      });

      $(".swal2-content .tms-icheck-tab").on('ifChecked', function (event) {
        if ($(this).val() == 1) {
          $('.new-appoiment').removeClass('d-none');
        } else {
          $('.new-appoiment').addClass('d-none');
        }
      });
    },
    preConfirm: () => {
      let checkResult = true;
      if ($('.swal2-content .result-appointment').val() == '') {
        if ($('.swal2-content .result-appointment').parent().find('span.custom-validation').length == 0) {
          $('.swal2-content .result-appointment').parent().append('<span class="invalid-feedback custom-validation">Không được để trống trường này!</span>')
        } else {
          $('.swal2-content .result-appointment').parent().find('span.custom-validation').css('display', 'block');
        }
        $('.swal2-content .result-appointment').parent().addClass('field_with_errors');
        checkResult = false;
      } else if (!$('.swal2-content .new-appoiment').hasClass('d-none')) {
        if ($('.swal2-content .new-content').val() == '' && $('.swal2-content .new-date').val() != '') {
          if ($('.swal2-content .new-content').parent().find('span.custom-validation').length == 0) {
            $('.swal2-content .new-content').parent().append('<span class="invalid-feedback custom-validation">Không được để trống trường này!</span>')
          } else {
            $('.swal2-content .new-content').parent().find('span.custom-validation').css('display', 'block');
          }
          $('.swal2-content .new-content').parent().addClass('field_with_errors');
          checkResult = false;
        }
        if ($('.swal2-content .new-content').val() != '' && $('.swal2-content .new-date').val() == '') {
          if ($('.swal2-content .new-date').parent().find('span.custom-validation').length == 0) {
            $('.swal2-content .new-date').parent().append('<span class="invalid-feedback custom-validation">Không được để trống trường này!</span>')
          } else {
            $('.swal2-content .new-date').parent().find('span.custom-validation').css('display', 'block');
          }
          $('.swal2-content .new-date').parent().addClass('field_with_errors');
          checkResult = false;
        }
      }
      return checkResult;
    }
  }).then((result) => {
    if (result.isConfirmed) {
      $('.swal-save').click();
    } else {
    }
  })
}

function countRequiredAttributes() {
  // Đếm fields required
  let generalRequiredFields = 0;
  let customerRequiredFields = 0;
  $('#general-info').find('input, select, textarea').each(function () {
    if ($(this).closest('.field_with_errors').length) {
      generalRequiredFields += 1;
    }
  })

  $('#customer-portrait').find('input, select, textarea').each(function () {
    if ($(this).closest('.field_with_errors').length) {
      customerRequiredFields += 1;
    }
  })

  if (generalRequiredFields > 0 && customerRequiredFields > 0) {
    $('.deal-invalid-warning-desktop .comma').show();
    $('.deal-invalid-warning .comma').show();
  } else {
    $('.deal-invalid-warning-desktop .comma').hide();
    $('.deal-invalid-warning .comma').hide();
  }

  if (generalRequiredFields === 0 && customerRequiredFields === 0) {
    $('.deal-invalid-warning-desktop').hide();
    $('.deal-invalid-warning').hide();
  } else {
    $('.deal-invalid-warning-desktop').show();
    $('.deal-invalid-warning').show();
  }

  $('#general-info-tab').find('span').text(generalRequiredFields);
  if (generalRequiredFields === 0) {
    $('#general-info-tab').find('span').addClass('d-none');
    $('.toggle-tab-general-desktop').addClass('d-none');
    $('.toggle-tab-general').addClass('d-none');
  } else {
    $('#general-info-tab').find('span').removeClass('d-none');
    $('.toggle-tab-general-desktop').removeClass('d-none');
    $('.toggle-tab-general').removeClass('d-none');
  }
  $('#customer-portrait-tab').find('span').text(customerRequiredFields);
  if (customerRequiredFields === 0) {
    $('#customer-portrait-tab').find('span').addClass('d-none');
    $('.toggle-tab-customer-desktop').addClass('d-none');
    $('.toggle-tab-customer').addClass('d-none');
  } else {
    $('#customer-portrait-tab').find('span').removeClass('d-none');
    $('.toggle-tab-customer-desktop').removeClass('d-none');
    $('.toggle-tab-customer').removeClass('d-none');
  }
}

var initHobbiesSelect2 = function () {
  // Customer hobby dropdown list
  if ($('#hobbies').val() != '' && $('#hobbies').val() != undefined) {
    $('.select2-search-hobby').select2({
      data: JSON.parse($('#hobbies').val()),
      theme: 'bootstrap',
      language: 'vi',
      width: 'resolve',
      placeholder: 'Vui lòng chọn',
      minimumInputLength: 0,
      dropdownAutoWidth: true,
      allowClear: true,
      multiple: true,
      closeOnSelect: false,
      escapeMarkup: function (markup) {
        return markup;
      },
      templateResult: function (data) {
        if (data.id != undefined && _.includes($(`.select2-search-hobby`).val(), data.id.toString())) {
          return `<input class="icheckbox_square-custom mr-1" type="checkbox" checked="true" id="select2-custom-hobby-${data.id}"><label data-p-id="${data.parent_id}" class="mr-1 ${data.class_name}">${data.text}</label>`;
        } else if (data.disabled == true) {
          return `<label data-id="${data.id}" data-p-id="${data.parent_id}" class="mr-1 has_child ${data.class_name}">${data.text}<i class="group-action fas fa-plus"/></label>`;
        } else {
          return `<input class="icheckbox_square-custom mr-1" type="checkbox" id="select2-custom-hobby-${data.id}"><label data-p-id="${data.parent_id}" class="mr-1 ${data.class_name}">${data.text}</label>`;
        }
      }
    }).on('select2:open', function (e) {
      customStyleSelect2();
      if ($(this).data('state') === 'unselected') {
        $(this).removeData('state');
        // $(this).select2('close');
      }
      $(this).next().find('.select2-search--inline').remove();
    }).on('change.select2', function (e) {
      // TODO
    }).on("select2:select", function (e) {
      $(`#select2-custom-hobby-${e.params.data.id}`).prop('checked', true);
    }).on("select2:unselect", function (e) {
      $(`#select2-custom-hobby-${e.params.data.id}`).prop('checked', false);
      $(`#select2-custom-hobby-${e.params.data.id}`).parent().removeClass('select2-results__option--highlighted').attr('aria-selected', false);
    });
  }
  ;

  if ($('#current_hobbies').val() != '' && $('#current_hobbies').val() != undefined) {
    var currentHobbies = JSON.parse($('#current_hobbies').val());
    $('.select2-search-hobby').val(currentHobbies).trigger('change');
  }
}

function checkValidation(elm) {
  if ($(elm).length <= 0) return true;
  $(document).on('keyup change', elm, function () {
    $(this).closest('.field_with_errors').removeClass('field_with_errors');
    $(this).closest('.form-group').find('.custom-validation').hide();
  })

  if ($(elm).val() == '') {
    $(elm).closest('.swal-input').addClass('field_with_errors');
    $(elm).closest('.form-group').find('.custom-validation').show();
    $(elm).closest('.swal2-content').find('.field_with_errors .auto_focus').first().focus();
    return false;
  }
  return true;
}

// Nhập lý do huỷ trang detail
function newSwalCancelReason(oldState, newState) {
  let html = '<div class="form-group">'
    + '<div class="swal-input">'
    + '<select class="form-control tms-select2 canceled-reason">'
    + '<option value>Vui lòng chọn</option>'
    + '<option value=10>Đã mua bên khác</option>'
    + '<option value=11>Khách thay đổi nhu cầu</option>'
    + '</select>'
    + '</div>'
    + '<span class="invalid-feedback custom-validation" style="display: none">Không được để trống trường này!</span>'
    + '</div>'

  let note;
  let reason;
  swalBootstrapCanceled.fire({
    title: 'Xác nhận chuyển trạng thái "Hủy"',
    html: html,
    showConfirmButton: true,
    showCancelButton: true,
    cancelButtonText: 'Huỷ',
    confirmButtonText: 'Đồng ý',
    showCloseButton: false,
    allowOutsideClick: false,
    focusConfirm: false,
    onOpen: function () {
      $('.tms-select2').select2({
        theme: 'bootstrap swal-select2-container',
        language: 'vi',
        placeholder: 'Vui lòng chọn'
      });
    },
    preConfirm: () => {
      var canceledReasonValid = checkValidation('.canceled-reason');

      reason = $('.canceled-reason').val();
      return canceledReasonValid;
    }
  }).then((result) => {
    if (result.isConfirmed) {
      $('#deal_canceled_reason').val(reason);
      $('#save-state-btn').click();
    } else if (oldState != null) {
      allowSubmitState = false;
      $('select[name="deal[state]"]').val(oldState).trigger('change');
    }
  })
}

// Nhập lý do không quan tâm trang detail
function swalUninterestedReason(oldState, newState) {
  let html = '<div class="form-group">'
    + '<div class="swal-input">'
    + '<select class="form-control tms-select2 uninterested-reason">'
    + '<option value>Vui lòng chọn</option>'
    + '<option value=0>Thông tin không phù hợp</option>'
    + '<option value=1>Spam</option>'
    + '<option value=2>Đã mua sản phẩm tương tự</option>'
    + '<option value=3>Môi giới</option>'
    + '</select>'
    + '</div>'
    + '<span class="invalid-feedback custom-validation" style="display: none">Không được để trống trường này!</span>'
    + '</div>'

  let note;
  let reason;
  swalBootstrapUninterested.fire({
    title: 'Xác nhận chuyển trạng thái "Không quan tâm"',
    html: html,
    showConfirmButton: true,
    showCancelButton: true,
    cancelButtonText: 'Huỷ',
    confirmButtonText: 'Đồng ý',
    showCloseButton: false,
    allowOutsideClick: false,
    focusConfirm: false,
    onOpen: function () {
      $('.tms-select2').select2({
        theme: 'bootstrap swal-select2-container',
        language: 'vi',
        placeholder: 'Vui lòng chọn'
      });
    },
    preConfirm: () => {
      var canceledReasonValid = checkValidation('.uninterested-reason');

      reason = $('.uninterested-reason').val();
      return canceledReasonValid;
    }
  }).then((result) => {
    if (result.isConfirmed) {
      $('#deal_uninterested_reason').val(reason);
      $('#save-state-btn').click();
    } else if (oldState != null) {
      allowSubmitState = false;
      $('select[name="deal[state]"]').val(oldState).trigger('change');
    }
  })
}

// Nhập lý do không quan tâm trang list
function listPageSwalUninterestedReason(dnd, item) {
  let html = '<div class="form-group">'
    + '<div class="swal-input">'
    + '<select class="form-control tms-select2 uninterested-reason">'
    + '<option value>Vui lòng chọn</option>'
    + '<option value=0>Thông tin không phù hợp</option>'
    + '<option value=1>Spam</option>'
    + '<option value=2>Đã mua sản phẩm tương tự</option>'
    + '<option value=3>Môi giới</option>'
    + '</select>'
    + '</div>'
    + '<span class="invalid-feedback custom-validation" style="display: none">Không được để trống trường này!</span>'
    + '</div>'

  let note;
  let reason;
  let reasonText;
  swalBootstrapUninterested.fire({
    title: 'Xác nhận chuyển trạng thái "Không quan tâm"',
    html: html,
    showConfirmButton: true,
    showCancelButton: true,
    cancelButtonText: 'Huỷ',
    confirmButtonText: 'Đồng ý',
    showCloseButton: false,
    allowOutsideClick: false,
    focusConfirm: false,
    onOpen: function () {
      $('.tms-select2').select2({
        theme: 'bootstrap swal-select2-container',
        language: 'vi',
        placeholder: 'Vui lòng chọn'
      });
    },
    preConfirm: () => {
      var uninterestedReasonValid = checkValidation('.uninterested-reason');
      reason = $('.uninterested-reason').val();
      reasonText = $('.uninterested-reason option:selected').text();
      return uninterestedReasonValid;
    }
  }).then((result) => {
    let dealIdCurrent;
    let dealIdAbove;
    if (result.isConfirmed) {
      dealIdCurrent = item.attr('data-id');
      dealIdAbove = null;
      if (item.prev()) {
        dealIdAbove = item.prev().attr('data-id');
      }
      $.changeStateUninterested({
        deal_id: dealIdCurrent,
        deal_id_above: dealIdAbove,
        state: 'uninterested',
        uninterested_reason: reason,
        onSuccess: function () {
          item.find('.card-header .statuses').remove();
          item.find('.card-header').append('<div class="statuses"><div class="statuses-container"><div class="status font-size-14 status-uninterested">' + reasonText + '</div></div></div>')
        }
      });
    } else {
      if (dnd != null) {
        dnd.sortable('cancel');
      }
    }
  })
}

// Nhập lý do huỷ trang list
function swalCancelReason(dnd, item) {
  $(document).off('focusin.modal');
  html = '<div class="form-group">'
    + '<div class="swal-input">'
    + '<select class="form-control tms-select2 canceled-reason">'
    + '<option value>Vui lòng chọn</option>'
    + '<option value=10>Đã mua bên khác</option>'
    + '<option value=11>Khách thay đổi nhu cầu</option>'
    + '</select>'
    + '</div>'
    + '<span class="invalid-feedback custom-validation" style="display: none">Không được để trống trường này!</span>'
    + '</div>'

  let note;
  let reason;
  let reasonText;
  swalBootstrapCanceled.fire({
    title: 'Xác nhận chuyển trạng thái "Hủy"',
    html: html,
    showConfirmButton: true,
    showCancelButton: true,
    cancelButtonText: 'Huỷ',
    confirmButtonText: 'Đồng ý',
    showCloseButton: false,
    allowOutsideClick: false,
    focusConfirm: false,
    onOpen: function () {
      $('.tms-select2').select2({
        theme: 'bootstrap swal-select2-container',
        language: 'vi',
        placeholder: 'Vui lòng chọn'
      });
    },
    preConfirm: () => {
      var canceledReasonValid = checkValidation('.canceled-reason');
      reason = $('.canceled-reason').val();
      reasonText = $('.canceled-reason option:selected').text();
      return canceledReasonValid;
    }
  }).then((result) => {
    let dealIdCurrent;
    let dealIdAbove;
    if (result.isConfirmed) {
      dealIdCurrent = item.attr('data-id');
      dealIdAbove = null;
      if (item.prev()) {
        dealIdAbove = item.prev().attr('data-id');
      }
      $.changeStateCanceled({
        deal_id: dealIdCurrent,
        deal_id_above: dealIdAbove,
        state: 'canceled',
        canceled_reason: reason,
        onSuccess: function () {
          item.find('.card-header .statuses').remove();
          item.find('.card-header').append('<div class="statuses"><div class="statuses-container"><div class="status font-size-14 status-canceled">' + reasonText + '</div></div></div>')
        }
      });
    } else {
      if (dnd != null) {
        dnd.sortable('cancel');
      }
    }
  })
}

function swalUpdateCommission(dealId) {
  $(document).off('focusin.modal');
  $.ajax({
    url: '/deals/get_comm_info',
    type: "GET",
    data: {
      id: dealId
    },
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },
    success: function (data) {
      if (data.status == 'success' && data.can_update_comm == true) {
        let cl = 'col-8';
        if (device === 'mobile') {
          cl = 'col-12';
        }
        let html = '<div class="row m-0 pt-2 pb-2">'
          + '<div class="col-2"></div>'
          + `<div class="${cl}">`
          + '<div class="form-group text-left">'
          + '<label class="detail-item-heading-sub" for="swal_deal_total_price">Giá trị giao dịch (vnđ)<span class="required">*</span></label>'
          + `<input class="form-control input-price-decimal" data-inputmask="'alias': 'decimal', 'groupSeparator': '.'" type="text" id="swal_deal_total_price" value=${data.comm_fields.total_price}>`
          + '<span class="invalid-feedback custom-validation" style="display: none">Không được để trống trường này!</span>'
          + '</div>'
          + '<div class="form-group text-left">'
          + '<label class="detail-item-heading-sub" for="swal_deal_total_price">Ngày ký hợp đồng GD<span class="required">*</span></label>'
          + `<input class="form-control" step="0" min="0" type="text" id="swal_deal_signed_at" readonly value=${data.comm_fields.contract_signed_at}>`
          + '<span class="invalid-feedback custom-validation" style="display: none">Không được để trống trường này!</span>'
          + '</div>'
          + '</div>'
          + '<div class="col-2"></div>'
          + '</div>';

        var title = 'Vui lòng cập nhật thông tin giao dịch để chuyển sang trạng thái </br> "Đã ký hợp đồng"';
        if (data.current_state === 'contract_signed') {
          title = 'Vui lòng bổ sung thông tin giao dịch để cập nhật </br> "Hoa hồng"';
        }

        swalBootstrapUpdateCommission.fire({
          title: title,
          html: html,
          showConfirmButton: true,
          showCancelButton: true,
          cancelButtonText: 'Hủy',
          confirmButtonText: 'Lưu lại',
          showCloseButton: false,
          allowOutsideClick: false,
          focusConfirm: false,
          onOpen: function () {
            $('.input-price-decimal').inputmask({
              rightAlign: false,
              removeMaskOnSubmit: true,
              groupSeparator: '.',
              radixPoint: '',
              digit: 3,
              placeholder: ""
            })
            $('#swal_deal_signed_at').daterangepicker({
              singleDatePicker: true,
              opens: 'left',
              minYear: 2019,
              maxYear: 2119,
              drops: "up",
              locale: {
                format: 'DD/MM/YYYY'
              }
            }, function (start, end, label) {
              var years = moment().diff(start, 'years');
            });
            if (data.comm_fields.contract_signed_at !== '') {
              $('#swal_deal_signed_at').data('daterangepicker').setStartDate(data.comm_fields.contract_signed_at);
            }
          },
          preConfirm: () => {
            var totalCommValid = checkValidation('#swal_deal_total_price');
            var signedAtCommValid = checkValidation('#swal_deal_signed_at');
            return totalCommValid && signedAtCommValid;
          }
        }).then((result) => {
          if (result.isConfirmed) {
            var commissionData = {
              total_price: $('#swal_deal_total_price').val(),
              signed_at: $('#swal_deal_signed_at').val()
            }
            $.ajax({
              url: '/deals/update_commission',
              type: "POST",
              data: {
                id: dealId,
                value: commissionData,
                field: 'all'
              },
              headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
              },
              success: function (data) {
                $.changeState({
                  deal_id: dealId,
                  state: 'contract_signed'
                });
              }
            });
          } else {
            $('input[name="drag_state_changed"]').val('');
            $.ajax({
              url: '/deals/' + dealId,
              type: "patch",
              data: {
                refresh: true,
              },
              headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
              },
              success: function (data) {
              }
            });
          }
        })
      } else if (data.status == 'success' && data.can_update_comm == false) {
        $.changeState({
          deal_id: dealId,
          state: 'contract_signed'
        });
      }
    }
  });
}

function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

var getDataAssignee = function () {
  // Show and search assignee
  var assigneeSuggestion = new Bloodhound({
    limit: 10,
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: window.location.origin + '/deals/assignee_search?query=%QUERY',
      filter: function (data) {
        return data;
      },
      wildcard: "%QUERY"
    }
  });

  assigneeSuggestion.initialize();
  var assigneeTypeahead = $('#kanban-assign .input-search-assignee');
  assigneeTypeahead.typeahead('destroy');
  assigneeTypeahead.typeahead({
      hint: true,
      highlight: true,
      minLength: 0
    },
    {
      limit: 500,
      name: 'value',
      displayKey: 'value',
      source: assigneeSuggestion.ttAdapter(),
      templates: {
        empty: '<div class="noitems">Không tìm thấy kết quả</div>',
        suggestion: function (data) {
          if (($(`[data-assignee-id]`).length === 0 || data._query.length > 0) && data.id === null ) {
            return `<div class="d-none"</div>`;
          } else {
          return `<div class="assign-item" data-value="${data.id}">${data.text} <br/><label class="info-item-label m-0">${data.email}</label></div>`;
          }
        }
      }
    });
}

// tinh toan lai
function setNumColumn() {
  $('.kanban-col').each(function (index) {
    let numItem = $(this).find('.kanban-item:not(.kanban-clone)').length;
    $(this).find('.num-item').html(`(${numItem})`);
  });
}

var customDateTimePicker = function (elm) {
  if (elm.length > 0) {
    var defaultDate = elm.find('input').val();
    if (defaultDate === '') defaultDate = moment(new Date()).format('DD/MM/YYYY - HH:mm');
    elm.daterangepicker({
      "singleDatePicker": true,
      "minYear": 2019,
      "maxYear": 2119,
      "timePicker": true,
      "timePicker24Hour": true,
      "timePickerIncrement": 1,
      "locale": {
        "format": "DD/MM/YYYY - HH:mm",
        "applyLabel": "Chọn",
        "cancelLabel": "Huỷ",
        "daysOfWeek": ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
        "monthNames": ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
        "firstDay": 1
      },
      "startDate": defaultDate,
      "endDate": defaultDate,
      "opens": "center",
      "drops": "auto"
    }).on('apply.daterangepicker', function (ev, picker) {
      $(this).find('input').val(picker.startDate.format("DD/MM/YYYY - HH:mm")).trigger('input');
    }).on('showCalendar.daterangepicker', function (ev, picker) {
      $('.hourselect').find('option').each(function () {
        if ($(this).val() < 6 || $(this).val() > 21) {
          $(this).remove();
        }
      })
    });
  }
}

function desktopAutoFocusWhenError() {
  window.setTimeout(function () {
    var error = $('.tab-pane.active.show form .field_with_errors').eq(1);
    if (error.length) {
      $('.modal-body .kanban-scroll').first().animate({
          scrollTop: (error.offset().top - $('.modal-body .kanban-scroll').first().offset().top - 40)
        },
        {
          duration: 600,
          complete: function () {
            if (error.find('input, textarea').length) {
              error.find('input, textarea').focus()
            }
            if (error.find('select').length) {
              error.find('select').select2('open');
            }
          }
        });
    }
  }, 500);
}

function mobileAutoFocusWhenError() {
  window.setTimeout(function () {
    var error = $('.active.tab-pane form .field_with_errors').eq(1);
    if (error.length) {
      $('html, body').animate({
          scrollTop: (error.offset().top - 50)
        },
        {
          duration: 600,
          complete: function () {
            if (error.find('input, textarea').length) {
              error.find('input, textarea').focus()
            }
            if (error.find('select').length) {
              error.find('select').select2('open');
            }
          }
        });
    }
  }, 500);
}

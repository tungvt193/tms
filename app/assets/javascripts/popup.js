$(document).on('change', '.tms-popup-state-select2', function(){
  if ($(this).val() == 'true' && $(this).data('confirm') == true) {
    swalWithBootstrapButtons.fire({
      title: 'Bạn có chắc chắn muốn hiển thị?',
      html: `Chọn "Đồng ý" đồng nghĩa rằng popup ${$(this).data('popup')} sẽ bị ẩn khỏi ${$(this).data('page')}. Bạn có muốn thay đổi không?`,
      showCloseButton: true,
      showCancelButton: true,
      confirmButtonText: 'Đồng ý',
      cancelButtonText: 'Hủy',
      reverseButtons: true,
      allowOutsideClick: false
    }).then((result) => {
      if (!result.isConfirmed) {
        if ($(this).hasClass('tms-select2')) {
          $(this).val("false").trigger("change");
        }else{
          $(this).closest('.form-group').find("input[value='false']").prop("checked", true);
        }
      }
    })
  }
})

$(document).on('click', '.btn-submit-guide-tour', function(){
  $('.active.fade.show.tab-pane').find('.btn-guide-tour-submit').click();
})

$(document).on('click', '.editable-popup', function() {
  editor = CKEDITOR.replace(this);
  editor.on('blur', function(e)
  {
    e.editor.destroy();
  });
});

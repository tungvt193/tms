$(document).on('turbolinks:load', function () {
  var accountSuggestion = new Bloodhound({
    limit: 10,
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: '../accounts/autocomplete?query=%QUERY',
      filter: function(data) {
        return data;
      },
      wildcard: "%QUERY"
    }
  });
  accountSuggestion.initialize();
  var accountTypeahead = $('#account-search');
  accountTypeahead.typeahead({
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      limit: 500,
      name: 'value',
      displayKey: 'value',
      source: accountSuggestion.ttAdapter(),
      templates: {
        empty: '<div class="noitems">Không tìm thấy kết quả</div>'
    }
  });

  check_account_lock();
  $('[name="account[active]"]').change(function(){
    check_account_lock();
  });
});

function check_account_lock(){
  if ($('[name="account[active]"]').val() == 'false'){
    $('.card_reason_lock').show();
  } else {
    $('.card_reason_lock').hide();
  }
}
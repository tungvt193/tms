var polygons = [];
var rooms = {};
var currentPolygon = null;
var pointW = 10;
var pointT = 5;
var lineH = 2;
var newScale = 1;

// TODO: new logic
var pickable = false;
$( document ).ready(function() {
  $('#layout_image_drawing').mapify();
  if ($('#layout_image_drawing') != undefined && $('#layout_image_drawing').length > 0) {
    setImageSize($('#layout_image_drawing'));
  }

  $('.btn-add-room').click(function(e) {
    clearCurrentPolygon();
    pickable = true;
  })

  $('#layout_image_drawing').click(function(e) {
    if (pickable) draw(e);
  });

  $(document).on('click', '.circle-point', function() {
    if (currentPolygon == null) return;
    if (currentPolygon.isTrailingPoint(this)) {
      Swal.fire({
        title: 'Xoá toạ độ?',
        text: "Toạ độ vừa chọn sẽ bị xoá!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Xoá',
        cancelButtonColor: '#d33',
      }).then((result) => {
        if (result.value) {
          $('.circle-point:last').remove();
          $('.point-connecting-line:first').remove();
          currentPolygon.removePoint(currentPolygon.trailingPoint());
        }
      })
    } else if (currentPolygon.isEnd(this)) {
      currentPolygon.drawConnectingLineEndToStart();
      Swal.fire({
        title: 'Gắn mã căn hộ',
        input: 'text',
        showCancelButton: true,
        confirmButtonText: 'Cập nhật',
        showLoaderOnConfirm: true,
        cancelButtonText: 'Huỷ',
        allowOutsideClick: false,
        preConfirm: (apartmentNo) => {
          var roomIds = $('.layout-info .code').map( function() {
            var $this = $(this);
            if ($this.closest('.nested-fields').is(':visible')) {
              return $this.val().toLowerCase();
            }
          });

          if (apartmentNo == "") {
            Swal.showValidationMessage(
              'Chưa nhập mã căn'
            )
          } else if ($.inArray(apartmentNo.toLowerCase(), roomIds) != -1) {
            Swal.showValidationMessage(
              'Căn đã tồn tại'
            )
          }
        }
      }).then((result) => {
        if (result.value) {
          currentPolygon.roomId = result.value;
          polygons.push(currentPolygon);

          $('#coordinates .nested-fields:last .code').val(result.value);
          $('#coordinates .nested-fields:last .positions').val(currentPolygon.coords());

          var newRoom = new Room(result.value, currentPolygon.coords(), $('.multi-rooms'));
          newRoom.addElement();

          rooms[result.value] = newRoom;
          currentPolygon.insertCoords();
          reloadMapify($('#layout_image_drawing'));

          pickable = false;
          // check eq coordinate show error
          let isDisabled = $('.btn-add-room').attr('disabled');
          if (isDisabled){
            $('#coordinates .eq-coordinates').removeClass('d-none');
          }

        }
        clearCurrentPolygon();
      })
    }
  });

  var clearCurrentPolygon = function() {
    currentPolygon = null;
    $('.circle-point').remove();
    $('.point-connecting-line').remove();
  }

  $(document).on('click', '.btn-remove-room', function() {
    var roomId = $(this).closest('.nested-fields').find('.code').val();
    $('area[data-id="' + roomId + '"]').remove();
    reloadMapify($('#layout_image_drawing'));
  });

  $('button.zoom-button').click(function(e) {
    var zoom = $(this).attr('data-zoom') === 'in' ? -1 : 1;
    var scale = parseFloat($('input[name="scale"]:checked').attr('data-scale')) || 1;
    newScale = scale + zoom * 0.25;

    var containerW = '720px';
    var containerH = '500px';
    if (newScale >= 1) {
      containerW = '735px';
      containerH = '515px';
      $('#zoom-container').css('overflow', 'auto');
    } else {
      $('#zoom-container').css('overflow', 'hidden');
    }

    $("#zoom-container").css('width', containerW);
    $("#zoom-container").css('height', containerH);

    if (newScale < 0.5 || newScale > 10) return;
    $('input[name="scale"]').removeAttr('checked');
    $('.zoom-control .zoom-percentage').text(newScale * 100 + '%');

    //resize point and line
    $('.circle-point').css({
      'transform': 'scale(' + 1/newScale +')'
    })
    $('.point-connecting-line').css({
      'height': lineH/newScale + 'px'
    })

    $('input[data-scale="' + newScale.toFixed(2) + '"]').click();
  })
});

var setImageSize = function (container) {
  var img = new Image();
  img.src = container.attr('src');
  img.onload = function() {
    var newHeight = parseInt(this.height/(this.width/container.width()));
    container.attr('width', container.width() + 'px');
    container.attr('height', newHeight + 'px');
  }
}

var reloadMapify = function (container) {
  container.removeClass('mapify');
  container.recreate_mapify({});
}

var Room = function(id, coords, container) {
  this.id = id;
  this.coords = coords;
  this.container = container;
}

$.extend(Room.prototype, {
  addElement: function() {
    var _this = this;
    var addElm = $('.multi-room:first-child').clone(true).prependTo(_this.container);
    addElm.find('input').each(function(index) {
      if (index == 0) {
        $(this).val(_this.id);
      } else {
        $(this).val(_this.coords);
      }
      $(this).attr('data-id', _this.id);
    })
    addElm.show();
  }
});

var Point = function(x, y, element) {
  this.x = parseFloat(x);
  this.y = parseFloat(y);
  this.self = $(element);
  this.polygon = null;
}

$.extend(Point.prototype, {
  prev: function() {
    if (this.polygon == null || !this.self.prev().hasClass('circle-point')) return null;
    var prevElm = this.self.prev();
    var X = parseInt(prevElm.css('left'));
    var Y = parseInt(prevElm.css('top'));
    return this.polygon.points.find(point => parseInt(point.x) === X && parseInt(point.y) === Y);
  }
});

var Polygon = function(points, container) {
  this.container = container;
  this.points = points;
  this.apartmentNo = '';
}

$.extend(Polygon.prototype, {
  addPoint: function(point) {
    this.points.push(point);
    point.polygon = this;
  },
  removePoint: function(point) {
    this.points = this.points.filter( function(p) {
      return p.x !== point.x || p.y !== point.y;
    });
  },
  drawPoint: function(point) {
    //resize point
    $("<span class='circle-point' title='Click điểm cuối để xoá' style='position:absolute; left: " + point.x + "px; top: " + point.y + "px; transform: scale(" + 1/newScale +");'/>").insertBefore(this.container);
    point.self = $('.circle-point').last();
    this.drawConnectingLine(point);
  },
  drawConnectingLine: function(point) {
    var prevPoint = point.prev();
    if (prevPoint != null) drawLine(prevPoint, point, this.container);
  },
  drawConnectingLineEndToStart: function() {
    drawLine(this.points[this.points.length - 1], this.points[0], this.container);
  },
  startPoint: function() {
    return this.points[0];
  },
  trailingPoint: function() {
    return this.points[this.points.length - 1];
  },
  isEnd: function(element) {
    if (this.points.length <= 2) return false;
    var _this = $(element);
    if (!_this.hasClass('circle-point')) return false;
    var scale = parseFloat($('input[name="scale"]:checked').attr('data-scale'));
    var X = parseInt(_this.css('left'));
    var Y = parseInt(_this.css('top'));
    var startPoint = this.startPoint();
    if (parseInt(startPoint.x) == X && parseInt(startPoint.y) == Y) return true;
    return false;
  },
  isTrailingPoint: function(element) {
    if (this.points.length == 0) return false;
    var _this = $(element);
    if (!_this.hasClass('circle-point')) return false;
    var scale = parseFloat($('input[name="scale"]:checked').attr('data-scale'));
    var X = parseInt(_this.css('left'));
    var Y = parseInt(_this.css('top'));
    var trailingPoint = this.trailingPoint();
    if (parseInt(trailingPoint.x) == X && parseInt(trailingPoint.y) == Y) return true;
    return false;
  },
  coords: function() {
    var c = [];
    $.each(this.points, function( index, point ) {
      c.push(point.x);
      c.push(point.y);
    });
    return c.join(', ');
  },
  insertCoords: function() {
    $("<area data-id='" + this.roomId + "' alt='" + this.roomId + "' data-title='" + this.roomId + "' shape='poly' coords='" + this.coords() + "'/>").appendTo($('#project_layout_image'));
  },
  removeCoords: function() {
    $('area[data-id="' + this.roomId + '"]').remove();
  }
});

var draw = function (e) {
  var offset = $(e.target).offset();
  var X = (e.pageX - offset.left);
  var Y = (e.pageY - offset.top);

  if (currentPolygon == undefined || currentPolygon == null) {
    currentPolygon = new Polygon([], e.target);
  }

  var scale = parseFloat($('input[name="scale"]:checked').attr('data-scale'));
  var point = new Point(parseFloat((X/scale).toFixed(2)), parseFloat((Y/scale).toFixed(2)), e.target);
  currentPolygon.addPoint(point);
  currentPolygon.drawPoint(point);
}

// Draw line
var lineDistance = function (x, y, x0, y0){
  return Math.hypot(y - y0, x - x0);
};

var drawLine = function (startPoint, endPoint, container) {
  var angle = Math.atan2(endPoint.y - startPoint.y, endPoint.x - startPoint.x) * 180 / Math.PI;
  var distance = lineDistance(startPoint.x, startPoint.y, endPoint.x, endPoint.y);

  var top = Math.round((endPoint.y + startPoint.y)/2 * 100)/100;;
  var left = Math.round((endPoint.x + startPoint.x - distance)/2 * 100)/100;

  $("<span class='point-connecting-line' style='position:absolute; left: " + left + "px; top: " + top + "px; height: " + lineH/newScale + "px;'/>").insertAfter(container);

  var $line = $('.point-connecting-line').first();
  $line.css('transform', 'rotate(' + angle + 'deg)');
  $line.css('width', distance + 'px');
}

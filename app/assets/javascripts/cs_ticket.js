$(document).on('turbolinks:load', function() {
  $(document).on('click', '.download_deal', function() {
    $(this).prop('disabled', true);
    var state = $(this).data('state');
    $.ajax({
      url: "/cs_tickets/download_csv?state=" + state,
      method: "get",
      dataType: "json",
      success: function(e) {
        toastr.success(e.message);
        $('.download_deal').prop('disabled', false);
      }
    })
  })
})
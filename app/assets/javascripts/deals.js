var oldState = null;
var allowSubmitState = true;
var unchecked_deal_fields = [];
$.datetimepicker.setLocale('vi');
$(document).ready(function () {
  if (getDevice() === 'tablet') {
    $('.show-deal-detail').addClass('d-none');
  }
  if ($('#modal-edit').length > 0) {
    getDataAssignee();
    initHobbiesSelect2();
    // Alert before change tab
    $(document).on('hide.bs.tab', '#general-info-tab, #customer-portrait-tab, #history-interaction-tab, #appointment-tab, #commission-tab', function (e) {
      var isEditing = false;
      if ($(e.target).attr("href") === "#appointment" || $(e.target).attr("href") === "#history-interaction") {
        isEditing = $('.btn-save-nested:visible').length > 0;
      } else {
        var targetForm = $(e.target).attr("href") + ' form';
        isEditing = $(targetForm).data('is-editing');
      }

      if (isEditing) {
        e.preventDefault();
        swalWithBootstrapButtons.fire({
          title: 'Bạn chưa lưu thay đổi',
          html: "Các thay đổi của bạn chưa được lưu lại.<br/>Vui lòng <strong>Lưu lại</strong> hoặc <strong>Huỷ</strong> các thay đổi hiện tại!",
          showCloseButton: true,
          confirmButtonText: 'Đóng',
          reverseButtons: true,
          allowOutsideClick: false
        })
      }
    });

    // Auto focus source selection when not yet set
    if ((!$('.source-select2').val() && $('.source-select2').attr('require') === 'true')) {
      if (!$('.source-select2').val() && $('.source-select2').attr('require') === 'true' && !$('.source-select2').select2("isOpen")) {
        $('.container-source-select2').addClass('d-block');
        $('.source-select2').select2('open');
      }
    }

    if (!$('#deal_source_mobile').attr('deal-source')) {
      $('#change_deal_source').click();
    }

    // Initial
    countRequiredAttributes();
    $('.kanban-item').removeClass('active');
    $('.btn-show-detail i').removeClass('fa-chevron-left').addClass('fa-chevron-right');
    $('.modal-backdrop').remove();
    oldState = $('select[name="deal[state]"]').val();
    let error = $('#modal-edit').data('error');
    if (error) {
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: error,
        showConfirmButton: false,
        showClass: {
          popup: 'swal-create-customer',
        },
        timer: 2000
      });
      return false;
    }
    if (getParameterByName('auto_focus') === 'assignee') {
      $('#modal-edit #assignee').click();
    }

    if ($('.appointment-late').length > 0) {
      reportAppointment();
    }

    $('.datetimepicker').datetimepicker({
      format: 'd/m/Y H:i',
      lang: 'vi'
    });

    oldState = $('select[name="deal[state]"]').val();
  }
})

// Change assignee
$(document).on('typeahead:selected', '#kanban-assign .input-search-assignee', function (data, selectedItem) {
  let text = selectedItem.text;
  var selectedHtml = '<div class="assign-item" data-value="' + selectedItem.id + '">' +
    '<div class="btn p-0 image-avatar">' +
    '<img alt="User Default Avatar" class="img-circle avatar-assign" src=' + selectedItem.avatar + '>' +
    '</div>' +
    selectedItem.text +
    '</div>'
  if (selectedItem.id === null) {
    selectedHtml = '<div class="assign-item" data-value="' + selectedItem.id + '">' +
      '<div class="btn p-0 image-avatar">' +
      '<img alt="User Default Avatar" class="img-circle w-100 avatar-assign" src="https://reti-tms-staging.s3.ap-southeast-1.amazonaws.com/assets/add-assignee.svg">' +
      '</div>' +
      "Chưa cập nhật" +
      '</div>'
  }
  $('#assignee-id').attr('value', selectedItem.id);
  $('#deal_assignee_id').val(selectedItem.id);
  $('#kanban-assign').toggle('hide');
  var dealId = $('#modal-edit').data('id');
  var data = {deal: {assignee_id: selectedItem.id}};
  var successCallback = function (res) {
    if (res.status === 'ok') {
      $('.team-lead-content').empty().append(res.team_lead);
      $('#assignee').empty().append(selectedHtml);
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: "Bạn đã phân công cho " + text,
        showConfirmButton: false,
        showClass: {
          popup: 'swal-create-customer',
        },
        timer: 2000
      });
      $('.info-item-group.dropdown-assign').find('.info-item-content').attr('data-assignee-id', selectedItem.id)
    } else if (res.status === 'cancel') {
      $('#assignee').empty().append(selectedHtml);
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: "Bạn đã xoá người phân công cho giao dịch",
        showConfirmButton: false,
        showClass: {
          popup: 'swal-create-customer',
        },
        timer: 2000
      });
      $('.info-item-group.dropdown-assign').find('.info-item-content').removeAttr('data-assignee-id')
    }
    getDataAssignee();
  }
  var failureCallback = function (res) {
    // Do nothing
  }
  $ajax(`/deals/${dealId}/update_assignee`, 'patch', data).then(successCallback, failureCallback);
});

$(document).on('click', 'a.toggle-tab-general-desktop', function () {
  $('#general-info-tab').click();
  desktopAutoFocusWhenError();
})

$(document).on('click', 'a.toggle-tab-customer-desktop', function () {
  $('#customer-portrait-tab').click();
  desktopAutoFocusWhenError();
})

$(document).on('turbolinks:load', function () {
  if ($('.init-kanban-deals').length) {
    let userAgent = navigator.userAgent.toLowerCase();
    let iOS = /(ipad|iphone|ipod)/g.test(userAgent) || (navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1);
    let android = /(android)/g.test(userAgent);
    let isMobile = iOS || android;

    if (!isMobile) {
      let otherColumn;
      let prevState;
      $('.kanban-body-container').sortable({
        containment: '.content',
        forcePlaceholderSize: true,
        zIndex: 1,
        placeholder: 'sort-highlight',
        start: function (event, ui) {
          $('.kanban-body-container').sortable('refreshPositions');
          ui.item.addClass('item-sorting');
          prevState = ui.item.closest('.kanban-col').data('state');
        },
        stop: function (event, ui) {
          ui.item.removeClass('item-sorting');
          let nextState = ui.item.closest('.kanban-col').data('state');
          if ($('.init-kanban-deals').data('policy-change-state') == false) {
            $(this).sortable('cancel');
          }
          if (nextState === 'canceled' && otherColumn && prevState !== 'uninterested' && prevState !== 'contract_signed') {
            swalCancelReason($(this), ui.item);
          }
          if (nextState === 'uninterested' && otherColumn && prevState === 'pending') {
            listPageSwalUninterestedReason($(this), ui.item);
          }
          if ((prevState === 'canceled' || prevState === 'uninterested' || prevState === 'contract_signed' || (prevState !== 'pending' && nextState === 'uninterested')) && otherColumn) {
            $(this).sortable('cancel');
          }
        },
        update: function (event, ui) {
          if (ui.sender == null) {
            // cap nhat vi tri
            let dealIdCurrent = ui.item.attr('data-id');
            let dealIdAbove = null;
            if (ui.item.prev()) {
              dealIdAbove = ui.item.prev().attr('data-id');
            }
            let stateUpdate = ui.item.closest('.kanban-col').attr('data-state');
            if (otherColumn) {
              if (prevState !== 'canceled' && prevState !== 'uninterested' && prevState !== 'contract_signed') {
                if (ui.item.closest('.kanban-col').data('state') !== 'canceled' && ui.item.closest('.kanban-col').data('state') !== 'uninterested') {
                  $.changeState({
                    deal_id: dealIdCurrent,
                    deal_id_above: dealIdAbove,
                    state: stateUpdate
                  });
                  $(`.kanban-col[data-state="${prevState}"]`).find('.num-item').html('(' + (parseInt($(`.kanban-col[data-state="${prevState}"]`).find('.num-item').text().replace("(", "").replace(")", "")) - 1) + ')');
                  $(`.kanban-col[data-state="${stateUpdate}"]`).find('.num-item').html('(' + (parseInt($(`.kanban-col[data-state="${stateUpdate}"]`).find('.num-item').text().replace("(", "").replace(")", "")) + 1) + ')');
                }
              }
            } else {
              $.reorder({
                deal_id: dealIdCurrent,
                deal_id_above: dealIdAbove,
                state: stateUpdate
              });
            }
          }
        },
        beforeStop: function (ev, ui) {
          ui.item.removeClass('item-sorting');
          otherColumn = $(ui.placeholder).parent()[0] !== undefined && $(ui.placeholder).parent()[0] != this && $('.init-kanban-deals').data('policy-change-state')
        },
        connectWith: ".container-sortable",
        items: '>.kanban-item'
      }).disableSelection();
      setHeightKanbanColumn();
    }
  }

  if ($('#modal-edit').length || $('#modal-transfer').length) {
    $('.select2-deal-project, .select2-deal-product, .select2-product-type, .select2-product-selection-criteria, .select2-deal-canceled-reason, .select2-deal-assignee').select2({
      theme: 'bootstrap',
      language: 'vi',
      allowClear: true
    });

    $('.select2-deal-state').select2({
      theme: 'bootstrap',
      language: 'vi',
      allowClear: false
    });

    if ($('#modal-transfer').length && $('.select2-deal-state').val() && $('.select2-deal-assignee' && $('.select2-deal-customer')).val()) {
      $('.btn-transfer-deal').attr('disabled', false).addClass('btn-reti-primary transfer-enabled').removeClass('transfer-disabled');
    }

    $('.select2-deal-product').select2({
      ajax: {
        url: window.location.origin + '/deals/product_search',
        dataType: 'json',
        data: function (params) {
          // Query parameters will be ?search=[term]
          return {
            search: params.term,
            project_id: $('#deal_project_id').val(),
            deal_id: $('#current_deal_id').val()
          };
        },
        processResults: function (data) {
          return {
            results: data
          };
        },
        cache: true
      },
      theme: 'bootstrap',
      language: 'vi',
      minimumInputLength: 1,
    });

    $('.select2-product-type').select2({
      ajax: {
        url: window.location.origin + '/deals/product_type_search',
        dataType: 'json',
        data: function (params) {
          return {
            search: params.term,
            project_id: $('#deal_project_id').val()
          };
        },
        processResults: function (data) {
          return {
            results: $.map(data, function (item) {
              return {
                text: item.text,
                id: item.id,
                criteria: item.criteria
              };
            })
          };
        },
        cache: true
      },
      theme: 'bootstrap',
      language: 'vi',
      minimumInputLength: 0,
    }).on('select2:select', function (e) {
      let data = e.params.data;
      $('.select2-product-selection-criteria').val(null).trigger('change');
      $('.select2-product-selection-criteria').html('').select2({data: [{id: '', text: ''}]});
      if (data.criteria != null) {
        $('.group-product-selection-criteria').removeClass('d-none');
        let options = [];
        $.each(data.criteria, function (key, value) {
          options.push({id: key, text: value})
        });
        $('.select2-product-selection-criteria').select2({
          theme: 'bootstrap',
          language: 'vi',
          data: options
        });
      } else {
        $('.group-product-selection-criteria').addClass('d-none');
      }
    });

    var customerArr = [];
    $('.select2-deal-customer').select2({
      ajax: {
        url: window.location.origin + '/deals/customer_search',
        dataType: 'json',
        data: function (params) {
          // Query parameters will be ?search=[term]
          return {
            search: params.term
          };
        },
        processResults: function (data) {
          customerArr = data;
          var dataResults = [];
          _.forEach(data, function (value) {
            value.html = '<div class="select2-customer-phone-result">' + value.text + '<p>- ' + value.phone_number + '</p></div>';
            value.title = value.text;
            dataResults.push(value);
          });
          return {
            results: dataResults
          };
        },
        cache: true
      },
      theme: 'bootstrap',
      language: 'vi',
      minimumInputLength: 1,
      escapeMarkup: function (markup) {
        return markup;
      },
      templateResult: function (data) {
        if (data.html == undefined) {
          return data.text;
        } else {
          return data.html;
        }
      },
      templateSelection: function (data) {
        return data.text;
      }
    }).on('change.select2', function (e) {
      let customer = customerArr.find(x => x.id == $(this).val());
      $('.deal-phone-number').val(customer != undefined ? customer.phone_number : '');
      $('.deal-customer-name').val(customer != undefined ? customer.text : '');

      // Only transfer deal screen
      if ($('.select2-deal-state').val() && $('.select2-deal-assignee').val()) {
        $('.btn-transfer-deal').attr('disabled', false).addClass('btn-reti-primary transfer-enabled').removeClass('transfer-disabled');
      }
    });

    if ($(".tms-icheck").length > 0) {
      $(".modal-edit .tms-icheck").each(function () {
        var $el = $(this);
        var skin = ($el.attr('data-skin') !== undefined) ? "_" + $el.attr('data-skin') : "",
          color = ($el.attr('data-color') !== undefined) ? "-" + $el.attr('data-color') : "";
        var opt = {
          checkboxClass: 'icheckbox' + skin + color,
          radioClass: 'iradio' + skin + color,
        };
        $el.iCheck(opt);
      });
    }

    $('.tms-select2').select2({
      theme: 'bootstrap',
      language: 'vi'
    });

    $('[name="deal[customer_persona_attributes][real_estate_type_to_invest][]"]').select2({
      theme: 'bootstrap',
      language: 'vi',
      placeholder: $('[name="deal[customer_persona_attributes][real_estate_type_to_invest][]"]').attr('disabled') ? 'Chưa cập nhật' : 'Vui lòng chọn',
    });

    // Chọn chi tiết vướng mắc
    $('.select2-deal-trouble-problem-list').select2({
      theme: 'bootstrap',
      language: 'vi',
      placeholder: $('.select2-deal-trouble-problem-list').attr('disabled') ? 'Chưa cập nhật' : 'Vui lòng chọn',
    }).on("change", function () {
      if (_.includes($(this).val(), '5')) {
        $('.trouble-problem-group').show();
      } else {
        $('.trouble-problem-group').hide();
      }
      setTimeout(function () {
        $('.select2-container-active').removeClass('select2-container-active');
        $(':focus').blur();
        $('#deal_trouble_problem').val('').focus();
      }, 1);
    });

    if ($('#deal_update_comm').val() === 'true') {
      swalUpdateCommission($('#current_deal_id').val());
    }

    // Quick create customer
    $(document).on('select2:open', '.select2-deal-customer', function (e) {
      let targetId = 'deal_customer';
      let $this = $(this);
      if (e.currentTarget.id === targetId) {
        let targetSelect = $(this).data('select2');
        if (!$('.select2-link').length && targetSelect.$results.parents('.select2-results').find('button').length === 0) {
          let addCustomerBtn = '<button name="button" type="button" class="btn btn-new-customer" style="width: 100%"><i class="fas fa-plus-circle"></i> Thêm khách hàng</button>';
          let newCustomerHtml = $('.new-customer-group-body').html();
          targetSelect.$results.parents('.select2-results')
          .append(addCustomerBtn)
          .on('click', function (b) {
            $this.select2('close');
            if ($(b.target).hasClass('btn-new-customer')) {
              swalBootstrap.fire({
                title: "Tạo khách hàng mới",
                html: newCustomerHtml,
                showConfirmButton: false,
                showCloseButton: true,
                allowOutsideClick: false,
                focusConfirm: false,
                onOpen: () => {
                  $('[name="customer[data_source]"]').val($('#input-hidden-source').val());
                  if ($('.tms-select2-customer').data('select2')) {
                    $('.tms-select2-customer').select2("destroy").select2();
                  } else {
                    $('.tms-select2-customer').select2();
                  }
                  swalBootstrap.getContent().querySelector('#customer_name').focus();
                  $('#customer_name').focus();
                }
              });
            }
          });
        }
      }
    });

    // Chọn assignee khi chuyển nhượng
    var assigneeArr = [];
    $('.select2-deal-assignee').select2({
      ajax: {
        url: window.location.origin + '/deals/assignee_search',
        dataType: 'json',
        data: function (params) {
          // Query parameters will be ?search=[term]
          return {
            query: params.term
          };
        },
        processResults: function (data) {
          assigneeArr = data;
          var dataResults = [];
          _.forEach(data, function (value) {
            value.html = '<div >' + value.text + '</div>' + '<label class="info-item-label m-0">' + value.email + '</label>';
            value.title = value.text;
            dataResults.push(value);
          });
          dataResults.shift()
          return {
            results: dataResults
          };
        },
        cache: true
      },
      theme: 'bootstrap',
      language: 'vi',
      minimumInputLength: 0,
      escapeMarkup: function (markup) {
        return markup;
      },
      templateResult: function (data) {
        if (data.html == undefined) {
          return data.text;
        } else {
          return data.html;
        }
      },
      templateSelection: function (data) {
        return data.text;
      }
    }).on('change.select2', function (e) {
      assigneeArr.find(x => x.id == $(this).val());
      if ($('.select2-deal-state').val() && $('.select2-deal-customer').val()) {
        $('.btn-transfer-deal').attr('disabled', false).addClass('btn-reti-primary transfer-enabled').removeClass('transfer-disabled');
      }
    });

    // Chọn state khi chuyển nhượng
    $('.select2-deal-state').on('change', function (e) {
      if ($('.select2-deal-assignee').val() && $('.select2-deal-customer').val()) {
        $('.btn-transfer-deal').attr('disabled', false).addClass('btn-reti-primary transfer-enabled').removeClass('transfer-disabled');
      }
    });
  }

  $(document).on('click', '.btn-transfer-label', function (b) {
    let transferHistoriesHtml = $('.transfer-history-body').html();
    swalBootstrapTransferHistories.fire({
      title: "Lịch sử chuyển nhượng",
      html: transferHistoriesHtml,
      showConfirmButton: false,
      showCloseButton: true,
      allowOutsideClick: false,
      focusConfirm: false,
      onOpen: () => {
      }
    });
  });

  $(document).on('click', '.content tr td .btn-edit', function () {
    $(this).closest('form').find('.btn-save-nested').addClass('is-edit');
    $(this).closest('form').find('.datetimepicker').datetimepicker({
      format: 'd/m/Y H:i',
      lang: 'vi'
    })
    $(this).closest('form').find(".tms-icheck-tab").each(function () {
      let $el = $(this);
      let skin = ($el.attr('data-skin') !== undefined) ? "_" + $el.attr('data-skin') : "",
        color = ($el.attr('data-color') !== undefined) ? "-" + $el.attr('data-color') : "";
      let opt = {
        checkboxClass: 'icheckbox' + skin + color,
        radioClass: 'iradio' + skin + color,
      };
      $el.iCheck(opt);
    });
    customDateTimePicker($('.input-group.appointment-datetimepicker'));
    $(this).closest('.nested-fields').find('.group-form').removeClass('d-none');
    $(this).closest('.nested-fields').find('.group-show').addClass('d-none');
    // $(this).closest('.active.show').find('.deal_add_fields').addClass('d-none');
  })
  // load history
  $(document).on('click', '#history-tab', function () {
    let dealID = $(this).closest('#modal-edit').attr('data-id');
    $.ajax({
      url: `/deals/${dealID}/histories`,
      success: function (data) {
        $('#histories').replaceWith($(data));
      }
    });
  })
  $(document).on('click', '#histories .page-link', function (e) {
    e.preventDefault();
    $.ajax({
      url: $(this).attr('href'),
      success: function (data) {
        $('#histories').replaceWith($(data));
      }
    });
  })

  $(document).on('change', '.select2-deal-contact-status', function () {
    if ($(this).val() === '1' && $('.modal-content').data('state') === 'pending' && $('#deal_state').val() === 'pending') {
      swalContactStatus();
    }
  });

  $(document).on('change', '.select2-deal-mobile-contact-status', function () {
    if ($(this).val() === '1' && $('#modal-edit').data('state') === 'pending' && $('#deal_state').val() === 'pending') {
      swalContactStatus();
    }
  });

  $(document).on('change', '.select2-deal-interest', function () {
    if ($(this).val() === 'uninterested') {
      $('.select2-uninterested-reason').closest('.form-group').removeClass('d-none');
      $(this).parents('.swal2-modal').find('.swal2-confirm').prop('disabled', true);
    } else {
      $('.select2-uninterested-reason').closest('.form-group').addClass('d-none');
      $(this).parents('.swal2-modal').find('.swal2-confirm').prop('disabled', false);
    }
  })

  $(document).on('change', '.select2-uninterested-reason', function () {
    $(this).parents('.swal2-modal').find('.swal2-confirm').prop('disabled', false);
  })

  $('#complete-deals-daterangepicker').daterangepicker({
    autoUpdateInput: false,
    minYear: 2019,
    maxYear: 2119,
    locale: {
      format: 'DD/MM/YYYY'
    }
  }).on('apply.daterangepicker', function (ev, picker) {
    $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
  });

  $('#deals-created-daterangepicker').daterangepicker({
    autoUpdateInput: false,
    minYear: 2019,
    maxYear: 2119,
    locale: {
      format: 'DD/MM/YYYY'
    }
  }).on('apply.daterangepicker', function (ev, picker) {
    $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
  });

  $('#deals-updated-daterangepicker').daterangepicker({
    autoUpdateInput: false,
    minYear: 2019,
    maxYear: 2119,
    locale: {
      format: 'DD/MM/YYYY'
    }
  }).on('apply.daterangepicker', function (ev, picker) {
    $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
  });

  $('#complete-deals-table tbody').on('click', 'tr', function () {
    $(this).find('.btn-edit')[0].click();
  });
  $('body').find('.range-labels-cs-ticket.show').each(function () {
    let labelClass = $(this).closest('.cs-ticket-field-group').find('.show').eq(2).attr('class-name');
    let rangeClass = $(this).closest('.cs-ticket-field-group').find('.show').eq(3).attr('class-name');
    csTicketInitJs($(this).closest('.cs-ticket-field-group'), 'show', labelClass, rangeClass);
    $(this).find('li.active').click();
  })

  $('.export-deals.dropdown').each(function (index, dropdown) {
    let search = $(dropdown).find('.search');
    let items = $(dropdown).find('.export-deals-optional-list-field').find('.dropdown-checkbox');
    $(search).on('input', function () {
      if (search.val() === '') {
        search.closest('.dropdown').find('.export-deals-list-field').removeClass('d-none');
        search.closest('.dropdown').find('.custom-list-dropdown-checkbox').children().show();
        filter($(search).val().trim().toLowerCase(), items, dropdown);
        $(dropdown).find('.dropdown_empty').hide();
      } else {
        search.closest('.dropdown').find('.export-deals-list-field').addClass('d-none');
        filter($(search).val().trim().toLowerCase(), items, dropdown);
      }
    });
  });
  unchecked_deal_fields.push($('.list-field-not-check-all').children());
});

// javascript deal detail

$(document).on('ifChecked', '.edit-appointment .tms-icheck-tab', function (event) {
  if ($(this).val() === 0) {
    $(this).parents('.form-group').find('.appointment-result').removeClass('d-none');
  } else {
    $(this).parents('.form-group').find('.appointment-result').addClass('d-none');
    $(this).parents('.form-group').find('.result-appointment').val('');
  }
})

// Common actions
$(document).on('shown.bs.tab', '.nav-activities a[data-toggle="pill"]', function () {
  $('.kanban-scroll').scrollTop(1200);
})

$(document).on('click', '.has_child', function () {
  var dataId = $(this).data('id');
  var childElm = $(`label[data-p-id="${dataId}"]`);
  if (childElm.is(':visible')) {
    $(this).find('.group-action').removeClass('fa-minus').addClass('fa-plus');
    childElm.parent().each(function () {
      var childDataId = $(this).find('label').data('id');
      if (childDataId != 'undefined' && childDataId != undefined) {
        $(`label[data-p-id="${childDataId}"]`).parent().hide();
        $(this).find('label').find('.group-action').removeClass('fa-minus').addClass('fa-plus');
      }
    })
    childElm.parent().hide();
  } else {
    $(this).find('.group-action').removeClass('fa-plus').addClass('fa-minus');
    childElm.parent().show();
  }
})

$(document).on('click', 'button[name="edit"], button[name="cancel"], button[name="save"]', function () {
  showLoading(true);
})

$(document).on('change', '.select2-region', function () {
  let parent = $(this);
  let children = parent.data("select-child-target");
  children.forEach(function (child) {
    let $child = $("#" + child);
    let $defaultChildValue = $("#" + child + "_default_value");
    let parentSelectedValue = parent.children('option:selected').val();
    if (parentSelectedValue !== null && parentSelectedValue !== '') {
      $.ajax({
        method: 'POST',
        url: '/regions/get_regions',
        contentType: 'application/json',
        data: JSON.stringify({
          parent_id: parentSelectedValue
        }),
        headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
        success: function (response) {
          if ($child.data('select2')) {
            $child.empty();
          }
          response.forEach(function (item) {
            let option = `<option value="${item.id}">${item.name_with_type}</option>`;
            $child.append(option);
          });
          if ($defaultChildValue !== undefined) {
            $child.val($defaultChildValue.val());
            $defaultChildValue.remove();
          }
        }
      })
    }
  })
})

// Update Deal state & re-order on kanban
$(document).on('change', 'select[name="deal[state]"]', function () {
  if (!$('#modal-transfer').length) {
    let newState = $(this).val();
    if (newState === 'canceled') {
      newSwalCancelReason(oldState, newState);
    } else if (newState === 'contract_signed') {
      swalUpdateCommission($('#current_deal_id').val());
    } else if (newState === 'uninterested') {
      swalUninterestedReason(oldState, newState);
    } else if (allowSubmitState) {
      $('#save-state-btn').click();
    }
  }
});

// Change deal source
var sourceData;
var sourceDealId;
var showControlSource = true;
$(document).on('change', '.source-select2', function () {
  let dealId = $('#modal-edit').data('id');
  let source = $(this).val();
  sourceData = {
    _method: 'patch',
    save: true,
    form_id: 'deal_state',
    deal: {
      source: source,
      lock_version: $('#modal-edit [name="deal[lock_version]"]').val()
    }
  };
  sourceDealId = dealId;
  if (showControlSource) {
    $('.source-group').removeClass('d-flex').addClass('d-none');
    $('.input-show-source-group').removeClass('d-none');
    let text = $('.source-select2 option:selected').text();
    $('#input-show-source-text').val(text);
  }
});

// Update deal source
$(document).on('click', '.btn-source-check', function () {
  var dealId = $('#modal-edit').data('id');
  var data = {deal: {source: $('select[name="deal[source]"]').val()}};
  let prevSource = $('#input-hidden-source').val();
  var successCallback = function (res) {
    $('.source-text .text').text($('select[name="deal[source]"] option:selected').text());
    $('#deal_state').removeAttr('disabled');
    $('.input-show-source-group').addClass('d-none');
    $('.source-group').removeClass('d-none').addClass('d-flex');

    // Check allow assignee
    if ($('.dropdown-assign .info-item-content').is('#assignee-readonly') && res.allow_assignee) {
      $('#assignee-readonly').attr('id', 'assignee');
    }
    // Trigger focus required fields
    if (res.general_invalid) {
      if (prevSource === '') {
        if ($('#general-info input[name="edit"]')) $('#general-info input[name="edit"]').click();
        if ($('#general-info input[name="save"]')) $('#general-info input[name="save"]').click();
        $('#input-hidden-source').val($('select[name="deal[source]"]').val())
      }
    }
  }
  var failureCallback = function (res) {
    // Do nothing
  }
  $ajax(`/deals/${dealId}/update_source`, 'patch', data).then(successCallback, failureCallback);
})

// Cancel update source
$(document).on('click', '.btn-source-cancel', function () {
  $('.input-show-source-group').addClass('d-none');
  $('.source-group').removeClass('d-none').addClass('d-flex');
  showControlSource = false;
  $('select[name="deal[source]"]').val($('#input-hidden-source').val()).trigger('change');
  showControlSource = true;
})

// Update Deal create date
$(document).on('click', '.edit-create-deal-date', function () {
  $('.input-create-deal-date').removeClass('d-none');
  $('.create-deal-date').addClass('d-none');
  $('#create_deal_date').focus()
});

$(document).on('click', '.btn-create-date-cancel', function () {
  $('.input-create-deal-date').addClass('d-none');
  $('.create-deal-date').removeClass('d-none');
});

$(document).on('click', '.btn-create-date-check', function () {
  var dealId = $('#modal-edit').data('id');
  var data = {deal: {create_deal_date: $('#create_deal_date').val()}};
  var successCallback = function (res) {
    $('.edit-create-deal-date .info-item-content').text($('#create_deal_date').val());
    $('.edit-create-deal-date .button-control-edit').remove();
    $('.edit-create-deal-date').removeClass('editable').removeClass('edit-create-deal-date');
    $('.input-create-deal-date').addClass('d-none');
    $('.create-deal-date').removeClass('d-none');
  }
  var failureCallback = function (res) {
    // Do nothing
  }
  $ajax(`/deals/${dealId}/update_create_date`, 'patch', data).then(successCallback, failureCallback);
})

$(document).on('change', '.input-edit-title-deal', function (e) {
  $(this).css('width', this.value.length * 11 + 'px');
  $(this).closest('form').find('input[type="submit"]').click();
  $('.deal-name-text').text($('.input-edit-title-deal').val());
  $('.input-edit-title-deal').hide();
  $('.deal-full-name').show();
});

$(document).on('click', '.deal-full-name', function (e) {
  if (!$('.input-edit-title-deal').attr('readonly')) {
    $('.deal-full-name').hide();
    var input = $('.input-edit-title-deal');
    var len = input.val().length;
    input.show().focus();
    input[0].setSelectionRange(len, len);
    if ($('#is_mobile').val() === 'true') input.css('background-color', '#fff');
  }
});

$(document).on('focusout', '.input-edit-title-deal', function (e) {
  $('.input-edit-title-deal').hide();
  $('.deal-full-name').show();
});

$(document).on('click', '.deal_add_fields', function (event) {
  event.preventDefault();
  $($(this).data('append')).prepend($(this).data('fields').replace("btn-save-nested", "btn-save-nested is-edit"));
  $($(this).data('append')).find('.datetimepicker').datetimepicker({
    format: 'd/m/Y H:i',
    lang: 'vi'
  });

  customDateTimePicker($($(this).data('append')).find('.input-group.appointment-datetimepicker'));

  $($(this).data('append')).find(".tms-icheck-tab").each(function () {
    let $el = $(this);
    let skin = ($el.attr('data-skin') !== undefined) ? "_" + $el.attr('data-skin') : "",
      color = ($el.attr('data-color') !== undefined) ? "-" + $el.attr('data-color') : "";
    let opt = {
      checkboxClass: 'icheckbox' + skin + color,
      radioClass: 'iradio' + skin + color,
    };
    $el.iCheck(opt);
  });

  $('.' + $(this).parent().attr('id') + '-').find('.form-control').first().focus();
    
  $('.history-interaction-datepicker').daterangepicker({
    singleDatePicker: true,
    autoUpdateInput: false,
    minYear: 2019,
    maxYear: 2119,
    timePicker: true,
    timePicker24Hour: true,
    locale: {
      format: 'DD/MM/YYYY HH:mm'
    }
  }).on('apply.daterangepicker', function (ev, picker) {
    $(this).find('input').val(picker.startDate.format('DD/MM/YYYY - HH:mm'));
  });

  $('.tms-select2').select2({
    theme: 'bootstrap',
    language: 'vi',
    placeholder: 'Vui lòng chọn'
  });
});

$(document).on('click', '.btn-save-nested', function () {
  $(this).attr('disabled', 'disabled')
  var pseudoId = $(this).parents('.edit-appointment').find('input[name="appointment[pseudo_id]"]');
  if (pseudoId.length > 0 && pseudoId.val() == '') {
    pseudoId.val(Math.random().toString(36).substr(2, 10));
    $(this).parents('.appointment-').removeClass('appointment-').addClass(`appointment-${pseudoId.val()}`)
  }
  let url = $(this).closest('form').attr('action');
  let data = $(this).closest('form').serialize();
  $ajax(url, 'POST', data).then();
})

$(document).on('click', '.btn-destroy-nested', function () {
  if ($(this).parents('.edit-appointment').length > 0 || $(this).parents('.edit-history-interaction').length > 0) {
    if ($(this).data('destroy') === 'remove') {
      $(this).parents('.nested-fields').remove();
    } else {
      let url = $(this).closest('form').attr('action');
      let data = $(this).closest('form').serialize() + "&cancel=true";
      $ajax(url, 'POST', data).then();
    }
  }
})

$(document).on('input', 'input, select, textarea', function () {
  if (!$(this).closest('.modal')) {
    if ($(this).parents('div#custom-content-above-home').length > 0 && $(this).closest('.form-group').find('.field_with_errors').length > 0) {
      var currentBadge = parseInt($('#custom-content-above-home-tab .tab-highlight').text());
      if (currentBadge > 1) $('#custom-content-above-home-tab .tab-highlight').text(currentBadge - 1);
      else $('#custom-content-above-home-tab .tab-highlight').hide();
    }
    $(this).closest('.form-group').removeClass('field_with_errors');
    $(this).closest('.form-group').find('.field_with_errors').removeClass('field_with_errors').addClass('field_with_errors_unhighlight');
    $(this).closest('.form-group').find('.label-warning').removeClass('label-warning');
    $(this).closest('.form-group').find('.custom-validation').hide();
    countRequiredAttributes();
  }
})

$(document).on('click', '.btn-collapse', function () {
  $(this).toggleClass('show-more');
  $(this).toggleClass('show-less');
})

$(document).on('click', 'input[name="save"]', function () {
  $(this).closest('form').find('input:text').each(function () {
    $(this).val($.trim($(this).val()));
  });
  $(this).closest('form').find('textarea').each(function () {
    $(this).val($.trim($(this).val()));
  });
})

$(document).on('click', '.save-contract-signed', function () {
  let dealId = $(this).closest('form').find('#current_deal_id').val();
  let productId = $(this).closest('form').find('.select2-deal-product').val();
  $.ajax({
    url: `/deals/${dealId}/check_deal_contract_signed_product`,
    type: 'POST',
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },
    data: {
      deal_id: dealId,
      product_id: productId
    }
  }).done(function (res) {
    if (res.value === true && $('#deal_check_deal_contract_signed').val() === 'false') {
      window.stop();
      swalBootstrap.fire({
        title: `<div style="line-height: 24px; font-size: 16px">Chỉnh sửa sản phẩm</div>`,
        html: 'Sản phẩm này đã được bán trước đó, bạn có chắc chắn thông tin nhập là đúng?',
        showCloseButton: true,
        showCancelButton: true,
        reverseButtons: true,
        focusConfirm: false,
        confirmButtonText: 'Xác nhận',
        cancelButtonText: 'Huỷ',
        onOpen: function () {
          $('.swal2-modal').css('width', 'auto');
          $('.swal2-header').css('height', '56px');
          $('.swal2-title').css({'top': '16px', 'font-weight': '500'});
          $('.swal2-content').css('padding', '16px 44px 0px 45px');
        }
      }).then((result) => {
        if (result.isConfirmed) {
          $('#deal_check_deal_contract_signed').val('');
          $('input[name="save"]').click();
        }
      })
    }
  })
})

$(document).on('click', '.source-text', function () {
  $('.container-source-select2').addClass('d-block');
  $('.source-select2').select2('open');
})

$(document).on('select2:close', '.source-select2', function () {
  $('.container-source-select2').removeClass('d-block');
})

$(document).on('ifChanged', '#deal_is_external_project', function (event) {
  if ($(this).is(':checked')) {
    $('#deal_external_project').removeClass('input-hidden');
    $('#deal_cross_selling_product_code').removeClass('input-hidden');
    $('.project-select-group').addClass('input-hidden');
    $('.project-select-group').find('.field_with_errors').addClass('field_with_errors-1').removeClass('field_with_errors');
  } else {
    $('#deal_external_project').addClass('input-hidden');
    $('#deal_cross_selling_product_code').addClass('input-hidden');
    $('.project-select-group').removeClass('input-hidden');
    $('.project-select-group').find('.field_with_errors-1').addClass('field_with_errors').removeClass('field_with_errors-1')
  }
  countRequiredAttributes();
})

$(document).on('mouseover', '.source-group, .total-price, .contract-signed-at, .show-button-edit-canceled, .show-button-edit-uninterested, .create-deal-date, .source-detail', function () {
  if ($(this).hasClass('editable')) {
    $(this).find('.fa-pen').removeClass('d-none');
  }
})
$(document).on('mouseout', '.source-group, .total-price, .contract-signed-at, .show-button-edit-canceled, .show-button-edit-uninterested, .create-deal-date, .source-detail', function () {
  if ($(this).hasClass('editable')) {
    $(this).find('.fa-pen').addClass('d-none');
  }
})

$(document).on('click', '.total-price, .contract-signed-at, .source-detail', function () {
  $(`.input-show-group-${$(this).data('field')}`).removeClass('d-none').addClass('d-block');
})

// $(document).on('click', '.btn-total-price-check, .btn-contract-signed-at-check', function () {
//   var dealField = $(this).data('field');
//   var fieldValue = $(`#input-show-text-${dealField}`).val();
//   var data = {
//     id: $(this).data('id'),
//     value: fieldValue,
//     field: dealField
//   }
//   $ajax(`/deals/update_commission`, 'POST', data).then();
// })

$(document).on('click', '.btn-source-detail-check', function () {
  var data = {
    id: $(this).data('id'),
    value: $(`#input-show-text-${$(this).data('field')}`).val()
  }
  var successCallback = function (res) {
    $(`.input-show-group-source-detail`).removeClass('d-block').addClass('d-none');
    $(`.source-detail`).removeClass('d-none').addClass('d-flex');
    $('.editable.source-detail .text').text(res.source_detail == null ? 'Đang cập nhật' : res.source_detail)
  }
  $ajax(`/deals/update_source_detail`, 'POST', data).then(successCallback, null);
})

$(document).on('click', '.btn-tms-cancel', function () {
  var field = $(this).data('field');
  $(`.input-show-group-${field}`).removeClass('d-block').addClass('d-none');
  $(`.input-show-group-${field} input`).val($(`.stored-${field}`).val());
  if (field === 'contract-signed-at') {
    $(`.input-show-group-${field} input`).data('daterangepicker').setStartDate($(`.stored-${field}`).val());
    $(`.input-show-group-${field} input`).data('daterangepicker').setEndDate($(`.stored-${field}`).val());
  }
  $(`.${field}`).removeClass('d-none').addClass('d-flex');
})

$(document).on('click', '.button-edit-canceled', function () {
  $('#show-change-canceled-reason').removeClass('d-none');
  $('#show-canceled-reason').addClass('d-none');
})

$(document).on('click', '.btn-canceled-cancel', function () {
  $('#show-change-canceled-reason').addClass('d-none');
  $('#show-canceled-reason').removeClass('d-none');
})

$(document).on('change', '#deal_canceled_reason', function () {
  if ($(this).val() === 6) {
    $('.show-canceled-note').removeClass('d-none');
  } else {
    $('.show-canceled-note').addClass('d-none');
  }
})

$(document).on('click', '.btn-canceled-check', function () {
  if ($('#deal_canceled_reason').val() !== 6) {
    $('#deal_canceled_note').val('')
  }
  $.ajax({
    url: `/deals/${$('#modal-edit').data('id')}`,
    type: "post",
    data: {
      _method: 'patch',
      save: true,
      form_id: 'deal_state',
      deal: {
        canceled_reason: $('#deal_canceled_reason').val(),
        canceled_note: $('#deal_canceled_note').val(),
        lock_version: $('#modal-edit [name="deal[lock_version]"]').val()
      }
    },
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },
    success: function (data) {
    }
  });
})

// javascript deal activity

$(document).on('click', '.report-appointment', function () {
  reportAppointment();
})

$(document).on('click', '#add-history-interaction .btn-kanban-danger', function () {
  if ($('.edit-history-interaction').length > 0) {
    $(this).parents('#add-history-interaction').siblings('.add-hs-fields').removeClass('d-none');
    $(this).attr('disabled', 'disabled')
  }
});

$(document).on('click', '.btn-save-nested', function () {
  let parent = $(this).parents('.edit-history-interaction');
  if (parent.find('select[name="history_interaction[interaction_type]"]').val().length > 0 && parent.find('textarea[name="history_interaction[interaction_content]"]').val().length > 0) {
    $('.add-hs-fields').addClass('d-none');
    $('#add-history-interaction').find('.btn-kanban-danger').attr('disabled', false)
  }
});

$(document).on('click', '.btn-destroy-nested', function () {
  if ($(this).parents('.edit-appointment').length === 0) {
    if ($(this).data('destroy') === 'cancel') {
      $(this).closest('.nested-fields').find('.group-form').addClass('d-none');
      $(this).closest('.nested-fields').find('.group-show').removeClass('d-none');
      $(this).closest('.active.show').find('.deal_add_fields').removeClass('d-none');
    } else {
      $(this).closest('.nested-fields').parent().siblings('.deal_add_fields').removeClass('d-none');
      $(this).closest('.nested-fields').remove();
    }
  }
  if ($(this).parents('.edit-history-interaction').length > 0) {
    $('.add-hs-fields').addClass('d-none');
    $('#add-history-interaction').find('.btn-kanban-danger').attr('disabled', false);
  }
});

$(document).on('click', '.leg-body-title-background', function () {
  let labelClass = $(this).closest('.cs-ticket-field-group').find('.show').eq(2).attr('class-name');
  let rangeClass = $(this).closest('.cs-ticket-field-group').find('.show').eq(3).attr('class-name');
  let label = $(this).find('.product-label');
  csTicketInitJs($(this).closest('.cs-ticket-field-group'), 'show', labelClass, rangeClass);
  $(this).closest('.cs-ticket-field-group').find('.range-labels-cs-ticket.show').find('li.active').click();
  let collapse = $(this).find('.collapse');
  let expand = $(this).find('.expand');
  if (expand.hasClass('d-none')) {
    collapse.addClass('d-none');
    collapse.removeClass('d-flex');
    expand.removeClass('d-none');
    expand.addClass('d-flex');
    label.addClass('d-none');
    label.removeClass('d-flex');
    $(this).closest('.cs-ticket-field-group').find('.col-12').eq(1).removeClass('d-none');
  } else {
    expand.addClass('d-none');
    expand.removeClass('d-flex');
    collapse.removeClass('d-none');
    collapse.addClass('d-flex');
    label.removeClass('d-none');
    label.addClass('d-flex');
    $(this).closest('.cs-ticket-field-group').find('.col-12').eq(1).addClass('d-none');
  }
  $(this).closest('.cs-ticket-field-group').find('textarea').each(function () {
    this.style.height = '38px';
    this.style.height = (this.scrollHeight) + 'px';
  })
})

$(document).on('show.bs.modal', '#create-cs-ticket, #new-voucher', function () {
  let modal = $(this);
  let btnVoucher = modal.find('.btn-voucher');
  csTicketInitJs($(this), 'create', '', '');
  modal.find('.form-control').val('');
  modal.find('.sale-rating-select2, .voucher-select2, .tms-select2, .voucher-multiple-select2').val('').trigger('change');
  modal.find('#cs_ticket_deal_rating').val(0);
  modal.find('.range-labels-cs-ticket.new').find('li').first().click();
  modal.find('textarea').css('height', '38px');
  modal.find('#cs_ticket_customer_opinion_1').prop('checked', true);
  modal.find('.cs-ticket-form-groups').removeClass('d-none');
  modal.find('#cs_ticket_selected_voucher_id').val('');
  modal.find('#cs_ticket_voucher_object_id').val('');
  btnVoucher.attr('disabled', true);
  btnVoucher.removeClass('btn-voucher-enable-submit').addClass('btn-tms-unable-submit');
  modal.find('#cs_ticket_disable_send_voucher').val('false');
  modal.find('#cs_ticket_voucher_id').attr('disabled', false);
  modal.find('.sale-rating-select2').select2({
    theme: 'bootstrap',
    language: 'vi',
    placeholder: 'Chọn 1 hoặc 2 sao'
  })
  initSelect2($(this));
  $(this).find('.closeable').val('true');
})

$(document).on('show.bs.modal', '.modal-edit-cs-ticket', function () {
  let labelClass = $(this).find('.show').eq(0).attr('class-name');
  let rangeClass = $(this).find('.show').eq(1).attr('class-name');
  let id = $(this).attr('id');
  csTicketInitJs($(this), 'edit', labelClass, rangeClass);

  $(`#${id}`).find('.range-labels-cs-ticket.show').find('li.active').click();
  initSelect2($(this));
})

$(document).on('shown.bs.modal', '.modal-edit-cs-ticket', function () {
  $(this).find('textarea').each(function () {
    this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
  }).on('input', function () {
    this.style.height = '38px';
    this.style.height = (this.scrollHeight) + 'px';
  });
})

$(document).on('click', '.range-labels-cs-ticket', function () {
  $(this).closest('.modal.show').find('.closeable').val('false');
})

$(document).on('change keyup', 'input, select, textarea', function () {
  if ($(this).closest('.modal.show').length > 0 && ($(this).closest('.form-group').find('.check_input_type_range').val() !== $(this).val())) {
    $(this).closest('.modal.show').find('.closeable').val('false');
  }
})

$(document).on('click', '.btn-save-modal', function () {
  $(this).closest('.modal.show').find('.cancel-cs-ticket-modal').attr('disabled', true).css({'cursor': 'not-allowed'});
  $(this).closest('.modal.show').find('.action-x').addClass('disable-x');
  $(this).closest('.modal.show').find('.action-x').removeClass('action-x');
})

$(document).on('click', '.cancel-cs-ticket-modal', function (e) {
  let voucher = $(this).closest('.edit-voucher-modal');
  let csTicketId = $(this).closest('.modal.show').attr('cs_ticket_id');
  let dealId = $(this).closest('.modal.show').attr('deal_id');
  let title = ''
  if ($(this).closest('.modal.show').find('#create-cs-ticket-label, #new-voucher-label').length > 0) {
    title = 'Bạn chưa lưu tạo mới'
  } else {
    title = 'Bạn chưa lưu thay đổi'
  }
  if ($(this).closest('.modal.show').find('.closeable').val() === 'true') {
    $(this).closest('.modal.show').modal('hide');
  } else {
    e.preventDefault();
    swalBootstrapUpdateCsTicket.fire({
      title: `<div style="line-height: 24px; font-size: 16px">${title}</div>`,
      html: '<div>Các thông tin của bạn chưa được lưu lại</div>'
        + '<div>Bạn chắc chắn muốn thoát?</div>',
      showCloseButton: true,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText: 'Thoát, không lưu',
      cancelButtonText: 'Ở lại',
      onOpen: function () {
        $('.swal2-modal').css('width', 'auto');
        $('.swal2-header').css('height', '56px');
        $('.swal2-title').css({'top': '16px', 'font-weight': '500'});
        $('.swal2-content').css('padding', '16px 44px 0px 45px');
      }
    }).then((result) => {
      if (result.isConfirmed) {
        $('.modal.show').modal('toggle');
        $('.modal-backdrop').remove();
        if (csTicketId) {
          ajaxReloadCsTicket(csTicketId, dealId);
        } else if (voucher.length > 0) {
          window.location.reload();
        } else {
          ajaxReloadEditDealCustomer(dealId);
        }
      }
    })
  }
})

$(document).on('click', '.cs-ticket-label-radio-button', function () {
  $(this).siblings().first().attr('checked', true).trigger('change').trigger('click');
})

$(document).on('change', "input[type=radio][name='cs_ticket[customer_opinion]']", function () {
  let modal = $(this).closest('.modal.show');
  if ($(this).val() === '2') {
    modal.find('.cs-ticket-form-groups').addClass('d-none');
    modal.find('hr').addClass('d-none');
    modal.find('.btn-save-modal').attr('disabled', false).removeClass('btn-tms-unable-submit').addClass('btn-reti-primary');
  } else {
    modal.find('.cs-ticket-form-groups').removeClass('d-none');
    modal.find('hr').removeClass('d-none');
    modal.find('.btn-save-modal').attr('disabled', true).removeClass('btn-reti-primary').addClass('btn-tms-unable-submit');
  }
})

$(document).on('keyup', '.modal .form-control', function () {
  checkValInput($(this).closest('.modal.show'));
})

$(document).on('select2:select', '.modal-select2', function () {
  checkValInput($(this).closest('.modal.show'));
})

$(document).on('select2:unselect', '.modal-select2', function () {
  checkValInput($(this).closest('.modal.show'));
})

$(document).on('change', '#voucher_voucher_type', function () {
  if ($(this).val() === '2') {
    $(this).closest('.modal.show').find('.applicable-condition-field').removeClass('d-none');
    $(this).closest('.modal.show').find('#voucher_applicable_condition').removeClass('d-none');
  } else {
    $(this).closest('.modal.show').find('.applicable-condition-field').addClass('d-none');
    $(this).closest('.modal.show').find('#voucher_applicable_condition').addClass('d-none');
    $(this).closest('.modal.show').find('#voucher_applicable_condition').val('').trigger('change');
  }
});

$(document).on('select2:select', 'select[name="cs_ticket[voucher_id]"]', function () {
  let btnVoucher = $(this).closest('.form-group').find('.btn-voucher');
  if ($(this).closest('.form-group').find('#cs_ticket_disable_send_voucher').val() === 'false') {
    btnVoucher.attr('disabled', false);
    btnVoucher.removeClass('btn-tms-unable-submit').addClass('btn-voucher-enable-submit');
  }
  $(this).closest('.modal.show').find('#cs_ticket_selected_voucher_id').val($(this).val());
})

$(document).on('click', '.btn-send-voucher', function () {
  let $this = $(this).closest('.modal.show');
  let csTicketBody = $('body').find('.modal.show').eq(0);
  let btnVoucher = csTicketBody.find('.btn-voucher');
  let btnSendVoucher = $this.find('.btn-send-voucher');
  let voucher_id = csTicketBody.find('.voucher-select2').val();
  let customer_id = csTicketBody.find('input[name="cs_ticket[customer_id]"]').val();
  let deal_id = csTicketBody.find('input[name="cs_ticket[deal_id]"]').val();
  let email = $this.find('#send-voucher-email').val();
  let cs_ticket_id = csTicketBody.find('#cs_ticket_cs_ticket_id').val();
  let optionSendVoucher = $this.find("input[type=radio][name='send-voucher']:checked").val();
  email.trim();
  let voucher_object_id = csTicketBody.find('#cs_ticket_voucher_object_id');
  csTicketBody.find('#cs_ticket_disable_send_voucher').val('true');
  csTicketBody.find('#cs_ticket_voucher_id').attr('disabled', true);
  btnSendVoucher.attr('disabled', true);
  btnSendVoucher.removeClass('btn-reti-primary').addClass('btn-tms-unable-submit');
  btnVoucher.attr('disabled', true);
  btnVoucher.removeClass('btn-voucher-enable-submit').addClass('btn-tms-unable-submit');
  $this.find('.btn-outline-reti-primary').attr('disabled', true).css({'cursor': 'not-allowed'});
  $this.find('.action-x').attr('disabled', true).css({'cursor': 'not-allowed'}).addClass('disable-x').removeClass('action-x');
  checkValInput(csTicketBody);
  $.ajax({
    method: 'POST',
    url: '/vouchers/send_voucher',
    type: 'JSON',
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },
    data: {
      voucher_id: voucher_id,
      customer_id: customer_id,
      deal_id: deal_id,
      email: email,
      cs_ticket_id: cs_ticket_id,
      value_option_send_voucher: optionSendVoucher
    }
  }).done(function (response) {
    if (response.status === 'success') {
      toastr.success('Gửi voucher thành công.');
      voucher_object_id.val(response.voucher_id);
      $this.hide();
      $('body').find('.modal-backdrop.fade.show.z-index-1051').remove();
    } else {
      toastr.error('Đã có lỗi xảy ra. Vui lòng thử lại.');
    }
  });
})

$(document).on('input', '#send-voucher-email', function () {
  let btnSendVoucher = $(this).closest('.modal.show').find('.btn-send-voucher');
  let $inputEmail = $(this);
  let email = $(this).val();
  if (/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email) !== true) {
    $inputEmail.parent().find('.invalid-feedback').remove();
    $inputEmail.after(`<span class='invalid-feedback custom-validation'>Email không hợp lệ.</span>`);
    $inputEmail.closest('.form-group').addClass('field_with_errors');
    btnSendVoucher.attr('disabled', true);
    btnSendVoucher.removeClass('btn-reti-primary').addClass('btn-tms-unable-submit');
    return;
  } else {
    $inputEmail.parent().removeClass('field_with_errors');
    $inputEmail.parent().find('.custom-validation').remove();
    btnSendVoucher.attr('disabled', false);
    btnSendVoucher.removeClass('btn-tms-unable-submit').addClass('btn-reti-primary');
  }
})

$(document).on('show.bs.modal', '#send-voucher', function () {
  let btnSendVoucher = $(this).find('.btn-send-voucher');
  let $inputEmail = $(this).find('#send-voucher-email');
  let csTicketBody = $('body').find('.modal.show').eq(0);
  $('body').find('.modal-backdrop.fade.show').first().addClass('z-index-1051');
  $(this).find('.btn-send-voucher').attr('disabled', false);
  $(this).find('.btn-send-voucher').removeClass('btn-tms-unable-submit').addClass('btn-reti-primary');
  $(this).find('#option-send-voucher-email').prop("checked", true);
  $(this).find('.col-12').eq(0).find('.form-group').removeClass('d-none');
  $(this).find('.col-12').eq(1).find('.form-group').addClass('d-none');
  $(this).find('.send-voucher-zalo-content').text(`RETI kính tặng quý khách voucher ${csTicketBody.find('#cs_ticket_voucher_id').find(':selected').text()}. LH 18008085 để biết thêm chi tiết.`)
  $(this).find('.btn-outline-reti-primary').attr('disabled', false).css({'cursor': 'pointer'});
  $(this).find('.disable-x').attr('disabled', false).css({'cursor': 'pointer'}).removeClass('disable-x').addClass('action-x');
  $inputEmail.val(csTicketBody.find('#cs_ticket_customer_email').val());
  $inputEmail.parent().removeClass('field_with_errors');
  $inputEmail.parent().find('.custom-validation').remove();
  if ($inputEmail.val().length === 0) {
    btnSendVoucher.attr('disabled', true);
    btnSendVoucher.removeClass('btn-reti-primary').addClass('btn-tms-unable-submit');
  }
});

$(document).on('click', '.cancel-send-voucher', function (e) {
  e.preventDefault();
  swalBootstrapUpdateCsTicket.fire({
    title: `<div style="line-height: 24px; font-size: 16px">Bạn chưa gửi voucher</div>`,
    html: '<div>Voucher của bạn chưa được gửi đi.</div>'
      + '<div>Bạn chắc chắn muốn thoát?</div>',
    showCloseButton: true,
    showCancelButton: true,
    focusConfirm: false,
    confirmButtonText: 'Thoát, không gửi',
    cancelButtonText: 'Ở lại',
    onOpen: function () {
      $('.swal2-modal').css('width', 'auto');
      $('.swal2-header').css('height', '56px');
      $('.swal2-title').css({'top': '16px', 'font-weight': '500'});
      $('.swal2-content').css('padding', '16px 44px 0px 45px');
    }
  }).then((result) => {
    if (result.isConfirmed) {
      $('#send-voucher').modal('toggle');
      $('.modal-backdrop.z-index-1051').remove();
    }
  })
})

$(document).on('hide.bs.modal', '#send-voucher', function () {
  $('body').find('.modal-backdrop.fade.show.z-index-1051').removeClass('z-index-1051');
});

$(document).on('change', "input[type=radio][name='send-voucher']", function () {
  let btnSendVoucher = $(this).closest('.modal.show').find('.btn-send-voucher');
  let inputEmail = $(this).closest('.modal.show').find('#send-voucher-email').val();
  $(this).closest('.col-12').find('.form-group').removeClass('d-none');
  $(this).closest('.col-12').siblings().find('.form-group').addClass('d-none');
  if ($(this).val() === '2') {
    btnSendVoucher.attr('disabled', false);
    btnSendVoucher.removeClass('btn-tms-unable-submit').addClass('btn-reti-primary');
  } else {
    if (inputEmail.length === 0) {
      btnSendVoucher.attr('disabled', true);
      btnSendVoucher.removeClass('btn-reti-primary').addClass('btn-tms-unable-submit');
    }
  }
})

$(document).on('click', '.btn-copy-send-voucher', function (e) {
  e.preventDefault();
  var copyText = $(this).closest('.form-group').find('.send-voucher-zalo-content').text();
  var textarea = document.createElement("textarea");
  textarea.textContent = copyText;
  document.body.appendChild(textarea);
  textarea.select();
  textarea.focus();
  document.execCommand("copy");
  document.body.removeChild(textarea);
  toastr.success('Copy nội dung thành công');
})


$(document).on('show.bs.modal', '#new-voucher', function () {
  initSelect2Voucher($(this));
  $(this).find('.closeable').val('true');
})

$(document).on('show.bs.modal', '.edit-voucher-modal', function () {
  initSelect2Voucher($(this));
  $(this).find('.closeable').val('true');
})

$(document).on('shown.bs.modal', '.edit-voucher-modal', function () {
  $(this).find('textarea.h-38').each(function () {
    this.style.height = '38px';
    this.style.height = (this.scrollHeight) + 'px';
  })
})

$(document).on('keyup', '#voucher_content', function () {
  let length = $(this).val().length;
  $(this).parent().find('.limited-text').text(`${length}/150`);
})

// Export deals
$(document).on('show.bs.dropdown', '.export-deals', function () {
  $(this).find('.btn-export-deals').css('background', '#3E9EAB');
  $(this).find('.search').val('');
  $(this).find('.export-deals-list-field').removeClass('d-none');
  $(this).find('.custom-list-dropdown-checkbox').children().show();
  $(this).find('.remove-option-export-deals').css({'color': '#DCDEE9', 'cursor': 'auto'});
  $('.export-deals-optional-list-field').find('input[name="deal_fields[]"]').prop('checked', false);
  $('.export-deals-list-field').find('.dropdown-checkbox').each(function () {
    if ($(this).index() > 2) {
      $(this).addClass('d-none');
    }
  })
  $(this).find('.dropdown_empty').hide();
  $('.expand-collapse-export-deals').text('Mở rộng');
})

$(document).on('shown.bs.dropdown', '.export-deals', function () {
  $(this).find('.dropdown-menu').removeClass('d-none');
  $('.dropdown-scroll').scrollTop(0);
})

$(document).on('hide.bs.dropdown', '.export-deals', function () {
  
  $('.remove-option-export-deals').parents('.dropdown').find('.export-deals-optional-list-field').find('input[name="deal_fields[]"]').prop("checked", false);
  $('.remove-option-export-deals').parents('.dropdown').find('.option-check-all').attr('check_all', false);
  $('.remove-option-export-deals').css({'color': '#DCDEE9', 'cursor': 'auto'});
  $('.remove-option-export-deals').closest('.dropdown-menu').find('.list-field-not-check-all').html(unchecked_deal_fields);
  $('.remove-option-export-deals').find('.btn-export-deals').css('background', '#56C5D0');

  $(this).find('.btn-export-deals').css('background', '#56C5D0');
})

$(document).on('click', '.dropdown-menu', function (e) {
  e.stopPropagation();
});

$(document).on('click', '.expand-collapse-export-deals', function () {
  if ($(this).text() === 'Mở rộng') {
    $(this).closest('.custom-list-dropdown-checkbox').children().removeClass('d-none');
    $(this).text('Thu gọn');
  } else {
    $(this).closest('.custom-list-dropdown-checkbox').find('.dropdown-checkbox').each(function () {
      if ($(this).index() > 2) {
        $(this).addClass('d-none');
      }
    })
    $(this).text('Mở rộng');
  }
});

$(document).on('click', '.remove-option-export-deals', function (e) {
  e.stopPropagation();
  $(this).parents('.dropdown').find('.export-deals-optional-list-field').find('input[name="deal_fields[]"]').prop("checked", false);
  $(this).parents('.dropdown').find('.option-check-all').attr('check_all', false);
  $(this).css({'color': '#DCDEE9', 'cursor': 'auto'});
  $(this).closest('.dropdown-menu').find('.list-field-not-check-all').html(unchecked_deal_fields);
});

$(document).on('click', '.submit-export-deals', function (e) {
  let urlSearchParams = new URLSearchParams(window.location.search);
  let dropdown = $(this).closest('.dropdown');
  let params = {
    query: urlSearchParams.get('query'),
    state: urlSearchParams.getAll('state[]'),
    source: urlSearchParams.getAll('source[]'),
    project: urlSearchParams.getAll('project[]'),
    assignee: urlSearchParams.getAll('assignee[]'),
    label: urlSearchParams.getAll('label[]'),
    purchase_purpose: urlSearchParams.getAll('purchase_purpose[]'),
    product_type: urlSearchParams.getAll('product_type[]'),
    balcony_direction: urlSearchParams.getAll('balcony_direction[]'),
    door_direction: urlSearchParams.getAll('door_direction[]'),
    contract_signed_at: urlSearchParams.get('contract_signed_at'),
    created_at: urlSearchParams.get('created_at'),
    updated_at: urlSearchParams.get('updated_at')
  }

  let listFields = []
  let listFieldsOptional = $(this).closest('.dropdown-menu').find('.export-deals-optional-list-field input[type=checkbox]:checked').parent().sort(function(a, b) {
    var vA = a.textContent.trim();
    var vB = b.textContent.trim();
    return (vA < vB) ? -1 : (vA > vB) ? 1 : 0;
  });
  $(this).closest('.dropdown-menu').find('.export-deals-list-field input[type=checkbox]:checked').each(function () {
    listFields.push($(this).val());
  })
  listFieldsOptional.children('input').each(function () {
    listFields.push($(this).val());
  })

  let exportCompleteDeals = $(this).attr('complete_deals')

  $(this).attr('disabled', true).css({'cursor': 'not-allowed'});
  $.ajax({
    url: "/deals/export_deals",
    type: "post",
    data: {
      filter: params,
      list_fields: listFields,
      export_complete_deals: exportCompleteDeals
    },
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },
    success: function (data) {
      dropdown.find('.submit-export-deals').attr('disabled', false).css({'cursor': 'pointer'});
      dropdown.find('.dropdown-menu').addClass('d-none');
      $('.export-deals').find('.btn-export-deals').css('background', '#56C5D0');
      toastr.success('File download sẽ được gửi vào email của bạn.');
    }
  });
});

$(document).on('change', '.export-deals-optional-list-field .dropdown-checkbox input[type=checkbox]', function () {
  let check_list_fields = [];
  let parent = $(this).closest('.list-field-not-check-all');
  let isCheckAll = $(this).parents('.dropdown').find('.export-deals-optional-list-field .dropdown-checkbox input[type=checkbox]:checked').not('.option-check-all').length
    === $(this).parents('.dropdown').find('.export-deals-optional-list-field .dropdown-checkbox input[type=checkbox]').not('.option-check-all').length
  if ($(this).hasClass('option-check-all')) {
    if ($(this).attr('check_all') === 'false') {
      $(this).parents('.dropdown').find('.export-deals-optional-list-field').find('input[name="deal_fields[]"]').prop("checked", true);
      $(this).attr('check_all', true);
    } else {
      $(this).parents('.dropdown').find('.export-deals-optional-list-field').find('input[name="deal_fields[]"]').prop("checked", false);
      $(this).attr('check_all', false);
      $(this).closest('.dropdown-menu').find('.list-field-not-check-all').html(unchecked_deal_fields);
    }
  } else {
    if (isCheckAll) {
      $(this).parents('.dropdown').find('.option-check-all').prop("checked", true).attr('check_all', true);
    } else {
      $(this).parents('.dropdown').find('.option-check-all').prop("checked", false).attr('check_all', false);
    }
  }
  if ($(this).parents('.dropdown').find('.export-deals-optional-list-field .dropdown-checkbox input[type=checkbox]:checked').length > 0) {
    $(this).parents('.dropdown').find('.remove-option-export-deals').css({'color': '#56C5D0', 'cursor': 'pointer'});
  } else {
    $(this).parents('.dropdown').find('.remove-option-export-deals').css({'color': '#DCDEE9', 'cursor': 'auto'});
  }
  parent.find('input[type=checkbox]:checked').each(function () {
    check_list_fields.push($(this).parent());
  })
  parent.find('input:checkbox:not(:checked)').each(function () {
    check_list_fields.push($(this).parent());
  })
  if (parent.find('input[type=checkbox]:checked').length === 0) {
    parent.html(unchecked_deal_fields);
  } else {
    parent.html(check_list_fields);
  }
})

// Function
function initSelect2($this) {
  $this.find('.sale-rating-select2').each(function () {
    $(this).select2({
      theme: 'bootstrap',
      language: 'vi',
      placeholder: 'Chọn 1 hoặc 2 sao'
    });
  });
  $this.find('.voucher-select2').each(function () {
    $(this).select2({
      theme: 'bootstrap',
      language: 'vi',
      placeholder: 'Chọn voucher'
    });
  });
  $this.find('.tms-select2').each(function () {
    $(this).select2({
      theme: 'bootstrap',
      language: 'vi',
      placeholder: 'Vui lòng chọn'
    });
  });
}

function initSelect2Voucher($this) {
  $this.find('.tms-select2').select2({
    theme: 'bootstrap',
    language: 'vi',
    placeholder: 'Vui lòng chọn'
  });


  let uncheckedVoucherProjects = [];
  $this.find('.voucher-multiple-select2').map(function () {
    uncheckedVoucherProjects.push($(this).children());
  })

  $this.find('.voucher-multiple-select2').select2({
    theme: 'bootstrap checkbox-select2-container',
    placeholder: 'Vui lòng chọn',
    closeOnSelect: false,
  }).on('select2:open', function () {
    if ($(this).siblings().find('.select2-selection__choice').length > 0) {
      let selectOption = [];
      let selectionChoices = $(this).siblings().find('.select2-selection__choice').map(function () {
        return $(this).attr('title');
      }).get();
      $(this).find('option').each(function (i, e) {
        if (selectionChoices.includes($(this).text())) {
          selectOption.push(e);
        }
      });
      $(this).find('option').each(function (i, e) {
        if (!selectionChoices.includes($(this).text())) {
          selectOption.push(e);
        }
      });
      $(this).append(selectOption);
    } else {
      $(this).append(uncheckedVoucherProjects);
    }
    if ($(this).find(":selected").val() === '0') {
      $(this).find('option').not("option[value='0']").attr('disabled', 'disabled');
      $(this).trigger("change");
    }
  }).on('select2:select', function (e) {
    if (e.params.data.id === '0') {
      $(this).find('option').not("option[value='0']").attr('disabled', 'disabled');
      $(this).find('option').not("option[value='0']").prop("selected", "");
      $(this).trigger("change");
      $(this).parent().find(".select2-container").siblings('select:enabled').select2('close').select2('open');
    }
  }).on('select2:unselect', function (e) {
    if (e.params.data.id === '0') {
      $(this).find('option').not("option[value='0']").attr('disabled', false)
      $(this).trigger("change");
      $(this).parent().find(".select2-container").siblings('select:enabled').select2('close').select2('open');
    }
  })
}

function csTicketInitJs($this, action, labelClass, rangeClass) {
  var sheetCsTicket = document.createElement('style'),
    prefsCsTicket = ['webkit-slider-runnable-track', 'moz-range-track', 'ms-track'],
    $rangeInputCsTicket = [];

  if (action === 'create') {
    $rangeInputCsTicket = $this.find('.range-cs-ticket.new input')
  } else {
    $rangeInputCsTicket = $this.find(`.${rangeClass}.show input`)
  }

  document.body.appendChild(sheetCsTicket);

  var getTrackStyleCsTicket = function (el) {
    var labelVal = el.value,
      val = labelVal * 10,
      style = '';

    // Set active label
    var labelRangeCsTicket = [];
    if (action === 'create') {
      $('.range-labels-cs-ticket.new li').removeClass('active selected');
      labelRangeCsTicket = $this.find('.range-labels-cs-ticket.new').find('li:nth-child(' + (parseInt(labelVal) + 1) + ')');
    } else {
      $(`.${labelClass}.show li`).removeClass('active selected');
      labelRangeCsTicket = $this.find(`.${labelClass}.show`).find('li:nth-child(' + (parseInt(labelVal) + 1) + ')');
    }

    labelRangeCsTicket.addClass('active selected');
    labelRangeCsTicket.prevAll().addClass('selected');

    // Change background gradient
    for (var i = 0; i < prefsCsTicket.length; i++) {
      if (action === 'create') {
        style += '.range-cs-ticket.new {background: linear-gradient(to right, #56C5D0 0%, #56C5D0 ' + val + '%, #fff ' + val + '%, #fff 100%)}';
        style += '.range-cs-ticket.new input::-' + prefsCsTicket[i] + '{background: linear-gradient(to right, #56C5D0 0%, #56C5D0 ' + val + '%, #E9EAED ' + val + '%, #E9EAED 100%)}';
      } else {
        style += '.' + rangeClass + '.show {background: linear-gradient(to right, #56C5D0 0%, #56C5D0 ' + val + '%, #fff ' + val + '%, #fff 100%)}';
        style += '.' + rangeClass + '.show input::-' + prefsCsTicket[i] + '{background: linear-gradient(to right, #56C5D0 0%, #56C5D0 ' + val + '%, #E9EAED ' + val + '%, #E9EAED 100%)}';
      }
    }
    return style;
  }

  $rangeInputCsTicket.on('input', function () {
    if (sheetCsTicket.textContent.length > 0) {
      sheetCsTicket.textContent = getTrackStyleCsTicket(this);
      checkValInput($(this).closest('.modal.show'));
    }
  });

  $this.find('.range-labels-cs-ticket li').on('click', function (e) {
    if (!(action === 'show' && sheetCsTicket.textContent.length > 0) && $rangeInputCsTicket.length > 0) {
      var index = $(this).index();
      $rangeInputCsTicket.val(index).trigger('change');
      sheetCsTicket.textContent = getTrackStyleCsTicket($rangeInputCsTicket.first()[0]);
    }
  });
  $(document).on('keyup', '.form-control', function () {
    checkValInput($(this).closest('.modal.show'));
  })

  $(document).on('click', '.range-labels-cs-ticket.new li, .edit-range-labels li', function () {
    if ($(this).text() !== '0') {
      checkValInput($(this).closest('.modal.show'));
    } else {
      $('#create-cs-ticket').find('.btn-save-modal').attr('disabled', true).removeClass('btn-reti-primary').addClass('btn-tms-unable-submit');
    }
  })
}

function checkValInput(modalId) {
  let checkValInputRange = true;
  let multipleSelect = modalId.find('.form-control.multiple-select2');
  if (modalId.find('input[type="range"]').length > 0) {
    checkValInputRange = modalId.find('input[type="range"]').filter(function () {
      return $(this).val() === "0";
    }).length === 0
  }
  if (modalId.find('.form-control').not('#cs_ticket_voucher_id').not('#cs_ticket_note').not('.d-none').filter(function () {
      if (!this.type.includes('multiple')) {
        return $(this).val().trim().length === 0;
      }
    }).length === 0
    && multipleSelect.filter(function () {
      return $(this).val().length === 0;
    }).length === 0 && checkValInputRange && modalId.find('.field_with_errors input').length === 0) {
    modalId.find('.btn-save-modal').attr('disabled', false).removeClass('btn-tms-unable-submit').addClass('btn-reti-primary');
  } else {
    modalId.find('.btn-save-modal').attr('disabled', true).removeClass('btn-reti-primary').addClass('btn-tms-unable-submit');
  }
}

function ajaxReloadEditDealCustomer(dealId) {
  $.ajax({
    url: `/deals/${dealId}`,
    type: 'PATCH',
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },
    data: {
      refresh: true,
      deal_id: dealId
    }
  })
  .done(function () {
  })
}

function ajaxReloadCsTicket(csTicketId, dealId) {
  $.ajax({
    url: `/cs_tickets/${csTicketId}`,
    type: 'PATCH',
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },
    data: {
      reload: true,
      deal_id: dealId,
      form: getUrlParameter('form'),
      after_create: getUrlParameter('after_create'),
      updated_at: getUrlParameter('updated_at')
    }
  })
  .done(function () {
  })
}

var getUrlParameter = function getUrlParameter(sParam) {
  var sPageURL = window.location.search.substring(1), sURLVariables = sPageURL.split('&'), sParameterName, i;
  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');
    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
    }
  }
  return false;
};

function setHeightKanbanColumn() {
  let height = 200;
  $('.kanban-body-container').each(function () {
    let childHeight = 0;
    $(this).find('.kanban-item').each(function () {
      childHeight += ($(this).height() + 8);
    })
    if (childHeight > height) {
      height = childHeight;
    }
  })
  $('.kanban-body-container').css('height', height + 16);
}

function swalContactStatus() {
  let html = '<div class="form-group">'
    + '<div class="swal-input">'
    + '<label>Trạng thái</label>'
    + '<select class="form-control tms-select2-deal-interest select2-deal-interest">'
    + '<option value=interest>Quan tâm</option>'
    + '<option value=uninterested>Không quan tâm</option>'
    + '</select>'
    + '</div>'
    + '</div>'
    + '<div class="form-group d-none">'
    + '<div class="swal-input">'
    + '<label>Lý do hủy</label>'
    + '<select class="form-control tms-select2-deal-interest select2-uninterested-reason">'
    + '<option value>Vui lòng chọn</option>'
    + '<option value=0>Thông tin không phù hợp</option>'
    + '<option value=1>Spam</option>'
    + '<option value=2>Đã mua sản phẩm tương tự</option>'
    + '<option value=3>Môi giới</option>'
    + '</select>'
    + '</div>'
    + '</div>'
  swalBootstrapUninterested.fire({
    title: 'Cập nhật trạng thái mới cho giao dịch',
    html: html,
    showConfirmButton: true,
    showCancelButton: true,
    cancelButtonText: 'Huỷ',
    confirmButtonText: 'Xác nhận',
    showCloseButton: false,
    allowOutsideClick: false,
    focusConfirm: false,
    onOpen: function () {
      $('.tms-select2-deal-interest').select2({
        theme: 'bootstrap swal-select2-container',
        language: 'vi',
        placeholder: 'Vui lòng chọn'
      });
    }
  }).then((result) => {
    if (result.isConfirmed) {
      $('#deal-contact-status').val($('.select2-deal-interest').val());
      $('#deal-uninterested-reason').val($('.select2-uninterested-reason').val());
    }
  })
}

function filter(word, items, dropdown) {
  let length = items.length
  let hidden = 0
  for (let i = 0; i < length; i++) {
    if ($(items[i]).closest('.export-deals-optional-list-field').length > 0) {
      if (parameterize(items[i].textContent.toString().toLowerCase()).includes(parameterize(word))) {
        $(items[i]).show()
      } else {
        $(items[i]).hide()
        hidden++
      }
    }
  }
  if (hidden === length) {
    $(dropdown).find('.dropdown_empty').show();
  } else {
    $(dropdown).find('.dropdown_empty').hide();
  }
}

// Ajax request wrapper
var $ajax = function (url, type, data) {
  return new Promise(function (resolve, reject) {
    $.ajax({
      url: url,
      type: type,
      data: data,
      headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
      },
      success: function (response) {
        resolve(response);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        reject(textStatus);
      },
      timeout: 15000
    });
  });
}

// Toggle fields when choose commission calculation type
$(document).on('click', '.commission-calculation-type-radio', function () {
  let isDisabled = $($(this).children()[0]).prop('disabled');
  let isAccountant = $($(this).children()[0]).attr('data-is-accountant');
  let calculationType = $(this).attr('data-calculation-type');
  if (!isDisabled) {
    if (calculationType === '0') {
      $('.fixed-calculation').addClass('d-none');
      $('.percentage-calculation').children().not('.fixed-calculation').removeClass('d-none');
      $('.temporary-commission-calculation').removeClass('d-none');
      if (isAccountant === 'false') {
        $('.percentage-calculation').removeClass('d-none');
      } else {
        $('#deal_commission_attributes_price_to_calculate_commission_for_seller').parent().parent().removeClass('d-none');
      }
    } else {
      $('.fixed-calculation').removeClass('d-none');
      $('.percentage-calculation').children().not('.fixed-calculation').addClass('d-none');
      $('.temporary-commission-calculation').addClass('d-none');
    }
  }
})

$(document).on('click', '#btn-calculate-commission', function () {
  calculateTemporaryCommission();
})

function calculateSellerCommissionRateWillBeReceived() {
  let retiCommissionRateForSeller = $('#deal_commission_attributes_reti_commission_rate_for_seller').val();
  let sellerCommissionRateFromReti = $('#deal_commission_attributes_seller_commission_rate_from_reti').val();
  if (retiCommissionRateForSeller !== '' && sellerCommissionRateFromReti !== '') {
    let sellerCommissionRateWillBeReceived = parseFloat((retiCommissionRateForSeller * sellerCommissionRateFromReti) / 100).toFixed(2);
    $('#deal_commission_attributes_seller_commission_rate_will_be_received').val(sellerCommissionRateWillBeReceived);
    $('#deal_commission_attributes_seller_commission_rate_will_be_received').trigger('input');
  } else {
    $('#deal_commission_attributes_seller_commission_rate_will_be_received').val('');
  }
}

function calculateTemporaryCommission() {
  let a = parseFloat($('#deal_commission_attributes_seller_commission_rate_will_be_received').val()),
    b = parseFloat($('#deal_commission_attributes_price_to_calculate_commission_for_seller').val().replaceAll('.', ''));
  let c = Math.round((a * b * 90) / 10000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  $('#temporary-commission-result').html(`${c}đ`);
  $('#temporary-commission-result').parent().show();
}

function enableBtnCalculateTemporaryCommission() {
  let a = $('#deal_commission_attributes_seller_commission_rate_will_be_received').val(),
    b = $('#deal_commission_attributes_price_to_calculate_commission_for_seller').val();
  if (a.length > 0 && b.length > 0) {
    $('#btn-calculate-commission').prop('disabled', false);
  } else {
    $('#btn-calculate-commission').prop('disabled', true);
  }
}

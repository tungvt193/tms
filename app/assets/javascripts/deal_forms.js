$(document).on('turbolinks:load', function () {
  var dealNameSuggestion = new Bloodhound({
    limit: 10,
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: location.origin + '/deal_forms/autocomplete?query=%QUERY',
      filter: function (data) {
        return data;
      },
      wildcard: "%QUERY"
    }
  });

  var dealTypeahead = $('.deal-form-search');
  dealTypeahead.typeahead({
    hint: true,
    highlight: true,
    minLength: 1
  },
    {
      limit: 500,
      name: 'value',
      displayKey: 'value',
      source: dealNameSuggestion.ttAdapter(),
      templates: {
        empty: '<div class="noitems">Không tìm thấy kết quả</div>',
        suggestion: function (data) {
          return '<div class="d-flex justify-content-start"><div class="deal-form-autocomplete"><i class="tms tms-customer mr-2"></i></div><div>' + data.value + '</div></div>';
        }
      }
    }).on('typeahead:selected', function ($e, data) {
    window.location.href = data.path
  });
  $('.img-deal-form').css({'height': ($(window).height() * 0.65)});
  if ($('#mobile-layout')) {
    $(document).on('click', '#assignee-edit', function () {
      $('#assignee').click();
    })
  }

  $('.sidebar-filter .dropdown').each(function (index, dropdown) {
    let search = $(dropdown).find('.search');
    let search_state = $('.dropdown-state').find('.search');
    let items = $(dropdown).find('.dropdown-item');
    let items_filter = search.parents('.dropdown-menu').find('.sidebar-filter-checkbox');
    $(search).on('input', function () {
      filter($(search).val().trim().toLowerCase())
    });

    function filter(word) {
      let length = items.length
      let collection = []
      let hidden = 0
      for (let i = 0; i < length; i++) {
        if (search_state.val().length > 0) {
          if (parameterize(items[i].text.toLowerCase()).includes(parameterize(word))) {
            $(items[i]).show()
          } else {
            $(items[i]).hide()
            hidden++
          }
        } else if (search.closest('.sidebar-filter-body').length > 0) {
          if (parameterize(items_filter[i].textContent.trim().toLowerCase()).includes(parameterize(word))) {
            $(items_filter[i]).show()
          } else {
            $(items_filter[i]).hide()
            hidden++
          }
        } else {
          if (parameterize(items[i].value.toString().toLowerCase()).includes(parameterize(word))) {
            $(items[i]).show()
          } else {
            $(items[i]).hide()
            hidden++
          }
        }
      }
      if (hidden === length) {
        $(dropdown).find('.dropdown_empty').show();
      } else {
        $(dropdown).find('.dropdown_empty').hide();
      }
    }
  });

  $('.dropdown').each(function (index, dropdown) {
    $(dropdown).find('.dropdown-menu').find('.menuItems').on('click', '.dropdown-item', function (e) {
      if ($(this).parent().hasClass('deal-info-tabs')) {
        var isEditing = !$(`${$(this).attr('href')}`).hasClass('active') && ($('.active.tab-pane form').data('is-editing') || $('.active.tab-pane .btn-save-nested:visible').length > 0);
        if (isEditing) {
          e.stopImmediatePropagation();
          swalWithBootstrapButtons.fire({
            title: 'Bạn chưa lưu thay đổi',
            html: "Các thay đổi của bạn chưa được lưu lại.<br/>Vui lòng <strong>Lưu lại</strong> hoặc <strong>Huỷ</strong> các thay đổi hiện tại!",
            showCloseButton: true,
            confirmButtonText: 'Đóng',
            reverseButtons: true,
            allowOutsideClick: false
          })
        } else {
          $(dropdown).find('.dropdown-toggle').text($(this)[0].value);
          $(dropdown).find('.dropdown-toggle').dropdown('toggle');
          $(this).parent().find('.dropdown-item').removeClass('active');
          $('.dropdown-menu').removeClass('show');
        }
      } else {
        $(dropdown).find('.dropdown-toggle').text($(this)[0].value);
        $(dropdown).find('.dropdown-toggle').dropdown('toggle');
      }
    })
  })

});

$(document).on('click', '.assignee-mobile.assignee', function () {
  let page;
  let dealId;
  let $_this = $(this);
  let img;
  if ($(this).hasClass('list-page')) {
    page = 'list-page';
    dealId = $(this).closest('.kanban-item').data('id');
    img = $(this).find('img');
  } else {
    page = 'deatil-page';
    dealId = $(this).closest('#modal-edit').data('id');
  }
  $ajax(`/deals/assignee_search`, 'get', '').then(
    (value) => {
      let assignee_actions = [];
      $.each(value, function () {
        let assignee_id = this.id;
        let assignee_avatar = this.avatar;
        let text = `${this.text}</br><label class="info-item-label m-0" style="color: #828282;">${this.email}</label>`;
        let textValue = this.text;
        assignee_actions.push({
          text: text,
          value: this.id,
          onClick: function () {
            var data = {deal: {assignee_id: assignee_id}};
            var successCallback = function (res) {
              if (res.status == 'ok') {
                if (page == 'list-page') {
                  img.attr('src', assignee_avatar);
                } else {
                  $_this.closest('.row').find('.info-item-content').text(textValue);
                  $('.team-lead-content').empty().append(res.team_lead);
                }
                Swal.fire({
                  position: 'center',
                  icon: 'success',
                  title: "Bạn đã phân công cho " + textValue,
                  showConfirmButton: false,
                  showClass: {
                    popup: 'swal-create-customer',
                  },
                  timer: 2000
                });
              }
            }
            var failureCallback = function (res) {
              // Do nothing
            }
            $ajax(`/deals/${dealId}/update_assignee`, 'patch', data).then(successCallback, failureCallback);
          }
        })
      });
      $.actions({
        title: 'Chọn người phụ trách',
        search: true,
        actions: assignee_actions,
      })
    }
    );
})

// js deal mobile
$(document).on('click', '.a-collapse', function () {
  if ($(this).hasClass('collapsed')) {
    $(this).html("Xem thêm <i class = 'tms tms-down-mobile ml-2'></i>")
  } else {
    $(this).html("Ẩn bớt <i class = 'tms tms-up-mobile ml-2'></i>")
  }
})

$(document).on('click', '#deal_source_mobile', function () {
  let actions = [];
  let check_click_edit = $.trim($('#deal_source_mobile').find('.text-display').text()) == 'Chưa cập nhật';
  $('#deal_source').find('optgroup').each(function () {
    let group = $(this).attr('label')
    $(this).find('option').each(function () {
      let text = $(this).text();
      let successCallback = function (res) {
        $('#deal_source_mobile').find('.text-display').html(text);
        // Check allow assignee
        if ($('.dropdown-assign .info-item-content').is('#assignee-readonly') && res.allow_assignee) {
          $('#assignee-readonly').attr('id', 'assignee');
          $('.assignee-mobile').addClass('assignee');
          $('.assignee-mobile .detail-page').removeClass('d-none');
        }
        // Trigger focus required fields
        if (res.general_invalid) {
          if (check_click_edit) {
            if ($('#general-info button[name="edit"]')) $('#general-info button[name="edit"]').click();
            if ($('#general-info button[name="save"]')) $('#general-info button[name="save"]').click();
          }
        }
        $('#deal_state').removeAttr('disabled');
      }
      let failureCallback = function (res) {
        // Do nothing
      }
      if ($(this).text()) {
        actions.push({
          group: group,
          text: text,
          value: $(this).attr('value'),
          disabled: ($(this).attr('disabled') == 'disabled'),
          onClick: function () {
            let dealId = $('#modal-edit').data('id');
            let data = {deal: {source: $(this)[0].value}};
            $ajax(`/deals/${dealId}/update_source`, 'patch', data).then(successCallback, failureCallback);
          }
        })
      }
    })
  })
  $.actions({
    title: 'Chọn nguồn',
    search: true,
    actions: actions,
  })
})

$(document).on('click', '.clickable-label .info-item-label', function () {
  if (!$('#input-show-text-source-detail').is(':visible')) {
    $('.input-show-commission-group input.form-control').click();
  }
})

$(document).ready(function () {
  if ($("#deal_created_at_mobile").length > 0) {
    $("#deal_created_at_mobile").cityPicker({
      element: "#create_deal_date",
      onClose: function () {
      },
      onSubmit: function () {
        let dealId = $('#modal-edit').data('id');
        let data = {deal: {create_deal_date: $('#create_deal_date').val()}};
        let successCallback = function () {
          $('#deal_created_at_mobile').find('.text-display').html($('#create_deal_date').val());
          $('#deal_created_at_mobile .btn-edit-mobile').remove();
          $('#deal_created_at_mobile').removeAttr('id');
        }
        $ajax(`/deals/${dealId}/update_create_date`, 'patch', data).then(successCallback, '');
      },
    })
  }
});

$(document).on('click', 'a.toggle-tab-general', function () {
  $(".dropdown-item[href='#general-info']").click();
  mobileAutoFocusWhenError();
})

$(document).on('click', 'a.toggle-tab-customer', function () {
  $(".dropdown-item[href='#customer-portrait']").click();
  mobileAutoFocusWhenError();
})

// end
// js deal form
$(document).on('click', '.same-customer', function () {
  $('#deal_customer_name').val($(this).data('customer-name'));
  $(this).closest('.form-group').removeClass('field_with_errors').find('.field_with_errors').removeClass('field_with_errors');
  $('#deal_customer_name').closest('.form-group').find('.field_with_errors').removeClass('field_with_errors');
  $('#deal_customer_name').closest('.form-group').find('span').remove();
  $(this).parent().remove();
})

$(document).on('change', '#deal_state_detail', function () {
  if ($(this).val() == 'canceled') {
    $('.canceled-reason-field').show();
    $('.interaction-plan-field').hide();
  } else {
    $('.canceled-reason-field').hide();
    $('.interaction-plan-field').show();
  }
})

// $(document).on('change', '#deal_customer_phone, #deal_customer_name', function () {
//   let phone_number = $('#deal_customer_phone').val();
//   $.ajax({
//     url: "/customers/check_same_phone_deal_form",
//     type: "get",
//     data: {
//       country_code: 'VN',
//       customer_name: $('#deal_customer_name').val(),
//       phone_number: phone_number
//     },
//     success: function (data) {
//       if (data.msg) {
//         $('#deal_customer_phone').parent().find('.invalid-feedback').remove();
//         $('#deal_customer_phone').after(`<span class='invalid-feedback custom-validation'>${data.msg} <i class="tms tms-info" data-html="true" data-placement="top" data-toggle="tooltip" title="Click vào Tên khách hàng để kết nối giao dịch với khách hàng này"></i></span>`);
//         $('[data-toggle="tooltip"]').tooltip({
//           template: '<div class="tooltip tms-tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
//         });
//         $('#deal_customer_phone').parent().addClass('field_with_errors');
//       } else {
//         $('#deal_customer_phone').parent().removeClass('field_with_errors');
//         $('#deal_customer_phone').parent().find('.custom-validation').remove();
//         $('.same-customer-tooltip').addClass('d-none');
//       }
//     }
//   });
// });

if (isMobile) {
  $(document).on('touchmove', 'body', function (e) {
    if ($(this).find('.kanban-mobile').length > 0) {
      if ($(window).scrollTop() + 200 >= ($(document).height() - $(window).height())) {
        if ($('.kanban-body-container').attr('load') == 'true') {
          showLoading(true);
          let num_page = parseInt($(".kanban-body-container").attr('num_page'));
          load_deals({
            replace: false,
            num_page: num_page
          });
        }
      }
    }
  })

  $(document).on('keyup', '#deal-search', function (e) {
    if (e.keyCode == 13) {
      $(".kanban-body-container").attr({'load': 'true', 'num_page': 0});
      showLoading(true);
      let num_page = parseInt($(".kanban-body-container").attr('num_page'));
      load_deals({
        replace: true,
        num_page: num_page - 1
      });
    }
  });

  var order_by_value;
  var sort_value;
  $(document).ready(function () {
    order_by_value = $('input[type=radio][name=order_by]:checked').val();
    sort_value = $('input[type=radio][name=sort]:checked').val();
  })

  $(document).on('click', '.btn-check', function () {
    $('form.navbar-search').submit();
  });

  $(document).on('change', 'input[type=radio][name=order_by]', function () {
    if ($(this).val() == 'none') {
      $('input[name=sort]').attr('disabled', 'disabled');
      $('input[name=sort]')[0].checked = true;
      $('input[name=sort]').closest('.container').addClass('disabled');
    } else {
      $('input[name=sort]').removeAttr('disabled');
      $('input[name=sort]').first().attr('checked', true);
      $('input[name=sort]').closest('.container').removeClass('disabled');
    }
  });

  $(document).on('click', '.dropdown-body-sort-deal', '.dropdown-header-sort-deal', function () {
    $('.dropdown-menu-sort-deal').addClass('dropdown-sorting-show')
  });

  $(document).on('click', '.btn-canceled', function () {
    $(`input[type=radio][name=sort][value=${sort_value}]`).click();
    $(`input[type=radio][name=order_by][value=${order_by_value}]`).click();
    $('.dropdown-menu-sort-deal').removeClass('dropdown-sorting-show');
  });

  function load_deals(params) {
    let num_page = params.num_page;
    $(".kanban-body-container").attr('load', 'false');
    showLoading(true);
    let sort = $('input[type=radio][name=sort]:checked').val() == undefined ? 'ASC' : $('input[type=radio][name=sort]:checked').val();
    let assignee = [];
    let project = [];
    let product_type = [];
    let purchase_purpose = [];
    $('.sidebar-filter-checkbox input[name="assignee[]"]:checked').each(function () {
      assignee.push($(this).val())
    });
    $('.sidebar-filter-checkbox input[name="project[]"]:checked').each(function () {
      project.push($(this).val())
    });
    $('.sidebar-filter-checkbox input[name="product_type[]"]:checked').each(function () {
      product_type.push($(this).val())
    });
    $('.sidebar-filter-checkbox input[name="purchase_purpose[]"]:checked').each(function () {
      purchase_purpose.push($(this).val())
    });
    let data = {
      is_loaded: true,
      page: num_page + 1,
      query: $('#deal-search').val(),
      'assignee': assignee,
      'balcony_direction': rejectUndefined('#balcony-direction-search-deal'),
      'created_at': $('#created-at-search-deal').val(),
      'door_direction': rejectUndefined('#door-direction-search-deal'),
      'group': rejectUndefined('#group-search-deal'),
      'label': rejectUndefined('#label-search-deal'),
      'product_type': product_type,
      'project': project,
      'purchase_purpose': purchase_purpose,
      'source': rejectUndefined('#source-search-deal'),
      'state': [$('#current-state').val()],
      'updated_at': $('#updated-at-search-deal').val(),
      'order_by': $('input[type=radio][name=order_by]:checked').val(),
      'sort': sort
    }
    var successCallback = function (res) {
      let html_append = $(res).find('.kanban-item');
      if (html_append.length < 5) {
        $('.kanban-body-container').attr('load', 'false');
      } else {
        $(".kanban-body-container").attr({'load': 'true', 'num_page': num_page + 1});
      }
      if (params.replace) {
        if (html_append.length == 0) {
          html_append = "<div class='card mb-2 ml-1 mr-1 mt-1 p-3'><p>Không có kết quả ở trạng thái này, anh/chị chọn trạng thái khác để xem</p></div>"
        }
        $(".kanban-body-container").html(html_append);
      } else {
        $(".kanban-body-container").append(html_append);
      }
      let html_dropdown = $(res).find('.dropdown.tms-card');
      $(".dropdown-state").html(html_dropdown);
      let text_span = $('.dropdown-toggle').find('span').first();
      if (text_span.text().length <= 16) {
        text_span.removeClass('dropdown-state');
      } else {
        text_span.addClass('dropdown-state');
        showLoading(false);
      }
      showLoading(false);
    }
    var failureCallback = function (res) {
      // Do nothing
    }
    $ajax(`/deals`, 'get', data).then(successCallback, failureCallback)
  }
}

$(document).on('change', '.range input', function () {
  sheet.textContent = getTrackStyle(this);
  let dealId = $('#modal-edit').data('id');
  let data = {lead_scoring: $(this).val()};
  $ajax(`/deals/${dealId}/update_lead_scoring`, 'patch', data).then();
});

$(document).on('click', '.btn-show-filter', function () {
  $('.content-wrapper').append(`<div id="sidebar-overlay"></div>`);
  $('body').find('#sidebar-overlay').addClass('d-block');
  $('body').addClass('overflow-hidden');
  $('.form-search-filter-sidebar').addClass('sidebar-filter-open');
});

$(document).on('click', '.icon-back-header', function () {
  $('.form-search-filter-sidebar').removeClass('sidebar-filter-open');
  $('body').find('#sidebar-overlay').remove();
  $('body').removeClass('overflow-hidden');
});

$(document).on('click', '#sidebar-overlay.d-block', function () {
  $('.form-search-filter-sidebar').removeClass('sidebar-filter-open');
  $(this).remove();
  $('body').removeClass('overflow-hidden');
});

$(document).on('click', '.btn-canceled-filter', function () {
  $('.sidebar-filter-body').find('.sidebar-filter-checkbox input[type=checkbox]:checked').each(function () {
    $(this).parents('.dropdown').find('.btn-filter').each(function () {
      if ($(this).text().indexOf('(') !== -1) {
        $(this).text($(this).text().substr(0, $(this).text().indexOf('(')));
      }
      $(this).css({background: '#FFFFFF', color: '##212529'});
    });
    $(this).parent().css({background: '#FFFFFF', color: '#6c757d', fontWeight: 'inherit'});
    $(this).prop('checked', false);
  });
  if ($('.sidebar-filter-checkbox input[type=checkbox]:checked').length === 0) {
    $('.border-50').css('display', 'none');
  }
});

$(document).on('click', '.btn-submit-filter', function () {
  $(this).parents('form.navbar-search').submit();
});

$(document).on('click', '.sidebar-filter-checkbox', function () {
  if ($(this).parents('.dropdown').hasClass('show')) {
    $(this).parents('.dropdown').find('.dropdown-toggle').dropdown('toggle');
  }
});

$(document).on('change', '.sidebar-filter-checkbox input[type=checkbox]', function () {
  var length = $(this).parents('.dropdown').find('.sidebar-filter-checkbox input[type=checkbox]:checked').length;
  var text = '';
  switch ($(this).attr('name')) {
    case 'project[]':
      text = 'Dự án';
      break;
    case 'product_type[]':
      text = 'Loại hình sản phẩm';
      break;
    case 'purchase_purpose[]':
      text = 'Mục đích mua';
      break;
    case 'subdivision_id[]':
      text = 'Tòa';
      break;
    case 'block_id[]':
      text = 'Tòa';
      break;
    case 'floor_id[]':
      text = 'Tầng';
      break;
    case 'name[]':
      text = 'Mã căn/Sàn';
      break;
    case 'real_estate_type[]':
      text ='Loại hình';
      break;
    case 'state[]':
      text = 'Trạng thái';
      break;
    default:
      break;
  }
  var isChecked = $(this).is(':checked');
  if (!isChecked) {
    $(this).parent().css({background: '#FFFFFF', color: '#6c757d', fontWeight: 'inherit'});
  } else {
    // $('.border-50').css('display', 'block');
    $(this).parent().css({background: '#F9F9F9', color: '#595959', fontWeight: '500'});
  }
  // if (length > 0) {
  //   $('.border-50').css('display', 'block');
  // }
  // else {
  //   $('.border-50').css('display', 'none');
  // }
  $(this).parents('.dropdown').find('.dropdown-toggle').dropdown('toggle');
  // if ($(this).attr('name') === 'assignee') {
  //   $(this).parents('.dropdown').find('.btn-filter').text('Người chăm sóc ' + `(${length})`).css({
  //     background: '#56C5D0',
  //     color: '#FFFFFF'
  //   });
  //   if (length === 0) {
  //     $(this).parents('.dropdown').find('.btn-filter').text('Người chăm sóc').css({
  //       background: '#FFFFFF',
  //       color: '#212529'
  //     })
  //   }
  // }
  checkbox_checked($(this).attr('name'), text, length);
});

function checkbox_checked(input_name, text, length) {
  var $this =  $(`input[name='${input_name}']`)
  $this.parents('.dropdown').find('.btn-filter').text(text + `(${length})`).css({
    background: '#56C5D0',
    color: '#FFFFFF'
  });
  if (length === 0) {
    $this.parents('.dropdown').find('.btn-filter').text(text).css({background: '#FFFFFF', color: '#212529'})
  }
}

$(document).on('turbolinks:load', function () {
  $('.menuItems').find('.sidebar-filter-checkbox').each(function () {
    if ($(window).width() <= 320) {
      if ($(this).text().trim().length >= 25) {
        $(this).addClass('pt-01');
      }
      if ($(this).text().trim().length >= 50) {
        $(this).addClass('h-50px');
      }
    } else {
      if ($(this).text().trim().length > 30) {
        $(this).addClass('pt-01');
      }
    }
  });
  $('.icon-back-header').click(function () {
    window.scrollTo(0, 0);
  });

  $(document).on('click', '.btn-toggle', function () {
    $('.wrapper').find('#sidebar-overlay').css({
      backgroundColor: 'rgba(0, 0, 0, 0.38)',
      display: 'block',
      height: 1000
    });
    $(this).removeClass('d-block').addClass('d-none');
    $('body').addClass('overflow-hidden');
  });

  $(document).on('click', '.btn-toggle-cancel', function () {
    $('.wrapper').find('#sidebar-overlay').attr('style', '');
    $('#navbarCTA-customer').removeClass('show');
    $('#navbarCTA-appointment').removeClass('show');
    $('#navbarCTA').removeClass('show');
    $('.btn-toggle').removeClass('d-none').addClass('d-block');
    $('body').removeClass('overflow-hidden');
  });

  $(document).on('click', '.tms-edit-btn', function () {
    $('.wrapper').find('#sidebar-overlay').attr('style', '');
    $('body').removeClass('overflow-hidden');
  });

  $(document).on('click', '#sidebar-overlay', function () {
    $('.wrapper').find('#sidebar-overlay').attr('style', '');
    $('.btn-toggle').removeClass('d-none').addClass('d-block');
    $('#navbarCTA-customer').removeClass('show');
    $('#navbarCTA-appointment').removeClass('show');
    $('#navbarCTA').removeClass('show');
    $('body').removeClass('overflow-hidden');
  });

  $(document).on('click', '.btn-deal-cancel', function () {
    $('.wrapper').find('#sidebar-overlay').attr('style', '');
  });

  let unchecked_projects = [], unchecked_product_types = [], unchecked_purchase_purposes = [], unchecked_subdivision_ids = [], unchecked_block_ids = [],
    unchecked_floor_ids = [], unchecked_names = [], unchecked_real_estate_types = [], unchecked_states = [];
  let projects = [], product_types = [], purchase_purposes = [], subdivision_ids = [], block_ids = [], floor_ids = [], names = [], real_estate_types = [], states = [];
  $('.sidebar-filter-checkbox input[type=checkbox]').map(function (i, ele) {
    switch ($(this).attr('name')) {
      case 'project[]':
        unchecked_projects.push(ele.parentElement);
        break;
      case 'product_type[]':
        unchecked_product_types.push(ele.parentElement);
        break;
      case 'purchase_purpose[]':
        unchecked_purchase_purposes.push(ele.parentElement);
        break;
      case 'subdivision_id[]':
        unchecked_subdivision_ids.push(ele.parentElement);
        break;
      case 'block_id[]':
        unchecked_block_ids.push(ele.parentElement);
        break;
      case 'floor_id[]':
        unchecked_floor_ids.push(ele.parentElement);
        break;
      case 'name[]':
        unchecked_names.push(ele.parentElement);
        break;
      case 'real_estate_type[]':
        unchecked_real_estate_types.push(ele.parentElement);
        break;
      case 'state[]':
        unchecked_states.push(ele.parentElement);
        break;
    }
  });
  $('.btn-filter').on('click', function () {
    $(this).parent().find('.custom-list-state').scrollTop(0);
    $(".custom-list-state").find('.sidebar-filter-checkbox input[type=checkbox]:checked').map(function (i, ele) {
      switch ($(this).attr('name')) {
        case 'project[]':
          projects.push(ele.parentElement);
          break;
        case 'product_type[]':
          product_types.push(ele.parentElement);
          break;
        case 'purchase_purpose[]':
          purchase_purposes.push(ele.parentElement);
          break;
        case 'subdivision_id[]':
          subdivision_ids.push(ele.parentElement);
          break;
        case 'block_id[]':
          block_ids.push(ele.parentElement);
          break;
        case 'floor_id[]':
          floor_ids.push(ele.parentElement);
          break;
        case 'name[]':
          names.push(ele.parentElement);
          break;
        case 'real_estate_type[]':
          real_estate_types.push(ele.parentElement);
          break;
        case 'state[]':
          states.push(ele.parentElement);
          break;
      }
    });
    $(".custom-list-state").find('.sidebar-filter-checkbox input:checkbox:not(:checked)').map(function (i, ele) {
      switch ($(this).attr('name')) {
        case 'project[]':
          projects.push(ele.parentElement);
          break;
        case 'product_type[]':
          product_types.push(ele.parentElement);
          break;
        case 'purchase_purpose[]':
          purchase_purposes.push(ele.parentElement);
          break;
        case 'subdivision_id[]':
          subdivision_ids.push(ele.parentElement);
          break;
        case 'block_id[]':
          block_ids.push(ele.parentElement);
          break;
        case 'floor_id[]':
          floor_ids.push(ele.parentElement);
          break;
        case 'name[]':
          names.push(ele.parentElement);
          break;
        case 'real_estate_type[]':
          real_estate_types.push(ele.parentElement);
          break;
        case 'state[]':
          states.push(ele.parentElement);
          break;
      }
    });
    var name = $(this).parent().find('input[type=checkbox]').attr('name');
    if (name === 'project[]') {
      if ($('.sidebar-filter-checkbox input[type=checkbox][name^="project"]:checked').length === 0) {
        $(this).parent().find('.custom-list-state').html(unchecked_projects);
      } else {
        $(this).parent().find('.custom-list-state').html(projects);
      }
    }
    if (name === 'product_type[]') {
      if ($('.sidebar-filter-checkbox input[type=checkbox][name^="product_type"]:checked').length === 0) {
        $(this).parent().find('.custom-list-state').html(unchecked_product_types);
      } else {
        $(this).parent().find('.custom-list-state').html(product_types);
      }
    }
    if (name === 'purchase_purpose[]') {
      if ($('.sidebar-filter-checkbox input[type=checkbox][name^="purchase_purpose"]:checked').length === 0) {
        $(this).parent().find('.custom-list-state').html(unchecked_purchase_purposes);
      } else {
        $(this).parent().find('.custom-list-state').html(purchase_purposes);
      }
    }
    if (name === 'subdivision_id[]') {
      if ($('.sidebar-filter-checkbox input[type=checkbox][name^="subdivision_id"]:checked').length === 0) {
        $(this).parent().find('.custom-list-state').html(unchecked_subdivision_ids);
      } else {
        $(this).parent().find('.custom-list-state').html(subdivision_ids);
      }
    }
    if (name === 'block_id[]') {
      if ($('.sidebar-filter-checkbox input[type=checkbox][name^="block_id"]:checked').length === 0) {
        $(this).parent().find('.custom-list-state').html(unchecked_block_ids);
      } else {
        $(this).parent().find('.custom-list-state').html(block_ids);
      }
    }
    if (name === 'floor_id[]') {
      if ($('.sidebar-filter-checkbox input[type=checkbox][name^="floor_id"]:checked').length === 0) {
        $(this).parent().find('.custom-list-state').html(unchecked_floor_ids);
      } else {
        $(this).parent().find('.custom-list-state').html(floor_ids);
      }
    }
    if (name === 'name[]') {
      if ($('.sidebar-filter-checkbox input[type=checkbox][name^="name"]:checked').length === 0) {
        $(this).parent().find('.custom-list-state').html(unchecked_names);
      } else {
        $(this).parent().find('.custom-list-state').html(names);
      }
    }
    if (name === 'real_estate_type[]') {
      if ($('.sidebar-filter-checkbox input[type=checkbox][name^="real_estate_type"]:checked').length === 0) {
        $(this).parent().find('.custom-list-state').html(unchecked_real_estate_types);
      } else {
        $(this).parent().find('.custom-list-state').html(real_estate_types);
      }
    }
    if (name === 'state[]') {
      if ($('.sidebar-filter-checkbox input[type=checkbox][name^="state"]:checked').length === 0) {
        $(this).parent().find('.custom-list-state').html(unchecked_states);
      } else {
        $(this).parent().find('.custom-list-state').html(states);
      }
    }
  });
})

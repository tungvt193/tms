module BlogService
  # type: new or video
  def get_posts_from_blog(url, type)
    require 'net/http'
    posts = []
    begin
      uri = URI.parse(url)
      Net::HTTP.start(uri.host, uri.port, use_ssl: uri.scheme == 'https', open_timeout: 1) do |http|
        request = Net::HTTP::Get.new uri
        response = http.request request
        if response.code === '200'
          JSON.parse(response.body).each do |post|
            posts.push({
                         'text' => post['title']['rendered'],
                         'link' => type == 'news' ? post['link'] : post['external_url'],
                         'image_url' => begin
                                          post['_embedded']['wp:featuredmedia'][0]['media_details']['sizes']['td_100x70']
                                        rescue
                                          nil
                                        end
                       })
          end
        end
      end
    rescue Exception => e
      puts e
    end
    posts
  end
end
require "axlsx"
class CustomerService
  def initialize file_path = nil, user = nil
    @file_path = file_path
    @user = user
  end

  def import
    @result_file = Axlsx::Package.new
    wb = @result_file.workbook
    @ws = wb.add_worksheet(name: 'Import Lỗi')
    style = wb.styles.add_style bg_color: '3ac5c9', fg_color: 'FF', b: true
    @ws.add_row ['Vị trí', 'Nội dung lỗi'], style: style
    ods = open_spreadsheet(@file_path)
    @import_success = true
    add_error('Max size', 'Giới hạn import mỗi lần là 1000 dòng') if ods.sheet(0).count > 1000
    import_data ods.sheet(0)
    # @result_file
    if @import_success
      { message: 'Dữ liệu đã được tải lên thành công' }
    else
      tempfile = File.new("#{Rails.root}/public/customer_error.xlsx", "w")
      @result_file.serialize tempfile
      UserMailer.notify_import(@user, tempfile, 'customer_error.xlsx', 'Thông báo lỗi import khách hàng').deliver
      ::File.delete tempfile
      { message: "Có một số lỗi xảy ra khi tải dữ liệu lên. Vui lòng check email < #{@user.email} > để xem chi tiết" }
    end
  end

  private

  def open_spreadsheet(file_path)
    case File.extname(file_path)
    when '.csv' then
      Roo::CSV.new(file_path)
    when '.xls' then
      Roo::Excel.new(file_path)
    when '.xlsx' then
      Roo::Excelx.new(file_path)
    else
      raise "Unknown file type: #{File.basename(file_path, ".*")}"
    end
  end

  def import_data spreadsheet
    header = spreadsheet.row(1)
    customers = []
    (2..spreadsheet.last_row).each do |i|
      row = spreadsheet.row(i)
      next if row[0]&.strip.blank? && row[1]&.strip.blank?
      customer = Customer.new
      customer.name = row[0]&.strip
      customer.phone_number = row[1]&.strip
      customer.created_at = row[21] if row[21].present?
      customer.created_by_id = @user.id
      unless customer.save
        add_error("Dòng #{i}", customer.errors.full_messages.join(", "))
      end
    end
  end

  def add_error info, message
    @import_success = false
    @ws.add_row [info, message]
  end
end

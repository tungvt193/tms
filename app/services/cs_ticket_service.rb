require "axlsx"

class CsTicketService
  def initialize(state, file_name)
    @state = state
    @file_name = file_name
  end

  def export
    p = Axlsx::Package.new
    wb = p.workbook
    header = wb.styles.add_style({ :border => { :style => :thin, :color => "000000" }, bg_color: "56C5D0", :fg_color => "FF", :b => true })
    border = wb.styles.add_style({ :border => { :style => :thin, :color => "000000" } })
    wb.add_worksheet(:name => "Phiếu CSKH") do |sheet|
      fields = ["Trạng Thái", "Nhu cầu khách hàng", "Tên giao dịch", "Khách hàng", "Số điện thoại", "Dự án", "NVKD", "ID Phiếu",
                "Ý kiến đánh giá của khách hàng", "Tốc độ xử lý giao dịch", "Chất lượng tư vấn của NVKD", "Sao dịch vụ cho NVKD",
                "Đánh giá giao dịch(1-10)", "Nhân viên CSKH", "Ngày tạo case"] if @state == "deposit"
      fields = ["Trạng Thái", "Nhu cầu khách hàng", "Tên giao dịch", "Khách hàng", "Số điện thoại", "Dự án", "NVKD", "ID Phiếu", "Lý do huỷ",
                "Nhu cầu của KH với giao dịch", "Chất lượng tư vấn của NVKD", "Hỗ trợ thêm",
                "Voucher tặng khách hàng", "Ghi chú", "Nhân viên CSKH", "Ngày tạo case"] if @state == "canceled"
      fields = ["Trạng Thái", "Nhu cầu khách hàng", "Tên giao dịch", "Khách hàng", "Số điện thoại", "Dự án", "NVKD", "ID Phiếu",
                "Ý kiến đánh giá của khách hàng", "Tốc độ xử lý giao dịch", "Chất lượng tư vấn của NVKD", "Sao dịch vụ cho NVKD",
                "Đánh giá giao dịch(1-10)", "Voucher tặng khách hàng", "Nhân viên CSKH", "Ngày tạo case"] if @state == "contract_signed"
      types = Array.new(fields.length, :string)

      sheet.add_row fields, style: header
      cs_tickets = CsTicket.includes(deal: [:project, :customer, :assignee]).where(deal_state: @state).order('deals.name asc')
      deal_ids = []
      cs_tickets.each do |ticket|
        deal = ticket.deal
        deal_ids.push(deal.id)
        if @state == "deposit"
          sheet.add_row deposit_params(deal, ticket), style: border, types: types
        end
        if @state == "canceled"
          sheet.add_row cancel_params(deal, ticket), style: border, types: types
        end
        if @state == "contract_signed"
          sheet.add_row contract_signed_params(deal, ticket), style: border, types: types
        end
      end
      deals = Deal.includes(:project, :customer, :cs_tickets, :assignee).where.not(id: deal_ids).where(state: @state).order(name: :asc)
      deals.each do |deal|
        if @state == "deposit"
          sheet.add_row deposit_params(deal, nil), style: border, types: types
        end
        if @state == "canceled"
          sheet.add_row cancel_params(deal, nil), style: border, types: types
        end
        if @state == "contract_signed"
          sheet.add_row contract_signed_params(deal, nil), style: border, types: types
        end
      end
    end
    tempfile = File.new("#{Rails.root}/tmp/#{@file_name}.xlsx", "w")
    p.serialize tempfile
  end

  def deposit_params(deal, ticket)
    [
      if ticket.present?
        'Có phiếu'
      elsif deal.cs_state == 1
        'Chưa có phiếu'
      elsif deal.cs_state == 2
        'Có phiếu'
      else
        'Từ chối đánh giá'
      end,
      ticket.present? && ticket.customer_opinion.present? ? (ticket.customer_opinion == 1 ? "Đồng ý đánh giá" : "Từ chối đánh giá") : '',
      deal.name,
      deal.customer&.name,
      "#{Phonelib.parse(deal&.customer&.phone_number, deal&.customer&.country_code).national&.delete(' ')}",
      deal.project&.name,
      deal.assignee&.full_name,
      ticket&.id,
      ticket&.support_level,
      ticket&.processing_speed.present? ? Constant::PROCESSING_SPEED[ticket.processing_speed] : '',
      ticket&.sale_consulting_quality.present? ? Constant::SALE_CONSULTING_QUALITY[ticket.sale_consulting_quality] : '',
      ticket&.sale_rating,
      ticket.nil? || ticket&.customer_opinion == 2 ? '' : ticket&.deal_rating,
      ticket&.author&.full_name,
      ticket&.created_at&.strftime("%d/%m/%Y")
    ]
  end

  def cancel_params(deal, ticket)
    [
      if ticket.present?
        'Có phiếu'
      elsif deal.cs_state == 1
        'Chưa có phiếu'
      elsif deal.cs_state == 2
        'Có phiếu'
      else
        'Từ chối đánh giá'
      end,
      ticket.present? && ticket.customer_opinion.present? ? (ticket.customer_opinion == 1 ? "Đồng ý đánh giá" : "Từ chối đánh giá") : '',
      deal.name,
      deal.customer&.name,
      "#{Phonelib.parse(deal&.customer&.phone_number, deal&.customer&.country_code).national&.delete(' ')}",
      deal.project&.name,
      deal.assignee&.full_name,
      ticket&.id,
      ticket&.cancel_reason.present? ? Constant::CANCELED_REASON[ticket.cancel_reason] : '',
      ticket&.demand_with_deal.present? ? Constant::DEMAND_WITH_DEAL[ticket.demand_with_deal] : '',
      ticket&.sale_consulting_quality.present? ? Constant::SALE_CONSULTING_QUALITY[ticket.sale_consulting_quality] : '',
      ticket&.extra_support_for_customer,
      ticket&.voucher&.content,
      ticket&.note,
      ticket&.author&.full_name,
      ticket&.created_at&.strftime("%d/%m/%Y")
    ]
  end

  def contract_signed_params(deal, ticket)
    [
      if ticket.present?
        'Có phiếu'
      elsif deal.cs_state == 1
        'Chưa có phiếu'
      elsif deal.cs_state == 2
        'Có phiếu'
      else
        'Từ chối đánh giá'
      end,
      ticket.present? && ticket.customer_opinion.present? ? (ticket.customer_opinion == 1 ? "Đồng ý đánh giá" : "Từ chối đánh giá") : '',
      deal.name,
      deal.customer&.name,
      "#{Phonelib.parse(deal&.customer&.phone_number, deal&.customer&.country_code).national&.delete(' ')}",
      deal.project&.name,
      deal.assignee&.full_name,
      ticket&.id,
      ticket&.support_level,
      ticket&.processing_speed.present? ? Constant::PROCESSING_SPEED[ticket.processing_speed] : '',
      ticket&.sale_consulting_quality.present? ? Constant::SALE_CONSULTING_QUALITY[ticket.sale_consulting_quality] : '',
      ticket&.sale_rating,
      ticket.nil? || ticket&.customer_opinion == 2 ? '' : ticket&.deal_rating,
      ticket&.voucher&.content,
      ticket&.author&.full_name,
      ticket&.created_at&.strftime("%d/%m/%Y")
    ]
  end
end

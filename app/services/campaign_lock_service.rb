require "axlsx"

class CampaignLockService
  def initialize(file_path = nil, project_id = nil)
    @file_path = file_path
    @project = Project.find_by id: project_id
    @high_columns = Constant::HIGH_COLUMNS
    @low_columns = Constant::LOW_COLUMNS
  end

  def export_template
    p = Axlsx::Package.new
    wb = p.workbook
    @header = wb.styles.add_style bg_color: '3ac5c9', fg_color: 'FF', b: true
    add_worksheet(wb, 'Cao tầng', @high_columns)
    add_worksheet(wb, 'Thấp tầng', @low_columns)
    p
  end

  def import_products
    @codes_duplicate = []
    @codes_invalid_state = []
    @file_valid = true
    @high_products = []
    @low_products = []

    ods = open_spreadsheet(@file_path)
    ods.each_with_pagename do |nameSheet, spreadsheet|
      validate_data(nameSheet, spreadsheet)
    rescue => e
      @file_valid = false
      return @file_valid, get_error_message, [], []
    end

    @file_valid = false if @high_products.empty? && @low_products.empty?
    return @file_valid, get_error_message, @high_products, @low_products
  end

  private

  def open_spreadsheet(file_path)
    case File.extname(file_path)
    when '.csv' then
      Roo::CSV.new(file_path)
    when '.xls' then
      Roo::Excel.new(file_path)
    when '.xlsx' then
      Roo::Excelx.new(file_path)
    else
      raise "Unknown file type: #{File.basename(file_path, ".*")}"
    end
  end

  def validate_data(nameSheet, spreadsheet)
    level = Reti::Product::LEVELS.invert[nameSheet]
    campaign_lock_product_ids = CampaignLock.where(project_id: @project.id).pluck(:products).flatten
    sheet_product_codes = (2..spreadsheet.last_row).map { |index| spreadsheet.cell(index,2).strip }.compact
    sheet_products = Product.where(project_id: @project.id, code: sheet_product_codes, level: level)

    raise 'data_invalid' if sheet_product_codes.size != sheet_products.size

    sheet_products.each  do |product|
      # validate sản phẩm đang có state: Đã bán, Đã đặt cọc, Đang lock
      @codes_invalid_state << product.code if %i(locking deposited sold).include?(product.state.to_sym)

      # validate trùng sản phẩm với campaign khác
      @codes_duplicate << product.code  if campaign_lock_product_ids.include?(product.id)
    end

    raise 'data_invalid' unless @codes_invalid_state.empty?
    raise 'data_invalid' unless @codes_duplicate.empty?

    # validate data từng dòng
    @columns = []
    @code_index = 0
    @column_settings = level == 0 ? @high_columns : @low_columns
    # Không check 'Nhãn sản phẩm'

    header = spreadsheet.row(1).compact
    header.each_with_index do |field, index|
      column_setting = @column_settings.detect { |cs| cs[:title] == field.strip }
      unless column_setting.present?
        raise 'data_invalid'
      end
      
      @columns[index] = column_setting
      @code_index = index if column_setting[:field] == :code
    end

    (2..spreadsheet.last_row).each do |i|
      row = spreadsheet.row(i)
      next if row.compact.empty?

      product = sheet_products.find_by!(code: cell_value(row[@code_index], @columns[@code_index]))
      product_import = {}
      row[0...@column_settings.size].each_with_index do |cell, index|
        # không check field nhãn sản phẩm
        next if index.zero?

        column = @columns[index]
        value = cell_value(cell, column)
        if column[:type].to_s == 'dropdown' && product[column[:field]].to_s != value.to_s && product[column[:field]].to_s != cell.to_s
          raise 'data_invalid'
        elsif column[:type].to_s == 'numeric'
          if %i(price sum_price).include?(column[:field])
            cell = cell.to_i
            raise 'data_invalid' if product[column[:field]].to_i != value.to_i
          elsif product[column[:field]].to_f != value.to_f
            raise 'data_invalid' 
          end
        elsif column[:type].to_s == 'text'
          if %i(subdivision_id block_id floor_id).include?(column[:field])
            raise 'data_invalid' if Division.find(product[column[:field]]).name != value.to_s
          else
            raise 'data_invalid' if product[column[:field]].to_s != value.to_s
          end
        end

        product_import[column[:field]] = cell.present? ? cell : ''
      end
      # add nhãn sản phẩm
      product_import[:tag] = row[0]
      product_import[:id] = product.id

      if level == 0
        @high_products << product_import
      else
        @low_products << product_import
      end
    end
  rescue => e
    puts e.backtrace.join("\n")
    raise 'data_invalid'
  end

  def cell_value(cell, column)
    case column[:type].to_s
    when 'text'
      cell = cell.to_i if column[:field] == :name && cell.kind_of?(Float)
      cell = cell.to_s
    when 'dropdown'
      cell = column[:value].invert[cell]
    when 'numeric'
      cell = cell.to_f > 0 ? cell.to_f : nil
    end
    cell.kind_of?(String) ? cell.strip.gsub('_x0008_', '').gsub(/[^[:print:]]/i, '') : cell
  end

  def get_error_message
    if @codes_duplicate.present?
      "Mã sản phẩm <span>#{@codes_duplicate.join(', ')}</span> đang bị trùng. Bạn vui lòng kiểm tra lại! "
    elsif  @codes_invalid_state.present?
      "Mã sản phẩm <span>#{@codes_invalid_state.join(', ')}</span> bị trùng với các căn ở trạng thái Đã bán, Đã đặt cọc hoặc Đang lock. Bạn vui lòng kiểm tra lại!"
    elsif @file_valid == false
      "Thông tin tải lên chưa hợp lệ. Bạn vui lòng kiểm tra lại!"
    end
  end

  def columnName(num)
    return 0 if num <= 0
    while num > 0 do
      mod = (num - 1) % 26
      name = (65 + mod).chr + (name || '')
      num = ((num - mod) / 26).to_i
    end
    name
  end

  def add_worksheet(wb, name, columns)
    header = columns.map { |c| c[:title] }
    wb.add_worksheet(name: name) do |ws|
      ws.add_row header, style: @header
      columns.each_with_index do |col, index|
        col_name = columnName(index + 1)
        case col[:type]
        when 'text'
          ws.add_data_validation("#{col_name}2:#{col_name}1000", {
            type: :textLength,
            operator: :between,
            formula1: "#{col[:value].first}",
            formula2: "#{col[:value].last}",
            showErrorMessage: true,
            errorTitle: col[:title],
            error: "Phải ít hơn #{col[:value].last} ký tự",
            errorStyle: :stop,
            showInputMessage: true,
            promptTitle: '',
            prompt: "Bắt buộc nhập, không vượt quá #{col[:value].last} ký tự" })
        when 'dropdown'
          ws.add_data_validation("#{col_name}2:#{col_name}1000", {
            type: :list,
            formula1: '"' + col[:value]&.values&.join(', ') + '"',
            showDropDown: false,
            showErrorMessage: true,
            errorTitle: col[:title],
            error: 'Chỉ được chọn các giá trị trong dropdown',
            errorStyle: :stop,
            showInputMessage: true,
            promptTitle: '',
            prompt: 'Chọn các giá trị trong dropdown' })
        when 'numeric'
          ws.add_data_validation("#{col_name}2:#{col_name}1000", {
            type: :decimal,
            operator: :greaterThanOrEqual,
            formula1: "#{col[:value].first}",
            showErrorMessage: true,
            errorTitle: col[:title],
            error: "Phải nhập số lớn hơn hoặc bằng #{col[:value].first}",
            errorStyle: :stop,
            showInputMessage: true,
            promptTitle: '',
            prompt: "Nhập số lớn hơn hoặc bằng #{col[:value].first}" })
        end
      end
    end
  end
end

require "axlsx"

class UserService
  def initialize(user = nil, users = nil)
    @user = user
    @users = users
  end

  def calculate_income
    @commission_deals = Commission.joins(:deal).where(deals: { state: %w(contract_signed completed) } )
    case true
    when @user.sale_member?
      sale_member_income
    when @user.sale_leader?
      sale_leader_income
    when @user.sale_manager?
      sale_manager_income
    when @user.sale_admin?
      sale_admin_income
    else
      0
    end
  end

  def export
    @data = []
    @users.each do |user|
      user_rows = []
      attributes.map{ |obj| obj[:field] }.each do |attribute|
        case attribute
        when :phone_number
          user_rows << user.phone_number
        when :gender
          gender = user.gender.to_s == 'male' ? 'Nam' : 'Nữ'
          user_rows << gender
        when :city_id
          user_rows << user.city&.name
        when :created_at
          user_rows << user.try(&attribute)&.strftime('%d/%m/%Y')
        when :created_by_id
          user_rows << user.creator&.name
        when :sign_in_count
          signed_in = user.try(&attribute) > 0 ? 'Đã đăng nhập' : 'Chưa đăng nhập'
          user_rows << signed_in
        when :last_sign_in_at
          user_rows << user.try(&attribute)&.strftime('%d/%m/%Y')
        when :account_type
          user_rows << Constant::ACCOUNT_TYPE.invert.key(user.try(&attribute))
        when :role_id
          user_rows << user.role.name
        when :main_job
          user_rows << Constant::USER_MAIN_JOB.invert.key(user.try(&attribute))
        else
          user_rows << user.try(&attribute)
        end
      rescue => e
        puts e.backtrace.join("\n")
        user_rows << nil
        next
      end
      @data << user_rows
    end

    file = Axlsx::Package.new
    wb = file.workbook
    wb.use_autowidth = true
    ws = wb.add_worksheet(name: 'Tài khoản')
    style_header = wb.styles.add_style(bg_color: '56C5D0', fg_color: 'FF', b: true)
    style_body = wb.styles.add_style(:alignment => {vertical: :top , wrap_text: true})
    heads = attributes.map{ |obj| obj[:title] }
    ws.add_row(heads, style: style_header)
    @data.each do |data|
      ws.add_row(data, style: style_body, height: 16)
      ws.column_widths *([20]*ws.column_info.count)
    end
    file
  end

  private

  def sale_member_income
    @commission_deals.where(assignee_id: @user.id).sum(:assignee_comm).to_i
  end

  def sale_leader_income
    @commission_deals.where(assignee_id: @user.id).sum(:assignee_comm).to_i + @commission_deals.where(leader_id: @user.id).sum(:leader_comm).to_i
  end

  def sale_manager_income
    @commission_deals.where(assignee_id: @user.id).sum(:assignee_comm).to_i + @commission_deals.where(manager_id: @user.id).sum(:manager_comm).to_i
  end

  def sale_admin_income
    @commission_deals.where(sale_admin_id: @user.id).sum(:admin_comm).to_i
  end

  def attributes
    [
      {
        title: I18n.t("user.template.full_name"),
        field: :full_name
      },
      {
        title: I18n.t("user.template.email"),
        field: :email
      },
      {
        title: I18n.t("user.template.phone_number"),
        field: :phone_number
      },
      {
        title: I18n.t("user.template.gender"),
        field: :gender
      },
      {
        title: I18n.t("user.template.city"),
        field: :city_id
      },
      {
        title: I18n.t("user.template.created_at"),
        field: :created_at
      },
      {
        title: I18n.t("user.template.created_by_id"),
        field: :created_by_id
      },
      {
        title: I18n.t("user.template.signed_in"),
        field: :sign_in_count
      },
      {
        title: I18n.t("user.template.last_sign_in_at"),
        field: :last_sign_in_at
      },
      {
        title: I18n.t("user.template.account_type"),
        field: :account_type
      },
      {
        title: I18n.t("user.template.role"),
        field: :role_id
      },
      {
        title: I18n.t("user.template.status"),
        field: :status
      },
      {
        title: I18n.t("user.template.main_job"),
        field: :main_job
      }
    ]
  end
end

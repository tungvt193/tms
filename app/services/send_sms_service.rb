require 'rest-client'

class SendSmsService
  def initialize(otp)
    @otp = otp
  end

  def send_sms
    payload = {
      "username": ENV['SMS_USERNAME'],
      "password": ENV['SMS_PASSWORD'],
      "brandname": ENV['SMS_BRANDNAME'],
      "textmsg": "RETI.VN: Ma xac thuc OTP cua quy khach tren Retizy App la #{@otp.otp}. Vui long khong cung cap ma nay cho bat ky ai.",
      "sendtime": Time.current.strftime("%Y%m%d%H%M%S"),
      "isunicode": 0,
      "listmsisdn": @otp.phone_number
    }
    response = RestClient.post(ENV['SMS_API_URL'], payload.to_json, content_type: :json)

    JSON.parse(response.body)["code"]
  end
end

class NotificationService
  cattr_accessor :routes
  self.routes = Rails.application.routes.url_helpers

  class << self
    # Notify to old assignee
    def notify_change_assignee(deal, old_assignee)
      title = "Bạn được phân công chăm sóc giao dịch mới!"
      message = "Bạn được phân công chăm sóc GD <strong>#{deal&.name}</strong> bởi <strong>#{User.current.full_name}</strong>. Xem ngay!"
      Notification.create(user_id: deal.assignee_id, title: title, content: message, related_url: routes.edit_admin_deal_path(deal), deal_id: deal.id, notification_type: 0)
      if deal.author&.agent? && deal.assignee&.internal?
        deal_assigned_from_share_customer(deal, deal.author)
      end
      update_state_to_request_recall(deal, old_assignee)
    end

    # Notification to assignee when deal came from Callio
    def deal_from_callio(deal)
      title = "Bạn được phân công chăm sóc giao dịch mới!"
      message = "Bạn được phân công chăm sóc giao dịch #{deal.name} bởi Telesale. Xem ngay!"
      Notification.create(user_id: deal.assignee_id, title: title, content: message, related_url: routes.edit_admin_deal_path(deal), deal_id: deal.id, notification_type: 0)
    end

    # Notify change deal state to recall or change assignee
    def update_state_to_request_recall(deal, old_assignee)
      message = "Giao dịch <strong>#{deal&.name}</strong> đã bị thu hồi bởi <strong>#{User.current.full_name}</strong>."
      if old_assignee && User.current != old_assignee && old_assignee.sale_member?
        Notification.create(user_id: old_assignee.id, title: "Giao dịch bị thu hồi", content: message, related_url: nil, deal_id: deal.id, notification_type: 0)
      end
    end

    # CĐT Thu hồi or Sàn khác giữ or Đã bán bởi sàn khác
    def product_recall_third_holding_third_sold(product)
      product.deals.each do |deal|
        next unless deal.assignee || (product.state == 'sold' && %w(contract_signed deposit).include?(deal.state))
        title = "Trạng thái sản phẩm vừa được cập nhật"
        notify_message = "Sản phẩm <strong>#{product&.code}</strong> thuộc giao dịch <strong>#{deal&.name}</strong> được sale admin cập nhật trạng thái #{I18n.t("product.states.#{product.state}")}. Vui lòng tư vấn sản phẩm khác cho Khách hàng."
        Notification.create(user_id: deal.assignee_id, title: title, content: notify_message, related_url: routes.edit_admin_deal_path(deal), deal_id: deal.id, notification_type: 0)
        remind_message = "Vui lòng nhập Mã sản phẩm mới cho giao dịch <strong>#{deal&.name}</strong>"
        Reminder.create(user_id: deal.assignee_id, content: remind_message, related_url: routes.edit_admin_deal_path(deal), deal_id: deal.id)
      end
    end

    # Sàn khác giữ/Thu hồi/Sàn khác bán/Đã bán -> for_sale
    def product_change_for_sale(product)
      title = "Mở bán sản phẩm mới"
      message = "Sản phẩm <strong>#{product&.code}</strong>, dự án <strong>#{product.project.name}</strong> đã mở bán, bạn có thể tư vấn khách hàng."
      subdivisions = product.project.divisions.select(:id, :name).to_json
      url = routes.edit_admin_project_product_path(product.project, product, params: { subdivisions: subdivisions })
      User.all.each do |user|
        Notification.create(user_id: user.id, title: title, content: message, related_url: url, notification_type: 1)
      end
    end

    # Tạo sản phẩm mới và import bảng hàng
    def create_product(project)
      title = "Bảng hàng mới vừa được cập nhật!"
      message = "Dự án <strong>#{project.name}</strong> đã có bảng hàng mới. Xem ngay!"
      subdivisions = product.project.divisions.select(:id, :name).to_json
      url = routes.show_products_admin_project_path(project, params: { subdivisions: subdivisions })
      User.all.each do |user|
        Notification.find_or_create_by(user_id: user.id, title: title, content: message, related_url: url, notification_type: 1, send_now: false)
      end
    end

    # Cập nhật hoa hồng
    def update_commission(deal, commission)
      if commission.previous_changes[:temporary_commission].present? && commission.previous_changes[:temporary_commission][1].present? && !commission.previous_changes[:actually_commission].present? && !commission.actually_commission.present?
        title = "Hoa hồng tạm tính đã cập nhật!!!"
        message = "Giao dịch <strong>#{deal.name}</strong> vừa được cập nhật hoa hồng tạm tính. Xem ngay!"
      elsif commission.previous_changes[:actually_commission].present? && commission.previous_changes[:actually_commission][1].present?
        title = "Ting ting! Hoa hồng thực tế đã cập nhật!"
        message = "Giao dịch <strong>#{deal.name}</strong> đã có hoa hồng thực tế. Xem ngay!"
      end
      if title && message
        Notification.create(user_id: deal.assignee_id, title: title, content: message, related_url: routes.edit_admin_deal_path(deal), deal_id: deal.id, notification_type: 0)
      end
    end

    # Cập nhật tài liệu bán hàng
    def update_sale_document(project)
      title = "Thư viện tài liệu dự án có cập nhật mới!"
      message = "Thư viện tài liệu của dự án <strong>#{project.name}</strong> vừa được cập nhật. Xem ngay!"
      url = routes.admin_project_path(project)
      User.all.each do |user|
        Notification.find_or_create_by(user_id: user.id, title: title, content: message, related_url: url, notification_type: 5, send_now: false)
      end
    end

    # Sale inhouse nhận được lead giới thiệu từ CTV
    def deal_from_share_customer(deal, user)
      title = "Cơ hội bán hàng mới!"
      message = "Bạn vừa được giới thiệu khách hàng mới cho dự án <strong>#{deal.project.name}</strong> bởi CTV <strong>#{user.full_name}</strong>. Xem ngay!"
      Notification.create(user_id: deal.assignee_id, title: title, content: message, related_url: routes.edit_admin_deal_path(deal), deal_id: deal.id, notification_type: 2)
    end

    # CTV giới thiệu KH mới, assign ngẫu nhiên
    def assign_deal_from_share_customer(deal, agent)
      title = "Có khách hàng mới từ nguồn CTV!"
      message = "CTV <strong>#{agent.full_name}</strong> vừa giới thiệu khách hàng mới cho dự án <strong>#{deal.project.name}</strong>. Hãy phân công người chăm sóc ngay nhé!"
      User.joins(:role).where(roles: { name: ['CSKH'] }).each do |user|
        Notification.create(user_id: user.id, title: title, content: message, related_url: routes.edit_admin_deal_path(deal), deal_id: deal.id, notification_type: 2)
      end
    end

    # Khách hàng chia sẻ được cập nhật trạng thái giao dịch mới
    def update_state_deal_from_share_customer(deal)
      if deal.author&.agent? && deal.assignee&.internal?
        title = "Cập nhật trạng thái mới cho khách hàng chia sẻ"
        message = "Khách hàng <strong>#{deal.customer.name}</strong> bạn giới thiệu được chuyển sang trạng thái <strong>#{I18n.t("deal.states.#{deal.state}")}</strong>. Xem ngay!"
        Notification.create(user_id: deal.created_by_id, title: title, content: message, related_url: routes.edit_admin_deal_path(deal), deal_id: deal.id, notification_type: 2)
      end
    end

    # KH giới thiệu assign ngẫu nhiên đã được RETI phân công người chăm sóc
    def deal_assigned_from_share_customer(deal, user)
      title = "Đã phân công RETIAN chăm sóc khách giới thiệu"
      message = "Khách hàng <strong>#{deal.customer.name}</strong> bạn giới thiệu được chuyển cho RETIAN <strong>#{deal.assignee.full_name}</strong> chăm sóc. Xem ngay!"
      Notification.create(user_id: user.id, title: title, content: message, related_url: routes.edit_admin_deal_path(deal), deal_id: deal.id, notification_type: 2)
    end

    # Thông báo chúc mừng sinh nhật khách hàng cho mỗi sale
    def remind_hpbd_customer_for_sale(customer, assignee, deal)
      title = "Sinh nhật khách hàng!"
      message = "Sắp đến sinh nhật của khách hàng <strong>#{customer.name}</strong>. Đừng quên gửi SMS/Email chúc mừng khách hàng nhé! Xem ngay"
      Notification.create(user_id: assignee.id, title: title, content: message, related_url: routes.edit_admin_deal_path(deal, params: { form: "customer" }), deal_id: deal.id, notification_type: 0)
    end

    # Thông báo chúc mừng sinh nhật tất cả khách hàng cho cskh
    def remind_hpbd_customer_for_cskh(customer, deal)
      title = "Sinh nhật khách hàng!"
      message = "Sắp đến sinh nhật của khách hàng <strong>#{customer.name}</strong>. Đừng quên gửi SMS/Email chúc mừng khách hàng nhé! Xem ngay"
      User.joins(:role).where(roles: { name: ['CSKH'] }).each do |user|
        Notification.create(user_id: user.id, title: title, content: message, related_url: routes.edit_admin_deal_path(deal, params: { form: "customer" }), deal_id: deal.id, notification_type: 0)
      end
    end

    def notify_stop_selling_project(project)
      title = "Dự án ngừng bán!"
      message = "Dự án #{project.name} đã ngừng bán. Vui lòng tư vấn dự án khác cho khách hàng!"
      User.all.each do |user|
        Notification.create(user_id: user.id, title: title, content: message, related_url: routes.admin_project_path(project), notification_type: 3)
      end
    end

    # Thông báo đối với user đã tạo giao dịch nhưng sau 24h chưa tạo lịch hẹn
    def create_appointment(deal)
      title = "Lên lịch hẹn ngay thôi!"
      message = "Bạn có muốn tạo lịch hẹn với khách hàng <strong>#{deal.customer.name}</strong> của giao dịch <strong>#{deal.name}</strong> không?"
      Notification.create(user_id: deal.assignee_id, title: title, content: message, related_url: routes.edit_admin_deal_path(deal, params: { form: "appointment" }), deal_id: deal.id, notification_type: 0)
    end

    # Thông báo cho user trước 1h diễn ra cuộc hẹn
    def remind_appointment_date(appointment, deal)
      title = "Lịch hẹn đang chờ bạn, chuẩn bị đi thôi!"
      message = "Sắp tới bạn có cuộc gặp với khách hàng <strong>#{deal.customer.name}</strong> lúc <strong>#{appointment.appointment_date.strftime('%d/%m/%Y - %H:%M')}</strong>. Đừng để khách phải chờ lâu và hãy đem về cho mình kết quả thật tốt nhé!"
      Notification.create(user_id: deal.assignee_id, title: title, content: message, related_url: routes.edit_admin_deal_path(deal, params: { form: "appointment" }), deal_id: deal.id, notification_type: 0)
    end

    # Thông báo sau 1 ngày kể từ khi bắt đầu lịch hẹn. Nếu user ko có cập nhật gì thêm kể từ lúc tạo lịch hẹn.
    def update_result_appointment(appointment, deal)
      title = "Cùng nhập kết quả ngay thôi!"
      message = "Bạn chưa nhập kết quả của lịch hẹn lúc <strong>#{appointment.appointment_date.strftime('%d/%m/%Y - %H:%M')}</strong>. Vui lòng hoàn tất quá trình! Xem ngay!"
      Notification.create(user_id: deal.assignee_id, title: title, content: message, related_url: routes.edit_admin_deal_path(deal, params: { form: "appointment" }), deal_id: deal.id, notification_type: 0)
    end

    # Dự án mới được tạo trên TMS
    def create_project(project)
      title = "Cập nhật dự án mới"
      message = "RETI đã mở bán dự án mới: <strong>#{project.name}</strong>. Xem ngay!"
      User.all.each do |user|
        Notification.create(user_id: user.id, title: title, content: message, related_url: routes.admin_project_path(project), notification_type: 3)
      end
    end

    # Khi có chiến dịch lock căn mới được tạo
    def create_campaign_lock(campaign_lock)
      title = "Lịch lock căn mới!"
      message = "Bảng hàng dự án <strong>#{campaign_lock.project.name}</strong> sẽ được mở lock vào <strong>#{campaign_lock.session_opening_time.strftime('%d/%m/%Y - %H:%M')}</strong>. Xem ngay!"
      subdivisions = campaign_lock.project.divisions.select(:id, :name).to_json
      url = routes.admin_project_products_path(campaign_lock.project, params: { subdivisions: subdivisions })
      User.all.each do |user|
        Notification.create(user_id: user.id, title: title, content: message, related_url: url, notification_type: 6)
      end
    end

    # 15ph trước khi chiến dịch lock mở
    def before_campaign_lock_open(campaign_lock)
      title = "Sắp tới giờ lock căn!"
      message = "Chỉ 15 phút nữa dự án <strong>#{campaign_lock.project.name}</strong> sẽ mở lock. Đừng bỏ lỡ cơ hội bán hàng nhé!"
      subdivisions = campaign_lock.project.divisions.select(:id, :name).to_json
      url = routes.admin_project_products_path(campaign_lock.project, params: { subdivisions: subdivisions })
      User.all.each do |user|
        Notification.create(user_id: user.id, title: title, content: message, related_url: url, notification_type: 6)
      end
    end

    # Đến giờ mở phiên lock
    def campaign_lock_open(campaign_lock)
      title = "Bắt đầu mở phiên lock"
      message = "Phiên lock của dự án <strong>#{campaign_lock.project.name}</strong> bắt đầu mở. Lock căn ngay!"
      subdivisions = campaign_lock.project.divisions.select(:id, :name).to_json
      url = routes.admin_project_products_path(campaign_lock.project, params: { subdivisions: subdivisions })
      User.all.each do |user|
        Notification.create(user_id: user.id, title: title, content: message, related_url: url, notification_type: 6)
      end
    end

    def sale_admin_accept_unc(product_lock_history)
      product_code = product_lock_history.product.code
      project_name = product_lock_history.project.name
      locking_title = 'Xác nhận cọc thành công!'
      locking_message = "Chúc mừng bạn! SP #{product_code} - #{project_name} đã được xác nhận cọc! Xem và hoàn tất thông tin ghi nhận giao dịch ngay!"
      enqueued_title = 'Sản phẩm bạn quan tâm đã bán!'
      enqueued_message = "SP #{product_code} - #{project_name} đã được NVKD ##{product_lock_history.user_id} chốt thành công. Vui lòng tư vấn SP khác cho khách hàng!"
      Notification.create(user_id: product_lock_history.user_id, title: locking_title, content: locking_message, related_url: routes.edit_admin_product_lock_history_path(product_lock_history), notification_type: 4)
      ProductLockHistory.enqueued.where(product_id: product_lock_history.product_id).each do |history|
        history.update_columns(state: :failed, failure_reason: 'Lock trượt')
        Notification.create(user_id: history.user_id, title: enqueued_title, content: enqueued_message, related_url: routes.edit_admin_product_lock_history_path(history), notification_type: 4)
      end
    end

    def sale_admin_reject_unc(product_lock_history)
      product_code = product_lock_history.product.code
      project_name = product_lock_history.project.name
      locking_title = 'Xác nhận cọc không thành công!'
      locking_message = "RETI từ chối yêu cầu xác nhận cọc SP #{product_code} do chưa đủ điều kiện. Xem chi tiết ngay!"
      enqueued_title = 'Sản phẩm bạn quan tâm đã mở lock!'
      enqueued_message = "SP #{product_code} - #{project_name} đã được mở lock! Truy cập lock căn ngay"
      Notification.create(user_id: product_lock_history.user_id, title: locking_title, content: locking_message, related_url: routes.edit_admin_product_lock_history_path(product_lock_history), notification_type: 4)
      ProductLockHistory.enqueued.where(product_id: product_lock_history.product_id).each do |history|
        Notification.create(user_id: history.user_id, title: enqueued_title, content: enqueued_message, related_url: routes.edit_admin_product_lock_history_path(history), notification_type: 4)
      end
    end

    def sale_admin_pending_unc(product_lock_history)
      product_code = product_lock_history.product.code
      locking_title = 'Gia hạn thời gian xác nhận cọc!'
      locking_message = "Yêu cầu xác nhận cọc SP #{product_code} được RETI tạm treo để có thêm thời gian xác nhận. Xem chi tiết ngay!"
      Notification.create(user_id: product_lock_history.user_id, title: locking_title, content: locking_message, related_url: routes.edit_admin_product_lock_history_path(product_lock_history), notification_type: 4)
    end

    # Gửi notification cho sale khi căn mà sale quan tâm còn 5 phút nữa mở trở lại
    def remind_product_for_sale(product, lock, user)
      title = "Căn bạn quan tâm sắp mở lock"
      message = "SP <strong>#{product.code}</strong> - <strong>#{product.project.name}</strong> có thể mở lock trong 5 phút nữa. Đừng bỏ lỡ cơ hội lock căn này nhé!"
      Notification.create(user_id: user.id, title: title, content: message, related_url: routes.edit_admin_product_lock_history_path(lock.id), notification_type: 4)
    end

    def lock_out_of_time(product_lock_history)
      product_code = product_lock_history.product.code
      locking_title = 'Hết hạn lock căn!'
      locking_message = "Rất tiếc, bạn đã hết thời gian xác nhận cọc SP #{product_code}. Xem chi tiết"
      Notification.create(user_id: product_lock_history.user_id, title: locking_title, content: locking_message, related_url: routes.edit_admin_product_lock_history_path(product_lock_history.id), notification_type: 4)
    end

    def warning_lock_timeout(product_lock_history)
      product_code = product_lock_history.product.code
      locking_title = 'Thời gian lock căn sắp hết!'
      locking_message = "Còn 10 phút để xác nhận cọc SP #{product_code}. Gửi UNC ngay!"
      Notification.create(user_id: product_lock_history.user_id, title: locking_title, content: locking_message, related_url: routes.edit_admin_product_lock_history_path(product_lock_history.id), notification_type: 4)
    end

    def sale_upload_unc(product_lock_history)
      product = product_lock_history.product
      user = User.find(product_lock_history.user_id)
      role_sale_admin = Role.find_by(name: 'Sale Admin')
      recipients = User.where(role_id: role_sale_admin.id)
      title = 'Xác nhận UNC'
      message = "<div class='d-none notification-upload-unc' product_lock_history='#{product_lock_history.id}'></div><strong>#{user.full_name}</strong> vừa upload UNC cho sản phẩm <strong>#{product.code}</strong> của dự án <strong>#{product.project.name}</strong>. Vui lòng xác nhận yêu cầu trong 10 phút. Xác nhận ngay!"
      recipients.each do |recipient|
        Notification.create(user_id: recipient.id, title: title, content: message, related_url: routes.edit_admin_product_lock_history_path(product_lock_history.id), notification_type: 4)
      end
    end

    def product_sold(product_lock_history)
      product = product_lock_history.product
      user = User.find(product_lock_history.user_id)
      recipients = ProductLockHistory.where(product_id: product.id).reject{ |p| p.user_id == user.id }.pluck(:user_id)
      title = 'Sản phẩm bạn quan tâm đã bán!'
      message = "SP <strong>#{product.code}</strong> - <strong>#{product.project.name}</strong> đã được NVKD ##{user.id} chốt thành công. Vui lòng tư vấn SP khác cho khách hàng!"
      recipients.each do |recipient|
        product_lock_history_enqueued = ProductLockHistory.find_by(state: 'enqueued', product_id: product.id, user_id: recipient)
        product_lock_history_enqueued.update(state: 'failed')
        Notification.create(user_id: recipient, title: title, content: message, related_url: routes.edit_admin_product_lock_history_path(product_lock_history_enqueued.id), notification_type: 4)
      end
    end

    def remind_timeout_for_sale_admin(product_lock_history)
      product = product_lock_history.product
      user = User.find(product_lock_history.user_id)
      role_sale_admin = Role.find_by(name: 'Sale Admin')
      recipients = User.where(role_id: role_sale_admin.id)
      title = 'Quá hạn xác nhận'
      message = "Hết thời gian xác nhận bằng chứng lock của <strong>#{user.full_name}</strong> cho sản phẩm <strong>#{product.code}</strong> của dự án <strong>#{product.project.name}</strong>!"
      recipients.each do |recipient|
        Notification.create(user_id: recipient, title: title, content: message, related_url: routes.admin_product_lock_histories_path, notification_type: 4)
      end
    end
  end
end

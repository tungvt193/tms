class PushNotifier

  def self.send_push(notification)
    user = notification.user
    if user && user.endpoint.present?
      message = {
        body: ActionView::Base.full_sanitizer.sanitize(notification.content),
        url: notification.related_url,
      }
      Webpush.payload_send(endpoint: user.endpoint,
                           message: JSON.generate(message),
                           p256dh: user.p256dh_key,
                           auth: user.auth_key,
                           ttl: 24 * 60 * 60,
                           vapid: {
                             public_key: ENV['VAPID_PUBLIC_KEY'],
                             private_key: ENV['VAPID_PRIVATE_KEY']
                           }
      )
    end
  end
end
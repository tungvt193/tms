require "axlsx"

class DealService

  def initialize deals, attributes
    @attributes = attributes
    @deals = deals
    @header = attributes.map { |attr| I18n.t("export_deals_csv.#{attr}") }
  end

  def export
    @data = []
    @deals.each do |deal|
      deals = []
      @attributes.each do |attribute|
        case attribute
        when 'state'
          deals << I18n.t("deal.states.#{deal.state}")
        when 'project', 'customer'
          deals << deal&.public_send(attribute)&.name
        when 'phone_number'
          deals << deal&.customer&.phone_number
        when 'assignee'
          deals << deal&.assignee&.full_name
        when 'product'
          deals << deal&.product&.code
        when 'source'
          deals << (deal.source.present? ? Constant::DEAL_SOURCES.invert.key(deal.source) : '')
        when 'contact_status'
          deals << (deal.contact_status.present? ? Constant::CONTACT_STATUSES.invert.key(deal.contact_status) : '')
        when 'interaction_detail'
          deals << (deal.interaction_detail.present? ? Constant::INTERACTION_CHECKLIST.values_at(*deal.interaction_detail).join(', ') : '')
        when 'product_type'
          deals << (deal.product_type.present? ? Constant::PROJECT_REAL_ESTATE_TYPE.invert.key(deal.product_type) : '')
        when 'price_range'
          deals << (deal.price_range.present? ? Constant::DEAL_PRICE_RANGE.invert.key(deal.price_range) : '')
        when 'acreage_range'
          deals << (deal.acreage_range.present? ? Constant::DEAL_ACREAGE_RANGE.invert.key(deal.acreage_range) : '')
        when 'purchase_purpose'
          deals << (deal.purchase_purpose.present? ? Constant::PURCHASE_PURPOSE.invert.key(deal.purchase_purpose) : '')
        when 'product_selection_criteria'
          product_selection_criteria = Constant::PRODUCT_SELECTION_CRITERIA[deal.product_type].present? ? Constant::PRODUCT_SELECTION_CRITERIA[deal.product_type] : []
          deals << ((deal.product_selection_criteria.present? && product_selection_criteria.present?) ? product_selection_criteria.values_at(*deal.product_selection_criteria).join(', ') : '')
        when 'door_direction', 'balcony_direction'
          deals << (deal.public_send(attribute).present? ? Reti::Product::DIRECTIONS.values_at(*deal.public_send(attribute).map{|d| d.to_i}).join(', ') : '')
        when 'trouble_problem_list'
          deals << (deal.trouble_problem_list.present? ? Constant::PROBLEMS.values_at(*deal.trouble_problem_list).join(', ') : '')
        when 'gender'
          deals <<  (deal&.customer_persona&.gender.present? ?  Constant::CUSTOMER_GENDER.invert.key(deal&.customer_persona&.gender) : '')
        when 'city_id'
          deals << RegionData.cities.pluck(:name, :id).select{ |c| c.second == deal&.customer_persona&.city_id }[0]&.first
        when 'district_id'
          deals << (deal.customer_persona&.city_id.present? ? RegionData.where(parent_id: deal.customer_persona&.city_id).pluck(:name_with_type, :id).select{ |c| c.second == deal.customer_persona&.district_id }[0]&.first : '')
        when 'ward_id'
          deals << (deal.customer_persona&.district_id.present? ? RegionData.where(parent_id: deal.customer_persona&.district_id).pluck(:name_with_type, :id).select{ |c| c.second == deal.customer_persona&.ward_id }[0]&.first : '')
        when 'financial_capability'
          deals << (deal&.customer_persona&.financial_capability.present? ? Constant::FINANCIAL_CAPABILITY.invert.key(deal.customer_persona&.financial_capability) : '')
        when 'marital_status'
          deals << (deal&.customer_persona&.marital_status.present? ? Constant::CUSTOMER_MARITAL_STATUS.invert.key(deal.customer_perona&.marital_status) : '')
        when 'real_estate_type_to_invest'
          deals << (deal&.customer_persona&.real_estate_type_to_invest.present? ? Constant::PROJECT_REAL_ESTATE_TYPE.values_at(*deal.customer_persona&.real_estate_type_to_invest).join(', ') : '')
        when 'hobbies'
          if deal&.customer_persona&.hobbies.present?
            hobbies_constant = Constant::HOBBIES_SELECT2_DATA.map{ |o| [o[:id], o[:text]] }.select{ |o| o.first.is_a?(Integer) }
            hobbies = deal.customer_persona.hobbies.map{ |h| h.to_i }
            deals << (deal.customer_persona.hobbies.present? ? hobbies_constant.to_h.values_at(*hobbies).join(', ') : '')
          else
            ''
          end
        when 'dob'
          deals << deal&.customer_persona&.get_dob
        when 'second_phone_number', 'email', 'settlements', 'identity_card', 'domicile', 'nationality', 'workplace',
          'customer_position', 'property_ownership', 'people_in_family', 'hobbies', 'positions_to_invest'
          deals << deal&.customer_persona&.public_send(attribute)
        when 'temporary_commission', 'actually_commission'
          deals << deal&.commission&.public_send(attribute)
        else
          deals << deal.public_send(attribute)
        end
      end
      @data << deals
    end
    file = Axlsx::Package.new
    wb = file.workbook
    wb.use_autowidth = true
    ws = wb.add_worksheet(name: 'Giao dịch')
    style_header = wb.styles.add_style(bg_color: '56C5D0', fg_color: 'FF', b: true)
    style_body = wb.styles.add_style(:alignment => {vertical: :top , wrap_text: true})
    ws.add_row(@header, style: style_header)
    @data.each do |data|
      ws.add_row(data, style: style_body, height: 16)
      ws.column_widths *([20]*ws.column_info.count)
    end
    file
  end
end

require "axlsx"
class ProjectService
  def initialize file_path = nil, user = nil
    @file_path = file_path
    @user = user
  end

  def import
    @result_file = Axlsx::Package.new
    wb = @result_file.workbook
    @ws = wb.add_worksheet(name: 'Import Lỗi')
    style = wb.styles.add_style bg_color: '3ac5c9', fg_color: 'FF', b: true
    @ws.add_row ['Vị trí', 'Nội dung lỗi'], style: style
    ods = open_spreadsheet(@file_path)
    @import_success = true
    add_error('Max size', 'Giới hạn import mỗi lần là 1000 dòng') if ods.sheet(0).count > 1000
    import_data ods.sheet(0)
    # @result_file
    if @import_success
      { message: 'Dữ liệu đã được tải lên thành công' }
    else
      tempfile = File.new("#{Rails.root}/public/prject_error.xlsx", "w")
      @result_file.serialize tempfile
      UserMailer.notify_import(@user, tempfile, 'project_error.xlsx', 'Thông báo lỗi import dự án').deliver
      ::File.delete tempfile
      { message: "Có một số lỗi xảy ra khi tải dữ liệu lên. Vui lòng check email < #{@user.email} > để xem chi tiết" }
    end
  end

  private

  def open_spreadsheet(file_path)
    case File.extname(file_path)
    when '.csv' then
      Roo::CSV.new(file_path)
    when '.xls' then
      Roo::Excel.new(file_path)
    when '.xlsx' then
      Roo::Excelx.new(file_path)
    else
      raise "Unknown file type: #{File.basename(file_path, ".*")}"
    end
  end

  def import_data spreadsheet
    header = spreadsheet.row(1)
    projects = []
    (2..spreadsheet.last_row).each do |i|
      row = spreadsheet.row(i)
      break if row[0].blank?
      project = Project.where(name: row[0].to_s.strip).first_or_initialize
      project.build_region unless project.region
      project.locking_time = row[1].to_i
      project.real_estate_type = Constant::PROJECT_REAL_ESTATE_TYPE.invert.map { |e| e[1] if row[2].to_s.split(',').collect(&:strip).include?(e[0]) }.compact
      project.construction_density = row[3]
      project.total_area = row[4]
      project.region.city_id = RegionData.cities.find_by("name ILIKE ?", "%#{row[5]}").try(:id)
      project.region.district_id = RegionData.districts.where(parent_id: project.region.city_id).find_by("name ILIKE ?", "%#{row[6]}").try(:id)
      project.region.ward_id = RegionData.wards.where(parent_id: project.region.district_id).find_by("name ILIKE ?", "%#{row[7]}").try(:id)
      project.region.street = row[8]
      project.investors = Investor.where(name: row[9]).pluck('id')
      project.constructors = Constructor.where(name: row[10]).pluck('id')
      # don vi thiet ke row[11]
      project.developments = Development.where(name: row[11]).pluck('id')
      project.operators = Operator.where(name: row[12]).pluck('id')
      project.features = Constant::PROJECT_FEATURES.invert.map { |e| e[1] if row[13].to_s.split(',').collect(&:strip).include?(e[0]) }.compact
      project.description = row[14]
      project.price_from = row[15] if row[15].present?
      project.price_to = row[16] if row[16].present?
      project.area_from = row[17] if row[17].present?
      project.area_to = row[18] if row[18].present?
      project.internal_utilities = Constant::PROJECT_INTERNAL_UTILITIES.invert.map { |e| e[1] if row[19].to_s.split(',').collect(&:strip).include?(e[0]) }.compact
      project.external_utilities = row[20]
      project.ownership_period = Constant::PROJECT_OWNERSHIP_PERIOD.invert[row[21].to_s.strip]
      # phap ly du an project = row[22]
      project.foreigner = row[23] == 'Có' if row[23].present?
      project.banks = Constant::PROJECT_BANK.invert.map { |e| e[1] if row[24].to_s.split(',').collect(&:strip).include?(e[0]) }.compact
      project.loan_percentage_support = row[25].to_f * 100 if row[25].present?
      project.loan_support_period = row[26].to_i if row[26].present?
      project.commission_type = row[27] == '%' ? 1 : 0 if row[27].present?
      project.commission = row[28] if row[28].present?
      project.bonus = row[29] if row[29].present?
      project.not_required_image = true
      # init milestones default
      if project.milestones.blank?
        project.milestones.build(
          [
            { event_time: '', event: 'Khởi công', is_default: true },
            { event_time: '', event: 'Dự kiến bàn giao', is_default: true }
          ]
        )
      end
      unless project.save
        add_error("Dòng #{i}", project.errors.full_messages.join(", "))
      end
    end
  end

  def add_error info, message
    @import_success = false
    @ws.add_row [info, message]
  end
end

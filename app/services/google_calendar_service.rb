require "google/apis/calendar_v3"
require "google/api_client/client_secrets.rb"
CALENDAR_ID = 'primary'

class GoogleCalendarService
  def initialize
  end

  def sync

  end

  def get_event(task)
    # Example params task
    # task = {
    #   'id' => 'call2',
    #   'summary' => 'summary',
    #   'description' => 'description',
    #   'start_time' => Time.now,
    #   'end_time' => Time.now+1.day,
    # }
    event = {
      id: task['id'],
      summary: task['summary'],
      description: task['description'],
      start: Google::Apis::CalendarV3::EventDateTime.new(date_time: task['start_time'].to_datetime),
      end: Google::Apis::CalendarV3::EventDateTime.new(date_time: task['end_time'].to_datetime),
      sendNotifications: true
    }
    event[:id] = task['id'] if task['id']
    event
  end

  def get_google_calendar_client current_user
    client = Google::Apis::CalendarV3::CalendarService.new
    return unless (current_user.present? && current_user.access_token.present? && current_user.refresh_token.present?)
    secrets = Google::APIClient::ClientSecrets.new({
      "web" => {
        "access_token" => current_user.access_token,
        "refresh_token" => current_user.refresh_token,
        "client_id" => ENV["GOOGLE_CLIENT_ID"],
        "client_secret" => ENV["GOOGLE_SECRET"]
      }
    })
    begin
      client.authorization = secrets.to_authorization
      client.authorization.grant_type = "refresh_token"

      if current_user.expired?
        client.authorization.refresh!
        current_user.update_attributes(
          access_token: client.authorization.access_token,
          refresh_token: client.authorization.refresh_token,
          expires_at: client.authorization.expires_at.to_i
        )
      end
    rescue => e
      raise e.message
    end
    client
  end

  def create(task, user)
    client = get_google_calendar_client user
    event = get_event task
    event = Google::Apis::CalendarV3::Event.new(event)
    client.insert_event(CALENDAR_ID, event)
  end

  def edit(task, user)
    client = get_google_calendar_client user
    event = get_event task
    event = Google::Apis::CalendarV3::Event.new(event)
    client.update_event(CALENDAR_ID, event.id, event)
  end

  def delete(event_id, user)
    client = get_google_calendar_client user
    client.delete_event(CALENDAR_ID, event_id)
  end

  def get(event_id, user)
    client = get_google_calendar_client user
    client.get_event(CALENDAR_ID, event_id)
  end

  def get_list_events(user)
    client = get_google_calendar_client user
    list_events = client.list_events(CALENDAR_ID)
    # items = list_events.items
  end

end

require 'rest-client'

class ZaloService

  class << self
    def send_sms_callio_lead(user, project)
      return unless user&.phone_number.present?
      payload = {
        "ApiKey": ENV['ESMS_ZALO_API_KEY'],
        "SecretKey": ENV['ESMS_ZALO_SECRET_KEY'],
        "Phone": Phonelib.parse(user.phone_number, user.country_code).national&.delete(' '),
        "Params": [user.full_name, project.name, user.email],
        "TempID": '225624',
        "OAID": ENV['ESMS_ZALO_OAID']
      }

      response = RestClient.post(ENV['ESMS_ZALO_API_URL'], payload.to_json, content_type: :json)

      JSON.parse(response.body)["CodeResult"]
    end

    def send_sms_account_info(user, password)
      return unless user&.phone_number.present?
      user_phone_number = Phonelib.parse(user.phone_number, user.country_code).national.delete(' ')
      payload = {
        "ApiKey": ENV['ESMS_ZALO_API_KEY'],
        "SecretKey": ENV['ESMS_ZALO_SECRET_KEY'],
        "Phone": Phonelib.parse(user.phone_number, user.country_code).national&.delete(' '),
        "Params": ["#{user.account_type == 'agent' ? user_phone_number : user.email}", password],
        "TempID": '227234',
        "OAID": ENV['ESMS_ZALO_OAID']
      }

      response = RestClient.post(ENV['ESMS_ZALO_API_URL'], payload.to_json, content_type: :json)

      JSON.parse(response.body)["CodeResult"]
    end
  end
end

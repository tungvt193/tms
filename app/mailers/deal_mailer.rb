class DealMailer < ApplicationMailer
  include Rails.application.routes.url_helpers
  add_template_helper(DateTimeHelper)
  default from: "support@reti.vn"

  def notification(deal, noti, title, content, show_created_at = false, show_same_deal = false)
    @deal = deal
    @content = content
    @deal_url = "#{ENV.fetch('TMS_URL') { 'http://localhost:3000' }}#{noti.related_url}"
    @same_deal_url = "#{ENV.fetch('TMS_URL') { 'http://localhost:3000' }}#{admin_same_deal_path(@deal.id)}"
    @show_created_at = show_created_at
    @show_same_deal = show_same_deal
    # mail to: noti.user&.email, :subject => title
  end

  def notification_to_manager(deal, related_url, title, content, recipient, show_created_at = false, show_same_deal = false)
    @deal = deal
    @content = content
    @deal_url = "#{ENV.fetch('TMS_URL') { 'http://localhost:3000' }}#{related_url}"
    @same_deal_url = "#{ENV.fetch('TMS_URL') { 'http://localhost:3000' }}#{admin_same_deal_path(@deal.id)}"
    @show_created_at = show_created_at
    @show_same_deal = show_same_deal
    # mail to: recipient.email, :subject => title
  end

  def notification_fyi(subject, content)
    @content = content
    mail to: 'thuynt.min@gmail.com', subject: subject
  end

  def notification_import_deal_fyi(subject, imported_deals, unimported_deals)
    @imported_deals = imported_deals
    @unimported_deals = unimported_deals
    mail to: 'thuy.nguyen@reti.vn', subject: subject
  end

  def download_cs_tickets(mail_to, url)
    @url_download = url
    mail to: mail_to, subject: "Phiếu CSKH #{Date.current.strftime("%d/%m/%Y")}"
  end

  def notification_send_voucher(mail_to, content)
    @content = content
    mail to: mail_to, subject: "RETI kính tặng quý khách voucher"
  end

  def deal_from_callio(deal)
    @deal = deal
    if @deal.assignee.email.present?
      mail to: @deal.assignee.email, subject: "Lead hot từ Telesale!"
    end
  end

  def export_deals(mail_to, file, file_name)
    attachments[file_name] = File.read(file)
    mail to: mail_to, subject: "Báo cáo QLGD #{Date.today.strftime('%d/%m/%Y')}"
  end
end

class UserMailer < ApplicationMailer
   default from: "RETI Support <support@reti.vn>"

  def reset_password_instructions(user)
    @user = user
    mail to: user.email, subject: 'RETI - Thiết lập lại mật khẩu đăng nhập TMS'
  end

   def retizy_reset_password_instructions(user, token, redirect_url, config)
     @user = user
     @token = token
     @redirect_url = redirect_url
     @config = config
     mail to: user.email, subject: 'RETI - Thiết lập lại mật khẩu đăng nhập RETIZY'
   end

  def notify_import(user, file, file_name, subject)
    @user = user
    attachments[file_name] = File.read(file)
    mail to: @user.email, subject: subject
  end

  def send_account_info_after_create(user, password)
   @user = user
   @password = password
   mail to: user.email, subject: 'RETI - Xác nhận tài khoản TMS'
  end

  def send_account_retizy_info_after_create(user, password)
    @user = user
    @password = password
    mail to: user.email, subject: 'RETI - Xác nhận tài khoản ứng dụng Retizy'
   end
end

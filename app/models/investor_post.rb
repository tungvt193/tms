class InvestorPost < ApplicationRecord

  attr_accessor :pseudo_id

  belongs_to :project

  validates :title, presence: true
  validates :post_url, presence: true, valid_url: true
end

class Document < ApplicationRecord
  
  # Uploader
  mount_uploader :preview_image, ProjectImageUploader
  mount_uploader :file, ProjectUploader

  # Associations
  belongs_to :project
  
  # Validations
  validates :name, presence: true
  validates :file, presence: true
  validates :description, presence: true
  validates :preview_image, presence: true
end

class ImportConstant
  # Dự án
  PROJECTS = Project.pluck(:name, :id).to_h
  # Giới tính
  GENDERS = { 'Nam' => 0, 'Nữ' => 1, 'Khác' => 2 }
  # Tình trạng liên hệ
  CONTACT_STATUSES = Constant::CONTACT_STATUSES.invert
  # Nội dung tương tác
  INTERACTION_DETAILS = Constant::INTERACTION_CHECKLIST.invert
  # Loại hình sản phẩm
  PRODUCT_TYPES = Constant::PROJECT_REAL_ESTATE_TYPE.invert
  # Khoảng giá
  PRICE_RANGES = Constant::DEAL_PRICE_RANGE.invert
  # Diện tích
  ACREAGE_RANGES = Constant::DEAL_ACREAGE_RANGE.invert
  # Mục đích mua
  PURCHASE_PURPOSES = Constant::PURCHASE_PURPOSE.invert
  # Hướng
  DIRECTIONS = Reti::Product::DIRECTIONS.invert
  # Vấn đề vướng mắc
  PROBLEMS = Constant::PROBLEMS.invert
  # Đánh giá khả năng tài chính
  FINANCIAL_CAPABILITY = Constant::FINANCIAL_CAPABILITY.invert
  # Trạng thái giao dịch
  STATES = %w(pending interest uninterested meet refundable_deposit lock deposit contract_signed canceled request_recall).map { |s| [I18n.t("deal.states.#{s}"), s] }.to_h
  # Nguồn
  SOURCES = (Constant::SOURCES[:sale][:online].map { |k, v| ['NVKD/' + I18n.t("deal.sources.sale.online.#{k}").split('-').last.strip, v] } +
    Constant::SOURCES[:sale][:offline].map { |k, v| ['NVKD/' + I18n.t("deal.sources.sale.offline.#{k}").split('-').last.strip, v] } +
    Constant::SOURCES[:company][:online].map { |k, v| ['Công ty/' + I18n.t("deal.sources.company.online.#{k}").split('-').last.strip, v] } +
    Constant::SOURCES[:company][:offline].map { |k, v| ['Công ty/' + I18n.t("deal.sources.company.offline.#{k}").split('-').last.strip, v] }).to_h
  # Lý do Không quan tâm
  UNINTERESTED_REASONS = Constant::UNINTERESTED_REASON.invert
  # Lý do Hủy
  CANCELED_REASONS = Constant::CANCELED_REASON.invert
end

class DealItem
  include ActiveModel::Model

  ATTRS = [:id, :project_name, :product_name, :customer_name, :customer_phone, :gender,
           :contact_status, :interaction_detail, :product_type, :product_type_detail, :price_range,
           :acreage_range, :interested_product, :purchase_purpose, :door_direction, :balcony_direction,
           :trouble_problem_list, :demand_for_advances, :second_phone_number, :email, :financial_capability,
           :property_ownership, :state, :assignee_email, :source, :source_detail, :create_deal_date,
           :updated_at, :lead_scoring, :uninterested_reason, :canceled_reason]

  attr_accessor(*ATTRS)

  validates :id, :project_name, presence: true
  # validate :check_assignee_existed
  validate :check_deal_existed

  def initialize(attributes = {})
    attributes.each { |name, value| send("#{name}=", value) }
  end

  # Mark this object has no related table in database
  def persisted?
    false
  end

  def check_assignee_existed
    unless User.find_by_email assignee_email
      errors.add(:assignee_email, 'Nhân viên kinh doanh chưa có trong hệ thống.')
    end
  end

  def check_deal_existed
    if Deal.find_by_import_id id
      errors.add(:id, 'Giao dịch đã tồn tại trong hệ thống.')
    end
  end

  def build_object
    ApplicationRecord.transaction do
      customer = Customer.new(
        name: customer_name,
        phone_number: customer_phone,
        second_phone_number: second_phone_number,
        country_code: 'VN',
        data_source: ImportConstant::SOURCES[source],
        gender: ImportConstant::GENDERS[gender],
        email: email
      )
      customer.deals.build(
        name: "#{customer_name} - #{project_name}",
        import_id: id,
        assignee_id: User.find_by_email(assignee_email)&.id,
        project_id: ImportConstant::PROJECTS[project_name],
        contact_status: ImportConstant::CONTACT_STATUSES[contact_status],
        interaction_detail: [ImportConstant::INTERACTION_DETAILS[interaction_detail]].compact,
        product_type: ImportConstant::PRODUCT_TYPES[product_type],
        product_type_detail: product_type_detail,
        price_range: ImportConstant::PRICE_RANGES[price_range],
        acreage_range: ImportConstant::ACREAGE_RANGES[acreage_range],
        interested_product: interested_product,
        purchase_purpose: ImportConstant::PURCHASE_PURPOSES[purchase_purpose],
        door_direction: [ImportConstant::DIRECTIONS[door_direction]].compact,
        balcony_direction: [ImportConstant::DIRECTIONS[balcony_direction]].compact,
        trouble_problem_list: [ImportConstant::PROBLEMS[trouble_problem_list]].compact,
        demand_for_advances: demand_for_advances,
        state: ImportConstant::STATES[state],
        source: ImportConstant::SOURCES[source],
        source_detail: source_detail,
        create_deal_date: create_deal_date,
        is_change_create_date: create_deal_date.present?,
        updated_at: updated_at,
        lead_scoring: lead_scoring,
        uninterested_reason: ImportConstant::UNINTERESTED_REASONS[uninterested_reason],
        canceled_reason: ImportConstant::CANCELED_REASONS[canceled_reason],
        author: User.current,
        labels: [contact_status].compact,
        assigned_at: Time.now,
        state_changed_at: Time.now,
        create_deal_date: create_deal_date || Time.now
      ).build_customer_persona(
        second_phone_number: second_phone_number,
        email: email,
        gender: ImportConstant::GENDERS[gender],
        financial_capability: ImportConstant::FINANCIAL_CAPABILITY[financial_capability],
        property_ownership: property_ownership
      )
      customer
    end
  rescue ActiveRecord::RecordInvalid
    nil
  end
end

class DealImport
  include ActiveModel::Model
  require 'roo'

  attr_accessor :file

  validates_presence_of :file, message: "File import không được để trống."
  validate :check_file_validation
  # validates_format_of :file, :with => %r{\.(csv|xls|xlsx)$}i, message: "File không đúng định dạng (csv, xls, xlsx)."

  def initialize(attributes = {})
    attributes.each { |name, value| send("#{name}=", value) }
  end

  # Mark this object has no related table in database
  def persisted?
    false
  end

  def check_file_validation
    if file.nil?
      errors.add(:file, 'File import không được để trống.') if errors.blank?
    elsif !%w(.csv .xls .xlsx).include? File.extname(file.path)
      errors.add(:file, 'File không đúng định dạng (csv, xls, xlsx).')
    else
      sheet = Roo::Spreadsheet.open(file.path, extension: File.extname(file.path)[1..-1])
      errors.add(:file, 'File không đúng.') if sheet.a2 != "ID deal"
    end
  end

  def open_spreadsheet
    Roo::Spreadsheet.open(file.path, extension: File.extname(file.path)[1..-1])
  end

  def load_items
    sheet = open_spreadsheet
    header = DealItem::ATTRS
    item_groups = (3..sheet.last_row).map { |i| DealItem.new Hash[[header, sheet.row(i).map { |cell| cell&.strip }].transpose] }.group_by(&:id)
    item_groups.map do |id, items|
      distinct_item = DealItem.new
      header.each { |col| distinct_item.send("#{col}=", items.map { |item| item.send("#{col}") }.compact.uniq.last) }
      distinct_item
    end
  end

  def items
    @items ||= load_items
  end

  def imported_items
    @imported_items ||= []
  end

  def save
    imported_ids = Deal.where(import_id: items.map(&:id)).pluck :import_id
    customers = []
    unimported_items = []
    items.reject { |item| imported_ids.include?(item.id) }.each do |item|
      if item.valid?
        customers << item.build_object
        imported_items << item
      else
        unimported_items << item
      end
    end
    result = Customer.import customers.compact, recursive: true, validate: false
    if result.failed_instances.blank?
      Deal.reindex
      Customer.reindex
    else
      result.failed_instances.each do |failure|
        puts "Import deal error: #{failure.errors}"
      end
    end
    DealMailer.notification_import_deal_fyi("Imported Deals TMS (#{Rails.env})", imported_items, unimported_items).deliver_later(wait: 3.second)
  rescue StandardError => e
    return false
  end
end

class Customer < ApplicationRecord

  # Include
  include CustomerSearch
  include AASM

  # Attributes Accessor
  attr_accessor :phone_number_sanitized
  attr_accessor :second_phone_number_sanitized
  attr_accessor :phone_number_international
  attr_accessor :second_phone_number_international
  attr_accessor :is_mobile, :same_customer
  attr_accessor :quick_create_from_app

  # Soft Delete
  acts_as_paranoid

  # Constants

  # Delegates

  # Callbacks
  after_commit :update_same_deal, :update_same_customer
  before_save :format_phone_number
  after_find :set_format_phone

  # Associations
  belongs_to :city, class_name: 'RegionData', foreign_key: :city_id, optional: true
  belongs_to :user, foreign_key: :created_by_id
  has_many :notes, as: :objectable, dependent: :destroy
  has_many :visit_journeys, dependent: :destroy
  accepts_nested_attributes_for :visit_journeys, allow_destroy: true
  has_many :deals, dependent: :nullify, inverse_of: :customer
  has_many :email_journeys
  accepts_nested_attributes_for :notes, allow_destroy: true
  has_many :form_journeys, -> { order("recent_conversion_date DESC") }, dependent: :destroy
  accepts_nested_attributes_for :form_journeys, allow_destroy: true
  has_many :ads_journeys, -> { order("create_date DESC") }, dependent: :destroy
  accepts_nested_attributes_for :ads_journeys, allow_destroy: true
  has_many :sms_journeys, -> { order("sms_created_date DESC") }, dependent: :destroy
  accepts_nested_attributes_for :sms_journeys, allow_destroy: true
  has_many :customer_personas, dependent: :nullify, inverse_of: :customer
  has_many :vouchers_customers, :class_name => 'VouchersCustomers'
  has_many :vouchers, through: :vouchers_customers

  # Validations
  validates :name, presence: true, format: { with: /\s/ }
  validates :data_source, presence: true, unless: Proc.new { |customer| customer.quick_create_from_app }
  validates :phone_number, presence: true, unless: -> { User.current&.marketing? || User.current&.super_admin? || User.current&.bi_ba? || User.current&.sale_admin? }
  validates :country_code, presence: true
  validates :email, allow_blank: true, format: { with: /\A^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$\z/ }
  validates :identity_card, allow_blank: true, format: { with: /\A^(?=[A-Z0-9]*$)(?:.{9}|.{12})$\z/ }
  validate :check_phone_valid

  # Scopes
  scope :by_ids, -> (ids) { where id: ids }

  # AASM
  aasm column: :state, whiny_transitions: false do
    state :inauthentic, initial: true # Chưa xác thực
    state :phone_authentic # Xác thực số điện thoại
    state :authentic # Xác thực đầy đủ
  end

  # ================================================== #

  def get_gender
    Constant::CUSTOMER_GENDER.invert.key(gender)
  end

  def get_dob
    dob.present? ? dob.strftime('%d/%m/%Y') : ''
  end

  def get_state
    I18n.t("customer.states.#{self.aasm.current_state}")
  end

  def get_next_visit_id
    self.visit_journeys.where.not(id: nil).pluck(:visit_id).uniq.compact.max.to_i + 1
  end

  def name_and_phone
    "#{name} - #{phone_number}"
  end

  def email_journey_groups
    groups = {}
    self.email_journeys.order(email_type: :desc, created_date: :desc).group_by(&:campaign_id).each do |campaign_id, journeys|
      sent_email = journeys.find { |j| j.sent? }
      delivered_email = journeys.find { |j| j.delivered? }
      open_emails = journeys.select { |j| j.open? }
      first_open_email = open_emails.last
      click_emails = journeys.select { |j| j.click? }
      groups[campaign_id] = {
        ids: journeys.pluck(:id),
        open_count: open_emails.size,
        click_count: click_emails.size,
        sent_email: sent_email,
        delivered_email: delivered_email,
        first_open_email: first_open_email
      }
    end
    groups.sort_by { |k, v| v[:sent_email]&.created_date || 0 }.reverse
  end

  def visit_journey_groups_base
    groups = {}
    self.visit_journeys.reverse.group_by(&:visit_id).each do |visit_id, journeys|
      journeys = journeys.sort { |a, b| (a.id.nil? || b.id.nil?) ? (b.object_id <=> a.object_id) : (a.visited_date <=> b.visited_date) }.reverse
      title = journeys.first.title
      title += " (+#{journeys.size - 1} trang)" if journeys.size > 1
      groups[visit_id.to_s] = {
        ids: journeys.pluck(:id),
        title: title,
        last_visited_date: journeys.first.visited_date,
        journeys: journeys
      }
    end
    groups
  end

  def visit_journey_groups
    visit_journey_groups_base.select { |k, v| !v[:journeys].all? { |j| j.new_record? } }
  end

  def all_new_visit_journey_groups
    visit_journey_groups_base.select { |k, v| v[:journeys].all? { |j| j.new_record? } }
  end

  def new_ads_journeys
    ads_journeys.reject { |n| !n.new_record? }
  end

  def old_ads_journeys
    ads_journeys.reject { |n| n.new_record? }
  end

  def update_same_customer(force_update = false)
    if previous_changes[:phone_number].present? || force_update
      old_same_customers = Customer.where("#{self.id} = ANY (same_customers)")
      old_same_customers.update_all("same_customers = array(select unnest(same_customers) except select unnest(array#{[self.id]}))")

      new_same_customers = Customer.where(phone_number: self.phone_number)
      new_same_ids = new_same_customers.pluck :id
      new_same_customers.each do |customer|
        customer.update_columns(same_customers: new_same_ids - [customer.id])
      end
    end
  end

  private

  def check_phone_valid
    errors.add(:phone_number, :invalid) if !Phonelib.parse(self.phone_number, self.country_code).valid? && self.phone_number.present?
    errors.add(:second_phone_number, :invalid) if !Phonelib.parse(self.second_phone_number, self.country_code).valid? && self.second_phone_number.present?
    self.phone_number = Phonelib.parse(self.phone_number, self.country_code).full_e164.sub('+', '')
    if self.user&.internal? && User.current&.is_sale?
      customer_same = CustomerPolicy::Scope.new(self.user, Customer).resolve.where.not(id: self.id).where(phone_number: self.phone_number)
      if self.phone_number.present? && customer_same.present?
        arr = []
        customer_same.each do |cus|
          if self.is_mobile
            arr.push("<span class='same-customer-mobile' data-customer-id='#{cus.id}' data-customer-name='#{cus.name}' data-customer-phone='#{cus.phone_number}'>#{cus.name}</span>")
          else
            arr.push("<a target='_blank' href=#{Rails.application.routes.url_helpers.edit_admin_customer_path(cus.id)}>#{cus.name}</a>")
          end
        end
        msg = "Số điện thoại này đang bị trùng với khách hàng: #{arr.join(', ')}"
        errors.add(:phone_number, msg)
        self.same_customer = self.is_mobile
      end
    end
  end

  def format_phone_number
    self.phone_number = Phonelib.parse(self.phone_number, self.country_code).full_e164.sub('+', '')
    self.second_phone_number = Phonelib.parse(self.second_phone_number, self.country_code).full_e164.sub('+', '')
  end

  def set_format_phone
    self.phone_number_sanitized = Phonelib.parse(self.phone_number, self.country_code).national&.delete(' ') if self.phone_number.present? && self.country_code.present?
    self.second_phone_number_sanitized = Phonelib.parse(self.phone_number, self.country_code).national&.delete(' ') if self.second_phone_number.present? && self.country_code.present?
    self.phone_number_international = Phonelib.parse(self.phone_number, self.country_code)&.international if self.phone_number.present? && self.country_code.present?
    self.second_phone_number_international = Phonelib.parse(self.second_phone_number, self.country_code)&.international if self.second_phone_number.present? && self.country_code.present?
  end

  def update_same_deal
    return if previous_changes[:id].present? # check new record
    if self.destroyed?
      customer_deals = Deal.where(customer_id: nil) # or Deal.where(customer_id: self.id)
      deal_ids = customer_deals.pluck('id')
      customer_deals.update_all(same_deals: [])
      Deal.joins(:customer).where('customers.phone_number': self.phone_number).update_all("same_deals = array(select unnest(same_deals) except select unnest(array#{deal_ids}))") if deal_ids.present?
    end
    # check same deal
    if previous_changes[:phone_number].present?
      # remove old
      if previous_changes[:phone_number][0].present?
        customer_deals = Deal.where(customer_id: self.id)
        deal_ids = customer_deals.pluck('id')
        customer_deals.update_all(same_deals: [])
        Deal.joins(:customer).where('customers.phone_number': previous_changes[:phone_number][0]).update_all("same_deals = array(select unnest(same_deals) except select unnest(array#{deal_ids}))") if deal_ids.present?
      end
      # set new
      if previous_changes[:phone_number][1].present?
        deals = Deal.joins(:customer).where('customers.phone_number': previous_changes[:phone_number][1])
        deals.each do |deal|
          same_deals = if deal.project_id
                         deals.where.not(id: deal.id).where('deals.project_id': [deal.project_id, nil])
                       else
                         deals.where.not(id: deal.id)
                       end
          deal.update_column(:same_deals, same_deals.pluck('id'))
        end
        # update labels
        self.deals.each do |deal|
          if deal.labels.include?("Chưa có số điện thoại")
            deal.labels.delete("Chưa có số điện thoại")
            labels = deal.labels
            deal.update_column(:labels, labels)
            ActionCable.server.broadcast "deal_channel", deal: deal, user_id: nil
          end
        end
      end
    end
  end

  def self.search_same_customers(params)
    fields = [:name, :phone_number, :name_and_phone, :phone_with_zero]
    str_query = params[:query].present? ? params[:query] : '*'
    search_results = search str_query,
                            fields: fields,
                            match: :word_middle,
                            misspellings: { below: 0 }
    return by_ids(search_results.pluck(:id))
  end

  # Update/clean all customers
  def self.update_same_customers
    Customer.all.group_by(&:phone_number).reject! { |k, v| v.count < 2 || k == nil }.each do |phone_number, customers|
      same_ids = customers.pluck :id
      customers.each do |customer|
        customer.update_columns(same_customers: same_ids - [customer.id])
      end
    end
  end

end

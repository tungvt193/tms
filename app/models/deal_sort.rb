class DealSort < ApplicationRecord
  # acts_as_paranoid

  belongs_to :deal
  belongs_to :user
  validates_uniqueness_of :deal_id, scope: :user_id
end

class ProductLockHistory < ApplicationRecord
  include AASM

  # Uploader
  mount_uploader :unc_image, ProductLockHistoryUploader

  belongs_to :user
  belongs_to :project
  belongs_to :product
  has_one :product_lock_history_log, dependent: :destroy
  has_one :deal

  after_save :lock_product
  after_save :update_to_logs

  scope :locking_process, -> { where(state: %i(locking deposit_confirmation waiting_reti_acceptance pending deal_confirmation success)) }
  scope :with_state, -> (state) { where(state: state) }

  # AASM
  aasm column: :state, whiny_transitions: false do
    state :enqueued, initial: true # Chờ lock
    state :locking # Đang lock
    state :deposit_confirmation # Xác nhận cọc(App)/Cần xác nhận(TMS)
    state :waiting_reti_acceptance # Chờ RETI xác nhận(App)/Xác nhận UNC(TMS)
    state :out_of_time # Hết hạn
    state :success # Thành công
    state :pending # Tạm treo(App)/Xác nhận lại(TMS)
    state :canceled # Đã hủy
    state :failed # Không thành công
    state :deal_confirmation # Xác nhận giao dịch

    event :sale_admin_accept_unc do
      after do
        self.product.update_column(:state, :deposited)
        NotificationJob.perform_later('sale_admin_accept_unc', self)
      end
      transitions from: :waiting_reti_acceptance, to: :deal_confirmation
    end
    event :sale_admin_reject_unc do
      after do
        self.product.update_column(:state, :for_sale)
        NotificationJob.perform_later('sale_admin_reject_unc', self)
      end
      transitions from: :waiting_reti_acceptance, to: :failed
    end
    event :sale_admin_pending_unc do
      after do
        NotificationJob.perform_later('sale_admin_pending_unc', self)
      end
      transitions from: [:deposit_confirmation, :waiting_reti_acceptance], to: :pending
    end
  end

  def update_state!(state, image_file_name)
    case state.to_sym
    when :deposit_confirmation
      return unless self.locking?
      self.update(state: :deposit_confirmation)
    when :waiting_reti_acceptance
      return unless image_file_name.present? && self.deposit_confirmation?

      campaign_lock = self.project.campaign_locks.find_by("products && ?", "{#{self.product_id}}")
      self.update_columns(unc_image: image_file_name, deadline_for_sale_admin: Time.zone.now.advance(minutes: campaign_lock.unc_confirmation_period_for_sale_admin))
      self.update(state: :waiting_reti_acceptance)
      NotificationService.sale_upload_unc(self)

      jobs = Sidekiq::ScheduledSet.new.select { |j| j.args.first["arguments"][0] == 'enqueued' && j.args.first["arguments"][1].values.first.split('/').last.to_i == self.product_id }
      jobs.each do |job|
        job.delete
      end if jobs.present?

      CheckProductLockHistoryJob.set(wait_until: self.deadline_for_sale_admin).perform_later(self.id, 'timeout_for_sale_admin')

      return true
    when :canceled
      self.update(state: :canceled)
    else
      return
    end
  end

  def self.get_product_lock_histories(state, user_id)
    product_lock_histories = ProductLockHistory.where(state: state, user_id: user_id)
    if %w(locking deposit_confirmation).include?(state)
      product_lock_histories.order(deadline_for_sale: :asc)
    else
      product_lock_histories.order(updated_at: :desc)
    end
  end

  def display_label_state
    case self.state.to_s
    when 'pending'
      'label-product-lock-history-pending'
    when 'waiting_reti_acceptance'
      'label-product-lock-history-waiting-reti-acceptance'
    when 'deposit_confirmation'
      'label-product-lock-history-deposit-confirmation'
    when 'locking'
      'label-product-lock-history-locking'
    when 'success'
      'label-product-lock-history-success'
    when 'failed'
      'label-product-lock-history-failed'
    when 'out_of_time'
      'label-product-lock-history-out-of-time'
    when 'canceled'
      'label-product-lock-history-canceled'
    end
  end

  def display_countdown
    case self.state.to_sym
    when :deposit_confirmation
      self.deadline_for_sale
    when :waiting_reti_acceptance
      self.deadline_for_sale_admin
    else
      return nil
    end
  end

  def message_update_state
    case self.state.to_sym
    when :deal_confirmation
      'Xác nhận đồng ý của bạn đã được gửi đi'
    when :failed
      'Xác nhận từ chối của bạn đã được gửi đi'
    else
      'Xác nhận tạm treo của bạn đã được gửi đi'
    end
  end

  private

  def lock_product
    if self.locking?
      self.product.update_column(:state, :locking)
      CheckProductLockHistoryJob.set(wait_until: self.deadline_for_sale).perform_later(self.id, 'timeout_for_sale')
      NotificationJob.set(wait_until: self.deadline_for_sale - 10.minutes).perform_later('warning_lock_timeout', self)
    end
  end

  def update_to_logs
    log = self.product_lock_history_log
    current_time = Time.zone.now
    current_state = self.state.to_sym
    if log.present?
      if self.saved_change_to_state?
        case current_state
        when :locking
          log.locking_entered = current_time
        when :deposit_confirmation
          log.deposit_confirmation_entered = current_time
        when :waiting_reti_acceptance
          log.waiting_reti_acceptance_entered = current_time
        when :out_of_time
          log.out_of_time_entered = current_time
        when :success
          log.success_entered = current_time
        when :pending
          log.pending_entered = current_time
        when :canceled
          log.canceled_entered = current_time
        when :failed
          log.failed_signed_entered = current_time
        when :deal_confirmation
          log.deal_confirmation_entered = current_time
        end

        prev_state = previous_changes['state'].first.to_sym
        case prev_state
        when :enqueued
          log.enqueued_exited = current_time
        when :locking
          log.locking_exited = current_time
        when :deposit_confirmation
          log.deposit_confirmation_exited = current_time
        when :waiting_reti_acceptance
          log.waiting_reti_acceptance_exited = current_time
        when :out_of_time
          log.out_of_time_exited = current_time
        when :success
          log.success_exited = current_time
        when :pending
          log.pending_exited = current_time
        when :canceled
          log.canceled_exited = current_time
        when :failed
          log.failed_signed_exited = current_time
        when :deal_confirmation
          log.deal_confirmation_exited = current_time
        end
      end
    else
      log = build_product_lock_history_log(:"#{self.state}_entered".to_sym => created_at)
    end

    log.save
  end
end

class DepositNote < ApplicationRecord
  #chuyển sang dùng polymorphic bảng note
  # Associations
  belongs_to :deposit
  belongs_to :author, class_name: 'User', foreign_key: :created_by_id, required: false

  # Validations
  validates :content, presence: true
end

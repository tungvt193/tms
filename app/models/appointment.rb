class Appointment < ApplicationRecord
  include WriteHistory
  acts_as_paranoid

  attr_accessor :canceled, :pseudo_id

  belongs_to :author, class_name: 'User', foreign_key: :created_by_id, required: false
  belongs_to :deal

  validates :content, :appointment_date, presence: true
  validates :result, :state, presence: true, if: -> { self.state == 0 }

  after_save :after_save_action
  after_destroy :after_destroy_action
  after_commit :push_notification_appointment_result, :push_notification_appointment_date

  scope :flag_true, -> { where flag: true }
  scope :flag_false, -> { where flag: false }

  def after_save_action
    task = {
      'id' => "appointment#{self.id}",
      'summary' => self.content,
      'description' => self.result,
      'start_time' => self.appointment_date,
      'end_time' => self.appointment_date + 1.hours,
    }
    g = GoogleCalendarService.new
    begin
      edi = g.edit(task, author)
    rescue Google::Apis::ClientError => e
      g.create(task, author) if e.status_code == 404
    rescue Exception => e
      p e
    end
  end

  def after_destroy_action
    g = GoogleCalendarService.new
    begin
      g.delete("appointment#{self.id}", author)
    rescue Google::Apis::ClientError => e
      p e
    rescue Exception => e
      p e
    end
  end

  def group_by_date
    appointment_date.to_date.to_s
  end

  def push_notification_appointment_date
    if self.appointment_date > Time.now && self.previous_changes[:appointment_date].present?
      old_value = self.new_record? ? self.previous_changes[:appointment_date].first.to_s : nil
      AppointmentJob.set(wait_until: self.appointment_date - 1.hours).perform_later('appointment_date', self, old_value, self.appointment_date.to_s, self.deal)
      job = Sidekiq::ScheduledSet.new.detect { |s| s.args.first["arguments"][0] == 'appointment_date' && s.args.first["arguments"][1].values.first.split('/').last.to_i == self.id && s.args.first["arguments"][3] == self.previous_changes[:appointment_date].first.to_s }
      job.delete if job.present?
    end
  end

  def push_notification_appointment_result
    if self.result.blank? && self.appointment_date > Time.now && self.previous_changes[:appointment_date].present?
      old_value = self.new_record? ? self.previous_changes[:appointment_date].first.to_s : nil
      AppointmentJob.set(wait_until: self.appointment_date + 1.day).perform_later('appointment_result', self, old_value, self.appointment_date.to_s, self.deal)
      job = Sidekiq::ScheduledSet.new.detect { |s| s.args.first["arguments"][0] == 'appointment_result' && s.args.first["arguments"][1].values.first.split('/').last.to_i == self.id && s.args.first["arguments"][3] == self.previous_changes[:appointment_date].first.to_s }
      job.delete if job.present?
    end
  end
end

class SaleDocument < ApplicationRecord
  include ConvertSlug

  enum sale_document_type: [:sale_kit, :sale_policy]

  # Uploader
  mount_uploader :file, SaleDocumentUploader

  # Associations
  belongs_to :project, touch: true

  # Validations
  validates :title, presence: true
  validates :file, presence: true

  def document_url
    "#{ENV['WEBSITE_URL']}/tai-lieu/#{self.slug}"
  end
end
 
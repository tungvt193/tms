class WishList < ApplicationRecord
  belongs_to :objectable, polymorphic: true
  belongs_to :user
end
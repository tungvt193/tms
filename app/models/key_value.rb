class KeyValue < ApplicationRecord
  
  # Uploader
  mount_uploader :icon, ProjectImageUploader

  # Associations
  belongs_to :project
  
  # Validations
  validates :icon, presence: true
  validates :title, presence: true
  validates :description, presence: true
end

class ReferralHistory < ApplicationRecord
  belongs_to :user
  belongs_to :referred, class_name: 'User', foreign_key: 'referred_id'
end

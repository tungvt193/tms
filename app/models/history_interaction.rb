class HistoryInteraction < ApplicationRecord

  attr_accessor :canceled, :pseudo_id

  belongs_to :author, class_name: 'User', foreign_key: :created_by_id, required: false
  belongs_to :deal

  validates :interaction_type, :interaction_date, :interaction_content, presence: true

  def get_interaction_type
    interaction_type == 0 ? 'Telesale' : 'Trực tiếp'
  end
end
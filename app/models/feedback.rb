class Feedback < ApplicationRecord
  include WriteHistory
  acts_as_paranoid

  belongs_to :author, class_name: 'User', foreign_key: :created_by_id, required: false
  belongs_to :deal

  validates :title, :content, presence: true
end

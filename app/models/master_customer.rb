class MasterCustomer < ApplicationRecord
  has_one :master_customer_persona, dependent: :destroy
  has_many :master_email_journeys, dependent: :destroy
  has_many :master_form_journeys, dependent: :destroy
  has_many :master_sms_journeys, dependent: :destroy
  has_many :master_visit_journeys, dependent: :destroy
  has_many :master_ads_journeys, dependent: :destroy
  accepts_nested_attributes_for :master_customer_persona, allow_destroy: true
end

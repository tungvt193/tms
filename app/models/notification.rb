require 'fcm'

class Notification < ApplicationRecord
  acts_as_paranoid

  enum notification_type: [:deal, :product, :share_customer, :project, :lock_history, :sale_document, :campaign_lock]

  belongs_to :user
  belongs_to :deal, optional: true

  # after_create :push_notification
  after_save :push_notification
  after_save :read_notification

  def self.push_notification_arr(ids)
    Notification.where(id: ids, user_id: User.current.id).each do |notification|
      ActionCable.server.broadcast "notification_channel_#{notification.user_id}", title: notification.title,
                                   notification: notification.render_notification(notification), popup: notification.render_notification_popup(notification)
      begin
        PushNotifier.send_push(notification)
      rescue Exception => e
        p e
      end
    end
  end

  def push_notification
    return unless self.send_now && self.saved_change_to_send_now? 
    ActionCable.server.broadcast "notification_channel_#{user.id}", title: self.title,
                                 notification: render_notification(self), popup: render_notification_popup(self)
    fcm = FCM.new(ENV["FIREBASE_SERVER_KEY"])
    registration_ids = DeviceToken.where(user_id: self.user_id).pluck(:device_token)
    options = {
      'notification': {
        'title': self.title,
        'body': ActionView::Base.full_sanitizer.sanitize(self.content)
      },
      'data': {
        'id': self.id,
        'object_id': self.get_object_id,
        'notification_type': self.notification_type,
        'clickable': self.related_url.present? ? true : false
      }
    }
    response = fcm.send(registration_ids, options)
    begin
      PushNotifier.send_push(self)
    rescue Exception => e
      p e
    end
  end

  def render_notification(notification)
    ApplicationController.renderer.render partial: "layouts/nav_item",
                                          locals: { notification: notification }
  end

  def render_notification_popup(notification)
    ApplicationController.renderer.render partial: "layouts/popup_notification",
                                          locals: { notification: notification }
  end

  def read_notification
    if self.saved_change_to_read_at?
      ActionCable.server.broadcast "notification_channel_#{self.user_id}",
                                   readed: self.id
    end
  end

  def self.read_all_notification
    ActionCable.server.broadcast "notification_channel_#{User.current&.id}", read_all: true
  end

  def get_object_id
    if self.deal_id
      object_id = self.deal_id
    else
      object_id = self.related_url.split('/')[2].to_i
    end
    object_id
  end
end

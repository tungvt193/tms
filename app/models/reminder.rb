class Reminder < ApplicationRecord
  acts_as_paranoid

  belongs_to :user
  belongs_to :deal, optional: true

  after_create :push_reminder
  after_save :read_reminder

  private

  def push_reminder
    ActionCable.server.broadcast "reminder_channel_#{user.id}",
                                 reminder: render_reminder(self)
  end

  def render_reminder(reminder)
    ApplicationController.renderer.render partial: "layouts/nav_reminder_item",
                                          locals: { reminder: reminder }
  end

  def read_reminder
    if self.saved_change_to_read_at?
      ActionCable.server.broadcast "reminder_channel_#{self.user_id}",
                                   readed: self.id
    end
  end

  def self.read_all_reminder
    ActionCable.server.broadcast "reminder_channel_#{User.current&.id}", read_all: true
  end
end

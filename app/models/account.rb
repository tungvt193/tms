class Account < ApplicationRecord
  include AccountSearch
  extend Devise::Models

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable

  # Associations
  has_many :reason_locks, dependent: :destroy
  accepts_nested_attributes_for :reason_locks, allow_destroy: true

  # Validations
  validates :password, :presence => true,
            :confirmation => true,
            :length => {within: 6..20},
            :on => :create
  validates :password, :confirmation => true,
            :length => {within: 6..20},
            :allow_blank => true,
            :on => :update
  validates :full_name, presence: true, format: { with: /\s/ }
  validates :phone, presence: true, format: { with: /\A([+]\w|)+[0-9]*$\z/ }
  validates :email, allow_blank: true, format: { with: /\A^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$\z/ }
  validates :identity_card, allow_blank: true, format: { with: /\A^(?=[A-Z0-9]*$)(?:.{9}|.{12})$\z/ }
  validate :reason_validate


  def phone_verify_text
    if phone_verify
      'Đã xác thực OTP'
    else
      'Chưa xác thực'
    end
  end

  def active_text
    if active
      'Hoạt động'
    else
      'Tạm khóa'
    end
  end

  def reason_validate
    unless self.active
      reason_size = reason_locks.reject { |p| p._destroy }.size
      errors.add(:reason_locks, 'Phải nhập ít nhất 1 lý do khoá') if reason_size == 0
    end
  end
  
end

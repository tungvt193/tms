class ProjectNewUpdate < ApplicationRecord

  # Delegates
  delegate :name, to: :project, prefix: :project, allow_nil: true

  # Associations
  belongs_to :project, optional: true
  belongs_to :author, class_name: 'User', foreign_key: :created_by_id, optional: true

  # Validations
  validates :project_id, presence: true
  validates :title, presence: true
  validates :content, presence: true

  def get_content_new_update
    self.content.html_safe
  end
end

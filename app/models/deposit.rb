class Deposit < ApplicationRecord
  include Rails.application.routes.url_helpers
  include AASM

  # Callbacks

  # Associations
  belongs_to :product
  belongs_to :customer, optional: true
  belongs_to :assignee, class_name: 'User',foreign_key: :user_id, optional: true
  has_many :notes, as: :objectable, dependent: :destroy
  accepts_nested_attributes_for :notes, allow_destroy: true

  delegate :project_name, to: :product, allow_nil: true
  delegate :locking_time, to: :product, allow_nil: true
  delegate :code, to: :product, prefix: :product, allow_nil: true

  # Validations
  # Validate require customer when change state from :locked to :request_book
  validates :customer_id, presence: true, if: -> { aasm.current_state == :request_book }
  # Validate require deposit money when change state from :deposited to :booked
  validates :deposit, presence: true, if: -> { aasm.current_state == :booked }
  # Validate require add note when change state to :canceled
  validate :canceled_note_validate, if: -> { aasm.current_state == :canceled && aasm.from_state != aasm.to_state }

  ATTRS = [:state, :name, :customer_id, :product_id, :user_id, :deposit, :total_price_after_discount, :commission, notes_attributes: [:id, :created_by_id, :content, :_destroy]]

  # AASM
  aasm column: :state, whiny_transitions: false do
    after_all_transitions :action_after_transition

    state :request_lock, initial: true   # Chờ lock căn
    state :locked                         # Đã lock
    state :request_book                   # Chờ book căn
    state :deposited                      # Đã đặt cọc
    state :booked                         # Đã book
    state :contract_signed                # Đã ký văn bản
    state :completed                      # Hoàn thành
    state :canceled                       # Đã hủy

    event :request_lock_to_locked do
      transitions from: :request_lock, to: :locked
    end
    event :locked_to_request_book do
      transitions from: :locked, to: :request_book
    end
    event :request_book_to_deposited do
      transitions from: :request_book, to: :deposited
    end
    event :deposited_to_booked do
      transitions from: :deposited, to: :booked
    end
    event :booked_to_contract_signed do
      transitions from: :booked, to: :contract_signed
    end
    event :contract_signed_to_completed do
      transitions from: :contract_signed, to: :completed
    end
    event :cancel do
      transitions from: [:request_lock, :locked, :request_book, :deposited, :booked, :contract_signed, :completed], to: :canceled
    end
  end

  def action_after_transition
    message = case aasm.current_event
    when :request_lock_to_locked
      product.locking_to_locked
      # Notify to author/team lead/sale admin/ sale director
      send_notify_state_change
    when :locked_to_request_book
      # Notify to author/team lead/sale admin/sale director
      send_notify_state_change
    when :request_book_to_deposited
      product.locked_to_deposited
      # Notify to author/team lead/sale admin/sale director
      send_notify_state_change
    when :deposited_to_booked
      product.deposited_to_booked
      # Notify to author/team lead/sale admin/sale director
      send_notify_state_change
    when :booked_to_contract_signed
      # Notify to author/team lead/sale admin/sale director
      send_notify_state_change
    when :contract_signed_to_completed
      product.booked_to_sold
      # Notify to author/team lead/sale admin/sale director
      send_notify_state_change
    when :cancel
      unless product.aasm.to_state == :recall
        if @is_third_holding
          product.to_third_holding
        elsif product.deposits.where.not(state: :canceled).count == 0
          product.to_for_sale
        end
      end
      send_notify_after_cancel
    end
  end

  def action_after_create
    product.for_sale_to_locking
    product.save
    send_notify_after_create
  end

  def send_notify_after_create
    # Notify to sale admin/sale director
    message = "Khách hàng <strong>#{customer&.name}</strong> đã tạo mới đơn hàng <strong>#{product&.code}-#{project_name}</strong> từ website"
    # message = "Sale <strong>#{assignee&.email}</strong> đã tạo mới phiếu lock sản phẩm <strong>#{name}</strong>
    #   và đang <strong>#{I18n.t("deposit.states.#{aasm.current_state}")}</strong>."
    User.current.notifications.create content: message, related_url: edit_admin_deposit_path(self)
  end

  def send_notify_after_cancel
    message = "Phiếu lock <strong>#{name} - KH #{customer&.name}</strong> đã được chuyển sang trạng thái
      <strong>#{I18n.t("deposit.states.#{aasm.to_state}")}</strong> "
    message += if @is_time_out
      "do vượt quá thời gian xử lý."
    elsif @is_third_holding
      "do sản phấm bị <strong>Sàn khác giữ</strong>"
    elsif product.aasm.current_state == :recall
      "do sản phấm bị thu hồi"
    else
      "bởi <strong>#{User.current.email}</strong>"
    end
    # TODO: vì không tìm thấy user khi hủy bằng job
    # User.current.notifications.create content: message, related_url: edit_admin_deposit_path(self)
  end

  def send_notify_state_change
    message = "Phiếu lock <strong>#{name} - KH #{customer&.name}</strong> đã được chuyển sang trạng thái
      <strong>#{I18n.t("deposit.states.#{aasm.to_state}")}</strong> bởi <strong>#{User.current.email}</strong>"
    # TODO: vì không tìm thấy user khi hủy bằng job
    # User.current.notifications.create content: message, related_url: edit_admin_deposit_path(self)
  end

  def next_states
    # TODO: check permission
    Deposit.aasm.events.map do |event|
      transitions = event.instance_variable_get("@transitions")
      {
        from: transitions.map{|t| t.opts[:from]}.flatten.uniq,
        to: transitions.map{|t| t.opts[:to]}.flatten.uniq
      }
    end.map do |state|
      state[:from].include?(aasm.current_state) ? state[:to] : nil
    end.flatten.compact.uniq
  end

  def execute_state_change to_state, is_third_holding: false, is_time_out: false
    @is_third_holding = is_third_holding
    @is_time_out = is_time_out
    return if state == to_state
    cancel and return if to_state.to_sym == :canceled || to_state == :canceled
    event = "#{state}_to_#{to_state}"
    send(event) if respond_to?(event)
  end

  def canceled_note_validate
    errors.add(:notes, :canceled_blank) if notes.last&.is_third_holding && notes.last&.content.blank?
  end
end

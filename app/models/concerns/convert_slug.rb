module ConvertSlug extend ActiveSupport::Concern
  included do
    before_create :create_slug
    before_update :update_slug

    def create_slug
      if self.class.name == 'SaleDocument'
        slug_convert = pre_slug = no_accent_vietnamese(title)
      else
        slug_convert = pre_slug = self.class.name == 'Product' ? no_accent_vietnamese("#{code} #{self.project.name}") : no_accent_vietnamese(name)
      end
      i = 1
      while self.class.exists?(slug: slug_convert)
        slug_convert = "#{pre_slug}-#{i}"
        i += 1
      end
      self.slug = slug_convert
    end

    def update_slug
      if self.class.name == 'Project'
        slug_convert = no_accent_vietnamese(name)
        i = 1
        while self.class.exists?(slug: slug_convert)
          slug_convert = "#{slug_convert}-#{i}"
          i += 1
        end
        self.slug = slug_convert
      end
    end
  end

  def no_accent_vietnamese(s)
    n = s.mb_chars.normalize(:kd).gsub(/\p{Mn}/, '').downcase.to_s.parameterize
  end
end

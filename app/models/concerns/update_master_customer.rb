module UpdateMasterCustomer extend ActiveSupport::Concern
  included do
    after_commit :update_to_master_customer

    def update_to_master_customer
      return unless self.customer
      if (customer.deals.size == 1 && %w(meet refundable_deposit lock confirm book deposit contract_signed completed).include?(self.state)) ||
        self.id == customer.deals.where(state: %w(lock confirm book deposit contract_signed completed)).joins(:deal_log).order(:meet_entered).first&.id

        # Update 
        # Thông tin cơ bản
        mc = MasterCustomer.find_by("#{self.customer_id} = ANY (customer_ids)")
        return unless mc
        # Trường hợp SĐT sau khi sửa chưa có trên DB công ty
        mc.phone_number = customer.phone_number unless MasterCustomer.exists?(phone_number: customer.phone_number)
        mc.customer_ids = (mc.customer_ids || []).push(customer_id).uniq
        mc.attributes = customer.as_json(only: [
          :name, :second_phone_number, :gender, :email, 
          :identity_card, :city_id, :address, :dob, :data_source, 
          :customer_characteristic, :job, :property_ownership, :nationality, 
          :financial_capability, :detail, :income, :position, :work_place, 
          :marital_status, :note, :state, :created_by_id, :country_code, 
          :facebook_uid, :hubspot_contact_id, :deleted_at
        ]).compact
        mc.save

        # Chân dung KH
        return unless self.customer_persona
        master_customer_persona = mc.master_customer_persona || mc.build_master_customer_persona
        master_customer_persona.hobbies = (master_customer_persona.hobbies || []).push(master_customer_persona.hobbies).uniq
        master_customer_persona.real_estate_type_to_invest = (master_customer_persona.real_estate_type_to_invest || []).push(master_customer_persona.real_estate_type_to_invest).uniq
        master_customer_persona.attributes = self.customer_persona.as_json(only: [
          :gender, :identity_card, :domicile, :yob, :nationality, :nation, 
          :workplace, :financial_capability, :customer_position, :property_ownership, 
          :settlements, :city_id, :district_id, :ward_id, :street, :marital_status, :people_in_family, 
          :hobbies, :positions_to_invest, :real_estate_type_to_invest, :deleted_at
        ]).compact
        master_customer_persona.save
      end
    end
  end
end
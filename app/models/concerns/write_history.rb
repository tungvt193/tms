module WriteHistory
  extend ActiveSupport::Concern
  included do
    after_save :create_history

    def create_history
      # if class note
      return if self.respond_to?('objectable_type') && self.send('objectable_type') != 'Deal'

      col_allow_log = %w(content appointment_date result contact_date action_type action_date title state)
      heading = ''
      contents = []
      # heading
      if id_previously_changed?
        heading = "#{Deal.time_history(updated_at) } - " +
          "#{Role::ROLE_SHORT[User.current&.role&.name]} <span class='detail-item-heading'>#{User.current&.full_name}</span> đã tạo #{self.model_name.human} mới với nội dung:"
      else
        heading = "#{Deal.time_history(updated_at) } - " +
          "#{Role::ROLE_SHORT[User.current&.role&.name]} <span class='detail-item-heading'>#{User.current&.full_name}</span> đã cập nhật #{self.model_name.human}:"
      end

      all_columns = self.class.column_names
      col_allow_write = col_allow_log & all_columns
      # self.previous_changes

      col_allow_write.each do |col|
        val_from = nil
        val_to = nil
        if previous_changes.key?(col)
          val_from = previous_changes[col][0]
          val_to = previous_changes[col][1]
        else
          val_to = self.send(col)
        end
        case col
        when 'action_type'
          val_from = Constant::ONLINE_FEEDBACKS[val_from] if val_from
          val_to = Constant::ONLINE_FEEDBACKS[val_to] if val_to
        when 'appointment_date', 'contact_date', 'action_date'
          val_from = time_history(val_from) if val_from
          val_to = time_history(val_to) if val_to
        when 'state'
          val_from = Constant::APPOINTMENT_STATE[val_from] if val_from
          val_to = Constant::APPOINTMENT_STATE[val_to] if val_to
        end

        val_from = val_from.present? ? val_from : 'Chưa cập nhật'
        val_to = val_to.present? ? val_to : 'Chưa cập nhật'
        if previous_changes.key?(col) && !id_previously_changed?
          contents.push("#{self.class.human_attribute_name(col)}: #{val_from} -> #{val_to}")
        else
          contents.push("#{self.class.human_attribute_name(col)}: #{val_to}")
        end
      end

      self.deal.histories.create(content: render_content(heading, contents))
    end
  end

  def time_history(time)
    # time.localtime.strftime Settings.date.formats.strftime_time_history
    time&.localtime&.strftime Settings.date.formats.strftime_time_history
  end

  def render_content heading, contents = []
    ApplicationController.renderer.render partial: "admin/histories/history_log",
                                          locals: { heading: heading, contents: contents }
  end
end

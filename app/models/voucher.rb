class Voucher < ApplicationRecord

  has_many :vouchers_projects, :class_name => 'VouchersProjects'
  has_many :projects, through: :vouchers_projects
  has_many :vouchers_customers, :class_name => 'VouchersCustomers'
  has_many :customers, through: :vouchers_customers

  def get_projects
    if self.project_id.length == Project.all.length
      'Tất cả'
    else
      Project.where(id: self.project_id).pluck(:name).join(',')
    end
  end

end
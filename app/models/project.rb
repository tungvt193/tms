class Project < ApplicationRecord
  include ProjectSearch
  include ConvertSlug
  include ActiveModel::Validations

  # Soft Delete
  acts_as_paranoid

  # Attributes Accessor
  attr_accessor :not_required_image

  # Uploader
  mount_uploaders :images, ProjectImageUploader
  mount_uploaders :internal_utility_images, ProjectImageUploader
  mount_uploaders :external_utility_images, ProjectImageUploader
  mount_uploaders :design_style_images, ProjectImageUploader

  # Associations
  has_many :milestones, inverse_of: :project, dependent: :destroy
  has_many :products, dependent: :destroy
  has_many :layouts, dependent: :destroy
  has_many :divisions, dependent: :destroy
  accepts_nested_attributes_for :milestones, allow_destroy: true
  has_many :legal_documents, inverse_of: :project, dependent: :destroy
  accepts_nested_attributes_for :legal_documents, allow_destroy: true
  has_one :region, as: :objectable, dependent: :destroy
  accepts_nested_attributes_for :region, allow_destroy: true
  has_many :payment_schedules, inverse_of: :project, dependent: :destroy
  accepts_nested_attributes_for :payment_schedules, allow_destroy: true
  has_many :customer_tickets, as: :objectable, dependent: :destroy
  has_many :interactive_layouts, inverse_of: :project, dependent: :destroy
  accepts_nested_attributes_for :interactive_layouts, allow_destroy: true
  has_many :regulations
  has_many :deals
  has_many :key_values, inverse_of: :project, dependent: :destroy
  accepts_nested_attributes_for :key_values, allow_destroy: true
  has_many :floorplan_images, inverse_of: :project, dependent: :destroy
  accepts_nested_attributes_for :floorplan_images, allow_destroy: true
  has_many :documents, inverse_of: :project, dependent: :destroy
  accepts_nested_attributes_for :documents, allow_destroy: true
  has_many :project_new_updates
  has_many :company_revenues, as: :objectable, dependent: :destroy, inverse_of: :objectable
  accepts_nested_attributes_for :company_revenues, reject_if: :all_blank, allow_destroy: true
  has_many :sale_documents, dependent: :destroy, inverse_of: :project
  accepts_nested_attributes_for :sale_documents, allow_destroy: true
  has_many :investor_posts, dependent: :destroy, inverse_of: :project
  accepts_nested_attributes_for :investor_posts, reject_if: :all_blank, allow_destroy: true
  has_many :vouchers_projects, :class_name => 'VouchersProjects'
  has_many :vouchers, through: :vouchers_projects
  has_many :product_lock_histories, inverse_of: :project
  has_many :campaign_locks, inverse_of: :project

  # Callbacks
  after_update :update_deal_cached_at
  after_create -> { sync_data_to_google_sheet('create_project') }
  after_update -> { sync_data_to_google_sheet('update_project') }
  after_destroy -> { sync_data_to_google_sheet('destroy_project') }
  after_create :push_notification_create_project
  after_update :update_deal_name

  # Validations
  validates :name, presence: true
  validates :real_estate_type, presence: true
  validates :locking_time, numericality: { allow_blank: true, only_integer: true, greater_than_or_equal_to: 0 }
  validates :construction_density, numericality: { greater_than: 0, less_than_or_equal_to: 100 }, allow_blank: true
  validates :total_area, numericality: { greater_than: 0 }, allow_blank: true
  validates :investors, presence: true
  validates :internal_utilities, presence: true
  validates :ownership_period, presence: true
  validates :loan_percentage_support, numericality: { greater_than: 0, less_than_or_equal_to: 100 }, allow_blank: true
  validates :loan_support_period, numericality: { only_integer: true, greater_than: 0 }, allow_blank: true
  validates :discount_support, numericality: { greater_than: 0, less_than_or_equal_to: 100 }, allow_blank: true
  validates :images, presence: true, unless: -> { not_required_image.present? }
  validates :high_level_number, numericality: { only_integer: true, greater_than: 0 }, allow_blank: true
  validates :low_level_number, numericality: { only_integer: true, greater_than: 0 }, allow_blank: true
  validate :payment_schedule_validate
  validates :price_from, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 1000 }, allow_blank: true
  validates :price_to, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 1000 }, allow_blank: true
  validates :area_from, numericality: { greater_than_or_equal_to: 0 }, allow_blank: true
  validates :area_to, numericality: { greater_than_or_equal_to: 0 }, allow_blank: true
  validate :interactive_layout_validate
  validates :link_video, :position_link_embed, valid_url: true
  validates :image_folder_url, valid_url: true, allow_blank: true
  validates :video_folder_url, valid_url: true, allow_blank: true
  validates :layout_url, valid_url: true, allow_blank: true
  # validates :queue_lock, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  # validates :unc_confirmation_period_for_sale, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  # validates :unc_confirmation_period_for_sale_admin, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  # validates :confirm_deposit_by_sale_admin, numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  scope :by_ids, -> (ids) { where id: ids }

  def is_stop_selling?
    labels.include?(3)
  end

  def update_sale_policy!(params, labels)
    pre_labels = self.labels
    
    ActiveRecord::Base.transaction do
      self.assign_attributes(
        sale_policy: params[:project][:sale_policy],
        labels: labels,
        description_for_seller: params[:project][:description_for_seller]
      )
      self.save(validate: false)

      if self.is_stop_selling?
        self.products.for_sale.update_all(state: :out_of_stock)
        product_ids = self.products.out_of_stock.pluck(:id)
        self.deals.where(state: %i(pending interest meet refundable_deposit), product_id: product_ids).update_all(product_id: nil)
      elsif pre_labels.include?(3)
        self.products.out_of_stock.update_all(state: :for_sale)
      end

      Product.reindex
    end

    NotificationService.notify_stop_selling_project(self) if self.is_stop_selling? && !pre_labels.include?(3)
  end

  def update_deal_cached_at
    self.deals.update_all(cached_at: Time.now)
  end

  def min_price
    products.minimum(:sum_price) || price_from ? price_from.to_f * 1000000000 : nil
  end

  def max_price
    products.maximum(:sum_price) || price_to ? price_to.to_f * 1000000000 : nil
  end

  def min_price_filter
    products.present? ? products.minimum(:sum_price) : price_from.to_f * 1000000000
  end

  def max_price_filter
    products.present? ? products.maximum(:sum_price) : price_to.to_f * 1000000000
  end

  def min_acreage
    [products.minimum(:carpet_area), products.minimum(:plot_area)].compact.min || area_from
  end

  def max_acreage
    [products.maximum(:carpet_area), products.maximum(:plot_area)].compact.max || area_to
  end

  def min_acreage_filter
    [products.minimum(:carpet_area), products.minimum(:plot_area)].compact.min || area_from.to_f
  end

  def max_acreage_filter
    [products.maximum(:carpet_area), products.maximum(:plot_area)].compact.max || area_to.to_f
  end

  def furnitures
    products&.map(&:furniture).compact.uniq
  end

  def online_transaction
    products.present?
  end

  def investors_name
    Investor.where(id: investors).map(&:name)
  end

  def real_estate_type_name
    real_estate_type&.map { |t| Constant::PROJECT_REAL_ESTATE_TYPE[t] }
  end

  def full_address
    "#{region&.city&.name} #{region&.district&.name} #{region&.ward&.name}"
  end

  def currencies
    products&.map(&:currency).compact.uniq.map { |c| Reti::Product::CURRENCIES[c] }
  end

  def payment_schedule_validate
    high_payment_schedules = payment_schedules.reject { |p| p._destroy || p.label_schedule != 0 }
    low_payment_schedules = payment_schedules.reject { |p| p._destroy || p.label_schedule != 1 }
    high = high_payment_schedules.size
    low = low_payment_schedules.size
    value_high = high_payment_schedules.map(&:pay).sum()
    value_low = low_payment_schedules.map(&:pay).sum()
    # check khoi cao tang
    errors.add(:high_level_number, :equal_to_payment_schedule) unless high == high_level_number.to_i
    errors.add(:high_level_number, :equal_to_value_payment_schedule) if high_level_number.to_i > 0 && high > 0 && value_high != 100
    # check khoi thap tang
    errors.add(:low_level_number, :equal_to_payment_schedule) unless low == low_level_number.to_i
    errors.add(:low_level_number, :equal_to_value_payment_schedule) if low_level_number.to_i > 0 && low > 0 && value_low != 100
  end

  def interactive_layout_validate
    errors.add(:divis, "Bạn đang nhập 1 mặt bằng 2 lần") if interactive_layouts.map(&:division_id).uniq.size != interactive_layouts.map(&:division_id).size
  end

  def get_regions
    get_regions = []
    if region.present?
      if region.ward
        get_regions << region.ward.name_with_type
      end
      if region.district
        get_regions << region.district.name_with_type
      end
      if region.city
        get_regions << region.city.name_with_type
      end
    end
    get_regions.join(', ')
  end

  def get_handover_date
    handover_date = milestones.where(event: 'Dự kiến bàn giao', is_default: true).first
    handover_date.present? && handover_date.event_time.present? ? handover_date.event_time.strftime('%d/%m/%Y') : 'Đang cập nhật'
  end

  def get_number_division
    products.present? ? divisions.map(&:childrens).flatten.count : 'Đang cập nhật'
  end

  def get_detail_level
    products.present? ? products.pluck(:level).uniq.map { |a| Reti::Product::LEVELS[a] }.join(', ') : 'Đang cập nhật'
  end

  def sync_data_to_google_sheet(event)
    SyncProjectToGoogleSheetJob.perform_later(self.id, event)
  end

  def count_deals
    deals.size
  end

  def push_notification_create_project
    NotificationJob.perform_later('create_project', self)
  end

  def update_deal_name
    if saved_change_to_name?
      self.deals.each do |deal|
        new_name = "#{deal&.customer&.name&.titleize_name} - #{deal&.project&.name&.titleize_name}"
        deal.update_column(:name, new_name)
      end
      Deal.reindex
    end
  end
end

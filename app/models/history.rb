class History < ApplicationRecord
  acts_as_paranoid

  belongs_to :deal

  validates :content, presence: true
end

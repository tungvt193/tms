class Group < ApplicationRecord

  # Include
  include GroupSearch

  # Validations
  validates :name, presence: true
  validates :city_ids, presence: true
  validates :director_id, presence: true
  belongs_to :supervisor, class_name: 'User', foreign_key: :supervisor_id, optional: true
  after_save :update_hierarchy, if: -> { saved_change_to_supervisor_id? || saved_change_to_director_id? }

  # Scopes
  scope :by_ids, -> (ids) { where id: ids }

  def director
    User.find_by(id: director_id)
  end

  def staffs
    User.where(id: user_ids)
  end

  def cities
    RegionData.where(id: city_ids).pluck(:name).join(', ')
  end

  def members
    user_ids.push(director_id)
  end

  def update_hierarchy
    ActiveRecord::Base.transaction do
      old_director = User.find_by(id: director_id_before_last_save) if saved_change_to_director_id?
      if old_director&.children.present?
        ancestry = director.root? ? "#{director.id}" : "#{director.ancestry}/#{director.id}"
        old_director.children.each do |child|
          child.update_column(:ancestry, ancestry)
        end
      end
    end
  end
end

class Popup < ApplicationRecord
  enum group: [:subscribe, :guide_tour]
  enum position: [:home, :project_list, :project_detail, :product_detail]

  has_many :steps, -> { order('name ASC') }, foreign_key: "popup_id", class_name: "PopupStep"
  accepts_nested_attributes_for :steps, allow_destroy: true

  validate :uniqueness_of_steps

  ATTRS = [:group, :position, :active, steps_attributes: [:id, :name, :content, :_destroy]]

  after_update :change_active_guide_tour

  private
  def change_active_guide_tour
    if active
      Popup.where.not(group: group).find_by(position: position)&.update_columns(active: false)
    end
  end

  def uniqueness_of_steps
    hash = {}

    steps.each do |step|
      if step.name && hash[step.name]
        if errors[:"step.name"].blank?
          errors.add(:"step.name", "duplicate error")
        end

        step.errors.add(:name, "Bước #{step.name} đã tồn tại!")
      end

      hash[step.name] = true
    end
  end
end

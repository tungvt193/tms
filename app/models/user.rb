class User < ApplicationRecord
  # Include default devise modules.
  devise :database_authenticatable, :trackable, :recoverable, :rememberable, :registerable,
         authentication_keys: [:username]
  include DeviseTokenAuth::Concerns::User

  include ActiveModel::Validations
  include UserSearch
  extend Devise::Models

  attr_accessor :skip_gender_validation
  attr_accessor :is_me_page
  attr_accessor :username

  devise :omniauthable, :omniauth_providers => [:google_oauth2]

  has_ancestry orphan_strategy: :restrict

  enum account_type: [:internal, :agent, :f2]
  enum gender: [:female, :male]
  mount_uploader :avatar, LogoUploader

  # Callbacks
  after_update :update_deal_cached_at
  before_create :set_code, :set_uid
  after_create :create_team
  after_create :send_account_info
  before_validation :format_phone_number, if: Proc.new { |u| u.phone_number? }

  # Associations
  has_many :notes, class_name: 'Note'
  has_many :notifications
  has_many :reminders
  has_many :deal_sorts
  has_many :deals, foreign_key: :assignee_id
  belongs_to :role, optional: true
  has_one :facebook_api
  has_many :appointments, foreign_key: :created_by_id
  has_one :team, required: false
  has_many :wish_lists
  has_many :favourite_products, through: :wish_lists, :source => :objectable,
           :source_type => "Product"
  has_many :customers, foreign_key: :created_by_id
  belongs_to :city, class_name: 'RegionData', foreign_key: :city_id, optional: true
  belongs_to :creator, class_name: 'User', foreign_key: :created_by_id, optional: true
  has_many :device_tokens, dependent: :destroy
  has_many :referral_histories, dependent: :destroy
  has_many :supervisor_groups, class_name: 'Group', foreign_key: :supervisor_id

  # Validations
  validates :password, presence: true,
            confirmation: true,
            length: { within: 6..20 },
            on: :create,
            unless: -> { User.current.super_admin? }
  validates :password, confirmation: true,
            length: { within: 6..20 },
            allow_blank: true,
            on: :update
  validates :email, presence: true, if: -> { (internal? || f2?) && !User.current.super_admin? }
  validates :email, uniqueness: true, format: { with: /\A[^@\s]+@[^@\s]+[@.]+[^@\s]+\z/ }, if: -> { email.present? }
  validates :phone_number, presence: true, on: :create, if: -> { (agent? || f2?) && !User.current.super_admin? }
  validates :full_name, presence: true, format: { with: /\s/ }
  validates :account_type, presence: true
  validates :role_id, presence: true
  validates :gender, presence: true, unless: -> { skip_gender_validation || User.current.super_admin? || f2?}
  validate :check_phone_valid, unless: -> { is_me_page || User.current.super_admin? }

  scope :by_ids, -> (ids) { where id: ids }

  def update_deal_cached_at
    self.deals.update_all(cached_at: Time.now)
  end

  def create_team
    Team.create(user_id: self.id, name: self.full_name)
  end

  def self.from_omniauth(access_token)
    data = access_token.info
    # user = User.where(email: data['email']).first
    user = User.current
    unless user
      user = User.create(
        full_name: data['name'],
        email: data['email'],
        # password: Devise.friendly_token[0,20]
        password: 123456789,
        confirmed_at: Time.now,
        account_type: :internal
      )
    end
    user
  end

  def username
    @username || self.phone_number || self.email
  end

  def email_required?
    false
  end

  def email_changed?
    false
  end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if (username = conditions.delete(:username))
      if username[0] == '0'
        username = username.sub(username[0], '84')
      end
      where(conditions.to_h).where(["lower(phone_number) = :value OR lower(email) = :value", { :value => username.downcase }]).first
    elsif conditions.has_key?(:phone_number) || conditions.has_key?(:email)
      where(conditions.to_h).first
    end
  end

  def expired?
    expires_at < Time.current.to_i
  end

  def active_for_authentication?
    # TODO: solution Google Calendar
    # super && !deactivated
    !deactivated
  end

  def status
    deactivated? ? 'Vô hiệu hóa' : 'Hoạt động'
  end

  def send_password_reset
    raw, enc = Devise.token_generator.generate(self.class, :reset_password_token)
    self.reset_password_token = enc
    self.reset_password_sent_at = Time.now.utc
    save(validate: false)
    raw
  end

  def super_admin?
    self.is_superadmin === true || false
  end

  def sale_leader?
    ['Trưởng nhóm kinh doanh'].include? self.role.name.strip
  end

  def sale_manager?
    ['Trưởng phòng kinh doanh'].include? self.role.name.strip
  end

  def sale_member?
    ['Nhân viên kinh doanh'].include? self.role.name.strip
  end

  def sale_admin?
    ['Sale Admin'].include? self.role.name.strip
  end

  def sale_director?
    ['GĐCN/GĐKD'].include? self.role.name.strip
  end

  def accounting?
    ['Kế toán viên'].include? self.role.name.strip
  end

  def leader_accounting?
    ['Kế toán trưởng'].include? self.role.name.strip
  end

  def marketing?
    ['Marketing'].include? self.role.name.strip
  end

  def bi_ba?
    ['BI/BA'].include? self.role.name.strip
  end

  def customer_service?
    self.role.name.strip.downcase.include?('cskh')
  end

  def director?
    ['Giám đốc, BOD'].include? self.role.name.strip
  end

  def is_sale?
    sale_leader? || sale_member? || sale_manager? || sale_director?
  end

  def self.current
    Thread.current[:user]
  end

  def director
    self.ancestors.joins(:role).where(roles: { name: ['GĐCN/GĐKD'] })&.first
  end

  def leader
    self.ancestors.joins(:role).where(roles: { name: ['Trưởng nhóm kinh doanh'] })&.first
  end

  def sale_manager
    self.ancestors.joins(:role).where(roles: { name: ['Trưởng phòng kinh doanh'] })&.first
  end

  def leaders
    self.ancestors.joins(:role).where(roles: { name: ['Trưởng nhóm kinh doanh'] })
  end

  def managers
    self.ancestors.joins(:role).where(roles: { name: ['Trưởng phòng kinh doanh'] })
  end

  def accountant
    self.root.descendants.joins(:role).where(roles: { name: ['Kế toán'] })&.first
  end

  def leader_accountant
    self.root.descendants.joins(:role).where(roles: { name: ['Leader Kế toán'] })&.first
  end

  def sale_admin
    self.root.descendants.joins(:role).where(roles: { name: ['Sale Admin'] })&.first
  end

  def self.sale_managers
    joins(:role).where(roles: { name: ['Trưởng phòng kinh doanh'] })
  end

  def self.sale_admins
    joins(:role).where(roles: { name: ['Sale Admin'] })
  end

  def self.leaders
    joins(:role).where(roles: { name: ['Trưởng nhóm kinh doanh'] })
  end

  def sale_member_descendants
    members.joins(:role).where(roles: { name: ['Nhân viên kinh doanh'] })
  end

  def sale_leader_descendants
    members.joins(:role).where(roles: { name: ['Trưởng nhóm kinh doanh'] })
  end

  def self.directors
    internal.joins(:role).where(roles: { name: ['BOD', 'GĐCN/GĐKD', 'Giám đốc'] })
  end

  def self.supervisors
    internal.joins(:role).where(roles: { name: ['Giám sát', 'BOD', 'Giám đốc'] })
  end

  def self.available_add_to_group
    added_to_group = Group.pluck(:user_ids).flatten.uniq
    staffs = internal.joins(:role).where.not(roles: { name: ['BOD', 'GĐCN/GĐKD', 'Giám đốc'] }).pluck(:id)
    where(id: staffs - added_to_group)
  end

  def members
    self.descendants
  end

  def self.current=(user)
    Thread.current[:user] = user
  end

  def get_permission(model_name)
    role = self.role
    permission_name = Permission::LIST_MODEL_NAME.key(model_name)
    return false unless role.present? && permission_name.present?
    permission = role.permissions.find_by_name(permission_name)
  end

  def check_permission(model_name, permission_type)
    return true if super_admin?
    role = self.role
    permission_name = Permission::LIST_MODEL_NAME.key(model_name)
    return false unless role.present? && permission_name.present?
    permission = role.permissions.find_by_name(permission_name)
    return false unless permission.present? && permission.respond_to?(permission_type)
    permission.send(permission_type)
  end

  def check_permission_index(model_name)
    return true if super_admin?
    role = self.role
    permission_name = Permission::LIST_MODEL_NAME.key(model_name)
    return false if role.blank? || permission_name.blank?
    permission = role.permissions.find_by_name(permission_name)
    return false if permission.blank?
    permission.can_create || permission.can_edit || permission.can_update || permission.can_destroy || permission.change_state || permission.can_update_price || permission.can_assign
  end

  def prepare_data_org_chart(group)
    user_ids = group.user_ids
    children = []
    self.children.by_ids(user_ids).each do |child|
      obj = {
        'name': child.full_name,
        'email': child.email,
        'user_id': child.id,
        'children': child.prepare_data_org_chart(group)
      }
      children.push(obj)
    end
    children
  end

  def get_avatar
    self.avatar? ? self.avatar.thumb.url : ActionController::Base.helpers.image_path("user_default.png")
  end

  def unsubscribe
    self.update_columns({
                          auth_key: nil,
                          endpoint: nil,
                          p256dh_key: nil,
                          subscribe: false
                        })
  end

  def select2_search_group_result
    self.members.joins(:role).where(roles: { name: ['Trưởng nhóm kinh doanh'] }).map { |leader| { id: leader.id, text: leader.full_name } }
  end

  def send_account_info
    if email.present?
      if self.is_sale?
        UserMailer.send_account_retizy_info_after_create(self, password).deliver
      else
        UserMailer.send_account_info_after_create(self, password).deliver
      end
    end
    if phone_number.present?
      ZaloService.send_sms_account_info(self, password)
    end
  end

  def get_deals
    Pundit.policy_scope(self, Deal)
  end

  def create_new_referral!(referred)
    referral_histories.create!(referred_id: referred.id, referred_date: Time.zone.now)
  end

  def self.generate_referral_code
    loop do
      referral_code = ReferralCode.generate
      next if self.find_by(referral_code: referral_code)

      return referral_code
    end
  end

  private

  def get_stored_label(label_list)
    return [] if label_list.blank?
    label_list.map do |l|
      label_id, label_type = l.split('-')
      if label_type == 'contact'
        { id: l, text: Constant::CONTACT_STATUSES[label_id.to_i] }
      else
        nil
      end
    end.compact
  end

  def set_code
    self.code = email.split("@")[0]&.upcase
    self.referral_code = User.generate_referral_code
  end

  def format_phone_number
    self.phone_number = Phonelib.parse(self.phone_number, self.country_code).full_e164.sub('+', '')
  end

  def check_phone_valid
    errors.add(:phone_number, :invalid) if !Phonelib.parse(self.phone_number, self.country_code).valid? && self.phone_number.present?
    errors.add(:phone_number, :blank) if self.agent? && self.phone_number.blank?
    errors.add(:phone_number, :taken) if self.phone_number.present? && User.where.not(id: self.id).where(phone_number: self.phone_number).present?
    errors.add(:phone_number, :has_changed) if self.phone_number_changed? && self.phone_number_change.first != nil && !self.new_record?
  end

  def set_uid
    if account_type == 'internal'
      if email.present?
        provider = 'email'
        uid = email
      else
        provider = 'phone_number'
        uid = phone_number
      end
    else
      if phone_number.present?
        provider = 'phone_number'
        uid = phone_number
      else
        provider = 'email'
        uid = email
      end
    end
    self.uid = uid
    self.provider = provider
  end
end

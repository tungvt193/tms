class Call < ApplicationRecord
  include WriteHistory
  acts_as_paranoid

  belongs_to :author, class_name: 'User', foreign_key: :created_by_id, required: false
  belongs_to :deal
  attr_accessor :quick_create

  validates :contact_date, presence: true
  validates :result, presence: true, if: -> { !new_record? || self.quick_create }
end

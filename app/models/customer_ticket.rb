class CustomerTicket < ApplicationRecord
  include CustomerTicketSearch
  include AASM
  # Associations
  belongs_to :objectable, polymorphic: true, optional: true
  belongs_to :city, :class_name => 'RegionData', optional: true, foreign_key: :city_id
  has_many :notes, as: :objectable, dependent: :destroy
  accepts_nested_attributes_for :notes, allow_destroy: true

  validates :state, :ticket_type, presence: true
  validates :name, presence: true, format: { with: /\s/ }
  validates :phone, presence: true, format: { with: /\A([+]\w|)+[0-9]*$\z/ }
  validates :email, allow_blank: true, format: { with: /\A^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$\z/ }
  # AASM
  aasm column: :state, whiny_transitions: false do
    state :pending, initial: true     # Chờ xử lý
    state :processing                 # Đang xử lý
    state :completed                  # Đã hoàn thành
  end

  before_save :remove_objectable_when_type_0

  def get_objectable_name
    case objectable_type
    when "Project"
      objectable ? objectable.name : ''
    when "Product"
      objectable ? "Căn hộ  #{objectable.code} -  #{objectable.project.name}" : ''
    else
      ''  
    end
  end

  private
    def remove_objectable_when_type_0
      if ticket_type == 0
        self.objectable = nil
      end
    end
end

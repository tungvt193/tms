class CustomHtml < ApplicationRecord
  ATTRS = [:content, :position]

  validates :position, presence: true, uniqueness: true
end

class TransferHistory < ApplicationRecord
  belongs_to :deal

  scope :by_deal, ->(deal_id) { where('deal_id = ? OR transferred_id = ?', deal_id, deal_id) }
end

class InteractiveLayout < ApplicationRecord
  require 'mini_magick'
  serialize :positions, Hash
  mount_uploader :image, LayoutUploader

  # Associations
  belongs_to :project, optional: true, touch: true
  belongs_to :division, optional: true
  validates :division_id, uniqueness: { scope: :project_id }
  validates :image, presence: true

  after_save :set_positions
  before_create :set_show_division

  def set_positions
    positions = {}
    image = MiniMagick::Image.open(self.image.url)
    svg = Nokogiri::XML(open(self.image.url) {|f| f.read })
    svg.css('path').each do |path|
      division = {}
      path.attributes.each do |p_attr|
        if p_attr.last.name == 'id'
          division[:id] = p_attr.last.value.to_i
        end
        if p_attr.last.name == 'd'
          layer = find_bounding_box(image[:width], image[:height], p_attr.last.value)
          division[:x] = (layer[:x].to_f + layer[:width].to_f/2).to_f/image[:width]
          division[:y] = (layer[:y].to_f + layer[:height].to_f/2).to_f/image[:height]
        end
      end
      positions[division[:id]] = division
    end
    positions[:width] = image[:width]
    positions[:height] = image[:height]
    self.update_columns positions: positions
  end

  private
    def find_bounding_box(width, height, path)
      drawing = Magick::Draw.new
      canvas =  Magick::Image.new(width,height) {self.background_color = 'white' }
      drawing.path path
      drawing.draw canvas
      canvas.trim!   
      { :x=> canvas.page.x, :y=>canvas.page.y, :width=> canvas.columns, :height=> canvas.rows}
    end

    def set_show_division
      if division_id.blank?
        self.show_division = true
      end
    end

end

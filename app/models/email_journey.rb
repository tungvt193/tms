class EmailJourney < ApplicationRecord
  enum email_type: [:sent, :delivered, :open, :click]

  belongs_to :customer

  validates :preview, :email_type ,presence: true
end

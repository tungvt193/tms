class PopupStep < ApplicationRecord
  belongs_to :popup, touch: true

  validates :name, :content, presence: true
end

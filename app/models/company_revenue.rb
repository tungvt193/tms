class CompanyRevenue < ApplicationRecord

  attr_accessor :pseudo_id
  # Associations
  belongs_to :objectable, polymorphic: true, touch: true

  # Validate
  validates :objectable_id, :objectable_type, :percentage, presence: true
  validates :objectable_id, uniqueness: true, if: -> { objectable_type == 'Product' }
  validates :real_estate_type, presence: true, if: -> { objectable_type == 'Project' }
  validate :check_date_range

  # Callbacks
  after_commit :update_deal_commission

  private
  
  def check_date_range
    if objectable_type == 'Project'
      if start_date.blank? || end_date.blank?
        errors.add(:apply_period, "Khoảng thời gian không được để trống")
      else
        CompanyRevenue.where(objectable_id: objectable_id, objectable_type: objectable_type, real_estate_type: real_estate_type).where.not(id: id).each do |company_revenue|
          if (company_revenue.start_date <= start_date && company_revenue.end_date >= start_date) || (company_revenue.start_date <= end_date && company_revenue.end_date >= end_date)
            errors.add(:apply_period, "Khoảng thời gian bị trùng")
          end
        end
      end
    end
  end

  def update_deal_commission
    if objectable_type == 'Product'
      deals = objectable.deals.contract_signed
    else
      deals = objectable.deals.joins(:product).contract_signed.where(products: {real_estate_type: self.real_estate_type})
    end
    deals.each do |deal|
      deal.is_update_comm =  true
      deal.save
    end
  end
end

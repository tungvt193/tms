class Product < ApplicationRecord

  # Include
  include Rails.application.routes.url_helpers
  include ProductSearch
  include AASM
  include ConvertSlug

  # Soft Delete
  acts_as_paranoid

  enum source: {
    'Công ty' => 0,
    'Khác' => 1
  }

  enum label: {
    'Độc quyền' => 0,
    'CĐT' => 1
  }

  # Attributes Accessor

  # Uploader
  mount_uploaders :images, ProductImageUploader

  # Constants
  HIGH_ATTRS = [:state, :project_id, :code, :subdivision_id, :block_id, :floor_id, :name, :label,
                :level, :real_estate_type, :product_type, :direction, :built_up_area, :carpet_area,
                :currency, :price, :sum_price, :deposit, :certificate, :use_term, :bedroom, :furniture, :furniture_quality, { images: [] }]
  LOW_ATTRS = [:state, :project_id, :code, :subdivision_id, :block_id, :name, :level, :label,
               :real_estate_type, :product_type, :direction, :plot_area, :density, :floor_area, :setback_front,
               :setback_behind, :statics, :living_room, :bedroom, :bath_room, :dining_room, :multipurpose_room,
               :mini_bar, :drying_yard, :kitchen, :balcony, :business_space, :currency, :price, :sum_price, :deposit,
               :certificate, :use_term, :handover_standards, { images: [] }, :images_cache]
  HIGH_ATTRS_NOT_STATE = [:project_id, :code, :subdivision_id, :block_id, :floor_id, :name, :label,
                          :level, :real_estate_type, :product_type, :direction, :built_up_area, :carpet_area,
                          :currency, :price, :sum_price, :certificate, :use_term, :bedroom, :furniture, :furniture_quality, { images: [] }, :images_cache]
  LOW_ATTRS_NOT_STATE = [:project_id, :code, :subdivision_id, :block_id, :name, :level, :label,
                         :real_estate_type, :product_type, :direction, :plot_area, :density, :floor_area, :setback_front,
                         :setback_behind, :statics, :living_room, :bedroom, :bath_room, :dining_room, :multipurpose_room,
                         :mini_bar, :drying_yard, :kitchen, :balcony, :business_space, :currency, :price, :sum_price,
                         :certificate, :use_term, :handover_standards, { images: [] }]

  # Delegates
  delegate :name, to: :project, prefix: :project, allow_nil: true
  delegate :locking_time, to: :project, allow_nil: true

  # Scopes
  scope :by_ids, -> (ids) { where id: ids }
  scope :without_state, ->(state) { where.not(state: state) }

  # Callbacks
  after_commit :stream_product
  after_destroy :delete_division_nil
  after_save :after_save_action

  # Associations
  belongs_to :project, touch: true
  belongs_to :subdivision, class_name: 'Division', foreign_key: :subdivision_id
  belongs_to :block, class_name: 'Division', foreign_key: :block_id
  belongs_to :floor, class_name: 'Division', foreign_key: :floor_id, optional: true
  has_many :deposits, inverse_of: :product
  has_many :customer_tickets, as: :objectable, dependent: :destroy
  has_many :deals
  has_one :company_revenue, as: :objectable, dependent: :destroy, inverse_of: :objectable
  accepts_nested_attributes_for :company_revenue, reject_if: :all_blank, allow_destroy: true
  has_many :wish_lists, as: :objectable, dependent: :destroy, inverse_of: :objectable
  has_many :product_lock_histories, inverse_of: :product

  # Validations
  # validates :code, presence: true, uniqueness: true
  validates :code, presence: true, uniqueness: { scope: :project_id }
  validates :name, presence: true
  validates :state, presence: true
  validates :project_id, presence: true
  validates :subdivision_id, presence: true
  validates :block_id, presence: true
  validates :level, presence: true
  validates :real_estate_type, presence: true
  # validates :product_type, presence: true
  validates :direction, presence: true
  validates :currency, presence: true
  validates :price, presence: true
  validates :price, numericality: true, allow_blank: true
  validates :sum_price, presence: true
  validates :sum_price, numericality: true, allow_blank: true
  validates :certificate, presence: true
  validates :use_term, presence: true
  validates :floor_id, presence: true, if: -> { level == 0 }
  validates :carpet_area, presence: true, if: -> { level == 0 }
  # validates :furniture, presence: true, if: -> { level == 0 }
  validates :plot_area, presence: true, if: -> { level == 1 }
  # validates :density, presence: true, if: -> { level == 1 }
  # validates :handover_standards, presence: true, if: -> { level == 1 }
  validates :label, presence: true
  validate :validation_action

  # AASM
  aasm column: :state, whiny_transitions: false do
    state :for_sale, initial: true # Còn hàng
    state :locking # Đang lock
    state :deposited # Đã đặt cọc
    state :sold # Đã bán
    state :recall # Thu hồi
    state :third_holding # Sàn khác giữ
    state :third_sold # Sàn khác bán
    state :out_of_stock # Hết hàng

    event :to_for_sale do
      transitions from: [:recall, :third_holding], to: :for_sale
    end
    event :to_recall do
      transitions from: [:for_sale, :locking, :deposited, :third_holding], to: :recall
    end
    event :to_third_holding do
      transitions from: [:for_sale, :locking, :deposited, :recall], to: :third_holding
    end
    event :to_third_sold do
      transitions from: [:for_sale, :locking, :deposited, :recall, :third_holding], to: :third_sold
    end
  end

  # ================================================== #

  def stream_product
    state_text_website = {
      'for_sale' => 'Còn hàng',
      'locking' => 'Đã giữ',
      'deposited' => 'Đã giữ',
      'sold' => 'Đã bán',
      'recall' => 'Chưa ra hàng',
      'third_holding' => 'Đã giữ',
      'locking_display' => 'Đã giữ',
      'third_sold' => 'Đã bán',
      'out_of_stock' => 'Hết hàng'
    }

    ActionCable.server.broadcast "stream_product_channel",
                                 {
                                   id: id,
                                   slug: slug,
                                   code: code,
                                   state: state,
                                   state_label: state_text_website[state],
                                   acreage: helpers.acreage(acreage),
                                   direction: Reti::Product::DIRECTIONS[direction],
                                   bedroom: product_type,
                                   price: helpers.currency(price),
                                   sum_price: helpers.currency(sum_price),
                                   available_products: 100,
                                 }
  end

  def next_states
    # TODO: check permission
    Product.aasm.events.map do |event|
      transitions = event.instance_variable_get("@transitions")
      {
        from: transitions.map { |t| t.opts[:from] }.flatten.uniq,
        to: transitions.map { |t| t.opts[:to] }.flatten.uniq
      }
    end.map do |state|
      state[:from].include?(aasm.current_state) ? state[:to] : nil
    end.flatten.compact.uniq
  end

  def execute_state_change(to_state)
    return if state == to_state
    event = if [:for_sale, :recall, :third_holding, :third_sold].include? to_state.to_sym
              "to_#{to_state}"
            else
              "#{state}_to_#{to_state}"
            end
    send(event) if respond_to?(event)
  end

  def acreage
    level == 0 ? carpet_area : plot_area
  end

  def get_real_estate_type
    real_estate_types = []
    Constant::PROJECT_REAL_ESTATE_TYPE.values_at(*real_estate_type).each do |real_estate_type|
      real_estate_types << real_estate_type
    end
    real_estate_types.join(', ')
  end

  def get_state
    I18n.t("product.states.#{self.aasm.current_state}")
  end

  def self.states
    Product.aasm.states.map do |state|
      [I18n.t("product.states.#{state}"), state.name.to_s]
    end.uniq
  end

  def display_label_state(label)
    case label
    when 'Còn hàng'
      'status-for-sale'
    when 'Đang lock'
      'status-locking'
    when 'Đã đặt cọc'
      'status-deposited'
    when 'Đã bán', 'Sàn khác giữ', 'Sàn khác bán', 'CĐT Thu hồi'
      'status-sold'
    when 'Hết hàng'
      'status-out-of-stock'
    end
  end

  def is_state?(state)
    self.state.to_sym == state.to_sym
  end

  def get_deals
    deals.joins(:assignee).where(state: %w(deposit lock contract_signed)).pluck(:id, :name, :full_name)
  end

  def can_lock?
    return false unless self.for_sale?
    product_lock_histories.where.not(state: %i(enqueued out_of_time canceled failed)).empty?
  end

  def get_campaign_locks
    project.campaign_locks.where("products && ?", "{#{self.id}}")
  end

  private

  def helpers
    @helpers ||= Class.new do
      include ActionView::Helpers::NumberHelper
      include ApplicationHelper
      include NumberFormatHelper
    end.new
  end

  def delete_division_nil
    if self.floor_id == nil
      division = self.block
      layout = Layout.where(block_id: division.id).first
      product_code = self.code.gsub(division.name, '')
    else
      division = self.floor
      layout = Layout.where("?=ANY(floor_ids)", division.id).first
      product_code = self.code.gsub("#{self.block.name}#{division.name}", '')
    end
    coordinate = Coordinate.find_by(layout_id: layout&.id, product_code: product_code)
    if coordinate
      coordinate.destroy
    end
  end

  def validation_action
    if self.state_changed? && state_was == 'sold' && self.deals.contract_signed.present?
      errors.add(:state, "Không thể thay đổi trạng thái của sản phẩm đã ở trạng thái \"#{I18n.t("product.states.#{state_was}")}\"")
    end
  end

  def after_save_action
    if saved_change_to_state?
      # CĐT Thu hồi/Sàn khác giữ/Đã bán bởi sàn khác
      if %w(recall third_holding third_sold sold locking deposited).include?(state)
        NotificationService.product_recall_third_holding_third_sold(self)
        self.deals.where('deals.state': %w(lock confirm book deposit contract_signed completed)).update_all({ product_id: nil, state: 'lock' })
        if self.project.products.for_sale.size == 0 && self.project.labels.include?(0)
          self.project.update(labels: self.project.labels -= [0])
        end
        
        self.get_campaign_locks.each { |campaign_lock| CampaignLockJob.perform_later('check_out_of_stock', campaign_lock) }
      end
      # Sàn khác giữ/Thu hồi/Sàn khác bán/Đã bán =>>> Còn hàng
      state_prev = previous_changes['state'].first
      if %w(recall third_holding sold third_sold locking deposited).include?(state_prev) && state == 'for_sale'
        NotificationService.product_change_for_sale(self)
        if self.project.products.for_sale.size > 0 && !self.project.labels.include?(0)
          self.project.update(labels: self.project.labels += [0])
        end
      end
    end
  end
end

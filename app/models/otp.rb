class Otp < ApplicationRecord
  before_create :set_data

  enum otp_type: [:sign_up, :forgot_password]

  validates :phone_number, presence: true
  validate :check_phone_valid

  enum status: [:open, :confirmed]

  def generate_otp
    ('0'..'9').to_a.shuffle.first(6).join
  end

  private

  def check_phone_valid
    errors.add(:phone_number, :invalid) if !Phonelib.parse(self.phone_number, "VN").valid? && self.phone_number.present?
    errors.add(:phone_number, :taken) if User.where(phone_number: self.phone_number).present? && self.otp_type != 'forgot_password'
  end

  def set_data
    self.otp = generate_otp
    self.expiry = Time.current + 180.seconds
  end
end

class VisitJourney < ApplicationRecord
  include ActiveModel::Validations

  validates :title, :visited_link, :visited_date , presence: true
  validates :visited_link, valid_url: true
end

class Commission < ApplicationRecord
  belongs_to :deal

  # Callbacks
  after_save :send_notification_when_update_commission, if: -> { temporary_commission_previously_changed? || actually_commission_previously_changed? }

  # Validations
  validates :cross_selling_agent_code, presence: true, if: -> { check_validate_commission_fields('cross_selling_agent_code') && self.is_according_to_formula? }
  validates :applicable_policy, presence: true, if: -> { check_validate_commission_fields('applicable_policy') && self.is_according_to_formula? }
  validates :seller_type, presence: true, if: -> { check_validate_commission_fields('seller_type') && self.is_according_to_formula? }
  validates :reti_commission_rate, presence: true, if: -> { check_validate_commission_fields('reti_commission_rate') && self.is_according_to_formula? }
  validates :reti_commission_rate, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 100 }, allow_blank: true
  validates :reti_actually_rate, presence: true, if: -> { check_validate_commission_fields('reti_actually_rate') && self.is_according_to_formula? }
  validates :reti_actually_rate, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 100 }, allow_blank: true
  validates :reti_commission_rate_for_seller, presence: true, if: -> { check_validate_commission_fields('reti_commission_rate_for_seller') && self.is_according_to_formula? }
  validates :reti_commission_rate_for_seller, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 100 }, allow_blank: true
  validates :seller_commission_rate_from_reti, presence: true, if: -> { check_validate_commission_fields('seller_commission_rate_from_reti') && self.is_according_to_formula? }
  validates :seller_commission_rate_from_reti, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 100 }, allow_blank: true
  validates :seller_commission_rate_will_be_received, presence: true, if: -> { check_validate_commission_fields('seller_commission_rate_will_be_received') && self.is_according_to_formula? }
  validates :seller_commission_rate_will_be_received, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 100 }, allow_blank: true
  validates :price_to_calculate_commission_for_seller, presence: true, if: -> { check_validate_commission_fields('price_to_calculate_commission_for_seller') && self.is_according_to_formula? }
  validates :price_to_calculate_commission_for_seller, numericality: { greater_than_or_equal_to: 0 }, allow_blank: true
  validates :temporary_commission, presence: true, if: -> { check_validate_commission_fields('temporary_commission') && !self.is_according_to_formula? }
  validates :temporary_commission, numericality: { greater_than_or_equal_to: 0 }, allow_blank: true
  validates :actually_commission, presence: true, if: -> { check_validate_commission_fields('actually_commission') }
  validates :actually_commission, numericality: { greater_than_or_equal_to: 0 }, allow_blank: true

  def check_validate_commission_fields(field_name)
    return false if User.current.super_admin?
    if User.current.role.present? && deal.state == 'contract_signed' && get_permission_commission.present?
      get_permission_commission.commission_fields_validation.include?(field_name)
    end
  end

  def check_commission_fields_can_view(field_name)
    return true if User.current.super_admin?
    if User.current.role.present? && get_permission_commission.present?
      get_permission_commission.commission_fields_can_view.include?(field_name) || get_permission_commission.commission_fields_can_edit.include?(field_name)
    end
  end

  def check_commission_fields_can_edit(field_name)
    return true if User.current.super_admin?
    if User.current.role.present? && get_permission_commission.present?
      get_permission_commission.commission_fields_can_edit.include?(field_name)
    end
  end

  def is_according_to_formula?
    calculation_type == 0
  end

  def get_calculation_type_label
    calculation_type == 0 ? 'Công thức %' : 'Cố định'
  end

  def send_notification_when_update_commission
    if deal.contract_signed?
      NotificationService.update_commission(deal, self)
    end
  end

  private

  def get_permission_commission
    User.current.role.permissions.where(name: 'Hoa hồng')&.first
  end
end

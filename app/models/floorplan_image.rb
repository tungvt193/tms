class FloorplanImage < ApplicationRecord
  
  # Uploader
  mount_uploaders :images, ProjectFloorplanImageUploader

  # Associations
  belongs_to :project
  
  # Validations
  validates :real_estate_type, presence: true
  validates :images, presence: true
end

# This model will contain all constants of project.
# Can store options for select here if you want.
# Create constant for a model with prefix its name.
# IMPORTANT: You should add comment for each constant!!!
class Constant < ApplicationRecord

  ### Constant for Project
  PROJECT_REAL_ESTATE_TYPE = { 0 => 'Apartel', 1 => 'Căn hộ', 2 => 'Condotel', 3 => 'Duplex', 4 => 'Penthouse', 5 => 'Sky villa',
                               6 => 'Hometel', 7 => 'Officetel', 8 => 'Sàn thương mại', 9 => 'Biệt thự', 10 => 'Bungalow', 11 => 'Đất nền',
                               12 => 'Liền kề', 13 => 'Shophouse', 14 => 'Shoptel', 15 => 'Đất TMDV', 16 => 'Mini Hotel', 17 => 'Townhouse' } # Loại hình BĐS
  PROJECT_FEATURES = { 0 => 'Mới', 1 => 'Độc quyền', 2 => 'Nổi bật', 3 => 'Flash sale', 4 => 'Sắp mở bán', 5 => 'Đang mở bán' } # Đặc tính dự án
  PROJECT_LABELS = { 0 => 'Đang mở bán', 1 => 'CSBH tốt', 2 => 'Độc quyền', 3 => 'Ngừng bán' } # Nhãn dự án (Hiển thị trên app)
  PROJECT_INTERNAL_UTILITIES = { 0 => 'Bệnh viện', 1 => 'Trung tâm thương mại', 2 => 'Gym', 3 => 'Spa', 4 => 'Bể bơi',
                                 5 => 'Công viên', 6 => 'Khu vui chơi trẻ em', 7 => 'Trường học', 8 => 'Vườn BBQ',
                                 9 => 'Rạp chiếu phim', 10 => 'Khu vui chơi giải trí' } # Tiện ích nội khu
  PROJECT_OWNERSHIP_PERIOD = { 0 => 'Có thời hạn', 1 => 'Lâu dài' } # Thời hạn sở hữu
  PROJECT_BANK = { 0 => 'NH An Bình - ABBANK', 1 => 'NH Á Châu - ACB', 2 => 'NH NN&PT Nông thôn Việt Nam - Agribank', 3 => 'NH Bắc Á - Bac A Bank',
                   4 => 'NH Bảo Việt - BAOVIET Bank', 5 => 'NH Đầu tư và Phát triển Việt Nam - BIDV', 6 => 'NH Xây dựng - CB', 7 => 'NH Đông Á - DongA Bank',
                   8 => 'NH Xuất Nhập Khẩu - Eximbank', 9 => 'NH Dầu khí toàn cầu - GPBank', 10 => 'NH Phát triển TP.Hồ Chí Minh - HDBank',
                   11 => 'NH Bưu điện Liên Việt - LienVietPostBank', 12 => 'NH Quân Đội - MB', 13 => 'NH Hàng Hải - MSB', 14 => 'NH Nam Á - Nam A Bank',
                   15 => 'NH Quốc dân - NCB',
                   16 => 'NH Phương Đông - OCB', 17 => 'NH Đại Dương - OceanBank', 18 => 'NH Public Bank Việt Nam - PBVN', 19 => 'NH Xăng dầu Petrolimex - PG Bank',
                   20 => 'NH Đại Chúng Việt Nam - PVcomBank', 21 => 'NH Sài Gòn Thương Tín - Sacombank', 22 => 'NH Sài Gòn Công Thương - SAIGONBANK',
                   23 => 'NH Sài Gòn - SCB', 24 => 'NH Đông Nam Á - SeABank', 25 => 'NH Sài Gòn Hà Nội - SHB', 26 => 'NH Shinhan Việt Nam - SHBVN',
                   27 => 'NH Kỹ Thương - Techcombank', 28 => 'NH Tiên Phong - TPBank', 29 => 'NH Quốc tế - VIB', 30 => 'NH Bản Việt - Viet Capital Bank',
                   31 => 'NH Việt Á - VietABank', 32 => 'NH Việt Nam Thương Tín - Vietbank', 33 => 'NH Ngoại Thương Việt Nam - Vietcombank',
                   34 => 'NH Công Thương Việt Nam - Vietinbank', 35 => 'NH Việt Nam Thịnh Vượng - VPBank' } # Ngân hàng hỗ trợ vay
  PROJECT_LEGAL_DOCUMENTS = { 0 => 'VB Tư cách pháp lý/chứng chỉ hành nghề của CĐT', 1 => 'Quy hoạch chi tiết tỉ lệ 1/500 được phê duyệt', 2 => 'QĐ giao đất/cho thuê đất',
                              3 => 'QĐ phê duyệt chủ trương đầu tư dự án', 4 => 'Giấy tờ về quyền sử dụng đất', 5 => 'Giấy phép xây dựng' } # Pháp lý dự án

  ### Constant for Customer
  CUSTOMER_GENDER = { 0 => 'Nam', 1 => 'Nữ', 2 => 'Khác' }
  CUSTOMER_DATA_SOURCE = { 0 => 'Data import', 1 => 'Kinh doanh', 2 => 'Facebook', 3 => 'Google', 4 => 'Event',
                           5 => 'SMS brandname', 6 => 'Viber', 7 => 'Zalo', 8 => 'Messenger',
                           9 => 'Email MKT', 10 => 'Ad-network', 11 => 'PR', 12 => 'Form' }
  CUSTOMER_CUSTOMER_CHARACTERISTIC = { 0 => 'Khách hàng có thu nhập cao' }
  CUSTOMER_MARITAL_STATUS = {
    0 => 'Độc thân',
    1 => 'Đã đính hôn',
    2 => 'Đã kết hôn',
    3 => 'Ly thân',
    4 => 'Ly hôn',
    5 => 'Góa'
  }
  
  ### Constant for Deal
  # New Sources(current index: 56)
  SOURCES = {
    sale: {
      online: {
        fb_ads: 30,
        supported_ads: 53,
        web_post: 31,
        seeding: 32,
        email: 33,
        zalo: 34
      },
      offline: {
        telesale: 35,
        onsite: 36,
        referral: 37
      }
    },
    company: {
      online: {
        fb_ads: 38,
        seeding: 39,
        email_marketing: 40,
        gg_search_ads: 41,
        organic_search: 42,
        direct_traffic: 43,
        gdn: 44,
        post: 50,
        zalo_ads: 56
      },
      offline: {
        telesale: 45,
        sms: 46,
        hotline: 47,
        onsite: 48,
        referral: 49,
        F2: 52,
        external_agent: 51,
        from_investor: 54,
        bod: 55
      }
    }
  }
  SOURCE_TYPES = {
    sale: SOURCES[:sale].map { |k, v| v.values }.flatten,
    company: SOURCES[:company].map { |k, v| v.values }.flatten
  }
  DEAL_SOURCES = (SOURCES[:sale][:online].map { |k, v| [v, I18n.t("deal.sources.sale.online.#{k}")] } +
    SOURCES[:sale][:offline].map { |k, v| [v, I18n.t("deal.sources.sale.offline.#{k}")] } +
    SOURCES[:company][:online].map { |k, v| [v, I18n.t("deal.sources.company.online.#{k}")] } +
    SOURCES[:company][:offline].map { |k, v| [v, I18n.t("deal.sources.company.offline.#{k}")] }).to_h
  LABELS_FILTER = [
    'Chưa liên hệ',
    'Đã liên hệ',
    'Máy bận/ Không nghe máy',
    'Thuê bao',
    'Đã mua bên khác',
    'Khách thay đổi nhu cầu',
    'Thông tin không phù hợp',
    'Spam',
    'Đã mua sản phẩm tương tự',
    'Môi giới',
    'Chưa có số điện thoại'
  ]
  CONTACT_STATUSES = {
    0 => 'Chưa liên hệ',
    1 => 'Đã liên hệ',
    2 => 'Máy bận/ Không nghe máy',
    3 => 'Thuê bao'
  }
  INTERACTION_CHECKLIST = {
    0 => 'Tư vấn tổng quan dự án (Chủ đầu tư, Vị trí)',
    1 => 'Tư vấn loại hình',
    2 => 'Tư vấn pháp lý',
    3 => 'Tư vấn tiến độ',
    4 => 'Tư vấn tài chính'
  }
  PROBLEMS = {
    0 => 'Vị trí',
    1 => 'Giá cả',
    2 => 'Dự án khác',
    3 => 'Pháp lý',
    4 => 'Tiềm năng đầu tư',
    5 => 'Khác'
  }
  ONLINE_FEEDBACKS = {
    0 => 'Email',
    1 => 'SMS'
  }
  CANCELED_REASON = {
    10 => 'Đã mua bên khác',
    11 => 'Khách thay đổi nhu cầu',
  }
  APPOINTMENT_STATE = {
    0 => 'Đã gặp',
    1 => 'Hủy hẹn',
  }
  FILTER_RESULTS = {
    0 => 'Nhãn',
    1 => 'Ngày tạo',
    2 => 'Ngày cập nhật mới nhất',
    3 => 'Mục đích mua',
    4 => 'Loại hình sản phẩm',
    5 => 'Hướng cửa',
    6 => 'Hướng ban công'
  }
  PURCHASE_PURPOSE = {
    0 => 'Mua để ở',
    1 => 'Mua đầu tư tự kinh doanh',
    2 => 'Mua đầu tư cho thuê',
    3 => 'Mua giữ tài sản'
  }
  HOBBIES_SELECT2_DATA = [
    {
      id: 'g1',
      text: 'Kinh doanh',
      disabled: true,
      class_name: 'l1'
    },
    { id: 0, text: 'Đầu tư bất động sản', parent_id: 'g1', class_name: 'l2', title: 'hidden' },
    { id: 1, text: 'Đầu tư chứng khoán', parent_id: 'g1', class_name: 'l2', title: 'hidden' },
    {
      id: 'g2',
      text: 'Sở thích/ hoạt động',
      disabled: true,
      class_name: 'l1'
    },
    { id: 2, text: 'Thích smart home', parent_id: 'g2', class_name: 'l2', title: 'hidden' },
    { id: 3, text: 'Thích xe sang', parent_id: 'g2', class_name: 'l2', title: 'hidden' },
    { id: 4, text: 'Thích đi du lịch', parent_id: 'g2', class_name: 'l2', title: 'hidden' },
    {
      id: 'g3',
      text: 'Mua sắm và thời trang',
      disabled: true,
      class_name: 'l1'
    },
    { id: 5, text: 'Làm đẹp', parent_id: 'g3', class_name: 'l2', title: 'hidden' },
    { id: 6, text: 'Đồ hiệu', parent_id: 'g3', class_name: 'l2', title: 'hidden' },
    { id: 7, text: 'Quần áo', parent_id: 'g3', class_name: 'l2', title: 'hidden' },
    { id: 8, text: 'Phụ kiện thời trang', parent_id: 'g3', class_name: 'l2', title: 'hidden' },
    {
      id: 'g4',
      text: 'Thể dục, thể thao và hoạt động ngoài trời',
      disabled: true,
      class_name: 'l1'
    },
    {
      id: 'g41',
      text: 'Thể dục',
      disabled: true,
      parent_id: 'g4',
      class_name: 'l2',
      title: 'hidden'
    },
    { id: 9, text: 'Chạy', parent_id: 'g41', class_name: 'l3', title: 'hidden' },
    { id: 10, text: 'Dinh dưỡng', parent_id: 'g41', class_name: 'l3', title: 'hidden' },
    { id: 11, text: 'Phòng tập', parent_id: 'g41', class_name: 'l3', title: 'hidden' },
    { id: 12, text: 'Thiền', parent_id: 'g41', class_name: 'l3', title: 'hidden' },
    { id: 13, text: 'Thế cực', parent_id: 'g41', class_name: 'l3', title: 'hidden' },
    { id: 14, text: 'Yoga', parent_id: 'g41', class_name: 'l3', title: 'hidden' },
    { id: 15, text: 'Zumba', parent_id: 'g41', class_name: 'l3', title: 'hidden' },
    { id: 16, text: 'Ăn kiêng', parent_id: 'g41', class_name: 'l3', title: 'hidden' },
    {
      id: 'g42',
      text: 'Thể thao',
      disabled: true,
      parent_id: 'g4',
      class_name: 'l2',
      title: 'hidden'
    },
    { id: 17, text: 'Golf', parent_id: 'g42', class_name: 'l3', title: 'hidden' },
    { id: 18, text: 'Bóng chuyền', parent_id: 'g42', class_name: 'l3', title: 'hidden' },
    { id: 19, text: 'Bóng rổ', parent_id: 'g42', class_name: 'l3', title: 'hidden' },
    { id: 20, text: 'Bơi lội', parent_id: 'g42', class_name: 'l3', title: 'hidden' },
    { id: 21, text: 'Marathon', parent_id: 'g42', class_name: 'l3', title: 'hidden' },
    { id: 22, text: 'Quần vợt', parent_id: 'g42', class_name: 'l3', title: 'hidden' },
    { id: 23, text: 'Đua xe', parent_id: 'g42', class_name: 'l3', title: 'hidden' },
    {
      id: 'g43',
      text: 'Hoạt động ngoài trời',
      disabled: true,
      parent_id: 'g4',
      class_name: 'l2',
      title: 'hidden'
    },
    { id: 24, text: 'Chèo thuyền', parent_id: 'g43', class_name: 'l3', title: 'hidden' },
    { id: 25, text: 'Câu cá', parent_id: 'g43', class_name: 'l3', title: 'hidden' },
    { id: 26, text: 'Cưỡi ngựa', parent_id: 'g43', class_name: 'l3', title: 'hidden' },
    { id: 27, text: 'Cắm trại', parent_id: 'g43', class_name: 'l3', title: 'hidden' },
    { id: 28, text: 'Lướt sóng', parent_id: 'g43', class_name: 'l3', title: 'hidden' },
    { id: 29, text: 'Săn bắn', parent_id: 'g43', class_name: 'l3', title: 'hidden' },
    { id: 30, text: 'Đi bộ đường dài', parent_id: 'g43', class_name: 'l3', title: 'hidden' },
    { id: 31, text: 'Đạp xe leo núi', parent_id: 'g43', class_name: 'l3', title: 'hidden' },
    {
      id: 'g5',
      text: 'Giải trí',
      disabled: true,
      class_name: 'l1'
    },
    { id: 40, text: 'Đọc sách', parent_id: 'g5', class_name: 'l2', title: 'hidden' },
    { id: 41, text: 'Âm nhạc', parent_id: 'g5', class_name: 'l2', title: 'hidden' },
    { id: 42, text: 'Chương trình truyền hình', parent_id: 'g5', class_name: 'l2', title: 'hidden' },
    { id: 43, text: 'Sự kiện trực tiếp', parent_id: 'g5', class_name: 'l2', title: 'hidden' },
    { id: 44, text: 'Phim', parent_id: 'g5', class_name: 'l2', title: 'hidden' },
    { id: 45, text: 'Trò chơi', parent_id: 'g5', class_name: 'l2', title: 'hidden' }
  ]
  FINANCIAL_CAPABILITY = {
    'high' => 'Tốt',
    'medium' => 'Trung bình',
    'low' => 'Hạn chế',
  }
  PRODUCTS_STATES = {
    0 => 'Không có sản phẩm phù hợp',
    1 => 'Chưa có sản phẩm phù hợp',
    2 => 'Có sản phẩm phù hợp'
  }
  UNINTERESTED_REASON = {
    0 => 'Thông tin không phù hợp',
    1 => 'Spam',
    2 => 'Đã mua sản phẩm tương tự',
    3 => 'Môi giới'
  }
  DEAL_PRICE_RANGE = {
    0 => 'Dưới 3 tỷ',
    1 => '3-5 tỷ',
    2 => '5-7 tỷ',
    3 => '7-10 tỷ',
    4 => '10-15 tỷ',
    5 => '15-20 tỷ',
    6 => 'Trên 20 tỷ'
  }
  DEAL_ACREAGE_RANGE = {
    0 => '50 - 100m2',
    1 => '100 - 150m2',
    2 => '150 - 200m2',
    3 => '200 - 250m2',
    4 => '250 - 300m2',
    5 => '300 - 350m2',
    6 => '350 - 400m2',
    7 => '400 - 450m2',
    8 => '450 - 500m2',
    9 => '500m2 trở lên',
  }
  DEAL_CS_STATE = {
    1 => 'Chưa chăm sóc',
    2 => 'Đã chăm sóc'
  }
  DEMAND_WITH_DEAL = {
    1 => 'Vẫn quan tâm',
    2 => 'Không còn quan tâm',
  }
  SALE_CONSULTING_QUALITY = {
    1 => 'Xuất sắc',
    2 => 'Giỏi',
    3 => 'Trung bình',
    4 => 'Kém'
  }
  PROCESSING_SPEED = {
    1 => 'Nhanh chóng',
    2 => 'Khá nhanh',
    3 => 'Trung bình',
    4 => 'Chậm'
  }
  PRODUCT_SELECTION_CRITERIA = {
    1 => { 1 => 'Gần bệnh viện', 2 => 'Có bể bơi', 3 => 'Có trung tâm thương mại', 4 => 'Có trường mầm non' },
    3 => { 1 => 'Gần bệnh viện', 2 => 'Có bể bơi', 3 => 'Có trung tâm thương mại', 4 => 'Có trường mầm non' },
    4 => { 1 => 'Gần bệnh viện', 2 => 'Có bể bơi', 3 => 'Có trung tâm thương mại', 4 => 'Có trường mầm non' },
    5 => { 1 => 'Gần bệnh viện', 2 => 'Có bể bơi', 3 => 'Có trung tâm thương mại', 4 => 'Có trường mầm non' },
    13 => { 5 => 'Mặt đường lớn', 6 => 'Sát biển hoặc khu vực trung tâm', 7 => 'Căn góc', 8 => 'Chủ đầu tư uy tín' },
  }

  ### Constant for Commission
  COMMISSION_CALCULATION_TYPE = {
    0 => 'Tính theo công thức %',
    1 => 'Cố định'
  }
  COMMISSION_APPLICABLE_POLICY = {
    0 => 'Không vay',
    1 => 'Vay'
  }
  COMMISSION_SELLER_TYPE = {
    0 => 'Nội bộ',
    1 => 'CTV',
    2 => 'BOD',
    3 => 'F2'
  }
  ### Constant for User
  USER_MAIN_JOB = {
    0 => 'Bất động sản',
    1 => 'Bảo hiểm',
    2 => 'Du lịch',
    3 => 'Kinh doanh tự do',
    4 => 'Đầu tư',
    5 => 'Khác'
  }

  ACCOUNT_TYPE = {
    'internal' => 'Nội bộ',
    'agent' => 'CTV',
    'f2' => 'F2',
  }

  ### Constant for Voucher
  VOUCHER_TYPE = {
    1 => 'Cho khách huỷ',
    2 => 'Cho khách ký HĐ'
  }

  ### Other Constant
  ICONS = {
    0 => 'fa-info-circle', # info
    1 => 'fa-exclamation-circle', # warning
    2 => 'fa-times-circle-o', # error
    3 => 'fa-check-circle', # success
    4 => 'fa-question-circle' # question
  }
  TICKET_TYPE = {
    0 => "Đăng ký tư vấn",
    1 => "Đăng ký xem nhà mẫu",
    2 => "Tính thử giá",
    3 => "Quan tâm dự án"
  }
  TICKET_SOURCE = {
    0 => "Form liên hệ tại footer",
    1 => "Trang Dự án",
    2 => "Trang Sản phẩm"
  }
  CUSTOM_HTML_TYPES = {
    0 => 'home-newspaper-feedback'
  }

  # Product columns
  LOW_COLUMNS = [
    {
      title: I18n.t("product.template.low.tag"),
      field: :tag,
      type: 'dropdown',
      value: Reti::Product::TAG
    },
    {
      title: I18n.t("product.template.low.code"),
      field: :code,
      type: 'text',
      value: [1, 255]
    },
    {
      title: I18n.t("product.template.low.subdivision"),
      field: :subdivision_id,
      type: 'text',
      value: [1, 255]
    },
    {
      title: I18n.t("product.template.low.block"),
      field: :block_id,
      type: 'text',
      value: [1, 255]
    },
    {
      title: I18n.t("product.template.low.name"),
      field: :name,
      type: 'text',
      value: [1, 255]
    },
    {
      title: I18n.t("product.template.low.real_estate_type"),
      field: :real_estate_type,
      type: 'dropdown',
      value: Reti::Product::REAL_ESTATE_TYPES[1]
    },
    {
      title: I18n.t("product.template.low.product_type"),
      field: :product_type,
      type: 'text',
      value: [1, 255]
    },
    {
      title: I18n.t("product.template.low.direction"),
      field: :direction,
      type: 'dropdown',
      value: Reti::Product::DIRECTIONS
    },
    {
      title: I18n.t("product.template.low.plot_area"),
      field: :plot_area,
      type: 'numeric',
      value: [0]
    },
    {
      title: I18n.t("product.template.low.density"),
      field: :density,
      type: 'numeric',
      value: [0]
    },
    {
      title: I18n.t("product.template.low.floor_area"),
      field: :floor_area,
      type: 'numeric',
      value: [0]
    },
    {
      title: I18n.t("product.template.low.statics"),
      field: :statics,
      type: 'text',
      value: [1, 255]
    },
    {
      title: I18n.t("product.template.low.currency"),
      field: :currency,
      type: 'dropdown',
      value: Reti::Product::CURRENCIES
    },
    {
      title: I18n.t("product.template.low.price"),
      field: :price,
      type: 'numeric',
      value: [0]
    },
    {
      title: I18n.t("product.template.low.sum_price"),
      field: :sum_price,
      type: 'numeric',
      value: [0]
    },
    {
      title: I18n.t("product.template.low.certificate"),
      field: :certificate,
      type: 'dropdown',
      value: Reti::Product::CERTIFICATES
    },
    {
      title: I18n.t("product.template.low.use_term"),
      field: :use_term,
      type: 'dropdown',
      value: Reti::Product::USE_TERMS
    },
    {
      title: I18n.t("product.template.low.handover_standards"),
      field: :handover_standards,
      type: 'text',
      value: [1, 255]
    },
    {
      title: I18n.t("product.template.low.deposit"),
      field: :deposit,
      type: 'numeric',
      value: [0]
    },
    {
      title: I18n.t("product.template.low.source"),
      field: :source,
      type: 'dropdown',
      value: Reti::Product::SOURCE
    },
    {
      title: I18n.t("product.template.low.label"),
      field: :label,
      type: 'dropdown',
      value: Reti::Product::LABEL
    }
  ]

  HIGH_COLUMNS = [
    {
      title: I18n.t("product.template.high.tag"),
      field: :tag,
      type: 'dropdown',
      value: Reti::Product::TAG
    },
    {
      title: I18n.t("product.template.high.code"),
      field: :code,
      type: 'text',
      value: [1, 255]
    },
    {
      title: I18n.t("product.template.high.subdivision"),
      field: :subdivision_id,
      type: 'text',
      value: [1, 255]
    },
    {
      title: I18n.t("product.template.high.block"),
      field: :block_id,
      type: 'text',
      value: [1, 255]
    },
    {
      title: I18n.t("product.template.high.floor"),
      field: :floor_id,
      type: 'text',
      value: [1, 255]
    },
    {
      title: I18n.t("product.template.high.name"),
      field: :name,
      type: 'text',
      value: [1, 255]
    },
    {
      title: I18n.t("product.template.high.real_estate_type"),
      field: :real_estate_type,
      type: 'dropdown',
      value: Reti::Product::REAL_ESTATE_TYPES[0]
    },
    {
      title: I18n.t("product.template.high.product_type"),
      field: :product_type,
      type: 'text',
      value: [1, 255]
    },
    {
      title: I18n.t("product.template.high.direction"),
      field: :direction,
      type: 'dropdown',
      value: Reti::Product::DIRECTIONS
    },
    {
      title: I18n.t("product.template.high.built_up_area"),
      field: :built_up_area,
      type: 'numeric',
      value: [0]
    },
    {
      title: I18n.t("product.template.high.carpet_area"),
      field: :carpet_area,
      type: 'numeric',
      value: [0]
    },
    {
      title: I18n.t("product.template.high.currency"),
      field: :currency,
      type: 'dropdown',
      value: Reti::Product::CURRENCIES
    },
    {
      title: I18n.t("product.template.high.price"),
      field: :price,
      type: 'numeric',
      value: [0]
    },
    {
      title: I18n.t("product.template.high.sum_price"),
      field: :sum_price,
      type: 'numeric',
      value: [0]
    },
    {
      title: I18n.t("product.template.high.certificate"),
      field: :certificate,
      type: 'dropdown',
      value: Reti::Product::CERTIFICATES
    },
    {
      title: I18n.t("product.template.high.use_term"),
      field: :use_term,
      type: 'dropdown',
      value: Reti::Product::USE_TERMS
    },
    {
      title: I18n.t("product.template.high.furniture"),
      field: :furniture,
      type: 'dropdown',
      value: Reti::Product::FURNITURES
    },
    {
      title: I18n.t("product.template.high.furniture_quality"),
      field: :furniture_quality,
      type: 'dropdown',
      value: Reti::Product::FURN_QUALITIES
    },
    {
      title: I18n.t("product.template.high.deposit"),
      field: :deposit,
      type: 'numeric',
      value: [0]
    },
    {
      title: I18n.t("product.template.high.source"),
      field: :source,
      type: 'dropdown',
      value: Reti::Product::SOURCE
    },
    {
      title: I18n.t("product.template.high.label"),
      field: :label,
      type: 'dropdown',
      value: Reti::Product::LABEL
    }
  ]
end

class Task < ApplicationRecord
  acts_as_paranoid

  # Callbacks
  after_update :update_deal_cached_at

  belongs_to :deal

  def overdue?
    self.deadline && self.deadline < Time.now
  end

  def update_deal_cached_at
    return unless self.deal
    self.deal.update_column(:cached_at, Time.now)
  end
end

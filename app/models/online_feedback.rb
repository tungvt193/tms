class OnlineFeedback < ApplicationRecord
  include WriteHistory
  acts_as_paranoid

  belongs_to :author, class_name: 'User', foreign_key: :created_by_id, required: false
  belongs_to :deal

  validates :action_type, :action_date, :content, presence: true

  after_initialize :set_action_type

  private
    def set_action_type
      self.action_type ||= 0
    end
end

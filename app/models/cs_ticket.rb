class CsTicket < ApplicationRecord

  belongs_to :deal, optional: true
  belongs_to :author, class_name: 'User', foreign_key: :created_by_id, required: false
  belongs_to :voucher, optional: true
end

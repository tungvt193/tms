class Kpi < ApplicationRecord
  ATTRS = [:team_id, :year, :jan, :feb, :mar, :apr, :may, :jun, :jul, :aug, :sep, :oct, :nov, :dec]

  belongs_to :team
  delegate :name, to: :team

  validates :year, :team_id, presence: true
  validates_uniqueness_of :team_id, scope: :year, message: "Đã tồn tại KPI cho nhóm kinh doanh này!"

  def self.current_kpi team_id
    self.find_by(year: Time.zone.now.year, team_id: team_id).try(Date::ABBR_MONTHNAMES[Time.zone.now.month].downcase.to_sym)
  end
end

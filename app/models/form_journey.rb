class FormJourney < ApplicationRecord
  belongs_to :customer
  # Validates
  validates :recent_conversion_date,presence: true
end

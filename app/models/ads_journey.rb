class AdsJourney < ApplicationRecord
  include ActiveModel::Validations
  validates :fb_ad_name, :fb_campaign_name, :fb_ad_group_name, :create_date, presence: true
end

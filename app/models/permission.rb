class Permission < ApplicationRecord
  # Associations
  belongs_to :role

  # Callback
  after_initialize :set_state_area

  DATA_AREA = {
    'Cá nhân' => 0,
    'Cá nhân & cấp dưới' => 1,
    'Tất cả' => 2
  }

  # key cua LIST_MODEL_NAME va GROUP_MODEL phai giong nhau
  LIST_MODEL_NAME = {
    "Khách hàng" => 'Customer',
    "Tài khoản nội bộ" => 'User',
    "Chủ đầu tư" => "Investor",
    "Tổng thầu" => "Constructor",
    "Đơn vị phát triển" => "Development",
    "Đơn vị quản lý" => "Operator",
    "Thông tin dự án" => "Project",
    "Bảng hàng" => "Product",
    "Mặt bằng" => "Layout",
    "Đăng ký tư vấn" => "CustomerTicket",
    "Banner" => "Banner",
    "Tùy chỉnh trang chủ" => "CustomHtml",
    "Tuỳ chỉnh chi tiết dự án" => "CustomDetail",
    "Tài khoản khách hàng" => "Account",
    "Phân quyền" => 'Role',
    "Sơ đồ tổ chức" => 'Group',
    "Giao dịch" => 'Deal',
    "Thiết lập KPI" => 'Kpi',
    "Hành trình tương tác" => 'CustomerJourney',
    "Tin tức dự án" => 'ProjectNewUpdate',
    "Cơ chế doanh thu công ty" => 'CompanyRevenue',
    "Mặt bằng điện tử" => "InteractiveLayout",
    "Hoa hồng" => "Commission",
    "Phiếu CSKH" => "CsTicket",
    "Voucher" => 'Voucher',
    "Lịch lock căn" => 'CampaignLock',
    "Phiếu lock căn" => "ProductLockHistory"
  }

  GROUP_MODEL = {
    "Khách hàng" => {
      "Hành trình tương tác" => 'CustomerJourney',
      "Khách hàng" => 'Customer',
    },
    "Tài khoản nội bộ" => {
      "Tài khoản nội bộ" => 'User',
      "Sơ đồ tổ chức" => 'Group',
    },
    "Chủ đầu tư" => {
      "Chủ đầu tư" => "Investor"
    },
    "Tổng thầu" => {
      "Tổng thầu" => "Constructor"
    },
    "Đơn vị phát triển" => {
      "Đơn vị phát triển" => "Development",
    },
    "Đơn vị quản lý" => {
      "Đơn vị quản lý" => "Operator",
    },
    "Dự án" => {
      "Thông tin dự án" => "Project",
      "Bảng hàng" => "Product",
      "Mặt bằng" => "Layout",
      "Mặt bằng điện tử" => "InteractiveLayout",
      "Lịch lock căn" => "CampaignLock",
      "Phiếu lock căn" => "ProductLockHistory"
    },
    "Website" => {
      "Banner" => "Banner",
      "Tùy chỉnh trang chủ" => "CustomHtml",
      "Tuỳ chỉnh chi tiết dự án" => "CustomDetail",
    },
    "Đăng ký tư vấn" => {
      "Đăng ký tư vấn" => "CustomerTicket",
    },
    "Tài khoản khách hàng" => {
      "Tài khoản khách hàng" => "Account",
    },
    "Phân quyền" => {
      "Phân quyền" => 'Role',
    },
    "Quản lý giao dịch" => {
      "Giao dịch" => 'Deal'
    },
    "Hoa hồng" => {
      "Hoa hồng" => 'Commission',
    },
    "Dashboard" => {
      "Tin tức dự án" => 'ProjectNewUpdate',
      "Thiết lập KPI" => 'Kpi'
    },
    "Cơ chế" => {
      "Cơ chế doanh thu công ty" => 'CompanyRevenue'
    },
    "Chăm sóc khách hàng" => {
      "Voucher" => 'Voucher',
      "Phiếu CSKH" => "CsTicket"
    }
  }

  def get_deal_state_area
    Deal.aasm.states.map do |state|
      [I18n.t("deal.states.#{state}"), state]
    end.uniq
  end

  def get_deal_commission_fields
    commission_fields = [['Chọn tất cả', 'check_all']]
    Commission.column_names[5..15].each do |commission|
      # [5..15] Remove commission fields (id, created_at, updated_at, deal_id, calculation_type)
      commission_fields << [I18n.t("deal.commissions.#{commission}"), commission]
    end
    commission_fields
  end

  private

  def set_state_area
    self.state_area = Deal.aasm.states.map { |state| state.to_s } if self.new_record? && self.name == 'Giao dịch'
  end
end

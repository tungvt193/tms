class Deal < ApplicationRecord

  # Include
  include AASM
  include DealSearch
  include UpdateMasterCustomer
  include DealDecorator

  # Attributes Accessor
  attr_accessor :is_update_comm
  attr_accessor :current_next_states
  attr_accessor :form
  attr_accessor :contact_status_when_update # Check contact status to change state when update
  attr_accessor :is_deal_transfer

  # Soft Delete
  acts_as_paranoid

  # Constants
  MAPPING_DEAL_PRODUCT = {
    nil => :for_sale,
    'pending' => :for_sale,
    'interest' => :for_sale,
    'meet' => :for_sale,
    'refundable_deposit' => :for_sale,
    'lock' => :locking,
    'deposit' => :deposited,
    'contract_signed' => :sold,
    'canceled' => :for_sale,
    'request_recall' => :for_sale,
    'uninterested' => :for_sale
  }
  THIRD_MANUAL = %w(recall third_holding third_sold)

  ATTRS = [:name, :customer_id, :product_id, :project_id, :state, :assignee_id, :lock_version, :canceled_reason, :uninterested_reason, :price_range,
           :total_price, :source, :tags, :contact_status, :demand_for_advances, :created_by_id, :updated_by_id, :acreage_range, :product_type_detail,
           :trouble_problem, :purchase_purpose, :product_type, :interested_product, :suggestive_name, :contract_signed_at, :is_external_project, :external_project,
           :from_website, :create_deal_date, :source_detail, :lead_scoring, :product_lock_history_id,
           { product_selection_criteria: [] }, { interaction_detail: [] }, { trouble_problem_list: [] }, { balcony_direction: [] }, { door_direction: [] },
           customer_persona_attributes: [:id, :gender, :second_phone_number, :email, :gender, :identity_card, :domicile, :dob, :nationality,
                                         :nation, :workplace, :financial_capability, :customer_position, :property_ownership, :settlements, :city_id, :district_id,
                                         :ward_id, :street, :marital_status, :people_in_family, { hobbies: [] }, :positions_to_invest, { real_estate_type_to_invest: [] }, :_destroy],
           commission_attributes: [:id, :calculation_type, :cross_selling_agent_code, :applicable_policy, :seller_type, :reti_commission_rate, :reti_actually_rate,
                                   :reti_commission_rate_for_seller, :seller_commission_rate_will_be_received, :seller_commission_rate_from_reti,
                                   :price_to_calculate_commission_for_seller, :temporary_commission, :actually_commission, :_destroy]
  ]
  GENERAL_ATTRS = [:tab, :suggestive_name, :assignee_id, :project_id, :product_id, :cross_selling_product_code, :is_external_project, :external_project, :customer_id, :contact_status, :price_range, :acreage_range, :contact_status_when_update,
                   :demand_for_advances, { trouble_problem_list: [] }, :trouble_problem, :purchase_purpose, { product_selection_criteria: [] }, :product_type, { balcony_direction: [] },
                   { door_direction: [] }, :interested_product, { interaction_detail: [] }, :uninterested_reason, :product_type_detail, customer_persona_attributes: [:id, :gender]
  ]
  CUSTOMER_ATTRS = [:tab, :second_phone_number, :email, customer_persona_attributes: [:id, :second_phone_number, :email, :gender, :identity_card, :domicile, :dob, :nationality,
                                                                                      :nation, :workplace, :financial_capability, :customer_position, :property_ownership, :settlements, :city_id, :district_id,
                                                                                      :ward_id, :street, :marital_status, :people_in_family, { hobbies: [] }, :positions_to_invest, { real_estate_type_to_invest: [] }, :_destroy]
  ]
  COMMISSION_PERCENTAGE_ATTRS = [:tab, :total_price, :contract_signed_at, commission_attributes: [:id, :calculation_type, :cross_selling_agent_code, :applicable_policy, :seller_type, :reti_commission_rate, :reti_actually_rate,
                                                                                                  :reti_commission_rate_for_seller, :seller_commission_rate_will_be_received, :seller_commission_rate_from_reti,
                                                                                                  :price_to_calculate_commission_for_seller, :actually_commission, :_destroy]
  ]
  COMMISSION_FIXED_ATTRS = [:tab, :total_price, :contract_signed_at, commission_attributes: [:id, :calculation_type, :temporary_commission, :actually_commission, :_destroy]
  ]

  SOURCE_SORT = ["NULL", 51, 50, 40, 52, 44, 49, 47, 41, 38, 39, 46, 45, 42, 43, 48, 31, 33, 37, 30, 32, 35, 36, 34]
  PURCHASE_PURPOSE = ["NULL", 3, 4, 1, 2, 0]
  PRODUCT_TYPE = ['NULL', 0, 9, 10, 1, 2, 3, 15, 11, 6, 12, 16, 7, 4, 13, 14, 5, 8]
  PROJECT = Project.all.order(:name).pluck(:id).unshift('NULL')
  LIST_FIELDS = { 'id': 'ID giao dịch', 'name': 'Tên giao dịch', 'project': 'Dự án', 'customer': 'Tên khách hàng',
                  'phone_number': 'Số điện thoại', 'source': 'Nguồn', 'state': 'Trạng thái', 'assignee': 'Người chăm sóc' }
  OPTION_LIST_FIELDS = { 'product': 'Mã sản phẩm', 'contact_status': 'Tình trạng liên hệ', 'interaction_detail': 'Nội dung tương tác', 'gender': 'Giới tính',
                         'product_type': 'Loại hình sản phẩm', 'product_type_detail': 'Chi tiết loại hình', 'price_range': 'Khoảng giá', 'acreage_range': 'Diện tích sản phẩm',
                         'interested_product': 'Căn đang quan tâm', 'purchase_purpose': 'Mục đích mua', 'product_selection_criteria': 'Tiêu chí chọn sản phẩm', 'door_direction': 'Hướng cửa',
                         'balcony_direction': 'Hướng ban công', 'trouble_problem_list': 'Vấn đề vướng mắc', 'trouble_problem': 'Chi tiết vướng mắc', 'source_detail': 'Chi tiết nguồn',
                         'demand_for_advances': 'Nhu cầu vay', 'telesale_note': 'Ghi chú', 'telesale_interest_detail': 'Chi tiết quan tâm', 'second_phone_number': 'Số điện thoại phụ', 'email': 'Email',
                         'city_id': 'Tỉnh/Thành phố', 'district_id': 'Quận/Huyện', 'ward_id': 'Xã/Phường', 'settlements': 'Địa chỉ', 'identity_card': 'Số CMTND/CCCD/Hộ chiếu', 'domicile': 'Nguyên quán', 'dob': 'Ngày sinh',
                         'nationality': 'Quốc tịch', 'workplace': 'Nơi làm việc', 'customer_position': 'Chức vụ', 'financial_capability': 'Đánh giá khả năng tài chính', 'property_ownership': 'Tài sản sở hữu',
                         'marital_status': 'Tình trạng hôn nhân', 'people_in_family': 'Thành viên trong gia đình', 'hobbies': 'Sở thích/Lối sống', 'positions_to_invest': 'Vị trí mong muốn đầu tư',
                         'real_estate_type_to_invest': 'Loại hình dự án mong muốn đầu tư', 'temporary_commission': 'Hoa hồng tạm tính', 'actually_commission': 'Hoa hồng thực nhận' }
  # Delegates
  delegate :code, to: :product, prefix: :product

  # Callbacks
  after_initialize :set_pre_state, :set_is_change_create_date_value
  before_create :create_deal_name
  before_create :set_author_assignee_and_time_fields, if: -> { self.from_app == false }
  before_update :update_deal_name
  before_update :update_label_assignee_at_and_change_state_at, :update_cached_at, :update_state_product
  before_update :update_commission_fields, if: -> { self.form == 'commission' }
  after_save :write_history, unless: -> { self.id_changed? && self.import_id.present? }
  after_commit :update_deal_sort, :update_same_deal
  after_destroy :stream_after_destroy
  after_commit :create_deal_log, on: :create, unless: -> { self.import_id.present? }
  after_commit :update_deal_log, :update_source_customer, on: :update
  before_commit :create_deal_journey, on: :update
  after_create :create_customer_persona, :push_notification_appointment
  after_create :push_notification_to_assignee, if: -> { self.callio_campaign_id.present? && self.assignee.present? && self.project.present? }

  # Associations
  belongs_to :product, optional: true
  belongs_to :project, optional: true
  belongs_to :customer, optional: true
  belongs_to :assignee, class_name: 'User', foreign_key: :assignee_id, optional: true
  belongs_to :author, class_name: 'User', foreign_key: :created_by_id, optional: true
  belongs_to :updater, class_name: 'User', foreign_key: :updated_by_id, optional: true
  belongs_to :product_lock_history, optional: true
  has_many :notes, as: :objectable, dependent: :destroy
  accepts_nested_attributes_for :notes, allow_destroy: true
  has_many :tasks, dependent: :destroy
  accepts_nested_attributes_for :tasks, allow_destroy: true
  has_many :appointments, dependent: :destroy
  accepts_nested_attributes_for :appointments, allow_destroy: true
  has_many :calls, dependent: :destroy
  accepts_nested_attributes_for :calls, allow_destroy: true
  has_many :feedbacks, dependent: :destroy
  accepts_nested_attributes_for :feedbacks, allow_destroy: true
  has_many :online_feedbacks, dependent: :destroy
  accepts_nested_attributes_for :online_feedbacks, allow_destroy: true
  has_many :histories, dependent: :destroy
  accepts_nested_attributes_for :histories, allow_destroy: true
  has_many :deal_sorts, dependent: :destroy
  has_many :reminders, dependent: :destroy
  has_many :notifications, dependent: :destroy
  has_one :deal_log, dependent: :destroy
  has_one :customer_persona, dependent: :destroy, inverse_of: :deal
  accepts_nested_attributes_for :customer_persona, allow_destroy: true
  has_many :deal_journeys, dependent: :destroy
  has_one :commission, dependent: :destroy
  accepts_nested_attributes_for :commission, allow_destroy: true
  has_many :history_interactions, dependent: :destroy, inverse_of: :deal
  accepts_nested_attributes_for :history_interactions, allow_destroy: true
  has_many :cs_tickets, dependent: :destroy
  has_many :transfer_histories

  # Validations
  validate :check_product, on: :update, if: -> { %w(lock deposit contract_signed).include?(state) }
  validates :project_id, presence: true, if: -> { self.from_app || !self.is_external_project || (self.callio_campaign_id.present? && self&.miss_project == true && %w(pending meet interest refundable_deposit).include?(state)) && !User.current.super_admin? }
  validates :customer_id, presence: true, if: -> { self.from_app || (!self.is_customer_form?) }
  validates :product_id, presence: true, if: -> { !self.from_app && User.current.is_sale? && %w(lock deposit contract_signed).include?(state) && !self.is_customer_form? && !self.is_external_project }
  validates :cross_selling_product_code, presence: true, if: -> { !self.from_app && User.current.is_sale? && %w(lock deposit contract_signed).include?(state) && !self.is_customer_form? && self.is_external_project }
  # validates :contact_status, presence: true, inclusion: { in: [1], message: "Bạn cần liên hệ được với khách hàng để chuyển giao dịch sang trạng thái kế tiếp" }, if: -> { !self.from_app && !User.current.is_sale? && %w(interest meet refundable_deposit lock deposit contract_signed).include?(state) && !self.is_customer_form? }
  validates :product_type, presence: true, if: -> { !self.from_app && User.current.is_sale? && %w(interest meet refundable_deposit lock deposit contract_signed).include?(state) && !self.is_customer_form? }
  validates :purchase_purpose, presence: true, if: -> { !self.from_app && User.current.is_sale? && %w(meet refundable_deposit lock deposit contract_signed).include?(state) && !self.is_customer_form? }
  validates :total_price, :contract_signed_at, presence: true, if: -> { !self.from_app && User.current.sale_admin? && %w(contract_signed).include?(state) && !self.is_deal_transfer }
  validates :state, :customer_id, :assignee_id, :product_id, presence: true, if: -> { self.is_deal_transfer }
  validates :external_project, presence: true, if: -> { self.is_external_project }

  # Scopes
  scope :by_ids, -> (ids) { where id: ids }
  scope :with_sort, -> (user_id) { joins("LEFT JOIN deal_sorts ON deal_sorts.user_id = #{user_id} AND deal_sorts.deal_id = deals.id").select("deals.*, deal_sorts.position AS current_position").order("current_position ASC NULLS FIRST, created_at DESC") }
  scope :order_by_created_at, -> (sort) { order("deals.create_deal_date #{sort}") }
  scope :order_by_updated_at, -> (sort) { order("deals.updated_at #{sort}") }
  scope :order_by_source, -> (sort) { order("CASE #{ (sort == 'DESC' ? SOURCE_SORT.reverse : SOURCE_SORT).each_with_index.map { |source, i| "WHEN #{source == "NULL" ? 'deals.source IS NULL' : "deals.source = #{source}"} THEN #{i}" }.join(' ') } END") }
  scope :order_by_project_id, -> (sort) { order("CASE #{ (sort == 'DESC' ? PROJECT.reverse : PROJECT).each_with_index.map { |project, i| "WHEN #{project == "NULL" ? 'deals.project_id IS NULL' : "deals.project_id = #{project}"} THEN #{i}" }.join(' ') } END") }
  scope :order_by_purchase_purpose, -> (sort) { order("CASE #{ (sort == 'DESC' ? PURCHASE_PURPOSE.reverse : PURCHASE_PURPOSE).each_with_index.map { |pp, i| "WHEN #{pp == "NULL" ? 'deals.purchase_purpose IS NULL' : "deals.purchase_purpose = #{pp}"} THEN #{i}" }.join(' ') } END") }
  scope :order_by_product_type, -> (sort) { order("CASE #{ (sort == 'DESC' ? PRODUCT_TYPE.reverse : PRODUCT_TYPE).each_with_index.map { |pt, i| "WHEN #{pt == "NULL" ? 'deals.product_type IS NULL' : "deals.product_type = #{pt}"} THEN #{i}" }.join(' ') } END") }
  scope :without_states, ->(states) { where.not(state: states) }
  scope :without_transfer, -> { where.not(id: TransferHistory.all.pluck(:deal_id)) }

  # AASM
  aasm column: :state, whiny_transitions: false do
    state :pending, initial: true # Chờ xử lý
    state :interest # Đang quan tâm
    state :meet # Đã gặp
    state :refundable_deposit # Cọc thiện chí
    state :lock # Lock căn
    state :deposit # Đã đặt cọc
    state :contract_signed # Ký hợp đồng
    state :canceled # Đã hủy
    state :request_recall # Đang thu hồi
    state :uninterested # Không quan tâm

    event :to_pending do
      transitions from: [:request_recall, :interest, :meet, :refundable_deposit, :lock, :deposit], to: :pending
    end

    event :to_interest do
      transitions from: [:pending, :meet, :refundable_deposit, :lock, :deposit], to: :interest
    end

    event :to_meet do
      transitions from: [:pending, :interest, :refundable_deposit, :lock, :deposit], to: :meet
    end

    event :to_refundable_deposit do
      transitions from: [:pending, :interest, :meet, :lock, :deposit], to: :refundable_deposit
    end

    event :to_lock do
      transitions from: [:pending, :interest, :meet, :refundable_deposit, :deposit], to: :lock
    end

    event :to_deposit do
      transitions from: [:pending, :interest, :meet, :refundable_deposit, :lock], to: :deposit
    end

    event :to_contract_signed do
      transitions from: [:pending, :interest, :meet, :refundable_deposit, :lock, :deposit], to: :contract_signed
    end

    event :to_request_recall do
      transitions from: [:pending, :interest, :meet, :refundable_deposit, :lock, :deposit], to: :request_recall
    end

    event :to_uninterested do
      transitions from: [:pending], to: :uninterested
    end

    event :cancel do
      transitions from: [:pending, :interest, :meet, :refundable_deposit, :lock, :deposit, :contract_signed], to: :canceled
    end
  end

  # ================================================== #

  def is_customer_form?
    self.form == 'customer'
  end

  def is_general_form?
    self.form == 'general'
  end

  def is_commission_form?
    self.form == 'commission'
  end

  # ================================================== #

  def company_source?
    return Constant::SOURCE_TYPES[:company].include?(source)
  end

  def sale_source?
    return Constant::SOURCE_TYPES[:sale].include?(source)
  end

  def before_company_source?
    return Constant::SOURCE_TYPES[:company].include?(source_before_last_save)
  end

  def before_sale_source?
    return Constant::SOURCE_TYPES[:sale].include?(source_before_last_save)
  end

  def render_content(heading, contents = [])
    ApplicationController.renderer.render partial: "admin/histories/history_log",
                                          locals: { heading: heading, contents: contents }
  end

  def get_deal_sort(user)
    self.deal_sorts.find_by(user_id: user.id)
  end

  def get_position(user)
    self.deal_sorts.find_by(user_id: user.id)&.position
  end

  def create_deal_journey
    if state != @pre_state
      self.deal_journeys.create(
        create_date: DateTime.now,
        content: "Đã chuyển trạng thái sang: #{state}"
      )
      if self.author&.agent?
        NotificationService.update_state_deal_from_share_customer(self)
      end
    end
  end

  def update_deal_log
    log = self.deal_log
    if state != @pre_state && log.present?
      current_time = DateTime.now
      case state
      when 'pending'
        log.pending_entered = log.pending_entered ? log.pending_entered : current_time
        log.pending_reentered = current_time
      when 'interest'
        log.interest_entered = log.interest_entered ? log.interest_entered : current_time
        log.interest_reentered = current_time
      when 'meet'
        log.meet_entered = log.meet_entered ? log.meet_entered : current_time
        log.meet_reentered = current_time
      when 'refundable_deposit'
        log.refundable_deposit_entered = log.refundable_deposit_entered ? log.refundable_deposit_entered : current_time
        log.refundable_deposit_reentered = current_time
      when 'lock'
        log.lock_entered = log.lock_entered ? log.lock_entered : current_time
        log.lock_reentered = current_time
      when 'deposit'
        log.deposit_entered = log.deposit_entered ? log.deposit_entered : current_time
        log.deposit_reentered = current_time
      when 'contract_signed'
        log.contract_signed_entered = log.contract_signed_entered ? log.contract_signed_entered : current_time
        log.contract_signed_reentered = current_time
        log.close_date = current_time
        log.days_to_close = (log.close_date - log.created_at).to_i
      when 'canceled'
        log.canceled_entered = log.canceled_entered ? log.canceled_entered : current_time
        log.canceled_reentered = current_time
        log.close_date = current_time
        log.days_to_close = (log.close_date - log.created_at).to_i
      when 'request_recall'
        log.request_recall_entered = log.request_recall_entered ? log.request_recall_entered : current_time
        log.request_recall_reentered = current_time
      end

      case @pre_state
      when 'pending'
        log.pending_exited = current_time
        log.pending_time_in = log.pending_time_in.to_i + (log.pending_exited - log.pending_reentered).to_i
      when 'interest'
        log.interest_exited = current_time
        log.interest_time_in = log.interest_time_in.to_i + (log.interest_exited - log.interest_reentered).to_i
      when 'meet'
        log.meet_exited = current_time
        log.meet_time_in = log.meet_time_in.to_i + (log.meet_exited - log.meet_reentered).to_i
      when 'refundable_deposit'
        log.refundable_deposit_exited = current_time
        log.refundable_deposit_time_in = log.refundable_deposit_time_in.to_i + (log.refundable_deposit_exited - log.refundable_deposit_reentered).to_i
      when 'lock'
        log.lock_exited = current_time
        log.lock_time_in = log.lock_time_in.to_i + (log.lock_exited - log.lock_reentered).to_i
      when 'deposit'
        log.deposit_exited = current_time
        log.deposit_time_in = log.deposit_time_in.to_i + (log.deposit_exited - log.deposit_reentered).to_i
      when 'contract_signed'
        log.contract_signed_exited = current_time
        log.contract_signed_time_in = log.contract_signed_time_in.to_i + (log.contract_signed_exited - log.contract_signed_reentered).to_i
      when 'canceled'
        log.canceled_exited = current_time
        log.canceled_time_in = log.canceled_time_in.to_i + (log.canceled_exited - log.canceled_reentered).to_i
      when 'request_recall'
        log.request_recall_exited = current_time
        log.request_recall_time_in = log.request_recall_time_in.to_i + (log.request_recall_exited - log.request_recall_reentered).to_i
      end
      log.save
    end
  end

  def self.get_source_group
    source_disabled = User.current&.sale_member? || User.current&.sale_leader? || User.current&.sale_manager?
    {
      'NVKD - Online' => Constant::SOURCES[:sale][:online].map { |k, v| [I18n.t("deal.sources.sale.online.#{k}"), v] },
      'NVKD - Offline' => Constant::SOURCES[:sale][:offline].map { |k, v| [I18n.t("deal.sources.sale.offline.#{k}"), v] },
      'Công ty - Online' => Constant::SOURCES[:company][:online].map { |k, v| [I18n.t("deal.sources.company.online.#{k}"), v, { disabled: source_disabled }] },
      'Công ty - Offline' => Constant::SOURCES[:company][:offline].map { |k, v| [I18n.t("deal.sources.company.offline.#{k}"), v, { disabled: source_disabled && v != 45 }] },
    }
  end

  def self.get_team_revenue(user_ids)
    contract_signed_deals = where("contract_signed_at >= ? AND contract_signed_at <= ?", Time.zone.now.beginning_of_month, Time.zone.now.end_of_month).where(assignee_id: user_ids)
    team_revenue = 0
    contract_signed_deals.each do |deal|
      total_price = deal.total_price
      product_revenue = deal.product&.company_revenue
      project_revenue = deal.project&.company_revenues.where(real_estate_type: deal.product&.real_estate_type).where("? BETWEEN start_date AND end_date", deal.contract_signed_at&.to_date).first if deal.project&.company_revenues.present?
      if product_revenue
        team_revenue += total_price * product_revenue.percentage / 100
      elsif project_revenue
        team_revenue += total_price * project_revenue.percentage / 100
      end
    end
    team_revenue
  end

  def can_transfer?
    contract_signed? || deposit?
  end

  # Deal đã chuyển nhượng
  def is_transferred?
    transfer_histories.present?
  end

  def transfer_label
    last_transfer = TransferHistory.by_deal(self.id).order(created_at: :desc).first
    return '' unless last_transfer
    last_transfer.deal_id == self.id ? 'Đã chuyển nhượng' : 'Được chuyển nhượng'
  end

  def push_notification_appointment
    unless self.appointments.present?
      old_value = self.previous_changes[:assignee_id].present? ? self.previous_changes[:assignee_id] : nil
      AppointmentJob.set(wait: 24.hours).perform_later('create_appointment', nil, old_value, self.assignee_id, self)
      job = Sidekiq::ScheduledSet.new.detect { |s| s.args.first["arguments"][3] == self.previous_changes[:assignee_id].first.to_s }
      job.delete if job.present?
    end
  end

  private

  def self.time_history(time)
    time.localtime.strftime Settings.date.formats.strftime_time_history
  end

  def write_history
    # Ngày giờ tạo GD
    if id_previously_changed?
      heading = if self.author.present?
                  "#{Deal.time_history(created_at) } - Giao dịch đã được tạo bởi " +
                    "<span class='detail-item-heading'>#{self.author&.full_name}</span>"
                else
                  "#{Deal.time_history(created_at) } - Giao dịch đã được tạo từ nguồn " +
                    "<span class='detail-item-heading'>#{Constant::DEAL_SOURCES[self.source]}</span>"
                end
      self.histories.create(content: render_content(heading))
    end

    # Thông tin người được phân công GD:
    if assignee_id_previously_changed? && self.assignee && self.author != self.assignee
      heading = "#{Deal.time_history(updated_at) } - " +
        "#{Role::ROLE_SHORT[assignee&.role&.name]} <span class='detail-item-heading'>#{assignee&.full_name}</span> được phân công chăm sóc giao dịch bởi " +
        "#{Role::ROLE_SHORT[User.current&.role&.name]} <span class='detail-item-heading'>#{User.current&.full_name}</span>"
      self.histories.create(content: render_content(heading))
    end

    # Thông tin các lần chỉnh sửa phiếu:
    col_allow_log = %w(name project_id product_id customer_id contact_status demand financial_capability demand_for_advances trouble_problem_list trouble_problem state interaction_status interaction_detail source)
    previous_changes_custom = previous_changes.merge(customer_persona&.previous_changes || {}).select { |k, v| col_allow_log.include?(k) }

    if !id_previously_changed? && previous_changes_custom.present?
      # if previous_changes_custom.present?
      heading = "#{Deal.time_history(updated_at) } - " +
        "#{Role::ROLE_SHORT[User.current&.role&.name]} <span class='detail-item-heading'>#{User.current&.full_name}</span> đã cập nhật thông tin cho giao dịch:"
      contents = []
      previous_changes_custom.each do |col_k, col_v|
        val_from = col_v[0]
        val_to = col_v[1]
        next if col_v[0].blank? && col_v[1].blank?

        case col_k
        when 'project_id'
          val_from = Project.find_by_id(val_from)&.name
          val_to = Project.find_by_id(val_to)&.name
        when 'product_id'
          val_from = Product.find_by_id(val_from)&.code
          val_to = Product.find_by_id(val_to)&.code
        when 'customer_id'
          val_from = Customer.find_by_id(val_from)&.name
          val_to = Customer.find_by_id(val_to)&.name
        when 'contact_status'
          val_from = Constant::CONTACT_STATUSES[val_from]
          val_to = Constant::CONTACT_STATUSES[val_to]
        when 'trouble_problem_list'
          val_from = "blank"
          val_to = "Đã cập nhật"
        when 'state'
          val_from = I18n.t("deal.states.#{val_from}") if val_from
          val_to = I18n.t("deal.states.#{val_to}")
        when 'interaction_detail'
          val_from = "blank"
          val_to = "Đã cập nhật"
        when 'source'
          val_from = Constant::DEAL_SOURCES[val_from]
          val_to = Constant::DEAL_SOURCES[val_to]
        end
        val_from = if val_from.present?
                     val_from != 'blank' ? "#{val_from} ->" : ""
                   else
                     'Chưa cập nhật ->'
                   end
        val_to = val_to.present? ? val_to : 'Chưa cập nhật'

        contents.push("#{Deal.human_attribute_name(col_k)}: #{val_from} #{val_to}")
      end
      self.histories.create(content: render_content(heading, contents))
    end
  end

  def update_deal_sort
    # destroy sort record when change state
    if state_previously_changed?
      DestroyDealSortJob.perform_later(self, User.current&.id)
    end
    # Change assignee || change state
    if assignee_id_previously_changed?
      UpdateDealSortJob.perform_later(self)
    end
  end

  def update_same_deal
    if previous_changes.present? && !(previous_changes.keys.count == 1 && previous_changes[:cached_at].present?)
      DealStreamJob.set(wait: 1.seconds).perform_later(self, User.current&.id)
    end
    if self.deleted_at
      Deal.update_all("same_deals = array_remove(same_deals, '#{self.id}')")
    end
    if previous_changes[:customer_id].present? || previous_changes[:project_id].present?
      Deal.update_all("same_deals = array_remove(same_deals, '#{self.id}')")
      if self.customer&.phone_number.present?
        same_deals = if self.project_id
                       Deal.where.not(id: self.id).joins(:customer).where('customers.phone_number': self&.customer.phone_number).where('deals.project_id': [self.project_id, nil])
                     else
                       Deal.where.not(id: self.id).joins(:customer).where('customers.phone_number': self&.customer.phone_number)
                     end
        self.update_column(:same_deals, same_deals.pluck('id'))
        same_deals.update_all("same_deals = array_append(same_deals, '#{self.id}')")
      else
        self.update_column(:same_deals, [])
      end
    end
  end

  def stream_after_destroy
    DealStreamJob.set(wait: 1.seconds).perform_later(self, User.current&.id)
  end

  def create_deal_log
    log = build_deal_log(:"#{self.state}_entered".to_sym => created_at, :"#{self.state}_reentered".to_sym => created_at)
    log.save
  end

  def update_source_customer
    if self.source && self.customer && self.customer.data_source.blank?
      deal_first = self.customer.deals.order(:created_at).first
      self.customer.update_columns(data_source: deal_first&.source) if deal_first&.source
    end
  end

  def set_pre_state
    @pre_state = self.state
    self.interaction_detail ||= []
  end

  def set_is_change_create_date_value
    self.is_change_create_date ||= false
  end

  def set_author_assignee_and_time_fields
    self.author = User.current
    self.assignee ||= User.current
    self.assigned_at = Time.now if self.assignee.present?
    self.state_changed_at = Time.now
    self.labels = self.labels.push("Chưa có số điện thoại") if customer_id && customer.phone_number.blank?
    self.create_deal_date ||= self.created_at
    self.cached_at = Time.now
  end

  def create_deal_name
    if self.is_external_project
      self.name = "#{self&.customer&.name&.titleize_name} - #{self.external_project}"
    elsif self.callio_campaign_id.present? && self.miss_project && !self.project.present?
      self.name = self.customer.name.titleize_name
    else
      self.name = "#{self&.customer&.name&.titleize_name} - #{self&.project&.name&.titleize_name}"
    end
  end

  def update_deal_name
    if self.is_external_project
      self.name = "#{self&.customer&.name&.titleize_name} - #{self.external_project}"
    end
  end

  def update_label_assignee_at_and_change_state_at
    self.labels = [Constant::CONTACT_STATUSES[contact_status]].reject(&:blank?)
    self.labels = self.labels.push("Chưa có số điện thoại") if customer_id && customer.phone_number.blank? && !self.labels.include?("Chưa có số điện thoại")
    self.assigned_at = Time.now if self.assignee_id_changed?
    unless self.trouble_problem_list && self.trouble_problem_list.include?(5)
      self.trouble_problem = nil
    end
    if self.state_changed?
      self.state_changed_at = Time.now
      case self.state
      when 'request_recall'
        self.assignee_id = User.current.id
      when 'canceled'
        self.labels = [Constant::CANCELED_REASON[self.canceled_reason]]
      when 'uninterested'
        self.labels = [Constant::UNINTERESTED_REASON[self.uninterested_reason]]
      end
    end
  end

  def update_commission_fields
    if commission.calculation_type == 1
      commission.attributes = { cross_selling_agent_code: nil, applicable_policy: nil, seller_type: nil, reti_commission_rate: nil,
                                reti_actually_rate: nil, reti_commission_rate_for_seller: nil, seller_commission_rate_will_be_received: nil,
                                seller_commission_rate_from_reti: nil, price_to_calculate_commission_for_seller: nil }
    else
      temporary_commission = nil
      if commission.seller_commission_rate_will_be_received && commission.price_to_calculate_commission_for_seller
        temporary_commission = (commission.seller_commission_rate_will_be_received * commission.price_to_calculate_commission_for_seller * 90 / 10000).round
      end
      commission.attributes = { temporary_commission: temporary_commission }
    end
    if self.transfer_histories.present?
      commission.temporary_commission = 0 if commission.temporary_commission.present?
      commission.actually_commission = 0 if commission.actually_commission.present?
    end
  end

  def update_cached_at
    self.cached_at = Time.now if self.changed?
  end

  def update_state_product
    if MAPPING_DEAL_PRODUCT[state_was] != MAPPING_DEAL_PRODUCT[state] && self.product_id_changed?
      old_product = Product.find_by(id: product_id_was)
      if old_product.present?
        old_product.state = 'for_sale'
        old_product.save(validate: false)
      end
      self.product&.state = MAPPING_DEAL_PRODUCT[state] if MAPPING_DEAL_PRODUCT[state] != :for_sale
    elsif MAPPING_DEAL_PRODUCT[state_was] != MAPPING_DEAL_PRODUCT[state]
      self.product&.state = MAPPING_DEAL_PRODUCT[state]
    elsif self.product_id_changed?
      old_product = Product.find_by(id: product_id_was)
      if old_product.present?
        old_product.state = 'for_sale'
        old_product.save(validate: false)
      end
      self.product&.state = MAPPING_DEAL_PRODUCT[state] if MAPPING_DEAL_PRODUCT[state] != :for_sale
      if state == 'contract_signed' && self.product&.deals&.reject { |d| d == self }.present?
        if product.deals.select { |d| d.state == 'contract_signed' }.size == 0
          NotificationService.product_recall_third_holding_third_sold(self.product)
        end
        self.product.deals.each do |deal|
          unless deal.state == 'contract_signed'
            deal.update_column(:product_id, nil)
          end
        end
      end
    end
    self.product.save(validate: false) if self.product.present?
  end

  def check_product
    if %w(lock deposit contract_signed).include?(state) && product
      if product.state != 'for_sale' && MAPPING_DEAL_PRODUCT[state_was] == :for_sale && MAPPING_DEAL_PRODUCT[state] != :for_sale
        errors.add(:product_id, "Sản phẩm không còn hàng. Vui lòng cập nhật Mã sản phẩm mới cho giao dịch")
      end
    end
  end

  def create_customer_persona
    CustomerPersona.create(deal: self, customer: self.customer) unless self.customer_persona.present?
  end

  def push_notification_to_assignee
    NotificationService.deal_from_callio(self)
    DealMailer.deal_from_callio(self)
    ZaloService.send_sms_callio_lead(self.assignee, self.project)
  end
end

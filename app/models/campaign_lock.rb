class CampaignLock < ApplicationRecord

  include AASM

  # Callbacks
  after_create :create_campaign_lock, :push_notification_campaign_lock
  before_create :check_products_deposited

  # Associations
  belongs_to :project

  # Validations
  validate :validate_session_opening_time
  validates :enable_locked_for_hot_product, numericality: { only_integer: true, greater_than: 0 }
  validates :unc_confirmation_period_for_sale, numericality: { only_integer: true, greater_than: 0 }
  validates :unc_confirmation_period_for_sale_admin, numericality: { only_integer: true, greater_than: 0 }

  # AASM
  aasm column: :state, whiny_transitions: false do
    state :upcoming, initial: true # Sắp mở bán
    state :locking # Đang mở bán
    state :out_of_stock # Hết hàng
  end

  def validate_session_opening_time
    if self.session_opening_time.present? && self.new_record?
      if self.session_opening_time < Date.today
        self.errors.add :session_opening_time, "Thời gian phải lớn hơn thời gian hiện tại"
      end
    end
  end

  def get_products
    if self.products.present?
      Product.where(id: products).pluck(:code).join(', ')
    end
  end

  def display_label_state(label)
    case label
    when 'upcoming'
      'label-campaign-lock-upcoming'
    when 'locking'
      'label-campaign-lock-locking'
    when 'out_of_stock'
      'label-campaign-lock-out-of-stock'
    end
  end

  def create_campaign_lock
    CampaignLockJob.set(wait_until: session_opening_time).perform_later('locking', self)
  end

  def check_products_deposited
    if products.present? && Product.where(id: products).select { |p| p.state != 'deposited' }.blank?
      self.state = 'out_of_stock'
    end
  end

  def push_notification_campaign_lock
    CampaignLockJob.perform_later('create_campaign_lock', self)
    if self.session_opening_time > Time.zone.now + 15.minutes
      CampaignLockJob.set(wait_until: self.session_opening_time - 15.minutes).perform_later('before_campaign_lock_open', self)
    end
    CampaignLockJob.set(wait_until: self.session_opening_time).perform_later('campaign_lock_open', self)
  end
end

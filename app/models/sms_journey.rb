class SmsJourney < ApplicationRecord
  validates :sms_subject, :phone_received, :sms_detail, :sms_created_date , presence: true
end

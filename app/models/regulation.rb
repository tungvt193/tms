class Regulation < ApplicationRecord
  include AASM
  include RegulationSearch

  attr_accessor :date_range
  ATTRS = [:name, :state, :project_id, {real_estate_type: []}, :start_date, :end_date, :manager_comm, :admin_comm, :leader_comm, :leader_duplicate_comm, :free_leader_comm, :free_leader_duplicate_comm, :manager_duplicate_comm, :member_comm_sale, :member_comm_company]
  validates :name, :state, :project_id, :real_estate_type, :start_date, :end_date, :manager_comm, :admin_comm, :leader_comm, :member_comm_sale, :member_comm_company, presence: true
  validate :check_duplicate

  belongs_to :project, optional: true

  scope :by_ids, -> (ids) { where id: ids }
  scope :by_current, -> (regulation) { where.not(id: regulation.id).where(project_id: regulation.project_id).where("real_estate_type && #{regulation.real_estate_type.blank? ? '?' : 'ARRAY[?]'}", regulation.real_estate_type)
    .where("(:start_date BETWEEN start_date AND end_date) OR (:end_date BETWEEN start_date AND end_date) OR (:end_date >= end_date AND :start_date <= start_date)", start_date: regulation.start_date&.beginning_of_day, end_date: regulation.end_date&.end_of_day)
    .where(state: ['pending', 'applying'])}
  scope :expired_list, -> { where("end_date::date < ?", Date.today).where.not(state: ['expired', 'not_apply', 'pending']) }
  scope :by_deal, -> (deal) { where(project_id: deal.project_id).where("real_estate_type && ARRAY[?]", [deal.product&.real_estate_type])
    .where("? BETWEEN start_date AND end_date", deal.contract_signed_at&.to_date)
    .where.not(state: ['pending']) }

  # Callbacks
  before_update :set_state
  before_create :set_state
  after_commit :update_deal_commission

  # AASM
  aasm column: :state, whiny_transitions: false do
    after_all_transitions :action_after_transition

    state :pending, initial: true           # Chờ duyệt
    state :applying                         # Đang áp dụng
    state :not_apply                        # Ngừng áp dụng
    state :expired                          # Hết hạn

    event :to_pending do
      transitions from: [:applying, :not_apply, :expired], to: :pending
    end

    event :to_applying do
      transitions from: [:pending, :not_apply, :expired], to: :applying
    end

    event :to_not_apply do
      transitions from: [:applying, :pending, :expired], to: :not_apply
    end

    event :to_expired do
      transitions from: [:applying, :not_apply, :pending], to: :expired
    end
  end

  def action_after_transition

  end

  def next_states
    case aasm.current_state
    when :pending
      [:applying, :not_apply]
    when :applying
      [:not_apply]
    when :not_apply
      [:applying]
    when :expired
      []
    end
  end

  def set_state
    self.to_expired if self.end_date && self.end_date.to_date < Date.today && self.applying?
    self.to_pending if self.end_date && self.end_date_changed? && self.end_date.to_date >= Date.today && !self.not_apply?
  end

  def update_deal_commission
    Deal.where(state: 'contract_signed', project_id: self.project_id).each do |deal|
      deal.is_update_comm =  true
      deal.save
    end
  end

  def check_duplicate
    return unless self.project_id && self.real_estate_type && self.start_date && self.end_date
    errors.add(:date_range, "Cơ chế đã tồn tại trong hệ thống!") if Regulation.by_current(self)&.first.present?
  end
end

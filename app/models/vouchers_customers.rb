class VouchersCustomers < ApplicationRecord
  belongs_to :customer
  belongs_to :voucher

  def get_deal_name
    self.cs_ticket_id.present? ? Deal.find(self.deal_id).name : ''
  end
end
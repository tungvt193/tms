class TicketNote < ApplicationRecord
  #chuyển sang dùng polymorphic bảng note
    # Associations
  belongs_to :customer_ticket
  belongs_to :author, class_name: 'User', foreign_key: :created_by_id, required: false

  # Validations
  validates :content, presence: true
end

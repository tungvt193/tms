class Banner < ApplicationRecord
  mount_uploader :banner_desktop, BannerUploader
  mount_uploader :banner_mobile, BannerUploader
  mount_uploader :banner_app, BannerUploader

  # Validations
  validates :title ,presence: true, if: -> { banner_type == 0 || banner_type == 2 }
  # validates :banner_app, presence: true, if: -> { banner_type == 2 }
  validates :link, allow_blank: true, format: { with: URI::regexp }
  validates :banner_mobile, presence: true, if: -> { banner_type == 2 }

  before_create :init_position

  scope :banner_for_app, -> { where(banner_type: 2) }

  def init_position
    self.position = Banner.where(banner_type: self.banner_type).maximum(:position) || 0
  end

  def banner_app_url
    self.banner_app.try(:url)
  end
end

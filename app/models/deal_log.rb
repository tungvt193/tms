class DealLog < ApplicationRecord
  acts_as_paranoid
  belongs_to :deal
end

class Layout < ApplicationRecord

  attr_writer :current_step
  attr_writer :current_action

  mount_uploader :image, LayoutUploader

  # Constants
  LAYOUT_ATTRS = [:name, :image, :number_of_product, :level, :project_id, :subdivision_id, :block_id, {floor_ids: []},
                  coordinates_attributes: [:id, :layout_id, :product_id, :product_code, :positions, :_destroy ]]

  # Association
  has_many :coordinates, inverse_of: :layout, dependent: :destroy
  accepts_nested_attributes_for :coordinates, allow_destroy: true
  belongs_to :subdivision, class_name: 'Division', optional: true
  belongs_to :block, class_name: 'Division', optional: true
  belongs_to :project

  # Validations
  validates :name, presence: true, if: ->{current_action != 'back' && current_step == '1'}
  validates :image, presence: true, if: ->{current_action != 'back' && current_step == '1'}
  validates :number_of_product, presence: true, if: ->{current_action != 'back' && current_step == '1'}
  validates :subdivision_id, presence: true, if: ->{current_action != 'back' && current_step == '2'}
  validates :block_id, presence: true, if: ->{current_action != 'back' && current_step == '2'}
  validates :level, presence: true, if: ->{current_action != 'back' && current_step == '2'}
  validates :floor_ids, presence: true, allow_blank: false, if: ->{current_action != 'back' && current_step == '2' && level == 0}
  validate :check_number_of_product

  def floors_name
    Division.where(id: floor_ids)&.pluck(:name).join(', ')
  end

  def current_step
    @current_step || steps.first
  end

  def current_action
    @current_action || 'next'
  end

  def steps
    %w[1 2 3]
  end

  def next_step
    self.current_step = steps[steps.index(current_step)+1]
  end

  def previous_step
    self.current_step = steps[steps.index(current_step)-1]
  end

  def first_step?
    current_step == steps.first
  end

  def last_step?
    current_step == steps.last
  end

  def all_valid?
    steps.all? do |step|
      self.current_step = step
      valid?
    end
  end

  def back
    self.current_action = 'back'
  end

  def next
    self.current_action = 'next'
  end

  # check number_of_product > coordinates
  def check_number_of_product
    errors.add(:coordinates, 'Mặt bằng đã đủ số lượng sản phẩm, vui lòng kiểm tra lại.') if self.number_of_product && self.number_of_product < self.coordinates.reject{|p| p._destroy}.size
  end
end

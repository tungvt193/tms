class CustomerPersona < ApplicationRecord
  acts_as_paranoid
  belongs_to :deal
  belongs_to :customer, optional: true

  # Validations
  validates :second_phone_number, format: { with: /\A([+]\w|)+[0-9]*$\z/ }
  validates :email, allow_blank: true, format: { with: /\A^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$\z/ }
  validates :identity_card, allow_blank: true, format: { with: /\A^[a-zA-Z0-9]+$\z/ }
  validate  :validate_dob
  validates :email, presence: true, if: -> { User.current.is_sale? && %w(deposit contract_signed).include?(deal.state) && !deal.is_general_form? }
  validates :settlements, :city_id, :district_id, :ward_id, :identity_card, :domicile, :dob, :nationality,
            presence: true, if: -> { (User.current.sale_admin? || User.current.customer_service?) && %w(refundable_deposit lock deposit contract_signed completed).include?(deal.state) && !deal.is_general_form? }
  validates :gender, presence: true, if: -> { User.current.is_sale? && %w(interest meet refundable_deposit lock deposit contract_signed).include?(deal.state) && !deal.is_general_form? }

  # ================================================== #

  def get_dob
    dob.present? ? dob.strftime('%d/%m/%Y') : ''
  end

  def validate_dob
    if self.dob.present?
      if self.dob >= Date.today
        self.errors.add :dob, "Ngày sinh phải nhỏ hơn #{Date.today.strftime('%d/%m/%Y')}"
      end
    end
  end
end
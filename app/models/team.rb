class Team < ApplicationRecord
  has_many :kpis
  belongs_to :user

  def self.for_kpi
    Team.where(user_id: User.current.root.descendant_ids)
        .select { |e| (e.user.sale_leader? || (e.user.sale_member? && e.user.parent && !e.user.parent.sale_leader?)) && !e.user.deactivated }
  end
end

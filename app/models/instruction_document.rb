class InstructionDocument < ApplicationRecord

  # Uploader
  mount_uploader :file, InstructionDocumentUploader

  # Validations
  validates :title, presence: true
  validates :file, presence: true
end
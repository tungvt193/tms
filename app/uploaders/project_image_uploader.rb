class ProjectImageUploader < CarrierWave::Uploader::Base
  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  include CarrierWave::MiniMagick
  process :quality => 85

  # Choose what kind of storage to use for this uploader:
  # if Rails.env === 'development'
  #   storage :file
  # else
  #   storage :fog
  # end
  storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url(*args)
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process scale: [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  # version :thumb do
  #   process resize_to_fit: [50, 50]
  # end
  version :thumb_slider, if: :is_project_image? do
    process resize_to_fill: [360, 200]
  end
  version :highlight, if: :is_project_image? do
    process resize_to_fill: [170, 170]
  end
  version :highlight_mobile, if: :is_project_image? do
    process resize_to_fill: [128, 175]
  end
  version :main_slider, if: :is_project_image? do
    process resize_to_fill: [645, 480]
  end
  version :main_slider_mobile, if: :is_project_image? do
    process resize_and_pad: [375, 230]
  end
  version :sub_slider, if: :is_project_image? do
    process resize_to_fill: [100, 69]
  end
  version :thumb, unless: :is_project_image? do
    process :process_version
  end

  def process_version
    case mounted_as
    when :internal_utility_images, :external_utility_images, :design_style_images
      resize_to_fill(360, 250)
    when :icon
      resize_to_fit(73, 73)
    when :preview_image
      resize_to_fill(265, 242)
    end
  end
  
  # Check is field images of project
  def is_project_image?(file)
    model.class.to_s.underscore == 'project' && mounted_as == :images
  end
  
  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  # def extension_whitelist
  #   %w(jpg jpeg gif png)
  # end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end
end

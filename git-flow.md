# RETI GIT FLOW
#### 1. Before start coding
- Fetch new coding version from Bitbucket
```
git checkout staging
git fetch origin
git rebase origin/staging
```
- Checkout new branch from `staging`
```
git checkout -b <branch-name>
```
#### 2. After finish coding and before push code to Bitbucket
- Fetch and rebase `staging` to avoid conflict
- Resolve if have conflict while rebase
#### 3. Create PR in Bitbucket
- Paste link issue to PR's description

# IMPORTANT NOTES
#### 1. Branch name convention
- Fix bug: `bugfix/<ticket-number>`
- New feature: `feature/<ticket-number>`
- Hot fix: `hotfix/staging-<ticket-number>` || `hotfix/production-<ticket-number>`
#### 2. Others
- 1 PR per 1 issue